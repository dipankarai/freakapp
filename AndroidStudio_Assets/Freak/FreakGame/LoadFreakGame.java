package in.Freak.FreakGame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

/**
 * Created by dipankerhackin on 3/6/17.
 */

public class LoadFreakGame extends Activity
{
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void LoadFreakGameActivity (String gamename, int gameType, String data, String classActivity)
    {
        Log.i("DemoActivity", "=====>> SwitchActivity " + gamename);

        Class<?> unityClassActivity = null;
        try
        {
            unityClassActivity = Class.forName(classActivity);
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        SharedPreferences gameData = UnityPlayer.currentActivity.getSharedPreferences ("freakapp.game.data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = gameData.edit();
        editor.clear();
        editor.putString("freakapp.game.data", data);
        editor.commit();

        Intent intent = new Intent(UnityPlayer.currentActivity, org.cocos2dx.cpp.AppActivity.class);
        UnityPlayer.currentActivity.startActivityForResult(intent, 100);

//        if (gamename.contentEquals("FreakQuiz") || gameType == 7)
//        {
//            Log.i("DemoActivity", "=====>> SwitchActivity ");
//
//            Intent intent = new Intent(UnityPlayer.currentActivity, org.cocos2dx.cpp.AppActivity.class);
//            UnityPlayer.currentActivity.startActivityForResult(intent, 100);
//        }
//        else if (gamename.contentEquals("SandCastle") || gameType == 6)
//        {
//            Log.i("DemoActivity", "=====>> SwitchActivity ");
//
//            Intent intent = new Intent(UnityPlayer.currentActivity, org.cocos2dx.cpp.AppActivity.class);
//            UnityPlayer.currentActivity.startActivityForResult(intent, 100);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
