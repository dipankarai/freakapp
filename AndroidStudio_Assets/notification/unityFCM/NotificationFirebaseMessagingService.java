package in.notification.unityFCM;

import android.app.*;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.astricstore.imageandvideopicker.OverrideUnity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonArray;
import in.freakapp.app.R;
import com.unity3d.player.UnityPlayer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.unity3d.player.UnityPlayerActivity;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by dipanker on 12/12/16.
 */

public class NotificationFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "NotificationFCM";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String msg=remoteMessage.getData().get("message");
            JsonElement payLoad = new JsonParser().parse(remoteMessage.getData().get("sendbird"));

            Log.d(TAG, "IsNotificationDisable -----" + IsNotificationDisable());

            if (IsNotificationDisable() == false)
            {
                sendNotification(msg, payLoad);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    private void sendNotification(String message, JsonElement payload) {
        // Your own way to show notifications to users.

        Log.d(TAG, "Message data: " + message);
        Log.d(TAG, "Message data payload: " + payload.toString());

        String senderName = "";
        String senderId = "";
        String channelUrl = "";
        String messageSend = message;

        if (payload.isJsonObject())
        {
            JsonObject obj = payload.getAsJsonObject();
            JsonObject senderDict = obj.getAsJsonObject("sender");
            senderName = senderDict.get("name").getAsString();
            senderId = senderDict.get("id").getAsString();

//            String msgType = obj.get("type").getAsString();
//            if (msgType.contentEquals("FILE"))
//            {
//                JsonArray filesArray = obj.getAsJsonArray("files");
//                JsonObject filesDict = filesArray.get(0).getAsJsonObject();
//                String fileType = filesDict.get("type").getAsString();

//                messageSend = senderName + " sent you a " + GetFileTypeMsg(fileType) + " file.";
//            }

            //CHANNEL
            JsonObject channelDict = obj.getAsJsonObject("channel");
            channelUrl = channelDict.get("channel_url").getAsString();
            channelUrl = channelUrl.replace("\"", "");
        }

        Intent intent = new Intent(this, OverrideUnity.class);
//        Intent intent = new Intent(this, UnityPlayer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setAction("ClickFromNotification");
        intent.putExtra("FromNotification", true);

        Log.d(TAG, "freak.open.Chat " + channelUrl);

        SharedPreferences mute = getSharedPreferences ("freak.open.Chat", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mute.edit();
        editor.clear();
        editor.putString("freak.open.Chat", channelUrl);
        editor.commit();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("Freak")
                .setContentText(messageSend)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Log.d(TAG, "IsAllNotificationMute " + IsAllNotificationMute());
        if (IsAllNotificationMute())
        {
            notificationBuilder.setSound(defaultSoundUri, Notification.DEFAULT_VIBRATE);
        }
        else
        {
            Log.d(TAG, "IsNotificationMuted " + IsNotificationMuted(channelUrl));
            if (IsNotificationMuted(channelUrl))
            {
                notificationBuilder.setSound(defaultSoundUri, Notification.DEFAULT_VIBRATE);
            }
            else
            {
                notificationBuilder.setSound(defaultSoundUri);
            }
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public static final String MUTE_ALL_NOTIFICATION = "com.freak.app.muteallnotification";
    public static final String MUTE_NOTIFICATION = "com.freak.app.mutenotification";
    public static final String DISABLE_NOTIFICATION = "com.freak.app.allnotification";

    private boolean IsNotificationDisable () {
        SharedPreferences allMuteNotification = getSharedPreferences(DISABLE_NOTIFICATION, Context.MODE_PRIVATE);
        int defaultValue = allMuteNotification.getInt(DISABLE_NOTIFICATION, Context.MODE_PRIVATE);

        if (defaultValue == 1)
            return true;

        return false;
    }

    private boolean IsAllNotificationMute () {

        SharedPreferences allMuteNotification = getSharedPreferences(MUTE_ALL_NOTIFICATION, Context.MODE_PRIVATE);
        int defaultValue = allMuteNotification.getInt(MUTE_ALL_NOTIFICATION, Context.MODE_PRIVATE);

        if (defaultValue == 1)
            return true;

        return false;
    }

    private boolean IsNotificationMuted (String channelID)
    {
        SharedPreferences muuteNotification = getSharedPreferences(MUTE_NOTIFICATION, Context.MODE_PRIVATE);
        String defaultValue = muuteNotification.getString(MUTE_NOTIFICATION, "");

        if (defaultValue.isEmpty() || defaultValue == null) {
            return false;
        }

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                JSONArray channelArray = new JSONArray(defaultValue);

                for (int i=0; i< channelArray.length(); i++)
                {
                    if (channelID.contains(channelArray.getString(i)))
                    {
                        return true;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("JSON ARRAY Exception->","" + e.getMessage());
            return false;
        }

        return false;
    }

    String GetFileTypeMsg (String filetype)
    {
        switch (filetype)
        {
            case "1":
                return "Audio Buzz";
            case "2":
                return "Contact";
            case "3":
                return "Doc";
            case "4":
            case "11":
                return "Image";
            case "5":
                return "Location";
            case "6":
                return "Video";
            case "7":
            case "10":
                return "Audio";
            case "8":
                return "Stickers";
            case "9":
                return "Others";
            case "12":
                return "Challenge";
            case "13":
                return "Referral";
            default:
                return "Others";
        }
    }
}
