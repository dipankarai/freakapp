package in.notification.unityFCM;

import android.content.Context;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import in.freakapp.app.R;
import android.support.v4.app.NotificationCompat;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

public class UnityNotificationManager extends BroadcastReceiver
{

    private static final String TAG = "LocalNotification";

    public static void SetNotification(int id, String title, String message, String msgSender, String messageId, int sound, int vibrate,
            int lights, String largeIconResource, String smallIconResource, int executeMode, String unityClass)
    {
        Log.d(TAG, "Message: " + message);

        Activity currentActivity = UnityPlayer.currentActivity;
        Resources res = currentActivity.getResources();

        Class<?> unityClassActivity = null;
        try
        {
            unityClassActivity = Class.forName(unityClass);
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        Intent notificationIntent = new Intent(currentActivity, NotificationActionService.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.setAction(ACTION_1);
        notificationIntent.putExtra("notid", id);
        notificationIntent.putExtra("messageId", messageId);

        PendingIntent contentIntent = PendingIntent.getService(currentActivity, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(currentActivity)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setContentText(msgSender +": "+ message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(contentIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);

        if(message != null && message.length() > 0)
            builder.setTicker(message);

        if (smallIconResource != null && smallIconResource.length() > 0)
            builder.setSmallIcon(res.getIdentifier(smallIconResource, "drawable", currentActivity.getPackageName()));

        if (largeIconResource != null && largeIconResource.length() > 0)
            builder.setLargeIcon(BitmapFactory.decodeResource(res, res.getIdentifier(largeIconResource, "drawable", currentActivity.getPackageName())));

        if(sound == 1)
            builder.setSound(defaultSoundUri);
        else
            builder.setSound(defaultSoundUri, Notification.DEFAULT_VIBRATE);

        if(vibrate == 1)
            builder.setVibrate(new long[] {
                    1000L, 1000L
            });

        if(lights == 1)
            builder.setLights(Color.GREEN, 3000, 3000);

        NotificationManager notificationManager = (NotificationManager) currentActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, builder.build());
    }

//    public static void SetNotification(int id, String title, String message, String msgSender, String messageId, int sound, int vibrate,
//                                       int lights, String largeIconResource, String smallIconResource, int executeMode, String unityClass) {
//        Log.d(TAG, "Message: " + message);
//
//        Activity currentActivity = UnityPlayer.currentActivity;
//        AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(currentActivity, UnityNotificationManager.class);
//        intent.putExtra("id", id);
//        intent.putExtra("title", title);
//        intent.putExtra("message", message);
//        intent.putExtra("ticker", message);
//        intent.putExtra("sender", msgSender);
//        intent.putExtra("messageid", messageId);
//        intent.putExtra("sound", sound == 1);
//        intent.putExtra("vibrate", vibrate == 1);
//        intent.putExtra("lights", lights == 1);
//        intent.putExtra("l_icon", largeIconResource);
//        intent.putExtra("s_icon", smallIconResource);
//        intent.putExtra("activity", unityClass);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//        {
//        	if (executeMode == 2)
//        		am.setExactAndAllowWhileIdle(0, 0, PendingIntent.getBroadcast(currentActivity, id, intent, 0));
//        	else if (executeMode == 1)
//        		am.setExact(0, 0, PendingIntent.getBroadcast(currentActivity, id, intent, 0));
//        	else
//        		am.set(0, 0, PendingIntent.getBroadcast(currentActivity, id, intent, 0));
//        }
//        else
//        	am.set(0, 0, PendingIntent.getBroadcast(currentActivity, id, intent, 0));
//    }

    public static void SetRepeatingNotification(int id, long delay, String title, String message, String ticker, long rep, int sound, int vibrate, int lights,
    		String largeIconResource, String smallIconResource, int bgColor, String unityClass)
    {
    	Activity currentActivity = UnityPlayer.currentActivity;
    	AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(currentActivity, UnityNotificationManager.class);
        intent.putExtra("ticker", ticker);
        intent.putExtra("title", title);
        intent.putExtra("message", message);
        intent.putExtra("id", id);
        intent.putExtra("color", bgColor);
        intent.putExtra("sound", sound == 1);
        intent.putExtra("vibrate", vibrate == 1);
        intent.putExtra("lights", lights == 1);
        intent.putExtra("l_icon", largeIconResource);
        intent.putExtra("s_icon", smallIconResource);
        intent.putExtra("activity", unityClass);
    	am.setRepeating(0, System.currentTimeMillis() + delay, rep, PendingIntent.getBroadcast(currentActivity, id, intent, 0));
    }

    public static final String ACTION_1 = "action_1";
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "Context: " + context.getPackageName());

    	int id = intent.getIntExtra("id", 0);
        String title = intent.getStringExtra("title");
        String message = intent.getStringExtra("message");
        String ticker = intent.getStringExtra("ticker");
        String sender = intent.getStringExtra("sender");
        String messageId = intent.getStringExtra("messageid");
        Boolean sound = intent.getBooleanExtra("sound", false);
        Boolean vibrate = intent.getBooleanExtra("vibrate", false);
        Boolean lights = intent.getBooleanExtra("lights", false);
        String s_icon = intent.getStringExtra("s_icon");
        String l_icon = intent.getStringExtra("l_icon");
//        int color = intent.getIntExtra("color", 0);
        String unityClass = intent.getStringExtra("activity");

        Resources res = context.getResources();

        Class<?> unityClassActivity = null;
		try
		{
			unityClassActivity = Class.forName(unityClass);
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

        //Activity currentActivity = UnityPlayer.currentActivity;
        Intent notificationIntent = new Intent(context, NotificationActionService.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.setAction(ACTION_1);
        notificationIntent.putExtra("notid", id);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent contentIntent = PendingIntent.getService(context, (id-1), notificationIntent, PendingIntent.FLAG_ONE_SHOT);
//        Notification.Builder builder = new Notification.Builder(context);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setContentText(sender +": "+ message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(contentIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);

        if(ticker != null && ticker.length() > 0)
            builder.setTicker(ticker);

		if (s_icon != null && s_icon.length() > 0)
			builder.setSmallIcon(res.getIdentifier(s_icon, "drawable", context.getPackageName()));

		if (l_icon != null && l_icon.length() > 0)
			builder.setLargeIcon(BitmapFactory.decodeResource(res, res.getIdentifier(l_icon, "drawable", context.getPackageName())));

        if(sound)
            builder.setSound(RingtoneManager.getDefaultUri(2));

        if(vibrate)
            builder.setVibrate(new long[] {
                    1000L, 1000L
            });

        if(lights)
            builder.setLights(Color.GREEN, 3000, 3000);


        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

//        Notification notification = new Notification.InboxStyle(builder)
//                .addLine("Sender: " +sender)
//                .addLine("Message: " +message)
//                .setBigContentTitle(sender + " send you a message.")
//                .build();
//
        notificationManager.notify(id, builder.build());
    }

    public static void CancelNotification(int id)
    {
        Activity currentActivity = UnityPlayer.currentActivity;
        AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(currentActivity, UnityNotificationManager.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(currentActivity, id, intent, PendingIntent.FLAG_NO_CREATE);
        if (pendingIntent != null)
        {
            am.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }

    public static void CancelAll(){
        NotificationManager notificationManager = (NotificationManager)UnityPlayer.currentActivity.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static class NotificationActionService extends IntentService {

        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        private static String notid;
        private static String messageId;
        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            notid = Integer.toString(intent.getIntExtra("notid",0));
            messageId = intent.getStringExtra("messageId");

            if (ACTION_1.equals(action)) {
                UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run () {
                        UnityPlayer.UnitySendMessage("NativeBridgeHandler", "LocalNotification", messageId);
                    }
                });
            }
        }
    }
}
