package in.notification.unityFCM;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.astricstore.imageandvideopicker.OverrideUnity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import org.json.JSONArray;
import org.json.JSONObject;


public class Notification {

    private static final String TAG = "Notification";
    public static final String MUTE_ALL_NOTIFICATION = "com.freak.app.muteallnotification";
    public static final String MUTE_NOTIFICATION = "com.freak.app.mutenotification";

    public static final String DISABLE_NOTIFICATION = "com.freak.app.allnotification";

    public static void GetFCMNotificationToken(String msg)
    {
        Log.e("FCM Token -before ->","############################ "+msg);

        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run () {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.e("FCM Token after ----->", " " + token);
                UnityPlayer.UnitySendMessage("NativeBridgeHandler", "GetFCM_NotificationToken", token);
            }
        });
    }

    public static void CloseAllNotification (int id)
    {
        Log.e("CLOSE NOTI ->","############################ " + id);

        NotificationManager notificationmanager = (NotificationManager) UnityPlayer.currentActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.cancel(id);
    }

    //NOTIFICATION DISABLE
    public static void DisableNotification (int isOn)
    {
        Log.d(TAG, "DisableNotification -----" + isOn);

        SharedPreferences mute = UnityPlayer.currentActivity.getSharedPreferences (DISABLE_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mute.edit();
        editor.clear();
        editor.putInt(DISABLE_NOTIFICATION, isOn);
        editor.commit();
    }

    private boolean IsNotificationDisable () {
        SharedPreferences allMuteNotification = UnityPlayer.currentActivity.getSharedPreferences(DISABLE_NOTIFICATION, Context.MODE_PRIVATE);
        int defaultValue = allMuteNotification.getInt(DISABLE_NOTIFICATION, Context.MODE_PRIVATE);
        if (defaultValue == 1)
            return true;

        return false;
    }

    //MUTE NOTIFICATION
    public static void MuteAllNotification (int isOn)
    {
        Log.d(TAG, "MuteAllNotification -----" + isOn);

        SharedPreferences mute = UnityPlayer.currentActivity.getSharedPreferences (MUTE_ALL_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mute.edit();
        editor.clear();
        editor.putInt(MUTE_ALL_NOTIFICATION, isOn);
        editor.commit();
    }

    public static void MuteNotification (String channelArray)
    {
        Log.d(TAG, "MuteAllNotification -----" + channelArray);

        SharedPreferences mute = UnityPlayer.currentActivity.getSharedPreferences (MUTE_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mute.edit();
        editor.clear();
        editor.putString(MUTE_NOTIFICATION, channelArray);
        editor.commit();
    }

    public static boolean IsAllNotificationMute () {

        SharedPreferences allMuteNotification = UnityPlayer.currentActivity.getSharedPreferences(MUTE_ALL_NOTIFICATION, Context.MODE_PRIVATE);
        int defaultValue = allMuteNotification.getInt(MUTE_ALL_NOTIFICATION, Context.MODE_PRIVATE);
        if (defaultValue == 1)
            return true;

        return false;
    }

    public static boolean IsNotificationMuted (String channelID)
    {
        SharedPreferences muuteNotification = UnityPlayer.currentActivity.getSharedPreferences(MUTE_NOTIFICATION, Context.MODE_PRIVATE);
        String defaultValue = muuteNotification.getString(MUTE_NOTIFICATION, "");

        if (defaultValue.isEmpty() || defaultValue == null) {
            Log.d(TAG, "SharedPreferences Working -----");
            return false;
        }

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                JSONArray channelArray = new JSONArray(defaultValue);

                for (int i=0; i< channelArray.length(); i++)
                {
                    JSONObject channel = channelArray.getJSONObject(i);

                    if (channelID == channel.toString())
                    {
                        return true;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("JSON ARRAY Exception->","" + e.getMessage());
            return false;
        }

        return false;
    }

    public static void GetNotificationChatChannel(String msg)
    {
//        Log.d(TAG, "OpenNotificationChat-> ############################ " + msg);

        SharedPreferences channelPreferences = UnityPlayer.currentActivity.getSharedPreferences("freak.open.Chat", 0);
        String channelUrl = channelPreferences.getString("freak.open.Chat", "");
        SharedPreferences.Editor channelEditor = channelPreferences.edit();
        channelEditor.commit();

//        Log.d(TAG, "OpenNotificationChat-> ############################ " + channelUrl);

        Boolean isFromNotification = (Boolean) UnityPlayer.currentActivity.getIntent().getBooleanExtra("FromNotification", false);

//        Log.d(TAG, "OpenNotificationChat-> ############################ *** " + isFromNotification);

        SharedPreferences notificationsPreference = UnityPlayer.currentActivity.getSharedPreferences("freak.open.FromNotification", 0);

        if (isFromNotification == false) {
            isFromNotification = notificationsPreference.getBoolean("freak.open.FromNotification", false);
        }

        SharedPreferences.Editor notificationEditor = notificationsPreference.edit();
        notificationEditor.commit();

//        Log.d(TAG, "OpenNotificationChat-> ############################ *** " + isFromNotification);

        if(!channelUrl.isEmpty() && isFromNotification) {

            if (msg.equals("FreakHomeScene")) {
                channelEditor.clear();
                channelEditor.commit();
                notificationEditor.clear();
                notificationEditor.commit();

//                Log.d(TAG, "OpenNotificationChat-> ############################ CLEAR DATA");
            }

            UnityPlayer.currentActivity.getIntent().removeExtra("FromNotification");
            UnityPlayer.UnitySendMessage("FileSharingManager", "OpenNotificationChatChannel", channelUrl);
        }
    }
}
