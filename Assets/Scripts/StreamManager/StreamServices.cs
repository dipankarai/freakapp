﻿using UnityEngine;
using System.Collections;

namespace Freak
{
    public class StreamServices : MonoBehaviour
    {
        public delegate void ServiceMethod();
        private static event ServiceMethod updateService, destroyService;

        public static void AddService (ServiceMethod update, ServiceMethod destroy)
        {
            if(update != null)
            {
                updateService -= update;
                updateService += update;
            }

            if(destroy != null)
            {
                destroyService -= destroy;
                destroyService += destroy;
            }
        }

        public static void RemoveService (ServiceMethod update, ServiceMethod destroy)
        {
            if(update != null)
            {
                updateService -= update;
            }

            if(destroy != null)
            {
                destroyService -= destroy;
            }
        }

        private static StreamServices instance;
        public static StreamServices Instance
        {
            get
            {
                return CheckService();
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public static StreamServices CheckService()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("StreamServices");
                instance = instGO.AddComponent<StreamServices>();

                DontDestroyOnLoad(instGO);
            }
            return instance;
        }
         
        /// <summary>
        /// Initializes its instance.
        /// </summary>
        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
        }

        // Use this for initialization
        void Start () {
            
        }
        
        // Update is called once per frame
        void Update () {
            // invoke update server delegates
            if (updateService != null) updateService();
        }
    }
}
