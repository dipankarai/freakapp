﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Freak
{
    enum StreamManagerQueueType
    {
        SaveFile,
        LoadFile,
        Download
    }

    class StreamManagerQueue
    {
        public StreamManagerQueueType queueType;
        public StreamSavedCallbackMethod streamSavedCallback;
        public string fileName;
        public byte[] data;
        public FolderLocation folderLocation;
        public bool isSentData;

        public StreamManagerQueue(StreamManagerQueueType type)
        {
            queueType = type;
        }
    }

    public static class StreamManager
    {
        private static List<StreamManagerQueue> queue;
        private static bool savingStream, downloading, loadingStream, checkingIfFileExists, deletingFile;

        static StreamManager()
        {
            //Debug.Log("HI....");
            StreamServices.CheckService();
            queue = new List<StreamManagerQueue>();
            StreamServices.AddService(update, null);
        }

        private static void update()
        {
//            //Debug.Log("UPDATE");
            updateQueue();
        }

        private static void updateQueue()
        {
            if (queue.Count == 0)
                return;

            foreach (var que in queue.ToArray())
            {
                switch (que.queueType)
                {
                    case StreamManagerQueueType.SaveFile:
                        {
                            if (savingStream == false)
                            {
                                SaveFile(que.fileName, que.data, que.folderLocation, que.streamSavedCallback, que.isSentData);
                                queue.Remove(que);
                            }
                        }
                        break;

                    case StreamManagerQueueType.LoadFile:
                        {
                            
                        }
                        break;

                    default:
                        //Debug.LogError("Unsuported StreamManagerQueTypes: " + que);
                        break;
                }
            }
        }

        /// <summary>
        /// Create a new string file path.
        /// </summary>
        /// <returns>The file path.</returns>
        /// <param name="fileName">New File name with extension.</param>
        /// <param name="folderLocation">Folder location or type of file.</param>
        /// <param name="isSentData">If set to <c>true</c> is sent data by sharing.</param>
        /// <param name="isSentData">If set to <c>false</c> is not share data.</param>
        public static string LoadFilePath(string fileName, FolderLocation folderLocation, bool isSentData)
        {
            return getCorrectFreakAppPath(fileName, folderLocation, isSentData);
        }

        /// <summary>
        /// Saves the file into local.
        /// </summary>
        /// <param name="fileName">File name with extension.</param>
        /// <param name="data">Byte Array data.</param>
        /// <param name="folderLocation">Folder location or type of file.</param>
        /// <param name="streamSavedCallback">Callback after saving file.</param>
        /// <param name="isSentData">If set to <c>true</c> is sent data by sharing.</param>
        /// <param name="isSentData">If set to <c>false</c> is not share data.</param>
        public static void SaveFile(string fileName, byte[] data, FolderLocation folderLocation, StreamSavedCallbackMethod streamSavedCallback, bool isSentData = false)
        {
            if(savingStream)
            {
                var que = new StreamManagerQueue(StreamManagerQueueType.SaveFile);
                que.streamSavedCallback = streamSavedCallback;
                que.fileName = fileName;
                que.data = data;
                que.folderLocation = folderLocation;
                que.isSentData = isSentData;
                queue.Add(que);
                return;
            }

            savingStream = true;
            string filename = getCorrectFreakAppPath(fileName, folderLocation, isSentData);
            try
            {
                using (var file = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    file.Write(data, 0, data.Length);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                savingStream = false;

                if (streamSavedCallback != null)
                    streamSavedCallback(false);

                return;
            }

            if (streamSavedCallback != null)
                streamSavedCallback(true);
            
            savingStream = false;
        }

        public static string getCorrectFreakAppPath(string fileName, FolderLocation folderLocation, bool isSentData)
        {
            string folderpath = FreakAppManager.ApplicationPath;

            if (isSentData)
                folderpath += "/" + folderLocation.ToString() + "/Sent";
            else
                folderpath += "/" + folderLocation.ToString();

            if(Directory.Exists(folderpath) == false)
                Directory.CreateDirectory(folderpath);

            return ConvertToPlatformSlash(folderpath + "/" + fileName);
        }

        /// <summary>
        /// Convert path slash char values to your current platform.
        /// </summary>
        /// <param name="path">Src path.</param>
        /// <returns>Dst path.</returns>
        public static string ConvertToPlatformSlash(string path)
        {
            #if UNITY_WINRT || UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            return path.Replace('/', '\\');
            #else
            return path.Replace('\\', '/');
            #endif
        }

        /// <summary>
        /// Loads the file in byte array by giving file path.
        /// </summary>
        /// <returns>The file direct.</returns>
        /// <param name="filepath">Filepath.</param>
        public static byte[] LoadFileDirect (string filepath)
        {
            byte[] data = null;
            try
            {
                using (var file = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    data = new byte[file.Length];
                    file.Read(data, 0, data.Length);
                }
            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
            }

            return data;
        }

        public static void RenameFile (string sourceFilePath, string destinationFileName, FolderLocation folderLocation)
        {
            try
            {
//                string newFile = FreakAppManager.ApplicationPath + "/" + destinationFileName;
                string newFile = getCorrectFreakAppPath(destinationFileName, folderLocation, false);
                Debug.Log(newFile);
                if (File.Exists(sourceFilePath))
                {

                    if (File.Exists(newFile))
                    {
                        File.Delete(newFile);
                    }

                    File.Move(sourceFilePath, newFile);
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError (e.Message);
            }
        }

        public static void CopyFile (string sourceFilePath, string destinationFilePath)
        {
            try
            {
                if (File.Exists(sourceFilePath))
                {

                    File.Copy(sourceFilePath, destinationFilePath, true);
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError (e.Message);
            }
        }
    }

    public enum FolderLocation
    {
        Audio,
        Contact,
        Image,
        Video,
        Others,
        GroupProfile,
		Profile,
		Wallpaper,
		NewsFeed,
        Games,
        ProfileHD,
        GroupProfileHD
    }

    public delegate void StreamSavedCallbackMethod (bool suddeeded);
    public delegate void StreamSavedTextureCallbackMethod (Texture2D trexture);
}
