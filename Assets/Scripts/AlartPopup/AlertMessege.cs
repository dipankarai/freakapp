﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


namespace Freak
{
	public class AlertMessege : MonoBehaviour 
	{

		public enum AlertType
		{
			MessegeWithOkButton = 0,
			MessegeWithYesNoButton = 1
		}

		public GameObject messegePanel;

		public GameObject typeOneButton;
		public GameObject typeTwoButton;

		public Text messegeText;

		public Button okButton;
		public Button yesButton;
		public Button noButton;

		public static AlertMessege Instance;
		public HomeScreenUIPanelController homeScreenUIPanelController;
		public UIPanelController uIPanelController;
		public UserProfileUIController userProfileUIController;

        public Action onClickOkButtonCallback;

		void Awake()
		{
			Instance = this;
		}

		void OnEnables()
		{
			okButton.onClick.AddListener (OnOkButtonClicked);
			yesButton.onClick.AddListener (OnYesButtonClicked);
			noButton.onClick.AddListener (OnNoButtonClicked);
		}

		void OnDisables()
		{
			okButton.onClick.RemoveAllListeners ();
			yesButton.onClick.RemoveAllListeners ();
			noButton.onClick.RemoveAllListeners ();
		}

		public void ShowAlertWindow(AlertType type, string messege)
		{
			typeTwoButton.SetActive (false);
			typeOneButton.SetActive (false);

			OnEnables ();

			if (AlertType.MessegeWithYesNoButton == type) 
			{
				typeTwoButton.SetActive (true);
			}
			else 
			{
				typeOneButton.SetActive (true);
			}
			messegeText.text = messege;

			if (homeScreenUIPanelController != null)
			{
				homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.PopUpPanel);
			} 
			else if (uIPanelController != null) 
			{
				uIPanelController.PushPanel (UIPanelController.PanelType.PopUpPanel);
			}
			else if (userProfileUIController != null) 
			{
				userProfileUIController.PushPanel (UserProfileUIController.PanelType.popUpPanel);
			}
			else 
			{
				messegePanel.SetActive (true);
			}
		}

		/// <summary>
		/// Raises the ok button clicked event.
		/// </summary>
		public void OnOkButtonClicked()
		{
			messegePanel.SetActive (false);

            if (onClickOkButtonCallback != null)
            {
                onClickOkButtonCallback ();
                onClickOkButtonCallback = null;
            }
		}

		/// <summary>
		/// Raises the yes button clicked event.
		/// </summary>
		public void OnYesButtonClicked()
		{
			messegePanel.SetActive (false);
			OnDisables ();
		}

		/// <summary>
		/// Raises the no button clicked event.
		/// </summary>
		public void OnNoButtonClicked()
		{
			messegePanel.SetActive (false);
			OnDisables ();
		}
	}
}
