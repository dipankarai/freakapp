﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;


public class AllContactsController : MonoBehaviour {

	public List<ServerUserContact> serverContactList = new List<ServerUserContact>();

	public AllContactsDisplayInRow allContactsDisplayInRow;
	public AllContactsDisplayInPanel allContactsDisplayInPanel;

	public GameObject searchBar;
	public InputField searchBarTextField;
	public GameObject searchBtn;
	public GameObject RowColumnBtn;
	public List<Contact> tempContacts = new List<Contact>();

	public Image rowColumImage;
	public Sprite rowSprite;
	public Sprite columnSprite;

	public List<Contact> dummyContactsList;
	private string failString;
	public ConnectingUI loadingObj;
    public Text noContactsText;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;


	enum Status{
		Grid,
		Panel
	}
	Status status;
	Vector3 bottomPanelPosition;

	void OnEnable()
	{
		Invoke ("InitialCall", 0.3f);
		//InitialCall();
	}

	void InitialCall()
	{
		loadingObj.DisplayLoading ();
        if (FreakAppManager.Instance.GetAllContacts().Count > 0)
		{
			OnInit ();
		}
		else
		{
            noContactsText.text = "No Contacts.";
			SyncContacts ();
		}

		#if UNITY_EDITOR
		OnInit ();
		#endif
		bottomPanelPosition = searchBar.transform.rectTransform ().localPosition;
	}

	void OnInit()
    {
//        FreakAppManager.Instance.LoadServerContactList (EnableTableView);
        tempContacts = new List<Contact>(FreakAppManager.Instance.GetAllContacts());

        if (tempContacts.Count > 0)
        {
            noContactsText.text = string.Empty;
        }

        #if UNITY_EDITOR
        foreach(Contact detailsObj in dummyContactsList)
        {
            tempContacts.Add( detailsObj );
            Debug.Log(tempContacts.Count);
        }
        Debug.Log("Contacts Added");
        #endif

        allContactsDisplayInPanel.enabled = true;
        allContactsDisplayInRow.enabled = false;
        searchBtn.gameObject.SetActive (false);
        SetSprite (rowSprite);
        status = Status.Panel;

        loadingObj.gameObject.SetActive (false);
	}

	void EnableTableView(List<ServerUserContact> data)
	{
		loadingObj.gameObject.SetActive (false);
		HideSearchBarOnClose ();
		serverContactList.Clear ();
		for (int i = 0; i < data.Count; i++)
		{
			serverContactList.Add (data [i]);
		}

		tempContacts.Clear ();
//		FreakAppManager.Instance.phoneContacts.Clear ();

        List<Contact> _deviceContacts = new List<Contact>();
        _deviceContacts = FreakPlayerPrefs.GetAllContactfromLocal;


        for (int i = 0; i < _deviceContacts.Count; i++)
        {
            for (int j = 0; j < serverContactList.Count; j++) 
            {
                //Debug.LogWarning ("Phone Contact and Sync Contact --- > " + Contacts.ContactsList [i].Phones [0].Number + "Sync Contacts -- >" + serverContactList [j].contactNumb);
                if (_deviceContacts[i].Phones [0].Number.Equals (serverContactList [j].contact_number))
                {
                    //Debug.LogError ("************Contacts Matching ************" + Contacts.ContactsList[i].Phones [0].Number);
                    _deviceContacts [i].isFreak = true;
                    _deviceContacts [i].freakUserId = serverContactList [j].user_id.ToString();
//                    Debug.Log("Contact bytes Image " + Contacts.ContactsList[i].textureByteArray);
                }
            }

            tempContacts.Add (_deviceContacts [i]);
        }

        FreakPlayerPrefs.GetAllContactfromLocal = _deviceContacts;

        /*for (int i = 0; i < Contacts.ContactsList.Count; i++)
		{
			for (int j = 0; j < serverContactList.Count; j++) 
			{
				//Debug.LogWarning ("Phone Contact and Sync Contact --- > " + Contacts.ContactsList [i].Phones [0].Number + "Sync Contacts -- >" + serverContactList [j].contactNumb);
				if (Contacts.ContactsList[i].Phones [0].Number.Equals (serverContactList [j].contact_number)) 
				{
					//Debug.LogError ("************Contacts Matching ************" + Contacts.ContactsList[i].Phones [0].Number);
					Contacts.ContactsList [i].isFreak = true;
					Contacts.ContactsList [i].freakUserId = serverContactList [j].user_id.ToString();
				}
			}

			tempContacts.Add (Contacts.ContactsList [i]);
			FreakAppManager.Instance.phoneContacts.Add (Contacts.ContactsList [i]);
		}*/

		#if UNITY_EDITOR
		foreach(Contact detailsObj in dummyContactsList)
		{
			tempContacts.Add( detailsObj );
			Debug.Log(tempContacts.Count);
		}
		Debug.Log("Contacts Added");
		#endif

		allContactsDisplayInPanel.enabled = true;
		allContactsDisplayInRow.enabled = false;
		searchBtn.gameObject.SetActive (false);
		SetSprite (rowSprite);
		status = Status.Panel;
	}

	void CheckForCachedContacts()
	{
		
	}

	public void OnClickOptions()
	{
		if (status == Status.Panel) 
		{
			Debug.Log ("Display Row");
			HideSearchBar ();
			allContactsDisplayInRow.enabled = true;
			allContactsDisplayInPanel.enabled = false;
			SetSprite (columnSprite);
			searchBtn.gameObject.SetActive (true);
			status = Status.Grid;
			return;
		} 
		else
		{
			Debug.Log ("Display Column");
			allContactsDisplayInPanel.enabled = true;
			allContactsDisplayInRow.enabled = false;
			searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
			searchBar.SetActive (false);
			searchBtn.gameObject.SetActive (false);
			RowColumnBtn.gameObject.SetActive (true);
			SetSprite (rowSprite);
			status = Status.Panel;
		}
	}

	public void HideSearchBar()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

		allContactsDisplayInRow.OnClearTexts ();
	}


	public void HideSearchBarOnClose()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

	}


	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;

        searchBarTextField.text=(string.Empty);
	}

	public void Hide()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (false);
	}

	public void OnClickShowContacts()
	{
		//FindObjectOfType<FreaksPanelUIController>().PushPanel (FreaksPanelUIController.PanelType.AllContactsPanel);
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.DisplayContactsPanel);
	}


	void OnDisable(){
		Destroy (this.gameObject);
		Resources.UnloadUnusedAssets ();
	}

	public void OnClickBack()
	{
		//		if (FindObjectOfType<HomeScreenUIPanelController> () != null) 
		//		{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
		//		} 
		//		else 
		//		{
		//			FindObjectOfType<FreaksPanelUIController> ().PopPanel ();
		//		}
	}

	void SetSprite(Sprite spr)
	{
		rowColumImage.sprite = spr;
	}


	public void SyncContacts()
	{
		Contacts.LoadContactList (onDone, onLoadFailed);
	}


	void CallSsync()
	{
		for (int i = 0; i < Contacts.ContactsList.Count; i++)
		{
			for (int k = 0; k < Contacts.ContactsList [i].Phones.Count; k++)
			{
				string number = Contacts.ContactsList [i].Phones [k].Number.ToString ();
                number = number.Replace (" ", string.Empty);
                number = number.Replace ("-", string.Empty);
                number = number.Replace ("+", string.Empty);
				bool digitsOnly = number.All(char.IsDigit);
				if (digitsOnly) 
				{
					if (number.Length > 10) 
					{
						number = number.Substring (number.Length - 10);
					}
					if (!number.Equals (FreakAppManager.Instance.mobileNumber)) 
					{
						Contacts.ContactsList [i].Phones [0].Number = number;
					}
				} 
				else {
					Debug .LogWarning("NUMBER DATA NOT SYNC-----"+  Contacts.ContactsList [i].Phones [k].Number.ToString ());
				}
			}
		}

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;
        FreakAppManager.Instance.allContacts = new List<Contact>(Contacts.ContactsList);

        OnInit ();
	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
		CallSsync ();
	}

}
