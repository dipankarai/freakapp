﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Chat;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;


public class AllContactsDisplayInPanel : MonoBehaviour, IEnhancedScrollerDelegate  {


	int currentCount;
//	public ContactDisplayUIComponent test_m_cellPrefab;
	public FreaksContactDisplayUIComponent m_cellPrefab;
	public EnhancedScroller m_EnhancedScroller;
//	public TableView m_tableView;
	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;
	private List<Contact> tempContacts = new List<Contact>();
	public AllContactsController allContactsController;
	public Sprite defaultUserImgSprite;

	void OnEnable()
	{
		Debug.Log ("DisplayContactsInPanel ********** ");
		tempContacts.Clear ();
//		for (int i = 0; i < allContactsController.tempContacts.Count; i++) 
//		{
//			tempContacts.Add (allContactsController.tempContacts [i]);
//		}

		tempContacts = new List<Contact> (allContactsController.tempContacts);

		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;
//		m_tableView.ReloadData ();
//		m_tableView.scrollY = 0;
		m_EnhancedScroller.Delegate = this;
		m_EnhancedScroller.ClearAll ();
	}


//	//Register as the TableView's delegate (required) and data source (optional)
//	//to receive the calls
//	#region ITableViewDataSource
//
//	//Will be called by the TableView to know how many rows are in this table
//	public int GetNumberOfRowsForTableView(TableView tableView)
//	{
//		if (tempContacts.Count > 0) {
//
//			float val = (float)(tempContacts.Count / 3f);
//			int d = (int) Mathf.Ceil(val);
//			Debug.Log ("serverContactList Count --- >" + tempContacts.Count + " current count --- > "+ d);
//			return d;
//		} else {
//			return 0;
//		}
//	}
//
//
//	//Will be called by the TableView to know what is the height of each row
//	public float GetHeightForRowInTableView(TableView tableView, int row) {
//		return GetHeightOfRow(row);
//	}
//
//
//	//Will be called by the TableView when a cell needs to be created for display
//	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
//	{
//		ContactDisplayUIComponent cell = tableView.GetReusableCell(test_m_cellPrefab.reuseIdentifier) as ContactDisplayUIComponent;
//		if (cell == null) {
//			cell = (ContactDisplayUIComponent)GameObject.Instantiate(test_m_cellPrefab);
//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//		}
//
//
//		cell.rowNumber = row;
//		AssignDetailsToGrids (cell, row);
//		cell.height = GetHeightOfRow(row);
//		return cell;
//	}


	#region EnhancedScroller Handlers

	/// <summary>
	/// This tells the scroller the number of cells that should have room allocated. This should be the length of your data array.
	/// </summary>
	/// <param name="scroller">The scroller that is requesting the data size</param>
	/// <returns>The number of cells</returns>
	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		if (tempContacts.Count > 0) {

			float val = (float)(tempContacts.Count / 3f);
			int d = (int) Mathf.Ceil(val);
			//Debug.Log ("serverContactList Count --- >" + tempContacts.Count + " current count --- > "+ d);
			return d;
		} else {
			return 0;
		}
	}

	/// <summary>
	/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
	/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
	/// cell size will be the width.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell size</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <returns>The size of the cell</returns>
	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
		//return (dataIndex % 2 == 0 ? 30f : 100f);
		return 230;
	}

	/// <summary>
	/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
	/// Some examples of this would be headers, footers, and other grouping cells.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
	/// <returns>The cell for the scroller to use</returns>
	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		// first, we get a cell from the scroller by passing a prefab.
		// if the scroller finds one it can recycle it will do so, otherwise
		// it will create a new cell.
		FreaksContactDisplayUIComponent cellView = scroller.GetCellView(m_cellPrefab) as FreaksContactDisplayUIComponent;

		// set the name of the game object to the cell's data index.
		// this is optional, but it helps up debug the objects in 
		// the scene hierarchy.
		//			cellView.name = "Cell " + dataIndex.ToString();

		// in this example, we just pass the data to our cell's view which will update its UI
		//cellView.InitFreakMessagingChannelNew(mFreakChachedChannelList[dataIndex]);

		AssignDetailsToGrids (cellView, dataIndex);

		// return the cell to the scroller
		return cellView;
	}

	#endregion


	Texture2D tex;
	Rect rec;
	Sprite spr;

	void AssignDetailsToGrids(FreaksContactDisplayUIComponent cell, int rowNumb)
	{
		int firstColoumnIndex = rowNumb * 3;
		for (int coloumn = 0; coloumn < 3; coloumn++)
		{
			if (firstColoumnIndex + coloumn < tempContacts.Count)
			{
				Contact cont = tempContacts [firstColoumnIndex + coloumn];
				FreakContactsCellView contactsdetailsUiGrid = cell.contactDetailsList [coloumn];
                contactsdetailsUiGrid.iD = string.Empty;
				//cell.contactDetailsList [coloumn];
				contactsdetailsUiGrid.gameObject.SetActive (true);
				if (cont.isFreak) {
					contactsdetailsUiGrid.inviteButton.gameObject.SetActive (false);
					contactsdetailsUiGrid.addButton.gameObject.SetActive (true);
					contactsdetailsUiGrid.iD = cont.freakUserId;
				} 
				else {
					contactsdetailsUiGrid.inviteButton.gameObject.SetActive (true);
					contactsdetailsUiGrid.addButton.gameObject.SetActive (false);
				}
				if (!string.IsNullOrEmpty (cont.Name)) 
				{
					contactsdetailsUiGrid.nameText.text = cont.Name;
				} 
				else 
				{
					contactsdetailsUiGrid.nameText.text = cont.Phones[0].Number;
				}

				contactsdetailsUiGrid.numberText.text = cont.Phones[0].Number;
				contactsdetailsUiGrid.phoneNumb = cont.Phones [0].Number;
				//contactsdetailsUiGrid.IntUser (cont.user_id.ToString(), cont.userName);
                if (cont.GetPhotoTexture != null) 
				{
					//GUILayout.Box( new GUIContent(c.PhotoTexture) , GUILayout.Width(size.y), GUILayout.Height(size.y));
                    /*tex = cont.GetPhotoTexture;
					rec = new Rect (0, 0, tex.width, tex.height);
					spr = Sprite.Create (tex, rec, new Vector2 (0, 0), 1);
					contactsdetailsUiGrid.img.sprite = spr;*/

                    contactsdetailsUiGrid.img.FreakSetTexture(cont.GetPhotoTexture);
				}
				else {
					contactsdetailsUiGrid.img.sprite = defaultUserImgSprite;
				}

			} 
			else {
				break;
			}

		}

	}
		
//	private float GetHeightOfRow(int row) 
//	{
//		if (m_customRowHeights.ContainsKey(row)) 
//		{
//			return m_customRowHeights[row];
//		} else {
//			return test_m_cellPrefab.height;
//		}
//	}
//
//	private void OnCellHeightChanged(int row, float newHeight) 
//	{
//		if (GetHeightOfRow(row) == newHeight) {
//			return;
//		}
//		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
//		m_customRowHeights[row] = newHeight;
//		m_tableView.NotifyCellDimensionsChanged(row);
//	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}


	public void OnContactSendBackButtonClick()
	{
		gameObject.SetActive (false);
	}

}






