﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public static class FreakPlayerPrefs
{
    const string SERVER_USER_CONTACT                = "serverusercontact";
    const string DEVICE_ALL_CONTACT                 = "deviceallcontacts";
    const string USER_INFO                          = "userInfo";
    const string ALL_NOTIFICATION                   = "allnotification";
    const string MUTE_ALL_CHAT                      = "muteallchat";
    const string DEVICE_TOKEN                       = "devicetokenkey";
    const string FIRST_OPENED                       = "devicefirstopened";
    const string GAME_HISTORY_STATUS_LIST           = "GetGameHistoryStatusList";
    const string GAME_INVENTORY_LIST                = "GetGameInventoryList";

    public static bool HasKey (string key)
    {
        return PlayerPrefs.HasKey (key);
    }

    public static void DeleteKey(string key)
    {
        PlayerPrefs.DeleteKey(key);
    }

    public static string DeviceToken
    {
        get
        {
            if (HasKey(DEVICE_TOKEN) == false)
            {
                return string.Empty;
            }

            return PlayerPrefs.GetString (DEVICE_TOKEN);
        }
        set
        {
            PlayerPrefs.SetString (DEVICE_TOKEN, value);
        }
    }

    public static bool AllNotification
    {
        get
        {
            if (HasKey(ALL_NOTIFICATION))
            {
                return (PlayerPrefs.GetInt (ALL_NOTIFICATION) == 1) ? true : false;
            }
            else
            {
                PlayerPrefs.SetInt (ALL_NOTIFICATION, 1);
                return true;
            }
        }
        set
        {
            int boolValue = (value == true) ? 1 : 0;
            PlayerPrefs.SetInt (ALL_NOTIFICATION, boolValue);
        }
    }

    public static bool MuteAllChat
    {
        get
        {
            if (HasKey(MUTE_ALL_CHAT))
            {
                return (PlayerPrefs.GetInt(MUTE_ALL_CHAT) == 1) ? true : false;
            }
            else
            {
                PlayerPrefs.SetInt (MUTE_ALL_CHAT, 0);
                return false;
            }
        }
        set
        {
            int boolValue = (value == true) ? 1 : 0;
            PlayerPrefs.SetInt(MUTE_ALL_CHAT, boolValue);
        }
    }

    public static void ClearServerUserContact ()
    {
        PlayerPrefs.DeleteKey (SERVER_USER_CONTACT);
    }

    public static List<ServerUserContact> GetSetServerUserContact
    {
        get
        {
            List<ServerUserContact> serverContactList = new List<ServerUserContact> ();

            if (HasKey(SERVER_USER_CONTACT))
            {
                string data = PlayerPrefs.GetString (SERVER_USER_CONTACT);

                if (data.Contains("null"))
                {
                    data = data.Replace ("null", "''");
                }

                #if UNITY_EDITOR && UNITY_DEBUG
//                Debug.Log ("GetSetServerUserContact");
                #endif
                //                IList list = (IList)MiniJSON.Json.Deserialize (data);
                serverContactList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServerUserContact>>(data);

                /*if (serverContactList.Count > 0)
                    return serverContactList;
                else
                    return null;*/
            }
            /*else
            {
                return null;
            }*/

            return serverContactList;
        }
        set
        {
            string data = Newtonsoft.Json.JsonConvert.SerializeObject (value);
            //            Debug.Log("GetSetServerUserContact " + data);
            PlayerPrefs.SetString (SERVER_USER_CONTACT, data);
        }
    }

    public static List<Contact> GetAllContactfromLocal
    {
        get
        {
            List<Contact> _contactList = new List<Contact> ();
            if (HasKey(DEVICE_ALL_CONTACT))
            {
                //              FreakAppManager.Instance.phoneContacts = new List<Contact> ();
                //              FreakAppManager.Instance.phoneContacts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Contact>> (PlayerPrefs.GetString (DEVICE_ALL_CONTACT));
                _contactList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Contact>> (PlayerPrefs.GetString (DEVICE_ALL_CONTACT));
            }
            else
            {
                //              FreakAppManager.Instance.phoneContacts = new List<Contact> ();
                //              string data = Newtonsoft.Json.JsonConvert.SerializeObject (FreakAppManager.Instance.phoneContacts);
                //              Debug.Log ("GetAllContactfromLocal " + data);
                //              PlayerPrefs.SetString (DEVICE_ALL_CONTACT, data);
            }

            //            return FreakAppManager.Instance.phoneContacts;
            return _contactList;
        }
        set
        {
            string data = Newtonsoft.Json.JsonConvert.SerializeObject (value);
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetAllContactfromLocal " + data);
            #endif
            PlayerPrefs.SetString (DEVICE_ALL_CONTACT, data);
        }
        //phoneContacts
    }

    public static MyUserProfile GetSavedUserProfile
    {
        get
        {
            if (HasKey(USER_INFO))
            {
                string data = PlayerPrefs.GetString (USER_INFO);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Get UserInfo " + data);
                #endif
                IDictionary userProfile = (IDictionary)Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary> (data);
                MyUserProfile newuserProfile = new MyUserProfile (userProfile);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Get UserInfo---- " + Newtonsoft.Json.JsonConvert.SerializeObject(userProfile));
                #endif
                if (newuserProfile != null)
                    return newuserProfile;
                else
                    return null;
            }
            else
            {
                return null;
            }
        }
        set
        {
            string data = Newtonsoft.Json.JsonConvert.SerializeObject (value);
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("UserInfo " + data);
            #endif
            PlayerPrefs.SetString (USER_INFO, data);
        }
    }

    public static long AppInstalledTime
    {
        get {
            if (HasKey (FIRST_OPENED))
            {
                return long.Parse (PlayerPrefs.GetString (FIRST_OPENED));
            }

            return 0;
        }

        set {
            if (HasKey (FIRST_OPENED) == false)
            {
                PlayerPrefs.SetString (FIRST_OPENED, value.ToString ());
            }
        }
    }


    public static List<GameStatus> GetGameHistoryStatusList
    {
        get
        {
            List<GameStatus> _historyList = new List<GameStatus>();
            if (HasKey(GAME_HISTORY_STATUS_LIST))
            {
                _historyList = (List<GameStatus>)JsonConvert.DeserializeObject<List<GameStatus>>(PlayerPrefs.GetString(GAME_HISTORY_STATUS_LIST));
            }

            return _historyList;
        }

        set
        {
            string data = JsonConvert.SerializeObject (value);
            PlayerPrefs.SetString(GAME_HISTORY_STATUS_LIST, data);
        }
    }

    public static List<StoreGames> GetGameInventoryList
    {
        get
        {
            List<StoreGames> _inventoryList = new List<StoreGames>();
            if (HasKey(GAME_INVENTORY_LIST))
            {
                _inventoryList = (List<StoreGames>)JsonConvert.DeserializeObject<List<StoreGames>>(PlayerPrefs.GetString(GAME_INVENTORY_LIST));
            }

            return _inventoryList;
        }
        set
        {
            string data = JsonConvert.SerializeObject (value);
            PlayerPrefs.SetString(GAME_INVENTORY_LIST, data);
        }
    }
}
