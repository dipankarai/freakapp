﻿using UnityEngine;
using System.Collections;
using ImageAndVideoPicker;
using System.Runtime.InteropServices;
using System.Collections.Generic;

public class IOSFileController : MonoBehaviour 
{

	#if  UNITY_IPHONE || UNITY_IOS

	[DllImport ("__Internal")] private static extern void pickiOSFile();

//	[DllImport ("__Internal")] private static extern void saveContactInList();

//	[DllImport ("__Internal")] private static extern void saveContactInList(ref ConfigStruct conf);

	public struct ConfigStruct
	{
		public string fname;
		public string phoneNumber;
	}
	/// <summary>
	/// Files the pick IO.
	/// </summary>
	public static void FilePickIOS()
	{
		pickiOSFile ();
	}

//	public static void SaveContactInList()
//	{
//		saveContactInList ();
//
//
//	}

	public static void ContactSave(string fname, string phoneNum)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.fname  = fname;
		conf.phoneNumber = phoneNum;
//		saveContactInList(ref conf);
	}

	#endif
	void OnEnable()
	{
		PickerEventListener.onImageSelect += OnImageSelect;
		PickerEventListener.onImageLoad += OnImageLoad;
		PickerEventListener.onVideoSelect += OnVideoSelect;
		PickerEventListener.onError += OnError;
		PickerEventListener.onCancel += OnCancel;
		PickerEventListener.onContactSelect += OnContactSelect;
	}

	void OnDisable()
	{
		PickerEventListener.onImageSelect -= OnImageSelect;
		PickerEventListener.onImageLoad -= OnImageLoad;
		PickerEventListener.onVideoSelect -= OnVideoSelect;
		PickerEventListener.onError -= OnError;
		PickerEventListener.onCancel -= OnCancel;
		PickerEventListener.onContactSelect -= OnContactSelect;
	}

	void OnContactSelect(string name, List<string> contactList, List<string> other)
	{
		//Debug.Log ("Contact pick Location : "+ name);
//		Debug.Log ("Contact list"+ contactList);
		//for (int i = 0; i < Contacts.ContactsList.Count; i++) {
		

			//Debug.Log ("Contact pick name -------->>>>>: "+Contacts.ContactsList [i].Name  +"<<------Is Concatct----->>>"+Contacts.ContactsList [i].Name.Equals(name));
//			if (Contacts.ContactsList [i].Name.Equals(name)) {
//				
//				Debug.Log ("Contact pick name : "+Contacts.ContactsList [i].Name);
//				Debug.Log ("Contact pick number : "+Contacts.ContactsList [i].Phones[0].Number);
//
//			}
		//}
	
	}

	void OnImageSelect(string imgPath, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
		Debug.Log ("Image Location : "+imgPath);
	}


	void OnImageLoad(string imgPath, Texture2D tex, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
		Debug.Log ("Image Location : "+imgPath);
		Freak.Chat.UIController.Instance.FilePickerCallback(imgPath);
	}

	void OnVideoSelect(string vidPath)
	{
		Debug.Log ("Video Location : "+vidPath);
		Freak.Chat.UIController.Instance.FilePickerCallback(vidPath);
		//log += "\nVideo Path : " + vidPath;
		//Handheld.PlayFullScreenMovie ("file://" + vidPath, Color.blue, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
	}

	void OnError(string errorMsg)
	{
		Debug.Log ("Error : "+errorMsg);
		//log += "\nError :" +errorMsg;
	}

	void OnCancel()
	{
		Debug.Log ("Cancel by user");
		//log += "\nCancel by user";
	}

}
