﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//holder of person details
[System.Serializable]
public class MyUserProfile{
	
	public string Id;
	public string name;
	public string secondary_name;
	public string avatar_url;
	public string cover_image;
	public string avatar_name;
	public string gender;
	public string status_message;
	public int coin;
    public int totalScore;

    [Newtonsoft.Json.JsonIgnore]
	public Texture2D userPic;
    [Newtonsoft.Json.JsonIgnore]
	public Texture2D userCoverPic;
	//public byte[] userPicByte;
	public string wallpaper_image;
	public string memberId;
	public string feedId;

	public string gameReferralCode;
	public List<Contact> userContacts;
	public CurrentTitle currentTile;
	public List<UserTitles> userTitles = new List<UserTitles> ();

	public MyUserProfile(IDictionary userDetailsDict)
	{

		name = DecodeString( (string)userDetailsDict ["name"]);
		secondary_name = DecodeString( (string)userDetailsDict ["secondary_name"]);
		status_message = DecodeString((string)userDetailsDict ["status_message"]);

		avatar_url = (string)userDetailsDict ["avatar_url"];
		avatar_name = (string)userDetailsDict ["avatar_name"];
		gender = (string)userDetailsDict ["gender"];
		coin = int.Parse(userDetailsDict ["coin"].ToString());
		cover_image = (string)userDetailsDict ["cover_image"];
		wallpaper_image = (string)userDetailsDict ["wallpaper_image"];

        if (userDetailsDict.Contains("total_score"))
            totalScore = int.Parse(userDetailsDict["total_score"].ToString());
		
        List<object> score_title = (List<object>) userDetailsDict ["score_title"];
		if (userDetailsDict ["current_score_title"] != null && userDetailsDict ["current_score_title"] != "") {
			Dictionary<string,object> currentTitleData = (Dictionary<string,object>)userDetailsDict ["current_score_title"];
			currentTile = new CurrentTitle (currentTitleData);
		}
		if (score_title != null) {
			for (int i = 0; i < score_title.Count; i++) {
				Dictionary<string,object> data = (Dictionary<string,object>)score_title [i];
				UserTitles titles = new UserTitles (data);
				userTitles.Add (titles);
			}
		}
		
	}
		
	string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}

	public MyUserProfile()
	{


	}


	void PopuplateUserContacts(List<object> data)
	{
		for (int i = 0; i < data.Count; i++) {
			Contact freakContacts = new Contact ();

			userContacts.Add (freakContacts);
		}
	}


	void log( string message )
	{
		//Debug.Log( message );
	}

}

public class ServerContactsSelectedForGroups
{
	public string userId;
	public string userName;
	public string phoneNumber;

	public ServerContactsSelectedForGroups(string userID, string userNAME, string phoneNUMB)
	{
		userId = userID;
		userName = userNAME;
		phoneNumber = phoneNUMB;
	}
}

public class UserTitles
{
	public string userTitle;
	public string userTitleID;
	public UserTitles(Dictionary<string,object> titles)
	{
		if (titles != null && titles.Count > 0)
		{
			userTitle = titles ["title"].ToString ();
			userTitleID = titles ["score_title_id"].ToString ();
		}
	}
}

public class CurrentTitle
{
	public string userTitle;
	public string userTitleID;
	public CurrentTitle(Dictionary<string,object> titles)
	{
		userTitle = titles ["title"].ToString ();
		userTitleID = titles ["score_title_id"].ToString ();

	}
}
	
