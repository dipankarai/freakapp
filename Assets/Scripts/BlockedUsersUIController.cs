﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Chat;
using SendBird;

public class BlockedUsersUIController : MonoBehaviour, ITableViewDataSource  {

	public ContactDetailsDisplay m_cellPrefab;
	public TableView m_tableView;
	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;
	private ImageController imageController;
	private List<User> blockedUsersList = new List<User>();
	public GameObject deafultText;

	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
	}


	void OnEnable()
	{
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
		Init ();
	}

	public void Init()
	{
		deafultText.SetActive (true);
		ChatManager.Instance.GetBlockedUserList (EnableTableView);
	}


	public void EnableTableView(List<User> usersList)
	{
		blockedUsersList.Clear ();
		for (int i = 0; i < usersList.Count; i++) 
		{
			blockedUsersList.Add (usersList[i]);
		}

		if (usersList.Count > 0)
		{
			deafultText.SetActive (false);
		}
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}


	public void OnClickBack(){
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return blockedUsersList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
		if (cell == null) {
			cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}


	void AssignDetails(ContactDetailsDisplay cell, int rowNumb)
	{
		User blockedUser = blockedUsersList [rowNumb];
		cell.nameText.text = blockedUser.Nickname;
		cell.iD = blockedUser.UserId;
		imageController.GetImageFromLocalOrDownloadImage (blockedUser.ProfileUrl, cell.img, Freak.FolderLocation.Profile);
	}


	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
	

