﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;


public class SelectPlayerPanelController : MonoBehaviour, ITableViewDataSource {


	public GameObject searchBar;
	public InputField searchBarTextField;
	public Button searchBtn;
	public GameObject rowColumnBtn;
	public string failString;
	int currentCount;

	public ContactDetailsDisplay m_cellPrefab;
	private List<string> contactNumbers = new List<string> ();

	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	public List<ServerUserContact> serverContactList = new List<ServerUserContact> ();
	public List<ServerUserContact> tempServerContactList = new List<ServerUserContact> ();


	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;


	enum Status{
		Grid,
		Panel
	}
	Status status;

	void Start(){
		serverContactList = new List<ServerUserContact> ();
	}


	void OnEnable()
	{
		HideSearchBar ();
        FreakAppManager.Instance.LoadServerContactList (EnableTableView);
	}

	void EnableTableView(List<ServerUserContact> data)
	{
		HideSearchBar ();
		serverContactList.Clear ();
		tempServerContactList.Clear ();
		for (int i = 0; i < data.Count; i++)
		{
			serverContactList.Add (data [i]);
			tempServerContactList.Add(data [i]);
		}

		status = Status.Panel;

		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}

	public void OnClickOptions()
	{
		if (status == Status.Panel) {
			HideSearchBar ();
			status = Status.Grid;
			return;
		} else
		{
			Hide ();
			status = Status.Panel;
		}
	}

	public void HideSearchBar()
	{
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		rowColumnBtn.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;
	}

	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		rowColumnBtn.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;
	}

	public void Hide()
	{
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (false);
	}


	public void OnClickBack()
	{
//		if (FindObjectOfType<FreaksPanelUIController> () != null)
//		{
//			FindObjectOfType<FreaksPanelUIController> ().PopPanel ();
//		}
//		else {
			FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
//		}
	}


	public void OnTextEdit(InputField searchinput)
	{
		Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text);
		//Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text + " Count ----- > "+  tempCountryNames.Count);
		if (searchinput.text.Length > 0) 
		{
			for (int i = 0; i < tempServerContactList.Count; i++)
			{
				if (tempServerContactList [i].name.ToUpper ().Contains (searchinput.text.ToUpper())) 
				{

				} 
				else {
					//Debug.Log ("Remove ---->" + tempCountryNames [i].countryName + " Count --- > " + tempCountryNames.Count);
					tempServerContactList.RemoveAt (i);
				}
			}
		}
		else {
			tempServerContactList.Clear ();
			for (int i = 0; i <  serverContactList.Count; i++) 
			{
				tempServerContactList.Add (serverContactList [i]);
			}
		}

		//m_tableView.RefreshVisibleRows ();
		m_tableView.ReloadData ();

	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView)
	{
		//return 10;
		if (tempServerContactList.Count > 0) {
			return tempServerContactList.Count;
		} else {
			return 0;
		}
	}


	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}


	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
		if (cell == null) {
			cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}


	void AssignDetails(ContactDetailsDisplay cell, int rowNumb)
	{
		ServerUserContact cont = tempServerContactList [rowNumb];

		if (!string.IsNullOrEmpty (cont.name)) {
			cell.nameText.text = cont.name;
		} else {
			cell.nameText.text = "Unknown Contact " + rowNumb;
		}
		cell.numberText.text = cont.contact_number;
//		cell.numberProText.text = cont.contactNumb;
		cell.iD = cont.user_id.ToString();
		cell.IntUser (cont.user_id.ToString(), cont.name);
		cell.selectionToggle.gameObject.SetActive (false);
		cell.addButton.onClick.RemoveAllListeners ();
		cell.addButton.onClick.AddListener(() => cell.OnClickServerContactToRefer());
	}
		

	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}
		

	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
