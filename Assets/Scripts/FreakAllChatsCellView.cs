﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;
using Freak.Chat;
using SendBird;

/// <summary>
/// This is the view of our cell which handles how the cell looks.
/// </summary>
public class FreakAllChatsCellView : EnhancedScrollerCellView
{
	/// <summary>
	/// This is a reference to the cell's underlying data.
	/// We will store it in the SetData method, and use it
	/// in the RefreshCellView method.
	/// </summary>
	//private Data _data;

	/// <summary>
	/// A reference to the UI Text element to display the cell data
	/// </summary>

	public ChatChannel chatChannel;
	private string m_channelUrl;
	public Text numberText;

	public Button contactsButton;
	public Image img;
	public RawImage profileImage;
	public Texture2D imgTex;
	public Text nameText;
	public Text otherDetailText;
	public Button addButton;
	public Toggle OnlineDisplay;
	public string imageUrl;
	public Text unreadMessage;
	public Image unreadMessageObj;
	public Button deleteGroup;

	public Text m_rowNumberText;
	public Slider m_cellHeightSlider;

	public string phoneNumb;

	public Toggle selectionToggle;

	private ServerContactsSelectedForGroups userForGroup;

	public Sprite profilePicHollder;
	public Sprite groupProfilePicHolder;

	public Text timerText;
	public Button inviteButton;
	//  private FreakChatSerializeClass.FreakMessagingChannel freakMessagingChannel;

	public bool isTagFriend;
	private Sprite defaultImage;

	//public GameObject freakPanel;
	public GameObject chatPanel;
	public Image messageTypeImage;
	public Text messageTypeText;

	private string profileAvatarLink;

    private AllChatsUIController allChatsUIController;

	public RectTransform RectTransform
	{
		get
		{
			var rt = gameObject.GetComponent<RectTransform>();
			return rt;
		}
	}

    void Awake ()
    {
        allChatsUIController = FindObjectOfType<AllChatsUIController>();
    }

    void OnEnable ()
    {
        AllChatsUIController.MessageTyping += OnMessageTyping;

        //Check for for previous typing.....
        allChatsUIController.CheckForTyping();
    }

    void OnDisable ()
    {
        AllChatsUIController.MessageTyping -= OnMessageTyping;
        OnDisableResetTypingParameter();
    }

	/// <summary>
	/// This function just takes the Demo data and displays it
	/// </summary>
	/// <param name="data"></param>
    public void SetData(ChatChannel channelDetails)
	{
		// store the data so that it can be used when refreshing
        chatChannel = channelDetails;
        m_channelUrl = chatChannel.channelUrl;

        // update the cell's UI
        DisplayDetails();
    }

    public override void RefreshCellView()
    {
        // update the UI text with the cell data
        RefreshChtChannel();
    }


    public void RefreshChtChannel ()
    {
        chatChannel = new ChatChannel(CacheManager.GetChatChannel(m_channelUrl));
		#if UNITY_EDITOR && UNITY_DEBUG
		//        Debug.Log ("ContactDetailsDisplay " + gameObject.name);
		#endif

		DisplayDetails();
	}

	void DisplayDetails ()
	{
        unreadMessage.text = chatChannel.allChatChannelDetails.unReadMsg;
        unreadMessageObj.gameObject.SetActive(string.IsNullOrEmpty(chatChannel.allChatChannelDetails.unReadMsg) == false);
        nameText.text = chatChannel.allChatChannelDetails.displayName;
        timerText.text = chatChannel.allChatChannelDetails.MessageSendTime;
        numberText.text = chatChannel.allChatChannelDetails.MessageToDisplay;

		OnlineDisplay.isOn = (chatChannel.allChatChannelDetails.status == ChatUser.UserConnectionStatus.Online);
		OnlineDisplay.gameObject.SetActive(chatChannel.isGroupChannel == false);

        DisplayMessage();
		ShowProfileImageURL();
		ChangeColourIfUnread(chatChannel.unreadMessageCount > 0);
	}

    void DisplayMessage ()
    {
        numberText.text = chatChannel.allChatChannelDetails.MessageToDisplay;
        if (chatChannel.allChatChannelDetails.FileType == ChatFileType.FileType.None)
        {
            messageTypeImage.gameObject.SetActive(false);
        }
        else
        {
            numberText.text = string.Empty;
            messageTypeImage.sprite = UIController.Instance.messageSpriteType[(int)chatChannel.allChatChannelDetails.FileType];
            messageTypeImage.gameObject.SetActive(true);
            messageTypeText.text = chatChannel.allChatChannelDetails.MessageToDisplay;
        }
    }

	public void ShowProfileImageURL ()
	{
		Freak.FolderLocation _location = Freak.FolderLocation.Profile;

		if (chatChannel.isGroupChannel)
		{
			_location = Freak.FolderLocation.GroupProfile;
		}

		if (!string.IsNullOrEmpty (chatChannel.allChatChannelDetails.imageUrl)) 
		{
			Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache (chatChannel.allChatChannelDetails.imageUrl, _location, null);
		
			if (texture != null) 
			{
				profileImage.texture = texture as Texture;
				profileImage.PreserveAspectToParent ();
			} 
			else {
				if (profileImage != null) 
				{
					if (chatChannel.isGroupChannel) 
					{
                        profileImage.texture = FreakAppManager.Instance.defaultGroupProfileTexture as Texture;
					}
					else 
					{
                        profileImage.texture = FreakAppManager.Instance.defaultProfileTexture as Texture;
					}
					profileImage.PreserveAspectToParent ();
				}

				Invoke ("ShowProfileImageURL", 0.5f);
			}
		}
	}

	private Color unreadColor = new Color (204f/255f, 60f/255f, 60f/255f);
	private Color readColor = new Color (131f/255f, 131f/255f, 131f/255f);
	void ChangeColourIfUnread(bool isUnread)
	{
		if (isUnread) 
		{
			nameText.color = unreadColor;
			numberText.color = unreadColor;
			timerText.color = unreadColor;
		}
		else{
			nameText.color = readColor;
			numberText.color = readColor;
			timerText.color = readColor;
		}
	}

	public void OnClickOpenProfile()
	{
		FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = (Texture2D)profileImage.texture;

        if (chatChannel.isGroupChannel)
        {
            chatChannel = CacheManager.GetChatChannel(m_channelUrl);
            
            ChatManager.Instance.groupUsersList.Clear();
            FreakAppManager.Instance.selectedGroupProfile = CacheManager.GetChatChannel(m_channelUrl);
            FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
        }
        else
        {
            /*FreakAppManager.Instance.currentGameData = new FreakAppManager.GameIntegrateData();
            FreakAppManager.Instance.currentGameData.opponent_id = chatChannel.otherUserId;
            FreakAppManager.Instance.currentGameData.opponent_name = chatChannel.otherUserName;
            FreakAppManager.Instance.currentGameData.opponent_profile_url = profileAvatarLink;*/
            
            Debug.Log("Open User Profile NAME: " + chatChannel.otherUserName + " ID: " + chatChannel.otherUserId);

            List<ServerUserContact> userSynchedContacts = new List<ServerUserContact>(FreakAppManager.Instance.GetAllServerUserInfo());
            if (userSynchedContacts.Count > 0)
            {
                for (int i = 0; i < userSynchedContacts.Count; i++)
                {
                    if (userSynchedContacts[i].user_id == int.Parse(chatChannel.otherUserId))
                    {
                        FreakAppManager.Instance.myUserProfile.memberId = chatChannel.otherUserId;
                        FreakAppManager.Instance.GetHomeScreenPanel().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
                        return;
                    }
                }
            }

            Debug.LogError("USER IS NOT IN SERVER LIST......");
            FindObjectOfType<Freak.AlertMessege>().ShowAlertWindow(Freak.AlertMessege.AlertType.MessegeWithOkButton, chatChannel.otherUserName + " is not in you Freak Contact List! \n \n Still you can send message.");
        }
	}


	public void OnClickOpenChat ()
	{

        #if UNITY_EDITOR && UNITY_DEBUG
        //        Debug.Log ("TIME.TIME:: " + Time.time);
        //        Debug.Log ("Chat User NAME ---- >"+ chatChannel.otherUserName);
        #endif

        FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = profileImage.texture as Texture2D;

        ChatChannel _chatChannel = CacheManager.GetChatChannel(m_channelUrl);
        GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
        go.GetComponent<ChatWindowUI> ().InitChat (_chatChannel, true);

        if (_chatChannel.groupChannel == null)
        {
            ChatManager.Instance.GetChannelByChannelUrl (_chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
        }
        else
        {
            go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(_chatChannel.groupChannel);
        }

        /*if (string.IsNullOrEmpty (m_channelUrl))
        {
            go.GetComponent<ChatWindowUI> ().InitChat (_chatChannel);
        }
        else
        {
            if (_chatChannel != null)
			{
                go.GetComponent<ChatWindowUI> ().InitChat (_chatChannel, true);
                if (_chatChannel.groupChannel == null)
				{
                    ChatManager.Instance.GetChannelByChannelUrl (_chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
				}
				else
				{
                    go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(_chatChannel.groupChannel);
				}
			}
		}*/
	}


	public void OnClickServerContact()
	{
		if (selectionToggle.isOn)
		{
			if (!ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
			{
				ChatManager.Instance.groupUsersList.Add (userForGroup);
			}
		}
		else {
			if (ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
			{
				ChatManager.Instance.groupUsersList.Remove (userForGroup);
			}
		}

		#if UNITY_EDITOR && UNITY_DEBUG
		Debug.Log ("Server Contacts selected for group ---- >"+  ChatManager.Instance.groupUsersList.Count);
		#endif
	}

    #region TYPING
    private string tempDisplayMessage;
    private ChatFileType.FileType tempDisplayFileType;
    private string typingUsername;
    private Coroutine typingRoutine;
    private bool isTyping;

    public void OnMessageTyping(bool isTyping, string channelurl, List<SendBird.User> userIds)
    {
        if (chatChannel.channelUrl.Equals (channelurl))
        {
            if (isTyping)
            {
                tempDisplayMessage = chatChannel.allChatChannelDetails.MessageToDisplay;
                tempDisplayFileType = chatChannel.allChatChannelDetails.FileType;
                
                for (int i = 0; i < userIds.Count; i++)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("OnMessageTyping " + isTyping + "By " + userIds[i].UserId);
                    #endif
                    
                    typingUsername = userIds [i].Nickname;
                    DisplayIsTyping ();
                }
            }
            else
            {
                if (typingRoutine != null)
                    StopCoroutine (typingRoutine);
                
                CancelInvoke ("DisplayIsTyping");
                this.isTyping = false;
                DisplayMessage();
                tempDisplayMessage = "";
                tempDisplayFileType = ChatFileType.FileType.None;
            }
        }
    }

    void OnDisableResetTypingParameter()
    {
        if (typingRoutine != null)
            StopCoroutine (typingRoutine);

        CancelInvoke ("DisplayIsTyping");
        this.isTyping = false;
        DisplayMessage();
        tempDisplayMessage = "";
        tempDisplayFileType = ChatFileType.FileType.None;
    }

    void DisplayIsTyping ()
    {
        if (isTyping) {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("_KOSA_+_SAS");
            #endif
            Invoke ("DisplayIsTyping", 1f);
            return;
        }

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Started "+isTyping);
        #endif
        isTyping = true;

        if (typingRoutine != null)
            StopCoroutine (typingRoutine);

        typingRoutine = StartCoroutine (IsTyping (typingUsername));
    }

    IEnumerator IsTyping(string username)
    {
        string _username = "typing";
        if (ChatManager.IsSingleChannel (chatChannel.channelName) == false)
        {
            _username = username + " is typing";
        }

        DisplayTypingMessage (string.Empty);
        DisplayTypingMessage (_username + " .");
        yield return new WaitForSeconds (0.5f);
        DisplayTypingMessage (_username + " . " + ".");
        yield return new WaitForSeconds(0.5f);
        DisplayTypingMessage (_username + " . " + ". " + ".");
        yield return new WaitForSeconds(0.5f);

        isTyping = false;

        DisplayIsTyping ();
    }

    void DisplayTypingMessage (string typingMsg)
    {
        numberText.text = typingMsg;
        messageTypeImage.gameObject.SetActive(false);
    }

    #endregion

}