﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tastybits.EmojiAssist;

namespace Freak
{
    public class Emoji : MonoBehaviour
    {
        enum EmojiType
        {
            emoticons = 0,
            dingbats
        }

        [HideInInspector]
        public Chat.ChatWindowUI chatWindowUI;
        public ScrollRect scrollRect;
        public ScrollRect horizontalScrollSnap;
        public Button emojiButton;

        IDictionary emojiDictionary;

        private ScrollRect[] mScrollRectList;
        public object currentEmoji;

        // Use this for initialization
        void Start () {

//            scrollRect.GetComponent<LayoutElement>().minWidth = (float)Screen.width;

            if (emojiDictionary == null)
            {
                var _textAssets = Resources.Load("LocalDatabase/EmojiList") as TextAsset;
                emojiDictionary = (IDictionary)MiniJSON.Json.Deserialize(_textAssets.text);
            }

            mScrollRectList = new ScrollRect[emojiDictionary.Count];

            foreach (string item in emojiDictionary.Keys)
            {
                EmojiType _emoType = (EmojiType)System.Enum.Parse(typeof(EmojiType), item);
                int _index = (int)_emoType;
                mScrollRectList[_index] = Instantiate(scrollRect) as ScrollRect;
                mScrollRectList[_index].transform.SetParent(transform, false);
                mScrollRectList[_index].gameObject.name = _emoType.ToString();
//                mScrollRectList[_index].gameObject.SetActive(true);


                IList _emojiList = (IList)emojiDictionary[_emoType.ToString()];
                for (int i = 0; i < _emojiList.Count; i++)
                {
                    Button _emoButton = (Button)Instantiate(emojiButton);
                    _emoButton.transform.SetParent(mScrollRectList[_index].content.transform);

                    Text _emoText = _emoButton.GetComponent<Text>();
                    _emoText.text = _emojiList[i].ToString();
                    _emoButton.onClick.AddListener(() =>
                        {
//                            OnSelectedEmoji(_emoText.text);
                            OnSelectedEmoji(_emoText.text);
                        });
                    _emoButton.gameObject.SetActive(true);
                }
            }

            OnClickToggleGroup(0);
        }

        public void OnClickToggleGroup (int index)
        {
            /*for (int i = 0; i < mScrollRectList.Length; i++)
            {
                if (index == i)
                    mScrollRectList[i].gameObject.SetActive(true);
                else
                    mScrollRectList[i].gameObject.SetActive(false);
            }*/
        }

//        void InitEmoji (int emojiType)
//        {
//            string _emojiTypeString = ((EmojiType)emojiType).ToString();
//            IList _emojiList = (IList)emojiDictionary[_emojiTypeString];
//            for (int i = 0; i < _emojiList.Count; i++)
//            {
//                Button _emoButton = (Button)Instantiate(emojiButton);
//                _emoButton.transform.SetParent(scrollRect.content.transform);
//
//                Text _emoText = _emoButton.GetComponent<Text>();
//                _emoText.text = _emojiList[i].ToString();
//                _emoButton.onClick.AddListener(() =>
//                    {
//                        OnSelectedEmoji(_emoText.text);
//                    });
//                _emoButton.gameObject.SetActive(true);
//            }
//        }

//        void AddText (GameObject obj)
//        {
//            Text _emojiText = obj.AddComponent<Text>();
//            _emojiText.resizeTextForBestFit = true;
//
//        }

        public InputField m_TestInputField;
        void OnSelectedEmoji (string emojiType)
        {
            if (m_TestInputField != null)
            {
                m_TestInputField.text = emojiType;
                return;
            }

//            emojiType = emojiSupport.TextAsStringWithEmojisReplacedWithInvalidChar;

//            Debug.Log("emoji Var" + emojiVar);

//            int _int = System.Char.ConvertToUtf32(emojiType, 0);
//            string _string = System.Char.ConvertFromUtf32(_int);
//            Debug.Log("emojiType " + _int);
//            Debug.Log("emojiType " + _string);
            Text = emojiType;// = "\U0001F41B";
            Debug.Log("FACE " + MiniJSON.Json.Serialize(emojiType));
            Debug.Log("LLL " + emojiType.Length);
            chatWindowUI.inputFieldPanelUI.EmojiSelected(emojiType);
//            Debug.Log(TextAsStringWithEmojisReplacedWithInvalidChar);
//            chatWindowUI.inputFieldPanelUI.EmojiSelected(emojiType);
        }
        
        // Update is called once per frame
        void Update () {
            
        }

        private string Text;

        private string TextAsStringWithEmojisReplacedWithInvalidChar {
            get {
                string result = string.Empty;
                foreach( int codePoint in Text.AsCodePoints() ) {
                    if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
                        Debug.Log("codePoint " + codePoint);
                        result += System.Char.ConvertFromUtf32(codePoint);//0xFFFD );
                    } else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
                        Debug.Log("codePoint " + codePoint);
                        result += System.Char.ConvertFromUtf32( 0xFFFD );
                    } else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
                        Debug.Log("codePoint " + codePoint);
                        result += System.Char.ConvertFromUtf32( 0xFFFD );
                    } else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
                        Debug.Log("codePoint " + codePoint);
                        result += System.Char.ConvertFromUtf32( 0xFFFD );
                    } else {
                        Debug.Log("codePoint " + codePoint);
                        string strCh = System.Char.ConvertFromUtf32( codePoint );
                        int szBefore = result.Length;
                        result += strCh;
                        if( result.Length > szBefore + 1 ) {
                            Debug.LogError("Warning the string increasd more than one character");
                        }
                    }
                }
                Debug.Log("Result " + MiniJSON.Json.Serialize(result));
                Debug.Log("Result " + result.Length);
                return result;
            }
        }
    }
}
