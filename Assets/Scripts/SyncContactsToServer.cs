﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Freak;
using System.Linq;

public class SyncContactsToServer : MonoBehaviour {

	private List<string> contactNumbers = new List<string> ();
	private string failString;
	private GameObject connectingObj;
	private bool isRefresh;

	// Use this for initialization
	void Start () 
	{
	
	}
		
	public void SyncContacts(GameObject loadingObj)
	{
		if (!isRefresh) 
		{
			isRefresh = true;
			connectingObj = loadingObj;
			Contacts.ContactsList.Clear ();
			Contacts.LoadContactList (onDone, onLoadFailed);
		}
	}

	void CallServerToSsync()
	{
		for (int i = 0; i < Contacts.ContactsList.Count; i++)
		{
			for (int k = 0; k < Contacts.ContactsList [i].Phones.Count; k++)
			{
				string number = Contacts.ContactsList [i].Phones [k].Number.ToString ();
				number = number.Replace (" ", "");
				number = number.Replace ("-", "");
				number = number.Replace ("+", "");
				bool digitsOnly = number.All(char.IsDigit);

				if (isDigits(number))
				{
					//ok, do something
					if (number.Length > 10) 
					{
						number = number.Substring (number.Length - 10);
					}
					if (!number.Equals (FreakAppManager.Instance.mobileNumber)) 
					{
						contactNumbers.Add (number);
						Contacts.ContactsList [i].Phones [k].Number = number;
					}
				}
				else
				{
					//not an int
					Debug .LogWarning("NUMBER DATA NOT SYNC-----"+ number);
				}
			}
		}

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;
        FreakAppManager.Instance.allContacts = new List<Contact>(Contacts.ContactsList);

		string numberdata = MiniJSON.Json.Serialize (contactNumbers);
		Debug .Log("NUMBER DATA TO SYNC-----"+ numberdata);
		SynAllContactsToServer (numberdata);
	}

    void InitAllContactWithServerContact (List<ServerUserContact> data)
    {
        for (int i = 0; i < FreakAppManager.Instance.allContacts.Count; i++)
        {
            for (int j = 0; j < data.Count; j++)
            {
                if (FreakAppManager.Instance.allContacts[i].Phones [0].Number.Equals (data [j].contact_number))
                {
                    FreakAppManager.Instance.allContacts [i].isFreak = true;
                    FreakAppManager.Instance.allContacts [i].freakUserId = data [j].user_id.ToString();
                }
            }
        }

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;
    }

	bool isDigits(string s) 
	{ 
		if (s == null || s == "") return false; 

		for (int i = 0; i < s.Length; i++) 
			if ((s[i] ^ '0') > 9) 
				return false; 

		return true; 
	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
		CallServerToSsync ();
	}


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	void SynAllContactsToServer(string dataString)
	{
		FreakApi api  = new FreakApi("contact.syncContactToServer", ServerResponseForSynAllContactsToServer);
		api.param("userId",FreakAppManager.Instance.userID);
		api.param("accessToken",FreakAppManager.Instance.APIkey);
		api.param("contact_number", dataString);
		api.post(this);
	}

	void ServerResponseForSynAllContactsToServer(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		isRefresh = false;
		// check for errors
		if (output!=null)
		{
            string responseInfo =(string)output["responseInfo"];

            switch(responseCode){
                case 001:
                    FreakPlayerPrefs.ClearServerUserContact();
                    AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.refreshContacts);
//                    connectingObj.SetActive(false);

                    GetContactListAPICall();
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
            connectingObj.SetActive(false);
			////inapi.get(this);
		}  
	}

    void GetContactListAPICall()
    {
        FreakApi api  = new FreakApi("contact.listContact", ServerResponseForGetContactListAPICall);
        api.param("userId",FreakAppManager.Instance.userID);
        api.param("accessToken",FreakAppManager.Instance.APIkey);
        api.get(this);
    }

    void ServerResponseForGetContactListAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
    {
        // check for errors
        if (output!=null)
        {
            string responseInfo =(string)output["responseInfo"];
            switch(responseCode)
            {
                case 001:
                    List<object> userDetailsList = (List<object>)output["responseMsg"];
                    List<ServerUserContact> tempserverContactList = new List<ServerUserContact>();
                    for (int i = 0; i < userDetailsList.Count; i++)
                    {
                        Dictionary<string, object> data = (Dictionary<string,object>)userDetailsList[i];
                        ServerUserContact newContact = new ServerUserContact(data);
                        if (!newContact.contact_number.Equals(FreakAppManager.Instance.mobileNumber))
                        {
                            tempserverContactList.Add(newContact);
                        }
                    }

                    LoadContactListFromServerDone(tempserverContactList);
                    break;
                case 207:
                    /**
                * User email id Does not Exists.. U need to signIn first...
                */
                    break;
                default:
                    /**
                 * Show Popup to let user know if anything has gone wrong while login in...
                 */
                    break;
            }   
        } 
        else {
            Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
            connectingObj.SetActive(false);
        }
    }

    void LoadContactListFromServerDone(List<ServerUserContact> data)
    {
        for (int i = 0; i < FreakAppManager.Instance.allContacts.Count; i++)
        {
            for (int j = 0; j < data.Count; j++)
            {
                if (FreakAppManager.Instance.allContacts[i].Phones [0].Number.Equals (data [j].contact_number))
                {
                    FreakAppManager.Instance.allContacts [i].isFreak = true;
                    FreakAppManager.Instance.allContacts [i].freakUserId = data [j].user_id.ToString();
                }
            }
        }

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;

        connectingObj.SetActive(false);
    }

	System.Action<bool> areContactsLoaded;

	public void GetPhoneContacts(System.Action<bool> loadedContacts)
	{
		areContactsLoaded = loadedContacts;
		Contacts.LoadContactList (onDoneLoading, onLoadFailedLoading);
	}

	void onLoadFailedLoading( string reason )
	{
		failString = reason;
		areContactsLoaded (false);
	}

	void onDoneLoading()
	{
		failString = null;
		areContactsLoaded (true);
	}
}
