﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AudienceNetwork;

namespace Freak.Ads
{
    public class FacebookNativeAd : MonoBehaviour
    {
        [HideInInspector] public string placementID;
        [HideInInspector] public bool isAdLoaded;

        public string Title                 { get { return nativeAd.Title; } }
        public string SocialContext         { get { return nativeAd.SocialContext; } }
        public string Subtitle              { get { return nativeAd.Subtitle; } }
        public string Body                  { get { return nativeAd.Body; } }
        public string CallToAction          { get { return nativeAd.CallToAction; } }
        public string IconImageURL          { get { return nativeAd.IconImageURL; } }
        public string CoverImageURL         { get { return nativeAd.CoverImageURL; } }
        public string PlacementId           { get { return nativeAd.PlacementId; } }

        public Sprite IconImage             { get { return nativeAd.IconImage; } }
        public Sprite CoverImage            { get { return nativeAd.CoverImage; } }

        public bool IsValid                 { get { return nativeAd.IsValid(); } }

        private NativeAd nativeAd;
        private Button callToActionButton;

        void Awake ()
        {
            callToActionButton = GetComponent<Button>();
        }

        // Use this for initialization
        void Start () {
            LoadAd();
        }

        public Button.ButtonClickedEvent GetButtonClickEvent ()
        {           
            return callToActionButton.onClick;
        }

        public void LoadAd ()
        {
            // Create a native ad request with a unique placement ID (generate your own on the Facebook app settings).
            // Use different ID for each ad placement in your app.
            NativeAd nativeAd = new AudienceNetwork.NativeAd (placementID);
            this.nativeAd = nativeAd;

            // Wire up GameObject with the native ad; the specified buttons will be clickable.
            nativeAd.RegisterGameObjectForImpression (gameObject, new Button[] { callToActionButton });

            // Set delegates to get notified on changes or when the user interacts with the ad.
            nativeAd.NativeAdDidLoad = HandleNativeAdDidLoad;
            nativeAd.NativeAdDidFailWithError = HandleNativeAdDidFailWithError;
            nativeAd.NativeAdWillLogImpression = HandleNativeAdWillLogImpression;
            nativeAd.NativeAdDidClick = HandleNativeAdDidClick;

            // Initiate a request to load an ad.
            nativeAd.LoadAd ();
        }

        void HandleNativeAdDidLoad ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleNativeAdDidLoad Native ad loaded.");
            #endif

            isAdLoaded = true;

            #if !UNITY_EDITOR
            StartCoroutine (nativeAd.LoadIconImage (nativeAd.IconImageURL));
            StartCoroutine (nativeAd.LoadCoverImage (nativeAd.CoverImageURL));
            #endif
        }

        void HandleNativeAdDidFailWithError (string error)
        {
            Debug.LogError ("HandleNativeAdDidFailWithError " + error);
        }

        void HandleNativeAdWillLogImpression ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleNativeAdWillLogImpression Native ad logged impression.");
            #endif
        }

        void HandleNativeAdDidClick ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleNativeAdDidClick Native ad clicked.");
            #endif
        }

        void OnDestroy ()
        {
            // Dispose of native ad when the scene is destroyed
            if (this.nativeAd) {
                this.nativeAd.Dispose ();
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("NativeAdTest was destroyed!");
            #endif
        }
    }
}
