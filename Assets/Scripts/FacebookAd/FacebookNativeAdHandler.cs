﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Ads
{
    public class FacebookNativeAdHandler : MonoBehaviour
    {
        #region Init Instance

        private static FacebookNativeAdHandler instance;
        public static FacebookNativeAdHandler Instance
        {
            get
            {
                return InitInstance();
            }
        }

        public static FacebookNativeAdHandler InitInstance()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("FacebookNativeAdHandler");
                instance = instGO.AddComponent<FacebookNativeAdHandler>();

                DontDestroyOnLoad(instGO);
            }
            return instance;
        }

        #endregion

        private const string FacebookNativeAd_Prefab_Path = "Prefab/Ads/FacebookNativeAd";
        private List<FacebookNativeAd> facebookNativeAdList = new List<FacebookNativeAd>();
        private FacebookNativeAd facebookNativeAdPrefab;
        private string[] placementIDs = new string[]{ "401083416908826_401272540223247" };

        void Awake ()
        {
            InitPrefab();
        }

        // Use this for initialization
        void Start () {
            
        }

        void InitPrefab ()
        {
            facebookNativeAdPrefab = (FacebookNativeAd)Resources.Load<FacebookNativeAd>(FacebookNativeAd_Prefab_Path);
            facebookNativeAdPrefab.placementID = placementIDs[0];
        }
        
        public List<FacebookNativeAd> GetAdsList ()
        {
            return facebookNativeAdList;
        }

        public void LoadAds ()
        {
            if (facebookNativeAdPrefab == null)
            {
                InitPrefab();
            }
            
            FacebookNativeAd _facebookNativeAd = (FacebookNativeAd) Instantiate(facebookNativeAdPrefab);
            facebookNativeAdList.Add(_facebookNativeAd);
            _facebookNativeAd.transform.SetParent(this.transform, false);
            _facebookNativeAd.gameObject.SetActive(true);
        }

        public void RemoveAllAds ()
        {
            for (int i = 0; i < facebookNativeAdList.Count; i++)
            {
                Destroy(facebookNativeAdList[i].gameObject);
            }

            facebookNativeAdList.Clear();
            Resources.UnloadUnusedAssets();
        }

        public void RemoveAdsExceptLastOne ()
        {
            if (facebookNativeAdList.Count > 1)
            {
                FacebookNativeAd _facebookNativeAd = facebookNativeAdList[facebookNativeAdList.Count - 1];

                RemoveAllAds();

                facebookNativeAdList.Add(_facebookNativeAd);
                _facebookNativeAd = null;
            }
        }
    }
}
