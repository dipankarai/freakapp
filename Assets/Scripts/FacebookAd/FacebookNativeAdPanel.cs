﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using AudienceNetwork;

namespace Freak.Ads
{
    public class FacebookNativeAdPanel : MonoBehaviour
    {
        // UI elements in scene
        [Header("Text:")]
        public Text title;
        public Text socialContext;
        public Text body;

        [Header("Images:")]
        public Image coverImage;
        public Image iconImage;
        public Sprite defaultImage;

        [Header("Buttons:")]
        public Text callToAction;
        public Button callToActionButton;

        public float PanelHeight { get { return 450f; } }

        private FacebookNativeAd nativeAd;
        private RectTransform rectTransform;
        private bool adsLoaded = false;

        void Awake ()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        void OnEnable ()
        {
            StartCoroutine("LoadAdsRoutine");
        }

        void OnDisable ()
        {
            adsLoaded = false;
            StopCoroutine("LoadAdsRoutine");
            iconImage.sprite = defaultImage;
            coverImage.sprite = defaultImage;
        }

        public void LoadAds (Freak.Ads.FacebookNativeAd nativeAd)
        {
            this.nativeAd = nativeAd;
        }

        public void InvokeCoroutine ()
        {
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine("LoadAdsRoutine");
            }
            else
            {
                Invoke("InvokeCoroutine", 1f);  
            }
        }

        IEnumerator LoadAdsRoutine ()
        {
			if (nativeAd != null) 
			{
				while (nativeAd.isAdLoaded == false) {
					if (nativeAd.isAdLoaded)
						break;

					yield return null;
				}
			
				socialContext.text = nativeAd.SocialContext;
				title.text = nativeAd.Title;
				body.text = nativeAd.Body;

				callToAction.text = nativeAd.CallToAction;
				callToActionButton.onClick = nativeAd.GetButtonClickEvent ();

				InvokeIconImage ();
				InvokeCoverImage ();
			}

        }

        void InvokeIconImage ()
        {
            if (nativeAd.IconImage != null)
            {
                iconImage.sprite = nativeAd.IconImage;
            }
            else
            {
                Invoke("InvokeIconImage", 1f);
            }
        }

        void InvokeCoverImage ()
        {
            if (nativeAd.CoverImage != null)
            {
                coverImage.sprite = nativeAd.CoverImage;
            }
            else
            {
                Invoke("InvokeCoverImage", 1f);
            }
        }
    }
}
