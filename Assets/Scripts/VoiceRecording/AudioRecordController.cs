﻿using UnityEngine;
using System.Collections;
using AudioRecorder;
using UnityEngine.UI;
using System.IO;
using Freak.Chat;
using Freak;
using System.Threading;

/// <summary>
/// /*Audio record controller this class used to recored the audio and save into disk*/
/// </summary>
public class AudioRecordController : MonoBehaviour 
{

	Recorder recorder;

	public bool autoPlay;

	private string path;
	private string newFilePath;

	public Text timerText;

//	private float startTime = 0;
	private float timer = 0;
    private string filePath;
    private string filename;
	public GameObject sendButtonParent;

	void OnEnable()
	{
		recorder = new Recorder();
		Recorder.onInit += OnInit;
		Recorder.onFinish += OnRecordingFinish;
		Recorder.onError += OnError;
		Recorder.onSaved += OnRecordingSaved;

		recorder.Init();
	}

	void OnDisable()
	{
		Recorder.onInit -= OnInit;
		Recorder.onFinish -= OnRecordingFinish;
		Recorder.onError -= OnError;
		Recorder.onSaved -= OnRecordingSaved;

        isHoldingButton = false;
        timerText.text = "00:00";
	}

    //Use this for initialization  
    void Start()   
    {  
        filePath = FreakAppManager.ApplicationPath;
    }  

    void Update()
    {
        if (isHoldingButton) 
        {
            //Timer function
            timer += Time.deltaTime;

            var minutes = timer / 60; 
            var seconds = timer % 60;

			timerText.text = string.Format ("{0:00}:{1:00}", minutes, seconds );
		}
	}
	private bool isHoldingButton = false;

	public void OnPointerDownEvent()
	{
        Debug.Log("OnPointerDownEvent");
		timer = 0;
		timerText.text = "00:00";

		sendButtonParent.SetActive (false);

		isHoldingButton = true;

		if (recorder.IsReady)
		{  
			if (!recorder.IsRecording) 
			{ 
				recorder.StartRecording (false, 60);
			}
		}
	}

	public void OnPointerUpEvent()
	{
        Debug.Log("OnPointerUpEvent");
		isHoldingButton = false;

		recorder.StopRecording();

		if (recorder.hasRecorded)
		{
			sendButtonParent.SetActive (true);

            filename = GetFileName();
            filePath = Freak.StreamManager.LoadFilePath(filename, FolderLocation.Audio, true);

            Debug.Log ("PATH==== " + filePath);
            if (!File.Exists (filePath))
//            if (!File.Exists (filePath + filename))
            {
//                recorder.Save(System.IO.Path.Combine(filePath,filename),recorder.Clip);
                recorder.Save(filePath,recorder.Clip);
			}

//            recorder.PlayRecorded(GetComponent<AudioSource>());
		}
	}

	void OnInit(bool success)
	{
		Debug.Log("Success : "+success);
	}

	void OnError(string errMsg)
	{
		Debug.Log("Error : "+errMsg);
	}

	void OnRecordingFinish(AudioClip clip)
	{
        Debug.Log("OnRecordingFinish");
		if(autoPlay)
		{
			//recorder.PlayAudio (clip, audioSource);

			// or you can use
			//recorder.PlayRecorded(audioSource);
		}
	}
	/// <summary>
	/// Raises the recording saved event.
	/// </summary>
	/// <param name="path">Path.</param>
	void OnRecordingSaved(string _path)
	{
		Debug.Log("File Saved at : "+ _path);
        path = _path;
		//recorder.PlayAudio (path, audioSource);
	}


	public void OnSendButtonClicked()
	{
        Debug.Log("OnSendButtonClicked");

		timerText.text = "00:00";
//        if (ChatManager.CheckInternetConnection())
        {
            if (File.Exists(filePath))
            {
                Debug.Log ("File Exits=====>>>>> "+filePath);
                ChatFileType chatFileToSend = new ChatFileType(path, ChatFileType.FileType.AudioRecord);
                transform.parent.GetComponent<AudioPanelUI>().chatWindowUI.DuplicateMessageBox(chatFileToSend, "");
//                ChatManager.Instance.fileSendCallback = UIController.Instance.PlaySendMessageSound;
//                ChatManager.Instance.SendFileMessage(chatFileToSend, "");
            }
            else
            {
                Debug.LogError ("File Not Exits=====>>>>> " + filePath);
            }
        }

		sendButtonParent.SetActive (false);
	}

	private void UploadFile()
	{
        if (string.IsNullOrEmpty (path) == false)
		{
            /*string _filename = FreakChatUtility.FileNameFromUrl(FreakChatUtility.GetCurrentTimeStamp(), path);
			Debug.Log(_filename);

			var bytes = File.ReadAllBytes(path);
			newFilePath = StreamManager.LoadFilePath(_filename, ChatFileType.GetFolderLocation(ChatFileType.FileType.AudioRecord), true);
			Debug.Log(newFilePath + File.Exists(newFilePath));
			StreamManager.SaveFile(_filename, bytes, ChatFileType.GetFolderLocation(ChatFileType.FileType.AudioRecord), SentCallback, true);*/


		}

		path = "";
	}

	private ChatFileType fileType;
	void SentCallback (bool success)
	{
		Debug.Log("SentCallback " + success);
		//Run Thread here
        /*fileThread = new Thread(ThreadMethod);

		fileType = new ChatFileType(newFilePath, ChatFileType.FileType.AudioRecord);
		transform.parent.GetComponent<AudioPanelUI> ().chatWindowUI.TempMessageBox (fileType);

		if (!fileThread.IsAlive)
		{
			fileThread.Start ();
		}*/
		//End Thread
	}

    private string newSavedFilePath;
    private void SaveFileToSendFolder(string filePath, ChatFileType.FileType fileType)
    {
        string _filename = FreakChatUtility.FileNameFromUrl(FreakChatUtility.GetCurrentTimeStamp(), filePath);
        Debug.Log(_filename);
        var bytes = File.ReadAllBytes(filePath);

        newSavedFilePath = StreamManager.LoadFilePath(_filename, ChatFileType.GetFolderLocation(fileType), true);
        Debug.Log(newSavedFilePath + File.Exists(newSavedFilePath));

        StreamManager.SaveFile(_filename, bytes, ChatFileType.GetFolderLocation(fileType), FileSavedCallback, true);
    }

    void FileSavedCallback (bool success)
    {
        Debug.Log("SentCallback " + success);

        ChatFileType chatFileToSend = new ChatFileType(newSavedFilePath, ChatFileType.FileType.AudioRecord);
        transform.parent.GetComponent<AudioPanelUI>().chatWindowUI.DuplicateMessageBox(chatFileToSend, "");
//        ChatManager.Instance.fileSendCallback = UIController.Instance.PlaySendMessageSound;
//        ChatManager.Instance.SendFileMessage(chatFileToSend, "");
        System.IO.File.Delete(path);

        //Run Thread here
        /*fileThread = new Thread(ThreadMethod);
            if (!fileThread.IsAlive)
            {
                fileThread.Start ();
            }*/
        //End Thread
    }

//    void FileUploadToSBCallback (SendBird.FileInfoEventArgs args)
//    {
//        if (args.Exception != null)
//        {
//            Debug.Log("FileUpload Exception " + args.Exception.Message);
//        }
//        else
//        {
//            Debug.Log("FileUploadEvent "+args.FileInfo.customField);
//            Debug.Log("FileUploadEvent "+args.FileInfo.name);
//            Debug.Log("FileUploadEvent "+args.FileInfo.path);
//            Debug.Log("FileUploadEvent "+args.FileInfo.size);
//            Debug.Log("FileUploadEvent "+args.FileInfo.type);
//            Debug.Log("FileUploadEvent "+args.FileInfo.url);
//            
//            ChatFileType.FileType fileType = (ChatFileType.FileType)int.Parse(args.FileInfo.type);
//            ChatManager.Instance.DownLoadFile(args.FileInfo.url, ChatFileType.GetFolderLocation(fileType), null, true);
//            ChatManager.Instance.SendAnyFile(args.FileInfo.url, args.FileInfo.name, args.FileInfo.type, args.FileInfo.size, fileType.ToString());
//        }
//
//		Debug.Log ("File uploaded--------------->>>>>>>>>>>");
//    }

    string GetFileName ()
    {
        return FreakChatUtility.TimeStampToFilename(FreakChatUtility.GetCurrentTimeStamp()) + ".wav";
//        return "Rec_"+System.DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".wav";
    }

	public void OnCancelButtonClick()
	{
		timerText.text = "00:00";
		sendButtonParent.SetActive (false);
        transform.parent.GetComponent<AudioPanelUI>().chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.CloseAll);
	}

	#region Private data
	private Thread fileThread;
	#endregion

	#region Threads
	private void ThreadMethod()
	{
		ChatFileType fileType = new ChatFileType(path, ChatFileType.FileType.AudioRecord);
//		ChatManager.Instance.UploadFileToSendBird(fileType, FileUploadToSBCallback, filename);  
	}
	#endregion
}
