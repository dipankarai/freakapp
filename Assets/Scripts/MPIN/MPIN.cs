﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is used to set and get User MPin
/// </summary>
public class MPIN : MonoBehaviour 
{

	public Text[]  textShow;
	public Text diplayMessegeText;

	public bool isSetMPin = false;

	private int preNum;
	private int currentNum;
	private int[] pin = new int[4];
	private int[] confirmPin = new int[4];
	private int count = 0;

	private string checkMpin ="-1";

	void OnEnable()
	{
		
		for (int i = 0; i < pin.Length; i++)
		{
			pin [i] = -1;
		}
	}

	/// <summary>
	/// This method is called from key board button 
	/// </summary>
	/// <param name="number">Number.</param>
	public void OnNumberButtonClicked(int number)
	{
		if (count < 4)
		{
			pin [count] = number;
			textShow [count].text = pin [count].ToString ();
			StartCoroutine (ShowDot(textShow [count]));
			count++;
			string mpinString = pin[0].ToString()+pin[1].ToString()+pin[2].ToString()+pin[3].ToString();
			if (count == 4) 
			{
                string storedMPin = PlayerPrefs.GetString (FreakAppConstantPara.PlayerPrefsName.MPin);
				if (storedMPin == mpinString) 
				{
					CallScene ();
				}
				else 
				{
					AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Wrong PIN");
					StartCoroutine (Reset());
				}
			}

		} else {
			Debug.Log ("Show alert");
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Login Check");
		}
	}

	/// <summary>
	/// Display dot after some time when user put in
	/// </summary>
	/// <returns>The dot.</returns>
	/// <param name="text">Text.</param>
	IEnumerator ShowDot(Text text)
	{
		yield return new WaitForSeconds (0.3f);
		text.text = "*";
	}

	/// <summary>
	/// Reset the pin
	/// </summary>
	IEnumerator Reset()
	{
		yield return new WaitForSeconds (0.4f);
		count = 0;
		for (int i = 0; i < pin.Length; i++)
		{
			pin [i] = -1;
			textShow [i].text = "_";
		}
	
	}

	/// <summary>
	/// After success call login scene
	/// </summary>
	public void CallScene()
	{
        SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.FreakHomeScene);
	}
}