﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
#if UNITY_IOS || UNITY_IPHONE
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class PushNotification : MonoBehaviour
{
    [DllImport("__Internal")] private static extern void CBG_RegisterForRemoteNotifications(int type);
    [DllImport("__Internal")] private static extern void GetNotificationChatChanneliOS();
    [DllImport("__Internal")] private static extern void DisableNotificationiOS(bool val);
    [DllImport("__Internal")] private static extern void MuteAllNotificationiOS(bool val);
    [DllImport("__Internal")] private static extern void MuteNotificationiOS(string val);
    [DllImport("__Internal")] private static extern void SetLocalNotificationiOS(int id,string title,string message,string sender,string messageId,bool sound);

    #if UNITY_IOS || UNITY_IPHONE
    private int deviceTokenCount = 0;
    private bool pollIOSDeviceToken = false;
    private IEnumerator updateRoutine;
    #endif
    private const int DEVICE_COUNT = 5;
    private static string notificationClassname = "in.notification.unityFCM.Notification";

    void Awake ()
    {
        DontDestroyOnLoad(this.gameObject);
    }

	// Use this for initialization
    void Start ()
    {
		#if !UNITY_EDITOR && !DIRECT_BUILD
        	RequestDeviceToken();

            StartUpdateRoutine();
        #endif
    }

    void StartUpdateRoutine ()
    {
        #if UNITY_IOS || UNITY_IPHONE
        StopUpdateRoutine();

        updateRoutine = Updates();
        StartCoroutine(updateRoutine);
        #endif
    }

    void StopUpdateRoutine ()
    {
        #if UNITY_IOS || UNITY_IPHONE
        if (updateRoutine != null)
            StopCoroutine(updateRoutine);
        #endif
    }

    #if UNITY_IOS || UNITY_IPHONE
    IEnumerator Updates ()
    {
        while(true)
        {
            // Unity does not tell us when the deviceToken is ready, so we have to keep polling after requesting it
            if (pollIOSDeviceToken)
            {
                RegisterIOSDevice();
            }
            yield return null;
        }
        yield return null;
    }
    #endif

    public void RequestDeviceToken() 
    {
        #if UNITY_IOS || UNITY_IPHONE
        if(NotificationServices.deviceToken == null) 
        {
            var unused_var =typeof(UnityEngine.iOS.LocalNotification);
            #if UNITY_EDITOR
            #elif UNITY_IOS || UNITY_IPHONE
            Debug.Log("RequestDeviceToken iOS ------- > Service is NULL");
            var unused = typeof(UnityEngine.iOS.RemoteNotification);
            pollIOSDeviceToken = true;
            NotificationServices.RegisterForNotifications(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);
            //CBG_RegisterForRemoteNotifications((int)NotificationType.Alert | (int)NotificationType.Badge | (int)NotificationType.Sound);
            #endif
        }
        else
        {
            RegisterIOSDevice();
        }

        #elif UNITY_ANDROID
        Debug.Log ("Get NotificationToken");
        GetFCMNotificationToken ();
        #endif
    }

    #if UNITY_IPHONE || UNITY_IOS
    private void RegisterIOSDevice() 
    {
        //Debug.Log("RegisterIOSDevice------- >");
        if(UnityEngine.iOS.NotificationServices.registrationError != null) 
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(UnityEngine.iOS.NotificationServices.registrationError);
            #endif
            deviceTokenCount ++;
            if(deviceTokenCount < DEVICE_COUNT)
            {
                pollIOSDeviceToken = true;
            }
        }

        if (UnityEngine.iOS.NotificationServices.deviceToken == null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("NotificationServices.deviceToken is NULL");
            #endif
            deviceTokenCount++;
            if (deviceTokenCount < DEVICE_COUNT)
            {
                pollIOSDeviceToken = true;
            }
            return;
        }

        pollIOSDeviceToken = false;
        StopUpdateRoutine();

        string hexToken = System.BitConverter.ToString(UnityEngine.iOS.NotificationServices.deviceToken).Replace ("-",string.Empty);

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("Device HexToken" + hexToken);
        #endif

        FreakPlayerPrefs.DeviceToken = hexToken;
    }
 #endif

//    #if UNITY_ANDROID
    /// <summary>
    /// Closes all notification. in status bar
    /// </summary>
    public static void CloseAllNotification ()
    {
        #if DIRECT_BUILD || UNITY_EDITOR
            #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("CloseAllNotification");
            #endif
        #elif UNITY_ANDROID
        AndroidJavaClass pluginClass = new AndroidJavaClass(notificationClassname);
        if (pluginClass != null)
        {
            pluginClass.CallStatic ("CloseAllNotification", 0);
        }
        #elif UNITY_iOS
        #endif
    }

    public static void DisableAllNotification (bool isOn)
    {
        #if DIRECT_BUILD || UNITY_EDITOR
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("DisableAllNotification " + isOn);
            #endif
        #elif UNITY_ANDROID
        using (AndroidJavaClass _class = new AndroidJavaClass(notificationClassname)) {
            _class.CallStatic("DisableNotification", isOn ? 1 : 0);
        }
        #elif UNITY_iOS
        DisableNotificationiOS(isOn);
        #endif
    }

    public static void MuteAllNotification (bool isOn)
    {
    #if UNITY_EDITOR
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("MuteAllNotification " + isOn);
        #endif
    #else

    #if !DIRECT_BUILD
        
        #if UNITY_ANDROID
        using (AndroidJavaClass _class = new AndroidJavaClass(notificationClassname)) {
        _class.CallStatic("MuteAllNotification", isOn ? 1 : 0);
        }
        #elif UNITY_iOS
        MuteAllNotificationiOS(isOn);
        #endif
    #endif

    #endif
    }

    public static void MuteNotification (string listString)
    {
    #if UNITY_EDITOR
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("MuteAllNotification " + listString);
        #endif

    #else

    #if !DIRECT_BUILD

        #if UNITY_ANDROID
        using (AndroidJavaClass _class = new AndroidJavaClass(notificationClassname)) {
            _class.CallStatic("MuteNotification", listString);
        }
        #elif UNITY_iOS
        MuteNotificationiOS(listString);
        #endif
    #endif

    #endif
    }

    public void GetFCMNotificationToken ()
    {
        #if DIRECT_BUILD || UNITY_EDITOR
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetFCMNotificationToken");
            #endif
        #elif UNITY_ANDROID
        using (AndroidJavaClass _class = new AndroidJavaClass(notificationClassname)) {
            _class.CallStatic ("GetFCMNotificationToken", "FCM");
        }
        #elif UNITY_iOS
        #endif
    }

    public void GetFCM_NotificationToken(string token)
    {
        Debug.Log ("NotificationToken " + token);

        #if DIRECT_BUILD || UNITY_EDITOR
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("NotificationToken " + token);
            #endif
        #elif UNITY_ANDROID
        FreakPlayerPrefs.DeviceToken = token;
        #elif UNITY_iOS
        #endif
    }

    public static void GetNotificationChannelUrl ()
    {
    #if UNITY_ANDROID && !DIRECT_BUILD
        using (AndroidJavaClass _class = new AndroidJavaClass(notificationClassname)) {
        _class.CallStatic("GetNotificationChatChannel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
    #elif UNITY_IOS || UNITY_IPHONE
    GetNotificationChatChanneliOS();
    #endif
    }

    #if UNITY_IOS || UNITY_IPHONE
    public static void SetLocalNotification (int id, string title, string message, string sender, string messageID, bool sound)
    {

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("SetLocalNotification " + message);
        #endif
//        SetLocalNotificationiOS(id, title, message, sender, messageID, sound);

        if (UnityEngine.iOS.NotificationServices.localNotificationCount > 0)
        {
            UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
        }

        IDictionary _dict = new Dictionary<string,string>();
        _dict.Add("sender", sender);
        _dict.Add("message", message);
        _dict.Add("title", title);
        _dict.Add("messageID", messageID);
        _dict.Add("sound", sound.ToString());

        UnityEngine.iOS.LocalNotification _notification = new UnityEngine.iOS.LocalNotification();
        _notification.alertAction = message;
        _notification.alertBody = title;
//        _notification.alertLaunchImage = "";
//        _notification.applicationIconBadgeNumber = "";
        _notification.fireDate = System.DateTime.Now.AddSeconds(0.2);
        _notification.hasAction = true;
//        _notification.repeatCalendar = "";
//        _notification.repeatInterval = "";
        _notification.soundName = "";
//        _notification.timeZone = "";
        _notification.userInfo = _dict;

        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(_notification);
    }
    #endif

//    #endif

    #region CALL FROM NATIVE
    public void LocalNotification(string notficationID)
    {
        Debug.Log ("LocalNotificatin-------->>>>> " + notficationID);
        Freak.Chat.ChatManager.Instance.OnReceiveNotification(long.Parse(notficationID));
    }
    #endregion
}
