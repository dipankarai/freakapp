﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MiniJSON;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Xml; 
using System.Xml.Serialization;  
using System.Linq;


public class AddProfileImageAPIController : MonoBehaviour {

	public ImageController imageController;

	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void SaveUserInfoAPICall(string username, string secondaryName, string gender, int profilePicStatus = 0, int coverPicStatus = 0, int wallpaperStatus = 0, string userStatus = null, string titleId = null)
	{
        FreakAppManager.Instance.username = username;
        FreakAppManager.Instance.secondaryUsername = secondaryName;
		string encodedName = EncoedeString (username);
		string encodedsecondaryName = EncoedeString (secondaryName);
		string encodedUserStatus = EncoedeString (userStatus);

		FreakApi api = new FreakApi ("user.saveUserInfo", ServerResponseForSaveUserInfoAPICall);
		api.param ("userId", FreakAppManager.Instance.userID);
		api.param ("name", encodedName);
		api.param ("country", FreakAppManager.Instance.countryId);
		api.param ("contact_number", FreakAppManager.Instance.mobileNumber);
		api.param ("secondary_name", encodedsecondaryName);
		api.param ("gender", gender);

		if (!string.IsNullOrEmpty (userStatus)) 
		{
			api.param ("status_message", encodedUserStatus);
		}
		if (!string.IsNullOrEmpty (titleId)) 
		{
			api.param ("score_title_id", titleId);
		}

		api.param ("remove_profile_pic", profilePicStatus);
		api.param ("remove_cover_pic", coverPicStatus);
		api.param ("remove_wallpaper", wallpaperStatus);
		api.get (this);

	}


	void ServerResponseForSaveUserInfoAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						 * Successfull Login... Load Game..
						*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
                    SaveFreakPlayerInfo ();
				break;
			case 207:
				/**
						* User email id Does not Exists.. U need to signIn first...
			 			*/
				break;
			default:
				/**
						 * Show Popup to let user know if anything has gone wrong while login in...
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

//	public bool HasSpecialChars(string yourString)
//	{
//		//Debug.Log ("HasSpecialChars yourString -------->>"+ yourString); 
//		if(!String.IsNullOrEmpty(yourString)){
//			return yourString.Any (ch => !Char.IsLetterOrDigit (ch));
//		} else {
//			return false;
//		}
//	}

	public string EncoedeString(string inputstring)
	{
		return WWW.EscapeURL (inputstring);
	}



	public void UpdateProfilePicture(byte[] bytes, Texture2D texture = null)
	{
		StartCoroutine (AddProfileImageAPICall (bytes, texture));
	}


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	IEnumerator AddProfileImageAPICall(byte[] bytes, Texture2D texture = null)
	{
		Debug.Log("NEW Image Uploaded");
		// Encode texture into PNG
		//Texture2D tex =  ConvertSpriteToTexture(avatarImage);
		WWWForm form = new WWWForm();
		form.AddField("methodName", "user.uploadProfilePicture" );
		form.AddField("userId", FreakAppManager.Instance.userID );
		form.AddField("accessToken", FreakAppManager.Instance.APIkey);
		form.AddField ("applicationKey", "12345");
		form.AddBinaryData ("avatar", bytes);

        WWW postRequest = new WWW(FreakAppManager.baseURL, form);

        yield return postRequest;

        while (postRequest.isDone == false)
        {
            Debug.Log("postRequest.text NOT DONE");
            yield return null;
        }

        if (postRequest.isDone)
        {
            Debug.Log("postRequest.text " + postRequest.text);
        }

		IDictionary resultDict = (IDictionary)Json.Deserialize(postRequest.text);
		IDictionary newsFeedDetails = (IDictionary)resultDict ["responseMsg"];

		string imageUrl = newsFeedDetails ["image_url"].ToString();

        FreakAppManager.Instance.profileLink = imageUrl;

        string _filePath = Freak.StreamManager.LoadFilePath("FreakAppManager.Instance.myUserProfile.avatar_url.png", Freak.FolderLocation.Profile, false);
        string _filePathHD = Freak.StreamManager.LoadFilePath("FreakAppManager.Instance.myUserProfile.avatar_url.png", Freak.FolderLocation.ProfileHD, false);

        if (System.IO.File.Exists(_filePath))
        {
            Freak.StreamManager.RenameFile(_filePath, System.IO.Path.GetFileName(imageUrl), Freak.FolderLocation.Profile);
        }
        else
        {
            Freak.DownloadManager.Instance.DownloadProfile(imageUrl, Freak.FolderLocation.Profile, null);
        }

        if (System.IO.File.Exists(_filePathHD))
        {
            Freak.StreamManager.RenameFile(_filePathHD, System.IO.Path.GetFileName(imageUrl), Freak.FolderLocation.ProfileHD);
        }
        else
        {
            Freak.DownloadManager.Instance.DownloadProfile(imageUrl, Freak.FolderLocation.ProfileHD, null);
        }

		//imageController.GetImageFromLocalOrDownloadImage (imageUrl, img, Freak.FolderLocation.Profile);
        Debug.Log("Image Uploaded----->"+ postRequest.text);
		if (texture != null) {
//			FreakAppManager.Instance.myUserProfile.userPic = texture;
		}

        FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();
        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
	}


	public void UpdateCoverProfilePicture(byte[] bytes, Texture2D texture = null)
	{
		StartCoroutine (AddProfileCoverImageAPICall (bytes, texture));
	}

	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	IEnumerator AddProfileCoverImageAPICall(byte[] bytes, Texture2D texture = null)
	{
		Debug.Log("NEW Image Uploaded");
		// Encode texture into PNG
		//Texture2D tex =  ConvertSpriteToTexture(avatarImage);
		WWWForm form = new WWWForm();
		form.AddField("methodName", "user.uploadCoverImage" );
		form.AddField("userId", FreakAppManager.Instance.userID );
		form.AddField("accessToken", FreakAppManager.Instance.APIkey);
		form.AddField ("applicationKey", "12345");
		form.AddBinaryData ("cover_image", bytes);

		WWW postRequest = new WWW( FreakAppManager.baseURL, form );
		yield return postRequest;

		Debug.Log("Image Uploaded----->"+ postRequest.text);
		IDictionary resultDict = (IDictionary)Json.Deserialize(postRequest.text);
		IDictionary newsFeedDetails = (IDictionary)resultDict ["responseMsg"];

		string imageUrl = newsFeedDetails ["image_url"].ToString();
		
        FreakAppManager.Instance.myUserProfile.cover_image = imageUrl;

        string _filePath = Freak.StreamManager.LoadFilePath("FreakAppManager.Instance.myUserProfile.cover_image.png", Freak.FolderLocation.Profile, false);
        if (System.IO.File.Exists(_filePath))
        {
            Freak.StreamManager.RenameFile(_filePath, System.IO.Path.GetFileName(imageUrl), Freak.FolderLocation.Profile);
        }
        else
        {
            Freak.DownloadManager.Instance.DownLoadWallpaper(imageUrl, Freak.FolderLocation.Profile, null);
        }

//		if (texture != null) {
//			FreakAppManager.Instance.myUserProfile.userCoverPic = texture;
//		}
		//imageController.GetImageFromLocalOrDownloadImage (imageUrl, img, Freak.FolderLocation.Profile);
		//FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();
        FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();
        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
	}


	Texture2D ConvertSpriteToTexture(Sprite sprite)
	{
		try
		{
			if (sprite.rect.width != sprite.texture.width)
			{
				Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
				Color[] colors = newText.GetPixels();
				Color[] newColors = sprite.texture.GetPixels((int)System.Math.Ceiling(sprite.textureRect.x),
					(int)System.Math.Ceiling(sprite.textureRect.y),
					(int)System.Math.Ceiling(sprite.textureRect.width),
					(int)System.Math.Ceiling(sprite.textureRect.height));
				Debug.Log(colors.Length+"_"+ newColors.Length);
				newText.SetPixels(newColors);
				newText.Apply();
				return newText;
			}
			else
				return sprite.texture;
		}catch
		{
			return sprite.texture;
		}
	}

    public void SaveFreakPlayerInfo()
    {
        Debug.Log ("access_token ---- > "+  PlayerPrefs.GetString ("access_token"));
        FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();
    }


	public void UpdateChatWallpaper(byte[] bytes, Image img = null)
	{
		StartCoroutine (AddChatWallpaperAPICall (bytes, img));
	}

	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	IEnumerator AddChatWallpaperAPICall(byte[] bytes, Image img)
	{
		Debug.Log("NEW Wallpaper Uploaded");
		// Encode texture into PNG
		//Texture2D tex =  ConvertSpriteToTexture(avatarImage);
		WWWForm form = new WWWForm();
		form.AddField("methodName", "user.uploadWallpaper" );
		form.AddField("userId", FreakAppManager.Instance.userID );
		form.AddField("accessToken", FreakAppManager.Instance.APIkey);
		form.AddField ("applicationKey", "12345");
		form.AddBinaryData ("wallpaper_image", bytes);

		WWW postRequest = new WWW( FreakAppManager.baseURL, form );
		
        yield return postRequest;

        while (postRequest.isDone == false)
        {
            yield return null;
        }

        if (string.IsNullOrEmpty (postRequest.error))
        {
            Debug.Log(postRequest.text);
            IDictionary responseDictionay = (IDictionary)MiniJSON.Json.Deserialize(postRequest.text);
            int responseCode = int.Parse(responseDictionay["responseCode"].ToString());

            switch(responseCode)
            {
                case 001:
                    IDictionary responseMsgDictionary = (IDictionary)responseDictionay["responseMsg"];
                    string image_url = responseMsgDictionary["image_url"].ToString();

                    if (string.IsNullOrEmpty(FreakAppManager.Instance.temporaryWallpaperName) == false)
                    {
                        string filePath = Freak.StreamManager.LoadFilePath(FreakAppManager.Instance.temporaryWallpaperName, Freak.FolderLocation.Wallpaper, false);
                        string newFilename = System.IO.Path.GetFileName(image_url);

                        Freak.StreamManager.RenameFile(filePath, newFilename, Freak.FolderLocation.Wallpaper);

                        FreakAppManager.Instance.temporaryWallpaperName = "";
                    }

                    FreakAppManager.Instance.myUserProfile.wallpaper_image = image_url;
                    FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;

                    break;
                case 207:
                    break;
                default:
                    break;
            }   
        }
        else
        {
            Debug.LogError(postRequest.error);
        }

//		Debug.Log("Image Uploaded----->"+ postRequest.text);
//		IDictionary resultDict = (IDictionary)Json.Deserialize(postRequest.text);
//		IDictionary newsFeedDetails = (IDictionary)resultDict ["responseMsg"];
//
//		string imageUrl = newsFeedDetails ["image_url"].ToString();
//		FreakAppManager.Instance.profileCoverLink = imageUrl;
//
//		imageController.GetImageFromLocalOrDownloadImage (imageUrl, img, Freak.FolderLocation.Profile);
		//FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();

	}


}
