﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Freak;

public class UserAuthentication : MonoBehaviour {

	public string saveduserName;
	public int countryId;
	public string mobileNumber;
	public ConnectingUI loadingObj;

	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void UserLogin(string user_Name, int country_ID, string mobile_numb)
	{
		countryId = country_ID;
		mobileNumber = mobile_numb;
		if (mobileNumber.Length > 10) 
		{
			mobileNumber = mobileNumber.Substring (mobileNumber.Length - 10);
		}

		Debug.Log ("user login dataaa --- >" + countryId + " number ------>" + mobileNumber);
		FreakApi api  = new FreakApi("user.login",ServerResponseForLogintoServer);
		api.param("country", countryId);
		api.param("contact_number",mobileNumber);

		api.get(this);
	}
		
			
	void ServerResponseForLogintoServer(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		loadingObj.gameObject.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode){
                case 001:
				/**
				 * Successfull Login... Load Game..
				*/
                    IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
                    int ServeruserId = int.Parse (string.Format ("{0}", userDetailsDict ["user_id"]));

                   #if UNITY_EDITOR || AUTO_OTP
                    FreakAppManager.Instance.otp = string.Format ("{0}", userDetailsDict ["otp"]);
                    #endif

                    FreakAppManager.Instance.userID = ServeruserId;
                    FreakAppManager.Instance.mobileNumber = mobileNumber;
                    FreakAppManager.Instance.countryId = countryId;

//                  Freak.Chat.ChatManager.Instance.FirstTimeLoginToSendbird (ServeruserId.ToString ());

				//string _name = (string)userDetailsDict["name"];
				/** 
				 * user id & accessToken which is given by the server is saved into the playerprefs...
				*/
//				TestOTPAPI (LoadOtpScreen);
				LoadOtpScreen("Otp");
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else 
		{
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
			if (InternetConnection.Check ()) 
			{
                AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.networkIssue);
			} 
			else 
			{
				Debug.LogError (" 5555555555555555555 No Internet Connection!");
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
			}
		}  
	}


	void LoadOtpScreen(string callback)
	{
		if(!string.IsNullOrEmpty(callback))
		FindObjectOfType<UIPanelController> ().PushPanel (UIPanelController.PanelType.OTPVerificationPanel);
	}

	System.Action<string> serverCallback;
	public void FilePickerCallback (string filePath)
	{
		if (serverCallback != null)
		{
			serverCallback(filePath);
			serverCallback = null;
		}
	}



	public void OnClickResendOtp()
	{
		ResendOtpForUserLogin ("", FreakAppManager.Instance.countryId, FreakAppManager.Instance.mobileNumber);
	}
	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	void ResendOtpForUserLogin(string user_Name, int country_ID, string mobile_numb)
	{
		loadingObj.gameObject.SetActive (true);
		Debug.Log ("user login dataaa --- >" + countryId + " number ------>" + mobileNumber);
		FreakApi api  = new FreakApi("user.login",ServerResponseForResendOtpForUserLogin);
		api.param("country", countryId);
		api.param("contact_number",mobileNumber);

		api.get(this);
	}


	void ServerResponseForResendOtpForUserLogin(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		loadingObj.gameObject.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				int ServeruserId = int.Parse (string.Format ("{0}", userDetailsDict ["user_id"]));

				//FreakAppManager.Instance.userID = ServeruserId;
				//FreakAppManager.Instance.mobileNumber = mobileNumber;
				//FreakAppManager.Instance.countryId = countryId;

				//                    Freak.Chat.ChatManager.Instance.FirstTimeLoginToSendbird (ServeruserId.ToString ());

				//string _name = (string)userDetailsDict["name"];
				/** 
				 * user id & accessToken which is given by the server is saved into the playerprefs...
				*/
				//				TestOTPAPI (LoadOtpScreen);
				//LoadOtpScreen("Otp");
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
			if (InternetConnection.Check ()) 
			{
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.networkIssue);
			} 
			else 
			{
				Debug.LogError (" 5555555555555555555 No Internet Connection!");
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
			}
		}  
	}


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	void TestOTPAPI(System.Action<string> onDone = null)
	{
		serverCallback = onDone;
		Debug.Log ("user login dataaa --- >" + countryId + " number ------>" + mobileNumber);
		FreakApi api  = new FreakApi("test.otpList",ServerResponseForTestOTPAPI);
		api.param("user_id", FreakAppManager.Instance.userID);

		api.get(this);
	}


	void ServerResponseForTestOTPAPI(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				List<object> userDetailsList = (List<object>)output ["responseMsg"];
				for (int i = 0; i < userDetailsList.Count; i++) {
					Dictionary<string,object> data = (Dictionary<string,object>)userDetailsList [i];
					string otp = data ["otp"].ToString ();
					FreakAppManager.Instance.otp = otp;
					Debug.Log ("otp ------ > " + otp);
				}
				//List<object> obj = (List<object>)userDetailsDict["responseMsg"];
				FilePickerCallback (FreakAppManager.Instance.otp);
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
			AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.serverError);
		}  
	}


	public bool HasAlredayRegistered()
	{
        if (FreakAppManager.Instance.userInfoPlayerPrefs != null)
        {
            return true;
        }

        /*if (PlayerPrefs.HasKey (FreakAppConstantPara.PlayerPrefsName.LoginUserId))
        {
            FreakAppManager.Instance.userID = int.Parse(PlayerPrefs.GetString (FreakAppConstantPara.PlayerPrefsName.LoginUserId));
            FreakAppManager.Instance.APIkey = PlayerPrefs.GetString (FreakAppConstantPara.PlayerPrefsName.LoginAccessToken);
            FreakAppManager.Instance.mobileNumber = PlayerPrefs.GetString(FreakAppConstantPara.PlayerPrefsName.UserMobileNumber);

			return true;
		}*/
        
		return false;
	}
}
