﻿using UnityEngine;
using System.Collections;

public class AddUserApiManager : MonoBehaviour {


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void AddUserAPICall(string contactNumb)
	{
		FreakApi api  = new FreakApi("contact.addContact",ServerResponseForAddUserAPICall);
		api.param("userId",FreakAppManager.Instance.userID);
		api.param("contact_number",contactNumb);

		api.get(this);
	}


	void ServerResponseForAddUserAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				//string APIKey = (string)userDetailsDict ["access_token"];
				//int ServeruserId = int.Parse (string.Format ("{0}", userDetailsDict ["user_id"]));
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
}
