﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak
{
    public class FreakServer : MonoBehaviour
    {
        #region INIT INSTANCE
        private static FreakServer instance;
        public static FreakServer Instance
        {
            get { return InitInstance(); }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public static FreakServer InitInstance()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("FreakServer");
                instance = instGO.AddComponent<FreakServer>();
            }
            return instance;
        }

        #endregion

        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
        }

        // Use this for initialization
        void Start ()
        {
            
        }

        #region gameHistory.get

        private System.Action<List<GameStatus>> GetGameHistoryAPICallbackHandler;
        public List<GameStatus> gameHistoryStatusList = new List<GameStatus>();
        private int lastStatusId = 0;
        private int maxLimit = 10;

        public void GetGameHistoryAPICall(System.Action<List<GameStatus>> _onDone = null)
        {
            GetGameHistoryAPICallbackHandler = _onDone;

            FreakApi api = new FreakApi ("gameHistory.get", ServerResponseForGetGameHistroyAPICall);
            api.param ("last_game_history_id", lastStatusId);
            api.param ("limit", maxLimit);
            api.get (this);
        }

        void ServerResponseForGetGameHistroyAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
        {
            Debug.Log("ServerResponseForGetGameHistroyAPICall " + MiniJSON.Json.Serialize(output));
            // check for errors
            if (output != null)
            {
                string responseInfo = (string)output["responseInfo"];

                switch (responseCode)
                {
                    case 001:
                        IDictionary gameHistoryDetails = (IDictionary)output["responseMsg"];
                        List<object> gameHistoryList = (List<object>)gameHistoryDetails["gameHistoryList"];

                        if (gameHistoryList != null)
                        {
                            gameHistoryStatusList = new List<GameStatus>();

                            for (int i = 0; i < gameHistoryList.Count; i++)
                            {
                                Dictionary<string,object> feeddata = (Dictionary<string,object>)gameHistoryList[i];
                                GameStatus feeditem = new GameStatus(feeddata);

                                gameHistoryStatusList.Add(feeditem);
                            }

                            UpdateGameHistoryPlayerPrefs();
                        }

                        if (GetGameHistoryAPICallbackHandler != null)
                        {
                            GetGameHistoryAPICallbackHandler(gameHistoryStatusList);
                        }

                        break;
                    case 207:
                        break;
                    default:
                        break;
                }   
            }
            else
            {
                Debug.Log("WWW Error from Server: " + MiniJSON.Json.Serialize(output) + " " + text);
            }
        }

        void UpdateGameHistoryPlayerPrefs ()
        {
            FreakPlayerPrefs.GetGameHistoryStatusList = gameHistoryStatusList;
        }

        #endregion gameHistory.get
    }
}
