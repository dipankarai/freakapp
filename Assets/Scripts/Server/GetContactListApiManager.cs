﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class GetContactListApiManager : MonoBehaviour {

	public List<ServerUserContact> serverContactList = new List<ServerUserContact>();
	public List<ServerUserContact> tempserverContactList = new List<ServerUserContact>();

	static System.Action<List<ServerUserContact>> onDone;

	void Start(){
	}

    /// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetContactListAPICall(string contactNumb, System.Action<List<ServerUserContact>> _onDone = null)
	{
		onDone = _onDone;
		FreakApi api  = new FreakApi("contact.listContact",ServerResponseForGetContactListAPICall);
		api.param("userId",FreakAppManager.Instance.userID);
		api.param("accessToken",FreakAppManager.Instance.APIkey);

		api.get(this);
	}


	void ServerResponseForGetContactListAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			serverContactList.Clear ();
			tempserverContactList.Clear ();
            switch(responseCode)
			{
                case 001:
				/**
				 * Successfull Login... Load Game..
				*/
                    List<object> userDetailsList = (List<object>)output ["responseMsg"];
                    for (int i = 0; i < userDetailsList.Count; i++) 
                    {
                        Dictionary<string, object> data = (Dictionary<string,object>)userDetailsList [i];
                        ServerUserContact newContact = new ServerUserContact (data);
                        if (!newContact.contact_number.Equals (FreakAppManager.Instance.mobileNumber)) 
                        {
                            serverContactList.Add (newContact);
                            tempserverContactList.Add (newContact);
                        }
                    }
				OnLoadedContacts ();
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

	void OnLoadedContacts()
	{
		if (serverContactList.Count > 0)
        {
            FreakAppManager.Instance.SetAllServerUserInfo(serverContactList);
        }

		if (onDone != null)
		{
			onDone (serverContactList);
		}
	}

}

[System.Serializable]
public class ServerUserContact
{
	public int user_contact_id;
	public int user_id;
    public int totalScore;
    public string name = string.Empty;
    public string status_message = string.Empty;
    public string contact_number = string.Empty;
    public string avatar = string.Empty;

    public string secondary_name = string.Empty;
    public string avatar_name = string.Empty;
    public string gender = "Male";

    [Newtonsoft.Json.JsonIgnore]
    public byte[] imageCache;
    [Newtonsoft.Json.JsonIgnore]
	public Texture2D userImage;
	public bool isSynchedContact;

    //Chat Group
    [Newtonsoft.Json.JsonIgnore]
    public Freak.Chat.ChatChannel groupChannel;

	public bool isSelectedForGroup;

    public ServerUserContact()
    {

    }

	public ServerUserContact(Dictionary<string,object> data)
	{
//        Debug.Log("ServerUserContact " + MiniJSON.Json.Serialize(data));

		user_contact_id     = int.Parse(data["user_contact_id"].ToString());
		user_id             = int.Parse(data["user_id"].ToString());
		name                = DecodeString( data["name"].ToString());
		avatar              = data["avatar"].ToString(); 
		contact_number      = data["contact_number"].ToString(); 
		
        if (data.ContainsKey("status_message")) 
		{
			status_message = DecodeString( data ["status_message"].ToString ());
		}
	}

	string DecodeString(string s)
	{
        string decodeString = s;
        try
        {
            decodeString = WWW.UnEscapeURL(s);
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message + " Decoding " + s);
        }

        return decodeString;
	}
}