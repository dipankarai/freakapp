﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ConnectingUI : MonoBehaviour {

	private string initialText = "              Loading";
	int count = 0;
	public Text textField;
	public GameObject eyeObject;
	private LTDescr _LTDescr;


	public NativeEditBox AttachedNativeText;
	void Start(){
		//textField.text = initialText;
	}

	void RotateEye()
	{
		_LTDescr = LeanTween.rotateAround (eyeObject, Vector3.forward, 360f, 0.6f );
		_LTDescr.setEase (LeanTweenType.linear);
	}

	void TweenObj()
	{
		_LTDescr = LeanTween.moveLocalX (eyeObject, 18f, 0.5f);
		_LTDescr.setEase (LeanTweenType.linear);
		_LTDescr.setOnComplete(TweenObjBack);
	}

	void TweenObjBack()
	{
		_LTDescr = LeanTween.moveLocalX (eyeObject, -26f, 0.5f);
		_LTDescr.setEase (LeanTweenType.linear);
		_LTDescr.setOnComplete(TweenObj);
	}

    private string message = string.Empty;

	public void ActivateNativetext()
	{
		if (AttachedNativeText) {
			
			AttachedNativeText.SetEnabled (true);
		}
	}

	public void DeActivateNativetext()
	{
		if (AttachedNativeText)
			AttachedNativeText.SetEnabled (false);
	}

    public void DisplayLoading(string msg = "")
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("********************* DisplayLoading LOADER");
        #endif
        //TweenObj ();
        //RotateEye();
        if (string.IsNullOrEmpty(msg))
        {
            message = initialText;
        }
        else
        {
            message = msg;
        }
            
        textField.text = message;
        this.gameObject.SetActive (true);
        textField.gameObject.SetActive (true);
        mWaitForSomeTimeRoutine = StartCoroutine (WaitForSomeTime ());
	}

    public void HideLoading ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("********************* HIDELOADER");
        #endif

        if (mWaitForSomeTimeRoutine != null)
        {
            StopCoroutine(mWaitForSomeTimeRoutine);
        }

        DisableThisObject();
    }

    public void HideAfterFewSecondsLoading (float delay)
    {
        Invoke("DisableThisObject", delay);
    }

    void DisableThisObject ()
    {
        this.gameObject.SetActive (false);
    }

    private Coroutine mWaitForSomeTimeRoutine;
    IEnumerator WaitForSomeTime()
	{
        /*textField.text = "";
        textField.text = message + " .";
		yield return new WaitForSeconds (0.5f);
        textField.text = message + " . " + ".";
		yield return new WaitForSeconds(0.5f);
        textField.text = message + " . " + ". " + ".";
		yield return new WaitForSeconds(0.5f);
        DisplayLoading (message);*/

        textField.text = string.Empty;
        textField.text = "            Loading .";
        yield return new WaitForSeconds (0.5f);
        textField.text = "            Loading . " + ".";
        yield return new WaitForSeconds(0.5f);
        textField.text = "            Loading . " + ". " + ".";
        yield return new WaitForSeconds(0.5f);

        if (InternetConnection.Check() == false)
        {
            if (FindObjectOfType<Freak.AlertMessege> () != null)
            {
				FindObjectOfType<Freak.AlertMessege> ().ShowAlertWindow (Freak.AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
				if (AttachedNativeText)
					AttachedNativeText.SetEnabled (true);
                this.gameObject.SetActive (false);
            }
            else
            {
                DisplayLoading (message);
            }
        }
        else
        {
            DisplayLoading (message);
		}

	}

	void OnDisable(){
        if(textField != null)
            textField.text = string.Empty;
	}

    #region SendBird Loader
    private System.Action loaderCallback;
    public void WaitForSendBirdLogin (System.Action callback)
    {
        Debug.Log ("WaitForSendBirdLogin");
        this.gameObject.SetActive (true);
        textField.gameObject.SetActive (true);

        LoadLoader ();
    }

    void LoadLoader ()
    {
        Debug.Log ("LoadLoader 1");
        if (this.gameObject.activeInHierarchy == false)
        {
            Debug.Log ("LoadLoader 2");
            this.gameObject.SetActive (true);
            textField.gameObject.SetActive (true);
            Invoke ("LoadLoader", 0.1f);
            return;
        }
        else
        {
            Debug.Log ("LoadLoader 3");
        }

        Debug.Log ("LoadLoader 4");

        mWaitForSomeTimeRoutine = StartCoroutine (WaitForSomeTime ());
        StartCoroutine (WaitForSendBirdLoginCoroutine());
    }

    IEnumerator WaitForSendBirdLoginCoroutine ()
    {
        Debug.Log ("WaitForSendBirdLoginCoroutine");
        while(Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
        {
            yield return null;
        }

        if (loaderCallback != null)
            loaderCallback ();
    }
    #endregion
}
