﻿using UnityEngine;
using System.Collections;

namespace Freak.Game
{
    public class FreakGameCommunicator
    {
        #region ANDROID BRIDGE
        private static string fullClassName = "in.Freak.FreakGame.LoadFreakGame";
        private static string mainActivityClassName = "com.astricstore.imageandvideopicker.OverrideUnity";

        public static void SwitchToGame (string gameName, int gameType, string data)
        {
            Debug.Log("SwitchToGame ------- > " + gameName + "gameType: " + gameType);

            AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
            if (pluginClass != null)
            {
                pluginClass.CallStatic("LoadFreakGameActivity", gameName, gameType, data, mainActivityClassName);
            }
        }

        #endregion
    }
}
