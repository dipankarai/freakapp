﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class MyTitlesUIController : MonoBehaviour, ITableViewDataSource  {

	public MyTitleComponent m_cellPrefab;
	public TableView m_tableView;
	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;
	public Text noTitlesText;

	void OnEnable()
	{
		if (FreakAppManager.Instance.myUserProfile.userTitles.Count > 0) {
            noTitlesText.text = string.Empty;
		} 
		else {
			noTitlesText.text = "No Titles Earned.";
		}
		EnableTableView ();
	}


	public void EnableTableView()
	{
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}

	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}
		

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return FreakAppManager.Instance.myUserProfile.userTitles.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		MyTitleComponent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MyTitleComponent;
		if (cell == null) {
			cell = (MyTitleComponent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}


	void AssignDetails(MyTitleComponent cell, int rowNumb)
	{
		UserTitles titles = FreakAppManager.Instance.myUserProfile.userTitles [rowNumb];
		cell.nameText.text = titles.userTitle;
		cell.iD = titles.userTitleID;
		cell.rowNumber = rowNumb;
		//imageController.GetImageFromLocalOrDownloadImage (stickers.description_imagePath, cell.cellImage);
	}


	#endregion
	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
	


