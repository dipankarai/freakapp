﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using SendBird;
//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
//containing this component as a cell in a TableView
using System.Text;
using System.IO;

public class MyTitleComponent : TableViewCell
{
	public Image img;
	public Text nameText;
	public Slider m_cellHeightSlider;
	public int rowNumber { get; set; }
	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	public string rowContent { get; set; }
	public string iD{ get; set; }

	public void SliderValueChanged(Slider slider) {
		float value = slider.value;
		Debug.Log ("Value --- >" + value);
		onCellHeightChanged.Invoke(rowNumber, value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}


	public void OnClickAddTitle()
	{
		FreakAppManager.Instance.myUserProfile.currentTile.userTitle = nameText.text;
		FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender, 0, 0, 0, "", iD);
	}


}
