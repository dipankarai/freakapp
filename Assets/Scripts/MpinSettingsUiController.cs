﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;

public class MpinSettingsUiController : MonoBehaviour {

	public Toggle mPinToggle;
	// Use this for initialization
	void Start () {
		mPinToggle.isOn = SettingsManager.Instance.isMPinLock;
	}
	
	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}

	public void OnClickChangePin()
	{
//        if (PlayerPrefs.HasKey (FreakAppConstantPara.PlayerPrefsName.MPin)) {
//			Debug.Log (" ****** Mpin Already Saved");
//			UIController.Instance.mPinPanel.SetActive (true);
//
//		} else {
			Debug.Log (" ****** Change Mpin");
			FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.MPINDtailsEnterPanel);
//		}
	}

	public void onClickMpinLock(Toggle toggle)
	{
        if (PlayerPrefs.HasKey (FreakAppConstantPara.PlayerPrefsName.MPin))
		{
			
			SettingsManager.Instance.isMPinLock = toggle.isOn;

		} else {
			FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.MPINDtailsEnterPanel);
            PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.MPinLock, "false");
		}
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
