﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak;

public class SettingsUIController : MonoBehaviour {

	HomeScreenUIPanelController homeScreenUIPanelController;

	public Toggle muteChatToggle;
	public Toggle muteGameToggle;
	public Toggle notificationControlToggle;
	public Toggle buzzControlToggle;
	public Toggle disableFreaksToggle;

	public Text nameTitleText;
	public ConnectingUI connectingUIText;


	void Start()
	{
		
		homeScreenUIPanelController = FindObjectOfType<HomeScreenUIPanelController> ();
//      muteChatToggle.isOn = SettingsManager.Instance.isMuteChat;
        muteChatToggle.isOn = FreakPlayerPrefs.MuteAllChat;
		muteGameToggle.isOn = SettingsManager.Instance.isMuteGame;
        notificationControlToggle.isOn = FreakPlayerPrefs.AllNotification;
		buzzControlToggle.isOn = SettingsManager.Instance.isBuzzControl;
		disableFreaksToggle.isOn = SettingsManager.Instance.isDisableFreaks;
		//Debug.Log ("MyTitle ----->" + FreakAppManager.Instance.myUserProfile.currentTile.userTitle);
		if (FreakAppManager.Instance.myUserProfile.currentTile != null) {
			nameTitleText.text = FreakAppManager.Instance.myUserProfile.currentTile.userTitle;
		}
	}


	public void OnClickBack()
	{
		homeScreenUIPanelController.PopPanel ();
	}

	public void OnClickSetWallpaper()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.SetWallpaperPanel);
	}
		
	public void OnClickPrivacySettings()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.PrivacySettingsPanel);
	}

	public void OnClickPrivacyPolicy()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.PrivacyPolicyPanel);
	}

	public void OnClickNameTitles()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.MyTitlesPanel);
	}

	public void OnClickBlockList()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.BlockedUsersSettingPanel);
	}

	public void OnClickRefreshContacts()
	{
		connectingUIText.gameObject.SetActive (true);
		connectingUIText.DisplayLoading ();
		FindObjectOfType<SyncContactsToServer> ().SyncContacts (connectingUIText.gameObject);

	}

	#region Toggle Buttons
	public void OnClickMuteChat(Toggle toggleBtn)
	{
		if (toggleBtn.isOn) {
			Debug.Log ("OnClickMuteChat ONNNNNNN -- >");
		} 
		else {
			Debug.Log ("OnClickMuteChat Offfffff -- >");
		}

//		SettingsManager.Instance.isMuteChat = toggleBtn.isOn;
        FreakPlayerPrefs.MuteAllChat = toggleBtn.isOn;
        #if !UNITY_EDITOR
        PushNotification.MuteAllNotification(FreakPlayerPrefs.MuteAllChat);
        #endif
	}

	public void OnClickMuteGame(Toggle toggleBtn)
	{
		if (toggleBtn.isOn) {
			Debug.Log ("OnClickMuteChat ONNNNNNN -- >");
		} 
		else {
			Debug.Log ("OnClickMuteChat Offfffff -- >");
		}
		SettingsManager.Instance.isMuteGame = toggleBtn.isOn;
	}

	public void OnClickNotificationControl(Toggle toggleBtn)
	{
		if (toggleBtn.isOn) {
			Debug.Log ("OnClickMuteChat ONNNNNNN -- >");
        } 
        else {
            Debug.Log ("OnClickMuteChat Offfffff -- >");
        }

        FreakPlayerPrefs.AllNotification = toggleBtn.isOn;
        #if !UNITY_EDITOR
        PushNotification.DisableAllNotification(FreakPlayerPrefs.AllNotification == false);
        #endif
	}

	public void OnClickBuzzControl(Toggle toggleBtn)
	{
		if (toggleBtn.isOn) {
			Debug.Log ("OnClickMuteChat ONNNNNNN -- >");
		} 
		else {
			Debug.Log ("OnClickMuteChat Offfffff -- >");
		}
		SettingsManager.Instance.isBuzzControl = toggleBtn.isOn;
	}

	public void OnClickDisableFreaks(Toggle toggleBtn)
	{
		if (toggleBtn.isOn) {
			Debug.Log ("OnClickMuteChat ONNNNNNN -- >");

		} 
		else {
			Debug.Log ("OnClickMuteChat Offfffff -- >");
		}

		SettingsManager.Instance.isDisableFreaks = toggleBtn.isOn;
	}
	#endregion

	void OnDisable()
	{
        Freak.Chat.ChatManager.Instance.SetPushNotification (FreakPlayerPrefs.AllNotification);
		Destroy (this.gameObject);
	}
}
