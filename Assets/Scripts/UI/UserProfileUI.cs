﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;
using System.Runtime.InteropServices;

public class UserProfileUI : MonoBehaviour {

	public Image profilePicImg;

	public GameObject editStatusPopup;
	public InputField statusInputTextField;
    public HelpPopupPanel helpPopupPanel;
    public InfoPopupPanel infoPopupPanel;
	NativeEditBox statusNativeTextField;

	public Text nameText;
	public Text secondaryNameText;

	public Text letterCountText;
	public Text statusText;

	public Text friendsCountText;
	public Text titleText;

	private Texture2D texture;

	public GameObject selectedProfilePicImgObj;
	public Image selectedProfilePicImg;

	public GameObject optionsToAddPhoto;
	//public GameObject editPicStatusObject;
	//public NativeEditBox picStatusTextField;

	public GameObject picStatusObject;
	public Text picStatusText;
	  
	public GameObject nameEditObj;
	public InputField nameEditTextField;
	NativeEditBox nameNativeTextField;

	public Texture2D newTex;

	public Sprite defaultImage;

	enum NameEditStatus{
		Defaukt,
		Name,
		GameName
	}
	NameEditStatus nameEditStatus;


	// Use this for initialization
	void OnEnable () 
	{
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
		SetCurrentImage ();
		SetDetails ();

		nameNativeTextField = nameEditTextField.gameObject.GetComponent<NativeEditBox> ();
		statusNativeTextField = statusInputTextField.gameObject.GetComponent<NativeEditBox> ();
	}


	void SetDetails()
	{
		nameText.text = FreakAppManager.Instance.myUserProfile.name;
		secondaryNameText.text =  FreakAppManager.Instance.myUserProfile.secondary_name;
		statusText.text =  FreakAppManager.Instance.myUserProfile.status_message;
//		friendsCountText.text = FreakAppManager.Instance.userSynchedContacts.Count.ToString();
        friendsCountText.text = FreakAppManager.Instance.GetAllServerUserInfo().Count.ToString();
		titleText.text = FreakAppManager.Instance.myUserProfile.userTitles.Count.ToString();

        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
	}


	void SetCurrentImage()
	{
        /*if (Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(FreakAppManager.Instance.profileLink, Freak.FolderLocation.Profile, null))
        {
            string profilePath = Freak.StreamManager.LoadFilePath(System.IO.Path.GetFileName(FreakAppManager.Instance.profileLink), Freak.FolderLocation.ProfileHD, false);
            if (System.IO.File.Exists(profilePath))
            {
                byte[] bytes = Freak.StreamManager.LoadFileDirect(profilePath);
                if (bytes != null)
                {
                    profilePicImg.FreakSetTexture(bytes);
                }
            }
        }
        else
        {
            Invoke("SetCurrentImage", 0.5f);
        }*/

        profilePicImg.FreakSetSprite(FreakAppManager.Instance.myUserProfile.userPic);
        Invoke("LoadHDImage", 0.02f);

        /*Texture2D tex = FreakAppManager.Instance.myUserProfile.userPic;
		if (tex != null) {
			Rect rec = new Rect (0, 0, tex.width, tex.height);
			Sprite spr = Sprite.Create (tex, rec, new Vector2 (0, 0), 1);
			profilePicImg.sprite = spr;
		}*/
	}

    void LoadHDImage ()
    {
        if (string.IsNullOrEmpty(FreakAppManager.Instance.myUserProfile.avatar_url) == false)
        {
            string _filename = System.IO.Path.GetFileName(FreakAppManager.Instance.myUserProfile.avatar_url);
            string _filePath = Freak.StreamManager.LoadFilePath(_filename, Freak.FolderLocation.ProfileHD, false);
            if (System.IO.File.Exists(_filePath))
            {
                Texture2D _texture = new Texture2D(1, 1);
                _texture.LoadImage (Freak.StreamManager.LoadFileDirect(_filePath));
                profilePicImg.FreakSetSprite(_texture);
                _texture = null;
            }
            else
            {
                Invoke("LoadHDImage", 0.5f);
            }
        }
    }

	public void OnClickEditStatus()
	{
		editStatusPopup.SetActive (true);
		statusNativeTextField.SetTextNative(string.Empty);
	}
		
	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel();
	}
		
	public void OnClickSave()
	{
		if (!string.IsNullOrEmpty (statusInputTextField.text)) 
		{
			FreakAppManager.Instance.myUserProfile.status_message = statusInputTextField.text;
			Debug.Log ("OnClickSave  status saved   "+FreakAppManager.Instance.myUserProfile.status_message);
			FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender, 0, 0, 0, 
			FreakAppManager.Instance.myUserProfile.status_message);
			SetDetails ();
		}
		editStatusPopup.SetActive (false);

	}

	public void OnClickCancel()
	{
		editStatusPopup.SetActive (false);
	}

	public void OnClickSettings()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.SettingsPanel);
	}

	public void OnClickInfo()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("On Click Info");
        #endif

        infoPopupPanel.OnClickOpenPanel();
	}

	public void OnClickHelp()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("On Click Help");
        #endif

        helpPopupPanel.OnClickOpenPanel();
	}

	public void OnClickRewards()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("On Click Rewards");
        #endif

	}

	public void OnClickInvite()
	{
        #if UNITY_EDITOR

        #else
		
            #if UNITY_ANDROID
    		string shareText = "Hey,\n\n\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n";
    		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
    		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

    		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
    		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
    		//AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
    		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
    		//intentObject.Call<AndroidJavaObject>("setType", "image/png");

    		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

    		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

    		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Freak Invite");
    		currentActivity.Call("startActivity", jChooser);
            #endif
		
            #if UNITY_IOS 
    		CallSocialShareAdvanced ("Hey,\n\n\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n","Invite",string.Empty,string.Empty);
            #endif
		    
		#endif

	}

	public void OnClickSelectedImage()
	{
		
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif


	public void OnClickPicture()
	{
        string _filename = System.IO.Path.GetFileName(FreakAppManager.Instance.myUserProfile.avatar_url);
        AssignImageToSelectedPic (_filename);
//      if (string.IsNullOrEmpty(FreakAppManager.Instance.profileLink) == false)
//      {
//          AssignImageToSelectedPic(System.IO.Path.GetFileName(FreakAppManager.Instance.profileLink));
//      }
        HideOptionsToChoosePicture ();
        selectedProfilePicImgObj.SetActive (true);
	}


	public void OnClickEditPicButton()
	{
        if (InternetConnection.Check() == false)
        {
            Freak.AlertMessege.Instance.ShowAlertWindow (Freak.AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
            return;
        }

		ShowOptionsToChoosePicture ();
	}


	public void OnClicDeleteProfilePicButton()
	{
		Debug.Log ("Remove profile pic");

        if (InternetConnection.Check() == false)
        {
            Freak.AlertMessege.Instance.ShowAlertWindow (Freak.AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
            return;
        }

		FreakAppManager.Instance.myUserProfile.userPic = null;
		FreakAppManager.Instance.myUserProfile.avatar_url = null;
		profilePicImg.sprite = defaultImage;
		selectedProfilePicImg.sprite = defaultImage;

		FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender, 1, 0, 0, 
		FreakAppManager.Instance.myUserProfile.status_message);
        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
		CancelUpdatePicStatus ();
	}


	void ShowOptionsToChoosePicture()
	{
		optionsToAddPhoto.SetActive (true);
		//editPicStatusObject.SetActive (true);
		picStatusObject.SetActive (false);
	}

	public void HideOptionsToChoosePicture()
	{
		optionsToAddPhoto.SetActive (false);
		//editPicStatusObject.SetActive (false);
		picStatusObject.SetActive (true);
	}

	public void UpdatePicStatus()
	{
		selectedProfilePicImgObj.SetActive (false);
		optionsToAddPhoto.SetActive (false);

	}

	public void CancelUpdatePicStatus()
	{
		selectedProfilePicImgObj.SetActive (false);
		optionsToAddPhoto.SetActive (false);
	}


	public void AddPhoto(int selectedName)
    {
        if (selectedName < 3) 
        {
        #if UNITY_EDITOR
            string _path = Application.persistentDataPath + "/group.jpg";
            if (System.IO.File.Exists(_path))
            {
            Debug.Log(_path);
            SetProfileImage(_path);
            }
            else
            {
            Debug.LogError(_path);
            }
        #else
            UIController.Instance.GetProfileImage (SetProfileImage, selectedName);
        #endif
        } 
        else {
            CancelUpdatePicStatus ();
        }
	}


	void SetProfileImage(string path)
	{
		texture = null;
		byte[] fileByte = System.IO.File.ReadAllBytes(path);
		texture = new Texture2D(1, 1);
		texture.LoadImage(fileByte);

        if (texture.height > 500 || texture.width > 500)
        {
            texture = TextureResize.ResizeTexture(500f, 500f, texture);
            fileByte = texture.EncodeToPNG();

        }

        Freak.StreamManager.SaveFile("FreakAppManager.Instance.myUserProfile.avatar_url.png", fileByte, Freak.FolderLocation.ProfileHD, null, false);
        AssignImage(texture);
        AssignImageToSelectedPic("FreakAppManager.Instance.myUserProfile.avatar_url.png");
        UpdatePicStatus ();
        FindObjectOfType<AddProfileImageAPIController>().UpdateProfilePicture(fileByte, null);
        texture = null;
	}

	void AssignImage (Texture2D tex)
	{
        if (tex != null) 
		{
            profilePicImg.FreakSetSprite(tex);

            byte[] fileByte = null;
            if (tex.height > 100 || tex.width > 100)
            {
                tex = TextureResize.ResizeTexture(100f, 100f, tex);
                fileByte = tex.EncodeToPNG();
            }

            Freak.StreamManager.SaveFile("FreakAppManager.Instance.myUserProfile.avatar_url.png", fileByte, Freak.FolderLocation.Profile, null, false);
            FreakAppManager.Instance.myUserProfile.userPic = tex;
		}
	}

    void AssignImageToSelectedPic (string filename)
    {
        string _filePath = Freak.StreamManager.LoadFilePath(filename, Freak.FolderLocation.ProfileHD, false);
        if (System.IO.File.Exists(_filePath) == false)
        {
            _filePath = Freak.StreamManager.LoadFilePath(filename, Freak.FolderLocation.Profile, false);
        }

        Debug.Log(_filePath);

        byte[] fileByte = System.IO.File.ReadAllBytes(_filePath);
        selectedProfilePicImg.FreakSetTexture(fileByte);

//      selectedProfilePicImg.FreakSetSprite(FreakAppManager.Instance.myUserProfile.userPic);

        /*Texture2D _texture = new Texture2D(1, 1);
        _texture.LoadImage (fileByte);

        if (_texture != null) 
        {
        }*/
    }

	void AssignImageToSelectedPic (Texture2D tex)
	{
		if (tex != null) {
			selectedProfilePicImg.FreakSetSprite (tex);
		}
	}

	public void OnClickGameName()
	{
		Debug.Log ("onclickgamename");
		nameEditStatus = NameEditStatus.GameName;
		//nameEditTextField.SetTextNative(FreakAppManager.Instance.myUserProfile.secondary_name);
		nameEditObj.SetActive (true);
	}


	public void OnClickName()
    {
        Debug.Log("onclickname");
        nameEditStatus = NameEditStatus.Name;
        //nameEditTextField.SetTextNative(FreakAppManager.Instance.myUserProfile.name);
        nameEditObj.SetActive(true);
    }


	public void OnClickSaveName()
	{
		if (nameEditStatus == NameEditStatus.Name) 
		{
            FreakAppManager.Instance.myUserProfile.name = nameEditTextField.GetComponent<InputField>().text;

            FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall 
            (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, 
            FreakAppManager.Instance.myUserProfile.gender);
            SetDetails ();
		}
		else 
		{

            FreakAppManager.Instance.myUserProfile.secondary_name = nameEditTextField.GetComponent<InputField>().text;
            FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender);
            SetDetails ();
        }
		nameEditObj.SetActive (false);
	}

	public void OnClickCanelNameEdit()
	{
//		if (!string.IsNullOrEmpty (nameEditTextField.text)) 
//		{
//			FreakAppManager.Instance.myUserProfile.gameName = nameEditTextField.text;
//			FindObjectOfType<AddProfileImageAPIController> ().SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.userName, FreakAppManager.Instance.myUserProfile.gameName, FreakAppManager.Instance.myUserProfile.gender);
//			SetDetails ();
//		}
		nameEditObj.SetActive (false);
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
