﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class StoreAudioBuzzUIController : MonoBehaviour, ITableViewDataSource  {

	public StoreAudioBuzzComponent m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;
	public List<StoreAudioBuzz> audioBuzzInventory = new List<StoreAudioBuzz> ();
	private ImageController imageController;

	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
	}

	void OnEnable()
	{
		Init ();
	}

	public void Init()
	{
        FindObjectOfType<StoreInventory>().GetStoreInventoryListAPICall(StoreInventory.StoreInventoryType.AudioBuzz.GetHashCode().ToString(), EnableTableView);
	}


	public void EnableTableView(List<object> inventoryAudioList)
	{
		audioBuzzInventory.Clear ();
		for (int i = 0; i < inventoryAudioList.Count; i++) 
		{
			Dictionary<string,object> data = (Dictionary<string,object>)inventoryAudioList [i];
			StoreAudioBuzz storeStickers = new StoreAudioBuzz (data);
			audioBuzzInventory.Add (storeStickers);
		}

		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}
		

	public void EnableTableView(){

		m_tableView.dataSource = this;
	}


	public void OnClickBack(){
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return audioBuzzInventory.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		StoreAudioBuzzComponent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as StoreAudioBuzzComponent;
		if (cell == null) {
			cell = (StoreAudioBuzzComponent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}


	void AssignDetails(StoreAudioBuzzComponent cell, int rowNumb)
	{
//		ServerUserContact cont = serverContactList [rowNumb];
//
//		if (!string.IsNullOrEmpty (cont.userName)) {
//			cell.nameText.text = cont.userName;
//		} else {
//			cell.nameText.text = "Unknown Contact " + rowNumb;
//		}
//		cell.numberText.text = cont.contactNumb;
//		cell.IntUser (cont.user_id.ToString (), cont.userName);

		StoreAudioBuzz audioBuzzDetails = audioBuzzInventory [rowNumb];
		cell.nameText.text = audioBuzzDetails.title;
		cell.iD = int.Parse(audioBuzzDetails.inventory_id);
		imageController.GetImageFromLocalOrDownloadImage (audioBuzzDetails.descriptionImagePath, cell.displayPic, Freak.FolderLocation.Profile);
		if (audioBuzzDetails.purchase_status) 
		{
			cell.downloadBtn.gameObject.SetActive (false);
			cell.purchasedText.SetActive (true);
		} 
		else
		{
			cell.downloadBtn.gameObject.SetActive (true);
			cell.purchasedText.SetActive (false);
		}
	}


	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}
		
}

public class StoreAudioBuzz
{
	public string inventory_id;
	public string title;
	public string type;
	public string cost;
	public string descriptionImagePath;
	public string descriptionImageName;
	public bool purchase_status;
	private List<object> audioObject = new List<object> ();
	public List<AudioBuzz> audioBuzzList = new List<AudioBuzz> ();

	public StoreAudioBuzz(Dictionary<string,object> data)
	{
		inventory_id = data ["inventory_id"].ToString ();
		if (data.ContainsKey ("type")) 
		{
			type = data ["type"].ToString ();
		}
		if (data.ContainsKey ("cost")) 
		{
			cost = data ["cost"].ToString ();
		}
		if (data.ContainsKey ("purchase_status")) 
		{
			purchase_status = bool.Parse (data ["purchase_status"].ToString ());
		}
		descriptionImagePath = data ["description_image_path"].ToString ();
		descriptionImageName = data ["description_image_name"].ToString ();
		audioObject = (List<object>)data ["sticker_image"];
		title = data ["title"].ToString ();

		for (int i = 0; i < audioObject.Count; i++) 
		{
			Dictionary<string, object> audioData = (Dictionary<string, object>)audioObject [i];
			AudioBuzz stickerImg = new AudioBuzz (audioData);
			audioBuzzList.Add (stickerImg);
		}
	}
}

public class AudioBuzz
{
	public string audioPath;
	public string audioName;

	public AudioBuzz(Dictionary<string,object> data)
	{
		audioPath = data ["sticker_image_path"].ToString ();
		audioName = data ["sticker_image_name"].ToString ();
	}
}




