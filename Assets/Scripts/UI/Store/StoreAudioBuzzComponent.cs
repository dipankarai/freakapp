﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;


public class StoreAudioBuzzComponent : TableViewCell {

	//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
	//containing this component as a cell in a TableView

	public Text nameText;
	public int iD;
	public Image displayPic;
	public Button downloadBtn;
	public GameObject purchasedText;
	public Slider m_cellHeightSlider;

	public int rowNumber { get; set; }

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	public void SliderValueChanged(Slider slider) {
		onCellHeightChanged.Invoke(rowNumber, slider.value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}

	public void OnClickPlay()
	{
	}

	public void OnClickDownload()
	{
		FindObjectOfType<StoreInventory> ().PurchasedStoreAPICall (iD, RefreshAudioTableView);
	}

	//		public void OnClickCountry()
	//		{
	//			UIPanelController uiPanelController = FindObjectOfType<UIPanelController> () as UIPanelController;
	//			uiPanelController.welcomePanelController.SetCountryDetails (this);
	//			uiPanelController.PopPanel ();
	//
	//		}

	void RefreshAudioTableView(bool isRefresh)
	{
		if (isRefresh)
		{
			FindObjectOfType<StoreAudioBuzzUIController> ().Init ();
		}
	}

}
