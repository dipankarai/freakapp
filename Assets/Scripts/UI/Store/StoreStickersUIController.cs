﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class StoreStickersUIController : MonoBehaviour, ITableViewDataSource  {

	public StrickerComponent m_cellPrefab;
	public TableView m_tableView;
	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;
	public List<StoreStickers> inventoryStickersList = new List<StoreStickers>();
	private ImageController imageController;

	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
	}


	void OnEnable()
	{
		Init ();
	}

	public void Init()
	{
        FindObjectOfType<StoreInventory> ().GetStoreInventoryListAPICall (StoreInventory.StoreInventoryType.Stickers.GetHashCode().ToString(), EnableTableView);
	}


	public void EnableTableView(List<object> stickersList)
	{
		inventoryStickersList.Clear ();
		for (int i = 0; i < stickersList.Count; i++) 
		{
			Dictionary<string,object> data = (Dictionary<string,object>)stickersList [i];
			StoreStickers storeStickers = new StoreStickers (data);
			inventoryStickersList.Add (storeStickers);
		}

		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}


	public void OnClickBack(){
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return inventoryStickersList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		StrickerComponent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as StrickerComponent;
		if (cell == null) {
			cell = (StrickerComponent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}


	void AssignDetails(StrickerComponent cell, int rowNumb)
	{
		//		ServerUserContact cont = serverContactList [rowNumb];
		//
		//		if (!string.IsNullOrEmpty (cont.userName)) {
		//			cell.nameText.text = cont.userName;
		//		} else {
		//			cell.nameText.text = "Unknown Contact " + rowNumb;
		//		}
		//		cell.numberText.text = cont.contactNumb;
		//		cell.IntUser (cont.user_id.ToString (), cont.userName);

		StoreStickers stickers = inventoryStickersList [rowNumb];
		cell.nameText.text = stickers.inventory_id;
		cell.iD = int.Parse(stickers.inventory_id);
		imageController.GetImageFromLocalOrDownloadImage (stickers.description_imagePath, cell.cellImage, Freak.FolderLocation.Profile);
		if (stickers.purchase_status) 
		{
			cell.downLoadButton.gameObject.SetActive (false);
			cell.purchasedText.SetActive (true);
		} 
		else
		{
			cell.downLoadButton.gameObject.SetActive (true);
			cell.purchasedText.SetActive (false);
		}
			
	}


	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}
}


public class StoreStickers
{
	public string inventory_id;
	public string type;
	public string cost;
	public string description_imagePath;
	public bool purchase_status;
	private List<object> StickerObject = new List<object> ();
	public List<StickerImages> stickersList = new List<StickerImages> ();
	public string title;
	public string descImageName;
	public List<Sprite> Stickerimages =  new List<Sprite> ();

	public StoreStickers(Dictionary<string,object> data)
	{
		inventory_id = data ["inventory_id"].ToString ();
		if (data.ContainsKey ("type"))
		{
			type = data ["type"].ToString ();
		}
		if (data.ContainsKey ("cost")) 
		{
			cost = data ["cost"].ToString ();
		}
		description_imagePath = data ["description_image_path"].ToString ();
		if (data.ContainsKey ("purchase_status")) 
		{
			purchase_status = bool.Parse(data ["purchase_status"].ToString ());
		}
		StickerObject = (List<object>)data ["sticker_image"];
		for (int i = 0; i < StickerObject.Count; i++) 
		{
			Dictionary<string, object> stickerdata = (Dictionary<string, object>)StickerObject [i];
			StickerImages stickerImg = new StickerImages (stickerdata);
			stickersList.Add (stickerImg);
		}
		title = data ["title"].ToString ();
		descImageName = data ["description_image_name"].ToString ();
	}
}


public class StickerImages
{
	public string stickerImagePath;
	public string stickerImageName;

	public StickerImages(Dictionary<string,object> data)
	{
		stickerImagePath = data ["sticker_image_path"].ToString ();
		stickerImageName = data ["sticker_image_name"].ToString ();
	}
}


