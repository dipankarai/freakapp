﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Freak;

public class StoreInventory : MonoBehaviour {

    public enum StoreInventoryType
    {
        Stickers = 1,
        AudioBuzz,
        Games
    }

	List<object> inventoryList = new List<object>();
	static System.Action<List<object>> onDone;

	void Awake()
	{
		//DontDestroyOnLoad (this);
	}

	/// <summary>
	/// Get Store Items to be purchased
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetStoreInventoryListAPICall(string type, System.Action<List<object>> _onDone = null)
	{
		onDone = _onDone;
		FreakApi api  = new FreakApi("inventory.getList",ServerResponseForGetStoreInventoryListAPICall);
		api.param("type", type);

		api.get(this);
	}


	void ServerResponseForGetStoreInventoryListAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			inventoryList.Clear ();
			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/

				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				inventoryList = (List<object>)userDetailsDict ["inventory"];
				Debug.Log ("INVENTORY LIST --- >" + inventoryList.Count);
				OnLoaded ();
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

	void OnLoaded()
	{
		if (onDone != null)
		{
			onDone (inventoryList);
		}
	}


	/// <summary>
	/// Get All Store purchased Items
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetStorePurchasedListAPICall(string type, System.Action<List<object>> _onDone = null)
	{
		onDone = _onDone;
		FreakApi api  = new FreakApi("inventory.getPurchaseList",ServerResponseForGetStorePurchasedListAPICall);
		api.param("type", type);

		api.get(this);
	}


	void ServerResponseForGetStorePurchasedListAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("ServerResponseForGetStorePurchasedListAPICall " + Newtonsoft.Json.JsonConvert.SerializeObject(output));
        #endif
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			inventoryList.Clear ();
			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				if (output ["responseMsg"] != null)
				{
					IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
					inventoryList = (List<object>)userDetailsDict ["inventory"];
					OnLoaded ();
				}
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	private System.Action<bool> m_callback;
	/// <summary>
	/// purchase store Items
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void PurchasedStoreAPICall(int inventory_id, System.Action<bool> callback)
	{
		m_callback = callback;
		FreakApi api  = new FreakApi("inventory.purchase",ServerResponseForPurchasedStoreAPICall);
		api.param("inventory_id", inventory_id.ToString());
		api.get(this);
	}


	void ServerResponseForPurchasedStoreAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			inventoryList.Clear ();
			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Successfull Purchase.");
				OnSuccessfulPurchase (true);
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	void OnSuccessfulPurchase(bool success)
	{
		if(success)
		{
			m_callback(success);
		}
	}

	/// <summary>
	/// Svae User Data after purchasing
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void SaveUserCoinsAPICall(int coinCount)
	{
		int totalCoinsCount = FreakAppManager.Instance.myUserProfile.coin + coinCount;
		FreakAppManager.Instance.myUserProfile.coin = totalCoinsCount;
		FreakApi api = new FreakApi ("user.saveUserInfo", ServerResponseForSaveUserCoinsAPICall);
		api.param ("coin", FreakAppManager.Instance.myUserProfile.coin.ToString());

		api.get (this);

	}


	void ServerResponseForSaveUserCoinsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						 * Successfull Login... Load Game..
						*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				break;
			case 207:
				/**
						* User email id Does not Exists.. U need to signIn first...
			 			*/
				break;
			default:
				/**
						 * Show Popup to let user know if anything has gone wrong while login in...
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	#region Game Referal
	/// <summary>
	/// purchase store Items
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GameReferralAPICall(int inventory_id)
	{
		FreakApi api  = new FreakApi("referral.getCode",ServerResponseForGameReferralAPICall);
		api.param("inventory_id", inventory_id.ToString());
		api.get(this);
	}


	void ServerResponseForGameReferralAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				string referralCode = userDetailsDict ["referral_code"].ToString ();
				FreakAppManager.Instance.myUserProfile.gameReferralCode = referralCode;
				FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.SelectPlayerToReferPanel);
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	/// <summary>
	/// purchase store Items
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void AcceptReferralAPICall(string referralCode)
	{
		FreakApi api  = new FreakApi("referral.acceptCode",ServerResponseForAcceptReferralAPICall);
		api.param("referral_code", referralCode);
		api.get(this);
	}


	void ServerResponseForAcceptReferralAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			switch(responseCode){
			case 001:
				/**
				 * Successfull Login... Load Game..
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
	#endregion
}
