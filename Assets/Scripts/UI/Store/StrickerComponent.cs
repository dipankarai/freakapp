﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;

public class StrickerComponent : TableViewCell {

	//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
	//containing this component as a cell in a TableView

	public Text nameText;
	public int iD;
	public Image cellImage;
	public Button downLoadButton;
	public Slider m_cellHeightSlider;

	public Text buttonText;
	public Text descText;

	public GameObject purchasedText;
	public bool isGames = false;

	public int rowNumber { get; set; }

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> {}
	public CellHeightChangedEvent onCellHeightChanged;

	public void SliderValueChanged(Slider slider) {
		onCellHeightChanged.Invoke(rowNumber, slider.value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}


	public void OnClickDownload()
	{
		FindObjectOfType<StoreInventory> ().PurchasedStoreAPICall (iD, RefreshStickerTableView);
	}



	void RefreshStickerTableView(bool isRefresh)
	{
		if (isRefresh)
		{
			FindObjectOfType<StoreStickersUIController> ().Init ();
		}
	}

	void RefreshGamesTableView(bool isRefresh)
	{
		if (isRefresh) 
		{
			FindObjectOfType<StoreGamesUIController> ().Init ();
		}
	}


//	public void OnClickCountry()
//		{
//		UIPanelController uiPanelController = FindObjectOfType<UIPanelController> () as UIPanelController;
//		uiPanelController.welcomePanelController.SetCountryDetails (this);
//		uiPanelController.PopPanel ();
//
//	}

	public void OnClickBuy()
	{
		Debug.Log ("Buy Game ----- > "+ iD);
		FindObjectOfType<StoreInventory> ().PurchasedStoreAPICall (iD, RefreshGamesTableView);
	}
		

	public void OnClickRefer()
	{
		Debug.Log ("Refer Game ----- > "+ iD);
		FindObjectOfType<StoreInventory> ().GameReferralAPICall (iD);
	}
}
