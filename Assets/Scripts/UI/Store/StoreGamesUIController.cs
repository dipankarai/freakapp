﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;


public class StoreGamesUIController : MonoBehaviour {

	public StrickerComponent m_cellPrefab;
	public List<StoreGames> inventoryGamesList = new List<StoreGames>();
	private ImageController imageController;
	public ScrollRect gamesScrollRect;
	public List<GameObject> gamesObjectList = new List<GameObject>();

	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
	}


	void OnEnable()
	{
		Init ();
	}


	public void Init()
	{
        FindObjectOfType<StoreInventory> ().GetStoreInventoryListAPICall (StoreInventory.StoreInventoryType.Games.GetHashCode().ToString(), EnableTableView);
	}


	public void EnableTableView(List<object> gamesList)
	{
		inventoryGamesList.Clear ();
		for (int i = 0; i < gamesList.Count; i++) 
		{
			Dictionary<string,object> data = (Dictionary<string,object>)gamesList [i];
			StoreGames storeGames = new StoreGames (data);
			inventoryGamesList.Add (storeGames);
		}

		InstantiateStickers ();
	}


	void AssignDetails(StrickerComponent cell, int rowNumb)
	{
		//		ServerUserContact cont = serverContactList [rowNumb];
		//
		//		if (!string.IsNullOrEmpty (cont.userName)) {
		//			cell.nameText.text = cont.userName;
		//		} else {
		//			cell.nameText.text = "Unknown Contact " + rowNumb;
		//		}
		//		cell.numberText.text = cont.contactNumb;
		//		cell.IntUser (cont.user_id.ToString (), cont.userName);

		//		StoreStickers stickers = inventoryStickersList [rowNumb];
		//		cell.nameText.text = stickers.inventory_id;
		//		cell.iD = int.Parse(stickers.inventory_id);
		//		imageController.GetImageFromLocalOrDownloadImage (stickers.description_imagePath, stickers.descImageName, cell.cellImage);
		//		if (stickers.purchase_status) 
		//		{
		//			cell.downLoadButton.gameObject.SetActive (false);
		//		} 
		//		else
		//		{
		//			cell.downLoadButton.gameObject.SetActive (true);
		//		}

	}
		
	public void OnClickMyGames()
	{
		
	}

	public void OnClickAllGames()
	{
		
	}


	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}


	void OnDisable()
	{
		Destroy (this.gameObject);
	}

	void InstantiateStickers ()
	{
		if (gamesObjectList != null)
		{
			for (int i = 0; i < gamesObjectList.Count; i++)
			{
				if (gamesObjectList[i] != null)
				{
					Destroy(gamesObjectList[i]);
				}
			}
			gamesObjectList.Clear();
		}

		gamesObjectList = new List<GameObject>();

//		string stickerName = UIController.Instance.assetsReferences.stickers[categoryId].name;
//		List<Sprite> stickerList = UIController.Instance.assetsReferences.stickers[categoryId].stickersList;
		for (int i = 0; i < inventoryGamesList.Count; i++)
		{
			StrickerComponent gObject = Instantiate (m_cellPrefab) as StrickerComponent;
			gObject.name = inventoryGamesList [i].title;
			gObject.nameText.text = inventoryGamesList [i].title;
			gObject.iD = int.Parse(inventoryGamesList [i].inventory_id);
			imageController.GetImageFromLocalOrDownloadImage (inventoryGamesList [i].description_imagePath, gObject.cellImage, Freak.FolderLocation.Profile);
			gObject.isGames = true;
			//GameObject go = new GameObject(stickerName + "_" + i.ToString());
			gObject.gameObject.transform.SetParent(gamesScrollRect.content.transform, false);
			if (inventoryGamesList [i].purchase_status) {
				gObject.downLoadButton.gameObject.SetActive (false);
				gObject.purchasedText.SetActive (true);
			} 
			else {
				gObject.downLoadButton.gameObject.SetActive (true);
				gObject.purchasedText.SetActive (false);
			}
			gObject.descText.text = inventoryGamesList [i].inventory_description;
			if (inventoryGamesList [i].referralCount > 0) 
			{
				gObject.buttonText.text = "Refer";
				gObject.downLoadButton.onClick.AddListener(() => gObject.OnClickRefer());
			} 
			else 
			{
				gObject.buttonText.text = "Buy";
				gObject.downLoadButton.onClick.AddListener(() => gObject.OnClickBuy());
			}
			gamesObjectList.Add(gObject.gameObject);
		}
	}
}


[System.Serializable]
public class StoreGames
{
    public string inventory_id = string.Empty;
    public string type = string.Empty;
    public string cost = string.Empty;
    public string description_imagePath = string.Empty;
    public string inventory_description = string.Empty;
	public bool purchase_status;
    public string title = string.Empty;
    public string descImageName = string.Empty;
	public int referralCount = -1;
    [Newtonsoft.Json.JsonIgnore]
    public Texture2D gameTexture;

    public StoreGames ()
    {
        
    }

	public StoreGames(Dictionary<string,object> data)
	{
		inventory_id = data ["inventory_id"].ToString ();
		if (data.ContainsKey ("type"))
		{
			type = data ["type"].ToString ();
		}
		if (data.ContainsKey ("cost")) 
		{
			cost = data ["cost"].ToString ();
		}
		description_imagePath = data ["description_image_path"].ToString ();
		if (data.ContainsKey ("purchase_status")) 
		{
			purchase_status = bool.Parse(data ["purchase_status"].ToString ());
		}
		title = data ["title"].ToString ();
		descImageName = data ["description_image_name"].ToString ();
		inventory_description = data ["inventory_description"].ToString ();
		if (data.ContainsKey ("referral_count")) 
		{
			referralCount = int.Parse (data ["referral_count"].ToString ());
		}
	}
}


