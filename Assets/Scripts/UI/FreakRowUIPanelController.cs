﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.Linq;
using System;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;

public class FreakRowUIPanelController : MonoBehaviour, IEnhancedScrollerDelegate  {


	public FreaksContentController freaksContentController;
	public string failString;
	int currentCount;

	public ImageController imageController;
	public FreakContactsCellView m_cellPrefab;
	private List<string> contactNumbers = new List<string> ();

//	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	public List<ServerUserContact> serverContactList = new List<ServerUserContact> ();
	public EnhancedScroller scroller;


	void Start()
	{
//		#if UNITY_EDITOR
//		foreach(Contact detailsObj in dummyContactsList)
//		{
//			Contacts.ContactsList.Add( detailsObj );
//			Debug.Log(Contacts.ContactsList.Count);
//		}
//		#endif
	}


	void OnEnable()
	{
		
		serverContactList.Clear ();
		//getContactListApiManager.GetContactListAPICall (FreakAppManager.Instance.mobileNumber, EnableTableView);
//		for (int i = 0; i < freaksContentController.serverContactList.Count; i++)
//		{
//			serverContactList.Add (freaksContentController.serverContactList [i]);
//		}
		scroller.Delegate = this;
		scroller.ClearAll ();
		serverContactList = new List<ServerUserContact> (freaksContentController.serverContactList);

		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;

	}


//	void EnableTableView(List<ServerUserContact> data)
//	{
//		m_tableView.dataSource = this;
//		serverContactList.Clear ();
//		for (int i = 0; i < data.Count; i++)
//		{
//			serverContactList.Add (data [i]);
//		}
//
//	}


	public void OnTextEdit(InputField searchinput)
	{
		//Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text);
		//Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text + " Count ----- > "+  tempCountryNames.Count);
		if (searchinput.text.Length > 0) 
		{
			for (int i = 0; i < serverContactList.Count; i++)
			{
				if (serverContactList [i].name.ToUpper ().Contains (searchinput.text.ToUpper())) 
				{
                    AddToList (serverContactList [i]);
                    /*if (serverContactList.Contains(freaksContentController.serverContactList[i]) == false)
					{
						serverContactList.Add(freaksContentController.serverContactList[i]);
					}*/
				} 
				else 
				{
                    RemoveFromList (serverContactList [i]);
                    /*if (serverContactList.Contains(freaksContentController.serverContactList[i]))
					{
						serverContactList.Remove(freaksContentController.serverContactList[i]);
					}*/
				}
			}
		}
		else {
			serverContactList.Clear ();
            serverContactList = new List<ServerUserContact> (freaksContentController.serverContactList);
            /*for (int i = 0; i <  freaksContentController.serverContactList.Count; i++) 
			{
				serverContactList.Add (freaksContentController.serverContactList [i]);
			}*/
		}

		//m_tableView.RefreshVisibleRows ();
//		m_tableView.ReloadData ();
		scroller.ReloadData();

	}

    void AddToList (ServerUserContact userContact)
    {
        for (int i = 0; i < serverContactList.Count; i++)
        {
            if (serverContactList[i].name.Equals(userContact.name))
            {
                return;
            }
        }

        serverContactList.Add(userContact);
    }

    void RemoveFromList (ServerUserContact userContact)
    {
        for (int i = 0; i < serverContactList.Count; i++)
        {
            if (serverContactList[i].name.Equals(userContact.name))
            {
                serverContactList.RemoveAt (i);
                return;
            }
        }

    }

	public void ClearOnClose()
	{
		serverContactList.Clear ();
		for (int i = 0; i <  freaksContentController.serverContactList.Count; i++) 
		{
			serverContactList.Add (freaksContentController.serverContactList [i]);
		}

//		m_tableView.ReloadData ();
		scroller.ReloadData ();
	}
		

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

//	//Will be called by the TableView to know how many rows are in this table
//	public int GetNumberOfRowsForTableView(TableView tableView)
//	{
//		if (serverContactList.Count > 0) {
//
//			float val = (float)(serverContactList.Count / 3f);
//			int d = (int) Mathf.Ceil(val);
//			Debug.Log ("serverContactList Count --- >" + serverContactList.Count + " current count --- > "+ d);
//			return d;
//		} else {
//			return 0;
//		}
//	}

			

//	//Will be called by the TableView to know what is the height of each row
//	public float GetHeightForRowInTableView(TableView tableView, int row) {
//		return GetHeightOfRow(row);
//	}
//
//
//		//Will be called by the TableView when a cell needs to be created for display
//		public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
//		{
//			ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
//			if (cell == null) {
//				cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
//				cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//				cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//			}
//	
//			cell.rowNumber = row;
//			AssignDetails (cell, row);
//			cell.height = GetHeightOfRow(row);
//			return cell;
//		}


	#region EnhancedScroller Handlers

	/// <summary>
	/// This tells the scroller the number of cells that should have room allocated. This should be the length of your data array.
	/// </summary>
	/// <param name="scroller">The scroller that is requesting the data size</param>
	/// <returns>The number of cells</returns>
	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		// in this example, we just pass the number of our data elements
		//Debug.Log("mFreakChachedChannelList.Count--- >"+ mFreakChachedChannelList.Count);
		if (serverContactList.Count > 0) {
			return serverContactList.Count;
		} else {
			return 0;
		}
	}

	/// <summary>
	/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
	/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
	/// cell size will be the width.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell size</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <returns>The size of the cell</returns>
	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
		//return (dataIndex % 2 == 0 ? 30f : 100f);
		return 180;
	}

	/// <summary>
	/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
	/// Some examples of this would be headers, footers, and other grouping cells.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
	/// <returns>The cell for the scroller to use</returns>
	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		// first, we get a cell from the scroller by passing a prefab.
		// if the scroller finds one it can recycle it will do so, otherwise
		// it will create a new cell.
		FreakContactsCellView cellView = scroller.GetCellView(m_cellPrefab) as FreakContactsCellView;

		// set the name of the game object to the cell's data index.
		// this is optional, but it helps up debug the objects in 
		// the scene hierarchy.
		//			cellView.name = "Cell " + dataIndex.ToString();

		// in this example, we just pass the data to our cell's view which will update its UI
		if(cellView)
		cellView.AssignDetails(serverContactList [dataIndex],dataIndex);

		// return the cell to the scroller
		return cellView;
	}

	#endregion

	IEnumerator loadProfileImage(string avatarUrl, Image img, ContactDetailsDisplay cell)
	{
		yield return new WaitForSeconds (0.1f);
		imageController.GetImageFromLocalOrDownloadImage (avatarUrl, img, Freak.FolderLocation.Profile);
		cell.imageUrl = avatarUrl;
	}

//	void AssignDetailsToGrids(ContactDisplayUIComponent cell, int rowNumb)
//	{
//		int firstColoumnIndex = rowNumb * 3;
//		for (int coloumn = 0; coloumn < 3; coloumn++)
//		{
//			if (firstColoumnIndex + coloumn < serverContactList.Count)
//			{
//				ServerUserContact cont = serverContactList [firstColoumnIndex + coloumn];
//				ContactDetailsDisplay contactsdetailsUiGrid = cell.contactDetailsList [coloumn];
//				//cell.contactDetailsList [coloumn];
//				contactsdetailsUiGrid.gameObject.SetActive (true);
//				contactsdetailsUiGrid.selectionToggle.gameObject.SetActive (false);
//				if (!string.IsNullOrEmpty (cont.userName)) 
//				{
//					contactsdetailsUiGrid.nameText.text = cont.userName;
//				} 
//				else 
//				{
//					contactsdetailsUiGrid.nameText.text = cont.contactNumb;
//				}
//
//				contactsdetailsUiGrid.numberText.text = cont.contactNumb;
//				contactsdetailsUiGrid.IntUser (cont.user_id.ToString(), cont.userName);
//
//			} 
//			else {
//				break;
//			}
//
//		}

		//		for(int i = 0; i < cell.contactDetailsList.Count; i++)
		//		{
		//			
		//			if (i < serverContactList.Count) 
		//			{
		//				ServerUserContact cont = serverContactList [i*rowNumb];
		//				ContactDetailsDisplay contactsdetailsUiGrid = cell.contactDetailsList [i];
		//				contactsdetailsUiGrid.gameObject.SetActive (true);
		//				if (!string.IsNullOrEmpty (cont.userName)) 
		//				{
		//					contactsdetailsUiGrid.nameText.text = cont.userName;
		//				} 
		//				else 
		//				{
		//					contactsdetailsUiGrid.nameText.text = "Unknown Contact " + rowNumb;
		//				}
		//				contactsdetailsUiGrid.numberText.text = cont.contactNumb;
		//				contactsdetailsUiGrid.IntUser (cont.user_id.ToString(), cont.userName);
		//			}
		//		}

//	}


	#endregion

//	private float GetHeightOfRow(int row) {
//		if (m_customRowHeights.ContainsKey(row)) {
//			return m_customRowHeights[row];
//		} else {
//			return m_cellPrefab.height;
//		}
//	}
//
//	private void OnCellHeightChanged(int row, float newHeight) {
//		if (GetHeightOfRow(row) == newHeight) {
//			return;
//		}
//		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
//		m_customRowHeights[row] = newHeight;
//		m_tableView.NotifyCellDimensionsChanged(row);
//	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}
		
	void OnDisable(){
		scroller.Delegate = null;
	}
}
