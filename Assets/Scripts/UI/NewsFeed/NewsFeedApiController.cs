﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Freak.Chat;
using System;
using MiniJSON;
using Freak;
using MiniJSON;
using System.Xml; 
using System.Xml.Serialization;  
using System.Linq;



public class NewsFeedApiController : MonoBehaviour {

	//public List<ServerUserContact> serverContactList;
	private int lastFeedId = 0;

	public List<NewsFeedItem> newsFeeds = new List<NewsFeedItem>();
	static System.Action<List<NewsFeedItem>> onDone;
	private GameObject loadingObj;

	public ConnectingUI connectingUI;
	public ImageController imageController;


	enum FeedType{
		Text = 1,
		Image = 2
	}
	FeedType feedType;


	#region API region
	/// <summary>
	/// Get News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetNewsFeedsAPICall(int maxLimit, GameObject _loadingObj, int otherUserId = 0, System.Action<List<NewsFeedItem>> _onDone = null)
	{
		onDone = _onDone;
		loadingObj = _loadingObj;
		FreakApi api = new FreakApi ("newsFeed.get", ServerResponseForGetNewsFeedsAPICall);
		api.param ("last_feed_id", lastFeedId);
		api.param ("limit", maxLimit);
		if (otherUserId > 0) 
		{
			api.param ("other_user_id", otherUserId);
		}
		api.get (this);

	}


	void ServerResponseForGetNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
        /*if(loadingObj != null)
		loadingObj.SetActive (false);
        
        FreakAppManager.Instance.isLoaderOn = false;
        */
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				newsFeeds.Clear ();
				if (output ["responseMsg"].ToString().Length > 50)
				{
                        Debug.Log("*******NEWSFEED******* " + Newtonsoft.Json.JsonConvert.SerializeObject(output));

					IDictionary newsFeedDetails = (IDictionary)output ["responseMsg"];
					//Debug.Log ("TYPE ---- >" + output ["responseMsg"].ToString().Length);
					List<object> data = (List<object>)newsFeedDetails ["content"];
					//Dictionary<string,object> initFeedData = new Dictionary<string, object> ();
					//initFeedData.Add ("name", "Test");
					//NewsFeedItem testFeedItem = new NewsFeedItem (initFeedData);
					//testFeedItem.imageUrl = FreakAppManager.Instance.myUserProfile.avatar_url;
					//FindObjectOfType<ImageController> ().LoadProfileImageForNewsFeed (testFeedItem.imageUrl, testFeedItem.imageSprite, FolderLocation.NewsFeed);
					//newsFeeds.Add (testFeedItem);
					for (int i = 0; i < data.Count; i++) 
					{
						Dictionary<string,object> feeddata = (Dictionary<string,object>)data [i];
						NewsFeedItem feeditem = new NewsFeedItem (feeddata);
						if (!string.IsNullOrEmpty (feeditem.feedType)) {
							//FindObjectOfType<ImageController> ().LoadProfileImageForNewsFeed (feeditem.imageUrl, feeditem.imageSprite, FolderLocation.NewsFeed);
							newsFeeds.Add (feeditem);
						}
					}
				} 
//				else 
//				{
//					Dictionary<string,object> initFeedData = new Dictionary<string, object> ();
//					initFeedData.Add ("name", "Test");
//					NewsFeedItem testFeedItem = new NewsFeedItem (initFeedData);
//					testFeedItem.imageUrl = FreakAppManager.Instance.myUserProfile.avatar_url;
//					//FindObjectOfType<ImageController> ().LoadProfileImageForNewsFeed (testFeedItem.imageUrl, testFeedItem.imageSprite, FolderLocation.NewsFeed);
//					newsFeeds.Add (testFeedItem);
//				}
				OnLoadedFeeds ();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);

            ////inapi.get(this);
        }  

        if(loadingObj != null)
            loadingObj.SetActive (false);
        
        FreakAppManager.Instance.isLoaderOn = false;
	}


	void OnLoadedFeeds()
	{
		if (onDone != null)
		{
			onDone (newsFeeds);
		}
	}

	/// <summary>
	/// Post News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void PostNewsFeedsAPICall(string content, string feedtype)
	{
		if (!string.IsNullOrEmpty (content)) 
		{
			string newstr = EncoedeString (content);
			connectingUI.DisplayLoading ();
			FreakApi api = new FreakApi ("newsFeed.post", ServerResponseForPostNewsFeedsAPICall);
			api.param ("userId", FreakAppManager.Instance.userID);
			api.param ("feed_type", feedtype);
			api.param ("content", newstr);
			api.get (this);
		}

	}


	void ServerResponseForPostNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		connectingUI.gameObject.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				FindObjectOfType<NewsFeedUIController>().InitAfterPosting();
				//FindObjectOfType<HomeScreenUIPanelController>().PopPanel();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
		

//	public bool HasSpecialChars(string yourString)
//	{
//		//Debug.Log ("HasSpecialChars yourString -------->>"+ yourString); 
//		if(!String.IsNullOrEmpty(yourString)){
//			return yourString.Any (ch => !Char.IsLetterOrDigit (ch));
//		} else {
//			return false;
//		}
//	}

	public string EncoedeString(string inputstring)
	{		
		return WWW.EscapeURL (inputstring);
	}


    public void PostNewsFeedWithImage(string content, string feedtype, Texture2D texture, System.Action<int,string> postCallback)
	{
        StartCoroutine(PostImageNewsFeedsAPICall(content, feedtype, texture, postCallback));
	}


	/// <summary>
	/// Post Image News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
    IEnumerator PostImageNewsFeedsAPICall(string content, string feedtype, Texture2D texture, System.Action<int,string> postCallback)
	{
		string newstr = EncoedeString (content);
		byte[] bytes = texture.EncodeToPNG();

		WWWForm form = new WWWForm();
		form.AddField ("methodName", "newsFeed.post" );
		form.AddField ("userId", FreakAppManager.Instance.userID );
		form.AddField ("accessToken", FreakAppManager.Instance.APIkey );
		form.AddField ("applicationKey", "12345");
		form.AddField ("feed_type", feedtype);
		form.AddField ("content", newstr);
		form.AddBinaryData ("news_feed_image", bytes);

		WWW postRequest = new WWW( FreakAppManager.baseURL, form );
		yield return postRequest;
		
        Debug.Log(postRequest.text);

        if (string.IsNullOrEmpty (postRequest.error))
        {
            IDictionary responseDict = (IDictionary)MiniJSON.Json.Deserialize(postRequest.text);
            int responseCode = int.Parse(responseDict["responseCode"].ToString());
            if (responseCode == 001)
            {
                IDictionary responseMsgDict = (IDictionary) responseDict["responseMsg"];
                int news_feed_id = int.Parse(responseMsgDict["news_feed_id"].ToString());
                string uploaded_image_path = responseMsgDict["uploaded_image_path"].ToString();

                if (postCallback != null)
                {
                    postCallback(news_feed_id, uploaded_image_path);
                }
            }
            else
            {
                if (postCallback != null)
                {
                    postCallback(-1, "");
                }
            }

        }
        else
        {
            if (postCallback != null)
            {
                postCallback(-1, "");
            }
        }  

        FindObjectOfType<NewsFeedUIController>().Init();
    }



//	void ServerResponseForPostImageNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
//	{
//		// check for errors
//		if (output!=null)
//		{
//			string responseInfo =(string)output["responseInfo"];
//
//			switch(responseCode)
//			{
//			case 001:
//				/**
//						
//				*/
//				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
//				GetNewsFeedsAPICall ();
//				break;
//			case 207:
//				/**
//						
//			 	*/
//				break;
//			default:
//				/**
//						
//				*/
//				break;
//			}	
//		} 
//		else {
//			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
//			////inapi.get(this);
//		}  
//	}

	System.Action<bool> m_Callback;

	//Newsfeed Like comment
	public void LikeCommentFeedAPICall(string type, string news_feed_id, System.Action<bool> callback)
	{
		m_Callback = callback;
		FreakApi api = new FreakApi ("newsFeed.like", ServerResponseForLikeCommentFeedAPICall);
		api.param ("userId", FreakAppManager.Instance.userID);
		api.param ("news_feed_id", news_feed_id);
		api.param ("type", type);
		api.get (this);

	}


	void ServerResponseForLikeCommentFeedAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				ChangeLikeCount(true);
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
		
	void ChangeLikeCount(bool isSuccess)
	{
		if (isSuccess) 
		{
			m_Callback (isSuccess);
		}
	}


	//Newsfeed Like comment
	public void ReportNewsFeedAPICall(string news_feed_id, string message)
	{
		FreakApi api = new FreakApi ("newsFeed.report", ServerResponseForReportNewsFeedAPICall);
		api.param ("news_feed_id", news_feed_id);
		api.param ("message", message);
		api.get (this);

	}


	void ServerResponseForReportNewsFeedAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	//Delete Newsfeed
	public void DeleteNewsFeedAPICall(string news_feed_id)
	{
		FreakApi api = new FreakApi ("newsFeed.delete", ServerResponseForDeleteNewsFeedAPICall);
		api.param ("news_feed_id", news_feed_id);
		api.get (this);

	}


	void ServerResponseForDeleteNewsFeedAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo = (string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				FindObjectOfType<NewsFeedUIController>().InitAfterPosting();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	//Share NewsFeed
	public void ShareNewsFeedAPICall(string news_feed_id)
	{
		FreakApi api = new FreakApi ("newsFeed.share", ServerResponseForShareNewsFeedAPICall);
		api.param ("news_feed_id", news_feed_id);
		api.get (this);
	}

	void ServerResponseForShareNewsFeedAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.newsFeedShare);
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}



	#endregion
}

[System.Serializable]
public class NewsFeedItem
{
    public enum PanelType
    {
        None,
        NewsFeed,
        Ads
    }

	public string newsfeedId;
	public string userId;
	public string userName;
	public string title;
	public string feedType;
	private IDictionary feedData;
	public string imageUrl;
	public string image_name;
	public string sharedNewsFeedId;
	public string likeCount;
	public string totalCommentCount;
	private List<object> feedComments;
	public string avatarUrl;
	public string avatarName;
	public List<CommentDetails> commentsList = new List<CommentDetails>();
	public FeedDataText feedDataText;
	public bool isLiked;
    public Sprite imageSprite = null;

    //COMMON
    public PanelType panelType = PanelType.None;

    //ADS PANEL
    public Freak.Ads.FacebookNativeAd facebooknativeAd;

	public string GetDataType { 
		get { 
			//Debug.Log(" SHOWWWWWWWWWWWWWW Text Return type -- "+ feedType);
			return this.feedType; 
		} 
	}

	public NewsFeedItem(Dictionary<string,object> data)
	{
		if(data.ContainsKey("title"))
		{
			newsfeedId = data["news_feed_id"].ToString();
			userId = data["user_id"].ToString();
			userName = DecodeString(data["name"].ToString());
			title = DecodeString(data["title"].ToString());
			feedType = data["feed_type"].ToString();
			feedData = (IDictionary) data["feed_data"];
            panelType = PanelType.NewsFeed;
			feedDataText = new FeedDataText (feedData);
            if(data.ContainsKey("image_url"))
            {
                imageUrl = data["image_url"].ToString();
            }
			//Debug.Log ("image_url ---->" + imageUrl);
			image_name = data["image_name"].ToString();
			sharedNewsFeedId = data["shared_news_feed_id"].ToString();
			likeCount = data["like_count"].ToString();
			isLiked = bool.Parse(data["liked"].ToString());
			totalCommentCount = data["total_comment_count"].ToString();
			feedComments = (List<object>)data["comments"];
			avatarUrl = data["avatar_url"].ToString();
//			Debug.Log ("Avatar URL --- >" + avatarUrl);
			avatarName =  data["avatar_name"].ToString();

			for (int i = 0; i < feedComments.Count; i++) 
			{
				Dictionary<string,object> datadetails = (Dictionary<string,object>)feedComments[i];
				CommentDetails commentsDetails = new CommentDetails (datadetails);
				commentsList.Add (commentsDetails);
			}

            ValidateGameType(feedDataText.feedText);
		}
	}

    private static string[] validGameType = {"Tic Tac Toe","Sand Castle","Quiz","Chess Circle","Game of Drawit","Self Moments","Snap Pool"} ;
    void ValidateGameType (string gamename)
    {
        if (FreakAppManager.Instance.inventoryGamesList.Count > 0)
        {
            for (int i = 0; i < FreakAppManager.Instance.inventoryGamesList.Count; i++)
            {
                string _a = gamename.ToLower();
                string _b = FreakAppManager.Instance.inventoryGamesList[i].title.ToLower();
                
                if (_a.Contains (_b))
                {
                    feedType = "4";
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < validGameType.Length; i++)
            {
                string _a = gamename.ToLower();
                string _b = validGameType[i].ToLower();
                if (_a.Contains (_b))
                {
                    feedType = "0";
                    break;
                }
            }
        }
    }

    public NewsFeedItem(Freak.Ads.FacebookNativeAd fbNativeAd)
    {
        panelType = PanelType.Ads;
        facebooknativeAd = fbNativeAd;
		feedType = "3";
    }

	public string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}
}
	
[System.Serializable]
public class FeedDataText
{
	public string feedText;

	public FeedDataText(IDictionary data)
	{
		if(data.Contains("text"))
		{
			feedText = DecodeString(data["text"].ToString());
		}
	}
	string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}
}