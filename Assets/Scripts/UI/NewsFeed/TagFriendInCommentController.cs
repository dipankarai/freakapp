﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.Collections.Generic;
using Freak.Chat;

public class TagFriendInCommentController :  MonoBehaviour, ITableViewDataSource  {

	#region TableView
	public ContactDetailsDisplay m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;

	#endregion

	public List<ServerUserContact> searchedMemberList = new List<ServerUserContact>();

	void Start()
	{
		
	}

	void OnEnable()
	{
		//FindObjectOfType<InitializeSeverContacts> ().GetContactListAPICall (EnableTableView);
		//EnableTableView();

        FreakAppManager.Instance.LoadServerContactList (EnableTableView);

        /*if (FreakAppManager.Instance.userSynchedContacts.Count > 0) 
		{
			EnableTableView (FreakAppManager.Instance.userSynchedContacts);
		} 
		else 
		{
            if (FreakPlayerPrefs.GetSetServerUserContact == null)
            {
				FreakAppManager.Instance.LoadServerContactList (EnableTableView);
//                FindObjectOfType<GetContactListApiManager>().GetContactListAPICall (FreakAppManager.Instance.mobileNumber, EnableTableView);
            }
            else
            {
                EnableTableView (FreakPlayerPrefs.GetSetServerUserContact);
            }
		}*/
	}

	void EnableTableView(List<ServerUserContact> data)
	{
		searchedMemberList.Clear ();
		for (int i = 0; i < data.Count; i++)
		{
			searchedMemberList.Add (data [i]);
		}
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}

//	void EnableTableView()
//	{
//		loadingObj.gameObject.SetActive (false);
//		HideSearchBar ();
//		searchedMemberList.Clear ();
//		for (int i = 0; i < FreakAppManager.Instance.userSynchedContacts.Count; i++)
//		{
//			searchedMemberList.Add (FreakAppManager.Instance.userSynchedContacts [i]);
//		}
//
//		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;
//	}

//	void EnableTableView(List<ServerUserContact> data)
//	{
//		serverContactList.Clear ();
//		for (int i = 0; i < data.Count; i++)
//		{
//			serverContactList.Add (data [i]);
//		}
//
//		freakHomeScreenPanel.enabled = true;
//		status = Status.Panel;
//		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;
//	}

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) 
	{
		return searchedMemberList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) 
	{
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
		if (cell == null) {
			cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}

	void AssignDetails(ContactDetailsDisplay cell, int rowNumb)
	{
		FindObjectOfType<ImageController> ().GetImageFromLocalOrDownloadImage (searchedMemberList [rowNumb].avatar, cell.img, Freak.FolderLocation.Profile);
		cell.InitTagMember(searchedMemberList[rowNumb]);
	}

	private float GetHeightOfRow(int row) 
	{
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) 
	{
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}

	#endregion

}
