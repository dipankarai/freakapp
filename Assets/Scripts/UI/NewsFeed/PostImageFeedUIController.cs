﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;
using Freak;

public class PostImageFeedUIController : MonoBehaviour {

	public InputField postInputField;
	public Image statusImage;

	private Texture2D texture;
	public GameObject imagePickerOptionsObj;

	public Texture2D testTexture;

    private ImageController imageController;

	void OnEnable()
	{
		imagePickerOptionsObj.SetActive (true);
	}

    void Start ()
    {
        imageController = FindObjectOfType<ImageController>();
    }

	public void OnClickPostNewsFeed()
	{
		//FindObjectOfType<NewsFeedApiController> ().PostNewsFeedWithImage (postInputField.text, "2", texture);
		//FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.postNewsFeedconfirm);
		AlertMessege.Instance.yesButton.onClick.AddListener(OnClickyesPost);
		//AlertMessege.Instance.noButton.onClick.AddListener(OnClickNoPost);

	}


	public void OnClickyesPost()
	{
        UIController.Instance.connectingUI.DisplayLoading();
        FindObjectOfType<NewsFeedApiController>().PostNewsFeedWithImage(postInputField.text, "2", texture, NewsFeedPostCallback);
	}


	public void OnClickNoPost()
	{
		//FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}

    void NewsFeedPostCallback (int id, string imageUrl)
    {
        if (string.IsNullOrEmpty(imageUrl) == false)
        {
            string _name = System.IO.Path.GetFileName(imageUrl);
            if (! imageController.feedImages.ContainsKey (_name))
            {
                imageController.feedImages.Add (_name, texture);
            }
        }

        UIController.Instance.connectingUI.HideLoading();
        texture = null;
        FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
    }

	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}

	public void AddPhoto(int selectedName)
	{
		if (selectedName < 3) {
			#if UNITY_EDITOR
			string _path = Application.persistentDataPath + "/group.jpg";
			if (System.IO.File.Exists(_path))
			{
				Debug.Log(_path);
				SetImage(_path);
			}
			else
			{
				Debug.LogError(_path);
			}
			#else
			UIController.Instance.GetProfileImage (SetImage, selectedName);
			#endif
		} 
		else {
			CancelUpdatePicStatus ();
		}
	}


	void SetImage(string path)
	{
		//Debug.Log(mChatMessage.messageTimestamp + "Image File Exits");
		texture = null;
		byte[] fileByte = System.IO.File.ReadAllBytes(path);
		texture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
		texture.LoadImage(fileByte);

        if (texture.width > 1000 || texture.height > 1000)
        {
            texture = TextureResize.ResizeTexture(1000f, 1000f, texture);
        }

		AssignImage (texture);
	}


	void AssignImage (Texture2D tex)
	{
		texture = tex;
		Rect rec = new Rect(0, 0, tex.width, tex.height);
		Sprite spr = Sprite. Create(tex,rec,new Vector2(0,0),1);
		statusImage.sprite = spr;

		imagePickerOptionsObj.SetActive (false);
	}


	public void CancelUpdatePicStatus()
	{
		statusImage.sprite = null;
		OnClickBack ();
	}

}