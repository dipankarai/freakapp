﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using Freak;

public class NewsFeedCommentContent : TableViewCell {

	public Text userId;
	public Text nameText;
	public Text descText;
	public Image profileImage;
	public Button htmlTextButton;
	public Text buttonText;
	public Slider m_cellHeightSlider;
	public Transform textPos;
	public string commentID;
	public GameObject deleteBtn;
	public int rowNumber { get; set; }

	private CommentDetails mChatMessage;

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	public RectTransform commentTextRect;

	public void SliderValueChanged(Slider slider) {
		onCellHeightChanged.Invoke(rowNumber, slider.value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}


	public void OnClickDeleteComment()
	{
		OnClickDelete ();
	}


	public void OnClickDelete()
	{
		
		AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton,  FreakAppConstantPara.MessageDescription.deleteFeedComment);
		AlertMessege.Instance.yesButton.onClick.AddListener(OnClickDeleteYes);
		//AlertMessege.Instance.noButton.onClick.AddListener(OnClickDeleteNo);
	}


	void OnClickDeleteYes()
	{
		//FindObjectOfType<NewsFeedApiController> ().DeleteNewsFeedAPICall (newsFeedID);
		AlertMessege.Instance.yesButton.onClick.RemoveListener(OnClickDeleteYes);
		FindObjectOfType<NewsFeedCommentsUIPanel> ().DeleteCommentAPICall (commentID);

		//Destroy (this.gameObject);
	}

//	void OnClickDeleteNo()
//	{
//		Destroy (this.gameObject);
//	}


	public void EnterMessages(CommentDetails _mChatMessage)
	{
		mChatMessage = _mChatMessage;
		if(mChatMessage.normalMessagerMetaData == null || mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.None)
		{
			descText.text = mChatMessage.normalMessagerMetaData.message;
			//PrintPos (mChatMessage.normalMessagerMetaData.message);
			//CheckTextMessageSize();
		}
		else if (mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.TagFriend)
		{
			TaggedMessage();
		}
	}


	private string atText = "<color=grey>@</color>";
	private string colorOpen = "<color=blue>";
	private string colorClose = "</color>";
	void TaggedMessage ()
	{
		htmlTextButton.gameObject.SetActive(true);
		htmlTextButton.onClick.AddListener(OnClickTagButton);

		string _whiteSpace = "";
		string _tagName = "@" + mChatMessage.normalMessagerMetaData.username;
		string message = mChatMessage.normalMessagerMetaData.message.Replace ("@", "");
		Debug.Log ("Display msg --- >" + message);
		descText.text = " " + _tagName + message;
		//CheckTextMessageSize();

		RectTransform textRectTransform = descText.GetComponent<RectTransform>();
		RectTransform buttonRectTransform = htmlTextButton.GetComponent<RectTransform>();

		Vector2 anchorPos = buttonRectTransform.anchoredPosition;
		//anchorPos.y = LayoutUtility.GetPreferredHeight(textRectTransform) / 2f;
		//float offsetsY = descText.fontSize / 2f;
		//anchorPos.y -= offsetsY;

		Vector2 sizeDelta = buttonRectTransform.sizeDelta;

		Text buttonText = htmlTextButton.transform.GetComponentInChildren<Text>();
		buttonText.text = "";
		buttonText.text = _tagName;
		//sizeDelta.x = LayoutUtility.GetPreferredWidth(buttonText.GetComponent<RectTransform>());
		//anchorPos.x = sizeDelta.x / 2f;
		buttonRectTransform.sizeDelta = sizeDelta;
		buttonRectTransform.anchoredPosition = anchorPos;

		buttonText.text = atText + colorOpen + mChatMessage.normalMessagerMetaData.username + colorClose;
		descText.text = "<color=gray>" + _tagName + "</color> " + message;
	}

	public void OnClickTagButton ()
	{
		//Debug.Log("OnClickTagButton Name: " + mChatMessage.normalMessagerMetaData.username + "ID: " + mChatMessage.normalMessagerMetaData.userid);
		if (FreakAppManager.Instance.GetHomeScreenPanel() != null)
		{
			FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
			FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
		} 
		//else {
			//FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
		//	FindObjectOfType<FreaksPanelUIController> ().PushPanel (FreaksPanelUIController.PanelType.OtherUserProfilePanel);
		//}
	}
		
}
