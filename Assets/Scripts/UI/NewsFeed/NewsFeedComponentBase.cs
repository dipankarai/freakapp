﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using Freak.Ads;
using Freak;


public class NewsFeedComponentBase : TableViewCell {

//	public string newsFeedID;
	public GameObject myNewsFeedObj;
	public Image profileImage;
//
	public NewsFeedItem feedItem;
	public FacebookNativeAdPanel facebookNativeAdPanel;
//
	public string imageUrl;
//
	public LikeCommentComponent likeCommentObj;
	public GameObject textFeedObj;
	public Text likeCountText;
	public Text commentsCountText;
	public GameObject optionsObj;
//
	public Transform reportPopupParent;
	public ReportDeletePopupController reportDeletePopupObj;
	public Transform reportDeletePosObj;
//
	public UIController uiController;
	public Slider m_cellHeightSlider;
	public InputField postInputField;
	public Image likeImg;

	public GameObject likesDisplayButton;
	public GameObject commentsDisplayButton;

		
	public int rowNumber { get; set; }

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEngine.Events.UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	// Use this for initialization
	void Start () {

	}


	public void SliderValueChanged(Slider slider) {
		float value = slider.value;
		//Debug.Log ("Value --- >" + value);
		onCellHeightChanged.Invoke(rowNumber, value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}
		

	public virtual string GetDataType { 
		get { 
			//Debug.Log ("GetDataType BASECLASS***********");
			return ""; 
		} 
	}

	public virtual void SetData(NewsFeedItem feedItem, float textval, ImageController imageController, Sprite defaultPlaceHolderImage, Transform parentTransform){
	}

	public virtual void SetAdds()
	{

	}
}
