﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;

public class LikeCommentComponent : MonoBehaviour {

	public Text nameText;
	public Text commentText;
	public RectTransform commentTextRect;
	public Image image;
	public Image commentImg;


	public GameObject lastCommentObject;

	public Text CommentNameText;
	public Text commentMsgtxt;
    public Image UserCommentImage;
	public Image gameImage;
	public Button htmlTextButton;
	public Text buttonText;

	public ContentSizeFitter content;
	public CommentDetails mChatMessage;

    public string avatarUrl = string.Empty;
    public string imageUrl = string.Empty;
    public string feedType = string.Empty;

//	public void SetImage(Texture2D tex)
//	{
//		Rect rec = new Rect(0, 0, tex.width, tex.height);
//		Sprite spr = Sprite. Create(tex,rec,new Vector2(0,0),1);
//		commentImg.sprite = spr;
//	}

	public void EnterMessages(CommentDetails _mChatMessage)
	{
		mChatMessage = _mChatMessage;
		if(mChatMessage.normalMessagerMetaData == null || mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.None)
		{
			commentMsgtxt.text = mChatMessage.normalMessagerMetaData.message;
			//CheckTextMessageSize();
		}
		else if (mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.TagFriend)
		{
			TaggedMessage();
		}
		CommentNameText.text = mChatMessage.userName;
	}

//    public void ImageNewsFeed ()
//    {
//        Freak.DownloadManager.Instance.DownLoadWallpaper(imageUrl, Freak.FolderLocation.NewsFeed, LoadImageCallback);
//    }
//
//    void LoadImageCallback (Texture2D texture)
//    {
//        
//    }

    public void OnClickImage(Image img)
	{
        UIController.Instance.imageHandler.loadImage = false;
		UIController.Instance.imageHandler.viewPort.sprite = img.sprite;
		//UIController.Instance.imageHandler.gameObject.SetActive(true);
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.NewsFeedImagePreviewPanel);
	}

    public void DisplayImage ()
    {
        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(avatarUrl, Freak.FolderLocation.Profile, null);
        if (texture != null)
        {
            image.FreakSetSprite(texture);
            texture = null;
        }
        else
        {
            Invoke("DisplayImage", 0.5f);
        }
    }

	private string atText = "<color=grey>@</color>";
	private string colorOpen = "<color=blue>";
	private string colorClose = "</color>";
	void TaggedMessage ()
	{
		htmlTextButton.gameObject.SetActive(true);
		htmlTextButton.onClick.AddListener(OnClickTagButton);
		//RectTransform newrt = htmlTextButton.GetComponent<RectTransform>();
		//newrt.sizeDelta = new Vector2 (100,100);
		string _whiteSpace = "";
		string _tagName = "@" + mChatMessage.normalMessagerMetaData.username;

		Debug.Log ("Display msg --- >" + mChatMessage.normalMessagerMetaData.message);
		commentMsgtxt.text = " " + _tagName + mChatMessage.normalMessagerMetaData.message;
		//CheckTextMessageSize();

		RectTransform textRectTransform = commentMsgtxt.GetComponent<RectTransform>();
		RectTransform buttonRectTransform = htmlTextButton.GetComponent<RectTransform>();

		Vector2 anchorPos = buttonRectTransform.anchoredPosition;
		//anchorPos.y = LayoutUtility.GetPreferredHeight(textRectTransform) / 2f;
		//float offsetsY = commentMsgtxt.fontSize / 2f;
		//anchorPos.y -= offsetsY;

		Vector2 sizeDelta = buttonRectTransform.sizeDelta;

		Text buttonText = htmlTextButton.transform.GetComponentInChildren<Text>();
		buttonText.text = "";
		buttonText.text = _tagName;
		sizeDelta.x = LayoutUtility.GetPreferredWidth(buttonText.GetComponent<RectTransform>());
		anchorPos.x = sizeDelta.x / 2f;

		buttonRectTransform.sizeDelta = sizeDelta;
		buttonRectTransform.anchoredPosition = anchorPos;

		buttonText.text = atText + colorOpen + mChatMessage.normalMessagerMetaData.username + colorClose;
		commentMsgtxt.text = "<color=white>" + _tagName + "</color> " + mChatMessage.normalMessagerMetaData.message;
	}

	public void OnClickTagButton ()
	{
		//Debug.Log("OnClickTagButton Name: " + mChatMessage.normalMessagerMetaData.username + "ID: " + mChatMessage.normalMessagerMetaData.userid);
//		if (FreakAppManager.Instance.GetHomeScreenPanel() != null) {
			FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
			FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);

//		} 
//		else {
//			//FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
//			FindObjectOfType<FreaksPanelUIController> ().PushPanel (FreaksPanelUIController.PanelType.OtherUserProfilePanel);
//		}
	}
}
