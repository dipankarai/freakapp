﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak;

public class PostNewsFeedUI : MonoBehaviour {

	public InputField postInputField;
	public Image profileMsg;
	private ImageController imagecontroller;

	void Start()
	{
		imagecontroller = FindObjectOfType<ImageController> ();
	}


	public void OnClickPostNewsFeed()
	{
		if(!string.IsNullOrEmpty(postInputField.text))
		{
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.postNewsFeedconfirm);
			AlertMessege.Instance.yesButton.onClick.AddListener(OnClickYesPost);
			//AlertMessege.Instance.noButton.onClick.AddListener(OnClickNoPost);

		}
	}

	public void OnClickYesPost()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		FindObjectOfType<NewsFeedApiController> ().PostNewsFeedsAPICall (postInputField.text, "1");
	}

	public void OnClickNoPost()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}

	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
