﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class NewsFeedUIController : MonoBehaviour  {

	public HomeScreenUIPanelController homeScreenUIPanelController;
	int currentCount;

	public List<Contact> dummyContactsList;
	public ImageController imageController;

	private NewsFeedApiController newsFeedApiController;

	//public bool isRightSlide;

	public Toggle allFeedsToggle;
	public Toggle myFeedsToggle;

	public AllFeedsController allFeedsController;
	public MyFeedsController myFeedsController;

	public enum FeedStatus{
		Allfeeds,
		MyFeeds
	}
	FeedStatus feedStatus;

	public GameObject imagePreviewPanel;

	public enum SlideStatus{
		Straight,
		Right,
		Left
	}
	public SlideStatus slideStatus;
	public Transform panelPos;
	public bool isSlide;

	public NewsFeedComponent newsFeedComponent;
	public Text allFeedsText;
	public Outline allFeedsTextOutline;

	public Text myFeedsText;
	public Outline myFeedsTextOutline;



	void Start()
	{
		
		//imageController = FindObjectOfType<ImageController> ();
		//newsFeedApiController = FindObjectOfType<NewsFeedApiController> ();
	}
		

    void OnDisables()
    {
        isSlide = false;
        gameObject.SetActive(false);
        //this.rectTransform().localPosition = panelPos.rectTransform().localPosition;
    }

//	IEnumerator ShowOnEnable()
//	{
//		yield return new WaitForSeconds (0.2f);
//
//		myFeedsController.gameObject.SetActive (false);
//		allFeedsController.gameObject.SetActive (false);
//		isSlide = false;
//		this.gameObject.GetComponent<RectTransform> ().position = panelPos.position;
//		feedStatus = FeedStatus.MyFeeds;
//		homeScreenUIPanelController.newsFeedImagePreviewPanel = imagePreviewPanel;
//		Init ();
//	}

    private bool canTurnLoaderOff = false;
	void OnEnable()
	{
        myFeedsController.gameObject.SetActive (false);
        allFeedsController.gameObject.SetActive (false);
        canTurnLoaderOff = (FreakAppManager.Instance.FeedList.Count > 0);
        FreakAppManager.Instance.isLoaderOn = true;
        myFeedsController.loadingObj.DisplayLoading();
        Freak.Chat.UIController.Instance.imageHandler = imagePreviewPanel.GetComponent<Freak.Chat.ImageHandler>();

        Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
    }

    void InitialCall()
    {
		isSlide = false;
		this.gameObject.GetComponent<RectTransform> ().position = panelPos.position;
		feedStatus = FeedStatus.MyFeeds;
		homeScreenUIPanelController.newsFeedImagePreviewPanel = imagePreviewPanel;
		Init ();
	}


    System.Action OnCompleteSwipeHandler;
    public void SlideScreen(SlideStatus status, System.Action onCompleteAction)
	{
        OnCompleteSwipeHandler = onCompleteAction;

		Vector3 _leftPosition = homeScreenUIPanelController.panaelPosObj.localPosition;
        _leftPosition.x += this.rectTransform().rect.width;

        if (status == SlideStatus.Right) 
		{
            isSlide = true;
            LeanTween.move (this.rectTransform(), _leftPosition, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete(OnDisables);
            slideStatus = SlideStatus.Right;
        } 
        else if (status == SlideStatus.Left) 
        {
//            newsFeedComponent.DisableInputField();
            isSlide = true;
            this.rectTransform().localPosition = _leftPosition;
            LeanTween.move(this.rectTransform(), panelPos.rectTransform().localPosition, 0.4f).setEase(LeanTweenType.easeInSine).setOnComplete(() =>
                {
                    if (OnCompleteSwipeHandler != null)
                    {
                        OnCompleteSwipeHandler();
                        Debug.Log("OnCompleteSwipeHandler..........");
                    }
                });
            slideStatus = SlideStatus.Left;
		} 
	}

	public void Init()
	{
		
		//if (feedItem.imageSprite == null) {
//		imageController.GetImageFromLocalOrDownloadImage (FreakAppManager.Instance.myUserProfile.avatar_url, newsFeedComponent.profileImage, Freak.FolderLocation.Profile);
//		} 
//		else {
//			cell.profileImage.sprite = feedItem.imageSprite;
//		}
		newsFeedComponent.imageUrl = FreakAppManager.Instance.myUserProfile.avatar_url;
		newsFeedComponent.Init();
        myFeedsController.loadingObj.HideLoading();
		if (feedStatus == FeedStatus.MyFeeds) 
		{
			allFeedsToggle.isOn = true;
			myFeedsController.gameObject.SetActive (false);
			allFeedsController.gameObject.SetActive (true);
			feedStatus = FeedStatus.Allfeeds;
			allFeedsText.color = Color.red;
			allFeedsTextOutline.effectColor = Color.red;
			myFeedsText.color = Color.white;
			myFeedsTextOutline.effectColor = Color.white;
		} 
		else {
			myFeedsToggle.isOn = true;
			allFeedsController.gameObject.SetActive (false);
			myFeedsController.gameObject.SetActive (true);
			feedStatus = FeedStatus.MyFeeds;
			allFeedsText.color = Color.white;
			allFeedsTextOutline.effectColor = Color.white;
			myFeedsText.color = Color.red;
			myFeedsTextOutline.effectColor = Color.red;
		}

        if (canTurnLoaderOff)
        {
            FreakAppManager.Instance.isLoaderOn = false;
        }
	}


	public void InitAfterPosting()
	{
		myFeedsController.gameObject.SetActive (false);
		allFeedsController.gameObject.SetActive (false);
		if (feedStatus == FeedStatus.MyFeeds) 
		{
			myFeedsToggle.isOn = true;
			allFeedsController.gameObject.SetActive (false);
			myFeedsController.gameObject.SetActive (true);

			//feedStatus = FeedStatus.Allfeeds;
		} 
		else {
//			myFeedsToggle.isOn = true;
//			allFeedsController.gameObject.SetActive (false);
//			myFeedsController.gameObject.SetActive (true);
//			feedStatus = FeedStatus.MyFeeds;

			allFeedsToggle.isOn = true;
			myFeedsController.gameObject.SetActive (false);
			allFeedsController.gameObject.SetActive (true);
			//feedStatus = FeedStatus.MyFeeds;
		}
	}


	public void OnClickAllFeedsToggle()
	{
		if (allFeedsToggle.isOn) 
		{
			feedStatus = FeedStatus.Allfeeds;
			myFeedsController.gameObject.SetActive(false);
			allFeedsController.gameObject.SetActive(true);
			allFeedsText.color = Color.red;
			allFeedsTextOutline.effectColor = Color.red;
			myFeedsText.color = Color.white;
			myFeedsTextOutline.effectColor = Color.white;
		}
	}

	public void OnClickmyFeedsToggle()
	{
		if (myFeedsToggle.isOn) 
		{
			feedStatus = FeedStatus.MyFeeds;
			allFeedsController.gameObject.SetActive(false);
			myFeedsController.gameObject.SetActive(true);
			allFeedsText.color = Color.white;
			allFeedsTextOutline.effectColor = Color.white;
			myFeedsText.color = Color.red;
			myFeedsTextOutline.effectColor = Color.red;
			Debug.Log("News Feed My News");
		}
	}


	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
//        newsFeedComponent.DisableInputField();
	}

	public void OnClickChatButton()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
	}


	public void OnClickOptionsButton()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}
		
		
	void OnDisable()
	{
		FreakAppManager.Instance.FeedList.Clear ();
		Resources.UnloadUnusedAssets ();

		allFeedsController.gameObject.SetActive(false);
		myFeedsController.gameObject.SetActive (false);
        //m_tableView.ReloadData ();
	}
		
}





