﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


namespace Freak.Chat
{
	public class CommentInputFieldPanel : MonoBehaviour
	{
		[SerializeField] private InputField messageInputField;
		NativeEditBox messageNativeinputField;

		[HideInInspector] public ChatWindowUI chatWindowUI;
		private TagFriendInCommentController memberSearch;
		public Transform parentObj;
		public NewsFeedCommentsUIPanel newsFeedCommentsUIPanel;
		private List<ServerUserContact> tempContactsList = new List<ServerUserContact> ();
		public ConnectingUI loadingObj;


		//public list
		// Use this for initialization
		void Start () 
		{
			//TODO: For timing we are using single line message
			messageInputField.lineType = InputField.LineType.SingleLine;
			messageNativeinputField = messageInputField.gameObject.GetComponent<NativeEditBox> ();

		}

		private bool isTypingStarted;
		private float typingStartInterval;

		// Update is called once per frame
		void Update () {

			if (Input.GetKeyDown(KeyCode.Escape))
			{
				chatWindowUI.OnEscapeButtonPressed();
			}

			if (messageInputField.isFocused)
			{
				//chatWindowUI.OnFousedInputField(true);

				if(messageInputField.text.Length > 0 && isTypingStarted == false)
				{
					SendTypingStart();
				}
			}
			else
			{
				if (isTypingStarted)
				{
					SendTypingEnd();
				}
			}
		}

		public void OnValueChangeMessageInput (string text)
		{

			//if (chatWindowUI.chatUser.isGroupChat)
			//{
				if (string.IsNullOrEmpty(taggedString) == false)
				{
					CheckForTaggedMember(text);
				}

				if (TagSearchCondition(text))
				{
					TagSearchMember(text);
				}
				else
				{
					if (memberSearch != null)
					{
						Destroy (memberSearch.gameObject);
					}
				}
			//}
		}


		void SendTypingStart()
		{
			isTypingStarted = true;

		}

		void SendTypingEnd()
		{
			isTypingStarted = false;
		}

		void TagSearchMember (string searchText)
		{
			searchText = searchText.Replace("@", string.Empty);

            List<ServerUserContact> serverContactList = new List<ServerUserContact>();
            serverContactList = new List<ServerUserContact>(FreakAppManager.Instance.GetAllServerUserInfo());

            for (int i = 0; i < serverContactList.Count; i++)
            {
                string _name = serverContactList[i].name;
                if (_name.ToUpper ().Contains (searchText.ToUpper ())) {
                    Debug.Log("Name:: " + _name + "Searched:: " + searchText);
                    if (tempContactsList.Contains(serverContactList[i]) == false)
                    {
                        tempContactsList.Add(serverContactList[i]);
                    }
                }
            }

            /*for (int i = 0; i < FreakAppManager.Instance.userSynchedContacts.Count; i++)
			{
				string _name = FreakAppManager.Instance.userSynchedContacts[i].name;
				if (_name.ToUpper ().Contains (searchText.ToUpper ())) {
					Debug.Log("Name:: " + _name + "Searched:: " + searchText);
					if (tempContactsList.Contains(FreakAppManager.Instance.userSynchedContacts[i]) == false)
					{
						tempContactsList.Add(FreakAppManager.Instance.userSynchedContacts[i]);
					}
				}
			}*/

			for (int i = 0; i < tempContactsList.Count; i++)
			{
				string _name = tempContactsList[i].name;
				if (_name.ToUpper ().Contains (searchText.ToUpper ()) == false) {
					Debug.Log("Name== " + _name + "Searched== " + searchText);
					tempContactsList.RemoveAt(i--);
				}
			}

			if (memberSearch == null)
			{
                GameObject go = Instantiate(UIController.Instance.assetsReferences.TagFriendPopup) as GameObject;
                memberSearch = go.GetComponent<TagFriendInCommentController>();
				memberSearch.gameObject.transform.SetParent (parentObj, false);
			}

			memberSearch.searchedMemberList = tempContactsList;
			memberSearch.m_tableView.ReloadData();
		}

		bool TagSearchCondition (string text)
		{
			if (text.Length == 0)
				return false;

			if (text.Substring(0, 1).Equals("@"))
				return true;

			return false;
		}

		public void OnClickSendButton ()
		{
			messageNativeinputField.InputReciver.OnBackButtonPress ();
			if (messageInputField.text.Length > 0)
			{
				if(tagMetaData != null)
				{
					tagMetaData.message = EncoedeString( messageInputField.text.Replace(taggedString, string.Empty));
					SendMessageWithData(tagMetaData);
					taggedString = string.Empty;
					tagMetaData = null;
				}
				else
				{
					FreakChatSerializeClass.NormalMessagerMetaData _meta_Data = new FreakChatSerializeClass.NormalMessagerMetaData();
					_meta_Data.messageType = ChatMessage.MetaMessageType.None;
					_meta_Data.message = EncoedeString( messageInputField.text);
					string _metaDataString = Newtonsoft.Json.JsonConvert.SerializeObject(_meta_Data);
					newsFeedCommentsUIPanel.AddCommentFeedAPICall (_metaDataString, FreakAppManager.Instance.myUserProfile.feedId);
				}

				if (loadingObj != null) {
					loadingObj.gameObject.SetActive (true);
					loadingObj.DisplayLoading ();
				}
				messageInputField.text = string.Empty;
				messageNativeinputField.SetTextNative (string.Empty);

				if (isTypingStarted)
				{
					SendTypingEnd();
				}
			}
		}
		public string EncoedeString(string inputstring)
		{
			return WWW.EscapeURL (inputstring);
		}

        public void SendMessageWithData(FreakChatSerializeClass.NormalMessagerMetaData metaDataMessage, string msg = "")
        {
			if (string.IsNullOrEmpty(msg))
			{
				msg = metaDataMessage.fileType.ToString();
			}

			string _metaDataString = Newtonsoft.Json.JsonConvert.SerializeObject(metaDataMessage);
			//SendBirdSDK.SendMessageWithData(msg, _metaDataString);
			newsFeedCommentsUIPanel.AddCommentFeedAPICall (_metaDataString, FreakAppManager.Instance.myUserProfile.feedId);
		}


		public void OnSendTagFriend (FreakChatSerializeClass.NormalMessagerMetaData metaData)
		{
			Debug.Log("OnSendTagFriend");

			tagMetaData = metaData;

			if (memberSearch != null)
			{
				Destroy (memberSearch.gameObject);
			}

			taggedString = /*atText +*/ colorOpen + tagMetaData.username + colorClose;
			messageInputField.text = messageInputField.text + taggedString + " ";
			Debug.Log("OnSendTagFriend ============================== >>"+ messageInputField.text);
			messageInputField.caretPosition = messageInputField.text.Length;
		}

		FreakChatSerializeClass.NormalMessagerMetaData tagMetaData;
		private string atText = "<color=grey>@</color>";
		//private string atText = "<#dbeeff>@</color>";
		private string colorOpen = "<color=blue>";
		//private string colorOpen = "<#38a2ff>";
		private string colorClose = "</color>";
		private string taggedString;
		private string tempText;
		void CheckForTaggedMember(string text)
		{
			if (text.Contains(taggedString))
			{
				tempText = text.Replace(taggedString,string.Empty);
			}
			else
			{
				taggedString = string.Empty;
				tagMetaData = null;

				//Chekcing for whie space in first charatcer
				if(tempText.Substring(0,1) == " ")
				{
					tempText = tempText.Remove(0, 1);
				}
				messageInputField.text = tempText;
				messageNativeinputField.SetTextNative (tempText);
			}
		}

	}
}
