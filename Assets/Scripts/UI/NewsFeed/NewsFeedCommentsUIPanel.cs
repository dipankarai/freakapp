﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Chat;
using System;
using MiniJSON;
using System.Linq;

public class NewsFeedCommentsUIPanel : MonoBehaviour, ITableViewDataSource  {

	public HomeScreenUIPanelController homeScreenUIPanelController;
	public ImageController imageController;
	int currentCount;

	public List<Contact> dummyContactsList;

	public NewsFeedCommentContent m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	public InputField commentTextField;
	public int commentId;


	private string lastCommentId = "0";

	public List<CommentDetails> CommentsList = new List<CommentDetails>();
	private float extraspace = 100;
 	public CommentInputFieldPanel commentInputFieldPanel;

	public ConnectingUI loadingObj;
	public Sprite defaultPlaceHolderImage;
	[SerializeField] private ScrollRect scrollRect;
	private bool isPullToRefresh;
	private int maxLimit = 10;
	private int currentLimit = 0;


	void OnEnable()
	{
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
		m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);
		loadingObj.DisplayLoading ();
		CommentsList.Clear ();
		GetNewsFeedsCommentsAPICall (lastCommentId, FreakAppManager.Instance.myUserProfile.feedId, maxLimit, loadingObj.gameObject);
		currentLimit = maxLimit;
	}

	void EnableTableView()
	{
		isPullToRefresh = false;
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
		m_tableView.ReloadData ();
		m_tableView.scrollY = 0;
	}

	void OnScrollDrag(bool isDrag)
	{
		//LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
		if(!isDrag && !isPullToRefresh)
		{
			//Debug.Log(" 11111111111111 Scroll Draaaaaaag CLASS Rfresh News Feed" +  scrollRect.verticalNormalizedPosition);
			if (scrollRect.verticalNormalizedPosition < 0 || scrollRect.verticalNormalizedPosition > 1.0f || scrollRect.verticalNormalizedPosition <= -0.01f) 
			{
				isPullToRefresh = true;
				currentLimit += 10;
				//Debug.Log("Scroll Draaaaaaag CLASS Rfresh News Feed" );
				loadingObj.DisplayLoading ();
				//m_tableView.scrollY = m_tableView.scrollableHeight;
				GetNewsFeedsCommentsAPICall (lastCommentId, FreakAppManager.Instance.myUserProfile.feedId, currentLimit, loadingObj.gameObject);

			}
		}
	}

	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}
		

	public void OnClickOptions()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}


	public void OnClickAllChats()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
	}

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return CommentsList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		NewsFeedCommentContent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as NewsFeedCommentContent;
		if (cell == null) {
			cell = (NewsFeedCommentContent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		return cell;
	}


	public void TagFriendSelected (ServerUserContact mMember)
	{
		FreakChatSerializeClass.NormalMessagerMetaData _meta_Data = new FreakChatSerializeClass.NormalMessagerMetaData();
		_meta_Data.messageType = ChatMessage.MetaMessageType.TagFriend;
		_meta_Data.userid = mMember.user_id.ToString();
		_meta_Data.username = mMember.name;

		commentInputFieldPanel.OnSendTagFriend(_meta_Data);
	}


	void AssignDetails(NewsFeedCommentContent cell, int rowNumb)
	{
		CommentDetails comment = CommentsList [rowNumb];
		cell.nameText.text = comment.userName;
		cell.EnterMessages (comment);
	
//		string _text = cell.descText.text;
//		TextGenerator textGen = new TextGenerator (_text.Length);
//		Vector2 extents = cell.descText.gameObject.GetComponent<RectTransform>().rect.size;
//		textGen.Populate (_text, cell.descText.GetGenerationSettings (extents));
//		Debug.Log ("extents SIZE --- >"+ textGen.lineCount);

		cell.commentID = comment.news_feed_comment_id;
		if (comment.user_id.Equals (FreakAppManager.Instance.userID.ToString()))
		{
			cell.deleteBtn.SetActive (true);
		}
		else
		{
			cell.deleteBtn.SetActive (false);
		}

//		Debug.Log ("GetLine Count --- >" + GetLineCount (cell.descText));
//		float val = GetLineCount (cell.descText);
//		float contentHeight = val * 24 + extraspace;
//		RectTransform textRext = cell.descText.GetComponent<RectTransform> ();
//		Rect newrect = textRext.rect;
//		textRext.sizeDelta = new Vector2(textRext.sizeDelta.x, val * 24);

//
//		cell.m_cellHeightSlider.minValue = contentHeight;
//		cell.m_cellHeightSlider.maxValue = contentHeight;
//		cell.height = contentHeight;

		cell.m_cellHeightSlider.maxValue = SetTextRext(cell.descText, cell.commentTextRect) + 120;
			
		cell.height = cell.m_cellHeightSlider.maxValue;
//		int val = cell.descText.text.Length;
//		//Debug.Log ("CONTENT SIZE PREFERD SIZE -- >" +  val);
//		float newval = val / 20;
//		cell.m_cellHeightSlider.minValue = newval * 24 + 50;
//		cell.m_cellHeightSlider.maxValue = newval * 24 + 50;
//
//		cell.height = newval * 24 + 50;


		//cell.descText.transform.localPosition = cell.textPos.transform.localPosition;
		cell.profileImage.sprite = defaultPlaceHolderImage;
		if (imageController == null)
		{
			imageController = FindObjectOfType<ImageController> ();
		}
		imageController.GetImageFromLocalOrDownloadImage (comment.avatarUrl, cell.profileImage, Freak.FolderLocation.Profile);

		//cell.PrintPos ();

		OnCellHeightChanged (rowNumb, cell.height);

		//		ServerUserContact cont = getContactListApiManager.serverContactList [rowNumb];
		//
		//		if (!string.IsNullOrEmpty (getContactListApiManager.serverContactList [rowNumb].userName)) {
		//			cell.nameText.text = getContactListApiManager.serverContactList [rowNumb].userName;
		//		} else {
		//			cell.nameText.text = "Unknown Contact " + rowNumb;
		//		}
		//		cell.numberText.text = cont.contactNumb;
		//		cell.IntUser (cont.user_id.ToString(), cont.userName);
	}
		

//	public float SetTextRext(Text textObj)
//	{
//		Debug.Log ("GetLine Count --- >" + GetLineCount (textObj));
//		float val = GetLineCount (textObj);
//		float contentHeight = val * 24 + 50;
//		RectTransform textRext = textObj.GetComponent<RectTransform> ();
//		Rect newrect = textRext.rect;
//		textRext.sizeDelta = new Vector2 (textRext.sizeDelta.x, val * 24);
//		return contentHeight;
//	}
//
//
//	public int GetLineCount (Text descText)
//	{
//		string text = descText.text;
//		TextGenerator textGen = new TextGenerator (text.Length);
//		Vector2 extents = descText.gameObject.GetComponent<RectTransform>().rect.size;
//		textGen.Populate (text, descText.GetGenerationSettings (extents));
//		//float kText = textGen.GetCharactersArray ();
//		Debug.Log ("extents SIZE --- >" + textGen.lineCount + "sasass -- >" + textGen.GetCharactersArray ()); 
//		return textGen.lineCount;
//
//	}

	public float SetTextRext(Text textObj, RectTransform textRect)
	{
		//RectTransform textRect = textObj.GetComponent<RectTransform> ();
		string newtext = textObj.text;
		TextGenerator textGen = new TextGenerator (newtext.Length);
		Vector2 extents = textRect.rect.size;
		textGen.Populate (newtext, textObj.GetGenerationSettings (extents));
		float val = textGen.lineCount;
		float contentHeight = val * 42 + 50;
		Rect newrect = textRect.rect;
		textRect.sizeDelta = new Vector2 (textRect.sizeDelta.x, val * 42);
		Debug.Log("lineCount -- >"+ val + "contentHeight -- >"+ contentHeight + "text -- >"+ newtext);
		return contentHeight;
	}


	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}
		

	void OnDisable()
	{
		Destroy (this.gameObject);
	}


	private GameObject LoadingObj;
	/// <summary>
	/// Post News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetNewsFeedsCommentsAPICall(string last_news_feed_comment_id, string news_feed_id, int commentsLimit, GameObject _loadingObj = null)
	{
		LoadingObj = _loadingObj;
		FreakApi api = new FreakApi ("newsFeed.getComment", ServerResponseForGetNewsFeedsCommentsAPICall);
		api.param ("news_feed_id", news_feed_id);
		api.param ("last_news_feed_comment_id", last_news_feed_comment_id);
		api.param ("limit", commentsLimit);
		api.get (this);

	}


	void ServerResponseForGetNewsFeedsCommentsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		if(LoadingObj != null)
		LoadingObj.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				List<object> content = (List<object>)userDetailsDict ["contents"];
				lastCommentId = userDetailsDict ["last_news_feed_comment_id"].ToString ();
				for (int i = 0; i < content.Count; i++) {
					Dictionary<string,object> newdata = (Dictionary<string,object>)content [i];
					CommentDetails newComments = new CommentDetails (newdata);
					CommentsList.Add (newComments);
				}
				EnableTableView ();
				m_tableView.ReloadData ();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}


	//Newsfeed Add comment
	public void AddCommentFeedAPICall(string comment, string news_feed_id)
	{
		string newstr = EncoedeString (comment);
		loadingObj.DisplayLoading ();
		FreakApi api = new FreakApi ("newsFeed.addComment", ServerResponseForPostNewsFeedsAPICall);
		api.param ("userId", FreakAppManager.Instance.userID);
		api.param ("news_feed_id", news_feed_id);
		api.param ("comment", comment);
		api.get (this);

	}


	void ServerResponseForPostNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		loadingObj.gameObject.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				loadingObj.gameObject.SetActive (false);
				GetNewsFeedsCommentsAPICall (lastCommentId, FreakAppManager.Instance.myUserProfile.feedId, 10);
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

	public string EncoedeString(string inputstring){

		return WWW.EscapeURL (inputstring);
	}


	//Newsfeed Like comment
	public void DeleteCommentAPICall(string news_feed_comment_id)
	{
		loadingObj.DisplayLoading ();
		FreakApi api = new FreakApi ("newsFeed.deleteComment", ServerResponseForLikeCommentFeedAPICall);
		api.param ("news_feed_comment_id", news_feed_comment_id);
		api.get (this);

	}


	void ServerResponseForLikeCommentFeedAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		loadingObj.gameObject.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				CommentsList.Clear ();
				GetNewsFeedsCommentsAPICall ("0", FreakAppManager.Instance.myUserProfile.feedId, 10);
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

}

public class CommentDetails
{
	public string news_feed_comment_id;
	public string user_id;
	public string userName;
	public string avatarUrl;
	public string avatarName;
	private Dictionary<string,object> commentData;
	public NormalMessagerMetaData normalMessagerMetaData;

	public CommentDetails (Dictionary<string,object> data)
	{
		news_feed_comment_id = data["news_feed_comment_id"].ToString();
		user_id = data["user_id"].ToString();
		string comment = data["comment"].ToString();
		//commentData = (IDictionary)Json.Deserialize(comment);
		commentData = (Dictionary<string,object>) Json.Deserialize(comment);
		if (commentData != null) {
//			Debug.Log ("Comments ---- > " + commentData.GetType ());
			normalMessagerMetaData = new NormalMessagerMetaData ();
			if (commentData.ContainsKey ("messageType")) 
			{
				int type = int.Parse (commentData ["messageType"].ToString ());
				if (type == 0) 
				{
					normalMessagerMetaData.messageType = ChatMessage.MetaMessageType.None; 
				} 
				else if (type == 3) 
				{
					normalMessagerMetaData.messageType = ChatMessage.MetaMessageType.TagFriend; 
					normalMessagerMetaData.userid = commentData ["userid"].ToString ();
					normalMessagerMetaData.username = DecodeString( commentData ["username"].ToString ());
				}
			}
			normalMessagerMetaData.message = DecodeString(commentData ["message"].ToString ());

		}

		if (data.ContainsKey ("name")) {
			userName = DecodeString( data ["name"].ToString ());
		}
		avatarUrl = data["avatar_url"].ToString();
		avatarName = data["avatar_name"].ToString();
	}

	string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}
}

[Serializable]
public class NormalMessagerMetaData
{
	public ChatMessage.MetaMessageType messageType = ChatMessage.MetaMessageType.None;
	public string message;
	public string userid;
	public string username;
}




