﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using Freak.Ads;
using Freak;


public class NewsFeedComponent : NewsFeedComponentBase {

	public string newsFeedID;

	private string like = "1";
	private string dislike = "2";

	public string feedUserId;

	private NativeEditBox postNativeField;

	public enum LikeStatus{
		Default,
		AlreadyLiked,
		Unlike
	}
	public LikeStatus likeStatus;

	public string feedType;

	private Color likeColor = new Color (204f / 255f, 60f / 255f, 60f / 255f);

	public override string GetDataType {
		get {
			Debug.Log(" Child Text Return type -- "+ feedType);
			return feedType;
		}
	}

	public override string reuseIdentifier {
		get {
			//Debug.Log(" Child Text Return type ==== "+ feedType);
			return feedType;
		}
	}
		

	public override void SetData(NewsFeedItem feedItem, float textval, ImageController imageController, Sprite defaultPlaceHolderImage, Transform parentTransform)
	{
		
			myNewsFeedObj.SetActive (false);
			likeCommentObj.gameObject.SetActive (true);
			facebookNativeAdPanel.gameObject.SetActive (false);

			likeCommentObj.commentImg.rectTransform.sizeDelta = new Vector2 (0, -50);
			likeCommentObj.image.sprite = defaultPlaceHolderImage;
			likeCommentObj.avatarUrl = feedItem.avatarUrl;
			imageUrl = feedItem.avatarUrl;
			likeCommentObj.DisplayImage();
			//loadProfileImage (feedItem.avatarUrl, cell.likeCommentObj.image, cell);
			m_cellHeightSlider.maxValue = 237f;
			if (!string.IsNullOrEmpty (feedItem.feedType)) {
				likeCommentObj.feedType = feedItem.feedType;

				if (feedItem.feedType.Equals ("1")) {
					m_cellHeightSlider.maxValue = textval + 230;
				} else {
					likeCommentObj.commentImg.sprite = null;
					//imageController.GetImageFromLocalOrDownloadImageForNewsFeed (feedItem.imageUrl, likeCommentObj.commentImg, Freak.FolderLocation.NewsFeed, this, textval);
					//Debug.Log("NewsFeed SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
				}

				//height = m_cellHeightSlider.maxValue;
				height = m_cellHeightSlider.maxValue;
				m_cellHeightSlider.value = m_cellHeightSlider.maxValue;
				SliderValueChanged (m_cellHeightSlider);
			}

			reportPopupParent = parentTransform;
			newsFeedID = feedItem.newsfeedId;
			feedItem = feedItem;
			likeCommentObj.nameText.text = feedItem.userName;
			feedUserId = feedItem.userId;

			SetLikes (int.Parse (feedItem.likeCount));
			SetComments (feedItem.totalCommentCount);
			if (feedItem.isLiked) {
				SetLikeCountClour (true);
			} else {

				SetLikeCountClour (false);
			}

			likeCommentObj.commentText.text = feedItem.feedDataText.feedText;

        if (feedItem.feedType == "4")
        {
            // Load Game Image
            freakGameName = feedItem.feedDataText.feedText;
//            Debug.Log("freakGameName " + freakGameName);
            likeCommentObj.gameImage.FreakSetSprite(Resources.Load<Texture2D>("FreaK_Log"));
            Invoke("LoadGameIcon", 0.1f);
        }
	}

    private string freakGameName;
    void LoadGameIcon ()
    {
        if  (FreakAppManager.Instance.inventoryGamesList.Count > 0)
        {
            for (int i = 0; i < FreakAppManager.Instance.inventoryGamesList.Count; i++)
            {
//                Debug.Log("freakGameName " + freakGameName + " :::: " + FreakAppManager.Instance.inventoryGamesList[i].title);
                if (freakGameName.Contains (FreakAppManager.Instance.inventoryGamesList[i].title))
                {
//                    Debug.Log("FreakAppManager.Instance.inventoryGamesList[i].title " + FreakAppManager.Instance.inventoryGamesList[i].title);
                    if (FreakAppManager.Instance.inventoryGamesList[i].gameTexture != null)
                    {
                        likeCommentObj.gameImage.FreakSetSprite(FreakAppManager.Instance.inventoryGamesList[i].gameTexture);
                    }
                    else
                    {
                        likeCommentObj.gameImage.FreakSetSprite(Resources.Load<Texture2D>("FreaK_Log"));
                        Freak.DownloadManager.Instance.DownloadStoreGameTexture(FreakAppManager.Instance.inventoryGamesList[i].description_imagePath, FreakAppManager.Instance.inventoryGamesList[i]);
                        Invoke("LoadGameIcon", 0.5f);
                    }
                }
            }
        }
    }

	public override void SetAdds()
	{
		
		facebookNativeAdPanel.LoadAds(feedItem.facebooknativeAd);
		facebookNativeAdPanel.gameObject.SetActive(true);
//		Debug.Log("cell.facebookNativeAdPanel.RectTransform.rect.height " + facebookNativeAdPanel.PanelHeight);
		m_cellHeightSlider.maxValue = facebookNativeAdPanel.PanelHeight;

	}


	public void OnClickOptionsArrow()
	{
		ReportDeletePopupController reportDeletePopup = Instantiate (reportDeletePopupObj) as ReportDeletePopupController;
		reportDeletePopup.newsFeedID = newsFeedID;
		reportDeletePopup.gameObject.transform.SetParent (reportPopupParent, false);
		if (feedUserId.Equals (FreakAppManager.Instance.userID.ToString()))
		{
			reportDeletePopup.EnableDelete ();
		}
		else 
		{
			reportDeletePopup.EnableReport ();
		}
		reportDeletePopup.reportDeleteObj.transform.position = reportDeletePosObj.transform.position;
	}


	public void OnClickPostNewsFeed()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.PostNewsFeedPanel);
	}


	public void OnClickPostImageNewsFeed()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.PostImageNewsFeedPanel);
	}


	public void OnClickOptions(int buttonName)
	{
		if (buttonName == 1) 
		{
			FindObjectOfType<NewsFeedApiController> ().ReportNewsFeedAPICall (newsFeedID, "");
		}
		else 
		{
			
		}

		optionsObj.SetActive (false);
	}
		
	public void OnClickLike()
	{
		Debug.Log ("ONCLICK LIKE");
		if (likeStatus == LikeStatus.Default || likeStatus == LikeStatus.Unlike) 
		{
			FindObjectOfType<NewsFeedApiController> ().LikeCommentFeedAPICall (like, newsFeedID, IncreaseLikeCount);
		} 
		else if (likeStatus == LikeStatus.AlreadyLiked) 
		{
			FindObjectOfType<NewsFeedApiController> ().LikeCommentFeedAPICall (dislike, newsFeedID, DecreaseLikeCount);
		}

	}

	void IncreaseLikeCount(bool isSuccess)
	{
		if (isSuccess)
		{
			for (int i = 0; i < FreakAppManager.Instance.FeedList.Count; i++) 
			{
				if (FreakAppManager.Instance.FeedList [i].Equals (feedItem)) 
				{
					int newLikeCount = int.Parse (FreakAppManager.Instance.FeedList [i].likeCount);
					newLikeCount += 1; 
					FreakAppManager.Instance.FeedList [i].likeCount = newLikeCount.ToString ();
					FreakAppManager.Instance.FeedList [i].isLiked = true;
					SetLikes (newLikeCount);
				}
			}

			likeImg.color = likeColor;
			likeStatus = LikeStatus.AlreadyLiked;

		}
	}
		

	void DecreaseLikeCount(bool isSuccess)
	{
		if (isSuccess)
		{
			for (int i = 0; i < FreakAppManager.Instance.FeedList.Count; i++) 
			{
				if (FreakAppManager.Instance.FeedList [i].Equals (feedItem)) 
				{
					int newLikeCount = int.Parse (FreakAppManager.Instance.FeedList [i].likeCount);
					newLikeCount -= 1; 
					FreakAppManager.Instance.FeedList [i].likeCount = newLikeCount.ToString ();
					FreakAppManager.Instance.FeedList [i].isLiked = false;
					SetLikes (newLikeCount);
				}
			}

			likeImg.color = Color.white;
			likeStatus = LikeStatus.Unlike;
		}
	}

	public void SetLikeCountClour(bool alreadyLiked)
	{
		if (alreadyLiked) 
		{
			
			likeImg.color = likeColor;
			likeStatus = NewsFeedComponent.LikeStatus.AlreadyLiked;
		}
		else 
		{
			likeImg.color = Color.white;
			likeStatus = NewsFeedComponent.LikeStatus.Default;
		}

	}


	public void OnClickComment()
	{
//		Debug.Log ("ONCLICK COMMENT");
		FreakAppManager.Instance.myUserProfile.feedId = newsFeedID;
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.NewsFeedCommentPanel);
	}

	public void OnClickShare()
	{
//		Debug.Log ("ONCLICK SHARE");
		AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.shareNewsFeedconfirm);
		AlertMessege.Instance.yesButton.onClick.AddListener(OnClickYesShare);

	}

	public void OnClickDisplayLikes()
	{
		//		Debug.Log ("ONCLICK SHARE");
		FreakAppManager.Instance.myUserProfile.feedId = newsFeedID;
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.LikesDislayPanel);

	}


	public void OnClickYesShare()
	{
		//FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		//FindObjectOfType<NewsFeedApiController> ().PostNewsFeedsAPICall (postInputField.text, "1");
		FindObjectOfType<NewsFeedApiController> ().ShareNewsFeedAPICall (newsFeedID);
	}



	public void SetLikes(int newlikes)
	{
//		Debug.Log ("gsvhasaghs LIKESSSS ---- > " + newlikes);
		likeCountText.text = "";
		if (newlikes > 0)
		{
			likesDisplayButton.SetActive (true);
			if (newlikes < 2) {
				likeCountText.text = newlikes.ToString () + "Like";
			} else {
				likeCountText.text = newlikes.ToString() + " Likes";
			}
		} 
		else 
		{
			likesDisplayButton.SetActive (false);
		}
	}

	public void SetComments(string count)
	{

		commentsCountText.text = "";
		if (int.Parse(count) > 0) {
			commentsDisplayButton.SetActive (true);
			if (int.Parse(count) < 2)
			{
				commentsCountText.text = count + " Comment";
			}
			else {
				commentsCountText.text = count + " Comments";
			}
		}
		else 
		{
			commentsDisplayButton.SetActive (false);
		}
//		Debug.Log ("Commnets Count ---- > " + count);
//		if (count > 1)
//		{
//			commentsCountText.text = count + " Comments";
//		} 
//		else 
//		{
//			if (count > 0)
//			{
//				commentsCountText.text = count + " Comment";
//			}
//		}
	}


	public void OnClickPostNewsFeeds()
	{
		postNativeField.InputReciver.OnBackButtonPress ();
		if(!string.IsNullOrEmpty(postInputField.text))
		{
			postNativeField.SetEnabled (false);
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.postNewsFeedconfirm);
			AlertMessege.Instance.yesButton.onClick.AddListener(OnClickYesPost);
			AlertMessege.Instance.noButton.onClick.AddListener(OnClickNoPost);

		}
	}

	public void OnClickYesPost()
	{
		//FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		postNativeField.SetEnabled (true);
		FindObjectOfType<NewsFeedApiController> ().PostNewsFeedsAPICall (postInputField.text, "1");
		AlertMessege.Instance.yesButton.onClick.RemoveListener(OnClickYesPost);
		postNativeField.SetTextNative("");
	}

	public void OnClickNoPost()
	{
		postNativeField.SetEnabled (true);
//		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}
	void OnEnable()
	{
		postNativeField = postInputField.gameObject.GetComponent<NativeEditBox> ();

        postNativeField.SetEnabled (true);
        postNativeField.SetTextNative ("");
	}
	public void Init()
	{
        postNativeField.gameObject.SetActive (false);
        postNativeField.gameObject.SetActive (true);
        postNativeField.SetEnabled (true);
        postNativeField.SetTextNative ("");
	}


	public void InitForNewsFeed()
	{
		postNativeField.gameObject.SetActive (true);
		postNativeField.SetEnabled (true);
	}

    public void DisplayImage ()
    {
        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(imageUrl, FolderLocation.Profile, null);
        if (texture != null)
        {
            profileImage.FreakSetSprite(texture);
            texture = null;
        }
        else
        {
            Invoke("DisplayImage", 0.5f);
        }
    }

    public void DisableInputField ()
    {
        postNativeField.SetEnabled (false);
        postNativeField.SetTextNative("");
    }

	public void DisableInputFieldOnSettingsPanel ()
	{
		postNativeField.gameObject.SetActive (false);
	}
}
