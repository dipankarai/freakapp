﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak;

public class WelcomePanelController : MonoBehaviour {

	public InputField mobileNumberInputField;
	public InputField countryCodeInputField;

	public Text selectCountryText;
	public Toggle toggleButton;

	private string selectCountryName;
	private int selectCountryID;
	private string initialSelectedCountry;

	private string failString;
	public ConnectingUI loadingObj;

	void Start()
	{
		initialSelectedCountry = selectCountryText.text;
	}


	public void SelectCountry()
	{
		FindObjectOfType<UIPanelController> ().PushPanel (UIPanelController.PanelType.SelectCountryPanel);
	}

	public void OnClickGo()
	{
		//#if !UNITY_EDITOR

		//#else

		if (CheckifCountrySelected ()) 
		{
			if (CheckifMobileEntered ())
			{
				if (toggleButton.isOn) 
				{
					loadingObj.DisplayLoading();
					FindObjectOfType<UserAuthentication> ().UserLogin ("", selectCountryID, mobileNumberInputField.text);
				} else {
					AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Accept Terms and Conditions");
				}
			}
		}
		//#endif
	}

	bool CheckifCountrySelected()
	{
		if (!selectCountryText.Equals (initialSelectedCountry) && !string.IsNullOrEmpty(countryCodeInputField.text)) {
			return true;
		} else {
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Select your Country");
		}
		return false;
	}


	bool CheckifMobileEntered()
	{
		if (!string.IsNullOrEmpty (mobileNumberInputField.text)) {
			return true;
		} else {
			AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Check mobile number");
		}
		return false;
	}


	public void SetCountryDetails(SelectCountryItem item)
	{
		selectCountryName = item.nameText.text;
		selectCountryID = item.iD;
		countryCodeInputField.text = "+"+item.iD.ToString ();
		selectCountryText.text = item.nameText.text;
	}


	public void OnClickTermCondtion()
	{
		
	}

	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;

//		if (userAuthentication.HasAlredayRegistered()) 
//		{
//			Debug .Log("NUMBER DATA TO SYNC-----");
//			SceneManager.LoadScene("FreakHomeScene");
//		} 
//		else {
//			ShowInitialWelcomePanel ();
//		}

		if(CheckifCountrySelected() && CheckifMobileEntered() && toggleButton.isOn)
			FindObjectOfType<UserAuthentication>().UserLogin("", selectCountryID, mobileNumberInputField.text);
	}

}
