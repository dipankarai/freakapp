﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft;


public class ContactDisplayUIComponent : TableViewCell {

	public Text m_rowNumberText;
	public Slider m_cellHeightSlider;

	public string phoneNumb;
	public int rowNumber { get; set; }

	public string rowContent { get; set; }
	public string iD{ get; set; }

	public List<ContactDetailsDisplay> contactDetailsList;

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	void Start(){
		//this.gameObject.GetComponent<LayoutElement> ().preferredHeight = 165;
	}
	void Updates() {
		m_rowNumberText.text = "Row " + rowNumber.ToString();
	}

	public void SliderValueChanged(Slider slider) {
		float value = slider.value;
		Debug.Log ("Value --- >" + value);
		onCellHeightChanged.Invoke(rowNumber, value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}
}
