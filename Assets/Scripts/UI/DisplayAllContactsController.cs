﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DisplayAllContactsController : MonoBehaviour {

	public List<ServerUserContact> serverContactList;

	public DisplayContacts displayContacts;
	public DisplayContactsInPanel displayContactsInPanel;

	public GameObject searchBar;
	public InputField searchBarTextField;
	public GameObject searchBtn;
	public GameObject RowColumnBtn;
	public List<Contact> tempContacts = new List<Contact>();

	public Image rowColumImage;
	public Sprite rowSprite;
	public Sprite columnSprite;

    public Text noContactsText;

	public List<Contact> dummyContactsList;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;
	public ConnectingUI loadingObj;

	enum Status{
		Grid,
		Panel
	}
	Status status;
	Vector3 bottomPanelPosition;

	void OnEnable()
	{
		bottomPanelPosition = searchBar.transform.rectTransform ().localPosition;
        if (FreakAppManager.Instance.GetAllContacts().Count > 0)
        {
            OnEnableView (true);
        }
        else
        {
            loadingObj.DisplayLoading ();
            FindObjectOfType<SyncContactsToServer> ().GetPhoneContacts (OnEnableView);
        }

        /*if (Contacts.ContactsList.Count > 0) 
		{
			OnEnableView (true);
		} 
		else
		{
			loadingObj.DisplayLoading ();
			FindObjectOfType<SyncContactsToServer> ().GetPhoneContacts (OnEnableView);
		}*/

#if UNITY_EDITOR
		OnEnableView(true);
#endif
	}

	void OnEnableView(bool contactsLoaded)
	{
		loadingObj.gameObject.SetActive (false);
		HideSearchBarOnClose ();
		if (contactsLoaded) 
		{
			/*tempContacts.Clear ();
            for (int i = 0; i < Contacts.ContactsList.Count; i++) {
				tempContacts.Add (Contacts.ContactsList [i]);
			}*/

            tempContacts = new List<Contact>(FreakAppManager.Instance.GetAllContacts());

			//tempContacts = new List<Contact> (Contacts.ContactsList);
			 
			#if UNITY_EDITOR
			foreach (Contact detailsObj in dummyContactsList) {
				tempContacts.Add (detailsObj);
				Debug.Log (tempContacts.Count);
			}
			Debug.Log ("Contacts Added");
			#endif

			displayContacts.enabled = false;
			displayContactsInPanel.enabled = true;
			searchBtn.gameObject.SetActive (false);
			SetSprite (rowSprite);
			status = Status.Panel;
		}

        if (tempContacts.Count > 0)
        {
            noContactsText.gameObject.SetActive(false);
        }
        else
        {
            noContactsText.gameObject.SetActive(true);
        }
	}


		

	public void OnClickOptions()
	{
		if (status == Status.Panel) 
		{
			Debug.Log ("Display Row");
			HideSearchBar ();
			displayContacts.enabled = true;
			displayContactsInPanel.enabled = false;
			SetSprite (columnSprite);
			searchBtn.gameObject.SetActive (true);
			status = Status.Grid;
			return;
		} 
		else
		{
			Debug.Log ("Display Column");
			displayContactsInPanel.enabled = true;
			displayContacts.enabled = false;
			searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
			searchBar.SetActive (false);
			searchBtn.gameObject.SetActive (false);
			RowColumnBtn.gameObject.SetActive (true);
			SetSprite (rowSprite);
			status = Status.Panel;
		}
	}

	public void HideSearchBar()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

		displayContacts.OnClearTexts ();
	}

	public void HideSearchBarOnClose()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;
	}


	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;

        searchBarTextField.text=string.Empty;
	}

	public void Hide()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (false);
	}

	public void OnClickShowContacts()
	{
//		FindObjectOfType<FreaksPanelUIController>().PushPanel (FreaksPanelUIController.PanelType.AllContactsPanel);
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.DisplayContactsPanel);
	}
		

//	void OnDisable(){
//		Destroy (this.gameObject);
//	}

	public void OnClickBack()
	{
//		if (FindObjectOfType<HomeScreenUIPanelController> () != null) 
//		{
			FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
//		} 
//		else 
//		{
//			FindObjectOfType<FreaksPanelUIController> ().PopPanel ();
//		}
	}

	void SetSprite(Sprite spr)
	{
		rowColumImage.sprite = spr;
	}
}
