﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Freak;
using Freak.Chat;

public class ImageController : MonoBehaviour {


	System.Action<Texture2D> onLoad;
	public Dictionary<string,Sprite> profileTextureSpriteDict = new Dictionary<string, Sprite> ();
	public Dictionary<string,Texture2D> feedImages = new Dictionary<string, Texture2D> ();
	public Dictionary<string,Texture2D> profileTexturesDict = new Dictionary<string, Texture2D > ();
	Texture2D newtex;


	#region loadImagewithCallBack
	public void GetImageFromLocalOrDownload(string url, Image image, FolderLocation folderLocation, System.Action<Texture2D> _onLoad = null)
	{
		onLoad = _onLoad;
		//Debug.Log("Iddddddddddd -- >"+ nameId.ToString());
		//Texture2D newtex = LoadImageDataformFile(url, folderLocation);
		newtex = null;
		string _name = System.IO.Path.GetFileName(url);
		if (profileTexturesDict.ContainsKey (_name)) 
		{
			//Debug.Log("Load IMAGE From Dict -- >"+ _name);
			newtex = (Texture2D)profileTexturesDict [_name];
			//return;
		}
		if (newtex == null)
		{
			StartCoroutine(StartLoadingProfilrImage (url, image, folderLocation));
		} 
		else
		{
			if (onLoad != null) 
			{
				onLoad (newtex);
			}

			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, newtex));
		}
	}
		
	IEnumerator StartLoadingProfilrImage(string url, Image image, FolderLocation imageLocation) 
	{
		WWW www = new WWW(url);
		//Debug.Log ("url --->" + url);

		//		while( www.progress < 1 )
		//		{
		//			Debug.Log( "Image Download Progress ------ >>>>"+ www.progress * 100);
		//		}

		yield return www;
		if (www.error != null)
		{
			//Debug.Log ("Error --->" + www.error);
		} 
		else {
			//Debug.Log ("Response --->" + www.text);
            Texture2D tex = www.texture as Texture2D;

			string _name = System.IO.Path.GetFileName(url);
			//SaveimageCollection (tex, url, imageLocation);
			if (tex != null) 
			{
				if (!profileTexturesDict.ContainsKey (_name))
					profileTexturesDict.Add (_name, tex);
			}

			if (onLoad != null)
			{
				onLoad (tex);
			}
			//SaveimageCollection (tex, url, imageLocation);

			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, tex));
            tex = null;
			Resources.UnloadUnusedAssets();
		}
	}
	#endregion

	#region LoadingImageImage
	public void GetImageFromLocalOrDownloadImage(string url, Image image, FolderLocation folderLocation)
	{
		newtex = null;
		//Texture2D newtex = LoadImageDataformFile(url, folderLocation);
		string _name = System.IO.Path.GetFileName(url);
		if (profileTextureSpriteDict.ContainsKey (_name)) 
		{
			//Debug.Log("Load IMAGE From Dict -- >"+ _name);
			image.sprite = (Sprite)profileTextureSpriteDict [_name];
			return;
		}
		else
		{
			StartCoroutine(StartLoadingProfilrImageIfPassedImage (url, image, folderLocation));
		} 
//		else
//		{
			//newtex = GetCircleTexture (newtex, 2, 2, 2, new Color (0, 0, 0));
//			Rect rec = new Rect(0, 0, newtex.width, newtex.height);
//			Sprite spr = Sprite. Create(newtex,rec,new Vector2(0,0),1);
//			image.sprite = spr;
//			image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, newtex));
//			Resources.UnloadUnusedAssets();
//		}
	}



//	Texture2D CalculateTexture(int h, int w,float r,float cx,float cy,Texture2D sourceTex)
//	{
//		Debug.Log("CalculateTexture ***************** -- >");
//		Color [] c= sourceTex.GetPixels(0, 0, sourceTex.width, sourceTex.height);
//		Texture2D b=new Texture2D(h,w);
//		for(int i = (int)(cx-r) ; i < cx + r ; i ++)
//		{
//			for(int j = (int)(cy-r) ; j < cy+r ; j ++)
//			{
//				float dx = i - cx;
//				float dy = j - cy;
//				float d = Mathf.Sqrt(dx*dx + dy*dy);
//				if(d <= r)
//					b.SetPixel(i-(int)(cx-r),j-(int)(cy-r),sourceTex.GetPixel(i,j));
//				else
//					b.SetPixel(i-(int)(cx-r),j-(int)(cy-r),Color.clear);
//			}
//		}
//		b.Apply ();
//		return b;
//	}

//	public Texture2D GetCircleTexture(Texture2D tex, int cx, int cy, int r, Color col)
//	{
//		int x, y, px, nx, py, ny, d;
//
//		for (x = 0; x <= r; x++)
//		{
//			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
//			for (y = 0; y <= d; y++)
//			{
//				px = cx + x;
//				nx = cx - x;
//				py = cy + y;
//				ny = cy - y;
//
//				tex.SetPixel(px, py, col);
//				tex.SetPixel(nx, py, col);
//
//				tex.SetPixel(px, ny, col);
//				tex.SetPixel(nx, ny, col);
//
//			}
//		} 
//
//		return tex;
//	}


	IEnumerator StartLoadingProfilrImageIfPassedImage(string url, Image image, FolderLocation imageLocation) 
	{
		WWW www = new WWW(url);
		//Debug.Log ("StartLoadingProfilrImageIfPassedImage url --->" + url);

		yield return www;
		if (www.error != null)
		{
			//Debug.Log ("Error --->" + www.error);
		} 
		else {
			//Debug.Log ("Response --->" + www.text);
			Texture2D tex = www.texture as Texture2D;
			Sprite imageSprite = GetCreatedSprrite(tex);
			if(image != null)
			image.sprite = imageSprite;
			string _name = System.IO.Path.GetFileName(url);
			//SaveimageCollection (tex, url, imageLocation);
			if (imageSprite != null) 
			{
				if (!profileTextureSpriteDict.ContainsKey (_name))
					profileTextureSpriteDict.Add (_name, imageSprite);
			}
			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, tex));
            tex = null;
			Resources.UnloadUnusedAssets();
		}
	}
	#endregion


	Texture2D LoadImageDataformFile(string imageUrl, FolderLocation imageLocation)
	{
        if(imageUrl != string.Empty)
		{
			string _name = System.IO.Path.GetFileName(imageUrl);
			//Debug.Log("LoadImageDataformFile NAME -- >"+imageUrl);
			string filePath = StreamManager.LoadFilePath (_name, imageLocation, false);
			if (System.IO.File.Exists (filePath)) 
			{
				Texture2D texture = null;
				byte[] fileByte = System.IO.File.ReadAllBytes (filePath);
				texture = new Texture2D (50, 50, TextureFormat.ARGB32, false);// 4, 4
				texture.LoadImage (fileByte);
				return texture;

                texture = null;
			}
			else
			{
				Debug.Log("WRONG NAME -- >"+_name);
			}
		}
		return null;
	}
		


	public void SaveimageCollection(Texture2D data, string location, FolderLocation folderlocation)
	{
		string _name = System.IO.Path.GetFileName(location);
//		Debug.Log ("SaveimageCollection --->" + _name);
		byte[] byteArray;
		byteArray = data.EncodeToPNG ();
		Freak.StreamManager.SaveFile (_name, byteArray, folderlocation, HandleStreamSavedCallbackMethod, false);
	}
		

	void HandleStreamSavedCallbackMethod (bool suddeeded)
	{
		Debug.Log ("SaveimageCollection HandleStreamSavedCallbackMethod");
	}
		

	public void GetImageFromLocalOrDownloadImageForNewsFeed(string url, Image image, FolderLocation folderLocation, NewsFeedImageUIComponent cell, float textVal)
	{
		//Debug.Log("Iddddddddddd -- >"+ nameId.ToString());
		Texture2D newtex = null;
		string _name = System.IO.Path.GetFileName(url);
		//Texture2D newtex = LoadImageDataformFile(url, folderLocation);
		if (feedImages.ContainsKey (_name)) 
		{
			Debug.Log("Load FEED IMAGE From Dict -- >"+ _name);
			Texture2D tex = (Texture2D) feedImages [_name];
			image.sprite = GetCreatedSprrite(tex);
			AdjustImageSize (tex, image, cell, textVal);
			return;
		}
		if (newtex == null)
		{
			StartCoroutine(StartLoadingProfilrImageIfPassedImageForNewsFeed (url, image, folderLocation, cell, textVal));
		} 
//		else
//		{
//			//newtex = GetCircleTexture (newtex, 2, 2, 2, new Color (0, 0, 0));
//			Rect rec = new Rect(0, 0, newtex.width, newtex.height);
//			Sprite spr = Sprite. Create(newtex,rec,new Vector2(0,0),1);
//			image.sprite = spr;
//			//image.rectTransform.sizeDelta = new Vector2(720, newtex.height);
//			image.rectTransform.sizeDelta = new Vector2(720, newtex.height+ (720 - newtex.width));
//			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, newtex));
//			//Resources.UnloadUnusedAssets();
//		}
	}

	IEnumerator StartLoadingProfilrImageIfPassedImageForNewsFeed(string url, Image image, FolderLocation imageLocation, NewsFeedImageUIComponent cell, float textval) 
	{
		WWW www = new WWW(url);
		//Debug.Log ("url --->" + url);

        while (www.isDone == false)
        {
            yield return null;
        }

		yield return www;

		if (www.error != null) 
		{
			//Debug.Log ("Error --->" + www.error);
		}
		else
		{
			//Debug.Log ("Response --->" + www.text);
			Texture2D tex = www.texture as Texture2D;
            if (tex.width > 1000 || tex.height > 1000)
            { 
                tex = TextureResize.ResizeTexture(1000f, 1000f, tex);
            }

            tex.Compress(false);

			if (image != null)
				image.sprite = GetCreatedSprrite(tex);
            
			SetImageSize(tex, image, cell, textval);

			string _name = System.IO.Path.GetFileName(url);
			if (tex != null) 
			{
				if (!feedImages.ContainsKey (_name))
                {
                    feedImages.Add (_name, tex);
                }
			}
			//SaveimageCollection (tex, url, imageLocation);

			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, tex));
			Resources.UnloadUnusedAssets();
		}
	}


	Sprite GetCreatedSprrite(Texture2D tex)
	{
		Rect rect = new Rect (0, 0, tex.width, tex.height);
		Sprite _spr = Sprite.Create (tex, rect, new Vector2 (0, 0), 1);
		return _spr;
	}


//	void SetImageSize(Texture2D texture, Image _image, NewsFeedImageUIComponent cell, float textval)
//	{
//		//Debug.Log("Texture height -- >"+ texture.height + "Texture width -- >"+ texture.width);
//		if (texture.height >= texture.width) 
//		{
//			_image.rectTransform.sizeDelta = new Vector2 (720, texture.height + (720 - texture.width) * 1.5f);
//		} 
//		else 
//		{
//			_image.rectTransform.sizeDelta = new Vector2 (720, texture.height + (720 - texture.width));
//		}
//
//        Debug.Log("_image.rectTransform.sizeDelta " + _image.rectTransform.sizeDelta);
//	    Debug.Log(string.Format("Cell height changed Before"+ cell.likeCommentObj.commentImg.rectTransform.localPosition));
//		cell.likeCommentObj.commentImg.rectTransform.localPosition = new Vector3 (cell.likeCommentObj.commentImg.rectTransform.localPosition.x, 
//            (cell.likeCommentObj.commentMsgtxt.transform.localPosition.y - (cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y / 2)) - textval, cell.likeCommentObj.commentImg.rectTransform.localPosition.z);
//        Debug.Log("tesdcsdxtval -- >"+ textval + "delta -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta);
//		//cell.m_cellHeightSlider.maxValue = textval + 240 + cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y;
//		cell.m_cellHeightSlider.maxValue = textval + 280 + cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y;
//		//Debug.Log("Load SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
//		cell.height = cell.m_cellHeightSlider.maxValue;
//		cell.m_cellHeightSlider.value = cell.m_cellHeightSlider.maxValue;
//		cell.SliderValueChanged (cell.m_cellHeightSlider);
//	}

    void SetImageSize(Texture2D texture, Image _image, NewsFeedImageUIComponent cell, float textval)
    {
        Debug.Log("Texture height -- >"+ texture.height + "Texture width -- >"+ texture.width);

        _image.rectTransform.sizeDelta = new Vector2 ((float)texture.width, (float)texture.height);
        if (texture.width > 720)
        {
            float ratio = (float)texture.width / (float)texture.height;
            _image.rectTransform.sizeDelta = new Vector2(720f, (720f / ratio));
        }

        Debug.Log("_image.rectTransform.sizeDelta " + _image.rectTransform.sizeDelta);
        Debug.Log(string.Format("Cell height changed Before"+ cell.likeCommentObj.commentImg.rectTransform.localPosition));
        cell.likeCommentObj.commentImg.rectTransform.localPosition = new Vector3 (cell.likeCommentObj.commentImg.rectTransform.localPosition.x, 
            (cell.likeCommentObj.commentMsgtxt.transform.localPosition.y - (cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y / 2)) - textval, cell.likeCommentObj.commentImg.rectTransform.localPosition.z);
        Debug.Log("tesdcsdxtval -- >"+ textval + "delta -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta);
        //cell.m_cellHeightSlider.maxValue = textval + 240 + cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y;
        cell.m_cellHeightSlider.maxValue = textval + 280 + cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y;
        //Debug.Log("Load SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
        cell.height = cell.m_cellHeightSlider.maxValue;
        cell.m_cellHeightSlider.value = cell.m_cellHeightSlider.maxValue;
        cell.SliderValueChanged (cell.m_cellHeightSlider);
    }


	void AdjustImageSize(Texture2D texture, Image _image, NewsFeedImageUIComponent cell, float textval)
	{
		//yield return new WaitForSeconds (0.1f);
		SetImageSize (texture, _image, cell, textval);
	}


//	public void LoadProfileImageForNewsFeed(string url, Sprite imageSprite, FolderLocation folderLocation)
//	{
//		//Debug.Log("LoadProfileImageForNewsFeed -- >"+ url);
//		Texture2D newtex = null;
//		string _name = System.IO.Path.GetFileName(url);
//		//Texture2D newtex = LoadImageDataformFile(url, folderLocation);
//		if (feedImages.ContainsKey (_name)) 
//		{
//			Debug.Log("Load FEED IMAGE From Dict -- >"+ _name);
//			imageSprite = (Sprite)feedImages [_name];
//			return;
//		}
//		if (newtex == null)
//		{
//			StartCoroutine(StartLoadingLoadProfileImageForNewsFeed (url, imageSprite, folderLocation));
//		} 
//	}
//
//
//	IEnumerator StartLoadingLoadProfileImageForNewsFeed(string url, Sprite imageSprite, FolderLocation imageLocation) 
//	{
//		WWW www = new WWW(url);
//		Debug.Log ("Load profile pic url from server --->" + url);
//
//		yield return www;
//		if (www.error != null)
//		{
//			//Debug.Log ("Error --->" + www.error);
//		} 
//		else {
//			//Debug.Log ("Response --->" + www.text);
//			Texture2D tex = www.texture as Texture2D;
//			//tex = GetCircleTexture (tex, 2, 2, 2, new Color (0, 0, 0));
//			Rect rec = new Rect(0, 0, tex.width, tex.height);
//			Sprite spr = Sprite. Create(tex,rec,new Vector2(0,0),1);
//			if(imageSprite != null)
//				imageSprite = spr;
//			//image.rectTransform.sizeDelta = new Vector2(Screen.width, image.rectTransform.rect.size.y);
//			string _name = System.IO.Path.GetFileName(url);
//			if (imageSprite != null) 
//			{
//				if (!feedImages.ContainsKey (_name)) 
//				{
//					feedImages.Add (_name, imageSprite);
//				}
//					
//			}
//			//SaveimageCollection (tex, url, imageLocation);
//
//			//image.FreakSetSprite (TextureResize.ResizeTexture(image.rectTransform, tex));
//			Resources.UnloadUnusedAssets();
//		}
//	}

}
	