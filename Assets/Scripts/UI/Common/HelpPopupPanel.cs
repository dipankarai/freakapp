﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelpPopupPanel : MonoBehaviour
{
    private const string EMAIL = "info@freakapp.in";
    private string subject = "Freak: Help";
    private string body = "";

	// Use this for initialization
	void Start () {
        subject = WWW.EscapeURL(subject);
        body = WWW.EscapeURL(body);
	}
	
    public void OnClickOpenPanel ()
    {
        this.gameObject.SetActive(true);
    }

    public void OnClickClosePanel ()
    {
        this.gameObject.SetActive(false);
    }

    public void OnClickOpenMail ()
    {
        Application.OpenURL("mailto:" + EMAIL + "?subject=" + subject + "&body=" + body);
        this.gameObject.SetActive(false);
    }
}
