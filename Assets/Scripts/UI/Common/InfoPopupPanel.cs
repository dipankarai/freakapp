﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPopupPanel : MonoBehaviour
{

	// Use this for initialization
	void Start () {
	
	}
	
    public void OnClickOpenPanel ()
    {
        this.gameObject.SetActive(true);
    }

    public void OnClickClosePanel ()
    {
        this.gameObject.SetActive(false);
    }
}
