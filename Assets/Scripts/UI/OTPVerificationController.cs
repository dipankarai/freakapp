﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using Freak;
using System.Linq;


public class OTPVerificationController : MonoBehaviour {

	public InputField otpVerificationTextField;
	private List<string> contactNumbers = new List<string> ();
	public ConnectingUI loadingObj;
	public string failString;

	void OnEnable()
	{
		Init ("Test");

        #if UNITY_EDITOR || AUTO_OTP
        EnterOtpAuto();
        #endif
	}

    void EnterOtpAuto ()
    {
        if (string.IsNullOrEmpty(FreakAppManager.Instance.otp))
        {
            Invoke("EnterOtpAuto", 2f);
        }
        else
        {
            otpVerificationTextField.text = FreakAppManager.Instance.otp;
        }
    }

	public void ResendOTP()
	{
		FindObjectOfType<UserAuthentication> ().OnClickResendOtp ();
	}

	void Init(string callback)
	{
		if(!string.IsNullOrEmpty(callback))
		otpVerificationTextField.text = FreakAppManager.Instance.otp;
	}


	public void VefifyOTP()
	{
		//Time being..
		if (!string.IsNullOrEmpty (otpVerificationTextField.text)) 
		{
			loadingObj.DisplayLoading();
			OTPVerification (otpVerificationTextField);
		} 
		else 
		{
//            SceneManager.LoadScene (FreakAppConstantPara.UnitySceneName.UserProfileScene);
		}
	}


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void OTPVerification(InputField otp)
	{
		FreakApi api = new FreakApi ("user.validateOTP", ServerResponseForOTPVerification);
		api.param ("user_id", FreakAppManager.Instance.userID);
		api.param ("otp", otp.text);

		api.get (this);

	}


	void ServerResponseForOTPVerification(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
					/**
					 * Successfull Login... Load Game..
					*/
					IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
					bool status = bool.Parse (userDetailsDict ["status"].ToString ());
					if (status) 
					{
						string APIKey = (string)userDetailsDict ["access_token"];
						FreakAppManager.Instance.APIkey = APIKey;
						FreakAppManager.Instance.profileLink = (string)userDetailsDict ["avatar"];

						//LoadAllContacts ();
						GetUserInfoAPICall ();
					}
					else {
						AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.otpVerification);
						loadingObj.gameObject.SetActive(false);
					}
					//int ServeruserId = int.Parse (string.Format ("{0}", userDetailsDict ["user_id"]));
					//string _name = (string)userDetailsDict["name"];
					/** 
					 * user id & accessToken which is given by the server is saved into the playerprefs...
					*/
					
					break;
				case 207:
					/**
					* User email id Does not Exists.. U need to signIn first...
		 			*/
					break;
				default:
					/**
					 * Show Popup to let user know if anything has gone wrong while login in...
					 */
					break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
			if (InternetConnection.Check ()) 
			{
				AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.serverError);
			} 
			else 
			{
				Debug.LogError (" 5555555555555555555 No Internet Connection!");
				AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);

			}
			loadingObj.gameObject.SetActive(false);
		}  
	}

    /*void LoadAllContacts()
	{
		Contacts.LoadContactList( onDone, onLoadFailed );

		#if UNITY_EDITOR
			SyncContacts();
		#endif
	}*/

	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
		SyncContacts ();
	}

	void SyncContacts()
	{
		for (int i = 0; i < Contacts.ContactsList.Count; i++)
		{
			for(int k = 0; k < Contacts.ContactsList [i].Phones.Count; k++)
			{
				string number = Contacts.ContactsList [i].Phones [k].Number.ToString ();
				number = number.Replace (" ", "");
				number = number.Replace ("-", "");
				number = number.Replace ("+", "");
				bool digitsOnly = number.All(char.IsDigit);
				if (digitsOnly) 
				{
					if (number.Length > 10)
					{
						number = number.Substring (number.Length - 10);

					}
					if (!number.Equals (FreakAppManager.Instance.mobileNumber)) 
					{
						contactNumbers.Add (number);
						Contacts.ContactsList [i].Phones [0].Number = number;
					}
				}
				else {
					Debug .LogWarning("NUMBER DATA NOT SYNC-----"+  Contacts.ContactsList [i].Phones [k].Number.ToString ());
				}
			}
			
		}
		#if UNITY_EDITOR
//			for (int i = 0; i < 300 ; i++)
//			{
//				
//				string number = "9538333447"+i;
//				number = number.Replace (" ", String.Empty);
//				if (number.Length > 10) {
//					number = number.Substring (number.Length - 10);
//				}
//				contactNumbers.Add (number);
//
//			}
			string number1 = "+91 95383 333447";
			number1 = number1.Replace (" ", "");
			number1 = number1.Replace ("-", "");
			number1 = number1.Replace ("+", "");
			contactNumbers.Add(number1);
			contactNumbers.Add("8861061197");
		#endif
		string numberdata = MiniJSON.Json.Serialize (contactNumbers);
		Debug .Log("NUMBER DATA TO SYNC-----"+ numberdata);
		SynAllContactsToServer (numberdata);
	}

	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void SynAllContactsToServer(string dataString)
	{

		FreakApi api  = new FreakApi("contact.syncContactToServer",ServerResponseForSynAllContactsToServer);
		api.param("userId",FreakAppManager.Instance.userID);
		api.param("accessToken",FreakAppManager.Instance.APIkey);
		api.param("contact_number", dataString);
		api.post(this);
	}


	void ServerResponseForSynAllContactsToServer(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			switch(responseCode){
                case 001:
				/**
				 * Successfull Login... Load Game..
				*/

                GetUserInfoAPICall ();

				break;
			case 207:
				/**
				* User email id Does not Exists.. U need to signIn first...
	 			*/
				loadingObj.gameObject.SetActive (false);
				break;
			default:
				/**
				 * Show Popup to let user know if anything has gone wrong while login in...
				 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
			loadingObj.gameObject.SetActive (false);
			AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.serverError);
			if (InternetConnection.Check ()) 
			{
				AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.serverError);
			} 
			else 
			{
				Debug.LogError (" 5555555555555555555 No Internet Connection!");
				AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);

			}
		}  
	}

    public void GetUserInfoAPICall()
    {
        FreakApi api = new FreakApi ("user.getUserInfo", ServerResponseForGetUserInfoAPICall);
        api.param ("other_user_id", "0");
        api.get (this);
    }

    void ServerResponseForGetUserInfoAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
    {
        // check for errors
        if (output!=null)
        {
            Debug.Log (MiniJSON.Json.Serialize (output));

            string responseInfo =(string)output["responseInfo"];

            switch(responseCode)
            {
                case 001:
                    IDictionary userDetailsDict = (IDictionary)output["responseMsg"];

                    string name = (string)userDetailsDict["name"];
                    if (string.IsNullOrEmpty(name) == false)
                    {
                        FreakAppManager.Instance.myUserProfile = new MyUserProfile(userDetailsDict);
                        
                        FreakAppManager.Instance.username = FreakAppManager.Instance.myUserProfile.name;
                        FreakAppManager.Instance.secondaryUsername = FreakAppManager.Instance.myUserProfile.secondary_name;
                        FreakAppManager.Instance.profileLink = FreakAppManager.Instance.myUserProfile.avatar_url;
                        
                        FreakAppManager.Instance.SaveFirstTimeUserInfoPlayerPrefs();
                    }

//              FreakAppManager.Instance.LoadAllContacts ();
                //LoadImage (FreakAppManager.Instance.myUserProfile.avatarLink, FreakAppManager.Instance.myUserProfile.avatarName);
                    StartCoroutine(WaitForSendBirdToLogin());
//                SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.UserProfileScene);

                break;
                case 207:
                    /**
                        
                */
                break;
                default:
                    /**
                        
                     */
                break;
            }   
        } 
        else {
            Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
            loadingObj.HideLoading();
            ////inapi.get(this);
            if (InternetConnection.Check ()) 
            {
                AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.serverError);
            } 
            else 
            {
                Debug.LogError (" 5555555555555555555 No Internet Connection!");
                AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);

            }
        }  
    }

    private AsyncOperation mAsyncOperation;
    IEnumerator WaitForSendBirdToLogin ()
    {
        float elapsedTime = 0f;
        while (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
        {
            if (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined)
            {
                break;
            }

            elapsedTime += Time.deltaTime;
            yield return null;

            if (elapsedTime > 5f)
            {
                break;
            }
        }

        Freak.Chat.ChatManager.Instance.StartRefreshCoroutine();
//        SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.UserProfileScene);
        mAsyncOperation = SceneManager.LoadSceneAsync(FreakAppConstantPara.UnitySceneName.UserProfileScene);
        while (mAsyncOperation.isDone == false)
        {
            yield return null;
        }

        loadingObj.HideLoading();
    }
}
