﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class FreaksContentController : MonoBehaviour {

	//public GetContactListApiManager getContactListApiManager;
	public List<ServerUserContact> serverContactList;
	//public HomeScreenUIPanelController homeScreenUIPanelController;

	public FreakHomeScreenPanel freakHomeScreenPanel;
	public FreakRowUIPanelController freakRowUIPanelController;

	public GameObject searchBar;
	public InputField searchBarTextField;
	public Button searchBtn;
	public Button RowColumnBtn;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;

	public Image rowColumImage;
	public Sprite rowSprite;
	public Sprite columnSprite;

	enum Status{
		Grid,
		Panel
	}
	Status status;
	public ConnectingUI loadingObj;
	Vector3 bottomPanelPosition;
    public Text contactFreakContact;

	void Awake()
	{
		serverContactList = new List<ServerUserContact> ();
		bottomPanelPosition = searchBar.transform.rectTransform ().localPosition;
	}
//	void Update()
//	{
//		#if UNITY_ANDROID
//		if (Input.GetKeyDown(KeyCode.Escape)&& searchBar.activeInHierarchy)
//		{
//			HideSearchBar();
//		}
//		#endif
//	}

	void OnEnable()
	{
        loadingObj.DisplayLoading ();
        Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
    }

    void InitialCall()
    {
        freakRowUIPanelController.enabled = false;
		FreakAppManager.Instance.LoadServerContactList (EnableTableView);
	}


	void EnableTableView(List<ServerUserContact> data)
	{

		HideSearchBarOnClose ();
		serverContactList.Clear ();
		for (int i = 0; i < data.Count; i++)
		{
			serverContactList.Add (data [i]);
		}

        serverContactList.Sort((a, b) =>
            {
                return a.name.CompareTo(b.name);
            });

        Freak.Chat.ChatManager.Instance.cachedGroupChannelData.Sort((a, b) =>
            {
                return a.channelName.CompareTo(b.channelName);
            });

        for (int i = 0; i < Freak.Chat.ChatManager.Instance.cachedGroupChannelData.Count; i++)
        {
            ServerUserContact _ServerUserContact = new ServerUserContact ();
            _ServerUserContact.name = Freak.Chat.ChatManager.Instance.cachedGroupChannelData[i].channelName;
            _ServerUserContact.avatar = Freak.Chat.ChatManager.Instance.cachedGroupChannelData[i].coverUrl;

            Freak.Chat.ChatChannel _chatChannel = new Freak.Chat.ChatChannel(Freak.Chat.ChatManager.Instance.cachedGroupChannelData[i]);
            _ServerUserContact.groupChannel = _chatChannel;

            serverContactList.Add (_ServerUserContact);
        }

        freakHomeScreenPanel.enabled = true;
		searchBtn.gameObject.SetActive (false);
		SetSprite (rowSprite);
		status = Status.Panel;

        if (serverContactList.Count > 0)
        {
            contactFreakContact.text = string.Empty;
        }
        else
        {
            contactFreakContact.text = "Invite Freak.";
        }

        if (loadingObj != null) 
        {
            loadingObj.HideLoading();
        }
	}

	public void OnClickOptions()
	{
		if (status == Status.Panel) {
			HideSearchBar ();
			freakHomeScreenPanel.enabled = false;
			freakRowUIPanelController.enabled = true;
			searchBtn.gameObject.SetActive (true);
			SetSprite (columnSprite);
			status = Status.Grid;
			return;
		} else
		{
			freakRowUIPanelController.enabled = false;
			freakHomeScreenPanel.enabled = true;
			searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
			searchBar.SetActive (false);
			searchBtn.gameObject.SetActive (false);
			RowColumnBtn.gameObject.SetActive (true);
			SetSprite (rowSprite);
			status = Status.Panel;
		}
	}

	public void HideSearchBarOnClose()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		RowColumnBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;
	
	}

	public void HideSearchBar()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		RowColumnBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

		freakRowUIPanelController.ClearOnClose ();
	}

	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		RowColumnBtn.gameObject.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;

        searchBarTextField.text=(string.Empty);
	}

	public void Hide()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (false);
		RowColumnBtn.gameObject.SetActive (false);
	}

	public void OnClickShowContacts()
	{
		FindObjectOfType<HomeScreenUIPanelController>().PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}

	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController>().PopPanel ();
	}

	public void OnClickCreateGroup()
	{
        Freak.Chat.ChatManager.Instance.groupUsersList.Clear ();
		FindObjectOfType<HomeScreenUIPanelController>().PushPanel (HomeScreenUIPanelController.PanelType.ServerSynchedContactsDisplayPanel);
	}

	void SetSprite(Sprite spr)
	{
		rowColumImage.sprite = spr;
	}

	void OnDisable()
	{
        Destroy (this.gameObject);
        Resources.UnloadUnusedAssets ();
	}
}
	