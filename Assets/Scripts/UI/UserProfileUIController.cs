﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
using System.Collections.Generic;

public class UserProfileUIController : MonoBehaviour {

	public enum PanelType {

		UserDetailEntryPanel,
		TutorialPanel,
		popUpPanel = 100
	} 

	public UserDetailEntryController userDetailEntryController;
	public TutorialController tutorialController;

	public PanelType currentpanelState;
	public GameObject popUpPanel;
	public List<PanelType> panelStack = new List<PanelType>();
	private bool isCompleted;
	public Transform PanelObj;
	public GameObject popUpMsgObj;
	public Transform panaelPosObj;
    public ConnectingUI loadingObj;

	void Awake()
	{
        loadingObj.DisplayLoading();
	}

	void Start()
	{
        #if UNITY_ANDROID
        Screen.fullScreen = false;
        #endif
		PanelObj = this.gameObject.transform;
		ShowInitialWelcomePanel ();

        FreakAppManager.Instance.LoadServerContactList(LoadServerContactListHandler);
	}

    void LoadServerContactListHandler (List<ServerUserContact> obj)
    {
        for (int i = 0; i < obj.Count; i++)
        {
            Freak.DownloadManager.Instance.DownloadProfile(obj[i].avatar, Freak.FolderLocation.Profile, null);
        }
    }

	public void ShowInitialWelcomePanel(){
		PushPanel (PanelType.UserDetailEntryPanel);
	}


	public void OnClickBackButton()
	{
//		Application.Quit();
        FreakAppManager.GoToBackground();
	}


	public void OnClickCancel()
	{
		PopPanel ();
	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{ 
			PopPanel();
		}
	}

	GameObject GetGamebjectforEnum(PanelType _CurrentpanelState)
	{

		switch (_CurrentpanelState) 
		{
			case PanelType.UserDetailEntryPanel:
				return userDetailEntryController.gameObject;
	
			case PanelType.TutorialPanel:
				return tutorialController.gameObject;

			case PanelType.popUpPanel:
				return popUpMsgObj;

			default:
				return null;
		}
	}


	void DisplayPanel(PanelType _CurrentpanelState)
	{

		if((int)_CurrentpanelState < 100)
		{
			currentpanelState = _CurrentpanelState;
			EnablePanel(GetGamebjectforEnum(currentpanelState), true);

		}
		else{
			ShowPopup(GetGamebjectforEnum(_CurrentpanelState));
		}
	}


	void HidePanel(PanelType currentPanel)
	{
		HidePopup(GetGamebjectforEnum (currentPanel));
	}


	public void PushPanel(PanelType currentpanelState)
	{
		panelStack.Add (currentpanelState);
		DisplayPanel (currentpanelState);
	}


	public void ShowPanelOnFocus(PanelType currentpanelState)
	{
		panelStack.Add (currentpanelState);
		if((int)currentpanelState < 100)
		{
			currentpanelState = currentpanelState;
			isCompleted = true;
			foreach (Transform child in PanelObj) 
			{
				if(child.gameObject.activeSelf)
				{
					previousObj = child.gameObject;
					child.gameObject.SetActive(false);
				}
			}

			GameObject obj = GetGamebjectforEnum(currentpanelState);
			obj.transform.localPosition = new Vector3(0,obj.transform.localPosition.y, obj.transform.localPosition.z);
			//CompleteAnimation();
		}
	}


	public void ReplacePanel(PanelType currentpanelState)
	{
		panelStack.RemoveAt (panelStack.Count - 1);
		panelStack.Add (currentpanelState);
		DisplayPanel (currentpanelState);
	}


	public void PopPanel()
	{
		PanelType toppanel1 = panelStack[panelStack.Count-1];
		if(panelStack.Count >1 ){
			if((int)toppanel1 < 100){
				panelStack.RemoveAt (panelStack.Count - 1);
				toppanel1 = panelStack[panelStack.Count-1];
				DisplayPanel (toppanel1);
			}
			// this is for popup panel..
			else{
				HidePanel(toppanel1);
				panelStack.RemoveAt (panelStack.Count - 1);
			}
		}
		else{
			OnClickBackButton();
		}
	}


	private GameObject previousObj;
	private GameObject currentObj;
	void EnablePanel(GameObject enableObj, bool isAnimate)
	{
		//		isCompleted = true;
		foreach (Transform child in PanelObj) 
		{
			if(child.gameObject.activeSelf)
			{
				previousObj = child.gameObject;
				//child.gameObject.SetActive(false);
				Vector3 _rightPosition = panaelPosObj.rectTransform().localPosition;
				_rightPosition.x -= enableObj.transform.rectTransform().rect.width;
				//				LeanTween.moveX (previousObj, _rightPosition.x, 0.58f).setEase (LeanTweenType.easeInSine).setOnComplete(CompleteAnimation);
				LeanTween.move (previousObj.transform.rectTransform(), new Vector3(_rightPosition.x, _rightPosition.y, _rightPosition.z), 0.5f).setEase (LeanTweenType.easeInSine).setOnComplete(CompleteAnimation);

                if (previousObj.GetComponent<UserDetailEntryController>() != null)
                {
                    previousObj.GetComponent<UserDetailEntryController>().DisableInputField();
                }
			}
		}

		currentObj = enableObj;
		ResetPosition (enableObj);
	}


	void ResetPosition(GameObject enableObj)
	{
		Vector3 _rightPosition = panaelPosObj.rectTransform().localPosition;
		_rightPosition.x += enableObj.transform.rectTransform().rect.width;
		enableObj.transform.rectTransform().localPosition = _rightPosition;
		//enableObj.transform.localPosition = new Vector3 ( panaelPosObj.rectTransform().localPosition.x + 1200, enableObj.transform.localPosition.y, enableObj.transform.localPosition.z);
		enableObj.SetActive (true);
		LeanTween.move (enableObj.transform.rectTransform(), panaelPosObj.rectTransform().localPosition, 0.5f).setEase (LeanTweenType.easeInSine);
	}
		
//	private GameObject previousObj;
//	void EnablePanel(GameObject enableObj, bool isAnimate)
//	{
//		isCompleted = true;
//		foreach (Transform child in PanelObj) {
//			if(child.gameObject.activeSelf){
//				previousObj = child.gameObject;
//				child.gameObject.SetActive(false);
//			}
//		}
//
//		ResetPosition (enableObj);
//
//	}
//
//
//	void ResetPosition(GameObject enableObj)
//	{
//		enableObj.SetActive (true);
//		//enableObj.transform.localPosition = new Vector3 (1200, enableObj.transform.localPosition.y, enableObj.transform.localPosition.z);
//		//iTween.MoveTo (enableObj.gameObject, iTween.Hash ("x", 0 , "time", 0.3f, "loopType", iTween.LoopType.none, "easeType", iTween.EaseType.linear,"oncompletetarget",this.gameObject,"oncomplete","CompleteAnimation" ));
//		//LeanTween.moveX( enableObj.gameObject, 0, 0.3f).setEase(LeanTweenType.easeOutSine).setOnComplete(CompleteAnimation);
//	}
//
//
	void CompleteAnimation()
	{
		DisableObj ();
		isCompleted = false;
	}


	void DisableObj()
	{
		if(previousObj != null)
			previousObj.SetActive (false);
	}


	void ShowPopup(GameObject enableObj){
		enableObj.SetActive (true);
		//enableObj.transform.localScale = Vector3.zero;
		//LeanTween.scale (enableObj.gameObject, Vector3.one, 0.3f).setEase (LeanTweenType.easeOutBack);

	}

	void HidePopup(GameObject enableObj){
		enableObj.SetActive (false);

	}

}
