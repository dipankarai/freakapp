﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadInitialScene : MonoBehaviour {

    public ConnectingUI loadingObject;
    private FreakAppManager mFreakAppManager;

    void Awake ()
    {
//        Application.targetFrameRate = 30;

        loadingObject.DisplayLoading ("Loading Freak");
        InternetConnection.Init();

        #if UNITY_ANDROID
        Screen.fullScreen = false;
        #endif

        FreakPlayerPrefs.AppInstalledTime = SendBird.Utils.CurrentTimeMillis ();

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("SettingsManager.Instance.isDisableFreaks " + SettingsManager.Instance.isDisableFreaks);
        #endif

        if (SettingsManager.Instance.isDisableFreaks == false)
        {
            Freak.Chat.ChatManager.Instance.InitSendBird ();
        }

        if (mFreakAppManager == null)
        {
            mFreakAppManager = FreakAppManager.Instance;
        }

        #if !UNITY_EDITOR
        FreakAppManager.ChangeStatusBarStyle(FreakAppManager.StatusBarStyle.StatusBarStyleDefault);
        #endif
    }

    void LoadContact ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("Contacts -----> LoadContact InitScene " + Time.time);
        #endif
        mFreakAppManager.LoadContactFromDevice();
    }

	// Use this for initialization
	void Start ()
    {
        LoadContact();
        Freak.Chat.FreakChatUtility.FreakChatPlayerPrefs.DeleteKey (FreakAppConstantPara.PlayerPrefsName.LocalNotificationID);
        Invoke ("NextScene", 1f);
    }

    /*void InvokeForNetCheck ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("InvokeForNetCheck");
        #endif
//        if (InternetConnection.Ch)
        {
            Invoke ("InvokeForNetCheck", 1f);
        }
    }*/

    void NextScene ()
    {
        if (InternetConnection.Check())
        {
            if (mFreakAppManager == null)
            {
                mFreakAppManager = FreakAppManager.Instance;
            }

            if (FreakAppManager.Instance.userInfoPlayerPrefs != null && FreakAppManager.Instance.userInfoPlayerPrefs.loginUserId != 0)
            {
                if (SettingsManager.Instance.isDisableFreaks == false)
                {
                    Freak.Chat.ChatManager.Instance.LoginToSendbird ();
                }
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("FIRST TIME LOGIN");
                #endif
            }

            LoadScene ();
        }
        else
        {
            Debug.LogError ("NO INTRENET CONNECTION");
            Invoke ("LoadScene", 1f);
        }
    }

    IEnumerator LoadNextScene ()
    {
        float elapsedTime = 0;

        while (InternetConnection.Check() == false)
        {
            elapsedTime += Time.deltaTime;
            yield return null;

            if (elapsedTime > 4f) 
            {
                break;
            }
        }

        if (InternetConnection.Check())
        {
//            Freak.Chat.ChatManager.Instance.InitSendBird ();

            if (mFreakAppManager == null)
            {
                mFreakAppManager = FreakAppManager.Instance;
            }
            
            if (FreakAppManager.Instance.userInfoPlayerPrefs != null)
            {
                if (SettingsManager.Instance.isDisableFreaks == false)
                {
                    Freak.Chat.ChatManager.Instance.LoginToSendbird ();
                }
                
                /*while (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
                {
                    yield return null;
                }*/

                /*while (Freak.Chat.ChatManager.Instance.updateQueryMessagingChannelList == false)
                {
                    yield return null;
                }*/
            }
            
            LoadScene ();
        }
        else
        {
            Debug.LogError ("NO INTRENET CONNECTION");
            yield return new WaitForSeconds (1f);
            //StartCoroutine ("LoadNextScene");
            LoadScene ();
        }
    }

    void LoadScene ()
    {
        SceneManager.LoadScene (FreakAppConstantPara.UnitySceneName.LoginScene);
    }
}
