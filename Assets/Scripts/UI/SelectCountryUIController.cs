﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class SelectCountryUIController : MonoBehaviour, ITableViewDataSource {

	public string failString;
	int currentCount;
	public SelectCountryItem m_cellPrefab;
	public TableView m_tableView;
	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;

	public List<Country.CountryCode> countryNames;
    private List<Country.CountryCode> tempCountryNames = new List<Country.CountryCode>();
	private List<Country.CountryCode> searchedCountryNames = new List<Country.CountryCode>();

	public GameObject searchBar;
	public GameObject searchButton;

	public TouchScreenKeyboard keyboard;
	public static string keyboardText = "";
	private bool KeyboardCallLimiter;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;
	public InputField searchInputField;
	float refHeight, screenRef ;

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls

	void OnEnable()
	{

		Vector3 pos = this.gameObject.GetComponent<RectTransform> ().position;
		this.transform.localPosition = new Vector3 (800, 0, 0);
		LeanTween.move(this.gameObject, pos, 0.3f ).setEase( LeanTweenType.easeInSine );

		tempCountryNames.Clear ();
		for (int i = 0; i < Country.GetAllCountry().Count ; i++) 
		{
			tempCountryNames.Add (Country.GetAllCountry() [i]);
		}

        searchedCountryNames = new List<Country.CountryCode>(tempCountryNames);

		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
		searchBar.SetActive (false);
		searchButton.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

		if(Screen.height < 1280)
			screenRef = 1184;
		else
			screenRef = 1280;
		refHeight= (100 * Screen.height) / screenRef;
		Invoke ("ShowSearchBar",0.1f);
	}

	void Update()
	{
		if (TouchScreenKeyboard.visible == false && keyboard != null)
		{
			if (keyboard.done == true)
			{
				keyboardText = keyboard.text;
				keyboard = null;
			}
		}
		if (GetKeyboardHeight () > refHeight && !KeyboardCallLimiter) 
		{
			KeyboardCallLimiter = true;
            PlayerPrefs.SetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight, GetKeyboardHeight());
		}
	}

	float GetKeyboardHeight()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR 
		using(AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
		AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");

		using(AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
		{
		View.Call("getWindowVisibleDisplayFrame", Rct);

		float _height=Screen.height - Rct.Call<int>("height");

		_height=(_height*screenRef)/Screen.height;

//		return Screen.height - Rct.Call<int>("height");
		return _height;
		}
		}
		#endif
		return 40;
	}
	public void HideSearchBar()
	{
		searchBar.SetActive (false);
		searchButton.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;


		searchedCountryNames = new List<Country.CountryCode>(tempCountryNames);

		m_tableView.ReloadData ();
	}

	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchButton.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;
		searchInputField.text = "";
		searchInputField.ActivateInputField();
	}

	public void OnClickBack()
	{
		FindObjectOfType<UIPanelController> ().PopPanel ();
	}

    public void OnValueChangedInputField (string text)
    {
        if (text.Length > 0)
        {
            for (int i = 0; i < tempCountryNames.Count; i++)
            {
                if (tempCountryNames [i].country_name.ToUpper ().Contains (text.ToUpper())) 
                {
                    if (searchedCountryNames.Contains(tempCountryNames[i]) == false)
                    {
                        searchedCountryNames.Add(tempCountryNames[i]);
                    }
                } 
                else 
                {
                    if (searchedCountryNames.Contains(tempCountryNames[i]))
                    {
                        searchedCountryNames.Remove(tempCountryNames[i]);
                    }
                }
            }
        }
        else
        {
            searchedCountryNames = new List<Country.CountryCode>(tempCountryNames);
        }
        m_tableView.ReloadData ();
    }

	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
        return searchedCountryNames.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
		SelectCountryItem cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as SelectCountryItem;
		if (cell == null) {
			cell = (SelectCountryItem)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		AssignDataToCell (cell, row);
		cell.rowNumber = row;
		cell.height = GetHeightOfRow(row);
		return cell;
	}

	void AssignDataToCell(SelectCountryItem cell, int rowNumber)
	{
		Country.CountryCode countryDetails = searchedCountryNames [rowNumber];
		cell.nameText.text = countryDetails.country_name;
		cell.iD = countryDetails.country_code;
	}
	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}

	public void GetAllContacts()
	{
		
	}

	public void ClearContats()
	{
		
	}
		
	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}

//	[System.Serializable]
//	public class Country{
//		public string countryName;
//		public int countryID;
//	}
}
