﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Freak;
using Freak.Chat;
using Newtonsoft.Json;

public class GroupProfileUIController : MonoBehaviour, ITableViewDataSource  {

	public GameObject addMemberButton;
	public InputField freakNameInputField;
	private NativeEditBox freakNameNativeField;
	public Image groupProfilePicImg;
    public ConnectingUI loadingObject;

    private string mImageUrl;
    private string mGroupName;
    private string mChannel_Url;
    private string removed_id;
    private string removed_name;
    private string mDeviceFilepath;
    private string mNewFilepath;

    private byte[] mImageFileByte;
//    private int memberCount;
    private GroupSerializableResponseClass.MetaData adminMetaData;
//    private GroupSerializableResponseClass.MemberList mGroupMember;
    private ChatChannel mChatChannel;
    private List<ChatUser> mMembersChatUser;
    private List<SendBird.User> mMembers;

    #region TableView
    public GroupMemberProfileContent m_cellPrefab;
    public TableView m_tableView;

    private int m_numInstancesCreated = 0;
    private Dictionary<int, float> m_customRowHeights;
    #endregion

    void OnEnable()
    {
        Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
    }


    void InitialCall()
    {
        if (!freakNameNativeField)
            freakNameNativeField = freakNameInputField.gameObject.GetComponent<NativeEditBox> ();

        m_customRowHeights = new Dictionary<int, float>();
        m_tableView.dataSource = this;

		InitGroupProfileInfo(FreakAppManager.Instance.selectedGroupProfile);
	}

    public void InitGroupProfileInfo (ChatChannel chatChannel)
    {
        mChannel_Url = chatChannel.channelUrl;
        mImageUrl = chatChannel.coverUrl;

        mChatChannel = chatChannel;

        //If group Cover link is there we save to local....
        DownloadImage ();

        mGroupName = WWW.UnEscapeURL(chatChannel.channelName);
        freakNameInputField.text = mGroupName;
        freakNameNativeField.SetTextNative (mGroupName);
        //        memberCount = channel.MemberCount;

        if (string.IsNullOrEmpty(chatChannel.data) == false)
        {
            // Get Metadata of Group....
            adminMetaData = (GroupSerializableResponseClass.MetaData)
                JsonConvert.DeserializeObject <GroupSerializableResponseClass.MetaData>(chatChannel.data);
            
            // Check admin ID in metadata
            if (adminMetaData.createdBy == ChatManager.Instance.UserId)
            {
                addMemberButton.SetActive(true);
            }
            else
            {
                addMemberButton.SetActive(false);
            }
        }
        
        // Init all members
        mMembersChatUser = new List<ChatUser>(chatChannel.channelMembers);
        m_tableView.ReloadData();
        m_tableView.scrollY = 0;

        if (ChatManager.Instance.groupUsersList.Count > 0)
        {
            List<string> userIdList = new List<string>();
            for (int i = 0; i < ChatManager.Instance.groupUsersList.Count; i++)
            {
                userIdList.Add(ChatManager.Instance.groupUsersList[i].userId);
            }
            AddGroupMember(userIdList);
        }
        

        /*if (ChatManager.Instance.currentChannel != null && ChatManager.Instance.currentChannel.Url == mChannel_Url)
        {
            SendBird.GroupChannel _groupChannel = ChatManager.Instance.currentChannel as SendBird.GroupChannel;
            InitGroupProfileInfo (_groupChannel);
        }
        else
        {
        }*/
        ChatManager.Instance.GetChannelByChannelUrl (mChannel_Url, GetNewGroupChannelCallback);
    }

    public void GetNewGroupChannelCallback (SendBird.GroupChannel groupChannel)
    {
        if (groupChannel != null)
        {
            ChatManager.Instance.currentChannel = groupChannel;
            InitGroupProfileInfo (groupChannel);
        }
    }

    void InitGroupProfileInfo (SendBird.GroupChannel channel)
    {
        mChannel_Url = channel.Url;
        mImageUrl = channel.CoverUrl;

        ChatChannelHandler.UpdateChatChannel (mChatChannel, channel);

        //If group Cover link is there we save to local....
        DownloadImage ();

		mGroupName = WWW.UnEscapeURL(channel.Name);
		freakNameInputField.text = mGroupName;
		freakNameNativeField.SetTextNative (mGroupName);
//        memberCount = channel.MemberCount;

        if (string.IsNullOrEmpty(channel.Data) == false)
        {
            // Get Metadata of Group....
            adminMetaData = (GroupSerializableResponseClass.MetaData)
                JsonConvert.DeserializeObject <GroupSerializableResponseClass.MetaData>(channel.Data);

            // Check admin ID in metadata
            if (adminMetaData.createdBy == ChatManager.Instance.UserId)
            {
                addMemberButton.SetActive(true);
            }
            else
            {
                addMemberButton.SetActive(false);
            }
        }

        // Init all members
        mMembers = new List<SendBird.User>(channel.Members);
        MemberUpdate();

        if (ChatManager.Instance.groupUsersList.Count > 0)
        {
            List<string> userIdList = new List<string>();
            for (int i = 0; i < ChatManager.Instance.groupUsersList.Count; i++)
            {
                userIdList.Add(ChatManager.Instance.groupUsersList[i].userId);
            }
            AddGroupMember(userIdList);
        }
    }

    void MemberUpdate ()
    {
        if (mMembers.Count >= mMembersChatUser.Count)
        {
            for (int i = 0; i < mMembers.Count; i++)
            {
                // .channelMembers.Add(_chatUser);
                if (AlreadyContainMember(mMembers[i]) == false)
                {
                    ChatUser _chatUser = new ChatUser(mMembers[i]);
                    mMembersChatUser.Add(_chatUser);
                }
            }
        }
        else
        {
            for (int i = 0; i < mMembersChatUser.Count; i++)
            {
                if (ContainRemovedMember(mMembersChatUser[i]) == false)
                {
                    mMembersChatUser.RemoveAt(i);
                    break;
                }
            }
        }

        m_tableView.ReloadData();
    }

    bool AlreadyContainMember (SendBird.User member)
    {
        for (int i = 0; i < mMembersChatUser.Count; i++) 
        {
            if (mMembersChatUser[i].userId == member.UserId)
            {
                ChatUser _chatUser = new ChatUser(member);
                mMembersChatUser[i] = _chatUser;
                return true;
            }
        }

        return false;
    }

    bool ContainRemovedMember (ChatUser member)
    {
        for (int i = 0; i < mMembers.Count; i++) 
        {
            if (mMembers[i].UserId == member.userId)
            {
                return true;
            }
        }

        return false;
    }

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (mMembersChatUser == null)
            return 0;

        if (mMembersChatUser.Count > 0)
            return mMembersChatUser.Count;
        else
            return 0;
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return GetHeightOfRow(row);
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
    {
        GroupMemberProfileContent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as GroupMemberProfileContent;
        if (cell == null) {
            cell = (GroupMemberProfileContent)GameObject.Instantiate(m_cellPrefab);
            cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
            cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
        }

        cell.rowNumber = row;
        AssignDetails (cell, row);
        cell.height = GetHeightOfRow(row);
        return cell;
    }

    private float GetHeightOfRow(int row) 
    {
        if (m_customRowHeights.ContainsKey(row)) {
            return m_customRowHeights[row];
        } else {
            return m_cellPrefab.height;
        }
    }

    private void OnCellHeightChanged(int row, float newHeight) 
    {
        if (GetHeightOfRow(row) == newHeight) {
            return;
        }
        //Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
        m_customRowHeights[row] = newHeight;
        m_tableView.NotifyCellDimensionsChanged(row);
    }

    void AssignDetails(GroupMemberProfileContent cell, int rowNumb)
    {
        cell.groupProfileUIController = this;
        cell.InitGroupMemberDetails(mMembersChatUser[rowNumb], adminMetaData);
    }
    #endregion

#region UI Events

    public void OnClickEditImage()
    {
        Debug.Log("OnClickEditImage");
        if (ChatManager.Instance.isInternetConnected == false)
        {
            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
            return;
        }

        if (ChatManager.Instance.IsSeandbirdLogined == false)
        {
            ChatManager.Instance.ReConnectSendBird (OnClickEditImage);
            return;
        }
        
#if UNITY_EDITOR
        string _path = Application.persistentDataPath + "/group.jpg";
        if (File.Exists(_path))
        {
            Debug.Log(_path);
            ImagePathCallback(_path);
        }
        else
        {
            Debug.LogError(_path);
        }
#else
        UIController.Instance.GetFile(ImagePathCallback,ChatFileType.FileType.Image);
#endif
    }

    public void OnClickEditName()
    {
//		freakNameInputField.text="Group  "+"\U0001F603";
        Debug.Log ("OnClickEditName");

        if (string.Equals(freakNameInputField.text, mGroupName))
        {
            Debug.Log("No Changes In Group Name");
            return;
        }

        if (ChatManager.Instance.isInternetConnected == false)
        {
            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
            return;
        }

        if (ChatManager.Instance.IsSeandbirdLogined == false)
        {
            ChatManager.Instance.ReConnectSendBird (OnClickEditName);
            return;
        }

        GroupSerializableRequestClass.UpdateRequest _updateRequest = new GroupSerializableRequestClass.UpdateRequest();
        _updateRequest.cover_url = mImageUrl;
		_updateRequest.name = WWW.EscapeURL( freakNameInputField.text);
		freakNameNativeField.InputReciver.OnBackButtonPress ();
        _updateRequest.data = JsonConvert.SerializeObject(adminMetaData);

        loadingObject.DisplayLoading ("Changing Name " + mGroupName + " to " + freakNameInputField.text);

//        ChatManager.Instance.UpdateGroup(mChannel_Url, _updateRequest, UpdateGroupCallback);
        ChatManager.Instance.UpdateGroup (WWW.EscapeURL(freakNameInputField.text), mImageUrl, adminMetaData, UpdateGroupCallback);
    }

    public void OnClickBack()
    {
        // Update Group.....
//        ChatManager.Instance.RefreshForNewMessages();

        if (FreakAppManager.Instance.GetHomeScreenPanel()) 
        {
            FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();

            if (FindObjectOfType<ChatWindowUI>())
            {
                ChatWindowUI _mChatWindowUI = FindObjectOfType<ChatWindowUI> ();//.InitChat (mChatChannel);
                if (_mChatWindowUI != null)
                {
                    _mChatWindowUI.InitChat (mChatChannel, true);
                    ChatManager.Instance.GetChannelByChannelUrl (mChatChannel.channelUrl, _mChatWindowUI.GetGroupChannelCallback);
                }
            }
        } 
    }

    public void OnClickAddMembers()
    {
        if (ChatManager.Instance.isInternetConnected == false)
        {
            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
            return;
        }

        if (ChatManager.Instance.IsSeandbirdLogined == false)
        {
            ChatManager.Instance.ReConnectSendBird (OnClickAddMembers);
            return;
        }

        ChatManager.Instance.memberList = new List<SendBird.User> (mMembers);
        GameObject obj = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ServerSynchedContactsDisplayPanel);
        obj.GetComponent<FreaksContactPanel> ().isAddMember = true;
        obj.GetComponent<FreaksContactPanel>().mChatChannel = mChatChannel;
//        FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.ServerSynchedContactsDisplayPanel);
    }

#endregion

    void ImagePathCallback(string path)
    {
        if (File.Exists(path))
        {
            mDeviceFilepath = path;
            Debug.Log("ImagePathCallback " + mDeviceFilepath);

            byte[] tempByte = TextureResize.GetResizedByteFromByte (groupProfilePicImg.rectTransform, Freak.StreamManager.LoadFileDirect (mDeviceFilepath));

            if (mImageFileByte.Length > 0 && mImageFileByte.SequenceEqual(tempByte) == true)
            {
                Debug.Log("SAME IMAGE========>>>>>");
                return;
            }
            else
            {
                SendBird.SBFile sendBirdFile = new SendBird.SBFile(path);
                ChatManager.Instance.UpdateGroup(mGroupName, sendBirdFile, adminMetaData, HandleSuccessCallbackMethod);

                loadingObject.DisplayLoading ("Changing Group Icon");
            }
        }
        else
        {
            Debug.Log("File Doesn't Exits......");
        }
    }

    //Send admin message after changing Group Profile Image...
    void HandleSuccessCallbackMethod (SendBird.GroupChannel groupChannel)
    {
        Debug.Log("HandleSuccessCallbackMethod");
        if (groupChannel != null)
        {
            mNewFilepath = StreamManager.LoadFilePath (Path.GetFileName (groupChannel.CoverUrl), FolderLocation.GroupProfile, false);
            StreamManager.CopyFile (mDeviceFilepath, mNewFilepath);
            LoadImageFromtexture(mDeviceFilepath);
            mDeviceFilepath = mNewFilepath;

            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.changeGroupPic;
            
            ChatManager.Instance.SendUserMessage (groupChannel, FreakAppConstantPara.MessageDescription.groupPicture, JsonConvert.SerializeObject (_metaData), string.Empty, MessageSendSuccessFullyCallback);

            CacheManager.UpdateGroupChannel (groupChannel);
        }
    
        loadingObject.gameObject.SetActive (false);
    }

    void UpdateGroupCallback (SendBird.GroupChannel groupChannel)
    {
        if (groupChannel != null)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.UserName +
                " changed the group name from " + mGroupName + " to " + freakNameInputField.text;

            ChatManager.Instance.SendUserMessage(groupChannel, "Group Name Changed", JsonConvert.SerializeObject(_metaData), string.Empty, MessageSendSuccessFullyCallback);

			mGroupName = WWW.EscapeURL(freakNameInputField.text);

            CacheManager.UpdateGroupChannel (groupChannel);
        }

        loadingObject.gameObject.SetActive (false);
    }

    void MessageSendSuccessFullyCallback ()
    {
        loadingObject.gameObject.SetActive (false);
    }

    void AddGroupMember (List<string> ids)
    {
        loadingObject.DisplayLoading ("Ading New Member in " + mGroupName);

        ChatManager.Instance.InviteUser(ids, MemberAddedCallback);
    }

    void MemberAddedCallback ()
    {
        for (int i = 0; i < ChatManager.Instance.groupUsersList.Count; i++)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.groupUsersList[i].userName + FreakAppConstantPara.MessageDescription.addedInGroup;

            ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "New Group Member", JsonConvert.SerializeObject(_metaData));
        }

        ChatManager.Instance.groupUsersList.Clear ();
        ChatManager.Instance.GetChannelByChannelUrl(mChannel_Url, GetChannelCallback);
    }

    public void DeleteGroupMember (string id, string name)
    {
        removed_id = id;
        removed_name = name;

        //Check the removed member is admin itself
        if (removed_id == adminMetaData.createdBy)
        {
            if (mMembers.Count == 1)
            {
                //Only one member delete the group....
                ChatManager.Instance.DeleteChannel(mChannel_Url, OnDeleteGroupCallback);

                loadingObject.DisplayLoading ("Deleting group");
            }
            else
            {
                for (int i = 0; i < mMembers.Count; i++)
                {
                    if(mMembers[i].UserId != removed_id)
                    {
                        adminMetaData.ChangeMainAdmin(mMembers[i].UserId);

                        removed_id = mMembers[i].UserId;
                        removed_name = mMembers[i].Nickname;

                        GroupSerializableRequestClass.UpdateRequest _updateRequest = new GroupSerializableRequestClass.UpdateRequest();
                        _updateRequest.cover_url = mImageUrl;
						_updateRequest.name = WWW.EscapeURL(freakNameInputField.text);
                        _updateRequest.data = JsonConvert.SerializeObject(adminMetaData);

//                        ChatManager.Instance.UpdateGroup(mChannel_Url, _updateRequest, OnGroupAdminChange);
                        ChatManager.Instance.UpdateGroup (mGroupName, mImageUrl, adminMetaData, OnGroupAdminChange);
                        return;
                    }
                }

                loadingObject.DisplayLoading ("Leaving from group");
            }
        }
        else
        {
            //Remove from group....
            GroupSerializableRequestClass.LeaveGroup _leaveGroup = new GroupSerializableRequestClass.LeaveGroup();
            _leaveGroup.user_ids.Add(removed_id);

            ChatManager.Instance.LeaveCurrentChannel(mChannel_Url, _leaveGroup, DeleteGroupMemberCallback);

            loadingObject.DisplayLoading ("Removing " + removed_name + " from group");
        }
    }

    void OnGroupAdminChange (SendBird.GroupChannel groupChannel)
    {
        if (groupChannel != null)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData1 = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData1.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData1.message = ChatManager.Instance.UserName + " changed admin to " + removed_name;
            
            ChatManager.Instance.SendUserMessage(groupChannel, "Group Admin Changed", JsonConvert.SerializeObject(_metaData1), string.Empty, GroupAdminChangedCallback);

            CacheManager.UpdateGroupChannel (groupChannel);
        }
    }

    void GroupAdminChangedCallback ()
    {
        FreakChatSerializeClass.NormalMessagerMetaData _metaData2 = new FreakChatSerializeClass.NormalMessagerMetaData();
        _metaData2.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
        _metaData2.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.leftGroup;

        ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "Group Member Left", JsonConvert.SerializeObject(_metaData2), string.Empty, GroupMemberLeftCallback);
    }

    void GroupMemberLeftCallback ()
    {
        GroupSerializableRequestClass.LeaveGroup _leaveGroup = new GroupSerializableRequestClass.LeaveGroup();
        _leaveGroup.user_ids.Add(ChatManager.Instance.UserId);

        ChatManager.Instance.LeaveCurrentChannel(mChannel_Url, _leaveGroup, OnDeleteGroupCallback);
    }

    void OnDeleteGroupCallback (bool success)
    {
        Debug.Log("OnDeleteGroupCallback " + success);
        if (success)
        {
//            ChatManager.Instance.OnChannelDeleted (mChannel_Url);
            OnClickBack();
        }
        else
        {
            Debug.Log("OnDeleteGroupCallback Error");
        }

        loadingObject.gameObject.SetActive (false);
    }

    void DeleteGroupMemberCallback (bool success)
    {
        Debug.Log("DeleteGroupMemberCallback " + success);
        if (success)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = removed_name + FreakAppConstantPara.MessageDescription.removedFromGroup;
            
            ChatManager.Instance.GetChannelByChannelUrl(mChannel_Url, GetChannelCallback);
            ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "Group Member Removed", JsonConvert.SerializeObject(_metaData));
        }
    }

    void GetChannelCallback (SendBird.GroupChannel channel)
    {
        if (mChannel_Url == channel.Url)
        {
            mMembers = new List<SendBird.User> (channel.Members);

            MemberUpdate();
        }

        loadingObject.gameObject.SetActive (false);
    }

	void FileSavedCallback (bool success)
    {
        if (mChatChannel != null)
        {
            groupProfilePicImg.FreakSetTexture(ChatManager.Instance.currentSelectedChatChannel.imageCache);
            return;
        }

        Debug.Log("FileSavedCallback" + success);
        string _filePath = StreamManager.LoadFilePath(Path.GetFileName(mImageUrl), Freak.FolderLocation.GroupProfile, false);

        LoadImageFromtexture(_filePath);
    }

    void DownloadImage ()
    {
        if (string.IsNullOrEmpty (mImageUrl) == false)
        {
            string _filePath = StreamManager.LoadFilePath (Path.GetFileName (mImageUrl), Freak.FolderLocation.GroupProfile, false);
            if (File.Exists (_filePath))
            {
                if (mChatChannel.imageCache != null)
                {
                    mImageFileByte = mChatChannel.imageCache;
                    groupProfilePicImg.FreakSetTexture (mImageFileByte);
                }
                else
                {
                    //If file already in local then Display the Image
                    LoadImageFromtexture (_filePath);
                }
            }
            else
            {
                DownloadManager.Instance.DownLoadFile (mImageUrl, FolderLocation.GroupProfile, FileSavedCallback, mChannel_Url);
            }
        }
    }

    void LoadImageFromtexture(string filePath)
    {
        mImageFileByte = TextureResize.GetResizedByteFromByte (groupProfilePicImg.rectTransform, Freak.StreamManager.LoadFileDirect (filePath));
        groupProfilePicImg.FreakSetTexture (mImageFileByte);
        mChatChannel.imageCache = mImageFileByte;
    }

    List<string> GroupMemberIdList ()
    {
        List<string> userIdList = new List<string>();
        for (int i = 0; i < mMembers.Count; i++)
        {
            userIdList.Add(mMembers[i].UserId);
        }

        return userIdList;
    }

    void OnDisable()
    {
        Destroy (this.gameObject);
    }
}
