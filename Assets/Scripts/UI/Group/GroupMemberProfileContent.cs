﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Events;
using Tacticsoft;
using Tacticsoft.Examples;
using UnityEngine.UI;
using Freak.Chat;
using Freak;

public class GroupMemberProfileContent : TableViewCell {

	public GameObject removeBtn;
	public Text nameText;
	public Text descText;
	public Image profileImage;
    public GroupProfileUIController groupProfileUIController;
    public Button removeButton;

	#region TableView
	public Slider m_cellHeightSlider;

	public int rowNumber { get; set; }
	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;
	#endregion

    private string id;
    private string profileImgLink;

	// Use this for initialization
	void Start () {
	
	}

    public void OnClickRemoveButton ()
    {
        if (ChatManager.Instance.isInternetConnected == false)
        {
            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
            return;
        }

        if (ChatManager.Instance.IsSeandbirdLogined == false)
        {
            ChatManager.Instance.ReConnectSendBird (null);
            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Network Problem Please Try Later.");
            return;
        }


        removeButton.interactable = false;
        groupProfileUIController.DeleteGroupMember(id, nameText.text);
    }

    public void InitGroupMemberDetails (GroupSerializableResponseClass.ChannelMember member,
        GroupSerializableResponseClass.MetaData adminMetaData)
    {
        id = member.user_id;
        nameText.text = member.nickname;

        if (adminMetaData.createdBy == member.user_id)
        {
            descText.text = "Group Admin";
        }
        else
        {
            descText.text = string.Empty;
        }

        if (adminMetaData.createdBy == ChatManager.Instance.UserId)
        {
            removeBtn.SetActive(true);   
        }
        else
        {
            removeBtn.SetActive(false);   
        }
    }

    public void InitGroupMemberDetails (ChatUser member, GroupSerializableResponseClass.MetaData adminMetaData)
    {
        id = member.userId;
        nameText.text = member.nickname;
        profileImgLink = member.profileUrl;

        if (adminMetaData.createdBy == id)
        {
            descText.text = "Group Admin";
        }
        else
        {
            descText.text = string.Empty;
        }

        if (adminMetaData.createdBy == ChatManager.Instance.UserId)
        {
            removeBtn.SetActive(true);   
        }
        else
        {
            removeBtn.SetActive(false);   
        }

        string filePath = StreamManager.LoadFilePath(Path.GetFileName(profileImgLink), FolderLocation.Profile, false);

        if (File.Exists(filePath))
        {
            DownloadProfileImageCallback(true);
        }
        else
        {
            DownloadManager.Instance.DownLoadFile(profileImgLink, FolderLocation.Profile, DownloadProfileImageCallback);
        }
    }

    void DownloadProfileImageCallback (bool success)
    {
        string filePath = StreamManager.LoadFilePath(Path.GetFileName(profileImgLink), FolderLocation.Profile, false);
        Debug.Log("FileSavedCallback" + filePath);

        if (File.Exists(filePath))
        {
            /*Texture2D texture = null;
            byte[] fileByte = System.IO.File.ReadAllBytes(filePath);
            texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
            texture.LoadImage(fileByte);

            Rect rect = new Rect(0, 0, texture.width, texture.height);
            profileImage.sprite = Sprite.Create(texture, rect, new Vector2(0, 0), 1f);*/

            Texture2D texture = new Texture2D(4,4);
            texture.LoadImage(Freak.StreamManager.LoadFileDirect (filePath));
            profileImage.FreakSetSprite (TextureResize.ResizeTexture (150f, 150f, texture));
            texture = null;
        }
    }

    public void OnClickOpenprofile ()
    {
        FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = profileImage.sprite.texture;

        FreakAppManager.Instance.myUserProfile.memberId = id;
        FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
    }

	#region TableView
	public void SliderValueChanged(Slider slider) {
		float value = slider.value;
		Debug.Log ("Value --- >" + value);
		onCellHeightChanged.Invoke(rowNumber, value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}
	#endregion
}
