﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.Collections.Generic;
using Freak.Chat;

public class GroupMemberSearch :  MonoBehaviour, ITableViewDataSource  {

	#region TableView
	public ContactDetailsDisplay m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;
	private Dictionary<int, float> m_customRowHeights;
	private ImageController imageController;

	#endregion

	//    public List<FreakChatSerializeClass.FreakMessagingChannel.FreakMember> searchedMemberList = new List<FreakChatSerializeClass.FreakMessagingChannel.FreakMember>();
	public List<ChatUser> searchedMembersList = new List<ChatUser>();

	void OnEnable()
	{
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) 
	{
		return searchedMembersList.Count;
		//        return searchedMemberList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) 
	{
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
		if (cell == null) {
			cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}

	void AssignDetails(ContactDetailsDisplay cell, int rowNumb)
	{
		cell.InitTagSearchMember(searchedMembersList[rowNumb]);
		if(imageController == null)
		{
			imageController = FindObjectOfType<ImageController>();
		}
		imageController.GetImageFromLocalOrDownloadImage(searchedMembersList[rowNumb].profileUrl, cell.img, Freak.FolderLocation.Profile);
		//        cell.InitTagSearchMember(searchedMemberList[rowNumb]);
	}

	private float GetHeightOfRow(int row) 
	{
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) 
	{
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}

	#endregion

}