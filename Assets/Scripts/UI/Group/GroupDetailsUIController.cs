﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Freak;
using Freak.Chat;
using System.IO;

public class GroupDetailsUIController : MonoBehaviour {

	public InputField groupNameInputField;
	private NativeEditBox groupNameNativeField;
    public Image groupProfilePicImg;
    public HomeScreenUIPanelController homeScreenUIPanelController;
    public ConnectingUI loadingObject;

    private string mDeviceFilepath;
    private string mImageUrl;
    private GroupSerializableResponseClass.MetaData gropuMetaData;
    private byte[] mImageFileBytes;
    private IEnumerator waitRoutine;
    private bool onCreateGroupClicked = false;
    private Sprite defaultGroupIcon;
    private SendBird.GroupChannel newGroupChannel;

    void OnEnable()
    {
        if (defaultGroupIcon == null)
        {
            defaultGroupIcon = groupProfilePicImg.sprite;
        }
        else
        {
            groupProfilePicImg.sprite = defaultGroupIcon;
        }

		groupNameInputField.text = string.Empty;
		groupNameNativeField = groupNameInputField.gameObject.GetComponent<NativeEditBox> ();
        groupNameNativeField.SetTextNative(string.Empty);
        mDeviceFilepath = string.Empty;
        mImageUrl = string.Empty;
        onCreateGroupClicked = false;
        loadingObject.gameObject.SetActive (false);
    }

#region UI Event

    public void OnClickEditGropupProfilePicture()
    {
#if UNITY_EDITOR
        string _path = Application.persistentDataPath + "/group.jpg";
        if (File.Exists(_path))
        {
            Debug.Log(_path);
            ProfileImagePath(_path);
        }
        else
            Debug.LogError(_path);
#else
        UIController.Instance.GetFile(ProfileImagePath,ChatFileType.FileType.Image);
#endif
    }

    public void OnClickBack()
    {
        ChatManager.Instance.groupUsersList.Clear ();
        homeScreenUIPanelController.PopPanel ();
    }

    public void OnClickDone()
    {
        #if UNITY_EDITOR
        if (string.IsNullOrEmpty (groupNameInputField.GetComponent<InputField>().text) == false)
        {
            if (string.IsNullOrEmpty(mDeviceFilepath))
            {
                ShowErrorPopUp("");
            }
            else
            {
                groupNameNativeField.SetEnabled (false);
                CreateGroup();
            }
        } 
        else {
//            Debug.Log ("Enter Group name");
            ShowErrorPopUp ("Enter group name first.");
        }
        #else
		if (string.IsNullOrEmpty (groupNameInputField.text) == false)
        {
			groupNameNativeField.SetEnabled (false);
            CreateGroup();
        } 
        else {
            Debug.Log ("Enter Group name");
        }
        #endif
    }

#endregion

    void ProfileImagePath(string path)
    {
        if (File.Exists(path))
        {
            mDeviceFilepath = path;

            LoadImageFromtexture(mDeviceFilepath);
            mDeviceFilepath = StreamManager.LoadFilePath((groupNameInputField.text + ".jpg"), FolderLocation.GroupProfile, false);
			SaveFile (mDeviceFilepath, mImageFileBytes, FileSavedCallback);
        }
        else
        {
            Debug.Log("File Doesn't Exits......");
        }
    }

	void CreateGroup ()
    {
//        if (ChatManager.CheckInternetConnection())
        {
            onCreateGroupClicked = true;
            
            //Spawn Loading panel
			loadingObject.DisplayLoading("Creating Group " + groupNameInputField.text);
            
            // Check whether the profile image is selected or not
            if (string.IsNullOrEmpty(mDeviceFilepath))
            {

                //If not selected upload default image
                //first save default image texture to file....
				mDeviceFilepath = StreamManager.LoadFilePath((groupNameInputField.text+".jpg"), Freak.FolderLocation.GroupProfile, false);
                mImageFileBytes = groupProfilePicImg.sprite.texture.EncodeToJPG ();
//				StreamManager.SaveFile((groupNameInputField.text + ".jpg"), mImageFileBytes, FolderLocation.GroupProfile, FileSavedCallback, false);
				string filepath = StreamManager.LoadFilePath((groupNameInputField.text + ".jpg"), FolderLocation.GroupProfile, false);
				SaveFile (filepath, mImageFileBytes, FileSavedCallback);
            }
            else
            {
                // Create group...
                SendBird.SBFile sendBirdFile = new SendBird.SBFile(mDeviceFilepath);
                CreateNewGroup(sendBirdFile);
            }
        }
//        else
//        {
//            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Network Problem!");
//        }
    }

    void CreateNewGroup (SendBird.SBFile sendBirdFile)
    {
        //Create all users list...
        List<string> userIdList = new List<string>();
        for (int i = 0; i < ChatManager.Instance.groupUsersList.Count; i++)
        {
            if (userIdList.Count > 0 && userIdList.Contains (ChatManager.Instance.groupUsersList [i].userId))
                continue;
                
            userIdList.Add(ChatManager.Instance.groupUsersList[i].userId);
        }

        if (userIdList.Count > 0 && userIdList.Contains (ChatManager.Instance.UserId) == false)
            userIdList.Add (ChatManager.Instance.UserId);

        gropuMetaData = new GroupSerializableResponseClass.MetaData(ChatManager.Instance.UserId);
		ChatManager.Instance.CreateNewGroupChannel(userIdList, groupNameInputField.text, sendBirdFile, gropuMetaData, OnCreateNewGroupCallback);
    }

    void OnCreateNewGroupCallback (SendBird.GroupChannel groupChannel)
    {
        if (groupChannel != null)
        {
            newGroupChannel = groupChannel;
            string newFilepath = StreamManager.LoadFilePath(Path.GetFileName(groupChannel.CoverUrl), FolderLocation.GroupProfile, false);
            System.IO.File.Move(mDeviceFilepath, newFilepath);
            System.IO.File.Delete(mDeviceFilepath);
            mDeviceFilepath = newFilepath;

            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.createdNewGroup;

            string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
            ChatManager.Instance.SendUserMessage (groupChannel, FreakAppConstantPara.MessageDescription.newGroup, _data, string.Empty, OnSendMessageCreatingGroupCallback);
        }
        else
        {
            Debug.Log("Retry creating group....");
        }
    }

    void OnSendMessageCreatingGroupCallback ()
    {
        SendBird.BaseChannel baseChannel = newGroupChannel;
        SendBird.BaseMessage baseMessage = newGroupChannel.LastMessage;
        ChatManager.Instance.NewMessageReceivedQueryNew (baseChannel, baseMessage);

        StartCoroutine(WaitForRefresh());
    }

    void FileSavedCallback (bool success)
    {
        Debug.Log("FileSavedCallback" + success);
        if (success)
        {
            if (onCreateGroupClicked)
            {
                SendBird.SBFile sendBirdFile = new SendBird.SBFile(mDeviceFilepath);
                CreateNewGroup(sendBirdFile);
                onCreateGroupClicked = false;
            }
        }
        else
        {
            mDeviceFilepath = string.Empty;
        }
    }

    void LoadImageFromtexture(string filePath)
    {
        Texture2D texture = new Texture2D(4,4);
        mImageFileBytes = Freak.StreamManager.LoadFileDirect (filePath);
        texture.LoadImage (mImageFileBytes);
        groupProfilePicImg.FreakSetSprite (TextureResize.ResizeTexture (150f, 150f, texture));
        texture = null;
    }

    IEnumerator WaitForRefresh ()
    {
        yield return new WaitForEndOfFrame ();

        loadingObject.gameObject.SetActive (false);

        ChatManager.Instance.groupUsersList.Clear();
        FreakAppManager.Instance.showAllChats = true;
        ChatManager.Instance.currentSelectedChatChannel = null;
        ChatManager.Instance.currentChannel = null;
        SceneManager.LoadScene (FreakAppConstantPara.UnitySceneName.FreakHomeScene);
    }

	void SaveFile (string filePath, byte[] data, System.Action<bool> Callback)
	{
		try
		{
			using (var file = new FileStream(filePath, FileMode.Create, FileAccess.Write))
			{
				file.Write(data, 0, data.Length);
				Debug.Log(file.Name);
			}
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);

			if (Callback != null)
				Callback(false);

			return;
		}

		if (Callback != null)
			Callback (true);
	}

    [Header("Error POPUP")]
    public Text errorMessage;
    public GameObject errorPanel;
    public GameObject iconErrorPanel;
    private CanvasGroup mCanvasGroup;

    public void ShowErrorPopUp (string errorMsg)
    {
        if (string.IsNullOrEmpty (errorMsg))
        {
            iconErrorPanel.SetActive(true);
        }
        else
        {
            errorMessage.text = errorMsg;
            iconErrorPanel.SetActive(false);
        }

        errorPanel.SetActive(true);

        LTDescr _AlphaValue = LeanTween.value(this.gameObject, 0, 1f, 0.2f);
        _AlphaValue.hasUpdateCallback = true;
        _AlphaValue.onUpdateFloat = UpdateCanvasGroup;

    }

    void UpdateCanvasGroup (float alphaValue)
    {
        if (mCanvasGroup == null)
        {
            mCanvasGroup = errorPanel.GetComponent<CanvasGroup>();
        }

        mCanvasGroup.alpha = alphaValue;
    }

    public void OnClickCloseErrorPopup ()
    {
        CloseErrorPopUpPanel();
    }

    public void OnClickYesButton()
    {
        groupNameNativeField.SetEnabled (false);
        CreateGroup();
    }

    public void OnClickCancelButton()
    {
        CloseErrorPopUpPanel();
    }

    void CloseErrorPopUpPanel ()
    {
        LTDescr _AlphaValue = LeanTween.value(this.gameObject, 1, 0f, 0.2f);
        _AlphaValue.hasUpdateCallback = true;
        _AlphaValue.onUpdateFloat = UpdateCanvasGroup;
        _AlphaValue.onComplete = DeactiveErrorPanel;
    }

    void DeactiveErrorPanel ()
    {
        errorPanel.SetActive(false);
        errorMessage.text = string.Empty;
    }
}
