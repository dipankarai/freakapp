﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Chat;

using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;


public class AllChatsUIController : MonoBehaviour, IEnhancedScrollerDelegate /*ITableViewDataSource*/  {

    public HomeScreenUIPanelController homeScreenUIPanelController;
    int currentCount;
    public List<Contact> dummyContactsList;

    public ContactDetailsDisplay m_cellPrefab;
    public TableView m_tableView;

	public EnhancedScrollerCellView cellViewPrefab;

    private int m_numInstancesCreated = 0;

    private Dictionary<int, float> m_customRowHeights;
    public GetContactListApiManager getContactListApiManager;

    public List<ServerUserContact> serverContactList;

    public GameObject searchBar;
	public InputField searchBarTextField;
    public ConnectingUI loadingObj;
	NativeEditBox searchBarEditField;
    public Button searchBtn;
    public Button freakBtn;

	List<ChatChannel> mFreakChachedChannelList = new List<ChatChannel>();

	Vector3 bottomPanelPosition;
	private RectTransform SearchBoxTransform;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;
	//public bool isRightSlide;

	public enum SlideStatus{
		Straight,
		Right,
		Left
	}
	public SlideStatus slideStatus;


	public Transform panelPos;
	public bool isSlide;

	public GameObject NoChatsDefaultMsgObj;
	public List<Allchats> newChatList = new List<Allchats>();

    void Awake ()
    {
    }

	void Start()
	{
		serverContactList = new List<ServerUserContact> ();
		#if UNITY_EDITOR
		foreach(Contact detailsObj in dummyContactsList)
		{
			Contacts.ContactsList.Add( detailsObj );
			Debug.Log(Contacts.ContactsList.Count);
		}
		#endif
		bottomPanelPosition = searchBar.transform.rectTransform ().localPosition;
	
	}


//	public void SlideScreen(SlideStatus status)
//	{
//		slideStatus = status;
//		if (slideStatus == SlideStatus.Right) 
//		{
//			this.transform.position = new Vector3 (800, 0, 0);
//			LeanTween.move (this.gameObject, panelPos.position, 0.3f).setEase (LeanTweenType.easeInSine);
//			slideStatus = SlideStatus.Straight;
//		} 
//		else if (slideStatus == SlideStatus.Left) 
//		{
//			this.transform.position = new Vector3 (-800, 0, 0);
//			LeanTween.move (this.gameObject, panelPos.position, 0.3f).setEase (LeanTweenType.easeInSine);
//			slideStatus = SlideStatus.Straight;
//		} 
//		else {
//			this.transform.localPosition = panelPos.position;
//			slideStatus = SlideStatus.Straight;
//		}
//	}

    System.Action OnCompleteSwipeHandler;
	public void SlideScreen(SlideStatus status, System.Action onCompleteAction)
	{
        OnCompleteSwipeHandler = onCompleteAction;

		Vector3 _rightPosition = homeScreenUIPanelController.panaelPosObj.localPosition;
        _rightPosition.x -= this.rectTransform().rect.width;

        if (status == SlideStatus.Right) 
        {
            isSlide = true;
            this.rectTransform().localPosition = _rightPosition;
            LeanTween.move(this.rectTransform(), panelPos.rectTransform().localPosition, 0.4f).setEase(LeanTweenType.easeInSine).setOnComplete(() =>
                {
                    if (OnCompleteSwipeHandler != null)
                    {
                        OnCompleteSwipeHandler();
                    }
                });
            slideStatus = SlideStatus.Right;
        } 
        else if (status == SlideStatus.Left) 
        {
            isSlide = true;
            LeanTween.move (this.rectTransform(), _rightPosition, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete(OnDisables);
            slideStatus = SlideStatus.Left;
        } 

        /*if (slideStatus == SlideStatus.Right) 
		{
			isSlide = true;
			this.transform.localPosition = new Vector3 (800, 0, 0);
			LeanTween.move (this.gameObject, panelPos.position, 0.6f).setEase (LeanTweenType.easeInSine).setOnComplete(OnEnables);
			slideStatus = SlideStatus.Straight;
		} 
		else if (slideStatus == SlideStatus.Left) 
		{
			isSlide = true;
			this.transform.localPosition = new Vector3 (-800, 0, 0);
			LeanTween.move (this.gameObject, panelPos.position, 0.6f).setEase (LeanTweenType.easeInSine).setOnComplete(OnEnables);
			slideStatus = SlideStatus.Straight;
		} 
		else {
			this.transform.position = panelPos.position;
			slideStatus = SlideStatus.Straight;
			OnEnables ();
		}*/
	}

	void OnEnable()
	{
        ChatManager.Instance.StartRefreshCoroutine ();
        ChatManager.Instance.newMessageUpdateCallback = NewUpdatesCallback;
        ChatManager.Instance.onMessageTypingAllChatCallback = OnMessageTyping;

        FreakAppManager.Instance.isLoaderOn = true;
//        loadingObj.DisplayLoading ();
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}


	void InitialCall()
	{
		Resources.UnloadUnusedAssets();
		//        InitSearchBar ();
		m_customRowHeights = new Dictionary<int, float>();

		LoadFreakChatTab ();


//		m_tableView.dataSource = this;

		scroller.Delegate = this;
        scroller.ReloadData();
//		LoadLargeData ();

//        if (m_tableView.dataSource == null)
//            m_tableView.dataSource = this;
//        else
//            m_tableView.RefreshVisibleRows();
//        
//        m_tableView.scrollY = 0;


        InitAllChat();
//        if(this.gameObject.activeSelf)
        //      StartCoroutine(Init ());
	}
		

    void InitAllChat ()
    {
        loadingObj.HideLoading();
        FreakAppManager.Instance.isLoaderOn = false;
        isSlide = false;
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Slided");
        #endif
        if (!searchBarEditField)
            searchBarEditField = searchBarTextField.gameObject.GetComponent<NativeEditBox> ();
        
        this.gameObject.GetComponent<RectTransform> ().position = panelPos.position;
    }

	IEnumerator Init(){
		yield return new WaitForSeconds (0.05f);
//		if (!isSlide) {
			OnEnables ();
//		}
	}
    /*void InvokeCheckForNewMessages ()
    {
        if (this.gameObject.activeInHierarchy == false)
        {
            Invoke("InvokeCheckForNewMessages", 1f);
            return;
        }

        if (checkMessageRoutine != null)
            StopCoroutine(checkMessageRoutine);

        checkMessageRoutine = CheckNewMessageRoutine();
        StartCoroutine(checkMessageRoutine);
    }

    IEnumerator checkMessageRoutine;
    IEnumerator CheckNewMessageRoutine ()
    {
        yield return new WaitForSeconds(0.5f);
//        ChatManager.Instance.RefreshForNewMessages();
//        Invoke("InvokeCheckForNewMessages", 20f);
    }*/

	IEnumerator ShowOnEnable(){
		yield return new WaitForSeconds (0.2f);

		isSlide = false;
		#if UNITY_EDITOR && UNITY_DEBUG
		Debug.Log ("Slided");
		#endif
		if (!searchBarEditField)
			searchBarEditField = searchBarTextField.gameObject.GetComponent<NativeEditBox> ();
        this.gameObject.GetComponent<RectTransform> ().position = panelPos.position;
        
        /*ChatManager.Instance.StartRefreshCoroutine ();
		ChatManager.Instance.newMessageUpdateCallback = NewUpdatesCallback;
		ChatManager.Instance.onMessageTypingAllChatCallback = OnMessageTyping;
		InitSearchBar ();
		m_customRowHeights = new Dictionary<int, float>();

		LoadFreakChatTab ();

		m_tableView.dataSource = this;*/
	}

	void OnEnables()
	{
		StartCoroutine (ShowOnEnable ());
    }

    void OnDisables ()
    {
        isSlide = false;
        gameObject.SetActive(false);
        //this.rectTransform().localPosition = panelPos.rectTransform().localPosition;
    }

    void NewUpdatesCallback()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("NewUpdateCallback");
        #endif
        LoadFreakChatTab ();

//        Resources.UnloadUnusedAssets ();

//        m_tableView.dataSource = this;
//        m_tableView.ReloadData();
        scroller.RefreshActiveCellViews();
//        m_tableView.RefreshVisibleRows();
    }

    public void HideSearchBar()
	{
        searchBarTextField.text=string.Empty;;
        OnTextEdit (searchBarTextField);
        /*mFreakChannelList.Clear();
        mFreakChannelList = new List<SendBird.GroupChannel>(ChatManager.Instance.userAllChannelList);*/
        InitSearchBar ();
	}

    void InitSearchBar ()
	{
//		if (searchBarEditField.InputReciver && searchBarEditField.InputReciver.InputfieldMoveUp ()) {
//			searchBarEditField.SetTextNative ("");
//			searchBarEditField.InputReciver.OnBackButtonPress ();
//		}
//		else 
//		{
//		}
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
        searchBtn.gameObject.SetActive (true);
        freakBtn.gameObject.SetActive (true);
        searchBarStatus = SearchBarStatus.NotDisplayed;
    }

    void LoadFreakChatTab ()
    {
        mFreakChachedChannelList.Clear ();
//        mFreakChachedChannelList = new List<ChatChannel>(ChatManager.Instance.cachedData.allChannelList);
        if (ChatManager.Instance.cachedData.allChannelList.Count > 0)
        {
            for (int i = 0; i < ChatManager.Instance.cachedData.allChannelList.Count; i++)
            {
                mFreakChachedChannelList.Add(InitAllChatChannelDetails(ChatManager.Instance.cachedData.allChannelList[i]));
            }
        }

		if (mFreakChachedChannelList.Count > 0) 
		{
			NoChatsDefaultMsgObj.SetActive (false);
		} 
		else {
			NoChatsDefaultMsgObj.SetActive (true);
		}

        scroller.ReloadData();
    }

    public ScrollRect scrollRect;
    /*
    private List<ContactDetailsDisplay> mContactDetailsDisplayList = new List<ContactDetailsDisplay>();
    void TestSimpleScroll ()
    {
        for (int i = 0; i < mContactDetailsDisplayList.Count; i++)
        {
            Destroy(mContactDetailsDisplayList[i].gameObject);
        }

        mContactDetailsDisplayList.Clear();

        for (int i = 0; i < mFreakChachedChannelList.Count; i++)
        {
            ContactDetailsDisplay _contact = (ContactDetailsDisplay)Instantiate(m_cellPrefab);
            _contact.transform.SetParent(scrollRect.content.transform, false);
            _contact.InitFreakMessagingChannelNew(mFreakChachedChannelList[i]);
            mContactDetailsDisplayList.Add(_contact);
        }
    }*/

    ChatChannel InitAllChatChannelDetails (ChatChannel chatChannel)
    {
        ChatChannel _chatChannel = new ChatChannel(chatChannel);
        return _chatChannel;
    }

	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		freakBtn.gameObject.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;
	}

	public void OnClickFreaks()
	{
		homeScreenUIPanelController.PushPanel(HomeScreenUIPanelController.PanelType.freakPanel);
	}

	public void OnTextEdit(InputField searchinput)
	{
        #if UNITY_EDITOR || UNITY_DEBUG
		Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text);
        #endif

		string searchinputText = searchinput.text;
		if (searchinputText.Length > 0) 
        {
            if (mFreakChachedChannelList.Count > 1)
            {
                for (int i = 0; i < ChatManager.Instance.cachedData.allChannelList.Count; i++)
                {
                    if (ChatManager.Instance.cachedData.allChannelList[i].isGroupChannel)
                    {
						if (ChatManager.Instance.cachedData.allChannelList [i].channelName.ToUpper ().Contains (searchinputText.ToUpper ())) 
                        {
                            AddToList (ChatManager.Instance.cachedData.allChannelList [i]);
                        }
                        else
                        {
                            RemoveFromList (ChatManager.Instance.cachedData.allChannelList [i].channelUrl);
                        }
                    }
                    else
                    {
                        ChatUser _user = ChatManager.GetSender(ChatManager.Instance.cachedData.allChannelList[i].channelMembers);
						if (_user.nickname.ToUpper().Contains(searchinputText.ToUpper()))
                        {
                            AddToList(ChatManager.Instance.cachedData.allChannelList[i]);
                        }
                        else
                        {
                            RemoveFromList(ChatManager.Instance.cachedData.allChannelList[i].channelUrl);
                        }
                    }
                }
            }
        }
        else
        {
            LoadFreakChatTab ();
        }

//        m_tableView.ReloadData ();
        scroller.ReloadData();
	}

    void AddToList(ChatChannel channel)
    {
        for (int i = 0; i < mFreakChachedChannelList.Count; i++)
        {
            if (mFreakChachedChannelList[i].channelUrl == channel.channelUrl)
            {
                return;
            }
        }

//		mFreakChachedChannelList.Add(channel);
        mFreakChachedChannelList.Add(InitAllChatChannelDetails(channel));
    }

    void RemoveFromList (string channelUrl)
    {
        for (int i = 0; i < mFreakChachedChannelList.Count; i++)
        {
            if (mFreakChachedChannelList[i].channelUrl == channelUrl)
            {
                mFreakChachedChannelList.RemoveAt(i);
                return;
            }
        }
    }

    /*void AddToList(FreakChatSerializeClass.FreakMessagingChannel channel)
    {
        for (int i = 0; i < mFreakMessagingChannel.Count; i++)
        {
            if(mFreakMessagingChannel[i].id == channel.id)
            {
                return;
            }
        }

        mFreakMessagingChannel.Add(channel);
    }

    void RemoveFromList (long channelId)
    {
        for (int i = 0; i < mFreakMessagingChannel.Count; i++)
        {
            if(mFreakMessagingChannel[i].id == channelId)
            {
                mFreakMessagingChannel.RemoveAt(i);
                return;
            }
        }
    }*/

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

//	//Will be called by the TableView to know how many rows are in this table
//	public int GetNumberOfRowsForTableView(TableView tableView) {
//
//        if (mFreakChachedChannelList.Count > 0) {
//            return mFreakChachedChannelList.Count;
//        } else {
//            return 0;
//        }
//	}
//
//	//Will be called by the TableView to know what is the height of each row
//	public float GetHeightForRowInTableView(TableView tableView, int row) {
//		return GetHeightOfRow(row);
//	}
//
//	//Will be called by the TableView when a cell needs to be created for display
//	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
//	{
//        ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
//		if (cell == null) {
//            cell = (ContactDetailsDisplay)GameObject.Instantiate (m_cellPrefab);
//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//            cell.mTableView = m_tableView;
//		}
//
//		cell.rowNumber = row;
//		AssignDetails (cell, row);
//		cell.height = GetHeightOfRow(row);
//		return cell;
//	}
//
//	void AssignDetails(ContactDetailsDisplay cell, int rowNumb)
//	{
//		
//		if (IsOdd (rowNumb)) {
//			SetGreyColor (cell);
//		} 
//		else {
//			SetWhiteColor (cell);
//		}
//
//        cell.InitFreakMessagingChannelNew(mFreakChachedChannelList[rowNumb]);
////        cell.ShowProfileImageURL(mFreakChachedChannelList[rowNumb].channelUrl, mFreakChachedChannelList[rowNumb].isGroupChannel);
//	}

    #endregion

	public bool IsOdd(int value)
	{
		return value % 2 != 0;
	}


	void SetGreyColor(ContactDetailsDisplay cell)
	{
		cell.GetComponent<Image> ().color = new Color(229f/255f, 229f/255f, 229f/255f);
	}


	void SetWhiteColor(ContactDetailsDisplay cell)
	{
		cell.GetComponent<Image> ().color = Color.white;
	}


	void SetGreyColor(FreakAllChatsCellView cell)
	{
		cell.GetComponent<Image> ().color = new Color(229f/255f, 229f/255f, 229f/255f);
	}


	void SetWhiteColor(FreakAllChatsCellView cell)
	{
		cell.GetComponent<Image> ().color = Color.white;
	}

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}

    /*void Update()
	{
        if (Input.GetKeyDown(KeyCode.P))
        {
//            scroller.RefreshActiveCellViews();
            Typing(true);
        }

		#if UNITY_ANDROID_AA
		if (Input.GetKeyDown(KeyCode.Escape)&& searchBar.activeInHierarchy)
		{
			HideSearchBar();
		}
		#endif
	}
*/

	public void OnClickShowContacts()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}
		
	public void OnClickBack()
	{
		homeScreenUIPanelController.PopPanel();
	}

    public void OnDisable()
    {
        /*for (int i = 0; i < mFreakChachedChannelList.Count; i++)
        {
            mFreakChachedChannelList [i].profileImageCache = null;
        }*/

        /*mFreakChachedChannelList.Clear ();
        m_tableView.ReloadData ();*/
        ChatManager.Instance.newMessageUpdateCallback = null;
        ChatManager.Instance.onMessageTypingAllChatCallback = null;

        Resources.UnloadUnusedAssets ();
    }

    #region MESSAGE TYPING

    public delegate void OnIsMessageTyping(bool isTyping, string channelUrl, List<SendBird.User> userIds);
    public static event OnIsMessageTyping MessageTyping = new OnIsMessageTyping ((isTyping, channelUrl, userIds) => {} );

    public delegate void OnTestTyping(bool isTyping);
    public static event OnTestTyping Typing = new OnTestTyping ((isTyping) => {} );

    private string typingChannelUrl = string.Empty;
    private bool isChannelTyping = false;
    private List<SendBird.User> typingMembers = new List<SendBird.User>();

    void OnMessageTyping(bool isTyping, SendBird.GroupChannel groupChannel)
    {
//        Debug.Log ("OnMessageTyping " + isTyping);

        isChannelTyping = isTyping;
        typingChannelUrl = groupChannel.Url;
        typingMembers = new List<SendBird.User>(groupChannel.TypingMembers);

        CheckForTyping();
        /*for (int i = 0; i < mFreakChachedChannelList.Count; i++)
        {
            if (mFreakChachedChannelList[i].channelUrl == groupChannel.Url)
            {
                Debug.Log (mFreakChachedChannelList [i].channelName);

                ContactDetailsDisplay cell = m_tableView.GetCellAtRow (i) as ContactDetailsDisplay;
                cell.OnMessageTyping (isTyping, groupChannel.TypingMembers);
            }
        }*/
    }

    public void CheckForTyping ()
    {
        if (isChannelTyping == false && string.IsNullOrEmpty(typingChannelUrl))
            return;

        MessageTyping(isChannelTyping, typingChannelUrl, typingMembers);
    }

    #endregion


		/// <summary>
		/// Internal representation of our data. Note that the scroller will never see
		/// this, so it separates the data from the layout using MVC principles.
		/// </summary>
		private SmallList<Data> _data;

		/// <summary>
		/// This is our scroller we will be a delegate for
		/// </summary>
		public EnhancedScroller scroller;

		/// <summary>
		/// This will be the prefab of each cell in our scroller. Note that you can use more
		/// than one kind of cell, but this example just has the one type.
		/// </summary>
		
		/// <summary>
		/// Populates the data with a lot of records
		/// </summary>
		private void LoadLargeData()
		{
			// set up some simple data
			_data = new SmallList<Data>();
			for (var i = 0; i < 150; i++)
				_data.Add(new Data() { someText = "Cell Data Index " + i.ToString() });

			// tell the scroller to reload now that we have the data
			scroller.ReloadData();
		}


		#region EnhancedScroller Handlers

		/// <summary>
		/// This tells the scroller the number of cells that should have room allocated. This should be the length of your data array.
		/// </summary>
		/// <param name="scroller">The scroller that is requesting the data size</param>
		/// <returns>The number of cells</returns>
		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			// in this example, we just pass the number of our data elements
			//Debug.Log("mFreakChachedChannelList.Count--- >"+ mFreakChachedChannelList.Count);
			return mFreakChachedChannelList.Count;
			//return 100;
		}

		/// <summary>
		/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
		/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
		/// cell size will be the width.
		/// </summary>
		/// <param name="scroller">The scroller requesting the cell size</param>
		/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
		/// <returns>The size of the cell</returns>
		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
			//return (dataIndex % 2 == 0 ? 30f : 100f);
			return 180;
		}

		/// <summary>
		/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
		/// Some examples of this would be headers, footers, and other grouping cells.
		/// </summary>
		/// <param name="scroller">The scroller requesting the cell</param>
		/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
		/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
		/// <returns>The cell for the scroller to use</returns>
		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			// first, we get a cell from the scroller by passing a prefab.
			// if the scroller finds one it can recycle it will do so, otherwise
			// it will create a new cell.
			FreakAllChatsCellView cellView = scroller.GetCellView(cellViewPrefab) as FreakAllChatsCellView;

			// set the name of the game object to the cell's data index.
			// this is optional, but it helps up debug the objects in 
			// the scene hierarchy.
//			cellView.name = "Cell " + dataIndex.ToString();

			// in this example, we just pass the data to our cell's view which will update its UI
            cellView.SetData(mFreakChachedChannelList[dataIndex]);
//			cellView.InitFreakMessagingChannelNew(mFreakChachedChannelList[dataIndex]);

			if (IsOdd (cellIndex)) {
				SetGreyColor (cellView);
			} 
			else {
				SetWhiteColor (cellView);
			}

			//cell.InitFreakMessagingChannelNew(mFreakChachedChannelList[rowNumb]);


			// return the cell to the scroller
			return cellView;
		}

		#endregion

}

public class Allchats{
	string username;
	string number;
}