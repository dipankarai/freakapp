﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class GameHistroyUIController : MonoBehaviour, ITableViewDataSource  {


	public string failString;
	int currentCount;

	public GameHistoryServerConponent m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;
	public Text descriptionText; 

	public List<GameStatus> gameHistoryStatus = new List<GameStatus>();
	private string defaultText = "Game History not Available.";
    public ConnectingUI loading;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;

	void Start()
	{
        descriptionText.text = string.Empty;
	}

	void OnEnable()
	{
        loading.DisplayLoading();
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
        if(Freak.FreakServer.Instance.gameHistoryStatusList.Count > 0)
        {
            EnableTableView(Freak.FreakServer.Instance.gameHistoryStatusList);
        }
        else
        {
            //FindObjectOfType<History> ().GetGameHistoryAPICall(EnableTableView);
        }

        Invoke("InvokeGameHistroy", 1f);
	}

    void InvokeGameHistroy ()
    {
        Freak.FreakServer.Instance.GetGameHistoryAPICall(EnableTableView);
    }

	void EnableTableView(List<GameStatus> gameHistory)
	{
        if (gameHistory != null && gameHistory.Count > 0)
        {
            gameHistoryStatus.Clear ();
            for (int i = 0; i < gameHistory.Count; i++) 
            {
                gameHistoryStatus.Add (gameHistory [i]);
            }
        }

        if (gameHistoryStatus.Count > 0) {
            descriptionText.text = string.Empty;
        } 
        else {
            descriptionText.text = defaultText;
        }

        loading.HideLoading();
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}


	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController>().PopPanel ();
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return gameHistoryStatus.Count;
		//return 10;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		GameHistoryServerConponent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as GameHistoryServerConponent;
		if (cell == null) {
			cell = (GameHistoryServerConponent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
        AssignCellDetails (cell, row);
		cell.height = GetHeightOfRow(row);
		return cell;
	}

    void AssignCellDetails(GameHistoryServerConponent cell, int rowNumb)
    {
        cell.InitCellDetails(gameHistoryStatus[rowNumb]);
    }

	void AssignDetails(GameHistoryServerConponent cell, int rowNumb)
	{
//		gameHistory.GameStatus

//		GameStatus
		GameStatus gameStatus = gameHistoryStatus[rowNumb];

		cell.messageText.text = gameStatus.Message;
		cell.statusDetailText.text = gameStatus.Status;
		cell.opponentNameText.text = gameStatus.OppenentName;
		cell.gameNameText.text = gameStatus.GameName;

//		HistoryDetail cont = tempHostiry [rowNumb];
//		cell.messageText.text = cont.Name;
//		for(int p = 0 ; p < cont.Name ; p++)
//		{
//			if (p < 1) 
//			{
//				//string text1 = "Number : " + c.Phones [p].Number;
//				//instance.numberText.text = text1;
//				cell.messageText.text = cont.Phones[p].Number;
//				cell.phoneNumb = cont.Phones[p].Number;
//			}
//		}
//		for(int e = 0 ; e < cont.Emails.Count ; e++)
//		{
//			if (e < 1)
//			{
//				//string text2 = "Email  : " + c.Emails [e].Address; 
//				//instance.otherDetailText.text = text2;
//			}
//
//		}
//		if( cont.PhotoTexture != null )
//		{
//			//GUILayout.Box( new GUIContent(c.PhotoTexture) , GUILayout.Width(size.y), GUILayout.Height(size.y));
//			Texture2D tex = cont.PhotoTexture;
//			Rect rec = new Rect(0, 0, tex.width, tex.height);
//			Sprite spr = Sprite. Create(tex, rec,new Vector2(0,0),1);
//			cell.img.sprite = spr;
//		}
	}


	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}
		
		
	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}






