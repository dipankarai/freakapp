﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using SendBird;
//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
//containing this component as a cell in a TableView
using System.Text;
using System.IO;

public class GameHistoryServerConponent : TableViewCell {


    public Texture2D imgTex;

    public Text gameNameText, messageText;
    public Text statusDetailText;

    public Text userNameText;
    public Image userProfileImage;
    
    public Text opponentNameText;
    public Image opponentProfileImage;

	public Button shareButton;

	public Text m_rowNumberText;

    public Slider m_cellHeightSlider;
	public int rowNumber { get; set; }

//
//	GameName = data ["game_name"].ToString();
//	Message = data["message"].ToString();
//	Status = data["status"].ToString();
//	ProfilePicLink = data["opponent_profile_pic"].ToString();
//	OppenentUserId = data["opponent_user_id"].ToString();

	[System.Serializable]
	public class CellHeightChangedEvent : UnityEvent<int, float> { }
	public CellHeightChangedEvent onCellHeightChanged;

	public string rowContent { get; set; }
	public string iD{ get; set; }
    public Color red;
    public Color green;

	public Toggle selectionToggle;

    private GameStatus mGameStatus;

	void Start()
	{
		shareButton.onClick.AddListener (ShareScore);
	}

//	void Updates() {
//		m_rowNumberText.text = "Row " + rowNumber.ToString();
//	}

    public void InitCellDetails (GameStatus details)
    {
        mGameStatus = details;

        gameNameText.text = mGameStatus.GameName;
        statusDetailText.text = mGameStatus.Status + " Vs";
        if (mGameStatus.Status.Contains("win"))
        {
            statusDetailText.color = red;
            Color _effect_Color = red;
            _effect_Color.a = statusDetailText.GetComponent<Outline>().effectColor.a;
            statusDetailText.GetComponent<Outline>().effectColor = _effect_Color;
        }
        else
        {
            statusDetailText.color = green;
            Color _effect_Color = green;
            _effect_Color.a = statusDetailText.GetComponent<Outline>().effectColor.a;
            statusDetailText.GetComponent<Outline>().effectColor = _effect_Color;
        }

        userNameText.text = FreakAppManager.Instance.username;
        opponentNameText.text = mGameStatus.OppenentName;

//        mGameStatus.Message;
//        mGameStatus.OppenentUserId;
//        mGameStatus.ProfilePicLink;

        Invoke("ShowUserProfileImage", 0.2f);
        Invoke("ShowOpponentProfileImage", 0.2f);
    }

    void ShowUserProfileImage ()
    {
        Texture2D _texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(FreakAppManager.Instance.myUserProfile.avatar_url, Freak.FolderLocation.Profile, null);

        if (_texture != null)
        {
            userProfileImage.FreakSetSprite(_texture);
        }
        else
        {
            Invoke("ShowUserProfileImage", 0.5f);
        }
    }

    void ShowOpponentProfileImage ()
    {
        Texture2D _texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mGameStatus.ProfilePicLink, Freak.FolderLocation.Profile, null);

        if (_texture != null)
        {
            opponentProfileImage.FreakSetSprite(_texture);
        }
        else
        {
            Invoke("ShowOpponentProfileImage", 0.2f);
        }
    }

	public void SliderValueChanged(Slider slider) {
		float value = slider.value;
		Debug.Log ("Value --- >" + value);
		onCellHeightChanged.Invoke(rowNumber, value);
	}

	public float height {
		get { return m_cellHeightSlider.value; }
		set { m_cellHeightSlider.value = value; }
	}


	/// <summary>
	/// Call this to share the score in feedview
	/// </summary>
	/// <param name="message">Message.</param>
	public void ShareScore(){

		PostNewsFeedsAPICall (messageText.text, "1");
	}
	/// <summary>
	/// Post News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void PostNewsFeedsAPICall(string content, string feedtype)
	{
		FreakApi api = new FreakApi ("newsFeed.post", ServerResponseForPostNewsFeedsAPICall);
		api.param ("userId", FreakAppManager.Instance.userID);
		api.param ("feed_type", feedtype);
		api.param ("content", content);
		api.get (this);

	}

	void ServerResponseForPostNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];
			switch(responseCode)
			{
			case 001:
				/**
					*/
				break;
			case 207:
				/**
			 		*/
				break;
			default:
				/**
					 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
		}  
	}

}




