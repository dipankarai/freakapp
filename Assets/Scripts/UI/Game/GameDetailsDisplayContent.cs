﻿using UnityEngine;
using System.Collections;

public class GameDetailsDisplayContent : MonoBehaviour
{
    [SerializeField]
    private GameDetailsDisplayComponent[] m_GameDetailsDisplayComponent;

	// Use this for initialization
	void Start () {
	
	}
	
    public GameDetailsDisplayComponent GetItem ()
    {
        for (int i = 0; i < m_GameDetailsDisplayComponent.Length; i++)
        {
            if (m_GameDetailsDisplayComponent[i].gameObject.activeInHierarchy == false)
            {
                return m_GameDetailsDisplayComponent[i];
            }
        }

        return null;
    }
}
