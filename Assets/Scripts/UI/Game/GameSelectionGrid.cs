﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Freak.Chat;

public class GameSelectionGrid : MonoBehaviour
{
    public RawImage gameImage;
    public Text gameNameText;
    public Sprite defaultImage;

	public GameObject lockImage;

    private StoreGames mStoreGames;
	// Use this for initialization
	void Start () {
	
	}

    public void InitData (StoreGames data)
    {
        mStoreGames = data;
        gameNameText.text = mStoreGames.title;
        ShowImage();
    }

    void ShowImage ()
    {
        if (mStoreGames.gameTexture != null)
        {
            gameImage.texture = mStoreGames.gameTexture as Texture;
            gameImage.PreserveAspectToParent();
        }
        else
        {
            gameImage.texture = defaultImage.texture as Texture;
            gameImage.PreserveAspectToParent();
            Freak.DownloadManager.Instance.DownloadStoreGameTexture(mStoreGames.description_imagePath, mStoreGames);
            Invoke("ShowImage", 0.5f);
        }

		if (mStoreGames.purchase_status) 
		{
			lockImage.SetActive (false);
		} 
		else {
			lockImage.SetActive (true);
		}
    }

    public void OnClickSelectGameButton ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("OnClickSelectGameButton");
        #endif

        if (mStoreGames.purchase_status)
        {
            FreakAppManager.Instance.currentGameData.isGameSelected = true;
            FreakAppManager.Instance.currentGameData.selectedStoreGame = mStoreGames;

            FreakAppManager.Instance.PlayGame(mStoreGames.title, mStoreGames.inventory_id);
        }
        else
        {
            GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel(HomeScreenUIPanelController.PanelType.GameSelectionLockedPanel);
            go.GetComponent<GameSelectionDetails>().InitDetailsView(mStoreGames);
        }
    }
}
