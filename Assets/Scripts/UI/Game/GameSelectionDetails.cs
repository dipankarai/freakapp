﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Freak.Chat;

public class GameSelectionDetails : MonoBehaviour
{
    [Header("COMMON")]
    public RawImage gameRawImage;
    public Text gameTitleText, descriptionText;

    [Header("UNLOCKED")]
    public Text totalPlayedText;
    public Text totalScoreText;

    [Header("LOCKED")]
    public ScrollRect descriptionScrollRect;
    public ScrollRect screenShotsScrollRect;
    public Text gameSizeText;
    public Text gamePriceText;
    public Text totalPlayingText;

    private StoreGames mStoreGames;

    #region UNITY DEFAULT METHOD
	// Use this for initialization
	void Start () {
	
	}

    void OnDisable()
    {
        Destroy (this.gameObject);
    }
    #endregion

    #region INITIALIZE GameSelectionDetails

    public void InitDetailsView (StoreGames data)
    {
        mStoreGames = data;

        ShowImage();
        gameTitleText.text = mStoreGames.title;
        descriptionText.text = mStoreGames.inventory_description;

        if (mStoreGames.purchase_status)
        {
            ViewUnLockedDetails();
        }
        else
        {
            ViewLockedDetails();
        }
    }

    void ViewLockedDetails ()
    {
        gameSizeText.text = "Size : ";
        gamePriceText.text = "Price: " + mStoreGames.cost;
        totalPlayingText.text = "";
    }

    void ViewUnLockedDetails ()
    {
//        totalPlayedText.text
//        totalScoreText.text
    }

    void ShowImage ()
    {
        if (mStoreGames.gameTexture != null)
        {
            gameRawImage.texture = mStoreGames.gameTexture as Texture;
            gameRawImage.PreserveAspectToParent();
        }
        else
        {
            gameRawImage.texture = (Texture) Resources.Load<Texture>("FreaK_Log");
            gameRawImage.PreserveAspectToParent();
            Freak.DownloadManager.Instance.DownloadStoreGameTexture(mStoreGames.description_imagePath, mStoreGames);
            Invoke("ShowImage", 0.5f);
        }
    }

    #endregion

    #region UNITY UI Events
    public void OnClickBack()
    {
        FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
    }

    public void OnClickOpponent()
    {
        Debug.Log("OnClickOpponent");
        /*
        IDictionary dict = new IDictionary();
        dict.Add("user_id", FreakAppManager.Instance.userID.ToString());
        dict.Add("user_name", FreakAppManager.Instance.username);
        dict.Add("access_token", FreakAppManager.Instance.APIkey);*/

//        FreakAppManager.Instance.PlayGame(mStoreGames.title, mStoreGames.inventory_id);

//        FreakAppManager.Instance.currentSelectedStoreGame = mStoreGames;
//        FreakAppManager.Instance.GetHomeScreenPanel().PushPanel(HomeScreenUIPanelController.PanelType.GameSelectOpponent);
    }

    public void OnClickPlayWithFreak()
    {
        Debug.Log("OnClickPlayWithFreak");

        FreakAppManager.GameIntegrateData data = new FreakAppManager.GameIntegrateData();
        data.is_single_mode = true;
        FreakAppManager.Instance.currentGameData = data;
        FreakAppManager.Instance.currentGameData.isGameSelected = true;

        FreakAppManager.Instance.PlayGame(mStoreGames.title, mStoreGames.inventory_id);
    }

    public void OnClickDelete()
    {
        Debug.Log("OnClickDelete");
    }

    public void OnClickGiftForFriend()
    {

    }

    public void OnClickRefer()
    {

    }

    public void OnClickBuy()
    {
        
    }
    #endregion
}
