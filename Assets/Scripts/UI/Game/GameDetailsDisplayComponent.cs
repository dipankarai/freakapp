﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using System.Runtime.InteropServices;
using Freak;
using Freak.Chat;

public class GameDetailsDisplayComponent : MonoBehaviour {

//	[DllImport ("__Internal")] private static extern void LoadScene(int sceneID);

	public Text gameNameText;
	public int iD;
	public Image cellImage;
	public GameObject lockImage;
	public Button buttonObj;
    public string gameName;

    public Sprite defaultImage;
    private StoreGames mStoreGames;

    public void InitData (StoreGames data)
    {
        mStoreGames = data;

        gameName = mStoreGames.title;
        gameNameText.text = mStoreGames.title;
        iD = int.Parse(mStoreGames.inventory_id);

        lockImage.SetActive(mStoreGames.purchase_status == false);

        ShowImage();
    }

    void ShowImage ()
    {
        if (mStoreGames.gameTexture != null)
        {
            cellImage.FreakSetSprite(mStoreGames.gameTexture);
        }
        else
        {
            cellImage.sprite = defaultImage;
            Freak.DownloadManager.Instance.DownloadStoreGameTexture(mStoreGames.description_imagePath, mStoreGames);
            Invoke("ShowImage", 0.5f);
        }
    }

	public void OnClickGame()
	{
        if (mStoreGames.purchase_status)
        {
            FreakAppManager.Instance.currentGameData = new FreakAppManager.GameIntegrateData();
            FreakAppManager.Instance.currentGameData.isGameSelected = true;
            FreakAppManager.Instance.currentGameData.selectedStoreGame = mStoreGames;

            FreakAppManager.Instance.PlayGame(mStoreGames.title, mStoreGames.inventory_id);
        }
        else
        {
            GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel(HomeScreenUIPanelController.PanelType.GameSelectionLockedPanel);
            go.GetComponent<GameSelectionDetails>().InitDetailsView(mStoreGames);
        }
//		LoadGame();
	}

//    void LoadGame ()
//    {
//        Debug.Log("Load Game -------->> " + gameName);
//
//        switch (gameName)
//        {
//            case "SandCastle":
//                {
//                    
//                    #if UNITY_iOS || UNITY_iPHONE
//                    LoadScene (1);
//                    #endif
//                }
//                break;
//            case "Quiz":
//                {
//                    #if UNITY_iOS || UNITY_iPHONE
//                    LoadScene (2);
//                    #endif
//                }
//                break;
//            case "FreakQuiz":
//                {
//                    #if UNITY_EDITOR
//                    Debug.Log("Load Game FreakQuiz Editor....");
//                    #elif UNITY_ANDROID
//                    Debug.Log("Load Game ------->> ....");
//                    Freak.Game.FreakGameCommunicator.SwitchToGame(gameName, FreakAppManager.Instance.userID.ToString(), FreakAppManager.Instance.APIkey);
//                    #elif UNITY_iOS || UNITY_iPHONE
//                    Debug.Log("Load Game FreakQuiz iOS TODO: Not inplemented yet....");
//                    #endif
//                }
//                break;
//            default:
//                {
//                    //Load TIC TAC TOE
//                    FindObjectOfType<HomeScreenUIPanelController>().gameLoadController.gameObject.SetActive(true);
//                }
//                break;
//        }
//    }
}
