﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tacticsoft;

public class GameSelectionPanelController : MonoBehaviour, ITableViewDataSource
{
	public Text gameSelectionText;
    public Text noofTimePlayedText;
    public Text totalScoreText;
    public Text descriptionText;

    #region UNITY DEFAULT METHOD

    void OnEnable ()
    {
        Invoke("InitEnable", 0.5f);
////        FindObjectOfType<StoreInventory> ().GetStoreInventoryListAPICall (StoreInventory.StoreInventoryType.Games.GetHashCode().ToString(), EnableTableView);
//        FindObjectOfType<StoreInventory> ().GetStoreInventoryListAPICall("3", EnableTableView);
//
//        InitTableViewProps();
    }

    void InitEnable ()
    {
        if (FreakAppManager.Instance.inventoryGamesList.Count == 0)
        {
            FindObjectOfType<StoreInventory> ().GetStoreInventoryListAPICall("3", EnableTableView);
        }

        InitTableViewProps();
    }

    void Start ()
    {
        
    }

    void OnDisable()
    {
        Destroy (this.gameObject);
    }

    #endregion

    #region UNITY UI Events
	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController>().PopPanel ();
	}
    #endregion

    #region GameSelection List
    /*void DummyGameList ()
    {
        for (int i = 0; i < 3; i++)
        {
            StoreGames _storeGames = new StoreGames();
            _storeGames.type = "3";
            _storeGames.purchase_status = true;
            _storeGames.referralCount = 0;

            if (i==0)
            {
                _storeGames.title = "TicTacToe";
                _storeGames.inventory_description = "Game of Tic and Tac having Toe.";
                _storeGames.inventory_id = "5";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/f5824f731e6c882b771161624ee71824.png";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }
            else if (i==1)
            {
                _storeGames.title = "Quiz";
                _storeGames.inventory_description = "Game of Questions and Answers";
                _storeGames.inventory_id = "7";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/01f72d8b4cd43e9bed8cfcb55520e09f.png";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }
            else if (i==2)
            {
                _storeGames.title = "SandCastle";
                _storeGames.inventory_description = "Game is about building castles on Sand.";
                _storeGames.inventory_id = "6";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/aec2be3c5e49269714338d7c876732e6.jpg";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }

            FreakAppManager.Instance.inventoryGamesList.Add(_storeGames);

            Debug.Log("inventoryGamesList " + FreakAppManager.Instance.inventoryGamesList.Count);
        }

        m_tableView.ReloadData();
    }*/

    int TotalListCount { get { return FreakAppManager.Instance.inventoryGamesList.Count; } }

    public void EnableTableView(List<object> gamesList)
    {
        FreakAppManager.Instance.inventoryGamesList.Clear ();

        for (int i = 0; i < gamesList.Count; i++) 
        {
            Dictionary<string,object> data = (Dictionary<string,object>)gamesList [i];
            StoreGames storeGames = new StoreGames (data);
            FreakAppManager.Instance.inventoryGamesList.Add (storeGames);
            FreakAppManager.Instance.inventoryGamesList[i].purchase_status = FreakAppManager.IsInventoryPurchased(FreakAppManager.Instance.inventoryGamesList[i].inventory_id);
        }

        FreakPlayerPrefs.GetGameInventoryList = FreakAppManager.Instance.inventoryGamesList;

        m_tableView.ReloadData();
    }

    void AssignGameSelectionDetails (GameSelectionGridsPanel cell, int rowNumb)
    {
        int firstColoumnIndex = rowNumb * 3;
        for (int coloumn = 0; coloumn < 3; coloumn++)
        {
            if ((firstColoumnIndex + coloumn) < TotalListCount)
            {
                cell.InitGmeSelectionGrid(FreakAppManager.Instance.inventoryGamesList[(firstColoumnIndex + coloumn)], coloumn);
            }
            else {
                break;
            }
        }
    }
    #endregion

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    #region TableView Methods
    public TableView m_tableView;
    public GameSelectionGridsPanel m_cellPrefab;
    private Dictionary<int, float> m_customRowHeights;

    void InitTableViewProps ()
    {
        m_customRowHeights = new Dictionary<int, float>();
        m_tableView.dataSource = this;
    }

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (TotalListCount > 0) {
            float val = (float)(TotalListCount / 3f);
            int d = (int) Mathf.Ceil(val);
            Debug.Log ("Count --- >" + TotalListCount + " current count --- > "+ d);
            return d;
        } else {
            return 0;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return GetHeightOfRow(row);
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
    {
        GameSelectionGridsPanel cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as GameSelectionGridsPanel;

        if (cell == null) {
            cell = (GameSelectionGridsPanel)GameObject.Instantiate(m_cellPrefab);
            cell.name = "DynamicHeightCellInstance_" + row.ToString();
            cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
        }

        cell.rowNumber = row;
        AssignGameSelectionDetails(cell, row);
        cell.height = GetHeightOfRow(row);
        return cell;
    }

    private float GetHeightOfRow(int row) {
        if (m_customRowHeights.ContainsKey(row)) {
            return m_customRowHeights[row];
        } else {
            return m_cellPrefab.height;
        }
    }

    private void OnCellHeightChanged(int row, float newHeight) {
        if (GetHeightOfRow(row) == newHeight) {
            return;
        }

        m_customRowHeights[row] = newHeight;
        m_tableView.NotifyCellDimensionsChanged(row);
    }
    #endregion
}
