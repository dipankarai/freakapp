﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using Tacticsoft;

public class GameSelectionGridsPanel : TableViewCell
{
    [SerializeField]
    private GameSelectionGrid[] gameSelectionGrid;

    void Awake ()
    {
        for (int i = 0; i < gameSelectionGrid.Length; i++)
        {
            gameSelectionGrid[i].gameObject.SetActive(false);
        }
    }

	// Use this for initialization
	void Start () {
	
	}

    public void InitGmeSelectionGrid (StoreGames data, int index)
    {
        gameSelectionGrid[index].gameObject.SetActive(true);
        gameSelectionGrid[index].InitData(data);
    }
	
    #region TableViewCell
    public int rowNumber { get; set; }
    public Slider m_cellHeightSlider;

    [System.Serializable]
    public class CellHeightChangedEvent : UnityEvent<int, float> { }
    public CellHeightChangedEvent onCellHeightChanged;

    public void SliderValueChanged(Slider slider) {
        float value = slider.value;
        Debug.Log ("Value --- >" + value);
        onCellHeightChanged.Invoke(rowNumber, value);
    }

    public float height {
        get { return m_cellHeightSlider.value; }
        set { m_cellHeightSlider.value = value; }
    }

    public override string reuseIdentifier
    {
        get {
            return this.GetType ().Name;
        }
    }
    #endregion
}
