﻿using UnityEngine;
using System.Collections;

public class OptionsPanelUiController : MonoBehaviour {

	public RectTransform panelObj;
	public NewsFeedUIController newsFeedUIController;

	void OnEnable()
	{
        FreakAppManager.Instance.GetHomeScreenPanel().ChangeBackGround(FreakAppManager.StatusBarStyle.StatusBarStyleDefault);

		panelObj.SetAsLastSibling ();

		if (newsFeedUIController.gameObject.activeInHierarchy) 
		{
			newsFeedUIController.newsFeedComponent.DisableInputFieldOnSettingsPanel ();
		}
	}


	public void OnClickNotifications()
	{
		//FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.NotificationsPanel);
		if (newsFeedUIController.gameObject.activeSelf) 
		{
			OnClickClose ();
		} 
		else {
			FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
			FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.NewsFeedPanel);
		}
	}

	public void OnClickSettings()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.SettingsPanel);
	}

	public void OnClickStore()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.StorePanel);
	}

	public void OnClickAllContacts()
	{
		//FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.DisplayContactsPanel);
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
//		GameObject obj = FindObjectOfType<HomeScreenUIPanelController> ().GetPushedPanel(HomeScreenUIPanelController.PanelType.DisplayContactsPanel);
//		FreaksContactPanel freaks = obj.GetComponent<FreaksContactPanel> ();
//		freaks.DisableNextButton ();
//		freaks.isOnlyDisplay = true;
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.AllContactsPanelToInvite);
	}

	public void OnClickChannels()
	{

	}

	public void OnClickClose()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}

	void OnDisable()
	{
        FreakAppManager.Instance.GetHomeScreenPanel().SwitchStatusBarColor();

		if (newsFeedUIController.gameObject.activeInHierarchy) 
		{
			newsFeedUIController.newsFeedComponent.InitForNewsFeed ();
		}
	}

}
