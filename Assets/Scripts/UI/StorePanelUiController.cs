﻿using UnityEngine;
using System.Collections;

public class StorePanelUiController : MonoBehaviour {

	HomeScreenUIPanelController homeScreenUIPanelController;

	void Start()
	{
		homeScreenUIPanelController = FindObjectOfType<HomeScreenUIPanelController> ();
	}

	public void OnClickBack()
	{
		homeScreenUIPanelController.PopPanel ();
	}


	public void OnClickGames()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.StoreGamesPanel);
	}
		
	public void OnClickStickers()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.StoreStickersPanel);
	}

	public void OnClickAudioBuzz()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.StoreAudioBuzzPanel);
	}

	public void OnClickFreakCoin()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.StoreFreakCoinPanel);
	}


	public void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
