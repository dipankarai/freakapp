﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrivacySettingsController : MonoBehaviour {

	public Toggle readRecieptsToggle;
	public Text profileButtonText;
	public Text lastSeenButtonText;
	public Text statusButtonText;
	public Text onlineButtonText;
	private string Everyone = "Everyone";
	private string MyContacts = "MyContacts";
	private string Nobody = "Nobody";

	enum ProfilePrivacyStatus{
		Everyone,
		MyContacts,
		Nobody
	}
	ProfilePrivacyStatus profilePrivacyStatus;

	enum LastSeenPrivacyStatus{
		Everyone,
		MyContacts,
		Nobody
	}
	LastSeenPrivacyStatus lastSeenPrivacyStatus;

	enum StatusPrivacyStatus{
		Everyone,
		MyContacts,
		Nobody
	}
	StatusPrivacyStatus statusPrivacyStatus;

	enum OnlinePrivacyStatus{
		Everyone,
		MyContacts,
		Nobody
	}
	OnlinePrivacyStatus onlinePrivacyStatus;


	void Start()
	{
		readRecieptsToggle.isOn = SettingsManager.Instance.isPrivacyReadReciepts;
	}

	void OnEnable()
	{
		profilePrivacyStatus = (ProfilePrivacyStatus)SettingsManager.Instance.ProfilePicPrivacy;
		SetProfilePictureText ();

		lastSeenPrivacyStatus = (LastSeenPrivacyStatus)SettingsManager.Instance.LastSeenPrivacy;
		SetLastSeenText ();

		statusPrivacyStatus = (StatusPrivacyStatus)SettingsManager.Instance.StatusPrivacy;
		SetStatusText ();

		onlinePrivacyStatus = (OnlinePrivacyStatus)SettingsManager.Instance.OnlinePrivacy;
		SetOnlineText ();
	}


	public void OnClickReadReciepts(Toggle toggle)
	{
		if (toggle.isOn) 
		{
			Debug.Log ("OnClickReadReciepts --  Onnnnn>");
		} 
		else {
			Debug.Log ("OnClickReadReciepts --  OFFFFFF>");
		}
		SettingsManager.Instance.isPrivacyReadReciepts = toggle.isOn;
	}


	public void OnClickProfilePicture()
	{
		Debug.Log ("OnClickProfilePicture --  Onnnnn>");
		SettingsManager.Instance.ProfilePicPrivacy = (int)profilePrivacyStatus;
		SetProfilePictureText ();

	}
	void SetProfilePictureText()
	{
		if (profilePrivacyStatus == ProfilePrivacyStatus.Everyone)
		{
			profileButtonText.text = Everyone;
			profilePrivacyStatus = ProfilePrivacyStatus.MyContacts;
		} 
		else if(profilePrivacyStatus == ProfilePrivacyStatus.MyContacts) 
		{
			profileButtonText.text = MyContacts;
			profilePrivacyStatus = ProfilePrivacyStatus.Nobody;
		}
		else if(profilePrivacyStatus == ProfilePrivacyStatus.Nobody) 
		{
			profileButtonText.text = Nobody;
			profilePrivacyStatus = ProfilePrivacyStatus.Everyone;
		}
	}


	public void OnClickLastSeen()
	{
		Debug.Log ("OnClickLastSeen --  Onnnnn>");
		SettingsManager.Instance.LastSeenPrivacy = (int)lastSeenPrivacyStatus;
		SetLastSeenText ();

	}
	void SetLastSeenText()
	{
		if (lastSeenPrivacyStatus == LastSeenPrivacyStatus.Everyone)
		{
			lastSeenButtonText.text = Everyone;
			lastSeenPrivacyStatus = LastSeenPrivacyStatus.MyContacts;
		} 
		else if(lastSeenPrivacyStatus == LastSeenPrivacyStatus.MyContacts) 
		{
			lastSeenButtonText.text = MyContacts;
			lastSeenPrivacyStatus = LastSeenPrivacyStatus.Nobody;
		}
		else if(lastSeenPrivacyStatus == LastSeenPrivacyStatus.Nobody) 
		{
			lastSeenButtonText.text = Nobody;
			lastSeenPrivacyStatus = LastSeenPrivacyStatus.Everyone;
		}
	}


	public void OnClickStatus()
	{
		Debug.Log ("OnClickStatus --  Onnnnn>");
		SettingsManager.Instance.StatusPrivacy = (int)statusPrivacyStatus;
		SetStatusText();

	}
	void SetStatusText()
	{
		if (statusPrivacyStatus == StatusPrivacyStatus.Everyone)
		{
			statusButtonText.text = Everyone;
			statusPrivacyStatus = StatusPrivacyStatus.MyContacts;
		} 
		else if(statusPrivacyStatus == StatusPrivacyStatus.MyContacts) 
		{
			statusButtonText.text = MyContacts;
			statusPrivacyStatus = StatusPrivacyStatus.Nobody;
		}
		else if(statusPrivacyStatus == StatusPrivacyStatus.Nobody) 
		{
			statusButtonText.text = Nobody;
			statusPrivacyStatus = StatusPrivacyStatus.Everyone;
		}

	}


	public void OnClickOnline()
	{
		Debug.Log ("OnClickOnline --  Onnnnn>");
		SettingsManager.Instance.OnlinePrivacy = (int)onlinePrivacyStatus;
		SetOnlineText ();

	}
	void SetOnlineText()
	{
		if (onlinePrivacyStatus == OnlinePrivacyStatus.Everyone)
		{
			onlineButtonText.text = Everyone;
			onlinePrivacyStatus = OnlinePrivacyStatus.MyContacts;
		} 
		else if(onlinePrivacyStatus == OnlinePrivacyStatus.MyContacts) 
		{
			onlineButtonText.text = MyContacts;
			onlinePrivacyStatus = OnlinePrivacyStatus.Nobody;
		}
		else if(onlinePrivacyStatus == OnlinePrivacyStatus.Nobody) 
		{
			onlineButtonText.text = Nobody;
			onlinePrivacyStatus = OnlinePrivacyStatus.Everyone;
		}
	}


	public void OnClickMPin()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel(HomeScreenUIPanelController.PanelType.MPINPanel);
	}

	public void OnClickBack()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}

	void OnDisable()
	{
		Destroy (this.gameObject);
	}
}
