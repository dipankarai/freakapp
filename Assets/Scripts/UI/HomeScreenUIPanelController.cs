﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Freak.Chat;

public class HomeScreenUIPanelController : MonoBehaviour {

	public enum PanelType {

		HomeScreenPanel,
		AllChatsPanel,
		DisplayContactsPanel,
		NewsFeedPanel,
		StorePanel,
		StoreGamesPanel,
		StoreStickersPanel,
		StoreAudioBuzzPanel,
		StoreFreakCoinPanel,
		SettingsPanel,
		SetWallpaperPanel,
		PrivacySettingsPanel,
		PrivacyPolicyPanel,
		NotificationsPanel,
		MyTitlesPanel,
		MPINPanel,
		GroupProfilePanel,
		OtherUsersProfilePanel,
		ServerSynchedContactsDisplayPanel,
		GameHistoryPanel,
		GameSelectionPanel,
		UserProfilePanel,
		PostNewsFeedPanel,
		NewsFeedCommentPanel,
		PostImageNewsFeedPanel,
		SelectPlayerToReferPanel,
		BlockedUsersSettingPanel,
		ChatWindow,
		freakPanel,
		CreateGroupPanel,
		AllContactsPanelToInvite,
		LikesDislayPanel,
        GameSelectionUnLockedPanel,
        GameSelectionLockedPanel,
        GameSelectOpponent,
		PopUpPanel = 100,
		MPINDtailsEnterPanel = 101,
		DisplayContactsToSend = 102,
		OptionsPanel = 103,
		ChatImagePreviewPanel = 104,
		NewsFeedImagePreviewPanel = 105,
		CoverPicSelectionPanel = 106
	} 

	public UserHomeScreenController userHomeScreenController;
	public AllChatsUIController allChatsController;
	public NewsFeedUIController newsFeedUIController;

	//public game freaksContentController;
	public GroupDetailsUIController groupDetailsUIController;

	public UIController uiController;

    public PanelType currentpanelState;
	public PanelType previousPanelState
    {
        get
        {
            if (panelStack.Count > 1)
                return panelStack [panelStack.Count - 1];
            else
                return currentpanelState;
        }
    }
	public GameObject popUpPanel;
	public List<PanelType> panelStack = new List<PanelType>();
//	private bool isCompleted;
	public Transform PanelObj;
	public string failString;

//	private List<string> contactNumbers = new List<string> ();

	enum PanelMovementStatus{
		Default,
		Left,
		Right
	}
	PanelMovementStatus panelMovementStatus;

	public GameLoadController gameLoadController;
	public GameObject popupObject;

	public GameObject displayContactsToSend;
	public GameObject chatImagePreviewPanel;
	public GameObject newsFeedImagePreviewPanel;
	public GameObject optionsPanelObj;
	public GameObject coverPicSelectionPanel;

	public SwipeDirection direction = SwipeDirection.None;
	public enum SwipeDirection {Right, Left, Up, Down, None}
	private Touch initialTouch;
	public float errorRange;
    private GameObject chatWindowUI;
	public Transform panaelPosObj;
	private Vector3 _rightPosition;
	public bool isComplete;

	public bool isSwipeLeftRight;

	public Dictionary<PanelType,string>panelMovementData = new Dictionary<PanelType,string>();
	public GameObject popUpGameObject;

	void Awake()
	{
        #if UNITY_ANDROID
        Screen.fullScreen = false;
        #else
        FreakAppManager.HideIOSStatusBar(false);
        Screen.fullScreen = false;
        #endif

		PanelObj = this.gameObject.transform;
		ShowInitialPanel ();

        InitFreakChat ();

        FreakAppManager.Instance.homeScreenUIPanelController = this;

        Invoke("LoadChatWindowUI", 0.5f);

        #if UNITY_IOS || UNITY_IPHONE
        //        #if !UNITY_EDITOR
        ChangePanelStyle ();
        //        #endif
        #endif

        maxSwipeDistance = (float)Screen.width;
    }

    void ChangePanelStyle ()
    {
        RectTransform mainPanel = GetComponent<RectTransform>();

//        Vector2 _anchorPos = mainPanel.anchoredPosition;
//        _anchorPos.y = -20f;
//        mainPanel.anchoredPosition = _anchorPos;

        Vector2 _sizeDelta = mainPanel.sizeDelta;
        _sizeDelta.y = FreakAppManager.GetStatusBarHeight() * -1f;// -30f;
        mainPanel.sizeDelta = _sizeDelta;
    }

    public Color redColorStyle;
    public Color blackColorStyle;
    public UnityEngine.UI.Image mainBackPanel;
    public void ChangeBackGround (FreakAppManager.StatusBarStyle style)
    {
        if (mainBackPanel != null)
        {
            Debug.Log("style " + style);

//            if (style == FreakAppManager.StatusBarStyle.StatusBarStyleDefault)
//            {
//                mainBackPanel.color = blackColorStyle;
//                FreakAppManager.ChangeStatusBarStyle(FreakAppManager.StatusBarStyle.StatusBarStyleLightContent);
//            }
//            else if (style == FreakAppManager.StatusBarStyle.StatusBarStyleLightContent)
//            {
//                mainBackPanel.color = redColorStyle;
//                FreakAppManager.ChangeStatusBarStyle(FreakAppManager.StatusBarStyle.StatusBarStyleDefault);
//            }
        }
    }

    void LoadChatWindowUI ()
    {
        if (chatWindowUI == null)
        {
            chatWindowUI = Instantiate(UIController.Instance.assetsReferences.ChatWindow) as GameObject;
        }
    }

	void Start()
	{
		Input.multiTouchEnabled = true;
        ChatManager.Instance.StartRefreshCoroutine ();

        //INITIALIZING ALL CONTACTS FROM LOCAL......
        int count = FreakAppManager.Instance.GetAllContacts().Count;
        FreakAppManager.Instance.currentGameData = new FreakAppManager.GameIntegrateData();
	}

    /*void ConnectIsAvailable ()
    {
        InternetConnection.Instance.OnInternetConnectionBack = null;
		FreakAppManager.Instance.LoadServerContactList (null);
//        FindObjectOfType<GetContactListApiManager> ().GetContactListAPICall (FreakAppManager.Instance.mobileNumber, null);
    }*/

    void InitFreakChat()
    {
        Freak.Chat.ChatManager.Instance.UserId = FreakAppManager.Instance.userID.ToString();
		Freak.Chat.ChatManager.Instance.UserName = FreakAppManager.Instance.myUserProfile.name;

        if (InternetConnection.Check ()) 
		{
            if (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
            {
                Freak.Chat.ChatManager.Instance.LoginSendbirdChat ();
            }
        } 
		else {
            Debug.LogError ("No Internet Connection!");
//            Invoke("InitFreakChat", 0.1f);
        }
    }


	void ShowInitialPanel()
	{
		PushPanel (PanelType.HomeScreenPanel);
	}

		
	public void OnClickBackButton()
	{
		//Application.Quit();
		//Application.bac = true;
        FreakAppManager.GoToBackground();
	}


	public void OnClickCancel()
	{
		PopPanel ();
	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{ 
			PopPanel();
		}

//        SwipeDetector();
	}
		
	void LateUpdate()
	{
		DetectSwipe ();
	}

	GameObject GetGamebjectforEnum(PanelType _CurrentpanelState)
	{
		switch (_CurrentpanelState) 
		{
			case PanelType.HomeScreenPanel:
				return userHomeScreenController.gameObject;

			case PanelType.AllChatsPanel:
				return allChatsController.gameObject;

			case PanelType.DisplayContactsPanel:
				GameObject displayContactsPanel = Instantiate (UIController.Instance.assetsReferences.AllContactsPanel);
				SetParentofPanel (displayContactsPanel);
				return displayContactsPanel;

			case PanelType.NewsFeedPanel:
				return newsFeedUIController.gameObject;

			case PanelType.LikesDislayPanel:
				GameObject LikesDislayPanelController = Instantiate (UIController.Instance.assetsReferences.LikesDisplayPanel);
				SetParentofPanel (LikesDislayPanelController);
				return LikesDislayPanelController;

			case PanelType.freakPanel:
				GameObject freaksContentController = Instantiate (UIController.Instance.assetsReferences.FreaksPanel);
				SetParentofPanel (freaksContentController);
				return freaksContentController;

			case PanelType.CreateGroupPanel:
				return groupDetailsUIController.gameObject;

			case PanelType.StorePanel:
				GameObject storePanel = Instantiate (UIController.Instance.assetsReferences.StorePanel);
				SetParentofPanel (storePanel);
				return storePanel;

			case PanelType.SettingsPanel:
				GameObject settingsPanel = Instantiate (UIController.Instance.assetsReferences.SettingsPanel);
				SetParentofPanel (settingsPanel);
				return settingsPanel;

			case PanelType.SetWallpaperPanel:
				GameObject setWallPaperPanel = Instantiate (UIController.Instance.assetsReferences.SetWallpaperPanel);
				SetParentofPanel (setWallPaperPanel);
				return setWallPaperPanel;

			case PanelType.PrivacySettingsPanel:
				GameObject privacySettingsPanel = Instantiate (UIController.Instance.assetsReferences.PrivacyPanel);
				SetParentofPanel (privacySettingsPanel);
				return privacySettingsPanel;

			case PanelType.StoreGamesPanel: 
				GameObject storeGamesPanel = Instantiate (UIController.Instance.assetsReferences.StoreGamesPanel);
				SetParentofPanel (storeGamesPanel);
				return storeGamesPanel;

			case PanelType.StoreStickersPanel: 
				GameObject storeStickersPanel = Instantiate (UIController.Instance.assetsReferences.StoreStickersPanel);
				SetParentofPanel (storeStickersPanel);
				return storeStickersPanel;

			case PanelType.StoreFreakCoinPanel: 
				GameObject storeFreakCoinPanel = Instantiate (UIController.Instance.assetsReferences.StoreFreakCoinPanel);
				SetParentofPanel (storeFreakCoinPanel);
				return storeFreakCoinPanel;

			case PanelType.StoreAudioBuzzPanel: 
				GameObject storeAudioBuzzPanel = Instantiate (UIController.Instance.assetsReferences.StoreAudioBuzzPanel);
				SetParentofPanel (storeAudioBuzzPanel);
				return storeAudioBuzzPanel;

			case PanelType.NotificationsPanel: 
				GameObject notificationsPanel = Instantiate (UIController.Instance.assetsReferences.NotificationsPanel);
				SetParentofPanel (notificationsPanel);
				return notificationsPanel;

			case PanelType.MyTitlesPanel: 
				GameObject myTitlesPanel = Instantiate (UIController.Instance.assetsReferences.MyTitlesPanel);
				SetParentofPanel (myTitlesPanel);
				return myTitlesPanel;

			case PanelType.MPINPanel: 
				GameObject mPINPanel = Instantiate (UIController.Instance.assetsReferences.MPINPanel);
				SetParentofPanel (mPINPanel);
				return mPINPanel;

			case PanelType.MPINDtailsEnterPanel: 
				GameObject mPINDtailsEnterPanel = Instantiate (UIController.Instance.assetsReferences.MPINDetailsEnterPanel);
				SetParentofPanel (mPINDtailsEnterPanel);
				return mPINDtailsEnterPanel;

			case PanelType.GroupProfilePanel: 
				GameObject GroupProfilePanel = Instantiate (UIController.Instance.assetsReferences.GroupProfilePanel);
				SetParentofPanel (GroupProfilePanel);
                return GroupProfilePanel;

			case PanelType.ServerSynchedContactsDisplayPanel: 
                GameObject serverSynchedContactsDisplayPanel = Instantiate (UIController.Instance.assetsReferences.AllContactsPanel);
				SetParentofPanel (serverSynchedContactsDisplayPanel);
				return serverSynchedContactsDisplayPanel;

			case PanelType.GameHistoryPanel: 
				GameObject gameHistoryPanel = Instantiate (UIController.Instance.assetsReferences.GameHistoryPanel);
				SetParentofPanel (gameHistoryPanel);
				return gameHistoryPanel;

			case PanelType.GameSelectionPanel: 
				GameObject gameSelectionPanel = Instantiate (UIController.Instance.assetsReferences.GameSelectionPanel);
				SetParentofPanel (gameSelectionPanel);
				return gameSelectionPanel;

            case PanelType.GameSelectionUnLockedPanel:
                {
                    GameObject gameSelectionUnLockedPanel = Instantiate (UIController.Instance.assetsReferences.GameSelectionUnLockedPanel);
                    SetParentofPanel (gameSelectionUnLockedPanel);
                    return gameSelectionUnLockedPanel;
                }

            case PanelType.GameSelectionLockedPanel:
                {
                    GameObject gameSelectionLockedPanel = Instantiate (UIController.Instance.assetsReferences.GameSelectionLockedPanel);
                    SetParentofPanel (gameSelectionLockedPanel);
                    return gameSelectionLockedPanel;
                }

            case PanelType.GameSelectOpponent:
                {
                    GameObject gameSelectionOpponentPanel = Instantiate (UIController.Instance.assetsReferences.GameSelectionOpponentPanel);
                    SetParentofPanel (gameSelectionOpponentPanel);
                    return gameSelectionOpponentPanel;
                }

			case PanelType.UserProfilePanel: 
				GameObject userProfilePanel = Instantiate (UIController.Instance.assetsReferences.UserProfilePanel);
				SetParentofPanel (userProfilePanel);
				return userProfilePanel;

			case PanelType.AllContactsPanelToInvite: 
				GameObject allContactsPanel = Instantiate (UIController.Instance.assetsReferences.AllContactsPanelToInvite);
				SetParentofPanel (allContactsPanel);
				return allContactsPanel;

			case PanelType.PostNewsFeedPanel: 
				GameObject postNewsFeedPanel = Instantiate (UIController.Instance.assetsReferences.PostNewsFeedPanel);
				SetParentofPanel (postNewsFeedPanel);
				return postNewsFeedPanel;

			case PanelType.NewsFeedCommentPanel: 
				GameObject newsFeedCommentPanel = Instantiate (UIController.Instance.assetsReferences.NewsFeedCommentPanel);
				SetParentofPanel (newsFeedCommentPanel);
				return newsFeedCommentPanel;

			case PanelType.OtherUsersProfilePanel: 
				GameObject otherUsersProfilePanel = Instantiate (UIController.Instance.assetsReferences.ProfilePanel);
				SetParentofPanel (otherUsersProfilePanel);
				return otherUsersProfilePanel;

			case PanelType.PostImageNewsFeedPanel: 
				GameObject postImageNewsFeedPanel = Instantiate (UIController.Instance.assetsReferences.PostImageNewsFeedPanel);
				SetParentofPanel (postImageNewsFeedPanel);
				return postImageNewsFeedPanel;

			case PanelType.SelectPlayerToReferPanel: 
				GameObject selectPlayerToReferPanel = Instantiate (UIController.Instance.assetsReferences.SelectPlayerPanel);
				SetParentofPanel (selectPlayerToReferPanel);
				return selectPlayerToReferPanel;

			case PanelType.PrivacyPolicyPanel: 	
				GameObject privacyPolicyPanel = Instantiate (UIController.Instance.assetsReferences.PrivacyPolicyPanel);
				SetParentofPanel (privacyPolicyPanel);
				return privacyPolicyPanel;

			case PanelType.BlockedUsersSettingPanel: 	
				GameObject blockedUsersSettingPanel = Instantiate (UIController.Instance.assetsReferences.BlockedUsersSettingPanel);
				SetParentofPanel (blockedUsersSettingPanel);
				return blockedUsersSettingPanel;

            case PanelType.ChatWindow: 	
                GameObject chatWindow = ChatWindowUIPrefab();
				SetParentofPanel (chatWindow);
				return chatWindow;

			case PanelType.PopUpPanel: 	
				return popupObject;

			case PanelType.DisplayContactsToSend: 	
				return displayContactsToSend;
				
			case PanelType.ChatImagePreviewPanel: 	
				return chatImagePreviewPanel;

			case PanelType.NewsFeedImagePreviewPanel:
				return newsFeedImagePreviewPanel;

			case PanelType.OptionsPanel: 
				return optionsPanelObj;

			case PanelType.CoverPicSelectionPanel: 
				return coverPicSelectionPanel;

			default:
				return null;
		}
	}

    GameObject ChatWindowUIPrefab ()
    {
        if (chatWindowUI == null)
        {
            chatWindowUI = Instantiate(UIController.Instance.assetsReferences.ChatWindow);
        }

        return (GameObject)Instantiate(chatWindowUI);
    }

	void SetParentofPanel(GameObject obj)
	{
		obj.transform.SetParent (this.gameObject.transform, false);
	}


	void DisplayPanel(PanelType _CurrentpanelState, bool isFade = false)
	{
		currentpanelState = _CurrentpanelState;
		if((int)_CurrentpanelState < 100)
		{
			currentpanelState = _CurrentpanelState;
			EnablePanel (GetGamebjectforEnum (currentpanelState), isFade);
		}
		else{
			ShowPopup(GetGamebjectforEnum(_CurrentpanelState));
		}
	}

	public void OnClickOpenChat ()
	{
        GameObject go = ChatWindowUIPrefab();
		go.transform.SetParent(UIController.Instance.parent, false);
		go.SetActive(true);
	}


	void HidePanel(PanelType currentPanel)
	{
		//HidePopup(GetGamebjectforEnum (currentPanel));
		HidePopup(popUpGameObject);
	}


	public void PushPanel(PanelType currentpanelState, bool isFade = false)
	{
		Debug.Log ("IsComplete Status --- >" + isComplete);
		if (!isComplete) 
		{
			isComplete = true;
			panelStack.Add (currentpanelState);
			DisplayPanel (currentpanelState, isFade);
		}
	}

    public GameObject GetCurrentObject
    {
        get { return currentObj; }
    }

	public GameObject GetPushedPanel(PanelType _currentpanelState)
	{
		panelStack.Add (_currentpanelState);
		currentpanelState = _currentpanelState;
		GameObject gObj = GetGamebjectforEnum (_currentpanelState);
		EnablePanel (gObj);
		return gObj;
	}

    public GameObject GetReplacedPanel(PanelType _currentpanelState)
    {
        panelStack.RemoveAt (panelStack.Count - 1);
        currentpanelState = _currentpanelState;
        panelStack.Add (currentpanelState);

        GameObject gObj = GetGamebjectforEnum (_currentpanelState);
        EnablePanel (gObj);
        return gObj;
    }

	public void ShowPanelOnFocus(PanelType currentpanelState)
	{
		panelStack.Add (currentpanelState);
		if((int)currentpanelState < 100)
		{
			currentpanelState = currentpanelState;
//			isCompleted = true;
			foreach (Transform child in PanelObj) 
			{
				if(child.gameObject.activeSelf)
				{
					previousObj = child.gameObject;
					child.gameObject.SetActive(false);
				}
			}

			GameObject obj = GetGamebjectforEnum(currentpanelState);
			obj.transform.localPosition = new Vector3(0,obj.transform.localPosition.y, obj.transform.localPosition.z);
			CompleteAnimation();
		}
	}


	public void ReplacePanel(PanelType currentpanelState, bool isFade = false)
	{
		panelStack.RemoveAt (panelStack.Count - 1);
		panelStack.Add (currentpanelState);
		DisplayPanel (currentpanelState, isFade);
	}


    public GameObject GetPopPanelObject()
	{
        panelStack.RemoveAt(panelStack.Count - 1);
		PanelType toppanel1 = panelStack[panelStack.Count-1];
        Debug.Log("GetPopPanelObject " + toppanel1);
        GameObject gObj = GetGamebjectforEnum (toppanel1);
        EnablePanel (gObj);
        currentpanelState = toppanel1;
        return gObj;
	}

    public void PopPanel(bool isFade = false)
    {
		
		if (!isComplete) {
			isComplete = true;
			PanelType toppanel1 = panelStack [panelStack.Count - 1];
			Debug.Log("1111111111111111111 IsCompleteStatus --- > " + toppanel1);
			if (panelStack.Count > 1) {
				if ((int)toppanel1 < 100) {
					panelStack.RemoveAt (panelStack.Count - 1);
					toppanel1 = panelStack [panelStack.Count - 1];
					DisplayPanel (toppanel1, isFade);
				}
            // this is for popup panel..
            else {
					Debug.Log(" Popupsssssssssssssssssssssssssssssss --- > " + toppanel1);
					panelStack.RemoveAt (panelStack.Count - 1);
					HidePanel (toppanel1);
					toppanel1 = panelStack [panelStack.Count - 1];
					currentpanelState = toppanel1;
				}
			} else {
				OnClickBackButton ();
				isComplete = false;
			}
		}
    }


	private GameObject previousObj;
    private GameObject currentObj;
	enum SlideStatus{
		Default,
		Left,
		Right
	}
	SlideStatus slideStatusType;

	void EnablePanel(GameObject enableObj, bool isfade = false)
	{
		currentObj = enableObj;
		_rightPosition = panaelPosObj.rectTransform().localPosition;

		foreach (Transform child in PanelObj) 
		{
			if (child.gameObject.activeSelf) 
			{
				previousObj = child.gameObject;
				if (!isfade)
				{
//				if (!panelMovementData.ContainsKey (currentpanelState))
//				{ 
//					if (enableObj.transform.rectTransform ().localPosition.x > 200)
//					{
//						_rightPosition.x -= panaelPosObj.transform.rectTransform ().rect.width;
//						slideStatusType = SlideStatus.Left;
//						Debug.Log ("MOVE LEFT");
//						//LeanTween.move (previousObj.transform.rectTransform (), new Vector3 (_rightPosition.x, _rightPosition.y, _rightPosition.z), 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete (CompleteAnimation);
//						panelMovementData.Add (currentpanelState, "1");
//						
//					} else {
//						_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
//						slideStatusType = SlideStatus.Right;
//						Debug.Log ("MOVE RIGHT");
//						panelMovementData.Add (currentpanelState, "2");
//
//					}
//				} 
//				else {
//					if (panelMovementData [currentpanelState].Equals ("1")) 
//					{
//						_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
//						slideStatusType = SlideStatus.Right;
//						Debug.Log ("1111111111111111111111111111111111111MOVE RIGHT");
//						panelMovementData.Remove (currentpanelState);
//						panelMovementData.Add (currentpanelState, "1");
//					} 
//					else {
//						_rightPosition.x -= panaelPosObj.transform.rectTransform ().rect.width;
//						slideStatusType = SlideStatus.Left;
//						Debug.Log ("2222222222222222222222222222222222222MOVE LEFT");
//						panelMovementData.Remove (currentpanelState);
//						panelMovementData.Add (currentpanelState, "2");
//					}
//				}

//				if (panelMovementStatus == PanelMovementStatus.Default || panelMovementStatus == PanelMovementStatus.Right)
//				{
//					_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
//					slideStatusType = SlideStatus.Right;
//					panelMovementStatus = PanelMovementStatus.Left;
//					Debug.Log("MOVE RIGHT");
//				}
//				else if (panelMovementStatus == PanelMovementStatus.Left)
//				{
//					_rightPosition.x -= panaelPosObj.transform.rectTransform ().rect.width;
//					slideStatusType = SlideStatus.Left;
//					panelMovementStatus = PanelMovementStatus.Right;
//					Debug.Log("MOVE Left");
//				}

//				LeanTween.move (previousObj.transform.rectTransform (), new Vector3 (_rightPosition.x, _rightPosition.y, _rightPosition.z), 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete (CompleteAnimation);
					if (enableObj.GetComponent<CanvasGroup> () == null) {
						enableObj.AddComponent<CanvasGroup> ();
						enableObj.GetComponent<CanvasGroup> ().alpha = 0;
						enableObj.GetComponent<CanvasGroup> ().blocksRaycasts = false;
					} else {
						enableObj.GetComponent<CanvasGroup> ().blocksRaycasts = false;
					}
					StartCoroutine (FadeOut (previousObj.GetComponent<CanvasGroup> ()));
				} 
				else {
					if (enableObj.transform.rectTransform ().localPosition.x > 200)
					{
						_rightPosition.x -= panaelPosObj.transform.rectTransform ().rect.width;
						slideStatusType = SlideStatus.Left;
						Debug.Log ("MOVE LEFT");
						//LeanTween.move (previousObj.transform.rectTransform (), new Vector3 (_rightPosition.x, _rightPosition.y, _rightPosition.z), 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete (CompleteAnimation);
						//panelMovementData.Add (currentpanelState, "1");
						
					} else {
						_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
						slideStatusType = SlideStatus.Right;
						Debug.Log ("MOVE RIGHT");
						//panelMovementData.Add (currentpanelState, "2");

					}

					LeanTween.moveX (previousObj.transform.rectTransform (), _rightPosition.x, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete (CompleteAnimation);

				}
			}
		}
			
		//StartCoroutine (SlidePanel());
		ResetPosition (currentObj, isfade);
	}



	IEnumerator FadeOut(CanvasGroup canvasGroup)
	{
		float time = 0.3f;
		while(canvasGroup.alpha > 0)
		{
			canvasGroup.alpha -= Time.deltaTime / time;
			//Debug.Log ("Fade "+canvasGroup.alpha);
			yield return null;
		}
		//Debug.Log ("After FadeOut --- "+canvasGroup.alpha);
		CompleteAnimation ();
	}
		

	IEnumerator Fadein(CanvasGroup canvasGroup)
	{
		float time = 0.3f;
		while(canvasGroup.alpha < 1)
		{
			canvasGroup.alpha += Time.deltaTime / time;
			//Debug.Log ("Fade In "+canvasGroup.alpha);
			yield return null;
		}
		//Debug.Log ("After FadeIn --- "+canvasGroup.alpha);
		ResetSlide ();
	}


	IEnumerator SlidePanel()
	{
		yield return new WaitForEndOfFrame();
		ResetPosition (currentObj);
	}


	void ResetPosition(GameObject enableObj, bool isfade = false)
	{
		_rightPosition = panaelPosObj.rectTransform ().localPosition;
		enableObj.transform.rectTransform ().localPosition = _rightPosition;
		enableObj.SetActive (true);
		if (isfade) 
		{
			if (slideStatusType == SlideStatus.Left) {
				_rightPosition.x += enableObj.transform.rectTransform ().rect.width;
			} else {
				_rightPosition.x -= enableObj.transform.rectTransform ().rect.width;
			}
			enableObj.transform.rectTransform ().localPosition = _rightPosition;
			//enableObj.transform.localPosition = new Vector3 ( panaelPosObj.rectTransform().localPosition.x + 1200, enableObj.transform.localPosition.y, enableObj.transform.localPosition.z);
			if (enableObj.name.Equals ("UserHomePanel")) {
				//panelStack.Clear ();
			}
			if (enableObj.GetComponent<CanvasGroup> () != null) {
				enableObj.GetComponent<CanvasGroup> ().alpha = 1;
				//enableObj.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			}
			LeanTween.moveX (enableObj.transform.rectTransform (), panaelPosObj.rectTransform ().localPosition.x, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete (ResetSlide);
		} 
		else {
			if (enableObj.GetComponent<CanvasGroup> () != null) {
				enableObj.GetComponent<CanvasGroup> ().alpha = 0;
				enableObj.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			}
			else {
				enableObj.AddComponent<CanvasGroup> ();
				enableObj.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			}
			StartCoroutine (Fadein (enableObj.GetComponent<CanvasGroup> ()));
		}
	}


	void CompleteAnimation()
	{
     
        #if UNITY_IOS || UNITY_IPHONE
        SwitchStatusBarColor();
        #endif

		DisableObj ();
	}

    public void SwitchStatusBarColor ()
    {
        #if UNITY_IOS || UNITY_IPHONE

        Debug.Log("currentpanelState " + currentpanelState);

        if (currentpanelState == PanelType.HomeScreenPanel || currentpanelState == PanelType.OptionsPanel)
        {
            FreakAppManager.Instance.GetHomeScreenPanel().ChangeBackGround(FreakAppManager.StatusBarStyle.StatusBarStyleDefault);
        }
        else
        {
            FreakAppManager.Instance.GetHomeScreenPanel().ChangeBackGround(FreakAppManager.StatusBarStyle.StatusBarStyleLightContent);
        }
        #endif
    }

	void DisableObj()
	{
		if(previousObj != null)
			previousObj.SetActive (false);
	}


	void ResetSlide()
	{
		//currentObj.transform.rectTransform().localPosition = panaelPosObj.rectTransform().localPosition;
		isComplete = false;
	}
		

	void ShowPopup(GameObject enableObj)
	{
		isComplete = false;
		popUpGameObject = enableObj;
		enableObj.SetActive (true);
		//enableObj.transform.localScale = Vector3.zero;
		//LeanTween.scale (enableObj.gameObject, Vector3.one, 0.3f).setEase (LeanTweenType.easeOutBack);

	}

	void HidePopup(GameObject enableObj){
		isComplete = false;

		enableObj.SetActive (false);
		//Destroy (enableObj);
	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}
		

	#region Touch
	public enum Swipe {
		None,
		Up,
		Down,
		Left,
		Right,
		UpLeft,
		UpRight,
		DownLeft,
		DownRight
	};

    [Header("SWIPE PROPERTIES")]
	// Min length to detect the Swipe
    public bool swiping;
    public float minSwipeDistance = 120;
    public float maxSwipeDistance = 800f;
    public static Swipe SwipeDirections;

    float distance;
//    private Vector2 _firstPressPos;
//    private Vector2 _secondPressPos;
    private Vector2 _currentSwipe;
    private Vector2 _firstClickPos;
    private Vector2 _secondClickPos;
    private GameObject slidingObject;

//	public float ReturnForce = 10f;
//	Vector3 firtClickPointerPos;
//	Vector3 secondClickPointerPos;

	public void DetectSwipe()
	{
        if (Input.GetMouseButtonDown (0)) {
            _firstClickPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
//            firtClickPointerPos = Camera.main.ScreenToWorldPoint(_firstClickPos);
        } else {
            SwipeDirections = Swipe.None;
            //Debug.Log ("None");
        }

        if (Input.GetMouseButtonUp(0))
        {
            _secondClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _currentSwipe = new Vector3(_secondClickPos.x - _firstClickPos.x, _secondClickPos.y - _firstClickPos.y);
//            secondClickPointerPos = Camera.main.ScreenToWorldPoint(_secondClickPos);
            // Make sure it was a legit swipe, not a tap
            distance = Vector2.Distance(_firstClickPos, _secondClickPos);
            
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SSSSWWWWWWIIIIIIPPPPPEEEEEEE---- >"+ distance + "_currentSwipe.x ---> "+ _currentSwipe.x + "_currentSwipe.x ---> "+  _currentSwipe.y);
            //Debug.Log ("SSSSWWWWWWIIIIIIPPPPPEEEEEEE---- >"+ distance + "_currentSwipe.x ---> "+ _currentSwipe.x + "_currentSwipe.x ---> "+  _currentSwipe.y);
            #endif

            if (distance < minSwipeDistance || distance > maxSwipeDistance)
            {
                SwipeDirections = Swipe.None;
                return;
            }
            
            if ((currentpanelState == PanelType.HomeScreenPanel || currentpanelState == PanelType.AllChatsPanel || currentpanelState == PanelType.NewsFeedPanel) && !isComplete && FreakAppManager.Instance.isLoaderOn == false)
            {
                _currentSwipe.Normalize();
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Swipe---- >"+ distance + "_currentSwipe.x ---> "+ _currentSwipe.x + "_currentSwipe.x ---> "+  _currentSwipe.y);
                #endif
                //Swipe directional check
                // Swipe left
                if (_currentSwipe.x < -0.5 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.3f)
                {
                    SwipeDirections = Swipe.Left;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("Left touch");
                    #endif
                    OnDrag(false);
                }
                // Swipe right
                else if (_currentSwipe.x > 0.5 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.3f)
                {
                    SwipeDirections = Swipe.Right;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("right touch");
                    #endif
                    OnDrag(true);
                }     // Swipe up left
            }
        }
	}	

	public void OnDrag(bool isRight)
	{
//		if (!isSwipeLeftRight) 
//			isComplete = true;
			isSwipeLeftRight = true;
        #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("OnDrag isRight " + isRight);
            Debug.Log ("currentpanelState " + currentpanelState);
        #endif
			if (isRight) {
				if (currentpanelState == PanelType.HomeScreenPanel) {
				
                /*slidingObject = GetSwipePushedPanel (PanelType.AllChatsPanel);
					allChatsController.SlideScreen (AllChatsUIController.SlideStatus.Right, OnCompleteSwipe);
					userHomeScreenController.SlideScreen (UserHomeScreenController.SlideStatus.Right, null);*/
					panelMovementStatus = PanelMovementStatus.Right;
                	PushPanel(PanelType.AllChatsPanel,true);

				} else if (currentpanelState == PanelType.NewsFeedPanel) {
                
                /*slidingObject = GetSwipeReplacedPanel (PanelType.HomeScreenPanel);
					newsFeedUIController.SlideScreen (NewsFeedUIController.SlideStatus.Right, null);
					userHomeScreenController.slideStatus = UserHomeScreenController.SlideStatus.Left;
					userHomeScreenController.SlideScreen (UserHomeScreenController.SlideStatus.Straight, OnCompleteSwipe);*/
					ReplacePanel(PanelType.HomeScreenPanel,true);
//					PopPanel(true);
					
				}
			} else {
				if (currentpanelState == PanelType.HomeScreenPanel) {
                /*slidingObject = GetSwipePushedPanel (PanelType.NewsFeedPanel);
					userHomeScreenController.SlideScreen (UserHomeScreenController.SlideStatus.Left, null);
					newsFeedUIController.SlideScreen (NewsFeedUIController.SlideStatus.Left, OnCompleteSwipe);*/
					_rightPosition = panaelPosObj.rectTransform().localPosition;
					_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
					newsFeedUIController.transform.rectTransform().localPosition = _rightPosition;
					panelMovementStatus = PanelMovementStatus.Left;
               		PushPanel(PanelType.NewsFeedPanel, true);
					
				} else if (currentpanelState == PanelType.AllChatsPanel) {
                /*slidingObject = GetSwipeReplacedPanel (PanelType.HomeScreenPanel);
					allChatsController.SlideScreen (AllChatsUIController.SlideStatus.Left, null);
					userHomeScreenController.slideStatus = UserHomeScreenController.SlideStatus.Right;
					userHomeScreenController.SlideScreen (UserHomeScreenController.SlideStatus.Straight, OnCompleteSwipe);*/
					_rightPosition = panaelPosObj.rectTransform().localPosition;
					_rightPosition.x += panaelPosObj.transform.rectTransform ().rect.width;
					userHomeScreenController.transform.rectTransform().localPosition = _rightPosition;
					panelMovementStatus = PanelMovementStatus.Left;
					ReplacePanel(PanelType.HomeScreenPanel,true);
//					PopPanel(true);
					
				}
			}
			isComplete = false;
		//}
	}

    void OnCompleteSwipe ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("OnCompleteSwipe");
        #endif
		isSwipeLeftRight = false;
        previousObj = currentObj;
        currentObj = slidingObject;
//      previousObj.SetActive(false);

    }

    public GameObject GetSwipePushedPanel(PanelType _currentpanelState)
    {
        panelStack.Add (_currentpanelState);
        currentpanelState = _currentpanelState;
        GameObject gObj = GetGamebjectforEnum (_currentpanelState);
        gObj.SetActive(true);
//        EnableSwipePanel (gObj, true);
        return gObj;
    }

    public GameObject GetSwipeReplacedPanel(PanelType _currentpanelState)
    {
        panelStack.RemoveAt (panelStack.Count - 1);
        currentpanelState = _currentpanelState;

        GameObject gObj = GetGamebjectforEnum (_currentpanelState);
        gObj.SetActive(true);
//        EnablePanel (gObj, true);
        return gObj;
    }

    void EnableSwipePanel(GameObject enableObj, bool isAnimate)
    {
        foreach (Transform child in PanelObj) 
        {
            if(child.gameObject.activeSelf)
            {
                previousObj = child.gameObject;
                child.gameObject.SetActive(false);
            }
        }

        currentObj = enableObj;
        ResetSwipePosition (enableObj);
    }


    void ResetSwipePosition(GameObject enableObj)
    {
        enableObj.SetActive (true);
    }

	#endregion

    #region SwipeDetector
    private const int mMessageWidth  = 200;
    private const int mMessageHeight = 64;

    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);

    private readonly string [] mMessage = {
        "",
        "Swipe Left",
        "Swipe Right",
        "Swipe Top",
        "Swipe Bottom"
    };

    private int mMessageIndex = 0;

    // The angle range for detecting swipe
    private const float mAngleRange = 30;

    // To recognize as swipe user should at lease swipe for this many pixels
    private const float mMinSwipeDist = 50.0f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    private const float mMinVelocity  = 2000.0f;
    private Vector2 mStartPosition;
    private float mSwipeStartTime;
    void SwipeDetector ()
    {
        if (Input.GetMouseButtonUp(0)) {
            float deltaTime = Time.time - mSwipeStartTime;

            Vector2 endPosition  = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector2 swipeVector = endPosition - mStartPosition;

            float velocity = swipeVector.magnitude/deltaTime;

            if (velocity > mMinVelocity && swipeVector.magnitude > mMinSwipeDist) {
                // if the swipe has enough velocity and enough distance

                swipeVector.Normalize();

                float angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange) {
                    OnSwipeRight();
                } else if ((180.0f - angleOfSwipe) < mAngleRange) {
                    OnSwipeLeft();
                } else {
                    // Detect top and bottom swipe
                    angleOfSwipe = Vector2.Dot(swipeVector, mYAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;
                    if (angleOfSwipe < mAngleRange) {
                        OnSwipeTop();
                    } else if ((180.0f - angleOfSwipe) < mAngleRange) {
                        OnSwipeBottom();
                    } else {
                        mMessageIndex = 0;
                    }
                }
            }
        }
    }

    void OnGUI() {
        // Display the appropriate message
        GUI.Label(new Rect((Screen.width-mMessageWidth)/2,
            (Screen.height-mMessageHeight)/2,
            mMessageWidth, mMessageHeight),
            mMessage[mMessageIndex]);
    }

    private void OnSwipeLeft() {
        mMessageIndex = 1;
        Debug.Log("OnSwipeLeft");
    }

    private void OnSwipeRight() {
        mMessageIndex = 2;
        Debug.Log("OnSwipeRight");
    }

    private void OnSwipeTop() {
        mMessageIndex = 3;
        Debug.Log("OnSwipeTop");
    }

    private void OnSwipeBottom() {
        mMessageIndex = 4;
        Debug.Log("OnSwipeBottom");
    }
    #endregion
}
