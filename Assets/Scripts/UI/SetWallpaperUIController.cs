﻿using UnityEngine;
using System.Collections;
using Freak.Chat;
using System.IO;
using Freak;

public class SetWallpaperUIController : MonoBehaviour {

	public Texture2D texture;

	public void OnClickBack()
	{
        FreakAppManager.Instance.GetHomeScreenPanel().PopPanel();
//		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}

	public void OnClickSetDefault()
	{
        if (InternetConnection.Check() == false)
        {
            AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
            return;
        }

		FindObjectOfType<AddProfileImageAPIController>().SaveUserInfoAPICall(FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender, 0, 0, 1);
        FreakAppManager.Instance.myUserProfile.wallpaper_image = "";

		AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.wallpaperChange);
	}

	public void OnClickGallery()
	{
		AddPhoto (1);
	}

	public void OnClickCamera()
	{
		AddPhoto (2);
	}
		

	public void AddPhoto(int selectedName)
	{
        if (InternetConnection.Check() == false)
        {
            AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
            return;
        }

        if (selectedName < 3)
        {
            #if UNITY_EDITOR
            string _path = Application.persistentDataPath + "/group.jpg";
            if (System.IO.File.Exists(_path))
            {
                Debug.Log(_path);
                SetImage(_path);
            }
            else
            {
                Debug.LogError(_path);
            }
            #else
			UIController.Instance.GetProfileImage (SetImage, selectedName);
            #endif
        }
        else
        {
            AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.wallpaperChange);
        }

	}


	void SetImage(string path)
	{
		//Debug.Log(mChatMessage.messageTimestamp + "Image File Exits");

        string _fileExtension = System.IO.Path.GetExtension(path);
        FreakAppManager.Instance.temporaryWallpaperName = "new_wallpaper_image" + _fileExtension;
        texture = null;
        byte[] fileByte = System.IO.File.ReadAllBytes(path);
        Freak.StreamManager.SaveFile(FreakAppManager.Instance.temporaryWallpaperName, fileByte, FolderLocation.Wallpaper, null, false);

        FindObjectOfType<AddProfileImageAPIController> ().UpdateChatWallpaper (fileByte);
        AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton,"Wallpaper changed !");

	}

//	void AssignImage (Texture2D tex, string textureName)
//	{
//		texture = tex;
//		Debug.Log ("Wallpaper Texrture Name ----------> " + textureName);
//		FindObjectOfType<ImageController>().SaveimageWallpaperCollection(texture, wallpaperLocation, textureName);
//		PlayerPrefs.SetString (wallpaperLocation, textureName);
//	}

	void HandleStreamSavedCallbackMethod (bool suddeeded)
	{
		Debug.Log ("HandleStreamSavedCallbackMethod");
	}

    void OnDisable ()
    {
        Destroy(this.gameObject);
    }
}
