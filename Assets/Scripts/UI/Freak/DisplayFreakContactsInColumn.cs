﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.Linq;
using System;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;


public class DisplayFreakContactsInColumn : MonoBehaviour, IEnhancedScrollerDelegate  {


	public FreaksContactPanel freaksContactPanel;
	public DisplayFreakContactsInRow freaksContentController;
	public string failString;
	int currentCount;

	public List<Contact> dummyContactsList;

	public ImageController imageController;

	public FreaksContactDisplayUIComponent m_cellPrefab;

//	public TableView m_tableView;
	public EnhancedScroller enhancedScroller;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	public List<ServerUserContact> serverContactList = new List<ServerUserContact> ();
	public Sprite defaultImage;

	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
	}


	void OnEnable()
	{
		//getContactListApiManager.GetContactListAPICall (FreakAppManager.Instance.mobileNumber, EnableTableView);
		//HideSearchBar ();
		serverContactList.Clear ();
		for (int i = 0; i < freaksContactPanel.serverContactList.Count; i++)
		{
			serverContactList.Add (freaksContactPanel.serverContactList [i]);
		}

        serverContactList.Sort((a, b) =>
            {
                return a.name.CompareTo(b.name);
            });

//		serverContactList = new List<ServerUserContact> (freaksContactPanel.serverContactList);
		Debug.Log ("serverContactList Count --- >" + freaksContactPanel.serverContactList.Count);
		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;
		enhancedScroller.Delegate = this;
		enhancedScroller.ClearAll ();
	}
		

	//
	//	//Will be called by the TableView to know what is the height of each row
	//	public float GetHeightForRowInTableView(TableView tableView, int row) {
	//		return GetHeightOfRow(row);
	//	}
	//
	//
	//
	//	//Will be called by the TableView when a cell needs to be created for display
	//	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	//	{
	//		ContactDisplayUIComponent cell = tableView.GetReusableCell(test_m_cellPrefab.reuseIdentifier) as ContactDisplayUIComponent;
	//		if (cell == null) {
	//			cell = (ContactDisplayUIComponent)GameObject.Instantiate(test_m_cellPrefab);
	//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
	//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
	//		}
	//
	//
	//		cell.rowNumber = row;
	//		AssignDetailsToGrids (cell, row);
	//		cell.height = GetHeightOfRow(row);
	//		return cell;
	//	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	 #region EnhancedScroller Handlers

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		if (serverContactList.Count > 0) {

			float val = (float)(serverContactList.Count / 3f);
			int d = (int) Mathf.Ceil(val);
			//Debug.Log ("serverContactList Count --- >" + serverContactList.Count + " current count --- > "+ d);
			return d;
		} else {
			return 0;
		}
	}
		

	/// <summary>
	/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
	/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
	/// cell size will be the width.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell size</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <returns>The size of the cell</returns>
	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
		//return (dataIndex % 2 == 0 ? 30f : 100f);
		return 250;
	}

	/// <summary>
	/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
	/// Some examples of this would be headers, footers, and other grouping cells.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
	/// <returns>The cell for the scroller to use</returns>
	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		// first, we get a cell from the scroller by passing a prefab.
		// if the scroller finds one it can recycle it will do so, otherwise
		// it will create a new cell.
		FreaksContactDisplayUIComponent cellView = scroller.GetCellView(m_cellPrefab) as FreaksContactDisplayUIComponent;

		// set the name of the game object to the cell's data index.
		// this is optional, but it helps up debug the objects in 
		// the scene hierarchy.
		//			cellView.name = "Cell " + dataIndex.ToString();

		// in this example, we just pass the data to our cell's view which will update its UI
		//		cellView.SetData(mFreakChachedChannelList[dataIndex]);
		AssignDetailsToGrids (cellView, dataIndex);
		//			cellView.InitFreakMessagingChannelNew(mFreakChachedChannelList[dataIndex]);

		//cell.InitFreakMessagingChannelNew(mFreakChachedChannelList[rowNumb]);


		// return the cell to the scroller
		return cellView;
	}

	#endregion


	void AssignDetailsToGrids(FreaksContactDisplayUIComponent cell, int rowNumb)
	{
		int firstColoumnIndex = rowNumb * 3;
		for (int coloumn = 0; coloumn < 3; coloumn++)
		{
			if (firstColoumnIndex + coloumn < serverContactList.Count)
			{
				ServerUserContact cont = serverContactList [firstColoumnIndex + coloumn];
				FreakContactsCellView contactsdetailsUiGrid = cell.contactDetailsList [coloumn];
				//cell.contactDetailsList [coloumn];
				contactsdetailsUiGrid.gameObject.SetActive (true);

				if (!string.IsNullOrEmpty (cont.name)) 
				{
					contactsdetailsUiGrid.nameText.text = cont.name;
				} 
				else 
				{
					contactsdetailsUiGrid.nameText.text = cont.contact_number;
				}

				contactsdetailsUiGrid.OnClickServerContact (cont);
				contactsdetailsUiGrid.numberText.text = cont.contact_number;
                contactsdetailsUiGrid.iD = cont.user_id.ToString();
                contactsdetailsUiGrid.IntUserForGroupChat (cont.user_id.ToString(), cont.name, cont.contact_number);

				if (!freaksContactPanel.isSelection) {
					contactsdetailsUiGrid.selectionToggle.gameObject.SetActive (true);
					contactsdetailsUiGrid.selectionToggle.isOn = cont.isSelectedForGroup;
				} else {
					contactsdetailsUiGrid.selectionToggle.gameObject.SetActive (false);
				}

				if (freaksContactPanel.isOnlyDisplay) 
				{
					contactsdetailsUiGrid.contactsButton.interactable = false;
				}

                contactsdetailsUiGrid.ShowProfileImage (cont);
			} 
			else {
				break;
			}
		}
	}


//	private float GetHeightOfRow(int row) {
//		if (m_customRowHeights.ContainsKey(row)) {
//			return m_customRowHeights[row];
//		} else {
//			return test_m_cellPrefab.height;
//		}
//	}
//
//	private void OnCellHeightChanged(int row, float newHeight) {
//		if (GetHeightOfRow(row) == newHeight) {
//			return;
//		}
//		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
//		m_customRowHeights[row] = newHeight;
//		m_tableView.NotifyCellDimensionsChanged(row);
//	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}



}
