﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using System.Linq;
using System;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;


public class DisplayFreakContactsInRow : MonoBehaviour, IEnhancedScrollerDelegate  {


	public FreaksContactPanel freaksPanelUIController;
	public DisplayFreakContactsInColumn freaksContentController;
	public string failString;
	int currentCount;

	public ImageController imageController;
//	public ContactDetailsDisplay m_cellPrefab;
	public FreakContactsCellView m_cellPrefab;
	private List<string> contactNumbers = new List<string> ();

//	public TableView m_tableView;
	public EnhancedScroller enhancedScroller;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	public List<ServerUserContact> serverContactList = new List<ServerUserContact> ();
	public Sprite defaultImage;

	void Start()
	{
		//imageController = FindObjectOfType<ImageController> ();
		//		#if UNITY_EDITOR
		//		foreach(Contact detailsObj in dummyContactsList)
		//		{
		//			Contacts.ContactsList.Add( detailsObj );
		//			Debug.Log(Contacts.ContactsList.Count);
		//		}
		//		#endif
	}


	void OnEnable()
	{

		serverContactList.Clear ();
		for (int i = 0; i < freaksPanelUIController.serverContactList.Count; i++)
		{
			serverContactList.Add (freaksPanelUIController.serverContactList [i]);
		}

//		serverContactList = new List<ServerUserContact> (freaksPanelUIController.serverContactList);

        serverContactList.Sort((a, b) =>
            {
                return a.name.CompareTo(b.name);
            });

		Debug.Log ("serverContactList ---->" + freaksPanelUIController.serverContactList.Count);
		m_customRowHeights = new Dictionary<int, float>();
//		m_tableView.dataSource = this;
		enhancedScroller.Delegate = this;
		enhancedScroller.ClearAll ();
	}
		

	public void OnTextEdit(InputField searchinput)
	{
		Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text);
		//Debug.Log ("ON TYPE SEARCH BAR ---->" + searchinput.text + " Count ----- > "+  tempCountryNames.Count);
		if (searchinput.text.Length > 0) 
		{
			for (int i = 0; i < serverContactList.Count; i++)
			{
				if (serverContactList [i].name.ToUpper ().Contains (searchinput.text.ToUpper())) 
				{

				} 
				else {
					//Debug.Log ("Remove ---->" + tempCountryNames [i].countryName + " Count --- > " + tempCountryNames.Count);
					serverContactList.RemoveAt (i);
				}
			}
		}
		else {
			serverContactList.Clear ();
			for (int i = 0; i <  freaksContentController.serverContactList.Count; i++) 
			{
				serverContactList.Add (freaksContentController.serverContactList [i]);
			}
		}

		//m_tableView.RefreshVisibleRows ();
		enhancedScroller.ReloadData ();

	}


	public void OnClearText()
	{
		serverContactList.Clear ();
		for (int i = 0; i <  freaksContentController.serverContactList.Count; i++) 
		{
			serverContactList.Add (freaksContentController.serverContactList [i]);
		}

		enhancedScroller.ReloadData ();
	}


	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//	//Will be called by the TableView to know how many rows are in this table
	//	public int GetNumberOfRowsForTableView(TableView tableView)
	//	{
	//		if (serverContactList.Count > 0) {
	//
	//			float val = (float)(serverContactList.Count / 3f);
	//			int d = (int) Mathf.Ceil(val);
	//			Debug.Log ("serverContactList Count --- >" + serverContactList.Count + " current count --- > "+ d);
	//			return d;
	//		} else {
	//			return 0;
	//		}
	//	}


//	//Will be called by the TableView to know how many rows are in this table
//	public int GetNumberOfRowsForTableView(TableView tableView)
//	{
//		//return 10;
//		if (serverContactList.Count > 0) {
//			return serverContactList.Count;
//		} else {
//			return 0;
//		}
//	}
//
//
//	//Will be called by the TableView to know what is the height of each row
//	public float GetHeightForRowInTableView(TableView tableView, int row) {
//		return GetHeightOfRow(row);
//	}
//
//
//	//Will be called by the TableView when a cell needs to be created for display
//	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
//	{
//		ContactDetailsDisplay cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ContactDetailsDisplay;
//		if (cell == null) {
//			cell = (ContactDetailsDisplay)GameObject.Instantiate(m_cellPrefab);
//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//		}
//
//		cell.rowNumber = row;
//		AssignDetails (cell, row);
//		cell.height = GetHeightOfRow(row);
//		return cell;
//	}
		

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region EnhancedScroller Handlers

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfCells(EnhancedScroller scroller)
	{

		if (serverContactList.Count > 0) {
			return serverContactList.Count;
		} else {
			return 0;
		}
	}

	/// <summary>
	/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
	/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
	/// cell size will be the width.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell size</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <returns>The size of the cell</returns>
	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
		//return (dataIndex % 2 == 0 ? 30f : 100f);
		return 180;
	}

	/// <summary>
	/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
	/// Some examples of this would be headers, footers, and other grouping cells.
	/// </summary>
	/// <param name="scroller">The scroller requesting the cell</param>
	/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
	/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
	/// <returns>The cell for the scroller to use</returns>
	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		// first, we get a cell from the scroller by passing a prefab.
		// if the scroller finds one it can recycle it will do so, otherwise
		// it will create a new cell.
		FreakContactsCellView cellView = scroller.GetCellView(m_cellPrefab) as FreakContactsCellView;

		// set the name of the game object to the cell's data index.
		// this is optional, but it helps up debug the objects in 
		// the scene hierarchy.
		//			cellView.name = "Cell " + dataIndex.ToString();

		// in this example, we just pass the data to our cell's view which will update its UI
		//		cellView.SetData(mFreakChachedChannelList[dataIndex]);
		AssignDetails (cellView, dataIndex);
		//			cellView.InitFreakMessagingChannelNew(mFreakChachedChannelList[dataIndex]);

		//cell.InitFreakMessagingChannelNew(mFreakChachedChannelList[rowNumb]);


		// return the cell to the scroller
		return cellView;
	}

	#endregion


	void AssignDetails(FreakContactsCellView cell, int rowNumb)
	{
		ServerUserContact cont = serverContactList [rowNumb];

		if (!string.IsNullOrEmpty (cont.name)) {
			cell.nameText.text = cont.name;
		} else {
			cell.nameText.text = "Unknown Contact " + rowNumb;
		}
		cell.numberText.text = cont.contact_number;
		cell.numberText.text = cont.status_message;
	//	cell.numberProText.text = cont.contactNumb;
	//	cell.numberProText.text = cont.statusMsg;
        cell.iD = cont.user_id.ToString();
        cell.IntUser (cont.user_id.ToString(), cont.name);
		cell.selectionToggle.gameObject.SetActive (false);
		cell.IntUserForGroupChat (cont.user_id.ToString(), cont.name, cont.contact_number);

        cell.ShowProfileImage (cont);

//		if (cont.userImage == null) 
//		{
        /*if (imageController == null) {
			imageController = FindObjectOfType<ImageController> ();
		}
		imageController.GetImageFromLocalOrDownloadImage (cont.avatar, cell.img, Freak.FolderLocation.Profile);*/

//		cell.img.sprite = defaultImage;
//		} 
//		else
//		{
//			Rect rec = new Rect(0, 0, cont.userImage.width, cont.userImage.height);
//			Sprite spr = Sprite. Create(cont.userImage,rec,new Vector2(0,0),1);
//			cell.img.sprite = spr;
//		}
		cell.OnClickServerContact (cont);
		if (!freaksPanelUIController.isSelection) {
			cell.selectionToggle.gameObject.SetActive (true);
			cell.selectionToggle.isOn = cont.isSelectedForGroup;
		} else {
			cell.selectionToggle.gameObject.SetActive (false);
		}

		if (freaksPanelUIController.isOnlyDisplay) 
		{
			cell.contactsButton.interactable = false;
		}

	}


	#endregion

//	private float GetHeightOfRow(int row) {
//		if (m_customRowHeights.ContainsKey(row)) {
//			return m_customRowHeights[row];
//		} else {
//			return m_cellPrefab.height;
//		}
//	}
//
//	private void OnCellHeightChanged(int row, float newHeight) {
//		if (GetHeightOfRow(row) == newHeight) {
//			return;
//		}
//		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
//		m_customRowHeights[row] = newHeight;
//		m_tableView.NotifyCellDimensionsChanged(row);
//	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}


	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}


}
