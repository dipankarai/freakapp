﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Freak.Chat;


public class FreaksContactPanel : MonoBehaviour {

	public List<ServerUserContact> serverContactList = new List<ServerUserContact> ();

	public DisplayFreakContactsInColumn displayContactsInPanel;
	public DisplayFreakContactsInRow displayFreakContactsInRow;

	public GameObject searchBar;
	public InputField searchBarTextField;
	public Button searchBtn;
	public Button RowColumnBtn;

	enum SearchBarStatus{
		NotDisplayed,
		Displayed
	}
	SearchBarStatus searchBarStatus;

	public bool isFreaksScene;
	public GameObject nextButton;
	public GameObject menuBtn;
	public bool isSelection;
	public bool isOnlyDisplay;

	public Image rowColumImage;
	public Sprite rowSprite;
	public Sprite columnSprite;
    public Text contactFreakContact;

//	private List<ServerUserContact> tempContacts = new List<ServerUserContact>();
	public ConnectingUI loadingObj;
	Vector3 bottomPanelPosition;

	enum Status{
		Grid,
		Panel
	}
	Status status;

	void Start(){
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
		serverContactList = new List<ServerUserContact> ();
		bottomPanelPosition = searchBar.transform.rectTransform ().localPosition;
		OnEnables ();
	}


//	void Update()
//	{
//		#if UNITY_ANDROID
//		if (Input.GetKeyDown(KeyCode.Escape)&& searchBar.activeInHierarchy)
//		{
//			HideSearchBar();
//		}
//		#endif
//	}

	void OnEnables()
	{
		displayFreakContactsInRow.enabled = false;
		loadingObj.DisplayLoading ();

        FreakAppManager.Instance.LoadServerContactList (EnableTableView);
	}

	void EnableTableView(List<ServerUserContact> data)
	{
		loadingObj.gameObject.SetActive (false);
		HideSearchBarOnClose ();
		serverContactList.Clear ();

        for (int i = 0; i < data.Count; i++)
        {
            if (CheckAlreadyInGroup(data[i].user_id.ToString()) == false)
            {
                serverContactList.Add (data [i]);
            }
        }

        ChatManager.Instance.memberList.Clear ();

//		for (int i = 0; i < Contacts.ContactsList.Count; i++) 
//		{
//
//			//for (int j = 0; j < data.Count; j++) 
//			//{
//				//if(Contacts.ContactsList[i].)
//				Contact phone = Contacts.ContactsList [i];
//				ServerUserContact phoneContacts = new ServerUserContact ();
//				phoneContacts.user_id = int.Parse(phone.Id);
//				phoneContacts.userName = phone.Name;
//				phoneContacts.contactNumb = phone.Phones [0].Number.ToString ();
//				phoneContacts.userImage = phone.PhotoTexture;
//			//}
//			//tempContacts.Add (Contacts.ContactsList [i]);
//		}

		displayContactsInPanel.enabled = true;
		searchBtn.gameObject.SetActive (false);
		SetSprite (rowSprite);
		status = Status.Panel;

        if (serverContactList.Count > 0)
        {
            contactFreakContact.text = string.Empty;
        }
        else
        {
            contactFreakContact.text = "Invite Freak.";
        }
	}

    bool CheckAlreadyInGroup (string id)
    {
        for (int i = 0; i < ChatManager.Instance.memberList.Count; i++)
        {
            if (ChatManager.Instance.memberList[i].UserId == id)
            {
                return true;
            }
        }

        return false;
    }

	public void OnClickOptions()
	{
		if (status == Status.Panel) {
			HideSearchBar ();
			displayFreakContactsInRow.enabled = true;
			displayContactsInPanel.enabled = false;
			searchBtn.gameObject.SetActive (true);
			SetSprite (columnSprite);
			status = Status.Grid;
			return;
		} else
		{
			displayContactsInPanel.enabled = true;
			displayFreakContactsInRow.enabled = false;
			searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
			searchBar.SetActive (false);
			searchBtn.gameObject.SetActive (false);
			RowColumnBtn.gameObject.SetActive (true);
			SetSprite (rowSprite);
			status = Status.Panel;
		}
	}

	public void HideSearchBar()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		RowColumnBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;

		displayFreakContactsInRow.OnClearText ();
	}

	public void HideSearchBarOnClose()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (true);
		RowColumnBtn.gameObject.SetActive (true);
		searchBarStatus = SearchBarStatus.NotDisplayed;
	}


	public void ShowSearchBar()
	{
		searchBar.SetActive (true);
		searchBtn.gameObject.SetActive (false);
		RowColumnBtn.gameObject.SetActive (false);
		searchBarStatus = SearchBarStatus.Displayed;

		searchBarTextField.text=("");
	}

	public void Hide()
	{
		searchBar.transform.rectTransform ().localPosition = bottomPanelPosition;
		searchBar.SetActive (false);
		searchBtn.gameObject.SetActive (false);
		RowColumnBtn.gameObject.SetActive (false);
	}

	public void OnClickShowContacts()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}
		

	public void OnClickCreateGroup()
	{
		//FindObjectOfType<FreaksPanelUIController> ().PushPanel (FreaksPanelUIController.PanelType.ServerSynchedContactsPanel);
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.ServerSynchedContactsDisplayPanel);
	}

	public void OnClickBack()
	{
        if (FreakAppManager.Instance.GetHomeScreenPanel().previousPanelState == HomeScreenUIPanelController.PanelType.GroupProfilePanel)
        {
            ChatManager.Instance.groupUsersList.Clear ();
            FreakAppManager.Instance.selectedGroupProfile = mChatChannel;
            GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPopPanelObject ();
            ChatManager.Instance.GetChannelByChannelUrl (mChatChannel.channelUrl, go.GetComponent<GroupProfileUIController> ().GetNewGroupChannelCallback);
//            go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (mChatChannel);
        }
        else
        {
            FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
        }
	}

    public bool isAddMember;
    public ChatChannel mChatChannel;
	public void OnClickNext()
	{
        if (ChatManager.Instance.groupUsersList.Count > 0)
        {
            if (isAddMember)
            {
                //FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
                FreakAppManager.Instance.selectedGroupProfile = mChatChannel;
                GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPopPanelObject ();
                ChatManager.Instance.GetChannelByChannelUrl (mChatChannel.channelUrl, go.GetComponent<GroupProfileUIController> ().GetNewGroupChannelCallback);
//                go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (mChatChannel);
            }
            else
            {
                FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.CreateGroupPanel);
            }
        } else 
        {
            Debug.Log ("Select More than one user");
        }
    }

	public void DisableNextButton()
	{
		isSelection = true;
		nextButton.SetActive (false);
		menuBtn.SetActive (true);
	}


	void OnDisable()
    {
		Destroy (this.gameObject);
		Resources.UnloadUnusedAssets ();
	}

	void SetSprite(Sprite spr)
	{
		rowColumImage.sprite = spr;
	}
}
