﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI.Extensions;
using System.Collections.Generic;

public class TutorialController : MonoBehaviour {

	public HorizontalScrollSnap horizontalScrollSnap;
	public List<TutorialNames> tutorialData;

	public Text tutorialText;
	public Text tutorialDesciptionText;
	public GameObject paginationToggleObj;


	void Update()
	{
		tutorialDesciptionText.text = tutorialData [horizontalScrollSnap.CurrentPage].tutorialDesc;
		tutorialText.text = tutorialData [horizontalScrollSnap.CurrentPage].tutorialName;
	}


	void OnEnable(){
		SetInit ();
	}
		

	void SetInit(){
		//horizontalScrollSnap.ChangeBulletsInfo (0);
	}
		

	[System.Serializable]
	public class TutorialNames
	{
		public string tutorialName;
		public string tutorialDesc; 
	}


	public void OnClickGo()
	{
        SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.FreakHomeScene);
	}
}
