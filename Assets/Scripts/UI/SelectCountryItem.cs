﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;

	//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
	//containing this component as a cell in a TableView
	public class SelectCountryItem : TableViewCell
	{
		public Text nameText;
		public int iD;

		public Slider m_cellHeightSlider;

		public int rowNumber { get; set; }

		[System.Serializable]
		public class CellHeightChangedEvent : UnityEvent<int, float> { }
		public CellHeightChangedEvent onCellHeightChanged;

		public void SliderValueChanged(Slider slider) {
			onCellHeightChanged.Invoke(rowNumber, slider.value);
		}

		public float height {
			get { return m_cellHeightSlider.value; }
			set { m_cellHeightSlider.value = value; }
		}

		public void OnClickCountry()
		{
			UIPanelController uiPanelController = FindObjectOfType<UIPanelController> () as UIPanelController;
			uiPanelController.welcomePanelController.SetCountryDetails (this);
			uiPanelController.PopPanel ();

		}

}
