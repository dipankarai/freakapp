﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;

public class ProfileController : MonoBehaviour {

	public Image profilePicImg;

	public Text nameText;
	public Text secondaryNameText;
	public Text statusText;
	public Text opponentText;
	public Text contactNumberText;
    public Text totalScoreText;
	private Texture2D texture;

	public string userId;
	public ConnectingUI loadingObj;
    public InfoPopupPanel infoPopupPanel;

	public Text blockUnblockText;

    private ServerUserContact mServerUserContact;

	// Use this for initialization
	void OnEnable () 
	{
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}

	void InitialCall()
	{
		userId = FreakAppManager.Instance.myUserProfile.memberId;
		mServerUserContact = FreakAppManager.Instance.GetServeruserContact(userId);

        if (mServerUserContact == null)
        {
            FreakAppManager.Instance.LoadServerContactList(LoadContactHandler);
        }

        LoadImage();
        InitUserData();

        if (!string.IsNullOrEmpty (userId) && InternetConnection.Check()) 
        {
            //loadingObj.DisplayLoading ();
            GetUserInfoAPICall (userId);
        }

        GetChannelUrl ();
        CheckIfUserBlocked ();

        /* if (FreakAppManager.Instance.senderCompressedPic != null)
        {
            profilePicImg.FreakSetSprite(FreakAppManager.Instance.senderCompressedPic);
            FreakAppManager.Instance.senderCompressedPic = null;
        }*/
	}

    void LoadContactHandler (System.Collections.Generic.List<ServerUserContact> obj)
    {
        mServerUserContact = FreakAppManager.Instance.GetServeruserContact(userId);
    }


    private ChatChannel mChatChannel;
    void GetChannelUrl ()
    {
        for (int i = 0; i < ChatManager.Instance.cachedData.allChannelList.Count; i++)
        {
            if (ChatManager.Instance.cachedData.allChannelList[i].otherUserId == userId)
            {
                mChatChannel = ChatManager.Instance.cachedData.allChannelList [i];
            }
        }
    }
		
    public bool gotoAllChat = true;
	public void OnClickBack()
	{
        if(FreakAppManager.Instance.tempCurrentChannel != null && string.IsNullOrEmpty(FreakAppManager.Instance.tempCurrentChannel.channelUrl) == false)
        {
            GameObject go = FindObjectOfType<HomeScreenUIPanelController> ().GetReplacedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
            go.GetComponent<ChatWindowUI> ().InitChat (FreakAppManager.Instance.tempCurrentChannel, true);
            ChatManager.Instance.GetChannelByChannelUrl (FreakAppManager.Instance.tempCurrentChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);

            FreakAppManager.Instance.tempCurrentChannel = null;
        }
        else
        {
            FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
        }
	}


	public void OnClickOpenChat ()
	{
		Debug.Log ("OnClick Open Chat ");
        FreakAppManager.Instance.senderCompressedPic = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);

        ChatChannel chatChannel = new ChatChannel ();
        GameObject go = FindObjectOfType<HomeScreenUIPanelController> ().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);

        if (mChatChannel != null && string.IsNullOrEmpty(mChatChannel.channelUrl) == false)
        {
            chatChannel =  mChatChannel;
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel, true);
            ChatManager.Instance.GetChannelByChannelUrl (chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
        }
        else
        {
            chatChannel.otherUserId = userId;
            chatChannel.otherUserName = nameText.text;
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel);
        }

	}

    public void OnClickCallContact ()
    {
        string _callTel = "tel:" + contactNumberText.text;
        Debug.Log ("OnClickCallContact Open Phone " + _callTel);

        #if UNITY_EDITOR
        Debug.Log ("Open Phone Editor " + _callTel);
        #else
        Application.OpenURL(_callTel);
        #endif
    }

	private void GetGroupChannelCallback (SendBird.GroupChannel groupChannel)
	{
		Debug.Log ("GetGroupChannelCallback " + groupChannel.Name);
		ChatManager.Instance.currentChannel = groupChannel;
	}


	public void OnClickPlay()
	{
        Debug.Log ("OnClickPlay");

        if (mServerUserContact != null)
        {
            FreakAppManager.Instance.currentGameData.is_single_mode = false;
            FreakAppManager.Instance.currentGameData.isGameSelected = true;
            FreakAppManager.Instance.currentGameData.isChallengeAccepted = false;
            FreakAppManager.Instance.currentGameData.opponent_name = mServerUserContact.name;
            FreakAppManager.Instance.currentGameData.opponent_id = mServerUserContact.user_id.ToString();
            FreakAppManager.Instance.currentGameData.opponent_profile_url = mServerUserContact.avatar;

            FreakAppManager.Instance.GetHomeScreenPanel().PushPanel(HomeScreenUIPanelController.PanelType.GameSelectionPanel);
        }
        else
        {
            
        }
	}

	public void OnClickHelp()
	{
        Debug.Log ("OnClickHelp");
	}

	public void OnClickRewards()
	{
        Debug.Log ("OnClickRewards");
	}

	public void OnClickInvite()
	{
        Debug.Log ("OnClickInvite");
	}

	public void OnClickSelectedImage()
	{
        Debug.Log ("OnClickSelectedImage");
	}


	public void OnClickInfo()
	{
        Debug.Log ("OnClickInfo");

        infoPopupPanel.OnClickOpenPanel();
	}


    private bool isblocked;
    private bool isClicked;

    void CheckIfUserBlocked()
    {
        FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
        if (blockUserList.IsBlocked (userId)) {
            isblocked = true;
            blockUnblockText.text = "UnBlock";
        } else {
            isblocked = false;
            blockUnblockText.text = "Block";
        }
    }

	public void OnClickBlock()
	{
		if (ChatManager.CheckInternetConnection ()) 
		{
            if (isClicked)
                return;

            if (InternetConnection.Check () == false)
                return;

            isClicked = true;

			if (isblocked)
			{
				Debug.Log ("Unblock User");
                ChatServerAPI.Instance.UnblockUser (ChatManager.Instance.UserId, userId, UnblockUserCallback);

			}
            else
            {
                Debug.Log ("Block User");
                ChatManager.Instance.BlockUser(userId, BlockUserCallback);
            }
            
        }
	}

    void BlockUserCallback (bool blocked)
    {
        isClicked = false;
        Debug.Log("BlockUserCallback " + blocked);
		if (blocked) 
		{
            isblocked = true;

			FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
			blockUserList.AddUser (userId);
			FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;

        }

        CheckIfUserBlocked ();
    }


	void UnblockUserCallback(bool success)
	{
        isClicked = false;

		if (success) 
		{
			isblocked = false;

            FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
            blockUserList.RemoveUser (userId);
            FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;
		}

        CheckIfUserBlocked ();
	}


//	void SetImage(string path)
//	{
//		//Debug.Log(mChatMessage.messageTimestamp + "Image File Exits");
//		texture = null;
//		byte[] fileByte = System.IO.File.ReadAllBytes(path);
//		texture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
//		texture.LoadImage(fileByte);
//		AssignImage (texture);
//	}
//
//	void AssignImage (Texture2D tex)
//	{
//		Rect rec = new Rect(0, 0, tex.width, tex.height);
//		Sprite spr = Sprite. Create(tex,rec,new Vector2(0,0),1);
//		//selectedProfilePicImg.sprite = spr;
//
//	}


	void OnDisable()
	{
		Destroy (this.gameObject);
	}


	/// <summary>
	/// Get User information
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetUserInfoAPICall(string userId)
	{
        FreakApi api = new FreakApi ("user.getUserInfo", ServerResponseForGetUserInfoAPICall);
		api.param ("other_user_id", userId);
		api.get (this);
	}

	void ServerResponseForGetUserInfoAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		loadingObj.gameObject.SetActive (false);
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
                case 001:
                    IDictionary userDetailsDict = (IDictionary)output["responseMsg"];

                    if (mServerUserContact == null)
                        mServerUserContact = new ServerUserContact();

					mServerUserContact.name = DecodeString((string)userDetailsDict["name"]);
                    mServerUserContact.secondary_name = (string)userDetailsDict["secondary_name"];
                    mServerUserContact.avatar = (string)userDetailsDict["avatar_url"];
                    mServerUserContact.avatar_name = (string)userDetailsDict["avatar_name"];
                    mServerUserContact.gender = (string)userDetailsDict["gender"];
                    mServerUserContact.status_message = (string)userDetailsDict["status_message"]; 
                    mServerUserContact.contact_number = userDetailsDict["contact_number"].ToString();  

                    if (userDetailsDict.Contains("total_score"))
                    {
                        mServerUserContact.totalScore = int.Parse(userDetailsDict["total_score"].ToString());
                    }
				
                    InitUserData();

                    /*string userName = (string)userDetailsDict ["name"];
                    string secondary_name = (string)userDetailsDict ["secondary_name"];
                    string avatarLink = (string)userDetailsDict ["avatar_url"];
                    string avatarName = (string)userDetailsDict ["avatar_name"];
                    string gender = (string)userDetailsDict ["gender"];
                    string status = (string)userDetailsDict ["status_message"]; 
                    string contactNumb = userDetailsDict ["contact_number"].ToString ();  


            if (nameText != null)
                    {
                        nameText.text = userName;
                        opponentText.text = userName;
                        contactNumberText.text = contactNumb;
                    }

				secondaryNameText.text = secondary_name;
				statusText.text = status;
				LoadImage (avatarLink, avatarName);*/
//				CheckIfUserBlocked ();
				break;
			case 207:
				break;
			default:
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}

	string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}

    void InitUserData ()
    {
        if (mServerUserContact != null)
        {
            if (nameText != null)
            {
                nameText.FreakSetTextBackground(mServerUserContact.name, 0.4f);
//                nameText.text = mServerUserContact.name;
                opponentText.text = mServerUserContact.name;
                contactNumberText.text = mServerUserContact.contact_number;
            }

            secondaryNameText.FreakSetTextBackground(mServerUserContact.secondary_name, 0.4f);
//            secondaryNameText.text = mServerUserContact.secondary_name;
            statusText.FreakSetTextBackground(mServerUserContact.status_message, 0.4f);
//            statusText.text = mServerUserContact.status_message;
            totalScoreText.text = FreakAppManager.Instance.myUserProfile.totalScore.ToString() + " : " + mServerUserContact.totalScore.ToString();

//            LoadImage(mServerUserContact.avatar, mServerUserContact.avatar_name);
            LoadImage();
        }
    }

    void LoadImage ()
    {
        //FreakAppManager.Instance.senderCompressedPic = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);

        if (FreakAppManager.Instance.senderCompressedPic != null)
        {
            /*string profilePath = Freak.StreamManager.LoadFilePath(System.IO.Path.GetFileName(mServerUserContact.avatar), Freak.FolderLocation.ProfileHD, false);
            if (System.IO.File.Exists(profilePath))
            {
                byte[] bytes = Freak.StreamManager.LoadFileDirect(profilePath);
                if (bytes != null)
                {
                    profilePicImg.FreakSetTexture(bytes);
                }
            }*/

            profilePicImg.FreakSetSprite(FreakAppManager.Instance.senderCompressedPic);
        }
        else
        {
            if (string.IsNullOrEmpty(mServerUserContact.avatar) == false)
                FreakAppManager.Instance.senderCompressedPic = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);
            
            Invoke("LoadImage", 0.5f);
            return;
        }

        Invoke("LoadHDImage", 0.01f);
    }

    void LoadHDImage ()
    {
        if (mServerUserContact != null && string.IsNullOrEmpty(mServerUserContact.avatar) == false)
        {
            string _filename = System.IO.Path.GetFileName(mServerUserContact.avatar);
            string _filePath = Freak.StreamManager.LoadFilePath(_filename, Freak.FolderLocation.ProfileHD, false);
            if (System.IO.File.Exists(_filePath))
            {
                Texture2D _texture = new Texture2D(1, 1);
                _texture.LoadImage (Freak.StreamManager.LoadFileDirect(_filePath));
                profilePicImg.FreakSetSprite(_texture);
                _texture = null;
            }
            else
            {
                FreakAppManager.Instance.senderCompressedPic = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);
                Invoke("LoadHDImage", 0.5f);
            }
        }
    }

    /*void LoadImage(string url, string avatarName)
	{
        if (FreakAppManager.Instance.senderCompressedPic != null)
        {
            profilePicImg.FreakSetSprite(FreakAppManager.Instance.senderCompressedPic);
            FreakAppManager.Instance.senderCompressedPic = null;
        }

        if (InternetConnection.Check())
        {
            string profilePath = Freak.StreamManager.LoadFilePath(System.IO.Path.GetFileName(url), Freak.FolderLocation.ProfileHD, false);
            byte[] bytes = Freak.StreamManager.LoadFileDirect(profilePath);

            if (bytes != null)
            {
                profilePicImg.FreakSetTexture(bytes);
            }
            else
            {
                FindObjectOfType<ImageController> ().GetImageFromLocalOrDownloadImage (url, profilePicImg, Freak.FolderLocation.Profile);
            }
        }
	}*/

//	void SetImage(Texture2D tex)
//	{
//		FreakAppManager.Instance.myUserProfile.userPic = tex;
//		Rect rec = new Rect(0, 0, tex.width, tex.height);
//		Sprite spr = Sprite. Create(FreakAppManager.Instance.myUserProfile.userPic,rec,new Vector2(0,0),1);
//		profilePicImg.sprite = spr;
//	}

}
