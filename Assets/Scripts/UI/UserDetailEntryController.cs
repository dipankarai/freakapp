﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;
using Freak;
using MiniJSON;
using System;
using System.Collections.Generic;
using System.Xml; 
using System.Xml.Serialization;  
using System.Linq;


public class UserDetailEntryController : MonoBehaviour {

	public InputField nameInputField;
	public InputField gameNameInputFieled;
	private NativeEditBox nameNativeField;
	private NativeEditBox gameNameNativeFieled;
	public InputField referenceCodeInputField;

	public Toggle maleToggleBtn;
	public Toggle femaleToggleBtn;

	public Image profileImage;

	public GameObject OptionsPanelObject;
	public AddProfileImageAPIController addProfileImageAPIController;

	public Texture2D texture;
//	public ConnectingUI loadingObj;
	byte[] fileByte;
    public ImageController imageController;

	private string nameInput,gameNameinput;

    void OnEnable ()
    {
        if(imageController == null)
        {
            imageController = FindObjectOfType<ImageController> ();
        }
		
        nameNativeField = nameInputField.gameObject.GetComponent<NativeEditBox> ();
		gameNameNativeFieled = gameNameInputFieled.gameObject.GetComponent<NativeEditBox> ();

        DisableInputField();

        Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
    }

    void InitialCall ()
    {
//      nameNativeField = nameInputField.gameObject.GetComponent<NativeEditBox> ();
//      gameNameNativeFieled = gameNameInputFieled.gameObject.GetComponent<NativeEditBox> ();

        nameInputField.gameObject.SetActive(true);
        gameNameInputFieled.gameObject.SetActive(true);

        nameNativeField.SetTextNative(nameInput);
        gameNameNativeFieled.SetTextNative(gameNameinput);

        if (FreakAppManager.Instance.myUserProfile != null && string.IsNullOrEmpty (FreakAppManager.Instance.myUserProfile.name) == false)
        {
            InitUserProfile ();

			if (FreakAppManager.Instance.myUserProfile.avatar_url.Equals("http://juegostudio.in/FREAK-CHAT/TEST/static/images/avatar.png") == false)
            {
                LoadImage (FreakAppManager.Instance.myUserProfile.avatar_url, FreakAppManager.Instance.myUserProfile.avatar_name);
            }

            if (string.IsNullOrEmpty(FreakAppManager.Instance.myUserProfile.cover_image) == false)
            {
                DownloadManager.Instance.DownLoadWallpaper(FreakAppManager.Instance.myUserProfile.cover_image, FolderLocation.Profile, null);
            }
        }

        FindObjectOfType<UserProfileUIController> ().loadingObj.HideAfterFewSecondsLoading(1f);
    }
		

    void InitUserProfile ()
    {
        nameInput = FreakAppManager.Instance.myUserProfile.name;
        gameNameinput = FreakAppManager.Instance.myUserProfile.secondary_name;

		AssignNameString();

        if (FreakAppManager.Instance.myUserProfile.gender.Equals("Male"))
        {
            maleToggleBtn.isOn = true;
        }
        else if (FreakAppManager.Instance.myUserProfile.gender.Equals("Female"))
        {
            femaleToggleBtn.isOn = true;
        }
    }

	void AssignNameString ()
    {
        nameInputField.text = nameInput;
        gameNameInputFieled.text = gameNameinput;
        nameNativeField.SetTextNative(nameInput);
        gameNameNativeFieled.SetTextNative(gameNameinput);
    }

    void LoadImage(string url, string avatarName)
    {
        DownloadManager.Instance.DownLoadWallpaper(url, FolderLocation.Profile, AssignImageCallBack);
//		imageController.GetImageFromLocalOrDownload (url, profileImage, FolderLocation.Profile, AssignImageCallBack);
    }

	public void OnClickGo()
	{
		string gender;
//		nameInput = nameInputField.GetTextNative ();
//		gameNameinput = gameNameInputFieled.GetTextNative ();
		nameInput = nameInputField.text;
		gameNameinput = gameNameInputFieled.text;
		if (CheckAllTextEntered()) 
		{
			if (maleToggleBtn.isOn || femaleToggleBtn.isOn)
			{
				if (maleToggleBtn.isOn) 
				{
					gender = "Male";
				} 
				else 
				{
					gender = "Female";
				}

                FindObjectOfType<UserProfileUIController> ().loadingObj.DisplayLoading ();
				SaveUserInfoAPICall (nameInput,gameNameinput, gender, 0, 0);
			}
			else 
			{
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Please select Gender.");
			}
		}
	}


	bool CheckAllTextEntered()
	{
		if (!string.IsNullOrEmpty (nameInput) && !string.IsNullOrEmpty (gameNameinput)) 
		{
			return true;
		} 
		else 
		{
			nameInputField.gameObject.SetActive(false);
			gameNameInputFieled.gameObject.SetActive(false);

			if (!string.IsNullOrEmpty (nameInput) && string.IsNullOrEmpty (gameNameinput))
			{
				AlertMessege.Instance.onClickOkButtonCallback = EnableInputField;
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Please enter your avatar name.");
				return false;
			} 
			else if (string.IsNullOrEmpty (nameInput) && !string.IsNullOrEmpty (gameNameinput)) 
			{
				AlertMessege.Instance.onClickOkButtonCallback = EnableInputField;
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Please enter your name.");
				return false;
			} 
			else {
				AlertMessege.Instance.onClickOkButtonCallback = EnableInputField;
				AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Please enter all details.");
				return false;
			}
		}
	}


	void EnableInputField()
	{
		nameInputField.gameObject.SetActive(true);
		gameNameInputFieled.gameObject.SetActive(true);
		nameNativeField.SetTextNative(nameInput);
		gameNameNativeFieled.SetTextNative(gameNameinput);
	}


	public void OnClickAddPhoto()
	{
		OptionsPanelObject.SetActive (true);
		HideInputField(true);
	}

	public void CancelAddPhoto()
	{
		OptionsPanelObject.SetActive (false);

		nameInputField.gameObject.SetActive(true);
		gameNameInputFieled.gameObject.SetActive(true);

        nameNativeField.SetTextNative(nameInput);
		gameNameNativeFieled.SetTextNative(gameNameinput);
	}

	public void AddPhoto(int selectedName)
	{
		
		if (selectedName < 3) {
			OptionsPanelObject.SetActive (false);
			#if UNITY_EDITOR
			string _path = Application.persistentDataPath + "/group.jpg";
			if (System.IO.File.Exists(_path))
			{
				Debug.Log(_path);
				SetImage(_path);
			}
			else
			{
				Debug.LogError(_path);
			}
			#else
			UIController.Instance.GetProfileImage (SetImage, selectedName);
			#endif
		} 
		else {
			CancelAddPhoto ();
		}
			
//      nameInputField.gameObject.SetActive(true);
//      gameNameInputFieled.gameObject.SetActive(true);
//
//      nameNativeField.SetTextNative(nameInput);
//      gameNameNativeFieled.SetTextNative(gameNameinput);
	}

		
	void SetImage(string path)
	{
		Debug.Log("SetImage Image File Exits");
		texture = null;
		fileByte = System.IO.File.ReadAllBytes(path);
		texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
		texture.LoadImage(fileByte);

        if (texture.height > 500 || texture.width > 500)
        {
            texture = TextureResize.ResizeTexture(500f, 500f, texture);
            fileByte = texture.EncodeToPNG();
        }

//        texture.Compress (true);
        Freak.StreamManager.SaveFile("FreakAppManager.Instance.myUserProfile.avatar_url.png", fileByte, Freak.FolderLocation.ProfileHD, null, false);
        AssignImage (texture);
	}

	void AssignImage (Texture2D tex)
	{
		Debug.Log("AssignImage");
        if (tex != null)
        {
            profileImage.FreakSetSprite(tex);

            byte[] fileByte = null;
            if (tex.height > 100 || tex.width > 100)
            {
                tex = TextureResize.ResizeTexture(100f, 100f, tex);
                fileByte = tex.EncodeToPNG();
            }

            Freak.StreamManager.SaveFile("FreakAppManager.Instance.myUserProfile.avatar_url.png", fileByte, Freak.FolderLocation.Profile, null, false);
            FreakAppManager.Instance.myUserProfile.userPic = tex;
        }

        /*Rect rec = new Rect(0, 0, tex.width, tex.height);
        Sprite spr = Sprite. Create(tex,rec,new Vector2(0,0),1);
        profileImage.sprite = spr;*/
//      FreakAppManager.Instance.userProfileImage = TextureResize.ResizeTexture (150f, 150f, tex).EncodeToPNG ();
	
		HideInputField(false);
	}
		

	void AssignImageCallBack(Texture2D tex)
	{
        if (tex != null)
        {
            //FreakAppManager.Instance.userProfileImage = TextureResize.ResizeTexture (150f, 150f, tex).EncodeToPNG ();
//            if (tex.width > 200 || tex.height > 200)
//            {
//                tex.Compress(false);
//            }
            
    //      FreakAppManager.Instance.myUserProfile.userPic = tex;
            profileImage.FreakSetSprite(tex);
        }

        CancelAddPhoto ();
	}


	/// <summary>
	/// User Authentication
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void SaveUserInfoAPICall(string username, string secondaryName, string geneder, int coverPicStatus, int profilePicStatus)
	{
		string encodedName = EncoedeString (username);
		string encodedsecondaryName = EncoedeString (secondaryName);

        FreakApi api = new FreakApi ("user.saveUserInfo", ServerResponseForSaveUserInfoAPICall);
        api.param ("userId", FreakAppManager.Instance.userID);
		api.param ("name", encodedName);
        api.param ("country", FreakAppManager.Instance.countryId);
        api.param ("contact_number", FreakAppManager.Instance.mobileNumber);
		api.param ("secondary_name", encodedsecondaryName);
        api.param ("gender", geneder);
		api.param ("remove_profile_pic", coverPicStatus);
		api.param ("remove_cover_pic", profilePicStatus);

        api.get (this);

        FreakAppManager.Instance.username = username;
        FreakAppManager.Instance.secondaryUsername = secondaryName;
	}


	void ServerResponseForSaveUserInfoAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
        Debug.Log("ServerResponseForSaveUserInfoAPICall " + text);
        // check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
					/**
						 * Successfull Login... Load Game..
					*/
					//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
					if (texture != null)
					{
						addProfileImageAPIController.UpdateProfilePicture (fileByte);
					}
                    
                    SaveFreakPlayerInfo ();
                    FindObjectOfType<UserProfileUIController> ().PushPanel (UserProfileUIController.PanelType.TutorialPanel);

					break;
				case 207:
					/**
						* User email id Does not Exists.. U need to signIn first...
			 		*/
					break;
				default:
					/**
						 * Show Popup to let user know if anything has gone wrong while login in...
					*/
					break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}

        FindObjectOfType<UserProfileUIController> ().loadingObj.HideLoading ();
	}

	public string EncoedeString(string inputstring)
	{
		return WWW.EscapeURL (inputstring);
	}


	public void SaveFreakPlayerInfo()
	{
        /*PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.LoginUserId, FreakAppManager.Instance.userID.ToString());
        PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.LoginAccessToken, FreakAppManager.Instance.APIkey);
		PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.UserCountryId, FreakAppManager.Instance.countryId.ToString());
		PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.UserMobileNumber, FreakAppManager.Instance.mobileNumber);*/

		Debug.Log ("access_token ---- > "+  PlayerPrefs.GetString ("access_token"));

        FreakAppManager.Instance.SaveUserInfoPlayerPrefs ();
	}

    public void DisableInputField()
    {
        nameInput = nameInputField.text;
        gameNameinput = gameNameInputFieled.text;

        nameInputField.gameObject.SetActive(false);
        gameNameInputFieled.gameObject.SetActive(false);
    }


	public void HideInputField(bool isHide)
	{
		nameInput = nameInputField.text;
		gameNameinput = gameNameInputFieled.text;
		if (isHide) {
			nameNativeField.SetVisible (false);
			gameNameNativeFieled.SetVisible(false);
		}
		else {
			nameNativeField.SetVisible (true);
			gameNameNativeFieled.SetVisible(true);
			//nameInputField.gameObject.SetActive(true);
			//gameNameInputFieled.gameObject.SetActive(true);
		}
	}
}
