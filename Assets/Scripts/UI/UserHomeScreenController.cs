﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Freak.Chat;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Freak;

public class UserHomeScreenController : MonoBehaviour {

	public Text NameText;
	public Text gameText;
	public Text titleText;
    public Text chatText;
	public GameObject chatTextCountObj;
	public Text gamesNumberText;
	public Text totalScoreText;
	public Text freaksText;

    public Image profileCoverImg;
	public RawImage profileCoverRawImg;
	public Image profileImage;
	public HomeScreenUIPanelController homeScreenUIPanelController;
	public AddProfileImageAPIController addProfileImageAPIController;
//	public GameObject optionsPanel;
	public GameObject loadingObj;

	public GameObject chatButtonObj;
	public Texture2D texture;

    public ScrollRect dashboardScrollRect;
    public GameDetailsDisplayContent gamesItemPrefab;

    public ScrollRect gamesPanelScrollRect;
	public GameDetailsDisplayComponent gamesPrefab;
	public List<GameObject> gamesObjectList = new List<GameObject>();
	public ImageController imageController;
	private UIController uiController;
	private StoreInventory storeInventory;

	public Sprite defaultProfileSprite;
	public Sprite defaultCoverPicSprite;

	enum OptionsPanelStatus
	{
		OptionsPanelDisplayed,
		NotDisplayed
	}
	OptionsPanelStatus optionsPanelStatus;
    int newMessage = 0;
	public enum SlideStatus{
		Straight,
		Right,
		Left
	}
	public SlideStatus slideStatus;
	public Transform panelPos;
	public bool isSlide;
	public bool isDataSaved;

	void Start()
	{
//		FreakAppManager.Instance.myUserProfile.userPic = null;
//		FreakAppManager.Instance.myUserProfile.userCoverPic = null;
	}

    /*void InitUserName (string username)
    {
        NameText.text = username;
        NameText.rectTransform().parent.rectTransform().sizeDelta = new Vector2(NameText.preferredWidth + 1f, NameText.preferredHeight + 1f);
        Color color = NameText.rectTransform().parent.GetComponent<Image>().color;
        color.a = 0.4f;
        NameText.rectTransform().parent.GetComponent<Image>().color = color;
    }*/

    /*void InitGameName (string gamename)
    {
        gameText.text = gamename;
        gameText.rectTransform().parent.rectTransform().sizeDelta = new Vector2(gameText.preferredWidth + 1f, gameText.preferredHeight + 1f);
        Color color = gameText.rectTransform().parent.GetComponent<Image>().color;
        color.a = 0.4f;
        gameText.rectTransform().parent.GetComponent<Image>().color = color;
    }*/

	void OnEnables()
	{
		ShowOnEnable ();
	}

    void OnDisables ()
    {
        isSlide = false;
        gameObject.SetActive(false);
        //this.rectTransform().localPosition = panelPos.rectTransform().localPosition;
    }

	void ShowOnEnable()
	{
        /*if (FreakAppManager.Instance.myUserProfile.userPic != null)
        {
            SetImage(FreakAppManager.Instance.myUserProfile.userPic);
        }
        if (FreakAppManager.Instance.myUserProfile.userCoverPic != null)
        {
            HandleStreamSavedCallbackMethodhod(FreakAppManager.Instance.myUserProfile.userCoverPic);
        }*/

        if (SettingsManager.Instance.isDisableFreaks)
		{
			AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.disableFreaks);
			AlertMessege.Instance.yesButton.onClick.AddListener(OnClickYes);
			AlertMessege.Instance.noButton.onClick.AddListener(OnClickNo);
		}

		isSlide = false;
		this.gameObject.GetComponent<RectTransform> ().position = panelPos.position;
		chatText.text = string.Empty;
		chatTextCountObj.gameObject.SetActive (false);
		newMessage = 0;

		if (!FreakAppManager.Instance.showAllChats)
		{
			optionsPanelStatus = OptionsPanelStatus.OptionsPanelDisplayed;	
			//OnClickOptionsButton ();

            CheckServerInventoryForGame();
            GetSavedUserDetailsInfo (FreakPlayerPrefs.GetSavedUserProfile);
			GetUserInfoAPICall ();

		} 
		else {
			homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
			FreakAppManager.Instance.showAllChats = false;
		}

		CheckIfAllChatsToBeLoaded ();

		if(ChatManager.Instance.IsSeandbirdLogined == false)
			ChatManager.Instance.LoginToSendbird();

		ChatManager.Instance.newMessageUpdateCallback = NewUpdatesCallback;
		//Invoke("InvokeCheckForNewMessages", 5f);
		chatButtonObj.SetActive (true);
		CheckIfChatsAreLoaded ();

		if (imageController == null) {
			imageController = FindObjectOfType<ImageController> ();
		}

        LoadForCount();
	}

    int serverInventoryCalled = 3;
    void CheckServerInventoryForGame ()
    {
        serverInventoryCalled++;
        if (serverInventoryCalled > 3)
        {
            FreakAppManager.Instance.inventoryGamesList = new List<StoreGames>(FreakPlayerPrefs.GetGameInventoryList);

            Debug.Log("inventoryGamesList.Count " + FreakAppManager.Instance.inventoryGamesList.Count);

            if (FreakAppManager.Instance.inventoryGamesList.Count > 0)
            {
                InstantiateStickers();
            }

            if (storeInventory == null)
            {
                storeInventory = FindObjectOfType<StoreInventory>();
            }
//            storeInventory.GetStorePurchasedListAPICall("3", EnableTableView);
            storeInventory.GetStoreInventoryListAPICall("3", EnableTableView);

            serverInventoryCalled = 0;
        }
    }

	void OnClickYes()
	{
		SettingsManager.Instance.isDisableFreaks = false;

        Freak.Chat.ChatManager.Instance.InitSendBird();
        Freak.Chat.ChatManager.Instance.LoginToSendbird();
        Freak.Chat.ChatManager.Instance.Start();

        SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.FreakHomeScene);
	}

	void OnClickNo()
	{
		Application.Quit ();
	}

    System.Action OnCompleteSwipeHandler;
    public void SlideScreen(SlideStatus status, System.Action onCompleteAction)
	{
        OnCompleteSwipeHandler = onCompleteAction;

		Vector3 _newPosition = homeScreenUIPanelController.panaelPosObj.localPosition;
        if (status == SlideStatus.Right) 
        {
            isSlide = true;
            _newPosition.x += this.rectTransform().rect.width;
            LeanTween.move (this.rectTransform(), _newPosition, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete(OnDisables);
            slideStatus = SlideStatus.Right;
        } 
        else if (status == SlideStatus.Left) 
        {
            isSlide = true;
            _newPosition.x -= this.rectTransform().rect.width;
            LeanTween.move (this.rectTransform(), _newPosition, 0.4f).setEase (LeanTweenType.easeInSine).setOnComplete(OnDisables);
            slideStatus = SlideStatus.Left;
        } 
        else if (status == SlideStatus.Straight) {

            isSlide = true;
            if (slideStatus == SlideStatus.Right)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("Move from LEFT");
                #endif
                _newPosition.x += this.rectTransform().rect.width;
            }
            else if (slideStatus == SlideStatus.Left)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("Move from RIGHT");
                #endif
                _newPosition.x -= this.rectTransform().rect.width;
            }

            this.rectTransform().localPosition = _newPosition;
            LeanTween.move(this.rectTransform(), panelPos.rectTransform().localPosition, 0.3f).setEase(LeanTweenType.easeInSine).setOnComplete(() =>
                {
                    if (OnCompleteSwipeHandler != null)
                        OnCompleteSwipeHandler();
                });

            slideStatus = SlideStatus.Straight;
//            this.transform.position = panelPos.position;
//            slideStatus = SlideStatus.Straight;
//            OnEnables ();
        }

        /*if (slideStatus == SlideStatus.Right) 
		{
			isSlide = true;
			this.transform.localPosition = new Vector3 (800, 0, 0);
			LeanTween.move (this.gameObject, panelPos.position, 0.8f).setEase (LeanTweenType.easeInSine).setOnComplete(OnEnables);
			slideStatus = SlideStatus.Straight;
		} 
		else if (slideStatus == SlideStatus.Left) 
		{
			isSlide = true;
			this.transform.localPosition = new Vector3 (-800, 0, 0);
			LeanTween.move (this.gameObject, panelPos.position, 0.8f).setEase (LeanTweenType.easeInSine).setOnComplete(OnEnables);
			slideStatus = SlideStatus.Straight;
		} 
		else {
			
			this.transform.position = panelPos.position;
			slideStatus = SlideStatus.Straight;
			OnEnables ();
		}*/
	}

    private bool showLoader = true;
	void OnEnable()
	{
        if (showLoader)
        {
            showLoader = false;
            DisplayLoader(true);
        }
        /* if (FreakAppManager.Instance.myUserProfile.userPic != null)
        {
            SetProfileImage(FreakAppManager.Instance.myUserProfile.userPic);
        }
        if (FreakAppManager.Instance.myUserProfile.userCoverPic != null)
        {
            HandleStreamSavedCallbackMethodhod(FreakAppManager.Instance.myUserProfile.userCoverPic);
        }*/
		Invoke ("InitialCall", FreakAppManager.Instance.timeToInit);
	}


	void InitialCall()
	{
		this.rectTransform().localPosition = homeScreenUIPanelController.panaelPosObj.rectTransform().localPosition;
//		if (FreakPlayerPrefs.GetSavedUserProfile != null)
//		{
//			GetSavedUserDetailsInfo (FreakPlayerPrefs.GetSavedUserProfile);
//		}

//		if(this.gameObject.activeSelf)
		OnEnables ();
	}
	
    void CheckIfAllChatsToBeLoaded()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("CheckIfAllChatsToBeLoaded.......");
        #endif

		if (FreakAppManager.Instance.isLoadAllChats) 
		{
            UIController.Instance.connectingUI.DisplayLoading ();

            if (PlayerPrefs.HasKey("DirectFileSend"))
            {
                ChatManager.Instance.DirectFileSendPathHandler(PlayerPrefs.GetString("DirectFileSend"));
            }
		}
        else
        {
            #if UNITY_EDITOR

                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("CheckIfAllChatsToBeLoaded");
                #endif

            #else
                #if !DIRECT_BUILD
                PushNotification.GetNotificationChannelUrl();
                #endif
            #endif

            if (PlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.LocalNotificationFromDifferentScene))
            {
                long _messageID = long.Parse(PlayerPrefs.GetString(FreakAppConstantPara.PlayerPrefsName.LocalNotificationFromDifferentScene));
                PlayerPrefs.DeleteKey(FreakAppConstantPara.PlayerPrefsName.LocalNotificationFromDifferentScene);
                ChatManager.Instance.OnReceiveNotification(_messageID);
            }
        }
	}

	public void OnClickChangeCoverPic()
	{
		Debug.Log ("selet cover pic");
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.CoverPicSelectionPanel);
	}


	public void AddPhoto(int selectedName)
	{
		if (selectedName < 3) 
		{
            if (InternetConnection.Check() == false)
            {
                Freak.AlertMessege.Instance.ShowAlertWindow (Freak.AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
                return;
            }

			#if UNITY_EDITOR
			string _path = Application.persistentDataPath + "/group.jpg";
			if (System.IO.File.Exists(_path))
			{
                Debug.Log(_path);
                SetCoverImage(_path);
			}
			else
			{
				Debug.LogError(_path);
			}
			#else
            UIController.Instance.GetProfileImage (SetCoverImage, selectedName);
			#endif
		} 
		else {
			if (selectedName == 3) 
			{
				CloseOptions ();
			}
			else 
			{
				DeleteCoverPic ();
			}
		}
	}

	public void CloseOptions()
	{
		homeScreenUIPanelController.PopPanel ();
	}


	public void DeleteCoverPic()
	{
        if (InternetConnection.Check() == false)
        {
            Freak.AlertMessege.Instance.ShowAlertWindow (Freak.AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.PopUpMessage.internetConnection);
            return;
        }

        FreakAppManager.Instance.myUserProfile.cover_image = string.Empty;
		FreakAppManager.Instance.myUserProfile.userCoverPic = null;
        profileCoverImg.FreakSetSprite(FreakAppManager.Instance.defaultCoverTexture);// = defaultCoverPicSprite;
		addProfileImageAPIController.SaveUserInfoAPICall (FreakAppManager.Instance.myUserProfile.name, FreakAppManager.Instance.myUserProfile.secondary_name, FreakAppManager.Instance.myUserProfile.gender, 0, 1);
		CloseOptions ();
        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
	}


    void SetCoverImage(string path)
	{
		texture = null;
		byte[] fileByte = System.IO.File.ReadAllBytes(path);
		texture = new Texture2D(1, 1);
		texture.LoadImage(fileByte);

        if (texture.height > 1000 || texture.width > 1000)
        {
            texture = TextureResize.ResizeTexture(1000f, 1000f, texture);
            fileByte = texture.EncodeToPNG();
        }

		AssignImage (texture, fileByte);

        Freak.StreamManager.SaveFile("FreakAppManager.Instance.myUserProfile.cover_image.png", fileByte, FolderLocation.Profile, null, false);
	}

	void AssignImage (Texture2D tex, byte[] fileImgByte = null)
	{
		CloseOptions ();
		texture = tex;
		if (texture != null) 
		{
            UpdateCoverImageTexture(texture);
//            Rect rec = new Rect (0, 0, texture.width, texture.height);
//            Sprite spr = Sprite.Create (texture, rec, new Vector2 (0, 0), 1);
//            profileCoverImg.sprite = spr;
//            FreakAppManager.Instance.myUserProfile.userCoverPic = texture;

			if (fileImgByte != null) 
			{
				FindObjectOfType<AddProfileImageAPIController> ().UpdateCoverProfilePicture (fileImgByte, texture);
			}
		}
	}

    /*void InvokeCheckForNewMessages ()
    {
        if (this.gameObject.activeInHierarchy == false)
        {
            Invoke("InvokeCheckForNewMessages", 5f);
            return;
        }

        if (ChatManager.Instance.IsSeandbirdLogined == false)
        {
            if (ChatManager.Instance.IsInitLogin == false)
                ChatManager.Instance.LoinToSendbird();

            return;
        }

        Debug.Log ("ChatManager.Instance.isGettingChannelList " + ChatManager.Instance.isGettingChannelList);
        if (ChatManager.Instance.isGettingChannelList)
        {
            Invoke("InvokeCheckForNewMessages", 20f);
            return;
        }

        if (checkMessageRoutine != null)
            StopCoroutine(checkMessageRoutine);

        checkMessageRoutine = CheckNewMessageRoutine();
        StartCoroutine(checkMessageRoutine);
    }*/

    /*IEnumerator checkMessageRoutine;
    IEnumerator CheckNewMessageRoutine ()
    {
        yield return new WaitForSeconds(2f);
        ChatManager.Instance.RefreshForNewMessages();
    }*/

    void NewUpdatesCallback()
    {
        //Invoke("InvokeCheckForNewMessages", 40f);
        if (ChatManager.Instance.newTotalChats > newMessage)
        {
            newMessage = ChatManager.Instance.newTotalChats;
//          chatText.text = "<color=black>" + newMessage.ToString() + "</color>";
			chatText.text = newMessage.ToString ();
			chatTextCountObj.gameObject.SetActive (true);
            Debug.Log("New Messages: Total unread messages " + newMessage);
        }
		DisplayLoader(false);
    }

	void CheckIfChatsAreLoaded()
    {
        chatButtonObj.SetActive (false);
        if (FreakAppManager.Instance.isLoadAllChats) 
        {
            homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
            FreakAppManager.Instance.isLoadAllChats = false;
            UIController.Instance.connectingUI.gameObject.SetActive (false);
        }
        NewUpdatesCallback ();
	}

	public void OnClickChatButton()
	{
        homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
	}


	public void OnClickNewsFeedButton()
	{
		//FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.NotificationsPanel);
		FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.NewsFeedPanel);
	}


	public void OnClickOptionsButton()
	{
//		if(optionsPanelStatus == OptionsPanelStatus.NotDisplayed)
//		{
//			optionsPanel.SetActive (true);
//			optionsPanelStatus = OptionsPanelStatus.OptionsPanelDisplayed;
//		}
//		else{
//			optionsPanel.SetActive (false);
//			optionsPanelStatus = OptionsPanelStatus.NotDisplayed;		
//		}
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}


	public void OnClickGamesButton()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.GameHistoryPanel);
	}


	public void OnClickFreaksButton()
	{
		//SceneManager.LoadScene("FreaksScene");
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.freakPanel);
	}
		


	public void OnClickProfile()
	{
		homeScreenUIPanelController.PushPanel (HomeScreenUIPanelController.PanelType.UserProfilePanel);
		//GameObject obj =  homeScreenUIPanelController.GetPushedPanel (HomeScreenUIPanelController.PanelType.UserProfilePanel);
		//obj.GetComponent<UserProfileUI> ().InitProfilePic ();

	}


	public void OnClickCoverPicProfile()
	{
		GameObject obj =  homeScreenUIPanelController.GetPushedPanel (HomeScreenUIPanelController.PanelType.UserProfilePanel);
		//obj.GetComponent<UserProfileUI> ().isCoverPic = true;
		//obj.GetComponent<UserProfileUI> ().InitCoverPic();
	}


	void OnDisable()
	{
        ChatManager.Instance.newMessageUpdateCallback = null;
		Resources.UnloadUnusedAssets ();
    }

    void OnDestroy ()
    {
        //ChatManager.Instance.newMessageUpdateCallback = null;
    }


	void DisplayLoader(bool isDisplay)
	{
		if (isDisplay) {
			loadingObj.SetActive (true);
			FreakAppManager.Instance.isLoaderOn = true;
		} else {
			loadingObj.SetActive (false);
			FreakAppManager.Instance.isLoaderOn = false;
		}

	}

	/// <summary>
	/// Get User information
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetUserInfoAPICall()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Call server to fetch info");
        #endif
		
        DisplayLoader(true);
        SaveUserDetailsInfo (FreakAppManager.Instance.myUserProfile);

//		if(!isDataSaved)
		{
			FreakApi api = new FreakApi ("user.getUserInfo", ServerResponseForGetUserInfoAPICall);
			api.param ("other_user_id", "0");
			api.get (this);
		} 
//		else {
//			SaveUserDetailsInfo (FreakAppManager.Instance.myUserProfile);
//		}
	}
		
	void ServerResponseForGetUserInfoAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
                case 001:
				/**
						
				*/
                IDictionary userDetailsDict = (IDictionary)output["responseMsg"];
                MyUserProfile myUserProfile = new MyUserProfile(userDetailsDict);
                isDataSaved = true;
                SaveUserDetailsInfo(myUserProfile);
                if (InternetConnection.Check())
                {
                    if (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
                        Freak.Chat.ChatManager.Instance.LoginSendbirdChat();
                }
                else
                {
                    Debug.LogError("No Internet Connection!");
                }
                break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
				*/
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			if (InternetConnection.Check ()) 
			{
                DisplayLoader(false);
            } 
            else 
            {
                Debug.LogError (" 5555555555555555555 No Internet Connection!");
                GetSavedUserDetailsInfo (FreakPlayerPrefs.GetSavedUserProfile);

            }
            ////inapi.get(this);
        }
	}


	void SaveUserDetailsInfo(MyUserProfile myUserProfile)
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("SaveUserDetailsInfo ------>>>>> " + Time.realtimeSinceStartup);
        #endif
        GetSavedUserDetailsInfo(myUserProfile);

        FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
	}


	void GetSavedUserDetailsInfo(MyUserProfile myUserProfile)
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("GetSavedUserDetailsInfo ------>>>>> " + Time.realtimeSinceStartup);
        #endif

        if (myUserProfile != null)
        {
            FreakAppManager.Instance.myUserProfile = myUserProfile;
            LoadImage();
            LoadCoverImage();

            NameText.FreakSetTextBackground(FreakAppManager.Instance.myUserProfile.name, 0.4f);
            gameText.FreakSetTextBackground(FreakAppManager.Instance.myUserProfile.secondary_name, 0.4f);
//          InitUserName(FreakAppManager.Instance.myUserProfile.name);
//          InitGameName(FreakAppManager.Instance.myUserProfile.secondary_name);
        
            if (FreakAppManager.Instance.myUserProfile.currentTile != null)
            {
                titleText.text = FreakAppManager.Instance.myUserProfile.currentTile.userTitle;
                titleText.FreakSetTextBackground(FreakAppManager.Instance.myUserProfile.currentTile.userTitle, 0.4f);
            }

            Freak.Chat.ChatManager.Instance.UserId = FreakAppManager.Instance.userID.ToString();
            Freak.Chat.ChatManager.Instance.UserName = FreakAppManager.Instance.myUserProfile.name;
            DisplayLoader(false);
        }
	}


//	IEnumerator StartLoadingProfilrImage(string url) 
//	{
//		WWW www = new WWW(url);
//		yield return www;
//		if (www.error != null)
//		{
//			Debug.Log ("Error --->" + www.error);
//		} 
//		else {
//			Debug.Log ("Error --->" + www.text);
//			FreakAppManager.Instance.profileImg = www.texture as Texture2D;
//			SetImage ();
//		}
//	}


    void LoadImage()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("LoadImage ------>>>>> " + Time.realtimeSinceStartup);
        #endif

        if (string.IsNullOrEmpty (FreakAppManager.Instance.myUserProfile.avatar_url))
        {
            profileImage.FreakSetSprite(FreakAppManager.Instance.defaultProfileTexture);// .sprite = defaultProfileSprite;
            FreakAppManager.Instance.myUserProfile.userPic = FreakAppManager.Instance.defaultProfileTexture;
            return;
        }

        if (FreakAppManager.Instance.myUserProfile.userPic != null)
        {
            SetProfileImage(FreakAppManager.Instance.myUserProfile.userPic);
        }

        FreakAppManager.Instance.myUserProfile.userPic = DownloadManager.Instance.GetProfileTexture2DFromCache(FreakAppManager.Instance.myUserProfile.avatar_url, FolderLocation.Profile, null);

        if (FreakAppManager.Instance.myUserProfile.userPic == null)
        {
//          DownloadManager.Instance.DownLoadWallpaper(FreakAppManager.Instance.myUserProfile.avatar_url, FolderLocation.Profile, SetProfileImage);
            Invoke("LoadImage", 0.5f);
        }
        else
        {
            SetProfileImage(FreakAppManager.Instance.myUserProfile.userPic);
        }

//		if (!FreakAppManager.Instance.myUserProfile.avatar_url.Equals (FreakAppManager.Instance.tempavatar_url)) 
//		{
//			imageController.GetImageFromLocalOrDownload (url, profileImage, Freak.FolderLocation.Profile, SetImage);
//		}
//		else 
//		{
//			if (!FreakAppManager.Instance.myUserProfile.cover_image.Equals (FreakAppManager.Instance.tempcover_image)) 
//			{
//				FreakAppManager.Instance.myUserProfile.cover_image = FreakAppManager.Instance.tempcover_image;
//				LoadCoverImage (FreakAppManager.Instance.myUserProfile.cover_image);
//			}
//		}
	}

    void SetProfileImage(Texture2D tex)
	{
        if (tex)
        {
            /*if (tex.width > 200 || tex.height > 200)
            {
                tex.Compress(false);
            }*/
            
//          FreakAppManager.Instance.myUserProfile.userPic = tex;
            FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
            
            profileImage.FreakSetSprite (tex);
        }
        else
        {
            Invoke("LoadImage", 2f);
        }
        //byte[] bytes = tex.EncodeToPNG();

//      FreakAppManager.Instance.userProfileImage = TextureResize.ResizeTexture (150f, 150f, tex).EncodeToPNG ();

//		if (!FreakAppManager.Instance.myUserProfile.cover_image.Equals (FreakAppManager.Instance.tempcover_image)) 
//		{

//		LoadCoverImage (FreakAppManager.Instance.myUserProfile.cover_image);
		
//      LoadCoverImage();
	}

    void LoadCoverImage()//(string url)
	{
//		imageController.GetImageFromLocalOrDownload (url, profileCoverImg, Freak.FolderLocation.Profile, SetCoverImage);

        if (string.IsNullOrEmpty (FreakAppManager.Instance.myUserProfile.cover_image))
        {
            profileCoverImg.FreakSetSprite(FreakAppManager.Instance.defaultCoverTexture);//.sprite = defaultCoverPicSprite;
            return;
        }

        if (FreakAppManager.Instance.myUserProfile.userCoverPic == null)
        {
            if (System.IO.Path.GetFileName(FreakAppManager.Instance.myUserProfile.cover_image).Contains("cover_image.png"))
            {
                UpdateCoverImageTexture(FreakAppManager.Instance.defaultCoverTexture);
            }
            else
            {
                DownloadManager.Instance.DownLoadWallpaper (FreakAppManager.Instance.myUserProfile.cover_image, FolderLocation.Profile, UpdateCoverImageTexture);
            }

        } 
        else
        {
            UpdateCoverImageTexture (FreakAppManager.Instance.myUserProfile.userCoverPic);
        }
	}

    void UpdateCoverImageTexture (Texture2D texture)
    {
        if (texture != null)
        {
            texture.Compress(false);
            FreakAppManager.Instance.myUserProfile.userCoverPic = texture;
            profileCoverImg.FreakSetSprite (texture);
        }
        else
        {
            Invoke("LoadCoverImage", 2f);
        }
    }

//	void SetProfileImage(Texture2D newtex)
//	{
//        //byte[] newbytes = newtex.EncodeToPNG();
////        FreakAppManager.Instance.myUserProfile.userPic = newtex;
////		FreakAppManager.Instance.myUserProfile.userCoverPic = newtex;
////		FreakPlayerPrefs.GetSavedUserProfile = FreakAppManager.Instance.myUserProfile;
////		Rect rec = new Rect(0, 0, newtex.width, newtex.height);
////        Sprite spr = Sprite. Create(newtex,rec,new Vector2(0,0),1);
////        profileImage.sprite = spr;
//        SetImage(newtex);
//	}


//	void SetImageUsingByteData(byte[] fileByte)
//	{
//		Texture2D tex = new Texture2D (4, 4, TextureFormat.ARGB32, false);
//		tex.LoadImage (fileByte);
//		Rect rec = new Rect(0, 0, tex.width, tex.height);
//		Sprite spr = Sprite. Create(FreakAppManager.Instance.myUserProfile.userPic,rec,new Vector2(0,0),1);
//		profileImage.sprite = spr;
//		//profileCoverImg.sprite = spr;
//	}


	public void EnableTableView(List<object> gamesList)
	{
        FreakAppManager.Instance.inventoryGamesList.Clear ();

        for (int i = 0; i < gamesList.Count; i++) 
        {
            Dictionary<string,object> data = (Dictionary<string,object>)gamesList [i];
            StoreGames storeGames = new StoreGames (data);
            FreakAppManager.Instance.inventoryGamesList.Add (storeGames);
            FreakAppManager.Instance.inventoryGamesList[i].purchase_status = FreakAppManager.IsInventoryPurchased(FreakAppManager.Instance.inventoryGamesList[i].inventory_id);
        }

        FreakPlayerPrefs.GetGameInventoryList = FreakAppManager.Instance.inventoryGamesList;

        InstantiateStickers ();

	}

    /*void DummyGameList ()
    {
        if (FreakAppManager.Instance.inventoryGamesList.Count > 0)
        {
            InstantiateStickers ();
            return;
        }
        
        for (int i = 0; i < 3; i++)
        {
            StoreGames _storeGames = new StoreGames();
            _storeGames.type = "3";
            _storeGames.purchase_status = true;
            _storeGames.referralCount = 0;
            
            if (i==0)
            {
                _storeGames.title = "TicTacToe";
                _storeGames.inventory_description = "Game of Tic and Tac having Toe.";
                _storeGames.inventory_id = "5";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/f5824f731e6c882b771161624ee71824.png";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }
            else if (i==1)
            {
                _storeGames.title = "Quiz";
                _storeGames.inventory_description = "Game of Questions and Answers";
                _storeGames.inventory_id = "7";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/01f72d8b4cd43e9bed8cfcb55520e09f.png";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }
            else if (i==2)
            {
                _storeGames.title = "SandCastle";
                _storeGames.inventory_description = "Game is about building castles on Sand.";
                _storeGames.inventory_id = "6";
                _storeGames.description_imagePath = "https://s3-ap-southeast-1.amazonaws.com/juego-sample/FREAK-CHAT/inventory-image/aec2be3c5e49269714338d7c876732e6.jpg";
                _storeGames.descImageName = "f5824f731e6c882b771161624ee71824.png";
            }
            
            FreakAppManager.Instance.inventoryGamesList.Add(_storeGames);
            
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("inventoryGamesList " + FreakAppManager.Instance.inventoryGamesList.Count);
            #endif
        }

            InstantiateStickers ();
    }*/


	void InstantiateStickers ()
	{
		if (gamesObjectList != null)
		{
            if (gamesObjectList.Count > 0)
                return;

			for (int i = 0; i < gamesObjectList.Count; i++)
			{
				if (gamesObjectList[i] != null)
				{
					Destroy(gamesObjectList[i]);
				}
			}
			gamesObjectList.Clear();
		}

		gamesObjectList = new List<GameObject>();

		//string stickerName = UIController.Instance.assetsReferences.stickers[categoryId].name;
		//List<Sprite> stickerList = UIController.Instance.assetsReferences.stickers[categoryId].stickersList;

        for (int i = 0; i < FreakAppManager.Instance.inventoryGamesList.Count; i++)
        {
            GameDetailsDisplayComponent gObject = GetInstantiateObject ();
            gObject.gameObject.SetActive(true);
            gObject.InitData(FreakAppManager.Instance.inventoryGamesList[i]);

//            gObject.gameObject.transform.SetParent(gamesPanelScrollRect.content.transform, false); 
//            gamesObjectList.Add(gObject.gameObject);
        }

        /*for (int i = 0; i < FreakAppManager.Instance.inventoryGamesList.Count; i++)
		{
			GameDetailsDisplayComponent gObject = Instantiate (gamesPrefab) as GameDetailsDisplayComponent;
			
            gObject.InitData(FreakAppManager.Instance.inventoryGamesList[i]);
            gObject.gameObject.transform.SetParent(gamesPanelScrollRect.content.transform, false); 
			gamesObjectList.Add(gObject.gameObject);
		}*/
	}

    GameDetailsDisplayComponent GetInstantiateObject ()
    {
        GameDetailsDisplayComponent _item = null;

        if (gamesObjectList.Count > 0)
        {
            _item = gamesObjectList[gamesObjectList.Count - 1].GetComponent<GameDetailsDisplayContent>().GetItem();
        }

        if (_item == null)
        {
            GameDetailsDisplayContent gObject = Instantiate (gamesItemPrefab) as GameDetailsDisplayContent;
            gObject.gameObject.transform.SetParent(dashboardScrollRect.content.transform, false); 
            gObject.gameObject.SetActive(true);
            gamesObjectList.Add(gObject.gameObject);
            _item = gObject.GetItem();
        }
        /*else
        {
            GameDetailsDisplayContent gObject = Instantiate (gamesItemPrefab) as GameDetailsDisplayContent;
            gObject.gameObject.transform.SetParent(dashboardScrollRect.content.transform, false); 
            gamesObjectList.Add(gObject.gameObject);
            _item = gObject.GetItem();
        }*/

        return _item;
    }

    #region LOAD COUNT

    void LoadForCount ()
    {
        FreakServer.Instance.gameHistoryStatusList = new List<GameStatus>(FreakPlayerPrefs.GetGameHistoryStatusList);
        FreakServer.Instance.GetGameHistoryAPICall(GetGameHistoryAPIHandler);
        FreakAppManager.Instance.LoadServerContactList (null);

        ShowGameHistoryCount();
        ShowGameScoreCount();
        ShowFreakContactCount();
    }

    void GetGameHistoryAPIHandler (List<GameStatus> obj)
    {
        ShowGameHistoryCount();
    }

    void ShowGameScoreCount ()
    {
        totalScoreText.text = FreakAppManager.Instance.myUserProfile.totalScore.ToString();
    }

    void ShowGameHistoryCount ()
    {
//        gamesNumberText.text = inventoryGamesList.Count.ToString();
        gamesNumberText.text = FreakServer.Instance.gameHistoryStatusList.Count.ToString();
    }

    void ShowFreakContactCount ()
    {
        freaksText.text = FreakAppManager.Instance.GetAllServerUserInfo().Count.ToString();
    }

    #endregion

	public void DeletePlayerPrefs()
	{
		PlayerPrefs.DeleteAll ();
        CacheManager.DeleteData ();
        FreakAppManager.Instance.userInfoPlayerPrefs = null;
        SceneManager.LoadScene (FreakAppConstantPara.UnitySceneName.SplashScene);

        #if UNITY_ANDROID && !UNITY_EDITOR
        PluginMsgHandler.getInst ().ResetSoftInputstyle ();
        #endif
	}

//    public Text statusBarTestText;
//
//    public void OnClickCheckStatusBarHeight ()
//    {
//        statusBarTestText.text = "TESTING " + FreakAppManager.GetStatusBarHeight().ToString();
//        ChatManager.Instance.TestUnRegisterAllPushNotification();
//    }
}
