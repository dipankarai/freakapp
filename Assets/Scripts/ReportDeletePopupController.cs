﻿using UnityEngine;
using System.Collections;
using Freak;

public class ReportDeletePopupController : MonoBehaviour {

	public Transform reportDeleteObj;
	public string newsFeedID;
	public GameObject reprotBtn;
	public GameObject deleteBtn;

	public void EnableReport()
	{
		reprotBtn.SetActive(true);
		deleteBtn.SetActive(false);
	}

	public void EnableDelete()
	{
		deleteBtn.SetActive(true);
		reprotBtn.SetActive(false);				
	}


	public void OnClickReport()
	{
		AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton, FreakAppConstantPara.MessageDescription.reportNewsFeed);
		AlertMessege.Instance.yesButton.onClick.AddListener(OnClickYes);
		AlertMessege.Instance.noButton.onClick.AddListener(OnClickNo);
	}

	void OnClickYes()
	{
		FindObjectOfType<NewsFeedApiController> ().ReportNewsFeedAPICall (newsFeedID, "");
		Destroy (this.gameObject);
	}

	void OnClickNo()
	{
		Destroy (this.gameObject);
	}


	public void OnClickDelete()
	{
		AlertMessege.Instance.ShowAlertWindow (AlertMessege.AlertType.MessegeWithYesNoButton,  FreakAppConstantPara.MessageDescription.deleteNewsFeed);
		AlertMessege.Instance.yesButton.onClick.AddListener(OnClickDeleteYes);
		AlertMessege.Instance.noButton.onClick.AddListener(OnClickDeleteNo);
	}


	void OnClickDeleteYes()
	{
		FindObjectOfType<NewsFeedApiController> ().DeleteNewsFeedAPICall (newsFeedID);
		Destroy (this.gameObject);
	}

	void OnClickDeleteNo()
	{
		Destroy (this.gameObject);
	}


	public void OnClickDestroy()
	{
		Destroy (this.gameObject);
	}

	void OnDisable(){
		Destroy (this.gameObject);
	}
}
