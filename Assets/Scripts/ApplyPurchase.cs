﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ApplyPurchase : MonoBehaviour 
{
	public Text purchaseText;

	//-------------------------- Here write all the actions to perform when PACK is purchased for IOS and ANDROID.
	public void ApplyPurchasedItem(string product)
	{
#if UNITY_IOS
		switch(product)
		{
		case "PACK_0":
			AddCoinsDemo(50);
			break;
			
		case "PACK_1":
			AddCoinsDemo(100);
			break;
			
		case "PACK_2":
			AddCoinsDemo(250);
			break;
			
		case "PACK_3":
			AddCoinsDemo(500);
			break;

		case "PACK_4":
			AddCoinsDemo(1000);
			break;

		case "PACK_5":
			AddCoinsDemo(2000);
			break;
			
		default : 
			AddCoinsDemo(100);
		break;
		}
#endif

#if UNITY_ANDROID
		switch(product)
		{
		case "PACK_0":
			AddCoinsDemo(50);
			break;
			
		case "PACK_1":
			AddCoinsDemo(100);
			break;
			
		case "PACK_2":
			AddCoinsDemo(250);
			break;
			
		case "PACK_3":
			AddCoinsDemo(500);
			break;

		case "PACK_4":
			AddCoinsDemo(1000);
			break;

		case "PACK_5":
			AddCoinsDemo(2000);
			break;

		default :
			AddCoinsDemo(100);
			break;
		}
#endif
	}

	public void AddCoinsDemo(int coinCount)
	{
		purchaseText.text = coinCount.ToString();
		FindObjectOfType<StoreInventory> ().SaveUserCoinsAPICall (coinCount);
	}
}
