﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

[System.Serializable]
public class InAppIDs
{
	public enum InAppType
	{
		Consumable,
		NonConsumable
	};
	
	public InAppType inAppType;
	public string purchaseID;
}

public class IAP : MonoBehaviour, IStoreListener
{
	public List<InAppIDs> AllIosInappIds;
	public List<InAppIDs> AllGoogleInappIds;
	public ApplyPurchase applyPurchase;

	private IStoreController m_Controller;
	private int m_SelectedItemIndex = -1; // -1 == no product
	private bool m_PurchaseInProgress;
	private Selectable m_InteractableSelectable; // Optimization used for UI state management
	
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_Controller = controller;
		foreach (var item in controller.products.all)
		{
			if (item.availableToPurchase)
			{
				//Debug.Log ("item.availableToPurchase-->>" + item.availableToPurchase);
//				Debug.Log(string.Join(" - ",
//					new[]
//					{
//						item.metadata.localizedTitle,
//						item.metadata.localizedDescription,
//						item.metadata.isoCurrencyCode,
//						item.metadata.localizedPrice.ToString(),
//						item.metadata.localizedPriceString
//					}));
			}
		}

		// Prepare model for purchasing
		if (m_Controller.products.all.Length > 0) 
		{
			m_SelectedItemIndex = 0;
		}
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		Debug.Log("purchasedProduct: " +  e.purchasedProduct.definition.id);
		string productIdStr = e.purchasedProduct.definition.id;

		applyPurchase.ApplyPurchasedItem(productIdStr);

		m_PurchaseInProgress = false;
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Debug.Log("Purchase failed: " + item.definition.id);
		Debug.Log(r);
		m_PurchaseInProgress = false;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("Billing failed to initialize!");
		switch (error)
		{
		case InitializationFailureReason.AppNotKnown:
			Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
			break;
		case InitializationFailureReason.PurchasingUnavailable:
			// Ask the user if billing is disabled in device settings.
			Debug.Log("Billing disabled!");
			break;
		case InitializationFailureReason.NoProductsAvailable:
			// Developer configuration error; check product metadata.
			Debug.Log("No products available for purchase!");
			break;
		}
	}

	public void Awake()
	{
#if UNITY_IOS
		InitializePurchaseIDs(AllIosInappIds, AppleAppStore.Name);
#endif

#if UNITY_ANDROID
		InitializePurchaseIDs(AllGoogleInappIds, GooglePlay.Name);
#endif
	}

	void InitializePurchaseIDs(List<InAppIDs> allIDs, string storeType)
	{
		var module = StandardPurchasingModule.Instance();
		module.useMockBillingSystem = true;
		var builder = ConfigurationBuilder.Instance(module);

		for(int i = 0; i < allIDs.Count; i++)
		{
			ProductType productType;
			if(allIDs[i].inAppType != InAppIDs.InAppType.NonConsumable)
			{
				productType = ProductType.Consumable;
			}
			else
			{
				productType = ProductType.NonConsumable;
			}
			builder.AddProduct("PACK_"+i, productType, new IDs
			{
				{allIDs[i].purchaseID, storeType}
			});
		}

		UnityPurchasing.Initialize(this, builder);
	}

	private void OnTransactionsRestored(bool success)
	{
		Debug.Log("Transactions restored.");
	}

	private void OnDeferred(Product item)
	{
		Debug.Log("Purchase deferred: " + item.definition.id);
	}

	public void BuyButton(int itemIndex)
	{
		if (m_PurchaseInProgress == true) {
			return;
		}

		print("m_SelectedItemIndex "+itemIndex + "products -- >"+ m_Controller.products.all.Length);
		m_PurchaseInProgress = true;
		m_Controller.InitiatePurchase(m_Controller.products.all[itemIndex]);
	}
}