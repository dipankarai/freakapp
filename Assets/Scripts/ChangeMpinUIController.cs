﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeMpinUIController : MonoBehaviour {

	public InputField newPinTextInputField;
	public InputField confirmPinTextInputField;

	// Use this for initialization
	void Starts () 
	{
        newPinTextInputField.text = "";
        confirmPinTextInputField.text = "";
	}

	public void SaveNewMPin()
	{
		
		if (!string.IsNullOrEmpty (newPinTextInputField.text) && !string.IsNullOrEmpty (confirmPinTextInputField.text) && newPinTextInputField.text.Equals (confirmPinTextInputField.text)) 
		{

            PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.MPin, confirmPinTextInputField.text);
			FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();

		}
	}

	public void CancelMPIN()
	{
		FindObjectOfType<HomeScreenUIPanelController> ().PopPanel ();
	}

	public void InputFiledCharCount(){
		Debug.Log ("Count");
	}

	void OnDisable()
	{
		Debug.Log (" ******************** Destroy Change Mpin");
		Destroy (this.gameObject);
	}
}
