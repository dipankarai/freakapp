﻿using UnityEngine;
using System;
using System.Collections;

public class InternetConnection : MonoBehaviour
{
    private static InternetConnection instance;
    public static InternetConnection Instance
    {
        get
        {
            return Init();
        }
    }

    public enum InternetConnectionState
    {
        UnKnown = -1,
        DisConnnected = 0,
        Connected = 1,
    }

    private static InternetConnectionState internetConnectionState = InternetConnectionState.UnKnown;
    public static bool IsInternetAvailable
    {
        get
        {
            return (internetConnectionState == InternetConnectionState.Connected);
        }

        private set
        {
            if (((internetConnectionState == InternetConnectionState.Connected) && (value == false)) ||
                ((internetConnectionState == InternetConnectionState.DisConnnected) /*&& (value == true)*/) ||
                internetConnectionState == InternetConnectionState.UnKnown) // check latest states are different or same.
            {
                internetConnectionState = (value == true) ? InternetConnectionState.Connected : InternetConnectionState.DisConnnected;
                if (internetConnectionState == InternetConnectionState.Connected)
                {
                    InternetConnectionBack();
                }
                else if (internetConnectionState == InternetConnectionState.DisConnnected)
                {
                    InternetConnectionFailed();
                }
            }
        }
    }

    private const int timeoutValue = 30;
    public const float checkInterval = 10F;
    private const string checkURL = "https://www.google.com";
    WaitForSeconds mShortWaitForSeconds = new WaitForSeconds (2f);
    WaitForSeconds mLongWaitForSeconds = new WaitForSeconds (10f);
    private IEnumerator internetRoutine;

    /// <summary>
    /// callback used On internet connection back.
    /// </summary>
    public Action OnInternetConnectionCallBack;

    /// <summary>
    /// callback used On internet connection back.
    /// </summary>
    public Action OnInternetConnectionBack;

    /// <summary>
    /// callback used On internet connection fail.
    /// </summary>
    public Action OnInternetConnectionFail;

    /// <summary>
    /// callback used On internet connection status updated.
    /// </summary>
    public Action<bool> OnInternetConnectionStatus;

    /// <summary>
    /// Initializes this instance.
    /// </summary>
    /// <returns></returns>
    public static InternetConnection Init()
    {
//        Debug.Log ("********InternetConnection******** " + Time.time);
        if (instance == null)
        {
            GameObject instGO = new GameObject("InternetTester");
            instance = instGO.AddComponent<InternetConnection>();

            DontDestroyOnLoad(instGO);
        }
        return instance;
    }

    /// <summary>
    /// Awake this instance.
    /// Trigger connectionin check in loop
    /// </summary>
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        LoopIntenetConnection();
    }

    /// <summary>
    /// Loops the intenet connection.
    /// </summary>
    void LoopIntenetConnection()
    {
        if (internetRoutine != null)
        {
            StopCoroutine(internetRoutine);
        }
        internetRoutine = CheckInternetConnection(NetworkTestResult);
        StartCoroutine(internetRoutine);
    }

    /// <summary>
    /// Networks the test result.
    /// </summary>
    /// <param name="isConnected">if set to <c>true</c> [is connected].</param>
    void NetworkTestResult(bool isConnected)
    {
        IsInternetAvailable = isConnected;

        if (OnInternetConnectionStatus != null)
        {
            OnInternetConnectionStatus(isConnected);
        }
    }

    /// <summary>
    /// Raises the application pause event.
    /// When the app comes from background check internet immediately...
    /// </summary>
    /// <param name="paused">If set to <c>true</c> paused.</param>
    void OnApplicationPause (bool paused)
    {
        if (paused == false)
        {
            LoopIntenetConnection ();
        }
    }

    /// <summary>
    /// Checks the internet.
    /// </summary>
    /// <returns> returns result </returns>
    public static bool Check()
    {
        return IsInternetAvailable;
    }

    /// <summary>
    /// Checks the internet connection.
    /// </summary>
    /// <param name="action">The action.</param>
    /// <returns></returns>
    IEnumerator CheckInternetConnection(Action<bool> action)
    {
//        WWW www = new WWW(checkURL);
        WWW www = new WWW(FreakAppManager.baseURL);
        float elapsedTime = 0f;
        bool internetTimeout = false;

        while (!www.isDone) 
        {           
            elapsedTime += Time.deltaTime;
            yield return null;

            if (elapsedTime > timeoutValue)
            {
                Debug.LogError ("*********Internet Connection Time Out********* " + Time.time);
                internetTimeout = true;
                break;
            }
        }

        if (internetTimeout || www.error != null || www.responseHeaders.Count == 0)
        {
//            Debug.LogError ("*********No Internet Connection********* " + Time.time);
            action(false);
            yield return mShortWaitForSeconds;
        }
        else
        {
//            Debug.Log ("*********Internet Connection********* " + Time.time);
            action(true);
            yield return mLongWaitForSeconds;
        }

        LoopIntenetConnection();
    }

    /// <summary>
    /// called when Internets connection is back.
    /// </summary>
    public static void InternetConnectionBack()
    {
        if (instance.OnInternetConnectionBack != null)
        {
            instance.OnInternetConnectionBack();
        }
    }

    /// <summary>
    /// called when Internet connection fails.
    /// </summary>
    public static void InternetConnectionFailed()
    {
        if (instance.OnInternetConnectionFail != null)
        {
            instance.OnInternetConnectionFail();
        }
    }
}
