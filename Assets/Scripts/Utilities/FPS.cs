using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPS: MonoBehaviour
{
    public float updateInterval = 0.5f;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeLeft; // Left time for current interval

	public Text fpsText;
    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 60;

        DontDestroyOnLoad(this.gameObject);
        timeLeft = updateInterval;
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if (timeLeft <= 0)
        {
            // display two fractional digits (f2 format)
//            fpsText.text = "FPS " + (accum / frames).ToString("f2");
            timeLeft = updateInterval;
            accum = 0;
            frames = 0;
        }

//        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }

    float deltaTime = 0.0f;
    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);

        /*float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);*/

        string text = "FPS " + (accum / frames).ToString("f2");

        GUI.Label(rect, text, style);
    }
}
