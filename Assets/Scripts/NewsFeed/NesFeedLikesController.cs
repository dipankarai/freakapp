﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Chat;
using System;
using MiniJSON;

public class NesFeedLikesController : MonoBehaviour, ITableViewDataSource  {

	public HomeScreenUIPanelController homeScreenUIPanelController;
	public ImageController imageController;
	int currentCount;

	public NewsFeedCommentContent m_cellPrefab;
	public TableView m_tableView;

	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	private string lastLikeId = "0";

	public List<LikesDetails> LikesList = new List<LikesDetails>();

	public ConnectingUI loadingObj;
	public Sprite defaultPlaceHolderImage;
	[SerializeField] private ScrollRect scrollRect;
	private bool isPullToRefresh;
	private int maxLimit = 15;
	private int currentLimit = 0;


	void OnEnable()
	{
		m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);
		loadingObj.DisplayLoading ();
		LikesList.Clear ();
		GetNewsFeedLikesAPICall (lastLikeId, FreakAppManager.Instance.myUserProfile.feedId, maxLimit, loadingObj.gameObject);
		currentLimit = maxLimit;
	}

	void EnableTableView()
	{
		isPullToRefresh = false;
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;
	}

	void OnScrollDrag(bool isDrag)
	{
		//LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
		if(!isDrag && !isPullToRefresh)
		{
			//Debug.Log(" 11111111111111 Scroll Draaaaaaag CLASS Rfresh News Feed" +  scrollRect.verticalNormalizedPosition);
			if (scrollRect.verticalNormalizedPosition < 0 || scrollRect.verticalNormalizedPosition > 1.0f || scrollRect.verticalNormalizedPosition <= -0.01f) 
			{
				isPullToRefresh = true;
				currentLimit += 10;
				//Debug.Log("Scroll Draaaaaaag CLASS Rfresh News Feed" );
				loadingObj.DisplayLoading ();
				//m_tableView.scrollY = m_tableView.scrollableHeight;
				GetNewsFeedLikesAPICall (lastLikeId, FreakAppManager.Instance.myUserProfile.feedId, currentLimit, loadingObj.gameObject);

			}
		}
	}

	public void OnClickBack()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
	}


	public void OnClickOptions()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OptionsPanel);
	}


	public void OnClickAllChats()
	{
		FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.AllChatsPanel);
	}

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return LikesList.Count;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		NewsFeedCommentContent cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as NewsFeedCommentContent;
		if (cell == null) {
			cell = (NewsFeedCommentContent)GameObject.Instantiate(m_cellPrefab);
			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		cell.rowNumber = row;
		AssignDetails (cell, row);
		return cell;
	}


	void AssignDetails(NewsFeedCommentContent cell, int rowNumb)
	{
		LikesDetails comment = LikesList [rowNumb];
		cell.nameText.text = comment.userName;
		//cell.EnterMessages (comment);
		cell.profileImage.sprite = defaultPlaceHolderImage;
		if (imageController == null)
		{
			imageController = FindObjectOfType<ImageController> ();
		}
		imageController.GetImageFromLocalOrDownloadImage (comment.avatarUrl, cell.profileImage, Freak.FolderLocation.Profile);

		//cell.PrintPos ();
		cell.m_cellHeightSlider.maxValue = 140;
		cell.height = cell.m_cellHeightSlider.maxValue;
		OnCellHeightChanged (rowNumb, cell.height);
	}
		

	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}


	void OnDisable()
	{
		Destroy (this.gameObject);
	}


	private GameObject LoadingObj;
	/// <summary>
	/// Post News Feed Info
	/// </summary>
	/// <param name="chatString">Chat string.</param>
	public void GetNewsFeedLikesAPICall(string last_news_feed_Like_id, string news_feed_id, int likesLimit, GameObject _loadingObj = null)
	{
		LoadingObj = _loadingObj;
		FreakApi api = new FreakApi ("newFeed.getLikes", ServerResponseForGetNewsFeedLikesAPICall);
		api.param ("news_feed_id", news_feed_id);
		api.param ("last_news_feed_like_id", last_news_feed_Like_id);
		api.param ("limit", likesLimit);
		api.get (this);

	}


	void ServerResponseForGetNewsFeedLikesAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		if(LoadingObj != null)
			LoadingObj.SetActive (false);
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
				List<object> content = (List<object>)userDetailsDict ["contents"];
				lastLikeId = userDetailsDict ["last_news_feed_like_id"].ToString ();
				for (int i = 0; i < content.Count; i++) {
					Dictionary<string,object> newdata = (Dictionary<string,object>)content [i];
					LikesDetails newComments = new LikesDetails (newdata);
					LikesList.Add (newComments);
				}
				EnableTableView ();
				m_tableView.ReloadData ();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
		
	public string EncoedeString(string inputstring){

		return WWW.EscapeURL (inputstring);
	}
		
}

public class LikesDetails
{
	public string news_feed_comment_id;
	public string user_id;
	public string userName;
	public string avatarUrl;
	public string avatarName;

	public LikesDetails (Dictionary<string,object> data)
	{
		news_feed_comment_id = data["news_feed_like_id"].ToString();
		user_id = data["user_id"].ToString();
		if (data.ContainsKey ("name")) {
			userName = DecodeString( data ["name"].ToString ());
		}
		avatarUrl = data["avatar_url"].ToString();
		avatarName = data["avatar_name"].ToString();
	}

	string DecodeString(string s)
	{
		return WWW.UnEscapeURL(s);
	}
}
	



