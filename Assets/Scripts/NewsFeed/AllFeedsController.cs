﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;
using Freak.Ads;

public class AllFeedsController : MonoBehaviour, ITableViewDataSource  {

	int currentCount;
	public List<Contact> dummyContactsList;

    public NewsFeedComponentBase m_cellPrefab;
	public NewsFeedComponentBase m_cellFeedGamePrefab;
	public NewsFeedComponentBase m_cellFeedImagePrefab;
	public NewsFeedComponentBase m_cellFeedAddPrefab;

	public TableView m_tableView;
	private int m_rowCount;
	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	private ImageController imageController;
	[SerializeField] private ScrollRect scrollRect;

	private NewsFeedApiController newsFeedApiController;
	private int maxLimit = 5;
	private int currentLimit = 0;

	public ConnectingUI loadingObj;
	public Sprite defaultPlaceHolderImage;

	public Transform parentTransform;
	public List<Sprite> imageSprite = new List<Sprite> ();
	private bool isPullToRefresh;


	void Start()
	{
		imageController = FindObjectOfType<ImageController> ();
		newsFeedApiController = FindObjectOfType<NewsFeedApiController> ();
	}


	void OnEnable()
	{
        if (FacebookNativeAdHandler.Instance.GetAdsList().Count == 0)
        {
            FacebookNativeAdHandler.Instance.LoadAds();
        }

		m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);
		Init ();
	}
		

	public void Init()
	{
        Debug.Log("News Feed All News");

//        if (FreakAppManager.Instance.FeedList.Count == 0)
        {
            FreakAppManager.Instance.isLoaderOn = true;
            loadingObj.DisplayLoading ();
            FindObjectOfType<NewsFeedApiController>().GetNewsFeedsAPICall(maxLimit, loadingObj.gameObject, 0 , EnableTableViews);
            currentLimit = maxLimit;
        }
	}

	public void EnableTableViews(List<NewsFeedItem> data)
	{
		isPullToRefresh = false;
		FreakAppManager.Instance.FeedList.Clear ();
		for (int i = 0; i < data.Count; i++) 
		{
			FreakAppManager.Instance.FeedList.Add (data [i]);
		}

        AddAdsAfterEveryFiveCount(5);

        m_rowCount = FreakAppManager.Instance.FeedList.Count;
        m_customRowHeights = new Dictionary<int, float>();
        m_tableView.dataSource = this;
//		m_tableView.ReloadData ();

//        loadingObj.HideLoading();
//        FreakAppManager.Instance.isLoaderOn = false;
	}

    void AddAdsAfterEveryFiveCount (int count)
    {
        if (FreakAppManager.Instance.FeedList.Count >= count)
        {
            if (FreakAppManager.Instance.FeedList.Count == 5)
            {
                NewsFeedItem _item = new NewsFeedItem(FacebookNativeAdHandler.Instance.GetAdsList()[0]);
                FreakAppManager.Instance.FeedList.Add(_item);
            }
            else
            {
                int totalCount = 5;
                int adsIndex = 0;
                for (int i = 0; i < FreakAppManager.Instance.FeedList.Count; i++)
                {
                    if (i == (totalCount-1))
                    {
                        NewsFeedItem _item = new NewsFeedItem(FacebookNativeAdHandler.Instance.GetAdsList()[adsIndex]);
                        FreakAppManager.Instance.FeedList.Insert(i, _item);
                        if (adsIndex == (FacebookNativeAdHandler.Instance.GetAdsList().Count - 1))
                        {
                            FacebookNativeAdHandler.Instance.LoadAds();
                        }
                        adsIndex++;
                        totalCount += count;
                    }
                }
            }
        }
        else
        {
            NewsFeedItem _item = new NewsFeedItem(FacebookNativeAdHandler.Instance.GetAdsList()[0]);
            FreakAppManager.Instance.FeedList.Add(_item);
        }
    }
	
    float dragNormalizedPosition;
    bool isDraging;
	void OnScrollDrag(bool isDrag)
	{
        if (isDrag)
        {
            if (isDraging)
                return;

            isDraging = true;

            if (dragNormalizedPosition == 0)
            {
                Debug.Log(isDrag + " Scroll Draaaaaaag " + scrollRect.verticalNormalizedPosition);
                dragNormalizedPosition = scrollRect.verticalNormalizedPosition;
            }
        }
        else
        {
            if (isDraging == false)
                return;

            isDraging = false;

            float dragVelocity = 0;
            if (scrollRect.verticalNormalizedPosition > 1f)
            {
                dragVelocity = scrollRect.verticalNormalizedPosition - dragNormalizedPosition;

                if (dragVelocity > 0.1f)
                {
                    RefreshTable();
                }
            }
            else if (scrollRect.verticalNormalizedPosition < 0)
            {
                dragVelocity = dragNormalizedPosition - scrollRect.verticalNormalizedPosition;

                if (dragVelocity > 0.05f)
                {
                    RefreshTable();
                }
            }

            Debug.Log(isDrag + " Scroll Draaaaaaag " + scrollRect.verticalNormalizedPosition);
            Debug.Log(isDrag + " Scroll Draaaaaaag... " + dragVelocity);

            dragNormalizedPosition = 0;
        }

        /*LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
        if(layoutElement.Length >= 1 && !isDrag && !isPullToRefresh)
        {
            Debug.Log(" 11111111111111 Scroll Draaaaaaag CLASS Rfresh News Feed" +  scrollRect.verticalNormalizedPosition);
            if (scrollRect.verticalNormalizedPosition < 0 || scrollRect.verticalNormalizedPosition > 1.0f || scrollRect.verticalNormalizedPosition <= -0.01f) 
            {
                isPullToRefresh = true;
                currentLimit += 10;
                //Debug.Log("Scroll Draaaaaaag CLASS Rfresh News Feed" );
                loadingObj.DisplayLoading ();
                //m_tableView.scrollY = m_tableView.scrollableHeight;
                newsFeedApiController.GetNewsFeedsAPICall (currentLimit, loadingObj.gameObject, 0, EnableTableViews);
            }
        }*/
	}

    void RefreshTable ()
    {
        LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
        if(layoutElement.Length >= 1 && isPullToRefresh == false)
        {
            Debug.Log("RefreshTable.....");
            isPullToRefresh = true;
            currentLimit += 10;
            loadingObj.DisplayLoading ();
            newsFeedApiController.GetNewsFeedsAPICall (currentLimit, loadingObj.gameObject, 0, EnableTableViews);
        }
    }

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return m_rowCount;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		NewsFeedItem feedItem = FreakAppManager.Instance.FeedList [row];
		NewsFeedComponentBase cell = tableView.GetReusableCell( FreakAppManager.Instance.FeedList [row].GetDataType) as NewsFeedComponentBase;
		if (cell == null) {
			Debug.Log("Instantiate NewsFeed Image -- >");
            cell = GetMessageBoxInstance(feedItem);//(cell, feedItem);
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

//		if (cell == null) {
//			cell = (NewsFeedComponent)GameObject.Instantiate(m_cellPrefab);
//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//		}

		cell.rowNumber = row;
		if (feedItem.panelType == NewsFeedItem.PanelType.NewsFeed) {
			float textval = SetTextRext (cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect);
			cell.SetData (feedItem, textval, imageController, defaultPlaceHolderImage, parentTransform);
		} 
		else if (feedItem.panelType == NewsFeedItem.PanelType.Ads) 
		{
			cell.SetAdds ();
		}
		//cell.height = cell.m_cellHeightSlider.maxValue;
		OnCellHeightChanged (row, cell.height);
		//cell.height = GetHeightOfRow(row);
		return cell;
	}


    public NewsFeedComponentBase GetMessageBoxInstance (NewsFeedItem feeditem) //(NewsFeedComponentBase previousMessageBox, NewsFeedItem feeditem)
	{

//		if (previousMessageBox != null)
//		{
//			//                if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
//			//                {
//			//                    return previousMessageBox;
//			//                }
//			//
//			//				if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
//			//				{
//			//					return previousMessageBox;
//			//				}
//		}
//		if (feedItem.feedType.Equals ("1")) 
//		{
//			m_cellHeightSlider.maxValue = textval + 230;
//		}
//		else 
//		{
//			likeCommentObj.commentImg.sprite = null;
//			//imageController.GetImageFromLocalOrDownloadImageForNewsFeed (feedItem.imageUrl, likeCommentObj.commentImg, Freak.FolderLocation.NewsFeed, this, textval);
//			//Debug.Log("NewsFeed SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
//		}


        Debug.Log("feeditem.feedType "+feeditem.feedType);
        switch (feeditem.feedType)
        {
            case "1":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellPrefab) as NewsFeedComponentBase;
            case "2":
                return (NewsFeedComponentBase)GameObject.Instantiate (m_cellFeedImagePrefab) as NewsFeedComponentBase;
            case "3":
                return (NewsFeedComponentBase)GameObject.Instantiate (m_cellFeedAddPrefab) as NewsFeedComponentBase;
            case "4":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellFeedGamePrefab) as NewsFeedComponentBase;
			default:
				return (NewsFeedComponentBase)GameObject.Instantiate(m_cellPrefab) as NewsFeedComponentBase;
			}

	}

	void AssignDetails(NewsFeedComponent cell, int rowNumb)
	{
        NewsFeedItem feedItem = FreakAppManager.Instance.FeedList [rowNumb];
            
        if (feedItem.panelType == NewsFeedItem.PanelType.NewsFeed)
        {

			
			//cell.SetData (feedItem, textval, imageController, defaultPlaceHolderImage, parentTransform);

//            cell.myNewsFeedObj.SetActive (false);
//            cell.likeCommentObj.gameObject.SetActive (true);
//            cell.facebookNativeAdPanel.gameObject.SetActive(false);
//
//            cell.likeCommentObj.commentImg.rectTransform.sizeDelta = new Vector2 (0, -50);
//            cell.likeCommentObj.image.sprite = defaultPlaceHolderImage;
//            cell.likeCommentObj.avatarUrl = feedItem.avatarUrl;
//            cell.likeCommentObj.DisplayImage();
////            loadProfileImage (feedItem.avatarUrl, cell.likeCommentObj.image, cell);
//            cell.m_cellHeightSlider.maxValue = 237f;
//            if (!string.IsNullOrEmpty (feedItem.feedType)) 
//            {
//                cell.likeCommentObj.feedType = feedItem.feedType;
//
//                if (feedItem.feedType.Equals ("2")) 
//                {
//                    cell.likeCommentObj.commentImg.sprite = null;
//                    float textval = SetTextRext (cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect);
//                    imageController.GetImageFromLocalOrDownloadImageForNewsFeed (feedItem.imageUrl, cell.likeCommentObj.commentImg, Freak.FolderLocation.NewsFeed, cell, textval);
//                    Debug.Log("NewsFeed SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
//                }
//                else 
//                {
//                    cell.m_cellHeightSlider.maxValue = SetTextRext(cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect) + 230;
//                }
//                
////                cell.height = cell.m_cellHeightSlider.maxValue;
//            }
//            
//            cell.reportPopupParent = parentTransform;
//            cell.newsFeedID = feedItem.newsfeedId;
//            cell.feedItem = feedItem;
//            cell.likeCommentObj.nameText.text = feedItem.userName;
//            cell.feedUserId = feedItem.userId;
//            
//            cell.SetLikes(int.Parse(feedItem.likeCount));
//            cell.SetComments(feedItem.totalCommentCount);
//            if (feedItem.isLiked)
//            {
//                cell.SetLikeCountClour (true);
//            } 
//            else {
//                
//                cell.SetLikeCountClour (false);
//            }
//            
//            cell.likeCommentObj.commentText.text = feedItem.feedDataText.feedText;
//            
        }
        else if (feedItem.panelType == NewsFeedItem.PanelType.Ads)
        {
            cell.myNewsFeedObj.SetActive (false);
            cell.likeCommentObj.gameObject.SetActive (false);
            cell.facebookNativeAdPanel.LoadAds(feedItem.facebooknativeAd);

            cell.facebookNativeAdPanel.gameObject.SetActive(true);
            Debug.Log("cell.facebookNativeAdPanel.RectTransform.rect.height " + cell.facebookNativeAdPanel.PanelHeight);
            cell.m_cellHeightSlider.maxValue = cell.facebookNativeAdPanel.PanelHeight;
        }

        cell.height = cell.m_cellHeightSlider.maxValue;
        OnCellHeightChanged (rowNumb, cell.height);
	}


//	IEnumerator SetFeedImage(string imageUrl, Image commentImg)
//	{
//		imageController.GetImageFromLocalOrDownloadImageForNewsFeed (imageUrl, commentImg, Freak.FolderLocation.NewsFeed);
//		yield return null;
//	}

//	void SetImage(int rownumb, Image img){
//		if (rownumb < imageSprite.Count) {
//			img.sprite = imageSprite [rownumb];
//		} else {
//			img.sprite = imageSprite [0];
//		}
//
//	}


	void loadProfileImage(string avatarUrl, Image img, NewsFeedComponent cell)
	{
		//yield return new WaitForEndOfFrame();
		imageController.GetImageFromLocalOrDownloadImage (avatarUrl, img, Freak.FolderLocation.Profile);
		cell.imageUrl = avatarUrl;
	}

//	IEnumerator LoadFeedImage(string imageUrl, Image cmtImg)
//	{
//		cmtImg.sprite = null;
//		yield return new WaitForSeconds (1);
//		imageController.GetImageFromLocalOrDownloadImageForNewsFeed (imageUrl, cmtImg, Freak.FolderLocation.NewsFeed);
//	}


	public float SetTextRext(Text textObj, RectTransform textRect)
	{
		//RectTransform textRect = textObj.GetComponent<RectTransform> ();
		string newtext = textObj.text;
		TextGenerator textGen = new TextGenerator (newtext.Length);
		Vector2 extents = textRect.rect.size;
		textGen.Populate (newtext, textObj.GetGenerationSettings (extents));
		float val = textGen.lineCount;
		float contentHeight = val * 24 + 50;
		Rect newrect = textRect.rect;
		textRect.sizeDelta = new Vector2 (textRect.sizeDelta.x, val * 24);
		//Debug.Log("lineCount -- >"+ val + "contentHeight -- >"+ contentHeight + "text -- >"+ newtext);
		return contentHeight;
	}


	//	public int GetLineCount (Text descText)
	//	{
	//		string text = descText.text;
	//		TextGenerator textGen = new TextGenerator (text.Length);
	//		Vector2 extents = descText.gameObject.GetComponent<RectTransform>().rect.size;
	//		textGen.Populate (text, descText.GetGenerationSettings (extents));
	//		return textGen.lineCount;
	//	}

	#endregion


	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		if (GetHeightOfRow(row) == newHeight) {
			return;
		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}

	void OnDisable()
	{
//		FreakAppManager.Instance.FeedList.Clear ();
		//Resources.UnloadUnusedAssets ();
		m_tableView.ReloadData ();
        m_tableView.scrollY = 0;
//		m_tableView.dataSource = null;
	}

}






