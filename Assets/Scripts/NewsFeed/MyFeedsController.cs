﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tacticsoft;
using Tacticsoft.Examples;

public class MyFeedsController : MonoBehaviour, ITableViewDataSource  {

	int currentCount;

	public List<Contact> dummyContactsList;

	public NewsFeedComponentBase m_cellPrefab;
    public NewsFeedComponentBase m_cellFeedGamePrefab;
	public NewsFeedComponentBase m_cellFeedImagePrefab;
	public NewsFeedComponentBase m_cellFeedAddPrefab;

	public TableView m_tableView;
	private int m_rowCount;
	private int m_numInstancesCreated = 0;

	private Dictionary<int, float> m_customRowHeights;

	private ImageController imageController;
	[SerializeField] private ScrollRect scrollRect;

	public Transform parentTransform;

	private NewsFeedApiController newsFeedApiController;
	private int maxLimit = 20;
	public int currentLimit = 0;

	public ConnectingUI loadingObj;
	public Sprite defaultPlaceHolderImage;

	public List<Sprite> imageSprite = new List<Sprite> ();
	private bool isPullToRefresh;


	void Start()
	{
		//m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);
		imageController = FindObjectOfType<ImageController> ();
		newsFeedApiController = FindObjectOfType<NewsFeedApiController> ();
	}


	void OnEnable()
	{
		m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);
		Init ();
	}


	public void Init()
	{
        Debug.Log("News Feed All News");

//        if (FreakAppManager.Instance.FeedList.Count == 0)
        {
            loadingObj.DisplayLoading ();
            FindObjectOfType<NewsFeedApiController>().GetNewsFeedsAPICall(maxLimit, loadingObj.gameObject, FreakAppManager.Instance.userID , EnableTableMyNewsFeedViews);
            currentLimit = maxLimit;
        }
	}



	public void EnableTableMyNewsFeedViews(List<NewsFeedItem> data)
	{
		isPullToRefresh = false;
		FreakAppManager.Instance.FeedList.Clear ();

//		Dictionary<string,object> initFeedData = new Dictionary<string, object> ();
//		initFeedData.Add ("name", "Test");
//		NewsFeedItem testFeedItem = new NewsFeedItem (initFeedData);
//		FreakAppManager.Instance.FeedList.Add (testFeedItem);

		for (int i = 0; i < data.Count; i++) 
		{
			if (data [i].userId != null)
			{
				
				//if (data [i].userId.Equals (FreakAppManager.Instance.userID.ToString())) 
				//{
					
					FreakAppManager.Instance.FeedList.Add (data [i]);
				//}
			}
		}
		m_rowCount = FreakAppManager.Instance.FeedList.Count;
		m_customRowHeights = new Dictionary<int, float>();
		m_tableView.dataSource = this;

	}

    float dragNormalizedPosition;
	void OnScrollDrag(bool isDrag)
	{
        if (isDrag)
        {
            if (dragNormalizedPosition == 0)
            {
                Debug.Log(isDrag + " Scroll Draaaaaaag " + scrollRect.verticalNormalizedPosition);
                dragNormalizedPosition = scrollRect.verticalNormalizedPosition;
            }
        }
        else
        {
            float dragVelocity = 0;
            if (scrollRect.verticalNormalizedPosition > 1f)
            {
                dragVelocity = scrollRect.verticalNormalizedPosition - dragNormalizedPosition;

                if (dragVelocity > 0.1f)
                {
                    RefreshTable();
                }
            }
            else if (scrollRect.verticalNormalizedPosition < 0)
            {
                dragVelocity = dragNormalizedPosition - scrollRect.verticalNormalizedPosition;

                if (dragVelocity > 0.05f)
                {
                    RefreshTable();
                }
            }

            Debug.Log(isDrag + " Scroll Draaaaaaag " + scrollRect.verticalNormalizedPosition);
            Debug.Log(isDrag + " Scroll Draaaaaaag... " + dragVelocity);

            dragNormalizedPosition = 0;
        }

        /*LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
		if(layoutElement.Length >= 1 && !isDrag && !isPullToRefresh)
		{
			Debug.Log(" 11111111111111 Scroll Draaaaaaag CLASS Rfresh News Feed" +  scrollRect.verticalNormalizedPosition);
			if (scrollRect.verticalNormalizedPosition < 0 || scrollRect.verticalNormalizedPosition > 1.0f || scrollRect.verticalNormalizedPosition <= -0.02f) 
			{
				isPullToRefresh = true;
				currentLimit += 10;
				//Debug.Log("Scroll Draaaaaaag CLASS Rfresh News Feed" );
				loadingObj.DisplayLoading ();
				newsFeedApiController.GetNewsFeedsAPICall (currentLimit, loadingObj.gameObject, FreakAppManager.Instance.userID, EnableTableMyNewsFeedViews);
				//m_tableView.scrollY = m_tableView.scrollableHeight;

			}

			//			if (layoutElement[1].GetComponent<MessageBoxBase>() != null)
			//			{
			//				TimeStampBox timeStamp = layoutElement[1].GetComponent<MessageBoxBase>() as TimeStampBox;
			//				if (timeStamp != null)
			//				{
			//					Debug.Log("BASE CLASS");
			//				}
			//				else
			//				{
			//					timeStampPopup.ShowTimeStamp(layoutElement[1].GetComponent<MessageBoxBase>().chatMessage.messageTimestamp);
			//				}
			//			}
			//			Debug.Log("Scroll Draaaaaaag CLASS" + scrollRect.verticalNormalizedPosition );
		}*/
	}
	
    void RefreshTable ()
    {
        LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
        if(layoutElement.Length >= 1 && isPullToRefresh == false)
        {
            isPullToRefresh = true;
            currentLimit += 10;
            loadingObj.DisplayLoading ();
            newsFeedApiController.GetNewsFeedsAPICall (currentLimit, loadingObj.gameObject, FreakAppManager.Instance.userID, EnableTableMyNewsFeedViews);
        }
    }

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		return m_rowCount;
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return GetHeightOfRow(row);
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
	{
		NewsFeedItem feedItem = FreakAppManager.Instance.FeedList [row];
		NewsFeedComponentBase cell = tableView.GetReusableCell( FreakAppManager.Instance.FeedList [row].GetDataType) as NewsFeedComponentBase;
		if (cell == null) {
			Debug.Log("Instantiate NewsFeed Image -- >");
			cell = GetMessageBoxInstance (cell, feedItem);
			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		}

		//		if (cell == null) {
		//			cell = (NewsFeedComponent)GameObject.Instantiate(m_cellPrefab);
		//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
		//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
		//		}

		cell.rowNumber = row;
		if (feedItem.panelType == NewsFeedItem.PanelType.NewsFeed) {
			float textval = SetTextRext (cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect);
			cell.SetData (feedItem, textval, imageController, defaultPlaceHolderImage, parentTransform);
		} 
		else if (feedItem.panelType == NewsFeedItem.PanelType.Ads) 
		{
			cell.SetAdds ();
		}
		//cell.height = cell.m_cellHeightSlider.maxValue;
		OnCellHeightChanged (row, cell.height);
		//cell.height = GetHeightOfRow(row);
		return cell;
	}

	void AssignDetails(NewsFeedComponent cell, int rowNumb)
	{
		NewsFeedItem feedItem = FreakAppManager.Instance.FeedList [rowNumb];
		float textval = SetTextRext (cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect);
		cell.SetData (feedItem, textval, imageController, defaultPlaceHolderImage, parentTransform);
//			cell.myNewsFeedObj.SetActive (false);
//			cell.likeCommentObj.gameObject.SetActive (true);
//			cell.likeCommentObj.image.sprite = defaultPlaceHolderImage;
//        	cell.imageUrl = feedItem.avatarUrl;
//        	cell.DisplayImage();
////			loadProfileImage (feedItem.avatarUrl, cell.likeCommentObj.image, cell);
//			cell.likeCommentObj.commentImg.rectTransform.sizeDelta = new Vector2 (0, -50);
//			cell.m_cellHeightSlider.maxValue = 237f;
//			if (!string.IsNullOrEmpty (feedItem.feedType)) 
//			{
//				if (feedItem.feedType.Equals ("1")) 
//				{
//					cell.m_cellHeightSlider.maxValue = SetTextRext(cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect) + 230;
//				}
//				else 
//				{
//					cell.likeCommentObj.commentImg.sprite = null;
//					float textval = SetTextRext (cell.likeCommentObj.commentText, cell.likeCommentObj.commentTextRect);
//					imageController.GetImageFromLocalOrDownloadImageForNewsFeed (feedItem.imageUrl, cell.likeCommentObj.commentImg, Freak.FolderLocation.NewsFeed, cell, textval);
//					Debug.Log("NewsFeed SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
//
//				}
//
//				cell.height = cell.m_cellHeightSlider.maxValue;
//			}
//				
//			cell.reportPopupParent = parentTransform;
//			cell.newsFeedID = feedItem.newsfeedId;
//			cell.feedItem = feedItem;
//			cell.likeCommentObj.nameText.text = feedItem.userName;
//			cell.feedUserId = feedItem.userId;
//
//			//likeComment.likesCountText.text = "";
//			cell.SetLikes(int.Parse(feedItem.likeCount));
//			cell.SetComments(feedItem.totalCommentCount);
//			//cell.likeImg.color = Color.white;
//
//			if (feedItem.isLiked)
//			{
//				cell.SetLikeCountClour (true);
//			} 
//			else {
//
//				cell.SetLikeCountClour (false);
//			}
//
//			cell.likeCommentObj.commentText.text = feedItem.feedDataText.feedText;
//			//Debug.Log (" 11111111111111 Feed Text " + feedItem.feedDataText.feedText);

			OnCellHeightChanged (rowNumb, cell.height);
//		}

	}


	public NewsFeedComponentBase GetMessageBoxInstance (NewsFeedComponentBase previousMessageBox, NewsFeedItem feeditem)
	{

		if (previousMessageBox != null)
		{
			//                if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
			//                {
			//                    return previousMessageBox;
			//                }
			//
			//				if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
			//				{
			//					return previousMessageBox;
			//				}
		}
		//		if (feedItem.feedType.Equals ("1")) 
		//		{
		//			m_cellHeightSlider.maxValue = textval + 230;
		//		}
		//		else 
		//		{
		//			likeCommentObj.commentImg.sprite = null;
		//			//imageController.GetImageFromLocalOrDownloadImageForNewsFeed (feedItem.imageUrl, likeCommentObj.commentImg, Freak.FolderLocation.NewsFeed, this, textval);
		//			//Debug.Log("NewsFeed SetImageSize -- >"+ cell.likeCommentObj.commentImg.rectTransform.sizeDelta.y);
		//		}


		switch (feeditem.feedType)
		{
            case "1":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellPrefab) as NewsFeedComponentBase;
            case "2":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellFeedImagePrefab) as NewsFeedComponentBase;
            case "3":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellFeedAddPrefab) as NewsFeedComponentBase;
            case "4":
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellFeedGamePrefab) as NewsFeedComponentBase;
            default:
                return (NewsFeedComponentBase)GameObject.Instantiate(m_cellPrefab) as NewsFeedComponentBase;
		}

	}

//	IEnumerator SetFeedImage(string imageUrl, Image commentImg)
//	{
//		yield return null;
//		imageController.GetImageFromLocalOrDownloadImageForNewsFeed (imageUrl, commentImg, Freak.FolderLocation.NewsFeed);
//	}
		

//	void SetImage(int rownumb, Image img){
//		if (rownumb < imageSprite.Count) {
//			img.sprite = imageSprite [rownumb];
//		} else {
//			img.sprite = imageSprite [0];
//		}
//
//	}


	void loadProfileImage(string avatarUrl, Image img, NewsFeedComponent cell)
	{
		//yield return new WaitForSeconds (0.5f);
		imageController.GetImageFromLocalOrDownloadImage (avatarUrl, img, Freak.FolderLocation.Profile);
		cell.imageUrl = avatarUrl;
	}

//	IEnumerator LoadFeedImage(string url, Image feedImage)
//	{
//		feedImage.sprite = null;
//		yield return new WaitForSeconds (0.1f);
//		imageController.GetImageFromLocalOrDownloadImageForNewsFeed (url, feedImage, Freak.FolderLocation.NewsFeed);
//	}


	public float SetTextRext(Text textObj, RectTransform textRext)
	{
		//RectTransform textRext = textObj.GetComponent<RectTransform> ();
		string text = textObj.text;
		TextGenerator textGen = new TextGenerator (text.Length);
		Vector2 extents = textRext.rect.size;
		textGen.Populate (text, textObj.GetGenerationSettings (extents));
		float val = textGen.lineCount;
		float contentHeight = val * 24 + 50;
		Rect newrect = textRext.rect;
		textRext.sizeDelta = new Vector2 (textRext.sizeDelta.x, val * 24);
		return contentHeight;
	}


	//	public int GetLineCount (Text descText)
	//	{
	//		string text = descText.text;
	//		TextGenerator textGen = new TextGenerator (text.Length);
	//		Vector2 extents = descText.gameObject.GetComponent<RectTransform>().rect.size;
	//		textGen.Populate (text, descText.GetGenerationSettings (extents));
	//		return textGen.lineCount;
	//	}

	#endregion

	private float GetHeightOfRow(int row) {
		if (m_customRowHeights.ContainsKey(row)) {
			return m_customRowHeights[row];
		} else {
			return m_cellPrefab.height;
		}
	}

	private void OnCellHeightChanged(int row, float newHeight) {
		//		if (GetHeightOfRow(row) == newHeight) {
		//			return;
		//		}
		//Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
		m_customRowHeights[row] = newHeight;
		m_tableView.NotifyCellDimensionsChanged(row);
	}


//	public void GetAllContacts()
//	{
//		m_tableView.ShowList ();
//	}
//
//	public void ClearContats()
//	{
//		m_tableView.RemoveList ();
//	}

	void OnDisable()
	{
//		FreakAppManager.Instance.FeedList.Clear ();
		//Resources.UnloadUnusedAssets ();
		m_tableView.ReloadData ();
        m_tableView.scrollY = 0;
//		m_tableView.dataSource = null;
	}

}





