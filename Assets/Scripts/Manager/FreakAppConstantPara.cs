﻿using UnityEngine;
using System.Collections;

public static class FreakAppConstantPara
{
    public static class PlayerPrefsName
    {
//        public const string LoginUserId = "user_id";
//        public const string LoginUserName = "user_name";
//        public const string LoginUserGameName = "user_game_name";
//        public const string LoginUserProfilePicLink = "user_profilepic_link";
//        public const string LoginAccessToken = "access_token";
//        public const string UserMobileNumber = "mobileNumber";
//        public const string UserCountryId = "countryId";

        public const string MPin                                = "MPIN";
        public const string MPinLock                            = "isMPinLock";

        public const string UserInfoPlayerPref                  = "UserInfoPlayerPrefs";
        public const string BlockedUser                         = "BlockedUser";
        public const string MutedUser                           = "MutedUser";
        public const string MutedGroup                          = "MutedGroup";
        public const string LocalNotificationID                 = "LocalNotificationID";
        public const string KeyboardHeight                      = "KeyboardHeight";
        public const string OpenNotificationChatChannel         = "OpenNotificationChatChannel";
        public const string LocalNotificationFromDifferentScene = "LocalNotificationFromDifferentScene";
    }

    public static class UnitySceneName
    {
		public const string SplashScene         = "SplashScreen";
		public const string FreakHomeScene      = "FreakHomeScene";
        public const string UserProfileScene    = "UserProfileScene";
        public const string LoginScene          = "LoginScene";
        public const string TicTacToe           = "TicTacToe";
    }

    public static class MessageDescription
    {
        public const string createdNewGroup     = " created a new group.";
        public const string leftGroup           = " left the group.";
        public const string removedFromGroup    = " is removed the group.";
        public const string addedInGroup        = " is added in the group.";
        public const string newMember           = "New member Added in group.";
        public const string removedMember       = "Removed member from group.";
        public const string newGroup            = "New Group!";
        public const string changeGroupPic      = "changed group picture.";
        public const string groupPicture        = "Group Picture!";

		public const string reportUser          = "You have reported this user!";
		public const string alreadyReported     = "You have already reported this user!";

		public const string reportNewsFeed      = "Are you sure you want to report this post?";
		public const string deleteNewsFeed      = "Are you sure you want to delete this post?";
		public const string newsFeedShare       = "Post shared Successfully!";
		public const string deleteFeedComment   = "Are you sure you want to delete this comment?";

		public const string serverError         = "Error from server!";
		public const string refreshContacts     = "Contacts refreshed!";

		public const string postNewsFeedconfirm     = "Are you sure you want to post this?";
		public const string shareNewsFeedconfirm    = "Are you sure you want to share this post?";

		public const string otpVerification     ="Not valid Otp.";
		public const string wallpaperChange     ="Wallpaper changed successfully.";

		public const string disableFreaks    	= "You have disabled freaks. Do you want to enable it?";
    }


	public static class PopUpMessage
	{
        public const string internetConnection      = "Please Check your internet connection!";
		public const string networkIssue            = "Something wrong with server!\nPlease try Again later.";
	}
}
