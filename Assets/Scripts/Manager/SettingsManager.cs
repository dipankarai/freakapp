﻿using UnityEngine;
using System.Collections;

/// <summary>
/// FreakAppManager class is a mager which is a Singleton class which holds all the current datas.
/// </summary>
public class SettingsManager : MonoBehaviour
{
	#region Initializing Instance
	private static SettingsManager instance;
	public static SettingsManager Instance
	{
		get
		{
			return InitInstance();
		}
	}

	public static SettingsManager InitInstance()
	{
		if (instance == null)
		{
			GameObject instGO = new GameObject("SettingsManager");
			instance = instGO.AddComponent<SettingsManager>();

			DontDestroyOnLoad(instGO);
		}
		return instance;
	}

	void CheckInstance ()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			DestroyImmediate(this.gameObject);
		}

	}

	#endregion

	private bool muteChat;
	public bool isMuteChat
	{
		get
		{ 
			if (PlayerPrefs.HasKey ("isMuteChat"))
			{
				bool getMuteChat = bool.Parse (PlayerPrefs.GetString ("isMuteChat"));
				return getMuteChat;
			}
			else
			{
				return muteGame;
			}
		}
		set
		{ 
			muteChat = value;
			if (PlayerPrefs.HasKey ("isMuteChat")) 
			{
				PlayerPrefs.DeleteKey ("isMuteChat");
			}
			PlayerPrefs.SetString ("isMuteChat", muteChat.ToString());
		}
	}

	private bool muteGame;
	public bool isMuteGame
	{
		get
		{ 
			if (PlayerPrefs.HasKey ("isMuteGame"))
			{
				bool getMuteGame = bool.Parse (PlayerPrefs.GetString ("isMuteGame"));
				return getMuteGame;
			}
			else
			{
				return muteGame;
			}
		}
		set
		{ 
			muteGame = value;
			if (PlayerPrefs.HasKey ("isMuteGame")) 
			{
				PlayerPrefs.DeleteKey ("isMuteGame");
			}
			PlayerPrefs.SetString ("isMuteGame", muteGame.ToString());
		}
	}

    /*private bool notificationControl;
	public bool isNotificationControl
	{
		get
		{ 
			if (PlayerPrefs.HasKey ("isNotificationControl"))
			{
				bool getNotificationControl = bool.Parse (PlayerPrefs.GetString ("isNotificationControl"));
				return getNotificationControl;
			}
			else
			{
				return notificationControl;
			}
		}
		set
		{ 
			notificationControl = value;
			if (PlayerPrefs.HasKey ("isNotificationControl")) 
			{
				PlayerPrefs.DeleteKey ("isNotificationControl");
			}
			PlayerPrefs.SetString ("isNotificationControl", notificationControl.ToString());
		}
	}*/

	private bool buzzControl;
	public bool isBuzzControl
	{
		get 
		{ 
			if (PlayerPrefs.HasKey ("isBuzzControl"))
			{
				bool getBuzzControl = bool.Parse (PlayerPrefs.GetString ("isBuzzControl"));
				return getBuzzControl;
			}
			else
			{
				return buzzControl;
			}
		}
		set 
		{ 
			buzzControl = value;
			if (PlayerPrefs.HasKey ("isBuzzControl")) 
			{
				PlayerPrefs.DeleteKey ("isBuzzControl");
			}
			PlayerPrefs.SetString ("isBuzzControl", buzzControl.ToString());
		}
	}

	private bool disableFreaks;
	public bool isDisableFreaks{
		get
		{ 
			if (PlayerPrefs.HasKey ("isDisableFreaks"))
			{
				bool getDisableFreaks = bool.Parse (PlayerPrefs.GetString ("isDisableFreaks"));
				return getDisableFreaks;
			}
			else
			{
                return false;
			}
		}
		set 
		{ 
			disableFreaks = value;
			if (PlayerPrefs.HasKey ("isDisableFreaks")) 
			{
				PlayerPrefs.DeleteKey ("isDisableFreaks");
			}
			PlayerPrefs.SetString ("isDisableFreaks", disableFreaks.ToString());
		}
	}


	private bool privacyReadReciepts;
	public bool isPrivacyReadReciepts{
		get
		{ 
			if (PlayerPrefs.HasKey ("isPrivacyReadReciepts"))
			{
				bool getPrivacyReadReciepts = bool.Parse (PlayerPrefs.GetString ("isPrivacyReadReciepts"));
				return getPrivacyReadReciepts;
			}
			else
			{
				return privacyReadReciepts;
			}
		}
		set 
		{ 
			privacyReadReciepts = value;
			if (PlayerPrefs.HasKey ("isPrivacyReadReciepts")) 
			{
				PlayerPrefs.DeleteKey ("isPrivacyReadReciepts");
			}
			PlayerPrefs.SetString ("isPrivacyReadReciepts", privacyReadReciepts.ToString());
		}
	}


	private bool mPinLock;
	public bool isMPinLock{
		get
		{ 
            if (PlayerPrefs.HasKey (FreakAppConstantPara.PlayerPrefsName.MPinLock))
			{
                bool getisMPinLock = bool.Parse (PlayerPrefs.GetString (FreakAppConstantPara.PlayerPrefsName.MPinLock));
				return getisMPinLock;
			}
			else
			{
				return mPinLock;
			}
		}
		set 
		{ 
			mPinLock = value;
            if (PlayerPrefs.HasKey (FreakAppConstantPara.PlayerPrefsName.MPinLock)) 
			{
                PlayerPrefs.DeleteKey (FreakAppConstantPara.PlayerPrefsName.MPinLock);
			}
            PlayerPrefs.SetString (FreakAppConstantPara.PlayerPrefsName.MPinLock, mPinLock.ToString());
		}
	}



	int profilePicPrivacy;
	public int ProfilePicPrivacy{
		get
		{ 
			if (PlayerPrefs.HasKey ("ProfilePicPrivacy"))
			{
				int getProfilePicPrivacy = int.Parse (PlayerPrefs.GetString ("ProfilePicPrivacy"));
				return getProfilePicPrivacy;
			}
			else
			{
				return profilePicPrivacy;
			}
		}
		set 
		{ 
			profilePicPrivacy = value;
			if (PlayerPrefs.HasKey ("ProfilePicPrivacy")) 
			{
				PlayerPrefs.DeleteKey ("ProfilePicPrivacy");
			}
			PlayerPrefs.SetString ("ProfilePicPrivacy", profilePicPrivacy.ToString());
		}
	}


	int lastSeenPrivacy;
	public int LastSeenPrivacy{
		get
		{ 
			if (PlayerPrefs.HasKey ("LastSeenPrivacy"))
			{
				int getLastSeenPrivacy = int.Parse (PlayerPrefs.GetString ("LastSeenPrivacy"));
				return getLastSeenPrivacy;
			}
			else
			{
				return lastSeenPrivacy;
			}
		}
		set 
		{ 
			lastSeenPrivacy = value;
			if (PlayerPrefs.HasKey ("LastSeenPrivacy")) 
			{
				PlayerPrefs.DeleteKey ("LastSeenPrivacy");
			}
			PlayerPrefs.SetString ("LastSeenPrivacy", lastSeenPrivacy.ToString());
		}
	}


	int statusPrivacy;
	public int StatusPrivacy{
		get
		{ 
			if (PlayerPrefs.HasKey ("StatusPrivacy"))
			{
				int getStatusPrivacy = int.Parse (PlayerPrefs.GetString ("StatusPrivacy"));
				return getStatusPrivacy;
			}
			else
			{
				return statusPrivacy;
			}
		}
		set 
		{ 
			statusPrivacy = value;
			if (PlayerPrefs.HasKey ("StatusPrivacy")) 
			{
				PlayerPrefs.DeleteKey ("StatusPrivacy");
			}
			PlayerPrefs.SetString ("StatusPrivacy", statusPrivacy.ToString());
		}
	}


	int onlinePrivacy;
	public int OnlinePrivacy{
		get
		{ 
			if (PlayerPrefs.HasKey ("OnlinePrivacy"))
			{
				int getOnlinePrivacy = int.Parse (PlayerPrefs.GetString ("OnlinePrivacy"));
				return getOnlinePrivacy;
			}
			else
			{
				return onlinePrivacy;
			}
		}
		set 
		{ 
			onlinePrivacy = value;
			if (PlayerPrefs.HasKey ("OnlinePrivacy")) 
			{
				PlayerPrefs.DeleteKey ("OnlinePrivacy");
			}
			PlayerPrefs.SetString ("OnlinePrivacy", onlinePrivacy.ToString());
		}
	}

	/// <summary>
	/// Initializes its instance.
	/// </summary>
	void Awake ()
	{
		CheckInstance();
	}
}
