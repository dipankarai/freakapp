﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Freak.Chat;
using System;
using Freak;
using System.Linq;
using System.Runtime.InteropServices;

/// <summary>
/// FreakAppManager class is a mager which is a Singleton class which holds all the current datas.
/// </summary>
public class FreakAppManager : MonoBehaviour
{
#region Initializing Instance
    private static FreakAppManager instance;
    public static FreakAppManager Instance
    {
        get
        {
            return InitInstance();
        }
    }

    public static FreakAppManager InitInstance()
    {
        if (instance == null)
        {
            GameObject instGO = new GameObject("FreakAppManager");
            instance = instGO.AddComponent<FreakAppManager>();
        }
        return instance;
    }

    void CheckInstance ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }

    }

#endregion

//    public const string baseURL = "http://35.154.90.175/FREAK-CHAT/TEST/rest.php?";
//      public const string AvatarURL = "http://35.154.90.175/FREAK-CHAT/TEST/static/images/avatar.png";

    public const string baseURL = "http://35.154.251.41/FREAK/API/FREAK-CHAT/rest.php?";
    public const string AvatarURL = "http://35.154.251.41/FREAK/API/FREAK-CHAT/static/images/avatar.png";

//    public const string baseURL = "http://www.juegostudio.in/FREAK-CHAT/TEST/rest.php?";
//    public const string AvatarURL = "http://juegostudio.in/FREAK-CHAT/TEST/static/images/avatar.png";

    public static string AppName
    {
        get { return Application.productName; }
    }
    public static string ApplicationPath
    {
        get
        {
            return Application.persistentDataPath;
        }
    }

    public UserInfoPlayerPrefs userInfoPlayerPrefs;

    public int userID;
    public int countryId;
    public string APIkey;
    public string mobileNumber;
    public string username = string.Empty;
    public string secondaryUsername = string.Empty;
    public string profileLink = string.Empty;
	public string profileCoverLink = string.Empty;
    public Texture2D senderCompressedPic;
    public string tempcover_image;
    public string tempavatar_url;
    public MyUserProfile myUserProfile;
    public List<NewsFeedItem> FeedList = new List<NewsFeedItem>();
    //testing purpose
    public string otp;
    public bool showAllChats;
    public bool isLoadAllChats;
    public Freak.Chat.ChatChannel tempCurrentChannel;
    public Freak.Chat.ChatChannel selectedGroupProfile;
    public bool areContactsSynched;
    public List<ServerUserContact> userSynchedContacts = new List<ServerUserContact> ();
    public List<StoreGames> inventoryGamesList = new List<StoreGames>();
//  public StoreGames currentSelectedStoreGame;
    public string temporaryWallpaperName;
    private PluginMsgHandler pluginMsgHandler;

    public List<Contact> allContacts = new List<Contact>();
	public float timeToInit = 0.8f;
	public bool isLoaderOn;
    public Texture2D defaultCoverTexture;
    public Texture2D defaultProfileTexture;
    public Texture2D defaultGroupProfileTexture;

	/// <summary>
    /// Initializes its instance.
    /// </summary>
    void Awake ()
    {
        DontDestroyOnLoad(this.gameObject);

        CheckInstance();

        if (FreakAppManager.GetSetUserInfoPlayerPrefs != null)
        {
            Instance.GetUserInfoPlayerPrefs (FreakAppManager.GetSetUserInfoPlayerPrefs);
        }

        LoadDefaultIcon();
    }

    void LoadDefaultIcon ()
    {
        if (defaultCoverTexture == null)
        {
            defaultCoverTexture = Resources.Load("DefaultIcon/DefaultCoverPic") as Texture2D;
        }

        if (defaultProfileTexture == null)
        {
            defaultProfileTexture = Resources.Load("DefaultIcon/ProfilePicCircle") as Texture2D;
        }

        if (defaultGroupProfileTexture == null)
        {
            defaultGroupProfileTexture = Resources.Load("DefaultIcon/GroupPlaceholder") as Texture2D;
        }
    }

    void HandleShowKeyboardDelegate (bool bKeyboardShow, int nKeyHeight)
    {
        if (bKeyboardShow == true)
        {
            Debug.Log("HandleShowKeyboardDelegate----->>>> " + bKeyboardShow + " Height " + nKeyHeight);
            PlayerPrefs.SetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight, (float)nKeyHeight);
        }
    }

    public HomeScreenUIPanelController homeScreenUIPanelController { set { mHomeScreenUIPanelController = value; } }
    private HomeScreenUIPanelController mHomeScreenUIPanelController;
    public HomeScreenUIPanelController GetHomeScreenPanel ()
    {
        if (mHomeScreenUIPanelController == null)
        {
            mHomeScreenUIPanelController = FindObjectOfType<HomeScreenUIPanelController>();
            return mHomeScreenUIPanelController;               
        }
        else
        {
            return mHomeScreenUIPanelController;
        }
    }

    #region USER INFO

    [System.Serializable]
    public class UserInfoPlayerPrefs
    {
        public int loginUserId;
        public int userCountryId;
        public string loginUserName;
        public string loginUserGameName;
        public string loginUserProfilePicLink;
        public string loginAccessToken;
        public string userMobileNumber;
    }

    public static UserInfoPlayerPrefs GetSetUserInfoPlayerPrefs
    {
        get
        {
            if (PlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.UserInfoPlayerPref))
            {
                return (UserInfoPlayerPrefs)Q.Utils.PlayerPrefsSerializer.LoadData<UserInfoPlayerPrefs> (FreakAppConstantPara.PlayerPrefsName.UserInfoPlayerPref);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Q.Utils.PlayerPrefsSerializer.SaveData (FreakAppConstantPara.PlayerPrefsName.UserInfoPlayerPref, value);
        }
    }

    public void SaveFirstTimeUserInfoPlayerPrefs ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("SaveUserInfoPlayerPrefs");
        #endif

        UserInfoPlayerPrefs newUserInfo = new UserInfoPlayerPrefs ();
        newUserInfo.loginAccessToken        = Instance.APIkey;
        newUserInfo.loginUserGameName       = Instance.secondaryUsername;
        newUserInfo.loginUserId             = Instance.userID;
        newUserInfo.loginUserName           = Instance.username;
        newUserInfo.loginUserProfilePicLink = Instance.profileLink;
        newUserInfo.userCountryId           = Instance.countryId;
        newUserInfo.userMobileNumber        = Instance.mobileNumber;

        userInfoPlayerPrefs = newUserInfo;

        //Save in PlayerPrefs
        GetSetUserInfoPlayerPrefs = newUserInfo;

        Freak.Chat.ChatManager.Instance.UserId = Instance.userID.ToString ();
        Freak.Chat.ChatManager.Instance.UserName = Instance.username;
        Freak.Chat.ChatManager.Instance.profileLink = Instance.profileLink;

        Freak.Chat.ChatManager.Instance.LoginToSendbird ();
//        Freak.Chat.ChatManager.Instance.StartRefreshCoroutine();
    }

    public void SaveUserInfoPlayerPrefs ()
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("SaveUserInfoPlayerPrefs");
        #endif

        UserInfoPlayerPrefs newUserInfo = new UserInfoPlayerPrefs ();
        newUserInfo.loginAccessToken        = Instance.APIkey;
        newUserInfo.loginUserGameName       = Instance.secondaryUsername;
        newUserInfo.loginUserId             = Instance.userID;
        newUserInfo.loginUserName           = Instance.username;
        newUserInfo.loginUserProfilePicLink = Instance.profileLink;
        newUserInfo.userCountryId           = Instance.countryId;
        newUserInfo.userMobileNumber        = Instance.mobileNumber;

        userInfoPlayerPrefs = newUserInfo;

        //Save in PlayerPrefs
        GetSetUserInfoPlayerPrefs = newUserInfo;

        Freak.Chat.ChatManager.Instance.UserId = Instance.userID.ToString ();

        if (Freak.Chat.ChatManager.Instance.UserName.Equals(Instance.username) == false ||
            Freak.Chat.ChatManager.Instance.profileLink.Equals(Instance.profileLink) == false)
        {
            Freak.Chat.ChatManager.Instance.UserName = Instance.username;
            Freak.Chat.ChatManager.Instance.profileLink = Instance.profileLink;
            Freak.Chat.ChatManager.Instance.UpdateUserInfo ();
//            Freak.Chat.ChatManager.Instance.RefreshForNewMessages ();
        }
    }

    public void GetUserInfoPlayerPrefs (UserInfoPlayerPrefs userInfo)
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("GetUserInfoPlayerPrefs");
        #endif

        Instance.APIkey             = userInfo.loginAccessToken;
        Instance.secondaryUsername  = userInfo.loginUserGameName;
        Instance.userID             = userInfo.loginUserId;
        Instance.username           = userInfo.loginUserName;
        Instance.profileLink        = userInfo.loginUserProfilePicLink;
        Instance.countryId          = userInfo.userCountryId;
        Instance.mobileNumber       = userInfo.userMobileNumber;

        userInfoPlayerPrefs = userInfo;

        Freak.Chat.ChatManager.Instance.UserName = Instance.username;
        Freak.Chat.ChatManager.Instance.UserId = Instance.userID.ToString ();
        Freak.Chat.ChatManager.Instance.profileLink = Instance.profileLink;

        /*if (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
        {
            Freak.Chat.ChatManager.Instance.LoginToSendbird ();
        }*/
    }

    #endregion USER INFO

    #region ALL SERVER CONTACT
	private Action<List<ServerUserContact>> LoadContactListFromServerCallback;

	public void LoadServerContactList (Action<List<ServerUserContact>> callback)
    {
        GetAllServerUserInfo();

        if (userSynchedContacts.Count > 0 && callback != null)
        {
            callback(userSynchedContacts);
        }

        if (InternetConnection.Check())
        {
            LoadContactListFromServerCallback = callback;
            GetContactListAPICall (FreakAppManager.Instance.mobileNumber);
        }
        else
        {
            if (callback != null)
            {
                callback(userSynchedContacts);
            }
        }
    }

    void GetContactListAPICall(string contactNumb)
    {
        FreakApi api  = new FreakApi("contact.listContact",ServerResponseForGetContactListAPICall);
        api.param("userId",FreakAppManager.Instance.userID);
        api.param("accessToken",FreakAppManager.Instance.APIkey);

        api.get(this);
    }

    void ServerResponseForGetContactListAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
    {
        // check for errors
        if (output!=null)
        {
            string responseInfo =(string)output["responseInfo"];
            switch(responseCode)
            {
                case 001:
                    List<object> userDetailsList = (List<object>)output["responseMsg"];
                    List<ServerUserContact> tempserverContactList = new List<ServerUserContact>();
                    for (int i = 0; i < userDetailsList.Count; i++)
                    {
                        Dictionary<string, object> data = (Dictionary<string,object>)userDetailsList[i];
                        ServerUserContact newContact = new ServerUserContact(data);
                        if (!newContact.contact_number.Equals(FreakAppManager.Instance.mobileNumber))
                        {
                            tempserverContactList.Add(newContact);
                        }
                    }

                    LoadContactListFromServerDone(tempserverContactList);
                    break;
                case 207:
                    /**
                * User email id Does not Exists.. U need to signIn first...
                */
                    break;
                default:
                    /**
                 * Show Popup to let user know if anything has gone wrong while login in...
                 */
                    break;
            }   
        } 
        else {
            Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
        }  
    }

    void LoadContactListFromServerDone(List<ServerUserContact> tempserverContactList)
    {
		if (LoadContactListFromServerCallback != null)
		{
			LoadContactListFromServerCallback (tempserverContactList);
		}

        LoadContactListFromServerCallback = null;

        List<ServerUserContact> serverContactList = new List<ServerUserContact>(GetAllServerUserInfo());

		for (int i = 0; i < tempserverContactList.Count; i++)
        {
			if (CheckForPreviousUser (tempserverContactList[i]) == false)
			{
                serverContactList.Add (tempserverContactList [i]);
			}
        }

        if (serverContactList.Count > 0)
        {
            SetAllServerUserInfo(serverContactList);
        }
    }

	bool CheckForPreviousUser (ServerUserContact user)
    {
        List<ServerUserContact> serverContactList = new List<ServerUserContact> (GetAllServerUserInfo());
        for (int i = 0; i < serverContactList.Count; i++) {
            if (serverContactList[i].user_id == user.user_id)
            {
                UpdateUserInfo (serverContactList [i], user);
                return true;
            }
        }

		return false;
	}

    public ServerUserContact GetServeruserContact (string userId)
    {
        List<ServerUserContact> serverContactList = new List<ServerUserContact> (GetAllServerUserInfo());

        for (int i = 0; i < serverContactList.Count; i++) {
            if (serverContactList[i].user_id == int.Parse (userId))
            {
                return serverContactList[i];
            }
        }

        return null;
    }

    public void SetAllServerUserInfo (List<ServerUserContact> serverUserContactList)
    {
        FreakPlayerPrefs.GetSetServerUserContact = serverUserContactList;
        userSynchedContacts = new List<ServerUserContact>(serverUserContactList);
    }

    public List<ServerUserContact> GetAllServerUserInfo ()
    {
        if (userSynchedContacts == null)
        {
            userSynchedContacts = new List<ServerUserContact>();
        }
        userSynchedContacts = FreakPlayerPrefs.GetSetServerUserContact;

        return userSynchedContacts;
    }

    void UpdateServerUserInfo (ServerUserContact userInfo)
    {
        List<ServerUserContact> serverContactList = new List<ServerUserContact> (GetAllServerUserInfo());
        for (int i = 0; i < serverContactList.Count; i++) {
            if (serverContactList[i].user_id == userInfo.user_id)
            {
                serverContactList[i] = userInfo;
            }
        }

        SetAllServerUserInfo(serverContactList);
    }

	void UpdateUserInfo (ServerUserContact oldInfo, ServerUserContact newInfo)
	{
		oldInfo.name = newInfo.name;
		oldInfo.status_message = newInfo.status_message;

		if (oldInfo.avatar.Equals(newInfo.avatar))
		{
			if (oldInfo.imageCache == null)
			{
			}
		}
		else
		{
			oldInfo.avatar = newInfo.avatar;
		}
	}

    #endregion ALL SERVER CONTACT

    private static readonly string[] dummy_Purchased_List = { "5", "6", "7" };
    public static bool IsInventoryPurchased(string intentoryID)
    {
        for (int i = 0; i <= dummy_Purchased_List.Length - 1; i++)
        {
            if (dummy_Purchased_List[i] == intentoryID)
                return true;
        }

        return false;
    }

    public static void GoToBackground()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        activity.Call<bool>("moveTaskToBack", true);
        #elif UNITY_IOS || UNITY_IPHONE
        Debug.Log("TODO::::");
        #endif
    }


	#region ALL LOAD CONTACT
    private bool isSyncContactsFromDevice;
	public void LoadContactFromDevice ()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("Contacts -----> Load Contact " + Time.time);
        #endif

        if (isSyncContactsFromDevice)
        {
            return;
        }

        if (FreakPlayerPrefs.GetAllContactfromLocal.Count == 0)
        {
            isSyncContactsFromDevice = true;
            Contacts.LoadContactList(LoadContactFromDeviceSuccess, LoadContactFromDeviceFailed);
        }
	}

	void LoadContactFromDeviceSuccess ()
	{
        for (int i = 0; i < Contacts.ContactsList.Count; i++) {

            for (int k = 0; k < Contacts.ContactsList[i].Phones.Count; k++)
            {

                string number = Contacts.ContactsList[i].Phones[k].Number.ToString();
                number = number.Replace(" ", string.Empty);
                number = number.Replace("-", string.Empty);
                number = number.Replace("+", string.Empty);

                bool digitsOnly = number.All(char.IsDigit);

                if (digitsOnly)
                {
                    if (number.Length > 10)
                    {
                        number = number.Substring(number.Length - 10);
                    }

                    if (!number.Equals(FreakAppManager.Instance.mobileNumber))
                    {
                        Contacts.ContactsList[i].Phones[0].Number = number;
                    }
                }
                else
                {
                    Debug.LogWarning("NUMBER DATA NOT SYNC-----" + Contacts.ContactsList[i].Phones[k].Number.ToString());
                }
            }
		}

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;
        FreakAppManager.Instance.allContacts = new List<Contact>(Contacts.ContactsList);

        LoadServerContactList(InitAllContactWithServerContact);

        Debug.Log("Contacts -----> Load Contact.. " + Time.time);
        isSyncContactsFromDevice = false;
	}

    void InitAllContactWithServerContact (List<ServerUserContact> data)
    {
        for (int i = 0; i < FreakAppManager.Instance.allContacts.Count; i++)
        {
            for (int j = 0; j < data.Count; j++)
            {
                if (FreakAppManager.Instance.allContacts[i].Phones [0].Number.Equals (data [j].contact_number))
                {
                    FreakAppManager.Instance.allContacts [i].isFreak = true;
                    FreakAppManager.Instance.allContacts [i].freakUserId = data [j].user_id.ToString();
                }
            }
        }

        FreakPlayerPrefs.GetAllContactfromLocal = Contacts.ContactsList;
    }

	void LoadContactFromDeviceFailed (string reason)
	{
        Debug.LogError("Load Contact From Device Failed " + reason);
        isSyncContactsFromDevice = false;
	}

    public List<Contact> GetAllContacts ()
    {
        if (allContacts.Count == 0)
        {
            allContacts = new List<Contact>(FreakPlayerPrefs.GetAllContactfromLocal);
        }

        if (allContacts.Count == 0)
        {
            LoadContactFromDevice();
        }

        return allContacts;
    }

	#endregion

    #region Freak Game
    [DllImport ("__Internal")] private static extern void LoadFreakGameiOS(int sceneID);

    public void PlayGame(string gameName, string gameId = "")
    {
        Debug.Log("PlayGame Name: " + gameName + " == ID: " + gameId);

        int _gameType = 0;
        if (string.IsNullOrEmpty(gameId) == false)
        {
            _gameType = int.Parse(gameId);
        }

        string _gameData = Newtonsoft.Json.JsonConvert.SerializeObject(FreakAppManager.Instance.currentGameData);

        if (gameName.Contains("Tic Tac Toe") || gameId.Equals("5"))
        {
            FindObjectOfType<HomeScreenUIPanelController>().gameLoadController.gameObject.SetActive(true);
        }
        else
        {
            #if UNITY_EDITOR
            Debug.Log("Load Game SandCastle ....");
            #else
            Debug.Log("Load Game ======");
            #if UNITY_ANDROID
            Debug.Log("Load Game SandCastle Editor....");
            Freak.Game.FreakGameCommunicator.SwitchToGame(gameName, _gameType, _gameData);
            #elif UNITY_IOS || UNITY_IPHONE
                Debug.Log("Load ...." + gameName +"::"+ _gameType);

                if (gameName.Contains("Sand Castle") || gameId.Equals("6"))
                {
                    Debug.Log("Load Game SandCastle Editor....");
                    LoadFreakGameiOS (1);
                }
                else if (gameName.Contains("Quiz") || gameId.Equals("7"))
                {
                    Debug.Log("Load Game Quiz ....");
                    LoadFreakGameiOS (2);
                }
                else if (gameName.Contains("Chess Circle") || gameId.Equals("8"))
                {
                    Debug.Log("Load Game Quiz ....");
                    LoadFreakGameiOS (3);
                }
                else if (gameName.Contains("Drawit") || gameId.Equals("9"))
                {
                    Debug.Log("Load Game Quiz ....");
                    LoadFreakGameiOS (4);
                }
                else if (gameName.Contains("Self Moments") || gameId.Equals("10"))
                {
                    Debug.Log("Load Game Quiz ....");
                    LoadFreakGameiOS (5);
                }
                else if (gameName.Contains("Snap Pool") || gameId.Equals("11"))
                {
                    Debug.Log("Load Game Quiz ....");
                    LoadFreakGameiOS (6);
                }

            #endif

            #endif
        }
    }
    #endregion

    public GameIntegrateData currentGameData;

    [System.Serializable]
    public class GameIntegrateData
    {
        public string user_id;
        public string user_name;
        public string user_profile_url;
        public string access_token;
        
        public string opponent_name;
        public string opponent_id;
        public string opponent_profile_url;

        public bool is_single_mode = true;
        public bool isGameSelected;
        public bool isChallengeAccepted;
        public int game_id = -1;

        public StoreGames selectedStoreGame;

        public GameIntegrateData()
        {
            user_id = FreakAppManager.Instance.userID.ToString();
            user_name = FreakAppManager.Instance.username;
            access_token = FreakAppManager.Instance.APIkey;
            user_profile_url = FreakAppManager.Instance.profileLink;
        }
    }

    #region IOS BRIDGE

    #region iOS STATUS BAR
    public enum StatusBarStyle
    {
        StatusBarStyleDefault = 0,
        StatusBarStyleLightContent
    }

    [DllImport ("__Internal")] private static extern float GetStatusBarHeightiOS();
    [DllImport ("__Internal")] private static extern void HideStatusBar(bool hide);
    [DllImport ("__Internal")] private static extern void ChangeStatusBar(int type);

    public static float GetStatusBarHeight ()
    {
        float _statusHeight = 0;

        #if UNITY_EDITOR
        _statusHeight = 32f;
        #else
        if (PlayerPrefs.HasKey("StatusBarHeightiOS"))
        {
        _statusHeight = PlayerPrefs.GetFloat("StatusBarHeightiOS");
        }
        else
        {
        _statusHeight = GetStatusBarHeightiOS();
        _statusHeight = (_statusHeight / 0.75f) + 5f;
        PlayerPrefs.SetFloat("StatusBarHeightiOS", _statusHeight);
        }
        #endif

        return _statusHeight;
    }

    public static void HideIOSStatusBar (bool hide)
    {
        #if !UNITY_EDITOR
        HideStatusBar(hide);
        #endif
    }

    public static void ChangeStatusBarStyle (StatusBarStyle style)
    {

        #if !UNITY_EDITOR

        #if UNITY_IOS || UNITY_IPHONE
        ChangeStatusBar((int)style);
        #endif

        #endif
    }

    #endregion

    #endregion
}
