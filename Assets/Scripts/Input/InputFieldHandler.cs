﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

[RequireComponent(typeof(InputField))]
public class InputFieldHandler : MonoBehaviour, IPointerClickHandler, ISubmitHandler, IPointerExitHandler
{
    public Action OnInputFieldFocus;
	public Action OnInputFieldSubmit;
	public Action OnPointerDownClick;

	public InputField nextInputFieldToFocus;

	private Coroutine jumpFieldRoutine;

	public void JumpNextField()
	{
		//this.GetComponent<InputField>().wasCanceled
			
		if (this != null) 
		{
			jumpFieldRoutine = StartCoroutine (NextFieldEnable (0.8f));
		}
	}

	public void OnPointerExit (PointerEventData data)
	{
//		print ("OnPointerExit");
//		StopJumpRoutine ();
	}

	public void OnDeselect(BaseEventData eventData)
	{
//		print("OnDeselect");
//		StopJumpRoutine ();
	}

	void StopJumpRoutine ()
	{
		if (jumpFieldRoutine != null) 
		{
			StopCoroutine (jumpFieldRoutine);
		}
	}

	IEnumerator NextFieldEnable(float value)
	{
		yield return new WaitForSeconds(value);
		EnableFocusNextInputField ();
	}

	void EnableFocusNextInputField ()
	{
		if ((nextInputFieldToFocus != null) && (String.IsNullOrEmpty (nextInputFieldToFocus.text))) 
		{
			nextInputFieldToFocus.ActivateInputField ();
			nextInputFieldToFocus.Select ();
		}
		InputFieldHandler nextInputHandler = nextInputFieldToFocus.GetComponent<InputFieldHandler>();
		if ((nextInputHandler != null) && (String.IsNullOrEmpty(nextInputFieldToFocus.text)))
		{
			nextInputHandler.OnPointerClick (null);
		}

//		nextInputFieldToFocus.ActivateInputField ();
//		nextInputFieldToFocus.Select ();

//		if ((nextInputHandler != null))
//		{
//			nextInputHandler.OnPointerClick (null);
//		}
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        if(OnInputFieldFocus != null)
        {
            OnInputFieldFocus();
        }
    }

	public void OnSubmit(BaseEventData  eventData)
	{
		if(OnInputFieldSubmit != null)
		{
			OnInputFieldSubmit();
		}
	}

}