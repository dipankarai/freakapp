﻿using UnityEngine;
using UnityEngine.EventSystems;


public class EventTriggerExample : EventTrigger
{
	private Vector3 positionVector;
	public HomeScreenUIPanelController homeScreenUIPanelController;

	void Start()
	{
		homeScreenUIPanelController = FindObjectOfType<HomeScreenUIPanelController> ();
	}


	public override void OnBeginDrag( PointerEventData data )
	{
		Debug.Log( "OnBeginDrag called."+ data.position );
		positionVector = data.position;
	}
		

	public override void OnDrag( PointerEventData data )
	{
		//Debug.Log( "OnDrag called." + data.position);

	}
		
	public override void OnEndDrag( PointerEventData data )
	{
		Debug.Log( "OnEndDrag called."+ data.position );
		if (positionVector.x > data.position.x) 
		{
			if((positionVector.x - data.position.x) > 100)
			Debug.Log( "OnDrag LEFT");
			homeScreenUIPanelController.OnDrag (false);
		} 
		else {
			if((data.position.x - positionVector.x) > 100)
			Debug.Log( "OnDrag Right");
			homeScreenUIPanelController.OnDrag (true);
		}
		//positionVector = data.position;
	}

}