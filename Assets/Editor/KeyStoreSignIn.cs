﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class KeyStoreSignIn
{
    const string KEYSTORE_FILEPATH  = "KayStore/androidKey2.keystore";
    const string KEYSTORE_PASSWORD  = "221234";
    const string KEYSTORE_ALIAS     = "juego";

    static KeyStoreSignIn ()
    {
        #if UNITY_EDITOR && UNITY_ANDROID
        string keyStorePath = Application.dataPath;
        keyStorePath = keyStorePath.Replace("Assets", KEYSTORE_FILEPATH);

        PlayerSettings.Android.keyaliasName = keyStorePath;
        PlayerSettings.Android.keystorePass = KEYSTORE_PASSWORD;
        PlayerSettings.Android.keyaliasName = KEYSTORE_ALIAS;
        PlayerSettings.Android.keyaliasPass = KEYSTORE_PASSWORD;
        #endif
    }
}
