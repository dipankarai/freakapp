﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using System.IO.Compression;
using System;
#if UNITY_EDITOR
using Ionic.Zip;
#endif 



namespace Tastybits.EmojiAssist {

	[CustomEditor(typeof(EmojiResources))]
	public class EmojiResourcesInspector : Editor {



		public override void OnInspectorGUI(){
			var targ = this.target as EmojiResources;
			//base.OnInspectorGUI();
			if( EmojiResources.instance == null )
				EmojiResources.instance = targ;

			GUILayout.Label ("Emoji Renderer Sources: ");

			foreach (var pf in EmojiResources.GetSupportedPlatformNames()) {
				//EditorGUILayout.LabelField ("" + pf);
				var curSource = (EmojiResources.EmojiSource)EmojiResources.GetEmojiSourceForPlatform(pf);
				var emojiSource = (EmojiResources.EmojiSource)EditorGUILayout.EnumPopup( "" + pf, (System.Enum)curSource );
				if (emojiSource != curSource) {

					if( emojiSource == EmojiResources.EmojiSource.Custom && !EmojiResources.HasCustomEmojiPack ) {
						Debug.LogError ("No custom pack installed");
						return;
					}

					EmojiResources.SetEmojiSourceForPlatform( ""+pf, (EmojiResources.EmojiSource)emojiSource );
				}
			}


			bool b1 = EmojiResources.UseCustomEmojiPack && targ.CustomEmojiPackInstalled;
			//EditorGUILayout.ToggleLeft( "Custom emoji pack", EmojiResources.UseCustomEmojiPack && targ.customEmojiPackInstalled );

			#if UNITY_EDITOR_WIN 
			if( Tastybits.EmojiAssist.WinEmoji.Win32EmojiRenderer.EmojiFontInstalled ) {
				var _old = GUI.color;
				GUI.color = Color.red;
				GUILayout.Label ("Could not find emoji font on Windows.\nIf running Windows 7 try installing this update https://support.microsoft.com/en-gb/kb/2729094");
				GUI.color = _old;
			}
			#endif 
	
			#if UNITY_EDITOR_OSX
			string strRenderer = "FontRenderer";
			if( !Tastybits.EmojiAssist.OSXEmoji.EmojiFontInstalled ) {
				targ.usePluginForOSX = true;
			}
			if( !Tastybits.EmojiAssist.OSXEmoji.EmojiFontInstalled || targ.usePluginForOSX ) {
				strRenderer = "PluginRenderer";
			}
			var _old = GUI.color;
			GUI.color = Color.yellow;
			GUILayout.Label ("OSX; EmojiRenderer used: " + strRenderer );
			if( !targ.usePluginForOSX ) {
				GUILayout.Label("Skin tones will only be rendered by OSXPluginRenderer.");
			}

			GUI.color = _old;
			#endif 
			if( !Tastybits.EmojiAssist.OSXEmoji.EmojiFontInstalled ) {
				GUI.enabled=false;
			}
			var new_value = EditorGUILayout.ToggleLeft ("Use OSX Plugin", targ.usePluginForOSX );
			if (new_value != targ.usePluginForOSX) {
				targ.usePluginForOSX = new_value;
				EmojiResources.Rebuild ();
			}
			GUI.enabled = true;


			DrawUseCustomPack ( targ );



			targ.emojiHeight = EditorGUILayout.IntSlider ("Glyph resolution", targ.emojiHeight, 29, 128);
			if( targ.emojiHeight < 32 )
				targ.emojiHeight = 29;
			else if( targ.emojiHeight >= 32 && targ.emojiHeight < 64 )
				targ.emojiHeight = 32;
			else if( targ.emojiHeight >= 64 && targ.emojiHeight < 128 )
				targ.emojiHeight = 64;
			else if( targ.emojiHeight >= 128 )
				targ.emojiHeight = 128;


			var _pre = GUI.color;


			GUI.color = Color.magenta;



			if( GUILayout.Button( "Install Emoji One\n(External Custom Pack)" ) ) {
				int ok = EditorUtility.DisplayDialogComplex( "Install Emoji One?", "Emoji Assist is not affiliated with Emoji One.\nClick View License to view license before installing.", "Download", "Cancel", "Emoji-One License" );
				if (ok == 0) {
					DownloadAndSaveFile ( "https://github.com/Ranks/emojione/raw/master/assets/fonts/emojione-apple.ttf?raw=true", "emojione.ttf" );
				}
				if ( ok == 2 ) {
					Application.OpenURL ("http://emojione.com/licensing/");
				}
			}

			GUI.color = Color.magenta;

			if( GUILayout.Button( "Install Google Noto\n(External Custom Pack)" ) ) {
				int ok = EditorUtility.DisplayDialogComplex( "Install Google Noto?", "Emoji Assist is not affiliated with Google Noto.\nClick View License to view license before installing.", "Download", "Cancel", "License" );
				if (ok == 0) {
					DownloadAndSaveFile ( "https://noto-website.storage.googleapis.com/pkgs/NotoColorEmoji-unhinted.zip", "custom.zip" );
				}
				if ( ok == 2 ) {
					Application.OpenURL ("https://www.google.com/get/noto/#emoji-zsye-color");
				}
			}

			GUI.color = _pre;





		}


		void DownloadAndSaveFile( string url, string fileName ) {
			UnityEditor.EditorUtility.DisplayProgressBar ("Downloading", "Downloading " + fileName + "...", 0f);
			var www = new WWW (url);
			EditorApplication.CallbackFunction deleg = null;
			deleg = () => {
				if( !www.isDone ) {
					UnityEditor.EditorUtility.DisplayProgressBar ("Downloading", "Downloading " + fileName + "...", www.progress );
					return;
				}
				UnityEditor.EditorUtility.ClearProgressBar();
				EditorApplication.update -= deleg;
				if( string.IsNullOrEmpty(www.error) ) {

					string path = System.IO.Path.Combine( Application.dataPath, "Resources");
					path = System.IO.Path.Combine( path, "Emoji" );
					EnsurePathExists( path, Application.dataPath );


					string resultingPath = System.IO.Path.Combine( path, fileName );
					System.IO.File.WriteAllBytes( resultingPath, www.bytes );


					UnityEditor.AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

					Debug.Log("Done downloading file :" + fileName );

					if( fileName.EndsWith(".zip") ) {
						#if UNITY_EDITOR
						ZipFile zip = ZipFile.Read (resultingPath);
						zip.ExtractAll (path, ExtractExistingFileAction.OverwriteSilently);
						#endif 
						UnityEditor.AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
					}

					//Debug.Log("Emoji one installed at path\n" + path );
				} else {
					Debug.LogError( www.error );
				}

			};
			EditorApplication.update += deleg;
		}


		void DrawUseCustomPack( EmojiResources targ ) {
			targ.useCustomEmojiPack = EditorGUILayout.ToggleLeft( "Use Custom Emoji Pack", targ.useCustomEmojiPack );
			string lastEmojiPack = targ.customPackTTFFile;
			var installed = targ.CustomEmojiPackInstalled;
			if( targ.useCustomEmojiPack ) {
				var pre = GUI.color;


				if( lastEmojiPack != targ.customPackTTFFile  ) { // clear this...
					
				}

				if( !installed ) {
					var _pre = GUI.color;
					GUI.color = Color.red;

					GUILayout.TextArea( "No custom emoji pack is installed\n"+
						"Custom Packs consists of an emoji TTF or emoji image files put into this folder:\n"+
						"Assets/Resources/Emoji/\n"+
						"\n"+
						"To download a custom emojipack goto emoji one and download their emoji pack:\n" +
						"http://emojione.com/wp-content/uploads/assets/e1-png.zip\n" +
						"\n" +
						"You can also drop some emoji TTF files into the folder\n" +
						"Click 'Install Emoji One' to download emoji one and install it."
					);
					GUI.color = _pre;
				} else {

					var _green = GUI.color;
					GUI.color = Color.green;
					GUILayout.Label ("Custom pack installed: " + System.IO.Path.GetFileName (targ.customPackTTFFile) + "\nttf:" + (EmojiResources.CustomPackIsTTF));
					GUI.color = _green;
				}

				GUI.color = pre;

			} else {

				if (installed) {

					var _green = GUI.color;
					GUI.color = Color.green;
					GUILayout.Label ("Custom pack installed: " + System.IO.Path.GetFileName (targ.customPackTTFFile) + "\nttf:" + (EmojiResources.CustomPackIsTTF) );
					GUI.color = _green;

				} else {
					var _pre = GUI.color;

					GUI.color = Color.yellow;
					GUILayout.Label ("No Custom pack installed.");
					GUI.color = _pre;
				}
					
				CustomEmojiPack.fontTextures.Clear();
			}
		}


		private static void EnsurePathExists(string path, string baseFolderPath)
		{
			var childPath = path.Replace(baseFolderPath, string.Empty);
			var subdirectories = childPath.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
			var currentDirectory = baseFolderPath;
			var currentDirectoryForReport = string.Empty;
			foreach (var subdirectory in subdirectories)
			{
				currentDirectory = Path.Combine(currentDirectory, subdirectory);
				currentDirectoryForReport += Path.Combine(currentDirectoryForReport, subdirectory);
				if (!Directory.Exists(currentDirectory))
				{
					Console.Write("\tcreating " + currentDirectoryForReport + " ...");
					Directory.CreateDirectory(currentDirectory);
					Console.WriteLine(" OK");
				}
			}
		}


	}


}