#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN 
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace Tastybits.EmojiAssist.WinEmoji {


#if UNITY_EDITOR_WIN 
	[UnityEditor.InitializeOnLoad]
#endif 
	public static class Win32EmojiRendererDLL 
	{
		public static System.IntPtr hModule = System.IntPtr.Zero;


		static bool is64BitProcess = (System.IntPtr.Size == 8);
		static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64();

		[DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool IsWow64Process(
			[In] System.IntPtr hProcess,
			[Out] out bool wow64Process
		);

		public static bool InternalCheckIsWow64()
		{
			if ((System.Environment.OSVersion.Version.Major == 5 && System.Environment.OSVersion.Version.Minor >= 1) ||
				System.Environment.OSVersion.Version.Major >= 6)
			{
				using (var p = System.Diagnostics.Process.GetCurrentProcess())
				{
					bool retVal;
					if (!IsWow64Process(p.Handle, out retVal))
					{
						return false;
					}
					return retVal;
				}
			}
			else
			{
				return false;
			}
		}



		static Win32EmojiRendererDLL() {
			/*long longVal = 0;
			Debug.Log ("UnityEditor.EditorApplication.timeSinceStartup = " + UnityEditor.EditorApplication.timeSinceStartup);
			if (UnityEditor.EditorApplication.timeSinceStartup > 5) {
				if( !long.TryParse (PlayerPrefs.GetString ("hModule", "0"), out longVal ) || longVal == 0 ) {
					longVal = 0;
					PlayerPrefs.SetString ("hModule", "0");
					LoadDLL ();
				}
				hModule = new System.IntPtr (longVal);
			} else {
				LoadDLL ();
			}*/
			LoadDLL ();
		} 

		  
		public static void LoadDLL() {
			//Debug.Log ("Loading dll");
			string pfm = "win32";
			if (InternalCheckIsWow64 () || is64BitProcess )
				pfm = "x64";
			string path = Application.dataPath.Replace("/","\\") + "\\Tastybits\\EmojiAssist\\Native\\Windows\\"+pfm+"\\EmojiRenderer.dll";
			//Debug.Log ("loading dll at path : " + path);
			hModule = Win32DLL.LoadLibrary( path );
			if (System.IO.File.Exists (path) == false) {
				Debug.LogError ("Library does not exists at path : " + path);
			}
			if (hModule.ToInt64 () == 0) {
				Debug.LogError ("error loading library at path\n" + path );
			}
			//PlayerPrefs.SetString ("hModule", "" + hModule.ToInt64() );
			//Debug.Log ("hModule = " + hModule.ToInt64() );
		}

		    
		public static void Unload(){
			Win32DLL.FreeLibrary( Win32EmojiRendererDLL.hModule );
			Win32EmojiRendererDLL.hModule = System.IntPtr.Zero;
			//PlayerPrefs.SetString ("hModule", "0");
		}

		[DllImportAttribute("EmojiRenderer.dll", CharSet = CharSet.Unicode, EntryPoint="RenderEmoji", CallingConvention=CallingConvention.StdCall)]
		static extern int _RenderEmoji(
			[MarshalAsAttribute(UnmanagedType.LPWStr)] System.Text.StringBuilder emoji, 
			[In, MarshalAs(UnmanagedType.LPArray)] byte[] inBuf,
			int nbytes 
		); 

		public static byte[] RenderEmoji( int codePoint ) {
			if( hModule == System.IntPtr.Zero ) {
				//Debug.Log ("Before load dll");
				LoadDLL ();
				//	Debug.Log ("after load dll");
			}
			int buflen = 512 * 512 * 4;
			var buf = new byte[buflen];
			var sb = new System.Text.StringBuilder ();
			string ch = System.Char.ConvertFromUtf32 (codePoint);
			sb.Append ( ch );
			//Debug.Log ("Before render");
			int numBytes = _RenderEmoji( sb, buf, buflen );
			//Debug.Log ("AFter render");
			System.Array.Resize (ref buf, numBytes);
			//var bytes_png = new byte[numBytes]; // resize the array.
			//System.Array.Copy ( buf, bytes_png, numBytes );
			//buf = null;
			return buf;
		}


	}




	  
	public class Win32EmojiRenderer /*: MonoBehaviour */{
		void Start () {
		}
		public int emoji = 0x1f602;


		public static bool EmojiFontInstalled {
			get {
				if( System.IO.File.Exists(@"C:\Windows\Fonts\seguiemj.ttf") || System.IO.File.Exists(@"C:\Windows\Fonts\segoeui.ttf") || System.IO.File.Exists(@"C:\Windows\Fonts\seguisym.ttf" ) ) {
					return true;
				}
				return false;
			}
		}


		public static Texture2D GetGlyphForChar( string ch, int codePoint ) {

			var tex = new Texture2D (2, 2);

			if( !EmojiFontInstalled ) {
//				Debug.LogError("Win32EmojiRenderer: c:\Windows\Fonts\seguiemj.ttf not installed in Windows.\nIf this is Windows 7 you might have to install an update to show emojis : https://support.microsoft.com/en-gb/kb/2729094");
				tex.SetPixels( new Color[]{Color.red,Color.red,Color.red,Color.red} ); 
				tex.Apply();
				return tex;
			}

			var bytes_png = Win32EmojiRendererDLL.RenderEmoji (codePoint);
			tex.LoadImage( bytes_png );
			tex.Apply ();

			//System.IO.File.WriteAllBytes( Application.dataPath + "/LastEmoji.png", tex.EncodeToPNG() );
			//	UnityEditor.AssetDatabase.Refresh (UnityEditor.ImportAssetOptions.ForceUpdate);

			return tex;
		}
	}  
	 


	public class HexadecimalEncoding {
		public static string ToHexString(string str) {
			var sb = new System.Text.StringBuilder();
			var bytes = System.Text.Encoding.Unicode.GetBytes(str);
			foreach (var t in bytes) {
				sb.Append(t.ToString("X2"));
			}
			return sb.ToString(); // returns: "48656C6C6F20776F726C64" for "Hello world"
		}

		public static string FromHexString(string hexString) {
			var bytes = new byte[hexString.Length / 2];
			for (var i = 0; i < bytes.Length; i++) {
				bytes[i] = System.Convert.ToByte(hexString.Substring(i * 2, 2), 16);
			}
			return System.Text.Encoding.Unicode.GetString(bytes); // returns: "Hello world" for "48656C6C6F20776F726C64"
		}
	}

	  
/*	[UnityEditor.CustomEditor(typeof(Win32EmojiRenderer))]
	public class Win32EmojiRendererEditor: UnityEditor.Editor {
		public override void OnInspectorGUI() {
			base.OnInspectorGUI();
			var targ = this.target as Win32EmojiRenderer;

			if (GUILayout.Button ("render")) {
				Debug.Log ("Testing");
				//string codepoint =  System.Char.ConvertFromUtf32 (0x1f602);
				//var strbld = new System.Text.StringBuilder ();
				//strbld.Append (codepoint);

				var bytes_png = Win32EmojiRendererDLL.RenderEmoji (targ.emoji);
				Debug.Log ("bytes_png = " + bytes_png.Length);

				var tex = new Texture2D (2, 2);
				tex.LoadImage( bytes_png );
				tex.Apply ();
				System.IO.File.WriteAllBytes( Application.dataPath + "/SavedEmoji.png", tex.EncodeToPNG() );
				UnityEditor.AssetDatabase.Refresh (UnityEditor.ImportAssetOptions.ForceUpdate);
				//Debug.Log("Ret = " + numBytes  );
			}  
			string hex = ((int)targ.emoji).ToString ("X");
			hex = UnityEditor.EditorGUILayout.TextField ("unicode", hex );
			try {
				int value = System.Convert.ToInt32(hex, 16);
				targ.emoji = value;
			}catch {
			}

			if( GUILayout.Button ("unload dll") ) {
				Debug.Log ("unloading dll");
				Win32EmojiRendererDLL.Unload ();
			}
			if( GUILayout.Button ("load dll") ) {
				Debug.Log ("loading dll");
				Win32EmojiRendererDLL.LoadDLL ();
			}
		}
	}*/


}
#endif
