﻿using UnityEngine;
using System.Collections;
using System.Linq;


namespace Tastybits.EmojiAssist {


	[ExecuteInEditMode]
	public class EmojiResources : MonoBehaviour {
		public static EmojiResources instance;
		EmojiResources():base(){
		}

		void Awake() {
			if (Application.isPlaying == false)
				return;
			if (instance != null && instance != this) {
				Debug.LogError ("cannot load more than one instance of an EmojiResources");
				Component.DestroyImmediate (this);
				return;
			}
			this.gameObject.transform.parent = null;
			GameObject.DontDestroyOnLoad (this);
			GameObject.DontDestroyOnLoad (this.gameObject);
			instance = this;
		}

		void Update(){
			if (Application.isPlaying) {
				instance = this;
			} else {
				if( instance == null ) 
					instance = this;
			}
		}

		//public bool useEmojione = false;
		public bool useIOSEmojis = true;
		public bool useCustomEmojiPack = false;
		public bool? customEmojiPackInstalled = null;
		public string customPackTTFFile = "";

		public int emojiHeight = 64;

		public bool usePluginForOSX = true;

		public enum EmojiSource
		{
			Native = 0,
			Custom = 1,
			Fallback = 2
		}

		[System.Serializable]
		public class EmojiSources
		{
			public EmojiSource EditorOSX = EmojiSource.Native;
			public EmojiSource EditorWin = EmojiSource.Native;
			public EmojiSource OSX = EmojiSource.Native;
			public EmojiSource Win = EmojiSource.Native;
			public EmojiSource iOS = EmojiSource.Native;
			public EmojiSource Android = EmojiSource.Native;
		}

		// holds the emoji sources.
		public EmojiSources sources = new EmojiSources();


		// true if we want to use the custom emoji
		// packs.
		public static bool UseCustomEmojiPack {
			get {
				if( instance==null ) return false;
				return instance.useCustomEmojiPack;
			}
		}
			

		// true if we want to use the custom emoji
		// packs.
		public static string CustomPackTTFFile {
			get {
				if( instance == null ) return "";
				return instance.customPackTTFFile;
			}
		}


		// true if we want to use the custom emoji
		// packs.
		public static bool CustomPackIsTTF {
			get {
				if( instance == null ) return false;
				return !string.IsNullOrEmpty(instance.customPackTTFFile);
			}
		}


		public enum SupportedPlatforms {
			EditorOSX,
			EditorWin,
			OSX,
			Win,
			iOS,
			Android
		}


		public static SupportedPlatforms[] GetSupportedPlatforms() {
			SupportedPlatforms[] ret = new SupportedPlatforms[] {
				SupportedPlatforms.EditorOSX,
				SupportedPlatforms.EditorWin,
				SupportedPlatforms.OSX,
				SupportedPlatforms.Win,
				SupportedPlatforms.iOS,
				SupportedPlatforms.Android
			};
			return ret;
		}


		public static EmojiSource GetEmojiSourceForPlatform( string str_platform ) {
			if( str_platform == "Editor (OSX)" ) {
				return instance.sources.EditorOSX;
			} else if( str_platform == "Editor (Win)" ) {
				return instance.sources.EditorWin;
			} else if( str_platform == "OSX" ) {
				return instance.sources.OSX;
			} else if( str_platform == "Win" ) {
				return instance.sources.Win;
			} else if( str_platform == "iOS" ) {
				return instance.sources.iOS;
			} else if( str_platform == "Android" ) {
				return instance.sources.Android;
			}
			throw new System.Exception("cannot return emoji source for undefined platform : " + str_platform );
		}


		public static void Rebuild() {
			instance.customEmojiPackInstalled = null;
			EmojiFontCache.Clear ();
			EmojiFontCache.SetDirty ();
			Canvas.ForceUpdateCanvases ();
			var elems = Component.FindObjectsOfType<EmojiSupport> ();
			foreach (var elem in elems) {
				elem.enabled = false;
				elem.enabled = true;
			}
			EmojiFontCache.Rebuild ();
		}


		public static void SetEmojiSourceForPlatform( string str_platform, EmojiSource src ) {
			if( str_platform == "Editor (OSX)" ) {
				instance.sources.EditorOSX = src; Rebuild (); return;
			} else if( str_platform == "Editor (Win)" ) {
				instance.sources.EditorWin = src; Rebuild (); return;
			} else if( str_platform == "OSX" ) {
				instance.sources.OSX = src; Rebuild (); return;
			} else if( str_platform == "Win" ) {
				instance.sources.Win = src; Rebuild (); return;
			} else if( str_platform == "iOS" ) {
				instance.sources.iOS = src; Rebuild (); return;
			} else if( str_platform == "Android" ) {
				instance.sources.Android = src; Rebuild (); return;
			}
			throw new System.Exception("cannot set emoji source for undefined platform : " + str_platform );
		}


		public static string[] GetSupportedPlatformNames() {
			string[] ret = new string[] {
				"Editor (OSX)",
				"Editor (Win)",
				"OSX",
				"Win",
				"iOS",
				"Android"
			};
			return ret;
		}


		public static bool UseCustomEmojiPackForPlatform( UnityEngine.RuntimePlatform platform ) {
			if( instance==null ) return false;

			if (platform == RuntimePlatform.OSXEditor) {
				return instance.sources.EditorOSX == EmojiSource.Custom;
			} else if (platform == RuntimePlatform.WindowsEditor) {
				return instance.sources.EditorWin == EmojiSource.Custom;
			} else if (platform == RuntimePlatform.OSXPlayer) {
				return instance.sources.OSX == EmojiSource.Custom;
			} else if (platform == RuntimePlatform.WindowsPlayer) {
				return instance.sources.Win == EmojiSource.Custom;
			} else if (platform == RuntimePlatform.IPhonePlayer) {
				return instance.sources.iOS == EmojiSource.Custom;
			} else if (platform == RuntimePlatform.Android) {
				return instance.sources.Android == EmojiSource.Custom;
			}

			return false;
		}



		public static bool UseFallbackRendererForPlatform( UnityEngine.RuntimePlatform platform ) {
			if( instance==null ) return false;

			if (platform == RuntimePlatform.OSXEditor) {
				return instance.sources.EditorOSX == EmojiSource.Fallback;
			} else if (platform == RuntimePlatform.WindowsEditor) {
				return instance.sources.EditorWin == EmojiSource.Fallback;
			} else if (platform == RuntimePlatform.OSXPlayer) {
				return instance.sources.OSX == EmojiSource.Fallback;
			} else if (platform == RuntimePlatform.WindowsPlayer) {
				return instance.sources.Win == EmojiSource.Fallback;
			} else if (platform == RuntimePlatform.IPhonePlayer) {
				return instance.sources.iOS == EmojiSource.Fallback;
			} else if (platform == RuntimePlatform.Android) {
				return instance.sources.Android == EmojiSource.Fallback;
			}

			return false;
		}






		public static bool UseWinEmojis {
			get {
				return (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) && !UseCustomEmojiPack;
			}
		}


		public static bool UseIOSEmojis {
			get {
				if( !(Application.platform == RuntimePlatform.IPhonePlayer) ) {
					return false;
				}
				bool useIOS=true;
				if( instance!=null ) {
					useIOS = instance.useIOSEmojis;
				}
				return useIOS;
			}
		}


		public static bool IsOSXEditorOrOSXApp {
			get {
				bool yes = Application.isEditor && Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer;
				return yes;
			}
		}



		public static bool ISWinEditorOrWinApp {
			get {
				bool yes = Application.isEditor && Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer;
				return yes;
			}
		}

		public static bool UseAndroidEmojis {
			get {
				return Application.platform == RuntimePlatform.Android && !UseCustomEmojiPack;
			}
		}


		public static bool HasCustomEmojiPack {
			get {
				return instance.CustomEmojiPackInstalled;
			}
		}


		public bool CustomEmojiPackInstalled {
			get {
				if (this.customEmojiPackInstalled != null) {
					return (bool)this.customEmojiPackInstalled;
				}

				string basePath = System.IO.Path.Combine (Application.dataPath, "Resources");
				basePath = System.IO.Path.Combine (basePath, "Emoji");
				this.customEmojiPackInstalled = null;
				this.customPackTTFFile = "";

				if( System.IO.File.Exists( System.IO.Path.Combine (basePath, "1f69f.png") ) ) {
					this.customEmojiPackInstalled = true;
					return true;
				}

				var tmp = new string[]{
					"custom.ttf",
					"emoji.ttf",
					"AppleColorEmoji.ttf",
					"Apple Color Emoji.ttf",
					"NotoColorEmoji.ttf",
					"emojione-apple.ttf",
					"emojione-android.ttf",
					"emojione.ttf",
					"AndroidEmoji.ttf",
				};

				foreach (var str in tmp) {
					if( System.IO.File.Exists(System.IO.Path.Combine (basePath, str)) ) {
						this.customEmojiPackInstalled = true;
						this.customPackTTFFile = System.IO.Path.Combine (basePath, str);
						return true;
					}
				}


				this.customEmojiPackInstalled = false;
				this.customPackTTFFile = "";
				return false;
			}
		}


		public static Texture2D RenderGlyph( string ch, int codePoint, int emojiModifier=0, int variationSelector = 0 ) {
			int emojiHeight = instance.emojiHeight;

			//Debug.Log ("Loading emoji : " + codePoint);
			if( UseCustomEmojiPackForPlatform (Application.platform) && HasCustomEmojiPack ) {
				//Debug.Log ("Loading custom : " + codePoint);
				return CustomEmojiPack.GetGlyphForChar( ch, EmojiResources.CustomPackTTFFile );
			}
			if( UseFallbackRendererForPlatform( Application.platform) && HasCustomEmojiPack ) {
				//Debug.Log ("Loading custom : " + codePoint);
				return FallbackEmojiRenderer.GetEmojiFromUnicodeChar (ch, codePoint);
			}

			if (Application.platform == RuntimePlatform.Android && 
				instance.sources.Android == EmojiSource.Native && !string.IsNullOrEmpty(Tastybits.EmojiAssist.AndroidEmoji.FontFilePath) ) 
			{
				var ret =  Tastybits.EmojiAssist.AndroidEmoji.GetGlyphForChar( ch );
				if (ret != null) {
					return ret;
				}
				// retry with fallback or custom emoji..
			}

			if (Application.platform == RuntimePlatform.IPhonePlayer && 
				instance.sources.iOS == EmojiSource.Native ) 
			{
				return iOSEmoji.GetGlyphForChar (ch, emojiHeight,emojiModifier,variationSelector);
			}

			if (Application.isEditor && Application.platform == RuntimePlatform.WindowsEditor && 
				instance.sources.EditorWin == EmojiSource.Native ) 
			{
				#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN 
				if( Tastybits.EmojiAssist.WinEmoji.Win32EmojiRenderer.EmojiFontInstalled ) {
					return Tastybits.EmojiAssist.WinEmoji.Win32EmojiRenderer.GetGlyphForChar( ch, codePoint );
				}
				#endif 
			}


			if( Application.platform == RuntimePlatform.WindowsPlayer && 
				instance.sources.Win == EmojiSource.Native )
			{
				
				#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN 
				if( Tastybits.EmojiAssist.WinEmoji.Win32EmojiRenderer.EmojiFontInstalled ) {
					return Tastybits.EmojiAssist.WinEmoji.Win32EmojiRenderer.GetGlyphForChar( ch, codePoint );
				}
				#endif 
			}

			if (Application.isEditor && Application.platform == RuntimePlatform.OSXEditor && 
				instance.sources.EditorOSX == EmojiSource.Native ) 
			{
				return Tastybits.EmojiAssist.OSXEmoji.GetGlyphForChar (ch, emojiHeight, instance.usePluginForOSX, emojiModifier, variationSelector );

			}

			if( Application.platform == RuntimePlatform.OSXPlayer &&
				instance.sources.OSX == EmojiSource.Native )
			{
				return Tastybits.EmojiAssist.OSXEmoji.GetGlyphForChar( ch, emojiHeight, instance.usePluginForOSX, emojiModifier, variationSelector );
			} 


			if( HasCustomEmojiPack ) {
				//Debug.Log ("Loading custom : " + codePoint);
				return CustomEmojiPack.GetGlyphForChar( ch, EmojiResources.CustomPackTTFFile );
			}

				
			return FallbackEmojiRenderer.GetEmojiFromUnicodeChar (ch, codePoint);
		}







	}


}