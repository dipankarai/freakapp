﻿using UnityEngine;
using System.Collections;

namespace Tastybits.EmojiAssist.TTF {
	using TTFReader = System.IO.BinaryReader;


	public class SbixTable {
		public System.UInt16 version;
		public System.UInt16 flags;
		public System.UInt32 numStrikes;
		// Offset from begining of table to data for the individual strike
		public System.UInt32[] strikesOffsets;

		public System.Collections.Generic.List<Strike> strikes = new System.Collections.Generic.List<Strike>();

		/*public void Lookup( int glyphId ) {
			foreach( var strike in strikes ) {
				var dataLen = strike.GetGlyphDataLen( glyphId );
				//Debug.Log( "dataLen=" + dataLen );
			}
		}*/

		public static SbixTable Read( TTFReader reader, TableRecord[] tables, BaseGlyph[] glyphs ){
			if (!SfntTables.SeekToTable(reader, tables, FourCC.sbix))
				return null;

			int numGlyphs = glyphs.Length;

			var tbl_offset = reader.GetPosition();

			SbixTable tbl = new SbixTable();
			tbl.version = reader.ReadUInt16BE();
			tbl.flags = reader.ReadUInt16BE();
			//Debug.LogError( "Flags : " + System.Convert.ToString(tbl.flags, 2) );

			tbl.numStrikes = reader.ReadUInt32BE();


			tbl.strikesOffsets = new uint[tbl.numStrikes];
			for( int i = 0; i<tbl.numStrikes; i++ ) {
				var offset = reader.ReadUInt32BE();
				//Debug.Log(" #"+i+ " strike , offset="+ offset);
				tbl.strikesOffsets[i] = offset;
			}


			for( int i = 0; i<tbl.numStrikes; i++ ) {
				var offset = tbl.strikesOffsets[i];
				var off = tbl_offset+offset;
				reader.Seek( (uint)off );
				var strike = new Strike();
				strike.ppem = reader.ReadUInt16BE();
				strike.resolution = reader.ReadUInt16BE();
				strike.strike_dataOff = (int)off;

				//Debug.LogError("Strike ppem = " +strike.ppem + " res = " + strike.resolution );

				strike.glyphDataOffset = new System.UInt32[numGlyphs];
				for( int j=0; j<numGlyphs; j++ ) {
					var glyphOff=reader.ReadUInt32BE();
					//Debug.Log( "glyph off : " + glyphOff );
					strike.glyphDataOffset[j] = glyphOff;
				}
				tbl.strikes.Add( strike );

			}
			//Debug.Log("done with the strikes");

			return tbl;
		}


		public byte[] GetBytes( TTFReader reader, int glyph_id, bool highrez=false ) {
			if( highrez ) {
				for( int s = strikes.Count-1; s >= 0; s--) {
					var strike = strikes[s];
					var bytes = strike.Read( reader, glyph_id );
					if( bytes!=null && bytes.Length > 0 ) return bytes;
				}	
			}
			foreach( var strike in strikes ) {
				var bytes = strike.Read( reader, glyph_id );
				if( bytes!=null && bytes.Length > 0 ) return bytes;
			}
			Debug.LogError("could not determine the bytes of a glyph : " + glyph_id );
			return null;
		}


		public class Strike {
			public System.UInt16 ppem;
			public System.UInt16 resolution;
			public System.UInt32[] glyphDataOffset;

			public int strike_dataOff;

			/* The length of the data for a given glyph is found by glyphDataOffset[glyphID+1] - glyphDataOffset[glyphID]. If this is zero, there is no bitmap data for that glyph in this strike. (This should always be true for non-printing glyphs such as space.) */
			public int GetGlyphDataLen( int glyphID ) {
				var len = glyphDataOffset[glyphID+1] - glyphDataOffset[glyphID];
				return (int)len;
			}
			public byte[] Read( TTFReader reader, int glyph_id ) {
				if( glyphDataOffset.Length <= glyph_id ) {
					Debug.LogError("glyphid doesn't exist");
				}
				var block_off = glyphDataOffset[glyph_id];
				var block_len = GetGlyphDataLen( glyph_id );
				if( block_off <= 0 ) {
					throw new System.Exception("invalid offset");
					return null;
				}

				//Debug.Log("block_off: " + block_off + " strike_dataOff=" + strike_dataOff  );
				var off = strike_dataOff + block_off;

				reader.Seek( (uint)off );

				var originOffsetX = reader.ReadInt16BE();
				var originOffsetY = reader.ReadInt16BE();

				var gfx_type = reader.ReadUInt32();

				//Debug.Log( "offx/y= " + originOffsetX + ", " + originOffsetY + " len=" + block_len  );
				if( gfx_type != FourCC.png_ ) {
					throw new System.Exception("unknown graphics type :"  + (new FourCC(gfx_type)).ToString() );
				}

				//reader.

				block_len -= 8;

				var b = reader.ReadBytes( block_len );
				return b;
			}
		}


	}
}
