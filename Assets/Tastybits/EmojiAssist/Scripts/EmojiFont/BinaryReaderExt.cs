﻿using UnityEngine;
using System.Collections;


namespace Tastybits.EmojiAssist.TTF {


	public static class BinaryReaderExt {

		public static void Seek( this System.IO.BinaryReader self, uint position ) {
			// if the position is within our buffer we can reuse part of it
			// otherwise, just clear everything out and jump to the right spot
			var stream = self.BaseStream;
			stream.Position = position;
		}


		public static int GetPosition( this System.IO.BinaryReader self ) {
			return (int)self.BaseStream.Position;
		}


		public static void Skip( this System.IO.BinaryReader self, int nbytes ) {
			var tmp = self.ReadBytes( nbytes );
		}


		public static System.Int16 ReadInt16BE( this System.IO.BinaryReader self ) {
			var a16 = self.ReadBytes(2);
			System.Array.Reverse(a16);
			return System.BitConverter.ToInt16(a16, 0);
		}
		public static System.UInt16 ReadUInt16BE( this System.IO.BinaryReader self ) {
			var a16 = self.ReadBytes(2);
			System.Array.Reverse(a16);
			return System.BitConverter.ToUInt16(a16, 0);
		}


		public static System.Int32 ReadInt32BE( this System.IO.BinaryReader self ) {
			var a32 = self.ReadBytes(4);
			System.Array.Reverse(a32);
			return System.BitConverter.ToInt32(a32, 0);
		}
		public static System.UInt32 ReadUInt32BE( this System.IO.BinaryReader self ) {
			var a32 = self.ReadBytes(4);
			System.Array.Reverse(a32);
			return System.BitConverter.ToUInt32(a32, 0);
		}

		public static System.UInt32 ReadFixedBE( this System.IO.BinaryReader self ) {
			var a32 = self.ReadBytes(4);
			System.Array.Reverse(a32);
			return System.BitConverter.ToUInt32(a32, 0);
		}


		public static System.UInt32 ReadULongBE( this System.IO.BinaryReader self ) {
			var a32 = self.ReadBytes(4);
			System.Array.Reverse(a32);
			return System.BitConverter.ToUInt32(a32, 0);
		}


		public static System.Int64 ReadInt64BE( this System.IO.BinaryReader self ) {
			var a64 = self.ReadBytes(8);
			System.Array.Reverse(a64);
			return System.BitConverter.ToInt64(a64, 0);
		}
		public static System.UInt64 ReadUInt64BE( this System.IO.BinaryReader self ) {
			var a64 = self.ReadBytes(8);
			System.Array.Reverse(a64);
			return System.BitConverter.ToUInt64(a64, 0);
		}

	}


}