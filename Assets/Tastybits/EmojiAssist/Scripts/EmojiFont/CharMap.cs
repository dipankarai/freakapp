﻿using UnityEngine;
using System.Collections;

namespace Tastybits.EmojiAssist.TTF {
	using TTFReader = System.IO.BinaryReader;

		
	public class CharacterMap {
		System.Collections.Generic.Dictionary<CodePoint, int> table;

		CharacterMap (System.Collections.Generic.Dictionary<CodePoint, int> table) {
			this.table = table;
		}

		public int Lookup (CodePoint codePoint) {
			int index;
			if (table.TryGetValue(codePoint, out index))
				return index;
			return -1;
		}


		public static CharacterMap ReadCmap( TTFReader reader, TableRecord[] tables ) {
			SfntTables.SeekToTable( reader, tables, FourCC.Cmap, required: true );

			// skip version
			var cmapOffset = reader.GetPosition();
			reader.Skip(sizeof(short));

			// read all of the subtable headers
			var subtableCount = reader.ReadUInt16BE();
			var subtableHeaders = new CmapSubtableHeader[subtableCount];
			for (int i = 0; i < subtableHeaders.Length; i++) {
				subtableHeaders[i] = new CmapSubtableHeader {
					PlatformID = reader.ReadUInt16BE(),
					EncodingID = reader.ReadUInt16BE(),
					Offset = reader.ReadUInt32BE()
				};
			}


			// search for a "full" Unicode table first
			var chosenSubtableOffset = 0u;
			for( int i = 0; i < subtableHeaders.Length; i++ ) {
				var platform = subtableHeaders[i].PlatformID;
				var encoding = subtableHeaders[i].EncodingID;
				if( (platform == PlatformID.Microsoft && encoding == WindowsEncoding.UnicodeFull ) ||
					(platform == PlatformID.Unicode && encoding == UnicodeEncoding.Unicode32) ) {
					chosenSubtableOffset = subtableHeaders[i].Offset;
					break;
				}
			}

			// if no full unicode table, just grab the first
			// one that supports any flavor of Unicode
			if( chosenSubtableOffset == 0 ) {
				for (int i = 0; i < subtableHeaders.Length; i++) {
					var platform = subtableHeaders[i].PlatformID;
					var encoding = subtableHeaders[i].EncodingID;
					if ((platform == PlatformID.Microsoft && encoding == WindowsEncoding.UnicodeBmp) ||
						platform == PlatformID.Unicode) 
					{
						chosenSubtableOffset = subtableHeaders[i].Offset;
						break;
					}
				}
			}

			//Debug.Log("test");

			// no unicode support at all is an error
			if( chosenSubtableOffset == 0 ){
				throw new System.Exception("Font does not support Unicode.");
			}

			for (int i = 0; i < subtableHeaders.Length; i++) {
				reader.Seek( (uint)( cmapOffset + subtableHeaders[i].Offset ) );
				var fmt =reader.ReadUInt16BE();
				//Debug.Log("Cmap sub header : " + subtableHeaders[i].EncodingID + " format : " + fmt );
			}


			// jump to our chosen table and find out what format it's in
			var off = cmapOffset + chosenSubtableOffset;
			if( off < 0 ) Debug.LogError("Error offset is less than zero");
			reader.Seek( (uint)( off ) );
			var format = reader.ReadUInt16BE();
			switch( format ) {
				case 4: 
					return ReadCmapFormat4(reader);
				case 12:
					return ReadCmapFormat12(reader);	
				default: 
					throw new System.Exception("Unsupported cmap format. : " +  format );
			}
		}


		static CharacterMap ReadCmapFormat12( TTFReader reader ) {
			//Debug.Log("ReadCmapFormat12");
			reader.Skip(2); // this one has a 2 byte header...
			var length = reader.ReadUInt32BE();
			var language = reader.ReadUInt32BE();
			var nGroups = reader.ReadUInt32BE();


			//Debug.Log("Got number of groups :" + nGroups + " length =" + length + " lang=" + language );

			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );
			//Debug.Log("next val = " + reader.ReadUInt32BE().ToString("X") );

			var table = new System.Collections.Generic.Dictionary<CodePoint, int>();
			for( int i = 0; i<nGroups; i++ ) {
				var startCharCode = reader.ReadUInt32BE();
				var endCharCode = reader.ReadUInt32BE();
				var startGlyphCode = reader.ReadUInt32BE();

				var glyphOff = startGlyphCode;
				for( uint ch = startCharCode; ch < endCharCode; ch++ ) {
					table.Add( new CodePoint((int)ch), (int)glyphOff );
					glyphOff++;
				}
				//var glyphIndex = (glyphId + delta) & 0xFFFF;
				//if (glyphIndex != 0)
				//var codepoint = new CodePoint(  );
				//table.Add( (CodePoint)codepoint, glyphIndex);
				//Debug.Log( "Glyph #" + i + " - startCharCode:"  + startCharCode.ToString("X") + " endCharCode:"+endCharCode.ToString("X")+  " startGlyphCode:" + startGlyphCode );
			}

			return new CharacterMap(table);
		}


		static CharacterMap ReadCmapFormat4 (TTFReader reader) {
			// skip over length and language
			reader.Skip(sizeof(short) * 2);

			// figure out how many segments we have
			var segmentCount = reader.ReadUInt16BE() / 2;
			if (segmentCount > MaxSegments)
				throw new System.Exception("Too many cmap segments.");

			// skip over searchRange, entrySelector, and rangeShift
			reader.Skip(sizeof(short) * 3);

			// read in segment ranges
			var endCount = new int[segmentCount];
			for (int i = 0; i < segmentCount; i++) {
				endCount[i] = reader.ReadUInt16BE();
			}

			reader.Skip(sizeof(short));     // padding

			var startCount = new int[segmentCount];
			for (int i = 0; i < segmentCount; i++)
				startCount[i] = reader.ReadUInt16BE();

			var idDelta = new int[segmentCount];
			for (int i = 0; i < segmentCount; i++)
				idDelta[i] = reader.ReadInt16BE();

			// build table from each segment
			var table = new System.Collections.Generic.Dictionary<CodePoint, int>();
			for (int i = 0; i < segmentCount; i++) {
				// read the "idRangeOffset" for the current segment
				// if nonzero, we need to jump into the glyphIdArray to figure out the mapping
				// the layout is bizarre; see the OpenType spec for details
				var idRangeOffset = reader.ReadUInt16BE();
				if (idRangeOffset != 0) {
					var currentOffset = reader.GetPosition();
					var off = currentOffset + idRangeOffset - sizeof(ushort);
					reader.Seek( (uint)off );

					var end = endCount[i];
					var delta = idDelta[i];
					for (var codepoint = startCount[i]; codepoint <= end; codepoint++) {
						var glyphId = reader.ReadUInt16BE();
						if (glyphId != 0) {
							var glyphIndex = (glyphId + delta) & 0xFFFF;
							if (glyphIndex != 0)
								table.Add( (CodePoint)codepoint, glyphIndex);
						}
					}

					reader.Seek( (uint)currentOffset );
				}
				else {
					// otherwise, do a straight iteration through the segment
					var end = endCount[i];
					var delta = idDelta[i];
					for (var codepoint = startCount[i]; codepoint <= end; codepoint++) {
						var glyphIndex = (codepoint + delta) & 0xFFFF;
						if (glyphIndex != 0)
							table.Add((CodePoint)codepoint, glyphIndex);
					}
				}
			}

			return new CharacterMap(table);
		}

		const int MaxSegments = 1024;

		struct CmapSubtableHeader {
			public int PlatformID;
			public int EncodingID;
			public uint Offset;
		}
	}

}