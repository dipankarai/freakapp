﻿using UnityEngine;
using System.Collections;


namespace Tastybits.EmojiAssist.TTF {
	using TTFReader = System.IO.BinaryReader;


	public class SbitTable {
		public static SbitTable Read (TTFReader reader, TableRecord[] tables) {
			if (!SfntTables.SeekToTable(reader, tables, FourCC.Eblc))
				return null;

			// skip version
			var baseOffset = reader.GetPosition();
			reader.Skip(sizeof(int));

			// load each strike table
			var count = reader.ReadInt32BE();
			if (count > MaxBitmapStrikes)
				throw new System.Exception("Too many bitmap strikes in font.");

			var sizeTableHeaders = new BitmapSizeTable[count];
			for (int i = 0; i < count; i++) {
				sizeTableHeaders[i].SubTableOffset = reader.ReadUInt32BE();
				sizeTableHeaders[i].SubTableSize = reader.ReadUInt32BE();
				sizeTableHeaders[i].SubTableCount = reader.ReadUInt32BE();

				// skip colorRef, metrics entries, start and end glyph indices
				reader.Skip(sizeof(uint) + sizeof(ushort) * 2 + 12 * 2);

				sizeTableHeaders[i].PpemX = reader.ReadByte();
				sizeTableHeaders[i].PpemY = reader.ReadByte();
				sizeTableHeaders[i].BitDepth = reader.ReadByte();
				sizeTableHeaders[i].Flags = (BitmapSizeFlags)reader.ReadByte();
			}

			// read index subtables
			var indexSubTables = new IndexSubTable[count];
			for (int i = 0; i < count; i++) {
				var off = baseOffset + sizeTableHeaders[i].SubTableOffset;
				reader.Seek( (uint)off );
				indexSubTables[i] = new IndexSubTable {
					FirstGlyph = reader.ReadUInt16BE(),
					LastGlyph = reader.ReadUInt16BE(),
					Offset = reader.ReadUInt32BE()
				};
			}

			// read the actual data for each strike table
			for (int i = 0; i < count; i++) {
				// read the subtable header
				var off = baseOffset + sizeTableHeaders[i].SubTableOffset + indexSubTables[i].Offset;
				reader.Seek((uint)off);
				var indexFormat = reader.ReadUInt16BE();
				var imageFormat = reader.ReadUInt16BE();
				var imageDataOffset = reader.ReadUInt32BE();


			}

			return null;
		}

		public struct BitmapSizeTable {
			public uint SubTableOffset;
			public uint SubTableSize;
			public uint SubTableCount;
			public byte PpemX;
			public byte PpemY;
			public byte BitDepth;
			public BitmapSizeFlags Flags;
		}

		public struct IndexSubTable {
			public ushort FirstGlyph;
			public ushort LastGlyph;
			public uint Offset;
		}

		[System.Flags]
		public enum BitmapSizeFlags {
			None,
			Horizontal,
			Vertical
		}

		const int MaxBitmapStrikes = 1024;
	}

}
