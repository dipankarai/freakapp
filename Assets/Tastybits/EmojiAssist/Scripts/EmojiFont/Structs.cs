﻿using UnityEngine;
using System.Collections;


namespace Tastybits.EmojiAssist.TTF {
	using TTFReader = System.IO.BinaryReader;

	public struct FourCC {
		uint value;
		public FourCC (uint value) {
			this.value = value;
		}

		public FourCC (string str) {
			if (str.Length != 4)
				throw new System.Exception("Invalid FourCC code");
			value = str[0] | ((uint)str[1] << 8) | ((uint)str[2] << 16) | ((uint)str[3] << 24);
		}
		public override string ToString () {
			return new string(new[] {
				(char)(value & 0xff),
				(char)((value >> 8) & 0xff),
				(char)((value >> 16) & 0xff),
				(char)(value >> 24)
			});
		}
		public static implicit operator FourCC (string value) { return new FourCC(value); }
		public static implicit operator FourCC (uint value) { return new FourCC(value); }
		public static implicit operator uint (FourCC fourCC) { return fourCC.value; }


		public static readonly FourCC CBDT = "CBDT";
		public static readonly FourCC CBLC = "CBLC";



		public static readonly FourCC Otto = "OTTO";
		public static readonly FourCC True = "true";
		public static readonly FourCC Ttcf = "ttcf";
		public static readonly FourCC Typ1 = "typ1";
		public static readonly FourCC Head = "head";
		public static readonly FourCC Maxp = "maxp";
		public static readonly FourCC Post = "post";
		public static readonly FourCC OS_2 = "OS/2";
		public static readonly FourCC Hhea = "hhea";
		public static readonly FourCC Hmtx = "hmtx";
		public static readonly FourCC Vhea = "vhea";
		public static readonly FourCC Vmtx = "vmtx";
		public static readonly FourCC Loca = "loca";
		public static readonly FourCC Glyf = "glyf";
		public static readonly FourCC Cmap = "cmap";
		public static readonly FourCC Kern = "kern";
		public static readonly FourCC Name = "name";
		public static readonly FourCC Cvt = "cvt ";
		public static readonly FourCC Fpgm = "fpgm";
		public static readonly FourCC Prep = "prep";
		public static readonly FourCC Eblc = "EBLC";
		public static readonly FourCC sbix = "sbix";

		public static readonly FourCC jpg_ = "jpg ";
		public static readonly FourCC pdf_ = "pdf ";
		public static readonly FourCC png_ = "png ";
		public static readonly FourCC tiff = "tiff";

	}


	[System.Flags]
	public enum HeadFlags {
		None = 0,
		SimpleBaseline = 0x1,
		SimpleLsb = 0x2,
		SizeDependentInstructions = 0x4,
		IntegerPpem = 0x8,
		InstructionsAlterAdvance = 0x10
	}

	public enum IndexFormat {
		Short,
		Long
	}

	public struct FaceHeader {
		public HeadFlags Flags;
		public int UnitsPerEm;
		public IndexFormat IndexFormat;
		public int UnderlinePosition;
		public int UnderlineThickness;
		public bool IsFixedPitch;
		public int GlyphCount;
		public int MaxTwilightPoints;
		public int MaxStorageLocations;
		public int MaxFunctionDefs;
		public int MaxInstructionDefs;
		public int MaxStackSize;
	}



	public struct MetricsHeader {
		public int Ascender;
		public int Descender;
		public int LineGap;
		public int MetricCount;
	}

	public struct MetricsEntry {
		public int Advance;
		public int FrontSideBearing;
	}

	public struct OS2Data {
		public FontWeight Weight;
		public FontStretch Stretch;
		public FontStyle Style;
		public int StrikeoutSize;
		public int StrikeoutPosition;
		public int TypographicAscender;
		public int TypographicDescender;
		public int TypographicLineGap;
		public int WinAscent;
		public int WinDescent;
		public bool UseTypographicMetrics;
		public bool IsWWSFont;
		public int XHeight;
		public int CapHeight;
	}


	public static class WindowsEncoding {
		public const int UnicodeBmp = 1;
		public const int UnicodeFull = 10;
	}

	public static class UnicodeEncoding {
		public const int Unicode32 = 4;
	}

	public static class PlatformID {
		public const int Unicode = 0;
		public const int Microsoft = 3;
	}

	public struct TableRecord {
		public FourCC Tag;
		public uint CheckSum;
		public uint Offset;
		public uint Length;
		public override string ToString () { return Tag.ToString(); }
	}


	public struct FUnit {
		int value;

		public static explicit operator int (FUnit v) { return v.value; }
		public static explicit operator FUnit (int v) { return new FUnit { value = v }; }

		public static FUnit operator -(FUnit lhs, FUnit rhs) { return (FUnit)(lhs.value - rhs.value); }
		public static FUnit operator +(FUnit lhs, FUnit rhs) { return (FUnit)(lhs.value + rhs.value);}
		public static float operator *(FUnit lhs, float rhs) { return lhs.value * rhs;}

		public static FUnit Max (FUnit a, FUnit b) { return (FUnit)Mathf.Max(a.value, b.value); }
		public static FUnit Min (FUnit a, FUnit b) { return (FUnit)Mathf.Min(a.value, b.value); }
	}

	public struct NameData {
		public string FamilyName;
		public string SubfamilyName;
		public string UniqueID;
		public string FullName;
		public string Version;
		public string Description;
		public string TypographicFamilyName;
		public string TypographicSubfamilyName;
	}


	[System.Flags]
	public enum FsSelectionFlags {
		Italic = 0x1,
		Bold = 0x20,
		Regular = 0x40,
		UseTypoMetrics = 0x80,
		WWS = 0x100,
		Oblique = 0x200
	}

	public struct GlyphHeader {
		public short ContourCount;
		public short MinX;
		public short MinY;
		public short MaxX;
		public short MaxY;
	}

	public abstract class BaseGlyph {
		public byte[] Instructions;
		public int MinX;
		public int MinY;
		public int MaxX;
		public int MaxY;
	}


	public class SimpleGlyph : BaseGlyph {
		public Point[] Points;
		public int[] ContourEndpoints;
	}


	[System.Flags]
	public enum SimpleGlyphFlags {
		None = 0,
		OnCurve = 0x1,
		ShortX = 0x2,
		ShortY = 0x4,
		Repeat = 0x8,
		SameX = 0x10,
		SameY = 0x20
	}


	public enum PointType {
		OnCurve,
		Quadratic,
		Cubic
	}


	public struct PointF {
		public Vector2 P;
		public PointType Type;

		public PointF (Vector2 position, PointType type) {
			P = position;
			Type = type;
		}

		public PointF Offset (Vector2 offset) { return new PointF(P + offset, Type); }

		public override string ToString () { return "{"+P+"} ({"+Type+"})"; }

		public static implicit operator Vector2 (PointF p) { return p.P; }
	}


	public struct Point {
		public FUnit X;
		public FUnit Y;
		public PointType Type;

		public Point (FUnit x, FUnit y) {
			X = x;
			Y = y;
			Type = PointType.OnCurve;
		}

		public static PointF operator *(Point lhs, float rhs) { return new PointF(new Vector2(lhs.X * rhs, lhs.Y * rhs), lhs.Type); }

		public static explicit operator Vector2 (Point p) { return new Vector2((int)p.X, (int)p.Y); }
	}

	[System.Flags]
	public enum CompositeGlyphFlags {
		None = 0,
		ArgsAreWords = 0x1,
		ArgsAreXYValues = 0x2,
		RoundXYToGrid = 0x4,
		HaveScale = 0x8,
		MoreComponents = 0x20,
		HaveXYScale = 0x40,
		HaveTransform = 0x80,
		HaveInstructions = 0x100,
		UseMetrics = 0x200,
		ScaledComponentOffset = 0x800
	}


	public struct Subglyph {
		//public Matrix3x2 Transform;
		public Matrix4x4 Transform;
		public CompositeGlyphFlags Flags;
		public int Index;
		public int Arg1;
		public int Arg2;
	}

	class CompositeGlyph : BaseGlyph {
		public Subglyph[] Subglyphs;
	}


	public struct StringData {
		public ushort Name;
		public ushort Offset;
		public ushort Length;
	}

	public static class NameID {
		public const int FamilyName = 1;
		public const int SubfamilyName = 2;
		public const int UniqueID = 3;
		public const int FullName = 4;
		public const int Version = 5;
		public const int Description = 10;
		public const int TypographicFamilyName = 16;
		public const int TypographicSubfamilyName = 17;
	}

	/*
	ULONG	tag	4 -byte identifier.
	ULONG	checkSum	CheckSum for this table.
	ULONG	offset	Offset from beginning of TrueType font file.
	ULONG	length	Length of this table.*/
	public class OffsetTable {
		ulong ident;
		ulong checkSum;
		ulong offset;
		ulong length;
	}


	/*
	TAG	TTCTag	Font Collection ID string: 'ttcf' (used for both CFF and TrueType, for backward compatibility)
	FIXED	Version	Version of the TTC Header (1.0), 0x00010000
	ULONG	numFonts	Number of fonts in TTC
	ULONG	OffsetTable[numFonts]	Array of offsets to the OffsetTable for each font from the beginning of the fil
	*/
	public class TCCHeader {
		string tag;
		int version;
		ulong numFonts;
		ulong[] OffsetTable;
	}


	public enum FontWeight {
		Unknown = 0, // The weight is unknown or unspecified.
		Thin = 100,
		ExtraLight = 200,
		Light = 300,
		Normal = 400,
		Medium = 500,
		SemiBold = 600,
		Bold = 700,
		ExtraBold = 800,
		Black = 900
	}


	public enum FontStretch {
		Unknown,
		UltraCondensed,
		ExtraCondensed,
		Condensed,
		SemiCondensed,
		Normal,
		SemiExpanded,
		Expanded,
		ExtraExpanded,
		UltraExpanded
	}



	public enum FontStyle {
		Regular, //(No particular styles applied.)
		Bold,
		Italic,
		Oblique // (The font is algorithmically italic / angled.)
	}




	public struct CodePoint : System.IComparable<CodePoint>, System.IEquatable<CodePoint> {
		readonly int value;
		/*public CodePoint (uint codePoint) {
			value = codePoint;
		}*/	
		public CodePoint (int codePoint) {
			value = codePoint;
		}
		public CodePoint (char character) {
			value = character;
		}
		public CodePoint (char highSurrogate, char lowSurrogate) {
			value = char.ConvertToUtf32(highSurrogate, lowSurrogate);
		}
		public int CompareTo (CodePoint other) { return value.CompareTo(other.value); }
		public bool Equals (CodePoint other) { return value.Equals(other.value); }
		public override bool Equals (object obj) {
			var codepoint = obj as CodePoint?;
			if (codepoint == null)
				return false;

			return Equals(codepoint);
		}
		public override int GetHashCode () { return value.GetHashCode(); }
		public override string ToString () { return "{"+value+"} ({"+(char)value+"})"; }
		public static bool operator == (CodePoint left, CodePoint right) { return left.Equals(right); }
		public static bool operator !=(CodePoint left, CodePoint right) { return !left.Equals(right); }
		public static bool operator <(CodePoint left, CodePoint right) { return left.value < right.value; }
		public static bool operator >(CodePoint left, CodePoint right) { return  left.value > right.value; }
		public static bool operator <=(CodePoint left, CodePoint right) { return  left.value <= right.value; }
		public static bool operator >=(CodePoint left, CodePoint right) { return  left.value >= right.value; }
		public static explicit operator CodePoint (int codePoint) { return new CodePoint(codePoint); }
		public static implicit operator CodePoint (char character) { return new CodePoint(character); }
		public static explicit operator char (CodePoint codePoint) { return (char)codePoint.value; }
	}


	public class SbitLineMetrics {
		public System.Char ascender;
		public System.Char descender;
		public System.Byte widthMax;
		public System.Char caretSlopeNumerator;
		public System.Char caretSlopeDenominator;
		public System.Char caretOffset;
		public System.Char minOriginSB;
		public System.Char minAdvanceSB;
		public System.Char maxBeforeBL;
		public System.Char minAfterBL;
		public System.Char pad1;
		public System.Char pad2;
	}

	public class EBLC_IndexSubTableArrayElement {
		public System.UInt16 firstGlyphIndex;
		public System.UInt16 lastGlyphIndex;
		public System.UInt32 additionalOffset;
		public EBLC_IndexSubHeader indexSubHeader;
		public EBLC_IndexSubElem indexSubElem;
	}


	public interface EBLC_IndexSubElem {
		
	}


	public class EBLC_IndexSubElem_Format1 : EBLC_IndexSubElem {
		public System.UInt32[] offsetArray;
	}


	public class BitmapSizeTable {
		public System.UInt32 indexSubTableArrayOffset;
		public System.UInt32 indexTablesSize;
		public System.UInt32 numberofIndexSubTables;
		public SbitLineMetrics Hori;
		public SbitLineMetrics Vert;
		public System.UInt16 startGlyphIndex;
		public System.UInt16 endGlyphIndex;
		public System.Byte ppemX;
		public System.Byte ppemY;
		public System.Byte bitDepth;
		public char Flags;
		public System.Collections.Generic.List<EBLC_IndexSubTableArrayElement> indexSubtableArray = new System.Collections.Generic.List<EBLC_IndexSubTableArrayElement>();
		//public System.Collections.Generic.List<EBLC_IndexSubHeader> indexSubHeaders;
	}

	public class EBLC_IndexSubHeader {
		public System.UInt16 indexFormat;
		public System.UInt16 imageFormat;
		public System.UInt32 imageDataOffset; // offset to image data in 'EBDT' table
	}

	public class CBLCTable {
		public System.Collections.Generic.List<BitmapSizeTable> bmstList = new System.Collections.Generic.List<BitmapSizeTable>();
	}

	public class SmallGlyphMetrics {
		public System.Byte height;
		public System.Byte width;
		public System.Char BearingX;
		public System.Char BearingY;
		public System.Byte Advance;
	}

	public class BigGlyphMetrics {
		public System.Byte height;
		public System.Byte width;
		public System.Char horiBearingX;
		public System.Char horiBearingY;
		public System.Byte horiAdvance;
		public System.Char vertBearingX;
		public System.Char vertBearingY;
		public System.Byte vertAdvance;
	}


}