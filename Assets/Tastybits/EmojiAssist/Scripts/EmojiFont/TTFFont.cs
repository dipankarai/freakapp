﻿using UnityEngine;
using System.Collections;
using System.Linq;
using TTFReader = System.IO.BinaryReader;


namespace Tastybits.EmojiAssist.TTF {


	public class TTFFont {
		public bool verbose = false;

		public int unitsPerEm;
		public bool integerPpems;
		public MetricsEntry[] hmetrics;
		public MetricsEntry[] vmetrics;
		public int xHeight;
		public int capHeight;
		public FontStretch Stretch;
		public FontWeight Weight;
		public FontStyle Style;
		public bool IsFixedWidth;
		public CharacterMap charMap;
		public KerningTable kernTable;

		public string Family;
		public string Subfamily;
		public string FullName;
		public string UniqueID;
		public string Version;
		public string Description;

		public BaseGlyph[] glyphs;

		public SbixTable sbix;

		public CBLCTable cblc;

		public TableRecord[] tables;

		public TTFReader reader;
		public string fn = "";


		public void Analyze() {
			string strTables = "";
			for( int i = 0; i<tables.Length ; i++ ) {
				strTables += (strTables!=""?",":"") + tables[i].Tag.ToString();
			}
			if (verbose) {
				Debug.Log("Tables : " + strTables );
				Debug.Log( "Glyphs #" + this.glyphs.Length );
				Debug.Log( "Has sbix : " + (sbix!=null ? "yes" : "no" ) );
				Debug.Log( "Filename : " + this.fn );
			}
		}


		public static TTFFont Read( string spath ) {
			//Debug.Log( "Loading font");
			var ret = new TTFFont();
			var stream = System.IO.File.OpenRead( spath );
			var reader = new TTFReader(stream);
			//Debug.Log( "adasdsad");
			ret.reader = reader;
			//Debug.Log( "aaaa");
			var tables = SfntTables.ReadFaceHeader(reader);
			//Debug.Log( "bbbbb");
			ret.tables = tables;
			ret.fn = spath;

			for( int i = 0; i<tables.Length ; i++ ) {
				//	Debug.LogError("Table #" + i+  " " + tables[i].Tag.ToString() );
			}
			//Debug.Log( "tables length : "+  tables.Length );

			FaceHeader head;
			SfntTables.ReadHead(reader, tables, out head);
			SfntTables.ReadMaxp(reader, tables, ref head);
			ret.unitsPerEm = head.UnitsPerEm;
			ret.integerPpems = ((int)(head.Flags & HeadFlags.IntegerPpem)) != 0;


			// horizontal metrics header and data
			SfntTables.SeekToTable(reader, tables, FourCC.Hhea, required: true);
			var hMetricsHeader = SfntTables.ReadMetricsHeader(reader);
			SfntTables.SeekToTable(reader, tables, FourCC.Hmtx, required: true);
			ret.hmetrics = SfntTables.ReadMetricsTable(reader, head.GlyphCount, hMetricsHeader.MetricCount);

			// font might optionally have vertical metrics
			if( SfntTables.SeekToTable(reader, tables, FourCC.Vhea) ) {
				var vMetricsHeader = SfntTables.ReadMetricsHeader(reader);
				SfntTables.SeekToTable( reader, tables, FourCC.Vmtx, required: true );
				ret.vmetrics = SfntTables.ReadMetricsTable( reader, head.GlyphCount, vMetricsHeader.MetricCount );
			}

			// GPOS -> Glyph positioning data..
			// GSUB -> Glyph substitution data...
			// LTSH -> Linear threshold data
			// loca -> offsets...
			// hmtx : horisontal metrix data...
			// COLR
			// The 'prep' (control value program) table
			// DSIG-> digital signature
			// GDEF -> Glyph definition data
			// GSUB -> Glyph substitution data


			// CBDT -> Color Bitmap Data Table (CBDT' table for embedded color bitmap data)
			// CBLC -> (table for embedded bitmap locators and)
			bool hasCBLC=false;
			System.Collections.Generic.List<BitmapSizeTable> bmstList = new System.Collections.Generic.List<BitmapSizeTable>();
			if( SfntTables.SeekToTable(reader, tables, FourCC.CBLC, required: false) ) {
				ret.cblc = new CBLCTable();
				ret.cblc.bmstList = bmstList;
				var CBLC_pos = reader.GetPosition();
				var version = reader.ReadFixedBE();
				hasCBLC=true;
				if( version != 0x00030000 && version != 0x20000 ) { // Initially defined as 0x00030000
					Debug.LogError("invalid version : " + version.ToString("X"));
				}  
				var numsizes = reader.ReadULongBE(); // Number of bitmapSizeTables
				//Debug.LogError("Number of bitmapsizetables..! : " + numsizes );

				// read each of the
				for( int i = 0; i<numsizes; i++ ) {
					//Debug.Log("Reading bitmapSizeTable #" + i );
					var bmst = new BitmapSizeTable();
					bmstList.Add( bmst );
					bmst.indexSubTableArrayOffset = reader.ReadUInt32BE();
					bmst.indexTablesSize = reader.ReadUInt32BE();
					bmst.numberofIndexSubTables = reader.ReadUInt32BE();

					var hori = new SbitLineMetrics() {
						ascender = reader.ReadChar(), descender=reader.ReadChar(), widthMax=reader.ReadByte(), caretSlopeNumerator=reader.ReadChar(),
						caretSlopeDenominator = reader.ReadChar(), caretOffset = reader.ReadChar(), minOriginSB = reader.ReadChar(), minAdvanceSB = reader.ReadChar(),
						maxBeforeBL = reader.ReadChar(), minAfterBL = reader.ReadChar(), pad1 = reader.ReadChar(), pad2 = reader.ReadChar()
					};
					bmst.Hori = hori;
						
					var vert = new SbitLineMetrics() {
						ascender = reader.ReadChar(), descender=reader.ReadChar(), widthMax=reader.ReadByte(), caretSlopeNumerator=reader.ReadChar(),
						caretSlopeDenominator = reader.ReadChar(), caretOffset = reader.ReadChar(), minOriginSB = reader.ReadChar(), minAdvanceSB = reader.ReadChar(),
						maxBeforeBL = reader.ReadChar(), minAfterBL = reader.ReadChar(), pad1 = reader.ReadChar(), pad2 = reader.ReadChar()
					};
					bmst.Vert = vert;

					bmst.startGlyphIndex = reader.ReadUInt16BE();
					bmst.endGlyphIndex = reader.ReadUInt16BE();
					//Debug.Log( "bmst.startGlyphIndex="+bmst.startGlyphIndex+"  bmst.endGlyphIndex=" + bmst.endGlyphIndex );

					bmst.ppemX = reader.ReadByte();
					bmst.ppemY = reader.ReadByte();
					bmst.bitDepth = reader.ReadByte();
					//Debug.Log("bmst.bitDepth=" + bmst.bitDepth );
					bmst.Flags = reader.ReadChar();
				}

				foreach( var bmst in bmstList ) {
					//Debug.Log(" reading EBLC_IndexSubTableArrayElement");
					var off = CBLC_pos+bmst.indexSubTableArrayOffset;
					if( off < 0 ) Debug.LogError("Error offset is less than zero");
					reader.Seek( (uint)off );
					//Debug.Log( "seek to : " + off );

					var istae = new EBLC_IndexSubTableArrayElement();
					istae.firstGlyphIndex = reader.ReadUInt16BE(); 
					//Debug.Log( "asdasda" );

					istae.lastGlyphIndex = reader.ReadUInt16BE();
					istae.additionalOffset = reader.ReadUInt32BE();
					//Debug.Log( "gggg" );
					bmst.indexSubtableArray.Add( istae );
					//Debug.Log( "cccc" );
					 
					off = CBLC_pos+bmst.indexSubTableArrayOffset+istae.additionalOffset;

					//Debug.Log( "seeking to : " + off );
					reader.Seek( (uint)off );
					var ish = new EBLC_IndexSubHeader();
					ish.indexFormat = reader.ReadUInt16BE();
					ish.imageFormat = reader.ReadUInt16BE();

					//Debug.Log( "ish.imageFormat = " + ish.imageFormat );
					ish.imageDataOffset = reader.ReadUInt32BE();
					istae.indexSubHeader = ish;
					if( ish.indexFormat == 1 ) {
						//Debug.Log("format 1 of indexsubtable");
					} else if( ish.indexFormat == 2 ) {
						Debug.Log("format 2 of indexsubtable");
					} else if( ish.indexFormat == 3 ) {
						Debug.Log("format 3 of indexsubtable");
					} else if( ish.indexFormat == 4 ) {
						Debug.Log("format 4 of indexsubtable");
					} else {
						Debug.LogError("Unknown format of indexsubtable! : " + ish.indexFormat );
					}
					 
					if( ish.indexFormat == 1 ) {
						var elem = new EBLC_IndexSubElem_Format1();
						var sizeOfArray= (istae.lastGlyphIndex-istae.firstGlyphIndex+1)+1+1;
						elem.offsetArray = new uint[sizeOfArray];
						int idx=0;
						for( int glyphIndex = istae.firstGlyphIndex; glyphIndex<=istae.lastGlyphIndex; glyphIndex++ ) {
							elem.offsetArray[idx] = reader.ReadUInt32BE();
							//Debug.Log( "Discovered offset for glyph #" + glyphIndex + " off=" + elem.offsetArray[idx] );
							idx++;
						}
						istae.indexSubElem = elem;
					} else {
						Debug.LogError("unimplemented support for : " + ish.indexFormat );
					}

				} 


			}


			if( SfntTables.SeekToTable(reader, tables, FourCC.CBDT, required: hasCBLC) ) {
				var version = reader.ReadFixedBE();
				if( version != 0x00030000 && version != 0x20000 ) {
					Debug.LogError("invalid version : " + version.ToString("X"));
				} 
			}
			 
			// OS/2 table has even more metrics
			var os2Data = SfntTables.ReadOS2(reader, tables);
			ret.xHeight = os2Data.XHeight;
			ret.capHeight = os2Data.CapHeight;
			ret.Weight = os2Data.Weight;
			ret.Stretch = os2Data.Stretch;
			ret.Style = os2Data.Style;

			// optional PostScript table has random junk in it
			SfntTables.ReadPost(reader, tables, ref head);
			ret.IsFixedWidth = head.IsFixedPitch;

			// read character-to-glyph mapping tables and kerning table
			ret.charMap = CharacterMap.ReadCmap(reader, tables);
			ret.kernTable = KerningTable.ReadKerningTable(reader, tables);

			// name data
			var names = SfntTables.ReadNames(reader, tables);
			ret.Family = names.TypographicFamilyName ?? names.FamilyName;
			ret.Subfamily = names.TypographicSubfamilyName ?? names.SubfamilyName;
			ret.FullName = names.FullName;
			ret.UniqueID = names.UniqueID;
			ret.Version = names.Version;
			ret.Description = names.Description;

			// load glyphs if we have them
			if( SfntTables.SeekToTable(reader, tables, FourCC.Glyf) ) {
				// read in the loca table, which tells us the byte offset of each glyph
				var loca = new uint[head.GlyphCount];
				int locaMPtr = 0;
				int tableIndex = 0;
				SfntTables.ReadLoca( reader, tables, head.IndexFormat, ref loca, head.GlyphCount, tableIndex );

				// we need to know the length of the glyf table because of some weirdness in the loca table:
				// if a glyph is "missing" (like a space character), then its loca[n] entry is equal to loca[n+1]
				// if the last glyph in the set is missing, then loca[n] == glyf table length
				SfntTables.SeekToTable(reader, tables, FourCC.Glyf);
				var glyfOffset = reader.GetPosition();
				var glyfLength = tables[SfntTables.FindTable(tables, FourCC.Glyf)].Length;

				// read in all glyphs
				ret.glyphs = new BaseGlyph[head.GlyphCount];
				for( int i = 0; i < ret.glyphs.Length; i++ ) {
					SfntTables.ReadGlyph( reader, i, 0, ref ret.glyphs, (uint)glyfOffset, glyfLength, ref loca, ref locaMPtr );
					//Debug.Log("read glyph #"+i);
				}
			} else {
				//Debug.Log("could not seek table");
			}

			// embedded bitmaps
			SbitTable.Read(reader, tables);

			ret.sbix = SbixTable.Read( reader, tables, ret.glyphs );
			if( ret.sbix == null ) {
				//Debug.Log("sbixtable is null" );
			} else {
				//Debug.Log("ok");
			}
			return ret;
		}
	}

}
