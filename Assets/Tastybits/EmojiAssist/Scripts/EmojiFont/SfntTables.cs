﻿using UnityEngine;
using System.Collections;


namespace Tastybits.EmojiAssist.TTF {
	using TTFReader = System.IO.BinaryReader;



	static class SfntTables {
		const uint TTFv1 = 0x10000;
		const uint TTFv2 = 0x20000;
		const int MaxGlyphs = short.MaxValue;
		const int MaxContours = 256;
		const int MaxRecursion = 128;
		const int MaxFontsInCollection = 64;
		const int MaxStackSize = 16384;
		const int MaxTwilightPoints = short.MaxValue;
		const int MaxFunctionDefs = 4096;
		const int MaxStorageLocations = 16384;
		const float F2Dot14ToFloat = 16384.0f;


		public static uint[] ReadTTCHeader (TTFReader reader) {
			// read the file header; if we have a collection, we want to
			// figure out where all the different faces are in the file
			// if we don't have a collection, there's just one font in the file
			var tag = reader.ReadUInt32();
			if (tag != FourCC.Ttcf)
				return new[] { 0u };

			// font file is a TrueType collection; read the TTC header
			//reader.Skip(4);
			var ver = reader.ReadUInt32BE();
			Debug.Log( "version : " + ver.ToString("X") );


			var count = reader.ReadUInt32BE();
			if (count == 0 || count > MaxFontsInCollection)
				throw new System.Exception("Invalid TTC header");

			var offsets = new uint[count];
			for (int i = 0; i < count; i++)
				offsets[i] = reader.ReadUInt32BE();

			return offsets;
		}


		public static TableRecord[] ReadFaceHeader (TTFReader reader) {
			if( reader.GetPosition() != 0 ) {
				Debug.LogError("Not supposed to be at non 0 position");
				reader.Seek( 0 );
			}
			var sfnt = reader.ReadUInt32BE();
			if( sfnt != TTFv1 && 
				sfnt != TTFv2 && 
				sfnt != FourCC.True && 
				sfnt != FourCC.Typ1 ) 
			{
				Debug.LogError("Unknown or unsupported sfnt version : " + sfnt.ToString("X") + " asInt="+sfnt+ " asStr=" +(new FourCC(sfnt)) );
				throw new System.Exception("Unknown or unsupported sfnt version : " + sfnt );
			}

			var tableCount = reader.ReadUInt16BE();
			reader.Skip(6); // skip the rest of the header

			//Debug.Log("here");
			// read each font table descriptor
			var tables = new TableRecord[tableCount];
			for (int i = 0; i < tableCount; i++) {
				//Debug.Log("here");
				//Debug.Log(" table : " + i );
				tables[i] = new TableRecord {
					Tag = reader.ReadUInt32(),
					CheckSum = reader.ReadUInt32BE(),
					Offset = reader.ReadUInt32BE(),
					Length = reader.ReadUInt32BE(),
				};
			}

			return tables;
		}


		public static MetricsHeader ReadMetricsHeader (TTFReader reader) {
			// skip over version
			reader.Skip(sizeof(int));

			var header = new MetricsHeader {
				Ascender = reader.ReadInt16BE(),
				Descender = reader.ReadInt16BE(),
				LineGap = reader.ReadInt16BE()
			};

			// skip over advanceWidthMax, minLsb, minRsb, xMaxExtent, caretSlopeRise,
			// caretSlopeRun, caretOffset, 4 reserved entries, and metricDataFormat
			reader.Skip(sizeof(short) * 12);

			header.MetricCount = reader.ReadUInt16BE();
			return header;
		}



		public static MetricsEntry[] ReadMetricsTable (TTFReader reader, int glyphCount, int metricCount) {
			var results = new MetricsEntry[glyphCount];
			for (int i = 0; i < metricCount; i++) {
				results[i] = new MetricsEntry {
					Advance = reader.ReadUInt16BE(),
					FrontSideBearing = reader.ReadInt16BE()
				};
			}

			// there might be an additional array of fsb-only entries
			var extraCount = glyphCount - metricCount;
			var lastAdvance = results[metricCount - 1].Advance;
			for (int i = 0; i < extraCount; i++) {
				results[i + metricCount] = new MetricsEntry {
					Advance = lastAdvance,
					FrontSideBearing = reader.ReadInt16BE()
				};
			}

			return results;
		}


		public static int FindTable (TableRecord[] tables, FourCC tag) {
			var index = -1;
			for (int i = 0; i < tables.Length; i++) {
				if (tables[i].Tag == tag) {
					index = i;
					break;
				}
			}

			return index;
		}



		public static bool SeekToTable (TTFReader reader, TableRecord[] tables, FourCC tag, bool required = false) {
			// check if we have the desired table and that it's not empty
			var index = FindTable(tables, tag);
			if (index == -1 || tables[index].Length == 0) {
				//Debug.LogError("cannto seek to table : " + tag.ToString() + " it doesnt exist" );
				if (required) {
					Debug.LogError("Missing table : " + tag.ToString());
					throw new System.Exception("Missing or empty '{"+tag+"}' table.");
				}
				return false;
			}

			// seek to the appropriate offset
			reader.Seek(tables[index].Offset);
			return true;
		}


		public static void ReadHead (TTFReader reader, TableRecord[] tables, out FaceHeader header) {
			SeekToTable(reader, tables, FourCC.Head, required: true);

			// 'head' table contains global information for the font face
			// we only care about a few fields in it
			reader.Skip(sizeof(int) * 4);   // version, revision, checksum, magic number

			header = new FaceHeader {
				Flags = (HeadFlags)reader.ReadUInt16BE(),
				UnitsPerEm = reader.ReadUInt16BE()
			};
			if (header.UnitsPerEm == 0)
				throw new System.Exception("Invalid 'head' table.");

			// skip over created and modified times, bounding box,
			// deprecated style bits, direction hints, and size hints
			reader.Skip(sizeof(long) * 2 + sizeof(short) * 7);

			header.IndexFormat = (IndexFormat)reader.ReadInt16BE();
		}


		public static void ReadMaxp (TTFReader reader, TableRecord[] tables, ref FaceHeader header) {
			SeekToTable(reader, tables, FourCC.Maxp, required: true);

			if (reader.ReadInt32BE() != 0x00010000)
				throw new System.Exception("Font contains an old style maxp table.");

			header.GlyphCount = reader.ReadUInt16BE();
			if (header.GlyphCount > MaxGlyphs)
				throw new System.Exception("Font contains too many glyphs.");

			// skip maxPoints, maxContours, maxCompositePoints, maxCompositeContours, maxZones
			reader.Skip(sizeof(short) * 5);

			header.MaxTwilightPoints = reader.ReadUInt16BE();
			header.MaxStorageLocations = reader.ReadUInt16BE();
			header.MaxFunctionDefs = reader.ReadUInt16BE();
			header.MaxInstructionDefs = reader.ReadUInt16BE();
			header.MaxStackSize = reader.ReadUInt16BE();

			// sanity checking
			if (header.MaxTwilightPoints > MaxTwilightPoints || header.MaxStorageLocations > MaxStorageLocations ||
				header.MaxFunctionDefs > MaxFunctionDefs || header.MaxInstructionDefs > MaxFunctionDefs ||
				header.MaxStackSize > MaxStackSize)
				throw new System.Exception("Font programs have limits that are larger than built-in sanity checks.");
		}


		public static OS2Data ReadOS2 (TTFReader reader, TableRecord[] tables) {
			SeekToTable(reader, tables, FourCC.OS_2, required: true);

			// skip over version, xAvgCharWidth
			reader.Skip(sizeof(short) * 2);

			var result = new OS2Data {
				Weight = (FontWeight)reader.ReadUInt16BE(),
				Stretch = (FontStretch)reader.ReadUInt16BE()
			};

			// skip over fsType, ySubscriptXSize, ySubscriptYSize, ySubscriptXOffset, ySubscriptYOffset,
			// ySuperscriptXSize, ySuperscriptYSize, ySuperscriptXOffset, ySuperscriptXOffset
			reader.Skip(sizeof(short) * 9);

			result.StrikeoutSize = reader.ReadInt16BE();
			result.StrikeoutPosition = reader.ReadInt16BE();

			// skip over sFamilyClass, panose[10], ulUnicodeRange1-4, achVendID[4]
			reader.Skip(sizeof(short) + sizeof(int) * 4 + 14);

			// check various style flags
			var fsSelection = (FsSelectionFlags)reader.ReadUInt16BE();
			result.Style = (fsSelection & FsSelectionFlags.Italic) != 0 ? FontStyle.Italic :
				(fsSelection & FsSelectionFlags.Bold) != 0 ? FontStyle.Bold :
				(fsSelection & FsSelectionFlags.Oblique) != 0 ? FontStyle.Oblique :
				FontStyle.Regular;
			result.IsWWSFont = (fsSelection & FsSelectionFlags.WWS) != 0;
			result.UseTypographicMetrics = (fsSelection & FsSelectionFlags.UseTypoMetrics) != 0;

			// skip over usFirstCharIndex, usLastCharIndex
			reader.Skip(sizeof(short) * 2);

			result.TypographicAscender = reader.ReadInt16BE();
			result.TypographicDescender = reader.ReadInt16BE();
			result.TypographicLineGap = reader.ReadInt16BE();
			result.WinAscent = reader.ReadUInt16BE();
			result.WinDescent = reader.ReadUInt16BE();

			// skip over ulCodePageRange1-2
			reader.Skip(sizeof(int) * 2);

			result.XHeight = reader.ReadInt16BE();
			result.CapHeight = reader.ReadInt16BE();

			return result;
		}

		public static NameData ReadNames (TTFReader reader, TableRecord[] tables) {
			if (!SeekToTable(reader, tables, FourCC.Name))
				return default(NameData);

			// read header
			var currentOffset = reader.GetPosition();
			var format = reader.ReadUInt16BE();
			var count = reader.ReadUInt16BE();
			var dataOffset = currentOffset + reader.ReadUInt16BE();

			// read name records, filtering out non-Unicode and platforms we don't know about
			var stringData = new StringData[count];
			var stringDataCount = 0;
			for (int i = 0; i < count; i++) {
				var platform = reader.ReadUInt16BE();
				var encoding = reader.ReadUInt16BE();
				var language = reader.ReadUInt16BE();
				var name = reader.ReadUInt16BE();
				var length = reader.ReadUInt16BE();
				var offset = reader.ReadUInt16BE();

				// we only support Unicode strings
				if (platform == PlatformID.Microsoft) {
					if (encoding != WindowsEncoding.UnicodeBmp && encoding != WindowsEncoding.UnicodeFull)
						continue;

					if (language != System.Globalization.CultureInfo.CurrentCulture.LCID)
						continue;
				}
				else if (platform != PlatformID.Unicode)
					continue;

				stringData[stringDataCount++] = new StringData {
					Name = name,
					Offset = offset,
					Length = length
				};
			}

			// find strings we care about and extract them from the blob
			var nameData = new NameData();
			for (int i = 0; i < stringDataCount; i++) {
				var data = stringData[i];
				switch (data.Name) {
					case NameID.FamilyName: nameData.FamilyName = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.SubfamilyName: nameData.SubfamilyName = ExtractString(reader,(uint) dataOffset, data); break;
					case NameID.UniqueID: nameData.UniqueID = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.FullName: nameData.FullName = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.Version: nameData.Version = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.Description: nameData.Description = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.TypographicFamilyName: nameData.TypographicFamilyName = ExtractString(reader, (uint)dataOffset, data); break;
					case NameID.TypographicSubfamilyName: nameData.TypographicSubfamilyName = ExtractString(reader, (uint)dataOffset, data); break;
				}
			}

			return nameData;
		}


		static string ExtractString (TTFReader reader, uint baseOffset, StringData data) {
			reader.Seek(baseOffset + data.Offset);
			var bytes = reader.ReadBytes(data.Length);
			return System.Text.Encoding.BigEndianUnicode.GetString(bytes);
		}


		public static void ReadPost (TTFReader reader, TableRecord[] tables, ref FaceHeader header) {
			if (!SeekToTable(reader, tables, FourCC.Post))
				return;

			// skip over version and italicAngle
			reader.Skip(sizeof(int) * 2);

			header.UnderlinePosition = reader.ReadInt16BE();
			header.UnderlineThickness = reader.ReadInt16BE();
			header.IsFixedPitch = reader.ReadUInt32BE() != 0;
		}

		public static void ReadLoca (TTFReader reader, TableRecord[] tables, IndexFormat format, ref uint[] table, int count, int tableIndex ) {
			SeekToTable(reader, tables, FourCC.Loca, required: true);
			if (format == IndexFormat.Short) {
				// values are ushort, divided by 2, so we need to shift back
				for (int i = 0; i < count; i++) {
					table[tableIndex++] = (uint)(reader.ReadUInt16BE() << 1);
				}
			} else {
				for (int i = 0; i < count; i++) {
					table[tableIndex++] = reader.ReadUInt32BE();
				}
			}
		}


		public static void ReadGlyph( TTFReader reader, int glyphIndex, int recursionDepth, ref BaseGlyph[] glyphTable, uint glyfOffset, uint glyfLength, ref uint[] loca, ref int locaPtr ) 
		{
			// check if this glyph has already been loaded; this can happen
			// if we're recursively loading subglyphs as part of a composite
			if (glyphTable[glyphIndex] != null)
				return;

			// prevent bad font data from causing infinite recursion
			if (recursionDepth > MaxRecursion)
				throw new System.Exception("Bad font data; infinite composite recursion.");

			// check if this glyph doesn't have any actual data
			GlyphHeader header;
			var offset = loca[glyphIndex];
			if ((glyphIndex < glyphTable.Length - 1 && offset == loca[glyphIndex + 1]) || offset >= glyfLength) {
				// this is an empty glyph, so synthesize a header
				header = default(GlyphHeader);
			}
			else {
				// seek to the right spot and load the header
				reader.Seek(glyfOffset + loca[glyphIndex]);
				header = new GlyphHeader {
					ContourCount = reader.ReadInt16BE(),
					MinX = reader.ReadInt16BE(),
					MinY = reader.ReadInt16BE(),
					MaxX = reader.ReadInt16BE(),
					MaxY = reader.ReadInt16BE()
				};

				if (header.ContourCount < -1 || header.ContourCount > MaxContours)
					throw new System.Exception("Invalid number of contours for glyph.");
			}

			if (header.ContourCount > 0) {
				// positive contours means a simple glyph
				glyphTable[glyphIndex] = ReadSimpleGlyph(reader, header.ContourCount);
			}
			else if (header.ContourCount == -1) {
				// -1 means composite glyph
				var composite = ReadCompositeGlyph(reader);
				var subglyphs = composite.Subglyphs;

				// read each subglyph recrusively
				int locaPtr2 = 0;
				for (int i = 0; i < subglyphs.Length; i++)
					ReadGlyph( reader, subglyphs[i].Index, recursionDepth + 1, ref glyphTable, (uint)glyfOffset, glyfLength, ref loca, ref locaPtr2);

				glyphTable[glyphIndex] = composite;
			}
			else {
				// no data, so synthesize an empty glyph
				glyphTable[glyphIndex] = new SimpleGlyph {
					Points = new Point[0],
					ContourEndpoints = new int[0]
				};
			}

			// save bounding box
			var glyph = glyphTable[glyphIndex];
			glyph.MinX = header.MinX;
			glyph.MinY = header.MinY;
			glyph.MaxX = header.MaxX;
			glyph.MaxY = header.MaxY;
		}


		static SimpleGlyph ReadSimpleGlyph (TTFReader reader, int contourCount) {
			// read contour endpoints
			var contours = new int[contourCount];
			var lastEndpoint = reader.ReadUInt16BE();
			contours[0] = lastEndpoint;
			for (int i = 1; i < contours.Length; i++) {
				var endpoint = reader.ReadUInt16BE();
				contours[i] = endpoint;
				if (contours[i] <= lastEndpoint)
					throw new System.Exception("Glyph contour endpoints are unordered.");
				lastEndpoint = endpoint;
			}

			// the last contour's endpoint is the number of points in the glyph
			var pointCount = lastEndpoint + 1;
			var points = new Point[pointCount];

			// read instruction data
			var instructionLength = reader.ReadUInt16BE();
			var instructions = reader.ReadBytes(instructionLength);

			// read flags
			var flags = new SimpleGlyphFlags[pointCount];
			int flagIndex = 0;
			while (flagIndex < flags.Length) {
				var f = (SimpleGlyphFlags)reader.ReadByte();
				flags[flagIndex++] = f;

				// if Repeat is set, this flag data is repeated n more times
				if ((f & SimpleGlyphFlags.Repeat) != 0) {
					var count = reader.ReadByte();
					for (int i = 0; i < count; i++)
						flags[flagIndex++] = f;
				}
			}

			// Read points, first doing all X coordinates and then all Y coordinates.
			// The point packing is insane; coords are either 1 byte or 2; they're
			// deltas from previous point, and flags let you repeat identical points.
			var x = 0;
			for (int i = 0; i < points.Length; i++) {
				var f = flags[i];
				var delta = 0;

				if ((f & SimpleGlyphFlags.ShortX) != 0) {
					delta = reader.ReadByte();
					if ((f & SimpleGlyphFlags.SameX) == 0)
						delta = -delta;
				}
				else if ((f & SimpleGlyphFlags.SameX) == 0)
					delta = reader.ReadInt16BE();

				x += delta;
				points[i].X = (FUnit)x;
			}

			var y = 0;
			for (int i = 0; i < points.Length; i++) {
				var f = flags[i];
				var delta = 0;

				if ((f & SimpleGlyphFlags.ShortY) != 0) {
					delta = reader.ReadByte();
					if ((f & SimpleGlyphFlags.SameY) == 0)
						delta = -delta;
				}
				else if ((f & SimpleGlyphFlags.SameY) == 0)
					delta = reader.ReadInt16BE();

				y += delta;
				points[i].Y = (FUnit)y;
				points[i].Type = (f & SimpleGlyphFlags.OnCurve) != 0 ? PointType.OnCurve : PointType.Quadratic;
			}

			return new SimpleGlyph {
				Points = points,
				ContourEndpoints = contours,
				Instructions = instructions
			};
		}


		static CompositeGlyph ReadCompositeGlyph( TTFReader reader ) {
			// we need to keep reading glyphs for as long as
			// our flags tell us that there are more to read
			var subglyphs = new System.Collections.Generic.List<Subglyph>();

			CompositeGlyphFlags flags;
			do {
				flags = (CompositeGlyphFlags)reader.ReadUInt16BE();

				var subglyph = new Subglyph { Flags = flags };
				subglyph.Index = reader.ReadUInt16BE();

				// read in args; they vary in size based on flags
				if ((flags & CompositeGlyphFlags.ArgsAreWords) != 0) {
					subglyph.Arg1 = reader.ReadInt16BE();
					subglyph.Arg2 = reader.ReadInt16BE();
				}
				else {
					subglyph.Arg1 = reader.ReadSByte();
					subglyph.Arg2 = reader.ReadSByte();
				}

				// figure out the transform; we can either have no scale, a uniform
				// scale, two independent scales, or a full 2x2 transform matrix
				// transform components are in 2.14 fixed point format
				//var transform = Matrix3x2.Identity;
				var transform = Matrix4x4.identity;
				if ((flags & CompositeGlyphFlags.HaveScale) != 0) {
					var scale = reader.ReadInt16BE() / F2Dot14ToFloat;
					transform.m11 = scale;
					transform.m22 = scale;
				}
				else if ((flags & CompositeGlyphFlags.HaveXYScale) != 0) {
					transform.m11 = reader.ReadInt16BE() / F2Dot14ToFloat;
					transform.m22 = reader.ReadInt16BE() / F2Dot14ToFloat;
				}
				else if ((flags & CompositeGlyphFlags.HaveTransform) != 0) {
					transform.m11 = reader.ReadInt16BE() / F2Dot14ToFloat;
					transform.m12 = reader.ReadInt16BE() / F2Dot14ToFloat;
					transform.m21 = reader.ReadInt16BE() / F2Dot14ToFloat;
					transform.m22 = reader.ReadInt16BE() / F2Dot14ToFloat;
				}

				subglyph.Transform = transform;
				subglyphs.Add(subglyph);

			} while ((flags & CompositeGlyphFlags.MoreComponents) != 0);

			var result = new CompositeGlyph { Subglyphs = subglyphs.ToArray() };

			// if we have instructions, read them now
			if ((flags & CompositeGlyphFlags.HaveInstructions) != 0) {
				var instructionLength = reader.ReadUInt16BE();
				result.Instructions = reader.ReadBytes(instructionLength);
			}

			return result;
		}




	}


}