﻿#if NO != YES
using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Tastybits.EmojiAssist.TTF {
		

	public class ParseFont : UnityEditor.EditorWindow {

		[MenuItem("Toolz/Parse Font Test")]
		static void _() {
			Debug.Log("Hey Joe");
			var wnd = EditorWindow.GetWindow<ParseFont>();
			wnd.Show();
		}

		void OnGUI() {
			if(GUILayout.Button("Test Save To Image")){
				var ret = TTFFont.Read( "/System/Library/Fonts/Apple Color Emoji.ttf" );
				int glyphid = ret.charMap.Lookup((new CodePoint(0x1F602)));
				var glyph = ret.glyphs[glyphid];
				Debug.Log( "Type of glyph  :"  + (glyph.GetType().Name) + " ins=" +glyph.Instructions.Length  );
				var bytes = ret.sbix.GetBytes( ret.reader, glyphid );
				Debug.LogError("bytes gotten");
				//ret.glyphs[0].	


				var tex = new Texture2D(2,2);
				tex.LoadImage( bytes );
				tex.Apply();
				var bytesEnc = tex.EncodeToPNG();
				System.IO.File.WriteAllBytes( Application.dataPath + "/Tst.png", bytesEnc );

			}
		}


	}

}
#endif 