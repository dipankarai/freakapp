﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;


namespace Tastybits.EmojiAssist {

	public class iOSEmoji {


		public static Texture2D GetGlyphForChar( string ch, int height, int modifier, int variationSelector ){
	#if UNITY_IPHONE
			if( modifier != 0 )
				ch += System.Char.ConvertFromUtf32( modifier );
			if( variationSelector != 0 ) 
				ch += System.Char.ConvertFromUtf32( variationSelector );
			
			int nBytes = 0; 
			IntPtr ptrBytes = AppleEmojiFont_GetGlyphForChar( ch, height, ref nBytes );

			if( nBytes==0 ) {
				Debug.LogError("Failed to get glyph character");
				return null;
			}
			// copy the raw allocated bytes into a Managed buffer and free the raw byes.
			byte[] bytes = new byte[nBytes]; 
			Marshal.Copy( ptrBytes, bytes, 0, nBytes );
			Marshal.FreeHGlobal( ptrBytes );
			
			// Load png into memory.
			Texture2D tex = new Texture2D(2,2);
			bool ok = tex.LoadImage( bytes );
			if( !ok ) {
				Debug.LogError("Error loading glyph");
			}
			return tex;
	#else
			throw new System.Exception("Not supported on : " + Application.platform );
	#endif
		}
		
	#if UNITY_IPHONE
		[DllImport ("__Internal")]
		private static extern IntPtr AppleEmojiFont_GetGlyphForChar( string ch, int height, ref int nBytes  );
	#endif

	}


}


