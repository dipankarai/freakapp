﻿/**
 * @desc 	Class to handle postprocess build additions primarily on iOS
 * 
 * @author 	Tastybits
 * @link 	www.tastybits.io
 */
#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
#if UNITY_IPHONE

using UnityEditor.iOS.Xcode;


public static class FrankPostBuildTrigger {

	[PostProcessBuild]
	public static void OnPostprocessBuild( BuildTarget buildTarget, string path ) {
		if( buildTarget != BuildTarget.iOS ) {
			return;
		}

		//Debug.Log("Removing Keyboard.mm from project");

		//Debug.Log ("path : " + path);

		string[] files = Directory.GetFiles( path, "Keyboard.mm", SearchOption.AllDirectories);
		if (files.Length == 0)
			Debug.LogError ("could not find keyboard.mm");
		else {
			string text = System.IO.File.ReadAllText (files [0]);
			string pattern = "#ifndef FILTER_EMOJIS_IOS_KEYBOARD \n#define FILTER_EMOJIS_IOS_KEYBOARD 1\n#endif";
			int i = text.IndexOf (pattern);
			if ( i != -1 ) {
				text = text.Replace (pattern, "");
				text = text.Insert( i, "#ifdef FILTER_EMOJIS_IOS_KEYBOARD\n#undef FILTER_EMOJIS_IOS_KEYBOARD\n#endif\n" );
				System.IO.File.WriteAllText (files [0], text);
			}
			if( text.IndexOf("#define FILTER_EMOJIS_IOS_KEYBOARD 1") != - 1) {
				text = text.Replace("#define FILTER_EMOJIS_IOS_KEYBOARD 1", "#define FILTER_EMOJIS_IOS_KEYBOARD 0");			}
		}
		
			
		//Debug.Log ("Post processing iOS build" );
		//string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
		//var pbx= new UnityEditor.iOS.Xcode.PBXProject();
		//pbx.ReadFromFile( projPath );
		//var guid1 = pbx.FindFileGuidByProjectPath( "Classes/UI/Keyboard.mm" );

		//string target = pbx.TargetGuidByName("Unity-iPhone");
		//pbx.AddFrameworkToProject( target, "CoreImage.framework", true );
		//pbx.AddFrameworkToProject( target, "MessageUI.framework", true );
		//pbx.AddFrameworkToProject( target, "Security.framework", true );

		//pbx.RemoveFile( guid1 );
		//Debug.Log("Removed Keyboard.mm from project");

		//pbx.AddFrameworkToProject( "asdsad", "asdasd", true );
		//pbx.WriteToFile(projPath);
	}


}
#endif

#endif