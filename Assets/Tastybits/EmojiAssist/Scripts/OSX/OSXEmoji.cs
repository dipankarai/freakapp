﻿
using UnityEngine;
using System.Collections;
using Tastybits.EmojiAssist.TTF;
using System.Runtime.InteropServices;
using System;

namespace Tastybits.EmojiAssist {



	public class OSXEmoji {
		static TTFFont font;
		static System.Collections.Generic.Dictionary<int,Texture2D> fontTextures = new System.Collections.Generic.Dictionary<int,Texture2D>();



		public static bool EmojiFontInstalled {
			get {
				string spath = "/System/Library/Fonts/Apple Color Emoji.ttf";
				return System.IO.File.Exists (spath);
			}
		}



		#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		[DllImport ("EmojiRenderer")]
		private static extern IntPtr OSXEmojiFont_GetGlyphForChar( string ch, int height, ref int nBytes  );
		#endif 

		public static Texture2D GetGlyphForChar( string ch, int height=0, bool usePlugin=false, int modifier=0, int variationSelector=0 ) {
			#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
			if (!usePlugin && !EmojiFontInstalled) {
				usePlugin = true;
			}

			if (usePlugin) {
				int nBytes = 0; 

				if( modifier != 0 )
					ch += System.Char.ConvertFromUtf32( modifier );
				if( variationSelector != 0 ) 
					ch += System.Char.ConvertFromUtf32( variationSelector );	

				IntPtr ptrBytes = OSXEmojiFont_GetGlyphForChar( ch, height, ref nBytes );

				if( nBytes==0 ) {
					Debug.LogError("Failed to get glyph character");
					return null;
				}
				// copy the raw allocated bytes into a Managed buffer and free the raw byes.
				byte[] bytes_png = new byte[nBytes]; 
				Marshal.Copy( ptrBytes, bytes_png, 0, nBytes );
				Marshal.FreeHGlobal( ptrBytes );

				// Load png into memory.
				Texture2D tex2 = new Texture2D(2,2);
				bool ok = tex2.LoadImage( bytes_png );
				if( !ok ) {
					Debug.LogError("Error loading glyph");
				}
				return tex2;
			}


			int utf32 = char.ConvertToUtf32(ch, 0);

			if( fontTextures.ContainsKey( utf32 ) ) {
				return fontTextures[utf32];
			}

			//Debug.LogError("Getting code point : " + string.Format("{0:X}", utf32) );

	 		if( font== null ) {
				// creating font instance....
				//Debug.Log("Creating font instance...");
				string spath = "/System/Library/Fonts/Apple Color Emoji.ttf";
				if( !System.IO.File.Exists(spath) ) {
					Debug.LogError ("Error cannot find OSX Emoji font at path : " + spath);
				}
				font = TTFFont.Read( spath ); 
			}

			int glyphid = font.charMap.Lookup((new CodePoint(utf32)));
			var glyph = font.glyphs[glyphid];

			var bytes = font.sbix.GetBytes( font.reader, glyphid, true );
			//Debug.LogError("bytes gotten");
			//ret.glyphs[0].	

			// conert to texture..

			var tex = new Texture2D(2,2);
			tex.LoadImage( bytes );
			tex.Apply();
			fontTextures.Add( utf32, tex );
			//var bytesEnc = tex.EncodeToPNG();
			//System.IO.File.WriteAllBytes( Application.dataPath + "/Tst.png", bytesEnc );

			//Tastybits.EmojiAssist.TTF.TTFFont.Read( "		
			return fontTextures[utf32];
			#else
			Debug.LogError("not available on this platform");
			return null;
			#endif 
		}

	}

}