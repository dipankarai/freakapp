﻿using System;
using Tastybits.EmojiAssist.TTF;
using UnityEngine;

namespace Tastybits.EmojiAssist
{


	public class AndroidEmoji {
		static TTFFont font;
		static System.Collections.Generic.Dictionary<int,Texture2D> fontTextures = new System.Collections.Generic.Dictionary<int,Texture2D>();

		static string fontFilePath = null;
		public static string FontFilePath {
			get {
				if (fontFilePath == "") {
					return null;
				}
				if (!string.IsNullOrEmpty (fontFilePath)) {
					return fontFilePath;
				}
				string[] fileNames = new string[] {
					"NotoColorEmoji.ttf",
					"AndroidEmoji.ttf",
					"AndroidEmoji-htc.tff",
					"AndroidEmoji-samsung.tff",
					"LG_ColorEmoji.tff",
					"LGColorEmoji.ttf",
					"SamColorEmoji.ttf",
					"LGColorEmoji.ttf"
				};
				string androidFontLocation = "/system/fonts/NotoColorEmoji.ttf";
				bool fontFound = false;
				for (int i = 0; i < fileNames.Length; i++) {
					androidFontLocation = "/system/fonts/" + fileNames [i];
					fontFound = System.IO.File.Exists (androidFontLocation);
					if (fontFound) {
						fontFilePath = androidFontLocation;
						return androidFontLocation;
					}
				}

				// Try enumerating the files:
				try {
					var files = System.IO.Directory.GetFiles( "/system/fonts/", "*.ttf");
					foreach (var file in files) {
						if( file.ToLower().Contains("emoji") ) {
							androidFontLocation = file;
							fontFilePath = androidFontLocation;
							return androidFontLocation;
						} 
					}
					Debug.LogError("Error locating android emoji amongst files : \n" + string.Join(",\n", files) );
					
				}catch( System.Exception e ) {
					Debug.LogError ("Error getting emoji file from system : " + e);				
				}
	
				fontFilePath = "";
				return null;
			}
		}

		public static Texture2D GetGlyphForChar( string ch, int height=0 ){
			int utf32 = char.ConvertToUtf32(ch, 0);

			if( fontTextures.ContainsKey( utf32 ) ) {
				return fontTextures[utf32];
			}


			if( font== null ){
				if (string.IsNullOrEmpty(FontFilePath) ) {
					return null;
				}
				font = TTFFont.Read( FontFilePath ); 
			}

			if( font == null ) {
				Debug.LogError("Error loading Emoji font!!");
				var tex = new Texture2D(2,2);
				tex.SetPixels( new Color[]{ Color.white,Color.white,Color.white,Color.white } );
				tex.Apply();
				return tex;
			}

			int glyphid = font.charMap.Lookup((new CodePoint(utf32)));
			var glyph = font.glyphs[glyphid];

			var bytes = font.sbix.GetBytes( font.reader, glyphid, true );
			//Debug.LogError("bytes gotten");
			//ret.glyphs[0].	

			// conert to texture..

			var tex2 = new Texture2D(2,2);
			tex2.LoadImage( bytes );
			tex2.Apply();
			fontTextures.Add( utf32, tex2 );
			//var bytesEnc = tex.EncodeToPNG();
			//System.IO.File.WriteAllBytes( Application.dataPath + "/Tst.png", bytesEnc );

			//Tastybits.EmojiAssist.TTF.TTFFont.Read( "		
			return fontTextures[utf32];
		}

	}


}

