﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Tastybits.EmojiAssist {


	[ExecuteInEditMode]
	public class EmojiFontCache : MonoBehaviour {
		[SerializeField]
		[HideInInspector]
		bool dirty = true;

		[SerializeField]
		[HideInInspector]
		bool verbose = false;

		[SerializeField]
		RenderTexture renderTexture;

		[SerializeField]
		[HideInInspector]
		System.Collections.Generic.List<Texture2D> textureImages;

		[SerializeField]
		[HideInInspector]
		System.Collections.Generic.List<string> textureKeys;

		[SerializeField]
		[HideInInspector]
		System.Collections.Generic.List<Vector4> textureUVs;

		[SerializeField] 
		[HideInInspector]
		Vector2 offset = Vector2.zero;

		[SerializeField] 
		[HideInInspector]
		float vpWidth;

		[SerializeField]
		[HideInInspector]
		float vpHeight;

		[SerializeField] 
		[HideInInspector]
		float drawWidth = 1f;

		[SerializeField]  
		[HideInInspector]
		float drawHeight = 1f;

		Vector2 renderTextureDimensions {
			get {
				if (renderTexture != null)
					return new Vector2 (renderTexture.width, renderTexture.height);
				return new Vector2 (512, 512);
			}
		}

		//[HideInInspector]
		[SerializeField]
		Shader emojiFontShader;

		[SerializeField]
		//[HideInInspector] 
		public Material fontMat = null;

		const float resolution = 8f;


		bool doRebuild=false;

		public static void QueueRebuild() {
			Instance.doRebuild = true;
		}


		public static Material Material {
			get {
				return Instance.fontMat;
			}
		}


		static EmojiFontCache _instance=null;
		public static EmojiFontCache Instance {
			get {
				if (_instance == null) {
					_instance = Component.FindObjectOfType<EmojiFontCache> ();
					if (_instance == null) {
						var cam = Camera.main;
						GameObject go = null;
						//if (cam != null) go = cam.gameObject;
						if( go == null ) go = new GameObject ("EmojiFontCache");
						_instance = go.AddComponent<EmojiFontCache> ();
						GameObject.DontDestroyOnLoad( _instance );
					}
				}
				return _instance;
			}
		}


		void Awake() {
			if (_instance != null && _instance != this ) {
				Debug.LogError ("Cannot have more than one EmojiFontCache instance.");
				GameObject.DestroyImmediate (this.gameObject);
				return;
			}
			_instance = this;
			if( renderTexture == null )	
				renderTexture = RenderTexture.GetTemporary( 
					(int)renderTextureDimensions.x, 
					(int)renderTextureDimensions.y, 
					16, RenderTextureFormat.ARGB32, 
					RenderTextureReadWrite.Default, 
					1 );
			dirty = true;
			if (fontMat == null) {
				fontMat = new Material (emojiFontShader); 
			}
			fontMat.SetTexture( "_EmojiTex", renderTexture );
			if( Application.isPlaying ) 
				GameObject.DontDestroyOnLoad( _instance );

			textureImages.Clear ();
			textureKeys.Clear ();
			textureUVs.Clear ();
		}


		public void SetNeedsEmoji( string codePoint, Texture2D texture ) {
			if( !textureKeys.Contains (codePoint) ) {
				dirty = true;
				textureKeys.Add (codePoint);
				textureImages.Add (texture);
				UpdateUVs ();
			} else {
				var i = textureKeys.IndexOf (codePoint);
				textureImages[i] = texture;
				UpdateUVs ();
			}
		}


		public static Vector4 GetUVs( string codePoint, Texture2D emoji_tex ) {
			return Instance._GetUVs (codePoint, emoji_tex);
		}


		public Vector4 _GetUVs( string codePoint, Texture2D emoji_tex ) {
			this.gameObject.SetActive (true);
			this.enabled = true;
			if( !textureKeys.Contains (codePoint) ) {
				dirty = true;
				textureKeys.Add( codePoint );
				textureImages.Add( emoji_tex );
				textureUVs.Add (new Vector4( 0,0,1,1 ) );
				UpdateUVs ();
				return textureUVs [textureUVs.Count - 1];
			} else {
				var _i = textureKeys.IndexOf( codePoint );
				if (textureImages [_i] != emoji_tex) {
					textureImages[_i] = emoji_tex;
					textureUVs [_i] = new Vector4 (0, 0, 1, 1);
					UpdateUVs ();
				}
				return textureUVs[_i];
			}
			return new Vector4 (0, 0, 1, 1);
		}


		public static void Clear() {
			Instance.textureImages.Clear ();
			Instance.textureKeys.Clear ();
			Instance.textureUVs.Clear ();
		}


		public static void SetDirty() {
			Instance.dirty = true;
		}


		public static void Rebuild() {
			Instance.dirty = true;
			Instance.Render ();
			Instance.enabled = false;
			Instance.enabled = true;
			Instance.dirty = true;
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.CallbackFunction fnc = null;
			int i = 0;
			Instance.enabled = false;
			fnc = () => {
				Instance.enabled = false;
				if( i++ < 1 )
					return;		
				//Debug.Log("enabling again");
				Instance.enabled = true;
				UnityEditor.EditorApplication.update -= fnc;
			};
			UnityEditor.EditorApplication.update += fnc;
			#endif 
		}


		public Texture Texture {
			get {
				return (Texture)renderTexture;
			}
		}


		int cnt = 0;

		// modifier sequences


		void Update()  {
			if( Application.isEditor && Application.isPlaying==false ) 
			if (cnt++ % 30 == 0)
				doRebuild = true;
			if (doRebuild) {
				doRebuild = false;
				Rebuild ();
			}
			if( dirty ) {
				this.enabled = false;
				this.enabled = true;
				CheckRender ();
			}
		}


		void OnPostRender(){
			if( dirty ) {
				CheckRender ();
			}
		}


		void OnEnable() {
			Render ();
			dirty = true;
		}


		void CheckRender() {
			if (dirty) {
				Render ();
			}
		}
			

		void Render() {
			dirty = false;

			var pre_rt = RenderTexture.active; 
			if( renderTexture == null )	
				renderTexture = RenderTexture.GetTemporary( 
					(int)renderTextureDimensions.x, 
					(int)renderTextureDimensions.y, 
					16, RenderTextureFormat.ARGB32, 
					RenderTextureReadWrite.Default, 
					1 );
			
			RenderTexture.active = renderTexture;                   

			GL.Clear( true, true, Color.clear );

			GL.PushMatrix ();

			vpWidth = (float)renderTextureDimensions.x;
			vpHeight = (float)renderTextureDimensions.y;

			GL.LoadPixelMatrix( 0, vpWidth, vpHeight, 0 );

			float rowMax = 0f;
			float offx = 0f;
			float offy = 0f;

			int i = 0;
			foreach( var tx in textureImages ) {
				drawWidth = renderTextureDimensions.x / resolution; //tx.width;
				drawHeight = renderTextureDimensions.y / resolution; //tx.height;


				if( offx + drawWidth > renderTextureDimensions.x ) {
					offx = 0f;
					offy += rowMax;
					rowMax = 0f;
				}
				//drawWidth = tx.width;
				//drawHeight = tx.height;

			
				if( rowMax < drawHeight )
					rowMax = drawHeight;

				//var uv = new Vector4 ( offx/vpWidth, offy/vpHeight, (offx+drawWidth)/vpWidth, (offy+drawHeight)/vpHeight );
				//textureUVs[i] = uv;

				if (tx != null) {
					Graphics.DrawTexture( new Rect( offx, offy, drawWidth, drawHeight ), tx );		
				}

				offx += drawWidth;
				i++;
			}

			GL.PopMatrix ();    
			GL.End ();


			RenderTexture.active = pre_rt;
		}


		void UpdateUVs( ) {
			float rowMax = 0f;
			float offx = 0f;
			float offy = 0f;


			int i = 0;

			float ix = 0;
			float iy = 0;
			foreach( var tx in textureImages ) {
				drawWidth = renderTextureDimensions.x / resolution; //tx.width;
				drawHeight = renderTextureDimensions.y / resolution; //tx.height;


				if( offx + drawWidth > renderTextureDimensions.x ) {
					offx = 0f;
					offy += rowMax;
					ix = 0;
					iy++;
					rowMax = 0f;
				}
				if( rowMax < drawHeight )
					rowMax = drawHeight;

				var uv = new Vector4( offx/vpWidth, offy/vpHeight, (offx+drawWidth)/vpWidth, (offy+drawHeight)/vpHeight );
				//Debug.Log ("draw w/h : " + drawWidth + "," + drawHeight + " off x/y: " + offx + " , " + offy + " ; uv:   ( " + uv.x + " , " + uv.y + " , " + uv.z + " , " + uv.w + " )"  );
				//Debug.Log ("draw w/h : " + drawWidth + "," + drawHeight);

				float rw = (drawWidth/vpWidth);
				float rh = (drawHeight/vpHeight);
				uv = new Vector4 ((rw*ix), (rh*iy), (rw*ix)+(rw), (rh*iy)+(rh));


				textureUVs[i] = uv;


				offx += drawWidth;
				i++;
				ix++;
			}
		}


	}


#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(EmojiFontCache))]
public class EmojiFontCacheEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI ();
		if (Application.isPlaying == false) {
			return;
		}
		var targ = (target as EmojiFontCache);
		if( GUILayout.Button("Update") ){
			EmojiFontCache.SetDirty ();
		}
		/*if( GUILayout.Button("Add Texture") ){
			targ.AddTestTexture ();
		}*/
	}
}
#endif 



}