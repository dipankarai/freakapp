﻿using UnityEngine;
using System.Linq;
using Tastybits.EmojiAssist.TTF;


namespace Tastybits.EmojiAssist {


	public class CustomEmojiPack {
		public static TTFFont font;
		public static System.Collections.Generic.Dictionary<int,Texture2D> fontTextures = new System.Collections.Generic.Dictionary<int,Texture2D>();
		static string filePath;


		#if EMOJIASSIST_DEV
		static bool verbose=false;
		#else
		static bool verbose=false;
		#endif 

		private static Texture2D _GetGlyphForChar_TTF( string ch ){
			int utf32 = char.ConvertToUtf32(ch, 0);

			//Debug.Log( "asdsad");
			if( fontTextures.ContainsKey( utf32 ) ) {
				//Debug.Log( "ffff");
				return fontTextures[utf32];
			}
			//Debug.Log( "aaaa");

			//Debug.LogError("Getting code point : " + string.Format("{0:X}", utf32) );

			if( font== null ){
				//Debug.Log( "aaaa");
				// creating font instance....
				//Debug.Log("Creating font instance...");
				font = TTFFont.Read( filePath ); 
				font.Analyze();
			}
			//Debug.Log( "bbbb");

			int glyphid = font.charMap.Lookup((new CodePoint(utf32)));

			byte[] bytes=null;
			if( font.sbix != null ) {
				var glyph = font.glyphs[glyphid];
				bytes = font.sbix.GetBytes( font.reader, glyphid, true );
				if( bytes == null || bytes.Length == 0 ) {
					Debug.Log("bytes is null");
				} 
			}
			else if( font.cblc!=null && font.cblc.bmstList.Count > 0 ) {

				var bmst = font.cblc.bmstList[0];
				//Debug.LogError("Locating glyphid = " + glyphid + " bmst.indexSubtableArray=" + bmst.indexSubtableArray.Count );
				foreach( var isa in bmst.indexSubtableArray ) {
					//Debug.Log("isa.firstGlyphIndex=" + isa.firstGlyphIndex + " last = "+ isa.lastGlyphIndex );	
					if( glyphid >= isa.firstGlyphIndex && glyphid <= isa.lastGlyphIndex ) {
						// found it...
						var arrayIndex = glyphid - isa.firstGlyphIndex;
						//Debug.Log("adasdasg" );	
						var hdr = isa.indexSubHeader;
						//if( (isa.indexSubElem as EBLC_IndexSubElem_Format1) == null ) Debug.LogError("EBLC_IndexSubElem_Format1 is null");
						//if( (isa.indexSubElem as EBLC_IndexSubElem_Format1).offsetArray == null ) Debug.LogError("array is null");
						//Debug.Log("222  (isa.indexSubElem as EBLC_IndexSubElem_Format1).offsetArray.Len= " + (isa.indexSubElem as EBLC_IndexSubElem_Format1).offsetArray.Length );	
						var glyphOffset = (isa.indexSubElem as EBLC_IndexSubElem_Format1).offsetArray[arrayIndex];
						var imageDataOffset = hdr.imageDataOffset;

						//Debug.LogError("Data format     : " + hdr.imageFormat );
						//Debug.LogError("ImageDataOffset : " + hdr.imageDataOffset );
						var reader = font.reader;

						if( hdr.imageFormat == 17 ) { // metrics, PNG image data
							SfntTables.SeekToTable( reader, font.tables, FourCC.CBDT, true );
							var tbl_offset = reader.GetPosition();

							var offFinal = tbl_offset + imageDataOffset+glyphOffset;
							if(verbose) Debug.Log("Seeking to final position : " + offFinal );
							reader.Seek( (uint)offFinal );
							 
							//var dataLen = font.reader.ReadUInt32BE();
							/*var bigGlyphMetrics = new BigGlyphMetrics(){
								height = reader.ReadByte(), width = reader.ReadByte(), horiBearingX = reader.ReadChar(), horiBearingY = reader.ReadChar(), 
								horiAdvance = reader.ReadByte(), vertBearingX = reader.ReadChar(), vertBearingY = reader.Readchar(),  
								vertAdvance =  reader.ReadByte(), 
							};*/
							var smallGlyphMetrics = new SmallGlyphMetrics(){
								height = reader.ReadByte(), width = reader.ReadByte(), BearingX = reader.ReadChar(), BearingY = reader.ReadChar(),
								Advance = reader.ReadByte()
							};

							if(verbose) Debug.Log("height / width = " + smallGlyphMetrics.height + " , " + smallGlyphMetrics.width );
							if(verbose) Debug.Log("bearx / beary = " + (int)smallGlyphMetrics.BearingX + " , " + (int)smallGlyphMetrics.BearingY );
							if(verbose) Debug.Log("Advance = " + smallGlyphMetrics.Advance );


							//Debug.Log( "width /height = "+smallGlyphMetrics.width + " , " + smallGlyphMetrics.height );
							var dataLen = reader.ReadUInt32BE();
							if(verbose) Debug.Log( "dataLen = " + dataLen );

							bytes = reader.ReadBytes( (int)dataLen );

						} else { 
							Debug.LogError("unsupported dataformat : " + hdr.imageFormat );


						}

						break;
					}
				}
			}

			// conert to texture..
			var tex = new Texture2D(2,2);
			tex.LoadImage( bytes );
			tex.Apply();
			if( bytes == null || bytes.Length == 0 ) return tex;
			fontTextures.Add( utf32, tex );
			//var bytesEnc = tex.EncodeToPNG();
			//System.IO.File.WriteAllBytes( Application.dataPath + "/Tst.png", bytesEnc );

			//Tastybits.EmojiAssist.TTF.TTFFont.Read( "		
			return fontTextures[utf32];
		}

		static Texture2D _GetGlyphForChar_IMG( string ch ) {
			var arr = ch.AsCodePoints().ToArray();
			if( arr.Length == 0 ) {
				Debug.LogError("Error getting codepoint in character string");
				return null;
			}

			string filePath = "Emoji/"+arr[0].ToString("X").ToLower();
			//Debug.Log ("loading from path : " + filePath );
			if( arr.Length>1 ) {
				filePath += "-" + arr[1].ToString("X").ToLower();
			}
			bool fontCanBeRenderedByUnity = false;
			if( filePath.Contains( "segui" ) ) {
				fontCanBeRenderedByUnity = true;
			}

			var tex = (Texture2D)Resources.Load( filePath, typeof(Texture2D) );
			/*if( Application.isPlaying == false ) {
					Debug.Log ("asdasd");
				}*/
			if( tex == null ) {
				Debug.LogError("tex is null when loading from path = " + filePath);
			}
			return tex;

		} 

		public static Texture2D GetGlyphForChar( string ch, string ttfFile ) {
			if( !string.IsNullOrEmpty(ttfFile) ) {

				string p = ttfFile;
				if( ttfFile.StartsWith("Assets/") ) 
					p = ttfFile.Replace("Assets/", Application.dataPath + "/" );
				filePath = p;

				if( System.IO.File.Exists(filePath) == false ) {
					Debug.LogError("filePath = " + filePath + " doesnt exist");
					return null;
				}
				return _GetGlyphForChar_TTF( ch );
			}
			return _GetGlyphForChar_IMG( ch );
		}

	} // ~class

}