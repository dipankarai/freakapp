﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tastybits.EmojiAssist {


	public static class StringExtension {
		public static IEnumerable<int> AsCodePoints(this string s) {
			for(int i = 0; i < s.Length; ++i) {
				//Debug.LogError("index="+i);
				yield return char.ConvertToUtf32(s, i);
				if( char.IsHighSurrogate(s, i) )
					i++;
			}
		}
	}

}