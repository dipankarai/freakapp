﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(UnityEngine.UI.InputField))]
[ExecuteInEditMode]
public class InputFieldEmojiSupport : MonoBehaviour {

	void Awake(){
		if (Application.isPlaying == false && Application.isEditor) {
			Debug.LogError ("cannot add object in editmode");
			Object.DestroyImmediate (this); 
		}
	}
	// Update is called once per frame
	void Update () {
		var i = this.GetComponent<UnityEngine.UI.InputField> ();
		if (i != null && i.textComponent != null && i.textComponent.GetComponent<Tastybits.EmojiAssist.EmojiSupport> () != null) {
			i.textComponent.text = i.text;
		}
	}
}
