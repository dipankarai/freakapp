﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using Tastybits;
using UnityEngine.UI;


namespace Tastybits.EmojiAssist {


	[CustomEditor(typeof(EmojiSupport))]
	public class EmojiSupportEditor : UnityEditor.Editor {
		/*protected override void OnEnable() {
			base.OnEnable();
		}*/


		public override void OnInspectorGUI() {
			var targ = this.target as EmojiSupport;


			targ.shortCodesEnabled = EditorGUILayout.Toggle( "Enable short codes", targ.shortCodesEnabled );
			GUILayout.Label("* To test: try inputting :smiley: in the text");

			if( targ.TextComponentIsNativeImpl && targ.shortCodesEnabled ) {
				EditorGUILayout.HelpBox ("Shortcodes works best if the Text field type used is Tastybits.EmojiAssist.TextWithEmoji instead of UnityEngine.UI.Text.\nYou can substitude the Text component with the TextWithEmoji component since it's just an extension of UnityEngine.UI.Text", MessageType.Warning );
			}



			#if EMOJIASSIST_DEV
		
			GUILayout.Label("Dev:" );
			if( GUILayout.Button("Emoji Keyboard") ) {
				EditorEmojiKeyboard.open ();
			}

			if( GUILayout.Button ("Add smiley to text")) {
				targ.Text += System.Char.ConvertFromUtf32 (0x1F600);
				targ.SetDirty ();
			}


			if( GUILayout.Button ("Add smiley with bandages to text")) {
				targ.Text += System.Char.ConvertFromUtf32 (0x1f915);
				targ.SetDirty ();
			}
				

			if (GUILayout.Button ("Add moji seq(boy+medium tone")) {
				string e = System.Char.ConvertFromUtf32 (0x1f466) + System.Char.ConvertFromUtf32 (0x1f3fd);
				targ.Text += e;
				targ.gameObject.SetActive (false);
				targ.gameObject.SetActive (true);
			}


			if (GUILayout.Button ("Add unsupported x1")) {
				string e = System.Char.ConvertFromUtf32 (0x1F910); // + System.Char.ConvertFromUtf32 (0x1f3fd);
				targ.Text += e;
				targ.gameObject.SetActive (false);
				targ.gameObject.SetActive (true);
			}

			if (GUILayout.Button ("Add unsupported x2")) {
				string e = System.Char.ConvertFromUtf32 (0x263A) + System.Char.ConvertFromUtf32 (0xFE0F);
				targ.Text += e;
				targ.gameObject.SetActive (false);
				targ.gameObject.SetActive (true);
			}
				
			if (GUILayout.Button ("Add unsupported x3")) {
				string e = System.Char.ConvertFromUtf32 (0x263A) + System.Char.ConvertFromUtf32 (0xFE0F);
				targ.Text += e;
				targ.gameObject.SetActive (false);
				targ.gameObject.SetActive (true);
			}

			string strCodes = "";
			foreach( var codepoint in targ.Text.AsCodePoints() ) {
				strCodes += " 0x" + codepoint.ToString("X");
			}
			GUILayout.Label( strCodes );


			#endif 

		}
	}

}


