using UnityEditor;
using UnityEngine;
using Tastybits;
using UnityEngine.UI;


namespace Tastybits.EmojiAssist {
	

	[CustomEditor(typeof(TextWithEmojiSupport))]
	public class TextWithEmojiInspector : UnityEditor.UI.TextEditor {

		// use this to to add a text element.
		[MenuItem("GameObject/UI/Text (With Emoji Support)")]
		public static void _(){
			var go = new GameObject("Text(WithEmojiSupport)");
			go.AddComponent<RectTransform>();
			var text = go.AddComponent<TextWithEmojiSupport>();
			//text.text = "New Text " + System.Char.ConvertFromUtf32(0x1F602);
			if( UnityEditor.Selection.activeGameObject!=null ) {
				go.transform.parent = UnityEditor.Selection.activeGameObject.transform;
			} else {
			}
			go.SetActive(false);
			text.enabled=false;
			text.text = "New Text :smiley:";
			text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
			text.SetAllDirty();
			var p = text.rectTransform.localPosition;
			p.x--;
			text.rectTransform.localPosition = p;
			EditorApplication.CallbackFunction tmp;
			int cnt=0;
			tmp=()=>{
				if(cnt++>3){
					EditorApplication.update -= tmp;
					text.enabled=true;
					go.SetActive(true);
				}
			};
			EditorApplication.update += tmp;
		}


		public static void Test() {
			EditorApplication.update -= Test;
		}



	    public override void OnInspectorGUI() {
			var targ = this.target as TextWithEmojiSupport;
			//GUILayout.Label("text=" +  targ.text );
			//GUILayout.Label("textemojified=" +  targ.TextEmojified );
			//GUILayout.Label("textunemojified=" +  targ.TextUnemojified );

	        base.OnInspectorGUI();

			(target as TextWithEmojiSupport).shortCodesEnabled = EditorGUILayout.ToggleLeft( "Enable short codes", (target as TextWithEmojiSupport).shortCodesEnabled );
			GUILayout.Label("* To test: try inputting :smiley: in the text");


			/*GUILayout.BeginHorizontal();
			if( GUILayout.Button ("Set Random Emoji" ) ) {
				// Pick a random emoticon
				var emojiCharCode = Random.Range( 0x1F600, 0x1F64F );
				string str = System.Char.ConvertFromUtf32( emojiCharCode );
				(this.target as TextWithEmojiSupport ).text = str + " Emoji support" + str;
				(this.target as TextWithEmojiSupport).SetAllDirty();
				UnityEditor.EditorUtility.SetDirty( target );
			}
			if( GUILayout.Button ("Add Random Emoji" ) ) {
				// Pick a random emoticon
				var emojiCharCode = Random.Range( 0x1F600, 0x1F64F );
				string str = System.Char.ConvertFromUtf32( emojiCharCode );
				(this.target as TextWithEmojiSupport ).text += str;
				(this.target as TextWithEmojiSupport).SetAllDirty();
				UnityEditor.EditorUtility.SetDirty( target );
			}
			GUILayout.EndHorizontal();*/
	    }
	}
	
}
