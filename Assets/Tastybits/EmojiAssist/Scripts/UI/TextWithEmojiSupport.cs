using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;


namespace Tastybits.EmojiAssist {
		

	[ExecuteInEditMode] 
	//[RequireComponent(typeof(EmojiSupport))]
	public class TextWithEmojiSupport : Text {
		public static bool InputIsAppending = false;
		public static bool _verbose=false;

		private float _fontHeight;
		private float _fontWidth;
		
		public float ImageScale=1f;
		
		UIVertex[] m_TempVerts  =new UIVertex[4];


		EmojiSupport emojiSupport {
			get {
				var tmp = this.gameObject.GetComponent<EmojiSupport> ();
				if (tmp == null) {
					//Debug.Log ("Added");
					tmp = this.gameObject.AddComponent<EmojiSupport> ();
				}
				return tmp;
			}
		}
		

		public bool shortCodesEnabled {
			get {
				return emojiSupport.shortCodesEnabled;
			}
			set { 
				emojiSupport.shortCodesEnabled = value;
			}
		}



		string _prevText="";
		string _formattetText="";
		private string _text {
			get {
				if( shortCodesEnabled ) {
					if( string.IsNullOrEmpty(_prevText) || _prevText!=this.text ) {
						_prevText = this.text;
						if( !this.text.Contains(":") ) {
							_formattetText = this.text;
						} else {
							_formattetText = Tastybits.EmojiAssist.Shortcodes.Emojify( this.text );
					 	}
					} 
					return _formattetText;
				}
				return this.text;
			}
		}

		// use this to get the string with the emojis as unicode chars in them.
		public string TextEmojified {
			get {
				return _text;
			}
		}

		// use this to get the text that is not emojifed.
		public string TextUnemojified {
			get {
				return Tastybits.EmojiAssist.Shortcodes.Unemojify( _text );
			}
		}
		
		
		// Get the number of emoji's available. // change it to the replacement character...
		public int CountNumberOfEmojiAvail( out int numNormal ){
			int ret = 0;
			numNormal = 0;
			foreach( int codePoint in _text.AsCodePoints() ) {
				//Debug.Log( "codepnt = " + codePoint + " ch = " + char.ConvertFromUtf32(codePoint) );
				if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
					ret++;
				} else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
					ret++;
				} else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
					ret++;
				} else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
					ret++;
				} else {
					numNormal++;
				}
			}
			return ret;
		}


		public string TextWithEmojisAsReplacementChars {
			get {
				return emojiSupport.TextWithEmojisAsReplacementChars;
			}
		}



		// used to get a text string with the emoji's replacedwith 
		string _textAsStringWithEmojisReplacedWithInvalidChar {
			get {
				return emojiSupport.TextAsStringWithEmojisReplacedWithInvalidChar;
			}
		}

		
		public string TextAsStringWithOutEmojisInIT {
			get {
				string result = "";
				foreach( int codePoint in _text.AsCodePoints() ) {
					if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
					} else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
					} else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
					} else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
					} else {
						string strCh = System.Char.ConvertFromUtf32( codePoint );
						int szBefore = result.Length;
						result += strCh;
						if( result.Length > szBefore + 1 ) {
							Debug.LogError("Warning the string increasd more than one character");
						}
					}
				}
				return result;
			}
			
		}


		protected override void OnPopulateMesh( VertexHelper toFill ) {
			//activateTheese.Clear();
			//DeactivateAllEmojiChildren(false);
			NewPopulate( toFill );
		}
		
		
		public int GetCharCnt() {
			return this.cachedTextGenerator.characterCount;
		}
		
		
		public int GetCharCntVis() {
			return this.cachedTextGenerator.characterCount;
		}
		

		protected override void OnEnable () {
			base.OnEnable();
		}


		protected override void OnDisable () {
			base.OnDisable();
		}
			
	
		/*void SetPositionOfEmojis(){
			IList<UIVertex> verts = this.cachedTextGenerator.verts ;
			
			int chidx=0;
			int j=0;
			var arr = _text.AsCodePoints().ToArray();
			
			float posoff=0f;
			for( int it = 0; it < verts.Count; it++) {
				// each time j is 0 we are standing at a chacter...
				if( j == 0 ) {
					
					// Get the character of the string..
					var charInfo = this.cachedTextGenerator.characters[j];
					//this.cachedTextGenerator.GetLinesArray()[0].startCharIdx;
					
					// invisible charas in the end?
					if( chidx >= arr.Count() ) {
						continue;
					}
					
					var codePoint = arr[chidx];
					
					//string strChar = System.Char.ConvertFromUtf32(codePoint);
					
					posoff+=charInfo.charWidth;
					var vp1 = verts[it].position;
					var vp2 = verts[it+2].position;
					
					//charInfo.
					//Debug.LogError ( "#" + chidx + " ch:" + strChar + " posx:" + charInfo.cursorPos.x );
					
					//Debug.Log ( "codePoint=" + codePoint );
					if( IsEmojiChar(codePoint) ) {
						var vert = verts[it];  
						UnityEngine.Color c = vert.color;
						c.a = 0f;
						
						vert.color = c; // change color
						verts[it] = vert;
						
						var vert2 = verts[it+1];  
						vert2.color = c; // change color
						verts[it+1] = vert2;
						
						var vert3 = verts[it+2];  
						vert3.color = c; // change color
						verts[it+2] = vert3;
						
						var vert4 = verts[it+3];  
						vert4.color = c; // change color
						verts[it+3] = vert4;
						
						//var emoji = GetEmoji();
						//var rawImg = emoji.GetComponent<RawImage>();
						//activateTheese.Add(emoji.gameObject);
						
						//Debug.Log ( "added emoji" );
						//emoji.SetEmoji( codePoint );
						//emoji.gameObject.SetActive(true);
						
						//	Debug.LogError ( "charInfo.cursorPos = " + charInfo.cursorPos );
						var vp_x = (Mathf.Max(vp1.x,vp2.x) - Mathf.Min(vp1.x,vp2.x)); 
						var vp_y = (Mathf.Max(vp1.y,vp2.y) - Mathf.Min(vp1.y,vp2.y)); 
						//Debug.Log ( "vp_y = " + vp_y );
						//var rt = emoji.GetComponent<RectTransform>();
						//rt.localPosition = new Vector2(vp1.x + (vp_x/2f), vp1.y - (vp_y/2f));
						//rt.sizeDelta = new Vector2 (vp_x, vp_y);
						
						
					} else {
						var vert = verts[it];  
						var p = vert.position;
						vert.position = p;
						//vert.color = Color.green; // change color
						verts[it] = vert;
						
					}
					
					
					//charInfo.cursorPos
					
				}
				if( j++ >= 3 ) {
					j=0;
					chidx++;
				}
			}
			
		}*/

		
		void NewPopulate( VertexHelper toFill ) {
			if( this.font == null ) {
				return;
			}
			if( InputIsAppending ) {
				Debug.Log("input is changing text ignore it " );
			}
			if( _verbose ) Debug.Log("New populate");
			
			//System.Reflection.BindingFlags bindFl = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
			
			this.m_DisableFontTextureRebuiltCallback = true;
			Vector2 size = this.rectTransform.rect.size;
			TextGenerationSettings generationSettings = this.GetGenerationSettings (size);
			
			//this.cachedTextGenerator.Populate ( _text, generationSettings);
			this.cachedTextGenerator.Populate( _textAsStringWithEmojisReplacedWithInvalidChar, generationSettings );
			
			Rect rect = this.rectTransform.rect;
			
			Vector2 textAnchorPivot = Text.GetTextAnchorPivot( this.alignment );
			Vector2 zero = Vector2.zero;
			zero.x = ((textAnchorPivot.x != 1f) ? rect.xMin : rect.xMax );
			zero.y = ((textAnchorPivot.y != 0f) ? rect.yMax : rect.yMin );
			Vector2 vector = base.PixelAdjustPoint (zero) - zero;
			
			IList<UIVertex> verts = this.cachedTextGenerator.verts ;
			
			//SetPositionOfEmojis();
			
			//DoFillVBO( this.cachedTextGenerator, ref verts );
			
			float num = 1f / this.pixelsPerUnit;
			int num2 = verts.Count - 4;
			toFill.Clear ();
			
			
			if (vector != Vector2.zero ) {
				for (int i = 0; i < num2; i++) {
					int num3 = i & 3;
					this.m_TempVerts [num3] = verts[ (i) ];
					UIVertex[] expr_147_cp_0 = this.m_TempVerts;
					int expr_147_cp_1 = num3;
					expr_147_cp_0 [expr_147_cp_1].position = expr_147_cp_0 [expr_147_cp_1].position * num;
					UIVertex[] expr_16B_cp_0_cp_0 = this.m_TempVerts;
					int expr_16B_cp_0_cp_1 = num3;
					expr_16B_cp_0_cp_0 [expr_16B_cp_0_cp_1].position.x = expr_16B_cp_0_cp_0 [expr_16B_cp_0_cp_1].position.x + vector.x;
					UIVertex[] expr_190_cp_0_cp_0 = this.m_TempVerts;
					int expr_190_cp_0_cp_1 = num3;
					expr_190_cp_0_cp_0 [expr_190_cp_0_cp_1].position.y = expr_190_cp_0_cp_0 [expr_190_cp_0_cp_1].position.y + vector.y;
					if (num3 == 3) {
						toFill.AddUIVertexQuad (this.m_TempVerts);
					}
					
					//string tmpCh = System.Text.RegularExpressions.Regex.Unescape( "\ud83d\ude02" );
					
					
				}
			} else {
				for (int j = 0; j < num2; j++) {
					int num4 = j & 3;
					this.m_TempVerts [num4] = verts[ (j) ];
					UIVertex[] expr_201_cp_0 = this.m_TempVerts;
					int expr_201_cp_1 = num4;
					expr_201_cp_0 [expr_201_cp_1].position = expr_201_cp_0 [expr_201_cp_1].position * num;
					if (num4 == 3) {
						toFill.AddUIVertexQuad (this.m_TempVerts);
					}
				}
			}
			
			//this.GetType().GetField( "m_TempVerts", bindFl ).SetValue(this, this.m_TempVerts );
			
			
			this.m_DisableFontTextureRebuiltCallback = false;
		}
		
		
		void Update() {
			//RefreshEmojis();
		}
		
		#if UNITY_EDITOR
		void UpdateEditor() {
			//RefreshEmojis();
		}
		#endif

	
		// The 
		void OrgPopulate( VertexHelper toFill ) {
			if( this.font == null ) {
				return;
			}
			//System.Reflection.BindingFlags bindFl = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
			
			this.m_DisableFontTextureRebuiltCallback = true;
			Vector2 size = this.rectTransform.rect.size;
			TextGenerationSettings generationSettings = this.GetGenerationSettings (size);
			this.cachedTextGenerator.Populate (_text, generationSettings);
			Rect rect = this.rectTransform.rect;
			
			
			Vector2 textAnchorPivot = Text.GetTextAnchorPivot( this.alignment );
			Vector2 zero = Vector2.zero;
			zero.x = ((textAnchorPivot.x != 1f) ? rect.xMin : rect.xMax );
			zero.y = ((textAnchorPivot.y != 0f) ? rect.yMax : rect.yMin );
			Vector2 vector = base.PixelAdjustPoint (zero) - zero;
			IList<UIVertex> verts = this.cachedTextGenerator.verts ;
			float num = 1f / this.pixelsPerUnit;
			int num2 = verts.Count - 4;
			toFill.Clear ();
			
			
			if (vector != Vector2.zero ) {
				for (int i = 0; i < num2; i++) {
					int num3 = i & 3;
					this.m_TempVerts [num3] = verts[ (i) ];
					UIVertex[] expr_147_cp_0 = this.m_TempVerts;
					int expr_147_cp_1 = num3;
					expr_147_cp_0 [expr_147_cp_1].position = expr_147_cp_0 [expr_147_cp_1].position * num;
					UIVertex[] expr_16B_cp_0_cp_0 = this.m_TempVerts;
					int expr_16B_cp_0_cp_1 = num3;
					expr_16B_cp_0_cp_0 [expr_16B_cp_0_cp_1].position.x = expr_16B_cp_0_cp_0 [expr_16B_cp_0_cp_1].position.x + vector.x;
					UIVertex[] expr_190_cp_0_cp_0 = this.m_TempVerts;
					int expr_190_cp_0_cp_1 = num3;
					expr_190_cp_0_cp_0 [expr_190_cp_0_cp_1].position.y = expr_190_cp_0_cp_0 [expr_190_cp_0_cp_1].position.y + vector.y;
					if (num3 == 3)
					{
						toFill.AddUIVertexQuad (this.m_TempVerts);
					}
				}
			} else {
				for (int j = 0; j < num2; j++) {
					int num4 = j & 3;
					this.m_TempVerts [num4] = verts[ (j) ];
					UIVertex[] expr_201_cp_0 = this.m_TempVerts;
					int expr_201_cp_1 = num4;
					expr_201_cp_0 [expr_201_cp_1].position = expr_201_cp_0 [expr_201_cp_1].position * num;
					if (num4 == 3) {
						toFill.AddUIVertexQuad (this.m_TempVerts);
					}
				}
			}
			
			this.m_DisableFontTextureRebuiltCallback = false;
		}
	}


}









