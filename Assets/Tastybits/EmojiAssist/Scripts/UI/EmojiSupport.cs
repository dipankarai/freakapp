using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


namespace Tastybits.EmojiAssist
{
	
    [AddComponentMenu("UI/Emoji/EmojiSupport", 14)]
	[RequireComponent(typeof(UnityEngine.UI.Text))]
	[ExecuteInEditMode]
	public class EmojiSupport : UnityEngine.UI.BaseMeshEffect
    {
		protected EmojiSupport():base() {
		}

		[SerializeField] 
		bool _verbose = false;

		// set to false if we dont want to detect shortcode like
		// ":smiley:" in the text and show it as a emoji.
		public bool shortCodesEnabled = true;

		// Formattet text.
		[SerializeField]
		[HideInInspector]
		string _prevText="";

		// Formattet text.
		[SerializeField]
		[HideInInspector]
		string _formattetText="";


#if UNITY_EDITOR
        protected override void OnValidate() {
            base.OnValidate();
        }
#endif


		bool siblingIsText {
			get {
				if (this.GetComponent<TextWithEmojiSupport> () != null ) {
					return false;
				}
				return this.GetComponent<UnityEngine.UI.Text> ();
			}
		}


		public bool TextComponentIsNativeImpl {
			get {
				return siblingIsText;
			}
		}


		public UnityEngine.UI.Text textComponent {
			get {
				if (this.GetComponent<TextWithEmojiSupport> () != null ) {
					return this.GetComponent<TextWithEmojiSupport> ();
				}
				return this.GetComponent<UnityEngine.UI.Text> ();
			}
		}


		void OnEnable(){
			var _text = textComponent;
			if (_text.material == null) {
				Debug.Log ("setting font material" );
				_text.material = EmojiFontCache.Instance.fontMat;
			}

			// if this text field is bound to an input field we have to 
			// add the inputfieldemojisupport componnet to the input field.
			// in order to make sure that the inputfield fixes a problem with 
			// unity where text is not updated every frame.
			if (Application.isPlaying) {
				if (this.transform.parent != null && this.transform.parent.GetComponent<UnityEngine.UI.InputField> () != null) {
					var inp = this.transform.parent.GetComponent<UnityEngine.UI.InputField> ();
					if (inp.textComponent == this.GetComponent<UnityEngine.UI.Text> () && inp.GetComponent<InputFieldEmojiSupport>() == null ) {
						inp.gameObject.AddComponent<InputFieldEmojiSupport> ();
					}
				}
			}
		}


		void OnDestroy() {
			this.GetComponent<UnityEngine.UI.Text> ().material = null;
		}


		public string Text {
			get {
				string this_text = this.GetComponent<UnityEngine.UI.Text> ().text;
				if( shortCodesEnabled ) {
					if( string.IsNullOrEmpty(_prevText) || _prevText!=this_text ) {
						_prevText = this_text;
						if( !this_text.Contains(":") ) {
							_formattetText = this_text;
						} else {
							_formattetText = Tastybits.EmojiAssist.Shortcodes.Emojify( this_text );
						}
					} 
					return _formattetText;
				}
				return this_text; 
			}
			set {
				textComponent.text = value;
			}
		}





		void Update() {
			var _text = textComponent;
			var mat = EmojiFontCache.Instance.fontMat;
			if (_text.material != mat) {
				//Debug.Log ("setting font material" );
				_text.material = mat;
			}

			// we have to change the text if the sibling 
			// is the native implementation.
			if( shortCodesEnabled && siblingIsText ) { 
				var this_text = _text.text;
				if( string.IsNullOrEmpty(_prevText) || _prevText!=this_text ) {
					_prevText = this_text;
					if( !this_text.Contains(":") ) {
						_formattetText = this_text;
					} else {
						_formattetText = Tastybits.EmojiAssist.Shortcodes.Emojify( this_text );
					}
				} 

				textComponent.text = _formattetText;
			}
			if( _text.material == null ) 
				_text.material = EmojiFontCache.Material;
		}


		public void SetDirty() {
			var pre = this.gameObject.activeSelf;
			this.gameObject.SetActive (false);

			graphic.SetVerticesDirty ();
			this.graphic.SetAllDirty ();
			this.gameObject.SetActive (pre);
		}


		// used to get a text string with the emoji's replacedwith 
		public string TextAsStringWithEmojisReplacedWithInvalidChar {
			get {
				string result = "";
				foreach( int codePoint in Text.AsCodePoints() ) {
					if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
						result += System.Char.ConvertFromUtf32( 0xFFFD );
					} else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
						result += System.Char.ConvertFromUtf32( 0xFFFD );
					} else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
						result += System.Char.ConvertFromUtf32( 0xFFFD );
					} else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
						result += System.Char.ConvertFromUtf32( 0xFFFD );
					} else {
						string strCh = System.Char.ConvertFromUtf32( codePoint );
						int szBefore = result.Length;
						result += strCh;
						if( result.Length > szBefore + 1 ) {
							Debug.LogError("Warning the string increasd more than one character");
						}
					}
				}
				return result;
			}
		}


		// use this to get the string with the emojis as unicode chars in them.
		public string TextEmojified {
			get {
				return Text;
			}
		}


		// use this to get the text that is not emojifed.
		public string TextUnemojified {
			get {
				return Tastybits.EmojiAssist.Shortcodes.Unemojify( Text );
			}
		}


		public static bool IsEmojiModifier( int codePoint ) {
			return codePoint >= 0x1F3FB && codePoint <= 0x1F3FF;
		}


		public static bool IsVariationSelector( int codePoint ) {
			return codePoint == 0xFE0F;
		}



		static bool InR( int val, int min, int max ) {
			return (((val) >= (min)) && ((val) <= (max)));
		}
			

		public static bool IsEmojiChar( int codePoint ) {
			// Musical: [U+1D000, U+1D24F]
			// Enclosed Alphanumeric Supplement: [U+1F100, U+1F1FF]
			// Enclosed Ideographic Supplement: [U+1F200, U+1F2FF]
			// Miscellaneous Symbols and Pictographs: [U+1F300, U+1F5FF]
			// Supplemental Symbols and Pictographs: [U+1F900, U+1F9FF]
			// Emoticons: [U+1F600, U+1F64F]
			// Transport and Map Symbols: [U+1F680, U+1F6FF]
			if( InR(codePoint,0x1D000, 0x1F9FF ) ) {
				return true;
			}
			if( codePoint == 0x1F915 ) {
				return true;
			}
			int hs = codePoint;
			if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
			} else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
			} else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
			} else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
			} else if(	// Latin-1 Supplement
				hs == 0x00A9 || hs == 0x00AE
				// General Punctuation
				||	hs == 0x203C || hs == 0x2049
				// Letterlike Symbols
				||	hs == 0x2122 || hs == 0x2139
				// Arrows
				||	InR(hs, 0x2194, 0x2199) || InR(hs, 0x21A9, 0x21AA)
				// Miscellaneous Technical
				||	InR(hs, 0x231A, 0x231B) || InR(hs, 0x23E9, 0x23F3) || InR(hs, 0x23F8, 0x23FA) || hs == 0x2328 || hs == 0x23CF
				// Geometric Shapes
				||	InR(hs, 0x25AA, 0x25AB) || InR(hs, 0x25FB, 0x25FE) || hs == 0x25B6 || hs == 0x25C0
				// Miscellaneous Symbols
				||	InR(hs, 0x2600, 0x2604) || InR(hs, 0x2614, 0x2615) || InR(hs, 0x2622, 0x2623) || InR(hs, 0x262E, 0x262F)
				||	InR(hs, 0x2638, 0x263A) || InR(hs, 0x2648, 0x2653) || InR(hs, 0x2665, 0x2666) || InR(hs, 0x2692, 0x2694)
				||	InR(hs, 0x2696, 0x2697) || InR(hs, 0x269B, 0x269C) || InR(hs, 0x26A0, 0x26A1) || InR(hs, 0x26AA, 0x26AB)
				||	InR(hs, 0x26B0, 0x26B1) || InR(hs, 0x26BD, 0x26BE) || InR(hs, 0x26C4, 0x26C5) || InR(hs, 0x26CE, 0x26CF)
				||	InR(hs, 0x26D3, 0x26D4) || InR(hs, 0x26D3, 0x26D4) || InR(hs, 0x26E9, 0x26EA) || InR(hs, 0x26F0, 0x26F5)
				||	InR(hs, 0x26F7, 0x26FA)
				||	hs == 0x260E || hs == 0x2611 || hs == 0x2618 || hs == 0x261D || hs == 0x2620 || hs == 0x2626 || hs == 0x262A
				||	hs == 0x2660 || hs == 0x2663 || hs == 0x2668 || hs == 0x267B || hs == 0x267F || hs == 0x2699 || hs == 0x26C8
				||	hs == 0x26D1 || hs == 0x26FD
				// Dingbats
				||	InR(hs, 0x2708, 0x270D) || InR(hs, 0x2733, 0x2734) || InR(hs, 0x2753, 0x2755)
				||	InR(hs, 0x2763, 0x2764) || InR(hs, 0x2795, 0x2797)
				||	hs == 0x2702 || hs == 0x2705 || hs == 0x270F || hs == 0x2712 || hs == 0x2714 || hs == 0x2716 || hs == 0x271D
				||	hs == 0x2721 || hs == 0x2728 || hs == 0x2744 || hs == 0x2747 || hs == 0x274C || hs == 0x274E || hs == 0x2757
				||	hs == 0x27A1 || hs == 0x27B0 || hs == 0x27BF
				// CJK Symbols and Punctuation
				||	hs == 0x3030 || hs == 0x303D
				// Enclosed CJK Letters and Months
				||	hs == 0x3297 || hs == 0x3299
				// Supplemental Arrows-B
				||	InR(hs, 0x2934, 0x2935)
				// Miscellaneous Symbols and Arrows
				||	InR(hs, 0x2B05, 0x2B07) || InR(hs, 0x2B1B, 0x2B1C) || hs == 0x2B50 || hs == 0x2B55
			)
			{
			} else {
				return false;
			}
		

			return true;
		}


		public string TextAsStringWithOutEmojisInIT {
			get {
				string result = "";
				foreach( int codePoint in Text.AsCodePoints() ) {
					if( codePoint >= 0x1F600 && codePoint <= 0x1F64F ) { // Emoticons
					} else if( codePoint >= 0x2700 && codePoint <= 0x27BF ) { // Dingbats
					} else if( codePoint >= 0x1F680 && codePoint <= 0x1F6C0 ) { // Transport and map symbols
					} else if( codePoint >= 0x1F300 && codePoint <= 0x1F5FF ) { // Miscellaneous Symbols and Pictographs
					} else {
						string strCh = System.Char.ConvertFromUtf32( codePoint );
						int szBefore = result.Length;
						result += strCh;
						if( result.Length > szBefore + 1 ) {
							Debug.LogError("Warning the string increasd more than one character");
						}
					}
				}
				return result;
			}

		}


		public string TextWithEmojisAsReplacementChars {
			get {
				string result = "";
				foreach( int codePoint in Text.AsCodePoints() ) {
					if( codePoint >= 0x1F600 && codePoint <= 0x1F64F || 
						codePoint >= 0x2700 && codePoint <= 0x27BF ||
						codePoint >= 0x1F680 && codePoint <= 0x1F6C0 || 
						codePoint >= 0x1F300 && codePoint <= 0x1F5FF) 
					{ // Emoticons
						result += System.Char.ConvertFromUtf32( 0xFFFD );
					} else {
						string strCh = System.Char.ConvertFromUtf32( codePoint );
						int szBefore = result.Length;
						result += strCh;
						if( result.Length > szBefore + 1 ) {
							Debug.LogError("Warning the string increasd more than one character");
						}
					}
				}
				return result;
			}
		}


		void ProcessTextVerts( VertexHelper vh, ref System.Collections.Generic.List<UIVertex> verts ) {
			int chidx = 0;
			float offx = 0f;
			float offy = 0f;

			var arr = Text.AsCodePoints().ToArray();

			for (int it = 0; it < verts.Count-5; it+=6) {
				if (arr == null || arr.Length == 0)
					continue;
				UnityEngine.Color c = verts [it].color;

				var chars = textComponent.cachedTextGenerator.characters;
				if( chars.Count <= chidx ) {
					continue;
				}

				var charInfo = chars[chidx];

				//posoff+=charInfo.charWidth;
				var vp1 = verts[it].position; // top left corner
				var vp2 = verts[it+2].position; // bottom right corner
			
				var wh = (vp2 - vp1);
				wh.x = Mathf.Abs (wh.x);
				wh.y = Mathf.Abs (wh.y);


				if ( chidx >= arr.Length) {
					continue;
				}
				int iCodePoint = arr[chidx];

				int modifier = 0;
				if ( chidx+1 < arr.Length ) {
					var nextCh = arr [chidx + 1];
					if ( EmojiSupport.IsEmojiModifier( nextCh ) ) {
						//Debug.Log ("there is a modifier next");
						modifier = nextCh;
					}
				}

				int variationSelector = 0;
				if( chidx+1 < arr.Length ) {
					var nextCh = arr[chidx+1];
					if( EmojiSupport.IsVariationSelector(nextCh) ) {
						variationSelector = nextCh;
					}
				}

				string sCodePoint = System.Char.ConvertFromUtf32( iCodePoint );
				bool isEmojiCh = IsEmojiChar (iCodePoint);


				bool hideThisChar = false;
				if( IsEmojiModifier(iCodePoint) || IsVariationSelector(iCodePoint) ) {
					hideThisChar = true;
					isEmojiCh = false;
				}
					

				float localStretchX = 0f;
				if( chidx == 2 || chidx == 4 ) {
					//c.a = 0f;
					//c = Color.green;
					//localStretchX = 20f;
				}
					
				var uv1s = new Vector2[] {
					// Top Right Triangle
					new Vector2(0,1), // top left
					new Vector2(1,1), // top right
					new Vector2(1,0), // bottom right
					// Bottome Left Triangle
					new Vector2(1,0), // bottom right
					new Vector2(0,0), // bottom left
					new Vector2(0,1), // top left
				};

				//	Debug.LogError ( "charInfo.cursorPos = " + charInfo.cursorPos );
				var vp_x = ( Mathf.Max(vp1.x,vp2.x) - Mathf.Min(vp1.x,vp2.x) ); 
				var vp_y = ( Mathf.Max(vp1.y,vp2.y) - Mathf.Min(vp1.y,vp2.y) ); 
				var lp = new Vector2( vp1.x + (vp_x/2f), vp1.y - (vp_y/2f) );
				var sz = new Vector2( vp_x, vp_y );
				float charWidth = sz.x;


				Vector2[] offsets = null;

				if (isEmojiCh) {
					//Debug.Log ("isEmojiCh");
					c.a=0f;
					var tex =
						(Texture2D)EmojiResources.RenderGlyph (
							sCodePoint, 
							iCodePoint,
							modifier,
							variationSelector
						);
					var uv = EmojiAssist.EmojiFontCache.GetUVs (sCodePoint, tex);

					//Debug.Log ("Char #" + chidx + " uv:" + uv);

					float w = Mathf.Max (uv.z, uv.x) - Mathf.Min (uv.z, uv.x);
					float h = Mathf.Max (uv.w, uv.y) - Mathf.Min (uv.w, uv.y);
					h *= 2f;
					w *= 2f;

					uv1s = new Vector2[] {
						// Top Right Triangle
						new Vector2 ( uv.x, 1-uv.y ), // top left
						new Vector2 ( uv.z, 1-uv.y ), // top right
						new Vector2 ( uv.z, 1-uv.w ), // bottom right
						// Bottome Left Triangle
						new Vector2 ( uv.z, 1-uv.w ), // bottom right
						new Vector2 ( uv.x, 1-uv.w ), // bottom left
						new Vector2 ( uv.x, 1-uv.y ), // top left
					};


					//Debug.Log ("wh = " + wh);
					var o = Vector2.zero;
					if (wh.y > wh.x)
						o.y = wh.y - wh.x;

					o /= 2f;


					//Debug.Log ("o = " + o);


					offsets = new Vector2[] {
						// Top Right Triangle
						new Vector2(0+(o.x*-1), 0+(o.y*-1)), // top left
						new Vector2(0+o.x, 0+(o.y*-1)), // top right
						new Vector2(0+o.x, 0+(o.y)), // bottom right
						// Bottome Left Triangle
						new Vector2(0+(o.x   ), 0+(o.y)), // bottom right
						new Vector2(0+(o.x*-1), 0+(o.y)), // bottom left
						new Vector2(0+(o.x*-1), 0+(o.y*-1)), // top left
					};
						
					//Debug.Log (" uv = " + uv);
					//Debug.Log (" offsets = " + o );
				} else {
					if (hideThisChar) {
						localStretchX = -charWidth;
						c.a = 0f;
					}
					//Debug.Log ("!isEmojiCh");
					uv1s = new Vector2[] {
						// Top Right Triangle
						Vector2.zero, // top left
						Vector2.zero, // top right
						Vector2.zero, // bottom right
						// Bottome Left Triangle
						Vector2.zero, // bottom right
						Vector2.zero, // bottom left
						Vector2.zero, // top left
					};
				}

				// 
				for( int j = 0; j < 6; j++) {
					var v = verts [it + j];
					v.uv1 = uv1s [j];

					v.color = c; // Top left.
					v.position.x += offx;
					if (j == 1 || j == 2 || j == 3 ) {
						v.position.x += localStretchX;
					}
					if (offsets != null) {
						v.position.x += offsets [j].x;
						v.position.y += offsets [j].y;
					}
					verts[it+j] = v;
					//vh.SetUIVertex (vert);
				}
				offx += localStretchX;


				chidx++;
			}

		}


		public override void ModifyMesh( VertexHelper vh ) {
			if (!IsActive())
				return;

			var output = new List<UIVertex>();
			vh.GetUIVertexStream(output);


			ProcessTextVerts( vh, ref output);
			//ApplyShadow(output, effectColor, 0, output.Count, 1, 1);
			vh.Clear();
			vh.AddUIVertexTriangleStream(output);

			//ListPool<UIVertex>.Release(output);
		}


    }
}
