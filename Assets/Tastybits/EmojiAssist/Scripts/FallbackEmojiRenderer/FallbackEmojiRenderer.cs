﻿using UnityEngine;
using System.Collections;


namespace Tastybits.EmojiAssist {
	

	/**
	 * This class is a way of showing fallback emojis.
	 */
	public class FallbackEmojiRenderer : MonoBehaviour {
		static FallbackEmojiRenderer instance;

		static System.Collections.Generic.Dictionary<int, Texture2D> dict = new System.Collections.Generic.Dictionary<int, Texture2D>();


		// download the emoji's as textures ( only in the editor ) 
		public static Texture2D GetEmojiFromUnicodeChar( string str_ch, int codePoint ) {
			 
			Texture2D ret;
			if( !dict.TryGetValue (codePoint, out ret) ) {
				//Debug.Log ("Downloading emoji from web : " + str_ch);

				ret = new Texture2D (32, 32);
				for( int i=0; i<32; i++ ) 
					for( int j=0; j<32; j++ ) 
						ret.SetPixel (i,j,Color.white); 
				
				dict.Add (codePoint, ret);
				 
				// https://noto-website.storage.googleapis.com/emoji/emoji_u1f600.png
				// https://raw.githubusercontent.com/Ranks/emojione/master/assets/png/1f3a4.png
				var tmp = codePoint.ToString ("X").ToLower ();
				string str_url = "https://raw.githubusercontent.com/Ranks/emojione/master/assets/png/" + tmp + ".png";


				var www = new WWW( str_url );
				//Debug.Log ("Downloading : " + str_url);
				System.Action act = null;
				act = () => {
					if( !string.IsNullOrEmpty(www.error) ) {
						Debug.LogError("Fallback Renderer: Error downloading from url : " +www.url + " ... retrying" );

						System.Action wait = null;
						int frames = 0;
						wait = ()=>{
							if( frames++ < 30*5 ) {
								return;
							}
							OnProcess -= wait;
							Debug.LogError("Fallback Renderer: Retry : " + www.url );
							GetEmojiFromUnicodeChar( str_ch, codePoint ); 
						};
						OnProcess += wait;

						OnProcess -= act;
						return;
					}
					if( www.isDone ) {
						//Debug.Log ("Downloading done : " + str_url);
						EmojiFontCache.QueueRebuild();
						ret.LoadImage(www.bytes);
						ret.Apply();
						OnProcess -= act;
					}
				};
				SubscribeUpdate( act );

			}

			return ret;
		}
		 

		static void SubscribeUpdate( System.Action onProcessDeleg, bool unsubscribe=false ) {
			OnProcess += onProcessDeleg;
			#if UNITY_EDITOR
				if( !unsubscribe ) {
					UnityEditor.EditorApplication.update += Process;
				} else {
					UnityEditor.EditorApplication.update -= Process;
				}
			#else 
				if( instance == null ) {
					var go = new GameObject("FallbackRenderer");
					instance = go.AddComponent<FallbackEmojiRenderer>();
				} 
				if( !unsubscribe ) {
					instance.OnUpdate += Process;
				} else {
					instance.OnUpdate -= Process;
				}
			#endif 
		}


		System.Action OnUpdate;
		void Update() {
			if( OnUpdate!=null ) {
				OnUpdate.Invoke ();
			}
		}


		static System.Action OnProcess;
		static void Process() {
			if( OnProcess != null ) {
				OnProcess.Invoke ();
			}
		}


	}

}
