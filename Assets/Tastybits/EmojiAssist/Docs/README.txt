============================================================
 Emoji Assist
============================================================

Emoji Assist is a plugin for unity that allows you to render Emojis within Unity's build in UI ( UnityEngine.UI ) or just use Emoji's as textures.



============================================================
 UI.TextWithEmojiSupport
============================================================

EmojiAssist implements a Compoent called TextWithEmoji it is a drop-in replacement for UI.Text. It has the same behavior as UI.Text and since it derives for it
it can be easily integrated with existing Unity UI code. UI.TextWithEmoji support also allows you to interpret emoji shortcodes as well as UTF8 represented emoji text coming from a 
webserver or the device keyboard when running on mobile. 


============================================================
 How to use.
============================================================

1) TextDemo - Simple setup. ( Assets/Tastybits/EmojiAssist/Scenes/DemoText )
	1.1. In order to use this you simple Replace a UI.Text you simply add a UI.TextWithEmoji instead of UI.Text
	1.2. try writing “hello:smiley:” into the text value of the UI.TextWithEmoji in the Inspector. This will show a smiley in the editor.
		 Take note that per default EmojiAssist uses emojione in the editor to render emoticons. On iPhone it will use the Apple emoji font. 
	1.3  In order to use emojione emoticons on the Phone. click EmojiResources and turn off the property called useiOSEmojis.








 
 

 