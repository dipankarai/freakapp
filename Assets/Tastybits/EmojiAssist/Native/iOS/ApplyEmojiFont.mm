extern UIViewController *UnityGetGLViewController(); // Root view controller of Unity screen.
extern void UnitySendMessage( const char * className, const char * methodName, const char * param );

void* AppleEmojiFont_GetGlyphForCharImpl( NSString* emojiString, int height, int* nBytesRet ){
    //
    UIFont* emojiFont = [UIFont fontWithName:@"AppleColorEmoji" size:height];
    CGRect emojiFrame = CGRectMake(0, 0, height, height);

    //
    CGSize sz = [emojiString sizeWithFont:emojiFont];
    CGPoint pnt = CGPointZero;
    pnt.x += height-sz.width;
    pnt.y += (height-sz.height) / 2;
    
    // Get the image as bytes...
    UIGraphicsBeginImageContextWithOptions(emojiFrame.size, NO, 0.0f);
    //CGContextRef context = UIGraphicsGetCurrentContext();
    [emojiString drawAtPoint:pnt withFont:emojiFont];
    UIImage *dstImg = UIGraphicsGetImageFromCurrentImageContext();
    NSData* data = UIImagePNGRepresentation(dstImg);
    UIGraphicsEndImageContext();
    
    int len = (int)[data length];
    *nBytesRet = len;
    
    void* dataPtr = calloc( len, sizeof(uint8_t) );
    memcpy(dataPtr, [data bytes], len);
    
    return dataPtr;
}


extern "C" void* AppleEmojiFont_GetGlyphForChar( const char* emoji, int height, int* nBytesRet ) {
    NSString* str = [[NSString alloc] initWithBytes:emoji length:strlen(emoji) encoding:NSUTF8StringEncoding];
    if( [str length] > 2 ) {
        NSLog(@"Warning proveded string contains more than one character");
    }
    return AppleEmojiFont_GetGlyphForCharImpl( str, 26, nBytesRet );
}


