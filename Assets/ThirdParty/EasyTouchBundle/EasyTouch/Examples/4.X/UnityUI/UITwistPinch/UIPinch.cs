﻿using UnityEngine;
using System.Collections;
using HedgehogTeam.EasyTouch;

public class UIPinch : MonoBehaviour {

	public void OnEnable(){
		EasyTouch.On_Pinch += On_Pinch;
	}

	public void OnDisable(){
		EasyTouch.On_Pinch -= On_Pinch;
		gameObject.transform.localScale = new Vector3 (1,1,1);
	}

	private float smoothScale = 0.2f;
	void On_Pinch (Gesture gesture){
	
//		if (gesture.isOverGui){
//			if (gesture.pickedUIElement == gameObject || gesture.pickedUIElement.transform.IsChildOf( transform)){
//				transform.localScale = new Vector3(transform.localScale.x +  gesture.deltaPinch * Time.deltaTime,  transform.localScale.y+gesture.deltaPinch * Time.deltaTime, transform.localScale.z );
//			}
//		}
		if (gesture.isOverGui)
		//if(gesture.touchCount == 2)
		{
			if (gesture.pickedUIElement == gameObject && gesture.pickedUIElement != null)
			{
				Vector3 newScale = transform.localScale;

				newScale.x = Mathf.Clamp (newScale.x, 0.5f, 3);

				transform.localScale = new Vector3(newScale.x +  gesture.deltaPinch * Time.deltaTime * smoothScale,  newScale.x + gesture.deltaPinch *  Time.deltaTime * smoothScale, newScale.x  );


			}
		}
	}
	

}
