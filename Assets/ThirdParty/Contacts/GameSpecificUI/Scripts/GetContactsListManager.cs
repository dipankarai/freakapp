﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;

public class GetContactsListManager : MonoBehaviour {

	public Transform contentPanel;
	public ContactDetailsDisplay contactDetailsDisplay;
	public string failString;
	int currentCount;
	public Text buttonCountText;


	void Start()
	{
		Contacts.LoadContactList( onDone, onLoadFailed );
	}

	void Update(){
		buttonCountText.text = Contacts.ContactsList.Count.ToString();
	}


	public void GetAllContacts()
	{
		DisplayContacts ();
	}
		
	void DisplayContacts()
	{
		currentCount = Contacts.ContactsList.Count;
		for (int i = 0; i < Contacts.ContactsList.Count; i++) 
		{
			ContactDetailsDisplay instance = Instantiate (contactDetailsDisplay) as ContactDetailsDisplay;
			instance.transform.SetParent (contentPanel);
			instance.transform.localScale = Vector3.one;
			Contact c = Contacts.ContactsList[i];
			string text = "Name : " + c.Name;
			instance.nameText.text = text;
			for(int p = 0 ; p < c.Phones.Count ; p++)
			{
				if (p < 1) 
				{
					string text1 = "Number : " + c.Phones [p].Number;
					instance.numberText.text = text1;
				}
			}
			for(int e = 0 ; e < c.Emails.Count ; e++)
			{
				if (e < 1)
				{
					string text2 = "Email  : " + c.Emails [e].Address; 
					instance.otherDetailText.text = text2;
				}

			}
            if( c.GetPhotoTexture != null )
			{
				//GUILayout.Box( new GUIContent(c.PhotoTexture) , GUILayout.Width(size.y), GUILayout.Height(size.y));
                /*Texture2D tex = c.GetPhotoTexture;
				Rect rec = new Rect(0, 0, tex.width, tex.height);
				Sprite spr = Sprite. Create(tex, rec,new Vector2(0,0),1);
				instance.img.sprite = spr;*/

                instance.img.FreakSetTexture(c.GetPhotoTexture);
			}
		}
	}

	void onLoadFailed( string reason )
	{
		failString = reason;
	}

	void onDone()
	{
		failString = null;
	}
}
