﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft.Examples;
using Freak.Chat;
using SendBird;
using System.Runtime.InteropServices;

	//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
	//containing this component as a cell in a TableView
using System.Text;
using System.IO;

public class ContactDetailsDisplay : TableViewCell
{
	public Button contactsButton;
    public Image img;
    public RawImage profileImage;
    public Texture2D imgTex;
    public Text nameText;
    public Text numberText;
    public Text otherDetailText;
	public Button addButton;
	public Toggle OnlineDisplay;
	public string imageUrl;
    public Text unreadMessage;
	public Image unreadMessageObj;
    public Button deleteGroup;
    
    public Text m_rowNumberText;
    public Slider m_cellHeightSlider;
    
    public string phoneNumb;
    
    public int rowNumber { get; set; }
    [System.Serializable]
    public class CellHeightChangedEvent : UnityEvent<int, float> { }
    public CellHeightChangedEvent onCellHeightChanged;
    
    public string rowContent { get; set; }
    public string iD{ get; set; }
    public string userName{ get; set; }
    
    public Toggle selectionToggle;
    
    public ChatChannel chatChannel;
    
    private ServerContactsSelectedForGroups userForGroup;
    
	public Sprite profilePicHollder;
	public Sprite groupProfilePicHolder;

	public Text timerText;
	public Button inviteButton;
//  private FreakChatSerializeClass.FreakMessagingChannel freakMessagingChannel;
    
	public bool isTagFriend;
    private Sprite defaultImage;

    //public GameObject freakPanel;
    public GameObject chatPanel;
    public Image messageTypeImage;
    public Text messageTypeText;

    private string profileAvatarLink;
    public TableView mTableView;

//    void OnEnable ()
//    {
//        
//    }

    /*void Updates() {
        m_rowNumberText.text = "Row " + rowNumber.ToString();
    }*/
    
    public void SliderValueChanged(Slider slider) {
        float value = slider.value;
        Debug.Log ("Value --- >" + value);
        onCellHeightChanged.Invoke(rowNumber, value);
    }
    
    public float height {
        get { return m_cellHeightSlider.value; }
        set { m_cellHeightSlider.value = value; }
    }

    public void IntUser(string userid, string name, Freak.Chat.ChatChannel channel = null)
    {
        iD = userid;
        userName = name;

        if (channel != null)
        {
            chatChannel = null;
            chatChannel = channel;

            m_channelUrl = channel.channelUrl;
            m_isGroup = channel.isGroupChannel;

//            ShowProfileImageURL();
        }
        else
        {
            chatChannel = new ChatChannel();
            chatChannel.otherUserId = userid;
            chatChannel.otherUserName = name;
        }
    }
    
    public void IntUserForGroupChat(string userid, string username, string phonenumber)
    {
        iD = userid;
        userName = username;
        userForGroup = new ServerContactsSelectedForGroups (userid, username, phonenumber);
    }
    
    public void OnClickOpenProfile()
    {
        if (mTableView != null && mTableView.isScrolling)
            return;

        FreakAppManager.Instance.senderCompressedPic = null;
        if (profileImage != null)
        {
            FreakAppManager.Instance.senderCompressedPic = (Texture2D)profileImage.texture;
        }
        else
        {
            FreakAppManager.Instance.senderCompressedPic = img.sprite.texture;
        }

        /* if (FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState == HomeScreenUIPanelController.PanelType.GameSelectOpponent)
        {
            Debug.Log("OPEN OPONENT ID: " + iD + " Name: " + userName);

            FreakAppManager.GameIntegrateData data = new FreakAppManager.GameIntegrateData();
            data.opponent_id = iD;
            data.opponent_name = userName;
            data.opponent_profile_url = profileAvatarLink;
            data.is_single_mode = false;

            FreakAppManager.Instance.currentGameData = data;
            FreakAppManager.Instance.currentGameData.isGameSelected = true;

//            MessageQueueHandler.Instance.SendUserMessage()
            //FreakAppManager.Instance.PlayGame(FreakAppManager.Instance.currentSelectedStoreGame.title, FreakAppManager.Instance.currentSelectedStoreGame.inventory_id);
        }
        else*/
        {
            if (m_isGroup)
            {
                chatChannel = CacheManager.GetChatChannel(m_channelUrl);
                
                ChatManager.Instance.groupUsersList.Clear();
                FreakAppManager.Instance.selectedGroupProfile = chatChannel;
                GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
                //            go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (chatChannel);
            }
            else
            {
                FreakAppManager.Instance.currentGameData = new FreakAppManager.GameIntegrateData();
                FreakAppManager.Instance.currentGameData.opponent_id = chatChannel.otherUserId;
                FreakAppManager.Instance.currentGameData.opponent_name = chatChannel.otherUserName;
                FreakAppManager.Instance.currentGameData.opponent_profile_url = profileAvatarLink;

                Debug.Log("Open User Profile NAME: " + chatChannel.otherUserName + " ID: " + chatChannel.otherUserId);
                FreakAppManager.Instance.myUserProfile.memberId = chatChannel.otherUserId;
                FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
            }
        }

        /*if (chatChannel.isGroupChannel)
        {
            ChatManager.Instance.groupUsersList.Clear();
            FreakAppManager.Instance.selectedGroupProfile = chatChannel;
            GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
//            go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (chatChannel);
        }
        else
        {
            Debug.Log("Open User Profile NAME: " + chatChannel.otherUserName + " ID: " + chatChannel.otherUserId);
			FreakAppManager.Instance.myUserProfile.memberId = chatChannel.otherUserId;
			FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
        }*/
    }

    private void GetNewGroupChannelCallback (SendBird.GroupChannel groupChannel)
    {
        ChatManager.Instance.currentChannel = groupChannel;
    }
    
	#region UnblockUser
	public void OnClickUnBlockUser()
	{
//		ChatManager.Instance.UnblockUser (iD, RefreshBlockList);
//        ChatManager.Instance.UnblockUser (iD, UnblockUserCallback);
        ChatServerAPI.Instance.UnblockUser(ChatManager.Instance.UserId, iD, UnblockUserCallback);
	}

    void UnblockUserCallback (bool blocked)
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("UnblockUserCallback");
        #endif
        if (blocked)
        {
            FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
            blockUserList.RemoveUser(iD);
            FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;
        }
        RefreshBlockList();
    }

	void RefreshBlockList()
	{
		FindObjectOfType<BlockedUsersUIController> ().Init ();
	}
	#endregion

    public void OnClickAdd()
    {
        if (mFreakmember == null && !isTagFriend)
        {
            FindObjectOfType<AddUserApiManager> ().AddUserAPICall (phoneNumb);
        }
        else
        {
            if (!isTagFriend)
            {
                FindObjectOfType<ChatWindowUI>().TagFriendSelected(mFreakmember);
            } 
            else 
            {
                FindObjectOfType<NewsFeedCommentsUIPanel> ().TagFriendSelected (mTagMember);
            }
        }

        /*if (mFreakMember == null && !isTagFriend)
        {
            FindObjectOfType<AddUserApiManager> ().AddUserAPICall (phoneNumb);
        }
        else
        {
			if (!isTagFriend)
			{
				FindObjectOfType<ChatWindowUI> ().TagFriendSelected (mFreakMember);
			} 
			else 
			{
				FindObjectOfType<NewsFeedCommentsUIPanel> ().TagFriendSelected (mTagMember);
			}
        }*/
    }
    
    public void OnClickSend()
    {
        
        var vcf = new StringBuilder();
       // vcf.Append("TITLE:" + nameText.text + System.Environment.NewLine); 
        //vcf.Append("NUMBER:" + phoneNumb + System.Environment.NewLine); 

		vcf.AppendLine("BEGIN:VCARD");
		vcf.AppendLine("VERSION:2.1");
		// Name
		vcf.AppendLine ("N:" + " " + ";" + " ");
		// Full name
		vcf.AppendLine("FN:" + nameText.text + " " + nameText.text);
		// Address
		vcf.Append("ADR;HOME;PREF:;;");
		vcf.Append ("StreetAddress" + ";");
		vcf.Append (" " + ";;");
		vcf.Append(" " + ";");
		vcf.AppendLine (" ");
		// Other data
		vcf.AppendLine("ORG:" + " ");
		vcf.AppendLine("TITLE:" + " ");
		vcf.AppendLine("TEL;HOME;VOICE:" + " ");
		vcf.AppendLine("TEL;CELL;VOICE:" + phoneNumb);
		vcf.AppendLine("URL;" + " ");
        vcf.AppendLine("EMAIL;PREF;INTERNET:" + string.Empty);
		vcf.AppendLine("END:VCARD");
        
        var filename = nameText.text+".vcf";
        var path = Freak.StreamManager.LoadFilePath(filename, ChatFileType.GetFolderLocation(ChatFileType.FileType.Contact), true);

      //  Debug.Log ("file path--->>"+path);
        File.WriteAllText(path, vcf.ToString());
        
        ChatFileType chatFileToSend = new ChatFileType(path, ChatFileType.FileType.Contact);
//        FindObjectOfType<ChatWindowUI> ().DuplicateMessageBox(chatFileToSend);

        FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
        _metaData.messageType = ChatMessage.MetaMessageType.Contact;
        _metaData.contactName = nameText.text;
        _metaData.contactNumber = phoneNumb;
        _metaData.message = nameText.text + " Contact";

        FindObjectOfType<ChatWindowUI> ().DuplicateMessageBox (chatFileToSend, Newtonsoft.Json.JsonConvert.SerializeObject (_metaData));

//        ChatManager.Instance.fileSendCallback = UIController.Instance.PlaySendMessageSound;

        /*ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage ();
        _chatMsg.messageType = ChatMessage.MessageType.FileLink;
        _chatMsg.fileName = System.IO.Path.GetFileName (chatFileToSend.filePath);
        _chatMsg.fileSize = chatFileToSend.fileSize;
        _chatMsg.filePath = chatFileToSend.filePath;
        _chatMsg.fileUrl = chatFileToSend.filePath;*/

//        ChatManager.Instance.currentSelectedChatChannel.messagesList.Add (_chatMsg);
//        ChatManager.Instance.SendFileMessage(chatFileToSend, _chatMsg.customField, Newtonsoft.Json.JsonConvert.SerializeObject(_metaData));
//        ChatManager.Instance.UploadFileToSendBird(chatFileToSend, FileUploadToSBCallback);
        

		if (FreakAppManager.Instance.GetHomeScreenPanel() != null) {
			FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		}
//		else {
//			FindObjectOfType<FreaksPanelUIController> ().PopPanel ();
//		}
    }

    public void OnClickFreaks()
    {
        FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.freakPanel);
    }

//    void FileUploadToSBCallback (SendBird.FileInfoEventArgs args)
//    {
//        if (args.Exception != null)
//        {
//            Debug.Log("FileUpload Exception " + args.Exception.Message);
//            return;
//        }
//        else
//        {
//            Debug.Log("FileUploadEvent "+args.FileInfo.customField);
//            Debug.Log("FileUploadEvent "+args.FileInfo.name);
//            Debug.Log("FileUploadEvent "+args.FileInfo.path);
//            Debug.Log("FileUploadEvent "+args.FileInfo.size);
//            Debug.Log("FileUploadEvent "+args.FileInfo.type);
//            Debug.Log("FileUploadEvent "+args.FileInfo.url);
//            
//            ChatFileType.FileType fileType = (ChatFileType.FileType)int.Parse(args.FileInfo.type);
//            ChatManager.Instance.DownLoadFile(args.FileInfo.url, ChatFileType.GetFolderLocation(fileType), null, true);
//            ChatManager.Instance.SendAnyFile(args.FileInfo.url, args.FileInfo.name, args.FileInfo.type, args.FileInfo.size, fileType.ToString());
//        }
//
//        FindObjectOfType<AttachmentPanelUI> ().contactPanelGameObject.SetActive (false);
//    }

    /*private FreakChatSerializeClass.FreakMessagingChannel.FreakMember mFreakMember;
	public void InitTagSearchMember (FreakChatSerializeClass.FreakMessagingChannel.FreakMember member)
    {
        mFreakMember = member;
        nameText.text = mFreakMember.name;
    }*/

    private ChatUser mFreakmember;
    public void InitTagSearchMember (ChatUser member)
    {
        mFreakmember = member;
        nameText.text = mFreakmember.nickname;
    }

	private ServerUserContact mTagMember;
	public void InitTagMember (ServerUserContact member)
	{
		isTagFriend = true;
		mTagMember = member;
		nameText.text = mTagMember.name;
	}

    /*public void InitFreakMessagingChannel (ChatChannel channel)
    {
        chatPanel.SetActive (true);

        #if UNITY_EDITOR && UNITY_DEBUG
//        Debug.Log ("ContactDetailsDisplay " + gameObject.name);
        #endif

        chatChannel = channel;
        numberText.text = string.Empty;

        ShowDeleteGroupButton ();

        if (chatChannel.unreadMessageCount > 0)
        {
            unreadMessageObj.gameObject.SetActive(true);
            unreadMessage.text = chatChannel.unreadMessageCount.ToString();
        }
        else
        {
            unreadMessage.text = string.Empty;
            unreadMessageObj.gameObject.SetActive(false);
        }

        if (chatChannel.isGroupChannel)
        {
			nameText.text =WWW.UnEscapeURL(chatChannel.channelName);
            OnlineDisplay.gameObject.SetActive(false);
        }
        else
        {
            nameText.text = chatChannel.otherUserName;

            if (OnlineDisplay != null)
            {
                OnlineDisplay.gameObject.SetActive(true);
                if (chatChannel.GetSender() != null)
                {
                    if (chatChannel.GetSender().connectionStatus == ChatUser.UserConnectionStatus.Online)
                    {
                        OnlineDisplay.isOn = true;
                    }
                    else
                    {
                        OnlineDisplay.isOn = false;
                    }
                }
                else
                {
                    OnlineDisplay.isOn = false;
                }
            }
        }

        string timeSend = FreakChatUtility.GetTime(chatChannel.createdAt);
		if (chatChannel.lastMessage != null)
        {
			timeSend = FreakChatUtility.GetTime(chatChannel.lastMessage.CreatedAt);

            if (chatChannel.lastMessage.chatMessageType == ChatBaseMessage.ChatMessageType.FileMessage)
            {
                if (chatChannel.isGroupChannel)
                    MinMessage(chatChannel.lastMessage.Sender.nickname, ChatFileType.ConvertChatFileTypeFromString(chatChannel.lastMessage.fileType));
                else
                    MinMessage(string.Empty, ChatFileType.ConvertChatFileTypeFromString(chatChannel.lastMessage.fileType));
            }
            else if (chatChannel.lastMessage.chatMessageType == ChatBaseMessage.ChatMessageType.UserMessage)
            {
                if (string.IsNullOrEmpty(chatChannel.lastMessage.Data))
                {
                    if (chatChannel.isGroupChannel)
                        MinMessage(chatChannel.lastMessage.Sender.nickname + " : " + chatChannel.lastMessage.Message);
                    else
                        MinMessage(chatChannel.lastMessage.Message);
                }
                else
                {
                    string _data = chatChannel.lastMessage.Data;
                    
                    if (_data.Contains("messageType")  && _data.Contains("fileType"))
                    {
                        if (chatChannel.lastMessage.metaData != null)
                        {
                            if (chatChannel.lastMessage.metaData.messageType == ChatMessage.MetaMessageType.SystemNewGroup)
                            {
                                MinMessage(chatChannel.lastMessage.metaData.message);
                            }
                            else if (chatChannel.lastMessage.metaData.fileType != ChatFileType.FileType.None)
                            {
                                if (chatChannel.isGroupChannel)
                                    MinMessage(chatChannel.lastMessage.Sender.nickname, chatChannel.lastMessage.metaData.fileType);
                                else
                                    MinMessage(string.Empty, chatChannel.lastMessage.metaData.fileType);
                            }
                            else
                            {
                                MinMessage(chatChannel.lastMessage.metaData.messageType.ToString());
                            }
                        }
                    }
                    else
                    {
                        MinMessage(chatChannel.lastMessage.Message);
                    }
                }
            }
        }
        else
        {
            MinMessage("Message " + nameText.text);
        }

		timerText.text = timeSend;

        ShowProfileImage();
        ChangeColourIfUnread(chatChannel.unreadMessageCount > 0);
    }*/

    public void InitFreakMessagingChannelNew (ChatChannel channelDetails)
    {
        m_channelUrl = channelDetails.channelUrl;
        m_isGroup = channelDetails.isGroupChannel;
//        m_imageTexture = channelDetails.allChatChannelDetails.imageTexture;
        chatChannel = channelDetails;

//        chatPanel.SetActive (true);

        #if UNITY_EDITOR && UNITY_DEBUG
        //        Debug.Log ("ContactDetailsDisplay " + gameObject.name);
        #endif

        numberText.text = string.Empty;

        unreadMessage.text = chatChannel.allChatChannelDetails.unReadMsg;
        unreadMessageObj.gameObject.SetActive(string.IsNullOrEmpty(chatChannel.allChatChannelDetails.unReadMsg) == false);
        nameText.text = channelDetails.allChatChannelDetails.displayName;
        timerText.text = channelDetails.allChatChannelDetails.MessageSendTime;
        numberText.text = channelDetails.allChatChannelDetails.MessageToDisplay;

        //ShowDeleteGroupButton ();
//        Invoke("DisplayDetails", 0.8f);  
        DisplayDetails();
    }

    void DisplayDetails ()
    {
        OnlineDisplay.isOn = (chatChannel.allChatChannelDetails.status == ChatUser.UserConnectionStatus.Online);
        OnlineDisplay.gameObject.SetActive(chatChannel.isGroupChannel == false);

        if (chatChannel.allChatChannelDetails.FileType == ChatFileType.FileType.None)
        {
            messageTypeImage.gameObject.SetActive(false);
        }
        else
        {
            numberText.text = string.Empty;
            messageTypeImage.sprite = UIController.Instance.messageSpriteType[(int)chatChannel.allChatChannelDetails.FileType];
            messageTypeImage.gameObject.SetActive(true);
            messageTypeText.text = chatChannel.allChatChannelDetails.MessageToDisplay;
        }
        //MinMessage(chatChannel.allChatChannelDetails.MessageToDisplay, chatChannel.allChatChannelDetails.FileType);


                ShowProfileImageURL();
        //        Invoke("ShowProfileImageURL", 0.5f);
        ChangeColourIfUnread(chatChannel.unreadMessageCount > 0);
    }

    //EDITOR ONLY
    void ShowDeleteGroupButton()
    {
        #if UNITY_EDITOR
        deleteGroup.gameObject.SetActive(false);
        if (m_isGroup)
        {
            deleteGroup.onClick.AddListener( () => {
                ChatManager.Instance.DeleteChannel(m_channelUrl, DeleteGroupCallback);

            });
            deleteGroup.gameObject.SetActive(true);
        }
        #else
        deleteGroup.gameObject.SetActive(false);
        #endif
    }

    public void InitFreakPanel ()
    {
        //freakPanel.SetActive (true);
        chatPanel.SetActive (false);
    }

    public void DeleteGroupCallback (bool success)
    {
        if (success)
        {
            //ChatManager.Instance.RefreshForNewMessages();
            ChatManager.Instance.OnChannelDeleted (m_channelUrl);
        }
    }

    private string displayMessage;
    private ChatFileType.FileType displayFileType;
    void MinMessage (string msg, ChatFileType.FileType fileType = ChatFileType.FileType.None)
    {
        messageTypeImage.gameObject.SetActive(false);
        displayMessage = msg;
        displayFileType = fileType;

        if (fileType == ChatFileType.FileType.None)
        {
            if(msg.Length > 50)
            {
                numberText.text = msg.Substring(0,49);
                numberText.text += "......";
            }
            else
            {
                numberText.text = msg;
            }
        }
        else
        {
            numberText.text = string.Empty;
            messageTypeImage.sprite = UIController.Instance.messageSpriteType [(int)fileType];//spriteType[(int)fileType];
            if (string.IsNullOrEmpty(msg))
                messageTypeText.text = ": " + fileType.ToString();
            else
                messageTypeText.text = msg + " : " + fileType.ToString();
            messageTypeImage.gameObject.SetActive(true);
        }
    }
    
    public void OnClickOpenChat ()
    {
        if (mTableView != null && mTableView.isScrolling)
            return;

        GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);

        #if UNITY_EDITOR && UNITY_DEBUG
//        Debug.Log ("TIME.TIME:: " + Time.time);
//        Debug.Log ("Chat User NAME ---- >"+ chatChannel.otherUserName);
        #endif

        Texture2D texture = new Texture2D(1, 1);
        texture = (Texture2D)profileImage.texture;
        FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = texture;
        texture = null;

        if (string.IsNullOrEmpty (m_channelUrl))
        {
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel);
        }
        else
        {
            chatChannel = CacheManager.GetChatChannel(m_channelUrl);

            if (chatChannel != null)
            {
                go.GetComponent<ChatWindowUI> ().InitChat (chatChannel, true);
                if (chatChannel.groupChannel == null)
                {
                    ChatManager.Instance.GetChannelByChannelUrl (chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
                }
                else
                {
                    go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(chatChannel.groupChannel);
                }
            }
        }

        /*if (string.IsNullOrEmpty (chatChannel.channelUrl))
        {
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel);
        }
        else
        {
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel, true);
            if (chatChannel.groupChannel == null)
            {
                ChatManager.Instance.GetChannelByChannelUrl (chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
            }
            else
            {
                go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(chatChannel.groupChannel);
            }
        }*/
    }

    private void GetGroupChannelCallback (SendBird.GroupChannel groupChannel)
    {
//        Debug.Log ("GetGroupChannelCallback " + groupChannel.Name);
        ChatManager.Instance.currentChannel = groupChannel;

        /*GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("TIME.TIME:: " + Time.time);
        Debug.Log ("Chat User NAME ---- >"+ chatChannel.otherUserName);
        #endif
        go.GetComponent<ChatWindowUI> ().InitChat (chatChannel);

        ChatManager.Instance.mConnectingUI.gameObject.SetActive (false);*/
    }

	public void OnClickServerContactToRefer ()
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Server Contacts selected for Referrak ---- >"+ iD);
        #endif
		SendChallenge (FreakAppManager.Instance.myUserProfile.gameReferralCode, int.Parse(iD), "You have a Game Referral to Accept.");
	}

    private string mReferralId;
    private string mMessage;
    private int mUserId;

	public void SendChallenge(string referralId, int to_userID, string message)
	{
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("gameId--->>"+referralId+"<<---to_userID-->>"+to_userID+"<<<---Message--->>"+message);
        #endif
        mReferralId = referralId;
        mMessage = message;
        mUserId = to_userID;

        ChatManager.Instance.CreateChannelForGame(to_userID.ToString(), HandleActionCallback);
	}

    void HandleActionCallback ()
    {
        /*string filename = System.IO.Path.GetFileNameWithoutExtension("AcceptOrRejectChallenge");
        FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
        _metaData.messageType = ChatMessage.MetaMessageType.InAppFile;
        _metaData.fileType = ChatFileType.FileType.Challenge;
        _metaData.filePath = "Challenge/" + filename;
        _metaData.message = mReferralId;

        ChatManager.Instance.SendUserMessage (ChatManager.Instance.currentChannel, mMessage, Newtonsoft.Json.JsonConvert.SerializeObject (_metaData));*/

        FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
        _metaData.messageType = ChatMessage.MetaMessageType.GameInvite;
        _metaData.gameType = FreakChatSerializeClass.NormalMessagerMetaData.GameType.Challenge;
        _metaData.gameData = "Challenge/AcceptOrRejectChallenge";
        _metaData.referralId = mReferralId;
        _metaData.message = mMessage;
        _metaData.userid = mUserId.ToString();

        string data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);

        ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage(data);
        _chatMsg.messageType = ChatMessage.MessageType.Message;
        _chatMsg.message = mMessage;
        _chatMsg.data = data;

        MessageQueueHandler.Instance.SendUserMessage(ChatManager.Instance.currentChannel, ChatManager.Instance.currentChannel.Url, _chatMsg.message, _chatMsg.data, _chatMsg.customField, null);
    }

	internal long mMaxMessageTimestamp = long.MinValue;
	public long GetMaxMessageTimestamp()
	{
		return mMaxMessageTimestamp == long.MinValue ? long.MaxValue : mMaxMessageTimestamp;
	}


    public void OnClickServerContact()
    {
        if (selectionToggle.isOn)
        {
            if (!ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
            {
                ChatManager.Instance.groupUsersList.Add (userForGroup);
            }
        }
        else {
            if (ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
            {
                ChatManager.Instance.groupUsersList.Remove (userForGroup);
            }
        }

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Server Contacts selected for group ---- >"+  ChatManager.Instance.groupUsersList.Count);
        #endif
    }
    
    public void OnClickSelectContactToTag()
    {
        Debug.Log ("Contact Selected --- >" + nameText.text);
    }

    private string m_channelUrl;
    private bool m_isGroup;
//    private Texture2D m_imageTexture;
    public void ShowProfileImageURL ()
    {
//        if (m_imageTexture == null)
//        {
//            m_imageTexture = ChatManager.Instance.GetChannelTexture(m_channelUrl);
//        }
        Freak.FolderLocation _location = Freak.FolderLocation.Profile;
        if (chatChannel.isGroupChannel)
        {
            _location = Freak.FolderLocation.GroupProfile;
        }

        if(chatChannel.allChatChannelDetails == null || string.IsNullOrEmpty(chatChannel.allChatChannelDetails.imageUrl))
        {
            if (chatChannel.isGroupChannel)
            {
                profileAvatarLink = chatChannel.coverUrl;
            }
            else
            {
                profileAvatarLink = chatChannel.GetSender().profileUrl; 
            }
        }
        else
        {
            profileAvatarLink = chatChannel.allChatChannelDetails.imageUrl;
        }

        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(profileAvatarLink, _location, mServerUserContact);
                
        if (texture != null)
        {
            if (profileImage != null)
            {
                profileImage.texture = texture as Texture;
                profileImage.PreserveAspectToParent();
            }
            else
            {
                img.FreakSetSprite(texture);
            }
        }
        else
        {
            if (profileImage != null)
            {
                if (m_isGroup)
                {
                    profileImage.texture = groupProfilePicHolder.texture as Texture;
                }
                else
                {
                    profileImage.texture = profilePicHollder.texture as Texture;
                }
                profileImage.PreserveAspectToParent();
            }
            else
            {
                if (m_isGroup)
                {
                    img.sprite = groupProfilePicHolder;
                }
                else
                {
                    img.sprite = profilePicHollder;
                }
            }

            Invoke("ShowProfileImageURL", 0.5f);
        }
    }

    /* void InvokeShowProfileImageUrl ()
    {
        ShowProfileImageURL();
    }*/

    private string profileImageFilePath;
    /*public void ShowProfileImage ()
    {
//        mServerUserContact = null;
        if (chatChannel == null)
            return;


        if (chatChannel.isGroupChannel)
        {
            if (chatChannel.textureCache != null)
            {
                if (profileImage != null)
                {
                    profileImage.texture = chatChannel.textureCache;
                    profileImage.PreserveAspectToParent();
                }
                else
                {
                    img.FreakSetSprite(chatChannel.textureCache);
                }
            }
            else
            {
                if (profileImage != null)
                {
                    profileImage.texture = groupProfilePicHolder.texture as Texture;
                    profileImage.PreserveAspectToParent();
                }
                else
                {
                    img.sprite = groupProfilePicHolder;
                }
                Freak.DownloadManager.Instance.DownloadProfile(chatChannel.coverUrl, Freak.FolderLocation.GroupProfile, chatChannel);
                Invoke("ShowProfileImage", 0.1f);
            }
//            profileImageFilePath = Freak.StreamManager.LoadFilePath(Path.GetFileName(chatChannel.coverUrl), Freak.FolderLocation.GroupProfile, false);
//            chatChannel.textureCache = null;
        }
        else
        {
            if (chatChannel.GetSender() == null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log(chatChannel.channelName);
                #endif
                return;
            }

            if (chatChannel.GetSender().texture != null)
            {
                if (profileImage != null)
                {
                    profileImage.texture = chatChannel.GetSender().texture;
                    profileImage.PreserveAspectToParent();
                }
                else
                {
                    img.FreakSetSprite(chatChannel.GetSender().texture);
                }
            }
            else
            {
                if (profileImage != null)
                {
                    profileImage.texture = profilePicHollder.texture as Texture;
                    profileImage.PreserveAspectToParent();
                }
                else
                {
                    img.sprite = profilePicHollder;
                }
                Freak.DownloadManager.Instance.DownloadProfile(chatChannel.GetSender().profileUrl, Freak.FolderLocation.Profile, chatChannel);
                Invoke("ShowProfileImage", 0.1f);
            }

//            chatChannel.GetSender().texture = null;
//            profileImageFilePath = Freak.StreamManager.LoadFilePath(Path.GetFileName(ChatManager.GetSender(chatChannel.channelMembers).profileUrl), Freak.FolderLocation.Profile, false);
        }

//        if (File.Exists(profileImageFilePath))
//        {
//            FileSavedCallback(true);
//        }
//        else
//        {
//            if (chatChannel.isGroupChannel)
//            {
//                Freak.DownloadManager.Instance.DownLoadFile (chatChannel.coverUrl, Freak.FolderLocation.GroupProfile, FileSavedCallback, chatChannel.channelUrl);
//            }
//            else
//            {
//                Freak.DownloadManager.Instance.DownLoadFile (ChatManager.GetSender (chatChannel.channelMembers).profileUrl, Freak.FolderLocation.Profile, FileSavedCallback, chatChannel.channelUrl);
//            }
//        }
    }*/

    private ServerUserContact mServerUserContact;
    public void ShowProfileImage (ServerUserContact serverUserContact)
    {
        mServerUserContact = serverUserContact;
//        profileImageFilePath = Freak.StreamManager.LoadFilePath (System.IO.Path.GetFileName (mServerUserContact.avatar), Freak.FolderLocation.Profile, false);

//        ShowProfileImage();
        Invoke("ShowProfileImage", 0.5f);
    }

    void ShowProfileImage ()
    {
        profileAvatarLink = mServerUserContact.avatar;
        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);

        if (texture != null)
        {
            if (profileImage != null)
            {
                profileImage.texture = texture as Texture;
                profileImage.PreserveAspectToParent();
            }
            else
            {
                img.FreakSetSprite(texture);
            }
        }
        else
        {
//            Freak.DownloadManager.Instance.DownloadProfile(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);
            if (profileImage != null)
            {
                profileImage.texture = profilePicHollder.texture as Texture;
                profileImage.PreserveAspectToParent();
            }
            else
            {
                img.sprite = profilePicHollder;
            }

            Invoke("ShowProfileImage", 0.5f);
        }
    }

    void FileSavedCallback (bool success)
    {
        if (System.IO.File.Exists(profileImageFilePath))
        {
            if (mServerUserContact == null)
            {
                if (chatChannel != null && chatChannel.textureCache != null)//chatChannel.imageCache != null)
                {
//                    img.FreakSetTexture(chatChannel.imageCache);
                    profileImage.texture = chatChannel.textureCache;
                    profileImage.PreserveAspectToParent();
                    return;
                }
            }
            else
            {
               
//                if (mServerUserContact.imageCache != null)
                if (mServerUserContact.userImage != null)
                {
//                    img.FreakSetTexture(mServerUserContact.imageCache);
                    profileImage.texture = mServerUserContact.userImage;
                    profileImage.PreserveAspectToParent();
                    return;
                }
            }

//            chatChannel.imageCache = null;
            Freak.DownloadManager.Instance.LoadImage(profileImageFilePath, FileSavedCallback, chatChannel, mServerUserContact);

            /*WWW www = new WWW("file://" + profileImageFilePath);
            Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
            img.sprite = null;
            img.sprite = Sprite.Create (www.texture, rect, new Vector2 (0, 0), 1f);
            www = null;*/

            /*Texture2D newImage = new Texture2D(4,4);
            newImage.LoadImage(Freak.StreamManager.LoadFileDirect (profileImageFilePath));
            img.FreakSetSprite (TextureResize.ResizeTexture (img.rectTransform, newImage));
            newImage = null;*/

            /*if (chatChannel != null)
            {
                chatChannel.imageCache = img.sprite.texture.EncodeToPNG();
            }

            if (mServerUserContact != null)
            {
                mServerUserContact.imageCache = img.sprite.texture.EncodeToPNG();
            }*/

            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log (this.gameObject.activeInHierarchy + " FileSave " + gameObject.name);
            #endif
        }
    }

    void LoadImageCallback (string userid, Sprite imageSprite)
    {
        if (mServerUserContact == null)
        {
            if (chatChannel != null && chatChannel.imageCache != null && chatChannel.otherUserId == userid)
            {
                img.sprite = imageSprite;
            }
        }
        else
        {
            if (mServerUserContact.imageCache != null && userid == mServerUserContact.user_id.ToString())
            {
                img.sprite = imageSprite;
            }
        }
    }

    /*void LoadImage()
    {
		if (this.gameObject != null)
		{
			if (this.gameObject.activeInHierarchy) 
			{
				StartCoroutine (LoadImageFromLocal (profileImageFilePath));
			} 
			else 
			{
				Invoke ("LoadImage", 0.5f);
			}
		}
    }*/


    /*IEnumerator LoadImageFromLocal (string path)
    {
        var www = new WWW("file://" + path);
        yield return www;
        while (!www.isDone)
        {
            yield return null;
        }

        if (string.IsNullOrEmpty(www.error))
        {
            Texture2D resizedTexture = TextureResize.ResizeTexture (img.rectTransform, www.texture);
            img.FreakSetSprite (resizedTexture);

            if (chatChannel != null)
            {
                chatChannel.profileImageCache = img.sprite;
            }
            if (mServerUserContact != null)
            {
                mServerUserContact.userImage = resizedTexture;
            }
        }
        else
        {
            Debug.LogError("url ::: " + path + www.error);
        }

        Resources.UnloadUnusedAssets ();
    }*/

    public void OnMessageTyping(bool isTyping, List<SendBird.User> userIds)
    {
        if (isTyping)
        {
            tempDisplayMessage = displayMessage;
            tempDisplayFileType = displayFileType;

            for (int i = 0; i < userIds.Count; i++)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OnMessageTyping " + isTyping + "By " + userIds[i].UserId);
                #endif

                typingUsername = userIds [i].Nickname;
                DisplayIsTyping ();
            }
        }
        else
        {
            if (typingRoutine != null)
                StopCoroutine (typingRoutine);

            CancelInvoke ("DisplayIsTyping");
            this.isTyping = false;
            MinMessage(tempDisplayMessage, tempDisplayFileType);
            Debug.Log("ISTYPING........FALSE " + displayMessage);
            tempDisplayMessage = "";
            tempDisplayFileType = ChatFileType.FileType.None;
        }
    }

    private string typingUsername;
    private bool isTyping;
    private Coroutine typingRoutine;
    private string tempDisplayMessage;
    private ChatFileType.FileType tempDisplayFileType;
    void DisplayIsTyping ()
    {
        if (isTyping) {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("_KOSA_+_SAS");
            #endif
            Invoke ("DisplayIsTyping", 1f);
            return;
        }

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("Started "+isTyping);
        #endif
        isTyping = true;

        if (typingRoutine != null)
            StopCoroutine (typingRoutine);

        typingRoutine = StartCoroutine (IsTyping (typingUsername));
    }

    IEnumerator IsTyping(string username)
    {
        string _username = "typing";
        if (ChatManager.IsSingleChannel (chatChannel.channelName) == false)
        {
            _username = username + " is typing";
        }

        MinMessage (string.Empty);
        MinMessage (_username + " .");
        yield return new WaitForSeconds (0.5f);
        MinMessage (_username + " . " + ".");
        yield return new WaitForSeconds(0.5f);
        MinMessage (_username + " . " + ". " + ".");
        yield return new WaitForSeconds(0.5f);

        isTyping = false;

        DisplayIsTyping ();
    }
	
    void OnDisable ()
    {
//        CancelInvoke ("LoadImage");
//        CancelInvoke();
        typingUsername = string.Empty;
        isTyping = false;
        typingRoutine = null;
//        ResetDefaultProfilePic();
    }

    void ResetDefaultProfilePic ()
    {
        if (profileImage != null)
        {
            profileImage.texture = profilePicHollder.texture as Texture;
            profileImage.PreserveAspectToParent();
        }
        else
        {
            img.sprite = profilePicHollder;
        }

    }

	public void OnClickInviteUser()
	{
		Debug.Log ("Invite User");
		#if UNITY_ANDROID
		//string shareText = "Hey,\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n";
		string shareText = "Hey,\n\n Check out the new Freak App!\n";
		//AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		//AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		//intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		//AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		//AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + " ");
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		//intentObject.Call<AndroidJavaObject>("setType", "image/png");

		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		//AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		//AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		//AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Freak Invite");
		//currentActivity.Call("startActivity", jChooser);


		AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
		intentObject.Call<AndroidJavaObject> ("setAction", intentClass.GetStatic<string> ("ACTION_SEND"));
		intentObject.Call<AndroidJavaObject> ("setType", "text/plain");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);
		AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
		currentActivity.Call ("startActivity", intentObject);


		#elif UNITY_IOS 
		CallSocialShareAdvanced ("Hey,\n\n\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n","Invite","","");
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif

//		string mobile = phoneNumb;
//		//string url = "sms:"+mobile +"?body=" +"asasdasdasdasda";
//		//Application.OpenURL("sms:" + mobile);
//
//		//string url = "sms:"+ mobile + "&body=/*Join Freaks*/";
//		//Application.OpenURL(url);
//
//		string separator=(Application.platform == RuntimePlatform.IPhonePlayer)?";":"?";
//		Application.OpenURL(string.Format("sms:{0}{1}body={2}", mobile, separator, WWW.EscapeURL("Join Freaks")));

		//string url = string.Format("sms:{0}?body={1}", mobile, "Freaks");
		//Application.OpenURL (url);

	}

		public void OnClickSelectedImage()
		{

		}

		#if UNITY_IOS
		public struct ConfigStruct
		{
		public string title;
		public string message;
		}

		[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

		public struct SocialSharingStruct
		{
		public string text;
		public string url;
		public string image;
		public string subject;
		}

		[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

		public static void CallSocialShare(string title, string message)
		{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
		}

		public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
		{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
		}
		#endif

		public void OnClickFreakMember()
		{
			FreakAppManager.Instance.myUserProfile.memberId = iD;
			FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
		}

	private Color unreadColor = new Color (204f/255f, 60f/255f, 60f/255f);
	private Color readColor = new Color (131f/255f, 131f/255f, 131f/255f);
	void ChangeColourIfUnread(bool isUnread)
	{
		if (isUnread) 
		{
			nameText.color = unreadColor;
			numberText.color = unreadColor;
			timerText.color = unreadColor;
		}
		else{
			nameText.color = readColor;
			numberText.color = readColor;
			timerText.color = readColor;
		}
	}
}
