﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using System.Collections.Generic;


namespace Tacticsoft.Examples
{
    //An example implementation of a class that communicates with a TableView
    public class SimpleTableViewController : MonoBehaviour, ITableViewDataSource
    {
        public VisibleCounterCell m_cellPrefab;
        public TableView m_tableView;

        public int m_numRows;
        private int m_numInstancesCreated = 0;

		private List<Country.CountryCode> tempCountryNames = new List<Country.CountryCode>();
		private List<Country.CountryCode> searchedCountryNames = new List<Country.CountryCode>();

		//public Sprite imgSprite;
		//public Sprite imgSprite1;

        //Register as the TableView's delegate (required) and data source (optional)
        //to receive the calls
        void OnEnable() {
			tempCountryNames.Clear ();
			for (int i = 0; i < Country.GetAllCountry().Count ; i++) 
			{
				tempCountryNames.Add (Country.GetAllCountry() [i]);
			}
			searchedCountryNames = new List<Country.CountryCode>(tempCountryNames);
            m_tableView.dataSource = this;
        }

        public void SendBeer() {
            Application.OpenURL("https://www.paypal.com/cgi-bin/webscr?business=contact@tacticsoft.net&cmd=_xclick&item_name=Beer%20for%20TSTableView&currency_code=USD&amount=5.00");
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView) {
			return searchedCountryNames.Count;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row) {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
            VisibleCounterCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as VisibleCounterCell;
            if (cell == null) {
              	cell = (VisibleCounterCell)GameObject.Instantiate(m_cellPrefab);
                cell.name = "VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
            }
            cell.SetRowNumber(row);
			AssignDataToCell (cell, row);
//			if(row % 2 == 0)
//			{
//				cell.img.sprite = imgSprite;
//			}
//			else{
//				cell.img.sprite = imgSprite1;
//			}
//			cell.contacts.Clear ();
//			for(int i = 0; i < 500; i++)
//			{
//				cell.contacts.Add (i);
//			}

            return cell;
        }
			

		void AssignDataToCell(VisibleCounterCell cell, int rowNumber)
		{
			Country.CountryCode countryDetails = searchedCountryNames [rowNumber];
			cell.m_rowNumberText.text = countryDetails.country_name;
			//cell.m_visibleCountText.text = countryDetails.country_code;
		}

        #endregion

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible) {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible) {
                VisibleCounterCell cell = (VisibleCounterCell)m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        #endregion

    }
}
