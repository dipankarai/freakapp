﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class NativeShare : MonoBehaviour 
{
	public void PickFile()
	{

#if UNITY_IOS
		FilePickIOS();
#else
		Debug.Log("No sharing set up for this platform.");
#endif
	}

#if UNITY_IOS

	[DllImport ("__Internal")] private static extern void saveContactInList(ref ConfigStruct conf);

	public struct ConfigStruct
	{
		public string fname;
		public string phoneNumber;
	}

	[DllImport ("__Internal")] private static extern void pickiOSFile();

	public static void FilePickIOS()
	{
		pickiOSFile ();
	}

//	public static void ContactSave(string fname, string phoneNum)
//	{
//		ConfigStruct conf = new ConfigStruct();
//		conf.fname  = fname;
//		conf.phoneNumber = phoneNum;
//		saveContactInList(ref conf);
//	}
#endif
}
