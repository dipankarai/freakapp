﻿
using UnityEngine;
using System.Collections;
using System;

public class JavaCallbackScript : MonoBehaviour
{
	public static JavaCallbackScript Instance;
    void Awake ()
    {
		if (Instance == null) 
		{
			Instance = this;
		} else {
			Destroy (gameObject);
		}

        DontDestroyOnLoad(this.gameObject);
    }

	void onActivityResult (string resultData)
	{
		Debug.Log ("Call back with data ---------------->>>>>>"+resultData);
     

		Freak.Chat.UIController.Instance.FilePickerCallback(resultData);
	}

	void OnError(string errorMessage){
		Debug.Log ("Error while coming back---------------->>>>>>" + errorMessage);
	}

	void GetIncomingFilePath(string filePath)
	{
		Debug.Log ("Call Back Incoming file----->>>"+filePath);
        PlayerPrefs.SetString("DirectFileSend", filePath);
		FreakAppManager.Instance.isLoadAllChats = true;
	}

    void GetIncomingMessage(string message)
    {
        Debug.Log ("Call Back Incoming Message----->>>"+message);
        PlayerPrefs.SetString("DirectMessageSend", message);
        FreakAppManager.Instance.isLoadAllChats = true;
    }

    void OpenNotificationChatChannel (string channelUrl)
    {
        Debug.Log("OpenNotificationChatChannel----->>> " + channelUrl);

        PlayerPrefs.SetString(FreakAppConstantPara.PlayerPrefsName.OpenNotificationChatChannel, channelUrl);
        Freak.Chat.ChatManager.Instance.OpenChatChannelFromNotification();
    }
}
