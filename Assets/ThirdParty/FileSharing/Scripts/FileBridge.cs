using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class FileBridge
{
#if UNITY_ANDROID
	/// <summary>
	/// Communicate with android native and pick any file
	/// </summary>
	/// <param name="requestCode">Request code.</param>
	public static void PickFile(int requestCode){
		AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("PickFile",requestCode);
	}

	/// <summary>
	/// Communicate with android native and pick video
	/// </summary>
	/// <param name="requestCode">Request code.</param>
	public static void PickVideo(int requestCode){
	AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("PickVideo",requestCode);
	}

	/// <summary>
	/// Communicate with android native and pick audio
	/// </summary>
	/// <param name="requestCode">Request code.</param>
	public static void PickAudio(int requestCode){
	AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("PickAudio",requestCode);
	}

	/// <summary>
	/// Communicate with android native and pick image
	/// </summary>
	/// <param name="requestCode">Request code.</param>
	public static void PickImage(int requestCode){
	AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("PickImage",requestCode);
	}

	/// <summary>
	/// Communicate with android native and pick contact
	/// </summary>
	/// <param name="requestCode">Request code.</param>
	public static void PickContact(int requestCode){
	AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("PickContact",requestCode);
	}

	public static void PickCamera(int requestCode){
		AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
		ajc.CallStatic("CaptureImage",requestCode);
	}

//	public static void CheckIfFileComesFromOtherApp(int requestCode)
//	{
//		AndroidJavaClass ajc = new AndroidJavaClass("com.juego.fileLoad.Bridge");
//		ajc.CallStatic("CheckIfFileComesFromOtherApp",requestCode);
//	}
	
	
#endif 
	
}

