#import "iOSNativeShare.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBookUI/ABUnknownPersonViewController.h>

@implementation iOSNativeShare 
{ }

#ifdef UNITY_4_0 || UNITY_5_0

#import "iPhone_View.h"

#else


- (void)viewDidLoad
{
    [super viewDidLoad];
}

extern UIViewController* UnityGetGLViewController();
#endif

//------

+(id) withTitle:(char*)fname withMessage:(char*)phoneNum
{
    return [[iOSNativeShare alloc] initWithContact:fname withMessage:phoneNum ];
}

-(id) initWithContact :(char*)fname withMessage:(char*)phoneNum   {
    
    self = [super init];
    
    if( !self ) return self;
   
 //   UIViewController *rootViewController = UnityGetGLViewController();
    
    /// activity.unknownPersonViewDelegate = self;

    ///activity.displayedPerson = (ABRecordRef)[self buildContactDetails];
    
    ///activity.allowsAddingToAddressBook = YES;
    
    /// [rootViewController presentViewController:activity animated:YES completion:Nil];

    ///CFRelease((__bridge CFTypeRef)(activity));
    
    ABAddressBookRef addressBook = ABAddressBookCreate(); // create address book record
    ABRecordRef person = ABPersonCreate(); // create a person
    
    NSString *phone =  [NSString stringWithFormat:@"%fname", fname]; // the phone number to add
    NSString *Fname = [NSString stringWithFormat:@"%phoneNum", phoneNum];;
    NSString *Lname ;
    //Phone number is a list of phone number, so create a multivalue
    ABMutableMultiValueRef phoneNumberMultiValue  = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(phone), kABPersonPhoneMobileLabel, NULL);
    
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(Fname) , nil); // first name of the new person
    ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)(Lname), nil); // his last name
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
    ABAddressBookAddRecord(addressBook, person, nil); //add the new person to the record
    
    ABAddressBookSave(addressBook, nil); //save the record
    
    CFRelease(person);

    return self;
}

- (ABRecordRef) buildContactDetails
{
    NSLog(@"building contact details");
    ABRecordRef person = ABPersonCreate();
    CFErrorRef  error = NULL;
    
    // firstname
    ABRecordSetValue(person, kABPersonFirstNameProperty, @"Shakti", NULL);

    //phone
    NSString* phone = @"1234567890";
    
    ABMutableMultiValueRef phoneNum = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNum, (__bridge CFTypeRef)(phone), CFSTR("phone"), NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNum, &error);
    CFRelease(phoneNum);

    
    if (error != NULL)
        NSLog(@"Error: %@", error);
    
    return person;
}

//------

+(id) withText
{
    return [[iOSNativeShare alloc] initWithText ];
    //return [[iOSNativeShare alloc] initWithContact ];
}

-(id) initWithText {
	
	self = [super init];
	
	if( !self ) return self;
    
    NSArray *types = @[(NSString*)kUTTypeImage,
                       (NSString*)kUTTypeSpreadsheet,
                       (NSString*)kUTTypePresentation,
                       (NSString*)kUTTypeDatabase,
                       (NSString*)kUTTypeFolder,
                       (NSString*)kUTTypeZipArchive,
                       (NSString*)kUTTypeVideo,
                       (NSString*)kUTTypeMP3,
                       (NSString*)kUTTypePDF,
                       (NSString*)kUTTypeData,
                       (NSString*)kUTTypeJPEG,
                       (NSString*)kUTTypeRTF,
                       (NSString*)kUTTypeText,
                       (NSString*)kUTTypeMPEG4,
                       (NSString*)kUTTypeVCard,
                       (NSString*)kUTTypePNG
                       ];
        
    //Create a object of document menu view and set the mode to Import
    UIDocumentMenuViewController *activity = [[UIDocumentMenuViewController alloc]initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];

    UIViewController *rootViewController = UnityGetGLViewController();
    
     activity.delegate = self;
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
          [rootViewController presentViewController:activity animated:YES completion:Nil];
    }
    //if iPad
    else
    {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activity];
        [popup presentPopoverFromRect:CGRectMake(rootViewController.view.frame.size.width, rootViewController.view.frame.size.height, 0, 0)inView:rootViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    return self;
}


void ShowAlertMessage (NSString *title, NSString *message){
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                          
                                                    message:message
                          
                                                   delegate:nil
                          
                                          cancelButtonTitle:@"OK"
                          
                                          otherButtonTitles: nil];
    
    [alert show];
    
}

+(id) withTextInvite:(char*)text withURL:(char*)url withImage:(char*)image withSubject:(char*)subject{
    
    return [[iOSNativeShare alloc] initWithTextInvite:text withURL:url withImage:image withSubject:subject];
}

-(id) initWithTextInvite:(char*)text withURL:(char*)url withImage:(char*)image withSubject:(char*)subject{
    
    self = [super init];
    
    if( !self ) return self;
    
    
    
    NSString *mText = text ? [[NSString alloc] initWithUTF8String:text] : nil;
    
    NSString *mUrl = url ? [[NSString alloc] initWithUTF8String:url] : nil;
    
    NSString *mImage = image ? [[NSString alloc] initWithUTF8String:image] : nil;
    
    NSString *mSubject = subject ? [[NSString alloc] initWithUTF8String:subject] : nil;
    
    
    NSMutableArray *items = [NSMutableArray new];
    
    if(mText != NULL && mText.length > 0){
        
        [items addObject:mText];
        
    }
    
    if(mUrl != NULL && mUrl.length > 0){
        
        NSURL *formattedURL = [NSURL URLWithString:mUrl];
        
        [items addObject:formattedURL];
        
    }
    if(mImage != NULL && mImage.length > 0){
        
        if([mImage hasPrefix:@"http"])
        {
            NSURL *urlImage = [NSURL URLWithString:mImage];
            
            NSError *error = nil;
            NSData *dataImage = [NSData dataWithContentsOfURL:urlImage options:0 error:&error];
            
            if (!error) {
                UIImage *imageFromUrl = [UIImage imageWithData:dataImage];
                [items addObject:imageFromUrl];
            } else {
                ShowAlertMessage(@"Error", @"Cannot load image");
            }
        }else{
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            if([fileMgr fileExistsAtPath:mImage]){
                
                NSData *dataImage = [NSData dataWithContentsOfFile:mImage];
                
                UIImage *imageFromUrl = [UIImage imageWithData:dataImage];
                
                [items addObject:imageFromUrl];
            }else{
                ShowAlertMessage(@"Error", @"Cannot find image");
            }
        }
    }
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:Nil];
    
    if(mSubject != NULL) {
        [activity setValue:mSubject forKey:@"subject"];
    } else {
        [activity setValue:@"" forKey:@"subject"];
    }
    
    UIViewController *rootViewController = UnityGetGLViewController();
    [rootViewController presentViewController:activity animated:YES completion:Nil];
    
    return self;
}







- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    
    UIViewController *rootViewController = UnityGetGLViewController();

    documentPicker.delegate = self;
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [rootViewController presentViewController:documentPicker animated:YES completion:Nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:documentPicker];
        [popup presentPopoverFromRect:CGRectMake(rootViewController.view.frame.size.width, rootViewController.view.frame.size.height, 0, 0)inView:rootViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    NSLog(@"picked %@", url);
  //  url.path
    //TODO need to send to unity3d
    NSString *myString = url.path;//absoluteString;
    
    UnitySendMessage("FileSharingManager", "onActivityResult", [myString UTF8String]);
}

/**
 *  Delegate called when user cancel the document picker
 *
 *  @param controller - document picker object
 */
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+(id) invite
{
    return [[iOSNativeShare alloc] initWithInvite ];
    //return [[iOSNativeShare alloc] initWithContact ];
}

-(id) initWithInvite {
    
    self = [super init];
    
    if( !self ) return self;
}

# pragma mark - C API
iOSNativeShare* instance;

//void showAlertMessage(struct ConfigStruct *confStruct) {
//	instance = [iOSNativeShare withTitle:confStruct->title withMessage:confStruct->message];
//}

void pickiOSFile() {
    
    //instance = [iOSNativeShare withContact];
    instance = [iOSNativeShare withText];//:confStruct->text withURL:confStruct->url withImage:confStruct->image withSubject:confStruct->subject];
}

void saveContactInList(struct ConfigStruct *confStruct) {
    instance = [iOSNativeShare withTitle:confStruct->fname withMessage:confStruct->phoneNum];
}

void invitePeople() {
    
    //instance = [iOSNativeShare withContact];
    instance = [iOSNativeShare invite];//:confStruct->text withURL:confStruct->url withImage:confStruct->image withSubject:confStruct->subject];
}

void showSocialSharing(struct SocialSharingStruct *confStruct) {
    instance = [iOSNativeShare withTextInvite:confStruct->text withURL:confStruct->url withImage:confStruct->image withSubject:confStruct->subject];
}

void showAlertMessage(struct ConfigringStruct *confStruct) {
    instance = [iOSNativeShare withTitle:confStruct->title withMessage:confStruct->message];
}

@end
