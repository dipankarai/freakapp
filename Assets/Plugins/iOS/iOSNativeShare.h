#import "UnityAppController.h"
#import <AddressBookUI/ABNewPersonViewController.h>


@interface iOSNativeShare : UIViewController <UIDocumentPickerDelegate,UIDocumentMenuDelegate,ABNewPersonViewControllerDelegate>
{
    UINavigationController *navController;
    ABNewPersonViewController *newPersonController;
   
}

struct ConfigStruct {
    char* fname;
    char* phoneNum;
};

struct ConfigringStruct {
    char* title;
    char* message;
};

struct SocialSharingStruct {
    char* text;
    char* url;
    char* image;
    char* subject;
};


#ifdef __cplusplus
extern "C" {
#endif

    void pickiOSFile();
    void saveContactInList(struct ConfigStruct *confStruct);
    void invitePeople();
    void showSocialSharing(struct SocialSharingStruct *confStruct);
    
    void showAlertMessage(struct ConfigringStruct *confStruct);
#ifdef __cplusplus
}
#endif


@end


