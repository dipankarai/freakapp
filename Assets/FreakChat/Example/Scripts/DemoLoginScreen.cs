﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Freak.Chat
{
    public class DemoLoginScreen : MonoBehaviour
    {
        public InputField userNameField;
        public Button loginButton;
        private string username;

        // Use this for initialization
        void Start () {
            userNameField.onValueChanged.AddListener(OnValueChangedUserName);
            loginButton.onClick.AddListener(OnClickLoginButton);
        }
        
        void OnValueChangedUserName (string text)
        {
            username = text;
            ChatManager.Instance.UserName = text;
        }

        void OnClickLoginButton ()
        {
            username = userNameField.text;
            if (string.IsNullOrEmpty(username))
            {
                return;
            }

            ChatManager.Instance.UserName = username;
        }
    }
}
