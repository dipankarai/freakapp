﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Freak.Chat;

public class ChatPanelDemo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
        loginPanel.gameObject.SetActive(true);
        LoginPanelEvent();

        #if UNITY_EDITOR
        ChatManager.Instance.UserId = this.userID;
        #else
        ChatManager.Instance.UserId = SystemInfo.deviceUniqueIdentifier;
        #endif
	}

    // Update is called once per frame
	void Update () {
	
	}

    #region LOGIN PANEL
    [Header("Login Panel")]
    public RectTransform loginPanel;
    public InputField userNameField;
    public Button loginButton;
    private string username;

    void LoginPanelEvent ()
    {
        userNameField.onValueChanged.AddListener(OnValueChangedUserName);
        loginButton.onClick.AddListener(OnClickLoginButton);
    }

    void OnValueChangedUserName (string text)
    {
        username = text;
        ChatManager.Instance.UserName = text;
    }

    void OnClickLoginButton ()
    {
        if (string.IsNullOrEmpty(username))
        {
            return;
        }

        ChatManager.Instance.LoginSendbirdChat();

        loginPanel.gameObject.SetActive(false);
        contactPanel.gameObject.SetActive(true);
    }

    #endregion

    #region CONTACT PANEL
    [Header("Contact Panel")]
    public RectTransform contactPanel;
    #endregion

    #if UNITY_EDITOR
    public string userID = "1";
    #endif
}
