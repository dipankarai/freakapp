﻿using UnityEngine;
using System.Collections;

public class ShowThumnail : MonoBehaviour {



	public void DownloadImage(string url)
	{   
		StartCoroutine(coDownloadImage(url));
	}

	IEnumerator coDownloadImage(string imageUrl)
	{

		WWW www = new WWW( imageUrl );

		yield return www;

		Texture2D mainTexture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);
//		www.LoadImageIntoTexture(thumbnail.mainTexture as Texture2D);
		www.Dispose();
		www = null;
	}
}
