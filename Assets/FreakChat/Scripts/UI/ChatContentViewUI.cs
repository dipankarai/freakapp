﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using Tacticsoft;

namespace Freak.Chat
{
    public class ChatContentViewUI : MonoBehaviour//, ITableViewDataSource
    {
        [SerializeField] private Text textTest;


        [SerializeField] private Button backButton, settingButton;
        [SerializeField] private ScrollRect chatScrollRect;
//        [SerializeField] private TableView m_tableView;
        private ChatContentUI mChatContentUI;

        private List<ChatChannel> userList;

        /// <summary>
        /// Initializes the Chats.
        /// </summary>
        void Awake ()
        {
            if (InternetConnection.Check())
            {
                ChatManager.Instance.LoginSendbirdChat();
            }
        }

        // Use this for initialization
        void Start () {
            GetUserList();

            backButton.onClick.AddListener(OnClickBackButton);
            settingButton.onClick.AddListener(OnClickSettingButton);
        }
        
        void GetUserList ()
        {
            if (ChatManager.Instance.IsSeandbirdLogined)
            {
//                ChatManager.Instance.GetUserList(InstantChatContent);

                textTest.text = "ID: " + ChatManager.Instance.UserId + " Name: " + ChatManager.Instance.UserName;
            }
            else
            {
                Invoke("GetUserList", 2f);
            }
        }

        void InstantChatContent ()
        {
            Debug.Log(ChatManager.Instance.chatUsersList.Count);
            userList = ChatManager.Instance.chatUsersList;

            for (int i = 0; i < userList.Count; i++)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.ChatContent);
                go.transform.SetParent(chatScrollRect.content.transform, false);
                go.GetComponent<ChatContentUI>().InitChatDetails(userList[i]);
                go.SetActive(true);
            }

//            m_customRowHeights = new Dictionary<int, float>();
//            m_tableView.dataSource = this;
//            mChatContentUI = UIController.Instance.assetsReferences.ChatContent.GetComponent<ChatContentUI>();
        }

        void OnClickBackButton ()
        {
            Debug.Log("OnClickBackButton");
        }

        void OnClickSettingButton ()
        {
            Debug.Log("OnClickSettingButton");
        }
/*
        #region ITableViewDataSource
        private int m_numInstancesCreated = 0;
        private Dictionary<int, float> m_customRowHeights;

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView) {
            return userList.Count;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row) {
            return GetHeightOfRow(row);
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            ChatContentUI _chatContentUI = tableView.GetReusableCell(mChatContentUI.reuseIdentifier) as ChatContentUI;
            if (_chatContentUI == null)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.ChatContent);
                _chatContentUI = go.GetComponent<ChatContentUI>();
                _chatContentUI.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
                _chatContentUI.onCellHeightChanged.AddListener(OnCellHeightChanged);
                go.SetActive(true);
            }

            _chatContentUI.rowNumber = row;

            _chatContentUI.InitChatDetails(userList[row]);

            _chatContentUI.height = GetHeightOfRow(row);
            return _chatContentUI;
        }

        private float GetHeightOfRow(int row) {
            if (m_customRowHeights.ContainsKey(row)) {
                return m_customRowHeights[row];
            } else {
                return mChatContentUI.height;
            }
        }

        private void OnCellHeightChanged(int row, float newHeight) {
            if (GetHeightOfRow(row) == newHeight) {
                return;
            }
            //Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
            m_customRowHeights[row] = newHeight;
            m_tableView.NotifyCellDimensionsChanged(row);
        }
        #endregion
*/
    }
}
