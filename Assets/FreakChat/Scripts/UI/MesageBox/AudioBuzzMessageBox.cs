﻿using UnityEngine;
using System.Collections;

namespace Freak.Chat
{
    public class AudioBuzzMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;
        private AudioSource audioSource;
        void Awake ()
        {
            audioSource = gameObject.GetComponent<AudioSource> ();
        }

    	// Use this for initialization
    	void Start () {
    	
    	}

        void OnDisable ()
        {
            audioSource.clip = null;
        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }
    	
        public override void InitMessageInfo(ChatMessageTableData data)
        {
            messageBoxType = MessageBoxType.Audio;
            base.InitMessageInfo(data);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

            SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

            LoadAudoBuzz ();
        }

        void LoadAudoBuzz ()
        {
            if (mChatMessage.audioClip == null)
            {
                if (System.IO.Path.HasExtension(mChatMessage.filePath))
                    mChatMessage.filePath = System.IO.Path.GetFileNameWithoutExtension(mChatMessage.filePath);

                mChatMessage.audioClip = Resources.Load(mChatMessage.filePath) as AudioClip;

                if (mChatMessage.audioClip == null) {
                    Debug.LogError ("Connot find file in Resources/" + mChatMessage.filePath);
                }
            }

            if (currentSideBox != null && currentSideBox.audioBuzzIconImage != null)
            {
                string _iconPath = "AudioBuzzIcon/" + System.IO.Path.GetFileNameWithoutExtension (mChatMessage.filePath);
                currentSideBox.audioBuzzIconImage.sprite = (Sprite)Resources.Load<Sprite> (_iconPath);
            }

            mChatMessage.messageState = ChatMessage.MessageState.Normal;
            base.CheckMessageState ();
        }

        public void OnClickPlayAudio ()
        {
            if (mChatMessage.audioClip != null)
            {
//                UIController.Instance.GetAudioSource().PlayOneShot(mChatMessage.audioClip);
                audioSource.clip = mChatMessage.audioClip;
                audioSource.Play();
            }
            else
            {
                LoadAudoBuzz ();
            }
        }

        public override void ForAudioBuzzButton()
        {
            if (currentSideBox != null && currentSideBox.audioBuzzPlayButton != null)
            {
                if (mChatMessage.messageState == ChatMessage.MessageState.Normal)
                {
                    currentSideBox.audioBuzzPlayButton.enabled = true;
                    currentSideBox.audioBuzzPlayButton.gameObject.SetActive(true);
                }
                else if (mChatMessage.messageState == ChatMessage.MessageState.Loading)
                {
                    currentSideBox.audioBuzzPlayButton.enabled = false;
                    currentSideBox.audioBuzzPlayButton.gameObject.SetActive(true);
                }
                else
                {
                    currentSideBox.audioBuzzPlayButton.gameObject.SetActive(false);
                }
            }
        }

        public void OnClickOpenAudioBuzz ()
        {
            chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.Audio);
            chatWindowUI.audioPanelUI.OnClickAudioBuzzButton();
        }

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
//            OnClickPlayAudio ();
            OnClickOpenAudioBuzz();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }
    }
}
