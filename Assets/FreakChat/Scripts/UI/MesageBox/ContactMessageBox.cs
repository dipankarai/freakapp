﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using System.Text;
using System.Collections.Generic;
using System.IO;
using ImageAndVideoPicker;
using System;

namespace Freak.Chat
{
    public class ContactMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;

        // Use this for initialization
        void Start () {
            
        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }
        
        public override void InitMessageInfo(ChatMessageTableData message)
        {
            messageBoxType = MessageBoxType.Contact;
            base.InitMessageInfo(message);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }
            
            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.contactFileText.text = string.Empty;
            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

			SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

//            string filename = System.IO.Path.GetFileName(mChatMessage.fileUrl);

            string filePath = LoadFilePath (FolderLocation.Contact);// StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Contact, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            if (System.IO.File.Exists(filePath))
            {
                FileSavedCallback(true);
            }
            else
            {
                base.DownloadFile(FolderLocation.Contact);
            }
        }

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
            base.DownloadFile (FolderLocation.Contact);
        }

        public override void ShowProfileImage(string imageUrl)
        {
            base.ShowProfileImage(imageUrl);
        }

        public override void DownloadProgress (float progress)
        {
            base.DownloadProgress(progress);
        }

//        private string contactFilePath;
        public override void FileSavedCallback (bool success)
        {
//            contactFilePath = StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Contact, isOwner);
            if (mChatMessage.normalMessagerMetaData != null)
            {
                Debug.Log("FileSavedCallback" + mChatMessage.normalMessagerMetaData.contactName);
                currentSideBox.contactFileText.text = mChatMessage.normalMessagerMetaData.contactName;
            }
            else
            {
                Debug.Log("FileSavedCallback Contact Meta Null");
                currentSideBox.contactFileText.text = Path.GetFileNameWithoutExtension (mChatMessage.fileName);
            }

            base.FileSavedCallback(success);
        }

		public void OnOpenFileButtonClicked()
		{

//			var vcf = new StringBuilder();
//			// vcf.Append("TITLE:" + nameText.text + System.Environment.NewLine); 
//			//vcf.Append("NUMBER:" + phoneNumb + System.Environment.NewLine); 
//
//
//			vcf.AppendLine("BEGIN:VCARD");
//			vcf.AppendLine("VERSION:2.1");
//			// Name
//			vcf.AppendLine ("N:" + "TestName" + ";" + "TestingName");
//			// Full name
//			vcf.AppendLine ("FN:" + "Tset name saving" + " " + "Test Name");
//			// Address
//			vcf.Append("ADR;HOME;PREF:;;");
//			vcf.Append ("StreetAddress" + ";");
//			vcf.Append (" " + ";;");
//			vcf.Append(" " + ";");
//			vcf.AppendLine (" ");
//			// Other data
//			vcf.AppendLine("ORG:" + " ");
//			vcf.AppendLine("TITLE:" + " ");
//			vcf.AppendLine("TEL;HOME;VOICE:" + "");
//			vcf.AppendLine("TEL;CELL;VOICE:" + 1234567890);
//			//vcf.AppendLine("URL;" + "");
//			vcf.AppendLine("EMAIL;PREF;INTERNET:" + "");
//			vcf.AppendLine("END:VCARD");
//
//			var filename = "NameTest.vcf";
			//var filename = Application.dataPath + "/" + mChatMessage.filePath;
//			Debug.Log ("file path--->>"+path);
//			File.WriteAllText(path, vcf.ToString());
//
//			string filename = System.IO.Path.GetFileName(mChatMessage.fileUrl);
		

            string filename = System.IO.Path.GetFileName(mChatMessage.fileUrl);
            #if UNITY_EDITOR
            Debug.Log(filename);
            #elif UNITY_ANDROID
            AndroidPicker.ReadVCF (filename);
            #endif

//            string contactFilePath = StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Contact, isOwner);

            #if  UNITY_IPHONE || UNITY_IOS
            if (mChatMessage.normalMessagerMetaData != null)
                IOSFileController.ContactSave(mChatMessage.normalMessagerMetaData.contactName,mChatMessage.normalMessagerMetaData.contactNumber);
			//Load (contactFilePath);
			#endif
		}

		public void OnMessageButtonClicked(){
			Debug.Log ("Message button clicked");
		}
//		#if  UNITY_IPHONE || UNITY_IOS
//		private bool Load(string fileName)
//		{
			// Handle any problems that might arise when reading the text
            /*try
			{
				string line;
				string number = " ";
				string name = " ";
				// Create a new StreamReader, tell it which file to read and what encoding the file
				// was saved as
				StreamReader theReader = new StreamReader(fileName, Encoding.Default);
				// Immediately clean up the reader after this block of code is done.
				// You generally use the "using" statement for potentially memory-intensive objects
				// instead of relying on garbage collection.
				// (Do not confuse this with the using directive for namespace at the 
				// beginning of a class!)
				using (theReader)
				{
					// While there's lines left in the text file, do this:
					do
					{
						line = theReader.ReadLine();

						if (line != null)
						{
							// Do whatever you need to do with the text line, it's a string now
							// In this example, I split it into arguments based on comma
							// deliniators, then send that array to DoStuff()
							string[] entries = line.Split(',');
							if (entries.Length > 0)
							{
								Debug.Log("--------->>>>>>"+line);

								if(line == "TEL;CELL;VOICE:")
								{
									//
									number = line;
									Debug.Log("----number----->>>>>>"+number);
								}
								if(line == "N:")
								{
									//ContactSave(line,line);
									name = line;
									Debug.Log("----name----->>>>>>"+name);
								}
							}
							//DoStuff(entries);
                            IOSFileController.ContactSave(name,number);                     
						}
					}
					while (line != null);
					// Done reading, close the reader and return true to broadcast success    
					theReader.Close();
                    return true;
                }
            }*/

			// If anything broke in the try block, we throw an exception with information
			// on what didn't work
            /*catch (Exception e)
			{
				Debug.Log("--------->>>>>>"+e.Message);
				//Console.WriteLine("{0}\n", e.Message);
				return false;
			}*/
//		}
//		#endif
    
        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            OnOpenFileButtonClicked ();
            //base.OnClickMessageButtonSingle ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }
    }
}
