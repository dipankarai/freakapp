﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using UnityEngine.EventSystems;
using System;

namespace Freak.Chat
{
	public class TextMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;

        private float mainTextContentSize;

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData data)
        {
            messageBoxType = MessageBoxType.Text;
            base.InitMessageInfo(data);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);

            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);
			SetTimerPanelWidth ();
            ShowProfileImage(mChatMessage.messageSender.profileUrl);
            currentSideBox.htmlTextButton.gameObject.SetActive(false);
            currentSideBox.messageText.text = string.Empty;
		//	currentSideBox.messageProText.text = "";

            EnterMessages();
        }

//		public void SetTimerPanelWidths()
//		{
//			if (currentSideBox != null && currentSideBox.timeerPanel != null) {
//				if (currentSideBox.timeText.text.Length > 9) {
//					//currentSideBox.timeerPanel.GetComponent<RectTransform> ().rect.size = new Vector2 (150, 20);
//					currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (146, 33);
//					if (isOwner == false) {
//						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (197, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);//186
//						Debug.Log("Message panel -- >"+ currentSideBox.timeerPanel.transform.position);
//					} else {
//						//currentSideBox.timeerPanel.transform.localPosition = new Vector2 (157, currentSideBox.timeerPanel.transform.localPosition.y);
//						Debug.Log("Message panel RIGHT-- >"+ currentSideBox.timeerPanel.transform.position);
//					}
//
//				} else {
//					//currentSideBox.timeerPanel.GetComponent<RectTransform> ().rect.size = new Vector2 (100, 20);
//					if (isOwner == false) {
//						currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (125, 33);
//						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (188, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);//186
//						Debug.Log("Small Message panel -- >"+ currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition);
//					} else {
//						currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (130, 33);
//						//currentSideBox.timeerPanel.transform.localPosition = new Vector2 (0, 0);
//						//currentSideBox.timeerPanel.transform.localPosition = new Vector2 (162, currentSideBox.timeerPanel.transform.localPosition.y);
//						Debug.Log("Small Message panel RIGHT-- >"+ currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition);
//					}
//				}
//			}
//		}


        void EnterMessages()
        {
            if (this.gameObject.activeInHierarchy == false)
            {
                Invoke("EnterMessages", 0.1f);
                return;
            }

            if(mChatMessage.normalMessagerMetaData == null || mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.None)
            {
				
                currentSideBox.messageText.text = mChatMessage.message;
			//	currentSideBox.messageProText.text = currentSideBox.messageText.text;
				currentSideBox.messageText.text = currentSideBox.messageText.text;

//				Debug.Log ("Chaaaaaat Message ---- >" + currentSideBox.messageText.text);
//				currentSideBox.GetComponent<DisplayEmojiInTextBox>().SendMsg2(mChatMessage.message, MsgType.UserMsg);

                /*this.m_cellHeightSlider.maxValue = GetMaxContentSizeValue(currentSideBox.messageText);
				height = m_cellHeightSlider.maxValue;*/

                chatMessageTableData.cellSize = GetMaxContentSizeValue(currentSideBox.messageText);
            }
            else if (mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.TagFriend)
            {
                TaggedMessage();
            }
        }


		public float GetMaxContentSizeValue(Text textObj)
		{
			float val;
			val = SetTextRext (textObj) + 150;
			return val;
		}

		public float SetTextRext(Text textObj)
		{
			RectTransform textRext = textObj.GetComponent<RectTransform> ();
			string text = textObj.text;
			TextGenerator textGen = new TextGenerator (text.Length);
			Vector2 extents = textRext.rect.size;
			textGen.Populate (text, textObj.GetGenerationSettings (extents));
			float val = textGen.lineCount;
			if (val > 1) {
//				Debug.Log ("Lines Count --- >" + textGen.characterCount + "Lines Count -- >" + val + "Text -- >" + textObj.text);
				float contentHeight = val * 34 + 50;
				Rect newrect = textRext.rect;
				textRext.sizeDelta = new Vector2 (textRext.sizeDelta.x, val * 34);
				return contentHeight;
			}

			return 20;
		}


        void CheckTextMessageSize ()
        {
            /* RectTransform textRectTransform = currentSideBox.messageText.GetComponent<RectTransform>();
            Debug.Log(LayoutUtility.GetPreferredHeight(textRectTransform) + " :: " + this.gameObject.name);
            Debug.Log(currentSideBox.messageText.text);
            if (LayoutUtility.GetPreferredHeight(textRectTransform) > currentSideBox.textHolderPanel.sizeDelta.y)
            {
                Vector2 sizeDelta = currentSideBox.textHolderPanel.sizeDelta;
                sizeDelta.y = LayoutUtility.GetPreferredHeight(textRectTransform);
                
                Vector2 anchorPos = currentSideBox.textHolderPanel.anchoredPosition;
                anchorPos.y -= (LayoutUtility.GetPreferredHeight(textRectTransform) - currentSideBox.textHolderPanel.sizeDelta.y) / 2f;

                GetComponent<LayoutElement>().preferredHeight += 
                    (LayoutUtility.GetPreferredHeight(textRectTransform) - currentSideBox.textHolderPanel.sizeDelta.y);
                currentSideBox.textHolderPanel.sizeDelta = sizeDelta;
                currentSideBox.textHolderPanel.anchoredPosition = anchorPos;

                //Table View Height Change....
                m_cellHeightSlider.value = GetComponent<LayoutElement>().preferredHeight;
                SliderValueChanged(m_cellHeightSlider);
            }
            */

        }

        private string atText = "<color=grey>@</color>";
        private string colorOpen = "<color=blue>";
        private string colorClose = "</color>";
        void TaggedMessage ()
        {
            currentSideBox.htmlTextButton.gameObject.SetActive(true);
            currentSideBox.htmlTextButton.onClick.AddListener(OnClickTagButton);

//            string _whiteSpace = string.Empty;
            string _tagName = "@" + mChatMessage.normalMessagerMetaData.username;

            currentSideBox.messageText.text = " " + _tagName + mChatMessage.normalMessagerMetaData.message;
            CheckTextMessageSize();

            RectTransform textRectTransform = currentSideBox.messageText.GetComponent<RectTransform>();
            RectTransform buttonRectTransform = currentSideBox.htmlTextButton.GetComponent<RectTransform>();

            Vector2 anchorPos = buttonRectTransform.anchoredPosition;
//            anchorPos.y = LayoutUtility.GetPreferredHeight(textRectTransform) / 2f;
//            float offsetsY = currentSideBox.messageText.fontSize / 2f;
//            anchorPos.y -= offsetsY;

            Vector2 sizeDelta = buttonRectTransform.sizeDelta;

            Text buttonText = currentSideBox.htmlTextButton.transform.GetComponentInChildren<Text>();
            buttonText.text = string.Empty;
            buttonText.text = _tagName;
            sizeDelta.x = LayoutUtility.GetPreferredWidth(buttonText.GetComponent<RectTransform>());
            anchorPos.x = sizeDelta.x / 2f;

            buttonRectTransform.sizeDelta = sizeDelta;
            buttonRectTransform.anchoredPosition = anchorPos;

            buttonText.text = atText + colorOpen + mChatMessage.normalMessagerMetaData.username + colorClose;
            currentSideBox.messageText.text = "<color=white>" + _tagName + "</color> " + mChatMessage.normalMessagerMetaData.message;
		//	currentSideBox.messageProText.text = "<#ffffff>" + _tagName + "</color> " + "<#000000>" + mChatMessage.normalMessagerMetaData.message + "</color> ";
			//currentSideBox.messageProText.text = currentSideBox.messageText.text;
        }

        public void OnClickTagButton ()
        {
//            Debug.Log("OnClickTagButton Name: " + mChatMessage.normalMessagerMetaData.username + "ID: " + mChatMessage.normalMessagerMetaData.userid);
//			if (FreakAppManager.Instance.GetHomeScreenPanel() != null) {
				FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
            FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);

//			} 
//			else {
//				FreakAppManager.Instance.myUserProfile.memberId = mChatMessage.normalMessagerMetaData.userid;
//				FindObjectOfType<FreaksPanelUIController> ().PushPanel (FreaksPanelUIController.PanelType.OtherUserProfilePanel);
//			}
        }

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }

        /*public override void MessageDeleteCallback(bool success)
        {
            base.MessageDeleteCallback(success);
        }*/
    }
}
