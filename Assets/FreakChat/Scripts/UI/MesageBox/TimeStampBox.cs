﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class TimeStampBox : MessageBoxBase
    {
        public Text timeText;

        // Use this for initialization
        void Start () {

        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData data)
        {
            messageBoxType = MessageBoxType.System;
            base.InitMessageInfo(data);

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            timeText.text = FreakChatUtility.GetDisplayTimeStamp(mChatMessage.createdAt);
        }
    }
}
