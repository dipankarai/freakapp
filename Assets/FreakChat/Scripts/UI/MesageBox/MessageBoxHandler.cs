﻿using UnityEngine;
using System.Collections;

namespace Freak.Chat
{
    public class MessageBoxHandler
    {
        /*public static MessageBoxBase GetMessageByType (ChatMessage chatMessage)
        {
            MessageBoxBase messageBox = new MessageBoxBase();
            ChatMessageTableData _messageBox = new ChatMessageTableData ();

            switch(chatMessage.messageType)
            {
            case ChatMessage.MessageType.AdminMessage:
                    {
                        #if UNITY_EDITOR && UNITY_DEBUG
                        Debug.Log("Instantiate AdminMessage Window Here.....");
                        #endif
                    }
                    break;

            case ChatMessage.MessageType.Deleted:
                    {
                        #if UNITY_EDITOR && UNITY_DEBUG
                        //                            Debug.Log("Deleted Messages.....");
                        #endif
                    }
                    break;

                case ChatMessage.MessageType.FileLink:
                    {
                        if (chatMessage.fileType == ChatFileType.FileType.Audio ||
                            chatMessage.fileType == ChatFileType.FileType.AudioRecord)
                        {
                            AudioMessageBox _audioMsg = new AudioMessageBox();
                            _audioMsg.InitMessageInfo(chatMessage);

                            messageBox = _audioMsg as MessageBoxBase;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.AudioBuzz)
                        {
                            AudioBuzzMessageBox _audioMsg = new AudioBuzzMessageBox();
                            _audioMsg.InitMessageInfo(chatMessage);

                            messageBox = _audioMsg as MessageBoxBase;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Contact)
                        {
                            ContactMessageBox _contactMsg = new ContactMessageBox();
                            _contactMsg.InitMessageInfo(chatMessage);

                            messageBox = _contactMsg as MessageBoxBase;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Image ||
                            chatMessage.fileType == ChatFileType.FileType.Stickers)
                        {
                            ImageMessageBox _imageMsg = new ImageMessageBox();
                            _imageMsg.InitMessageInfo(chatMessage);

                            messageBox = _imageMsg as MessageBoxBase;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Video)
                        {
                            VideoMessageBox _otherMsg = new VideoMessageBox();
                            _otherMsg.InitMessageInfo(chatMessage);

                            messageBox = _otherMsg as MessageBoxBase;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Others)
                        {
                            OtherFileMessageBox _otherMsg = new OtherFileMessageBox();
                            _otherMsg.InitMessageInfo(chatMessage);

                            messageBox = _otherMsg as MessageBoxBase;
                        }
                    }
                    break;

                case ChatMessage.MessageType.Message:
                    {
                        if (chatMessage.normalMessagerMetaData != null &&
                            chatMessage.normalMessagerMetaData.messageType != ChatMessage.MetaMessageType.None)
                        {
                            FreakChatSerializeClass.NormalMessagerMetaData _metaData = 
                                chatMessage.normalMessagerMetaData;

                            if (_metaData.messageType == ChatMessage.MetaMessageType.InAppFile)
                            {
                                if(_metaData.fileType == ChatFileType.FileType.Stickers)
                                {
                                    ImageMessageBox _imageMsg = new ImageMessageBox();
                                    _imageMsg.InitMessageInfo(chatMessage);

                                    messageBox = _imageMsg as MessageBoxBase;
                                }
                                else if (_metaData.fileType == ChatFileType.FileType.AudioBuzz)
                                {
                                    AudioBuzzMessageBox _audioMsg = new AudioBuzzMessageBox();
                                    _audioMsg.InitMessageInfo(chatMessage);
                                    
                                    messageBox = _audioMsg as MessageBoxBase;
                                }
                                else if (chatMessage.fileType == ChatFileType.FileType.Challenge)
                                {
                                    AcceptOrRejectChalleng _otherMsg = new AcceptOrRejectChalleng();
                                    _otherMsg.InitMessageInfo(chatMessage);

                                    messageBox = _otherMsg as MessageBoxBase;
                                }
                                else if (chatMessage.fileType == ChatFileType.FileType.Referral)
                                {
                                    GameReferralAcceptReject _otherMsg = new GameReferralAcceptReject();
                                    _otherMsg.InitMessageInfo(chatMessage);

                                    messageBox = _otherMsg as MessageBoxBase;
                                }
                            }
                            else if (_metaData.messageType == ChatMessage.MetaMessageType.SystemNewGroup)
                            {
                                SystemMessageBox _systemMsg = new SystemMessageBox();
                                _systemMsg.InitMessageInfo(chatMessage);

                                messageBox = _systemMsg as MessageBoxBase;
                            }
                            else if (_metaData.messageType == ChatMessage.MetaMessageType.TagFriend)
                            {
                                TextMessageBox _textMsg = new TextMessageBox();
                                _textMsg.InitMessageInfo(chatMessage);

                                messageBox = _textMsg as MessageBoxBase;
                            }
                        }
                        else
                        {
                            TextMessageBox _textMsg = new TextMessageBox();
                            _textMsg.InitMessageInfo(chatMessage);

                            messageBox = _textMsg as MessageBoxBase;
                        }

                        if (string.IsNullOrEmpty(chatMessage.data) == false)
                        {
                            if (chatMessage.fileType == ChatFileType.FileType.Stickers)
                            {
                                ImageMessageBox _imageMsg = new ImageMessageBox();
                                _imageMsg.InitMessageInfo(chatMessage);

                                messageBox = _imageMsg as MessageBoxBase;
                            }
                            else if (chatMessage.fileType == ChatFileType.FileType.AudioBuzz)
                            {
                                AudioBuzzMessageBox _audioMsg = new AudioBuzzMessageBox();
                                _audioMsg.InitMessageInfo(chatMessage);

                                messageBox = _audioMsg as MessageBoxBase;
                            }
                        }
                    }
                    break;
            }

            return messageBox;
        }*/

        public static ChatMessageTableData GetMessageTableDataByType (ChatMessage chatMessage)
        {
            ChatMessageTableData messageBox = new ChatMessageTableData ();
            messageBox.chatMessage = chatMessage;

            switch(chatMessage.messageType)
            {
                case ChatMessage.MessageType.FileLink:
                    {
                        if (chatMessage.fileType == ChatFileType.FileType.Audio ||
                            chatMessage.fileType == ChatFileType.FileType.AudioRecord)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.AudioMessageBox;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.AudioBuzz)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.AudioBuzzMessageBox;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Contact)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.ContactMessageBox;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Image ||
                            chatMessage.fileType == ChatFileType.FileType.Stickers)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.ImageMessageBox;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Video)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.VideoMessageBox;
                        }
                        else if (chatMessage.fileType == ChatFileType.FileType.Others)
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.OtherFileMessageBox;
                        }

						messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                    }
                break;

                case ChatMessage.MessageType.Message:
                    {
                        if (chatMessage.normalMessagerMetaData != null &&
                            chatMessage.normalMessagerMetaData.messageType != ChatMessage.MetaMessageType.None)
                        {
                            FreakChatSerializeClass.NormalMessagerMetaData _metaData = 
                                chatMessage.normalMessagerMetaData;

                            if (_metaData.messageType == ChatMessage.MetaMessageType.InAppFile)
                            {
                                if(_metaData.fileType == ChatFileType.FileType.Stickers)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.ImageMessageBox;
                                }
                                else if (_metaData.fileType == ChatFileType.FileType.AudioBuzz)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.AudioBuzzMessageBox;
                                }
                                else if (chatMessage.fileType == ChatFileType.FileType.Challenge)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.AcceptOrRejectChalleng;
                                }
                                else if (chatMessage.fileType == ChatFileType.FileType.Referral)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.GameReferralAcceptReject;
                                }

								messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                            }
                            else if (_metaData.messageType == ChatMessage.MetaMessageType.SystemNewGroup)
                            {
                                messageBox.tableDataType = ChatMessageTableData.TableDataType.SystemMessageBox;
								messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                            }
                            else if (_metaData.messageType == ChatMessage.MetaMessageType.TagFriend)
                            {
                                messageBox.tableDataType = ChatMessageTableData.TableDataType.TagFriend;
								messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                            }
                            else if (_metaData.messageType == ChatMessage.MetaMessageType.GameInvite)
                            {
                                if (_metaData.gameType == FreakChatSerializeClass.NormalMessagerMetaData.GameType.Challenge)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.AcceptOrRejectChalleng;
                                }
                                else if (_metaData.gameType == FreakChatSerializeClass.NormalMessagerMetaData.GameType.Referral)
                                {
                                    messageBox.tableDataType = ChatMessageTableData.TableDataType.GameReferralAcceptReject;
                                }

								messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                            }
                        }
                        else
                        {
                            messageBox.tableDataType = ChatMessageTableData.TableDataType.TextMessageBox;
							messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                        }

                        if (string.IsNullOrEmpty(chatMessage.data) == false)
                        {
                            if (chatMessage.fileType == ChatFileType.FileType.Stickers)
                            {
                                messageBox.tableDataType = ChatMessageTableData.TableDataType.ImageMessageBox;
                            }
                            else if (chatMessage.fileType == ChatFileType.FileType.AudioBuzz)
                            {
                                messageBox.tableDataType = ChatMessageTableData.TableDataType.AudioBuzzMessageBox;
                            }

							messageBox.cellSize = SetCellSize (messageBox.tableDataType);
                        }
                    }
                break;

                default:
                    messageBox = null;
                    break;
            }

            return messageBox;
        }
    
        public static float SetCellSize(ChatMessageTableData.TableDataType type)
		{

			switch (type)
			{
			case (ChatMessageTableData.TableDataType.TextMessageBox):
				{
					return 165;
				}
			case (ChatMessageTableData.TableDataType.AudioMessageBox):
				{
					return 155;
				}
			case (ChatMessageTableData.TableDataType.ImageMessageBox):
				{
					return 260;
				}
			case (ChatMessageTableData.TableDataType.VideoMessageBox):
				{
					return 210;
				}
			case (ChatMessageTableData.TableDataType.ContactMessageBox):
				{
					return 200;
				}
			case (ChatMessageTableData.TableDataType.OtherFileMessageBox):
				{
					return 170;
				}
			case (ChatMessageTableData.TableDataType.SystemMessageBox):
				{
					return 60;
				}
			case (ChatMessageTableData.TableDataType.TimeStampBox):
				{
					return 100;
				}
			case (ChatMessageTableData.TableDataType.AcceptOrRejectChalleng):
				{
					return 273;
				}
			case (ChatMessageTableData.TableDataType.GameReferralAcceptReject):
				{
					return 273;
				}
			case (ChatMessageTableData.TableDataType.AudioBuzzMessageBox):
				{
					return 275;
				}
			default:
				return 100;
			}

		}


        public static MessageBoxBase GetMessageBoxInstance (MessageBoxBase previousMessageBox, ChatMessageTableData messageBox)
        {

            if (previousMessageBox != null)
            {
//                if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
//                {
//                    return previousMessageBox;
//                }
//
//				if (previousMessageBox.reuseIdentifier.Equals(messageBox.GetDataType))
//				{
//					return previousMessageBox;
//				}
            }

            switch (messageBox.tableDataType)
            {
                case (ChatMessageTableData.TableDataType.TextMessageBox):
                    {
                        return (TextMessageBox)GameObject.Instantiate<TextMessageBox> (UIController.Instance.assetsReferences.textMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.AudioMessageBox):
                    {
                        return (AudioMessageBox)GameObject.Instantiate<AudioMessageBox> (UIController.Instance.assetsReferences.audioMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.ImageMessageBox):
                    {
                        return (ImageMessageBox)GameObject.Instantiate<ImageMessageBox> (UIController.Instance.assetsReferences.imageMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.VideoMessageBox):
                    {
                        return (VideoMessageBox)GameObject.Instantiate<VideoMessageBox> (UIController.Instance.assetsReferences.videoMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.ContactMessageBox):
                    {
                        return (ContactMessageBox)GameObject.Instantiate<ContactMessageBox> (UIController.Instance.assetsReferences.contactMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.OtherFileMessageBox):
                    {
                        return (OtherFileMessageBox)GameObject.Instantiate<OtherFileMessageBox> (UIController.Instance.assetsReferences.otherFileMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.SystemMessageBox):
                    {
                        return (SystemMessageBox)GameObject.Instantiate<SystemMessageBox> (UIController.Instance.assetsReferences.systemMessageBox);
                    }
                case (ChatMessageTableData.TableDataType.TimeStampBox):
                    {
                        return (TimeStampBox)GameObject.Instantiate<TimeStampBox> (UIController.Instance.assetsReferences.timeStampBox);
                    }
                case (ChatMessageTableData.TableDataType.AcceptOrRejectChalleng):
                    {
                        return (AcceptOrRejectChalleng)GameObject.Instantiate<AcceptOrRejectChalleng> (UIController.Instance.assetsReferences.AcceptRejectChallenge);
                    }
                case (ChatMessageTableData.TableDataType.GameReferralAcceptReject):
                    {
                        return (GameReferralAcceptReject)GameObject.Instantiate<GameReferralAcceptReject> (UIController.Instance.assetsReferences.GameReferralAcceptReject);
                    }
                case (ChatMessageTableData.TableDataType.AudioBuzzMessageBox):
                    {
                        return (AudioBuzzMessageBox)GameObject.Instantiate<AudioBuzzMessageBox> (UIController.Instance.assetsReferences.audioBuzzMessageBox);
                    }
                default:
                return null;
            }

        }

		public static MessageBoxBase GetMessageBoxPrefab (ChatMessageTableData messageBox)
		{
            switch (messageBox.tableDataType)
			{
			case (ChatMessageTableData.TableDataType.TextMessageBox):
				{
					return (TextMessageBox)(UIController.Instance.assetsReferences.textMessageBox);
				}
			case (ChatMessageTableData.TableDataType.AudioMessageBox):
				{
					return (AudioMessageBox)(UIController.Instance.assetsReferences.audioMessageBox);
				}
			case (ChatMessageTableData.TableDataType.ImageMessageBox):
				{
					return (ImageMessageBox)(UIController.Instance.assetsReferences.imageMessageBox);
				}
			case (ChatMessageTableData.TableDataType.VideoMessageBox):
				{
					return (VideoMessageBox)(UIController.Instance.assetsReferences.videoMessageBox);
				}
			case (ChatMessageTableData.TableDataType.ContactMessageBox):
				{
					return (ContactMessageBox)(UIController.Instance.assetsReferences.contactMessageBox);
				}
			case (ChatMessageTableData.TableDataType.OtherFileMessageBox):
				{
					return (OtherFileMessageBox)(UIController.Instance.assetsReferences.otherFileMessageBox);
				}
			case (ChatMessageTableData.TableDataType.SystemMessageBox):
				{
					return (SystemMessageBox)(UIController.Instance.assetsReferences.systemMessageBox);
				}
			case (ChatMessageTableData.TableDataType.TimeStampBox):
				{
					return (TimeStampBox)(UIController.Instance.assetsReferences.timeStampBox);
				}
			case (ChatMessageTableData.TableDataType.AcceptOrRejectChalleng):
				{
					return (AcceptOrRejectChalleng)(UIController.Instance.assetsReferences.AcceptRejectChallenge);
				}
			case (ChatMessageTableData.TableDataType.GameReferralAcceptReject):
				{
					return (GameReferralAcceptReject)(UIController.Instance.assetsReferences.GameReferralAcceptReject);
				}
			case (ChatMessageTableData.TableDataType.AudioBuzzMessageBox):
				{
					return (AudioBuzzMessageBox)(UIController.Instance.assetsReferences.audioBuzzMessageBox);
				}
			default:
				return null;
			}

		}

        /*public static MessageBoxBase GetMessageBoxInstance (MessageBoxBase previousMessageBox, MessageBoxBase messageBox)
        {
            if (previousMessageBox != null)
            {
                System.Type _typeA = previousMessageBox.GetType ();
                System.Type _typeB = messageBox.GetType ();

                if (_typeA == _typeB)
                {
                    return previousMessageBox;
                }
            }

            if (messageBox is TextMessageBox)
            {
                return (TextMessageBox)GameObject.Instantiate<TextMessageBox> (UIController.Instance.assetsReferences.textMessageBox);
            }
            else if (messageBox is AudioMessageBox)
            {
                return (AudioMessageBox)GameObject.Instantiate<AudioMessageBox> (UIController.Instance.assetsReferences.audioMessageBox);
            }
            else if (messageBox is ImageMessageBox)
            {
                Debug.LogError ("Instantiate ImageMessageBox");
                return (ImageMessageBox)GameObject.Instantiate<ImageMessageBox> (UIController.Instance.assetsReferences.imageMessageBox);
            }
            else if (messageBox is VideoMessageBox)
            {
                return (VideoMessageBox)GameObject.Instantiate<VideoMessageBox> (UIController.Instance.assetsReferences.videoMessageBox);
            }
            else if (messageBox is ContactMessageBox)
            {
                return (ContactMessageBox)GameObject.Instantiate<ContactMessageBox> (UIController.Instance.assetsReferences.contactMessageBox);
            }
            else if (messageBox is OtherFileMessageBox)
            {
                return (OtherFileMessageBox)GameObject.Instantiate<OtherFileMessageBox> (UIController.Instance.assetsReferences.otherFileMessageBox);
            }
            else if (messageBox is SystemMessageBox)
            {
                return (SystemMessageBox)GameObject.Instantiate<SystemMessageBox> (UIController.Instance.assetsReferences.systemMessageBox);
            }
            else if (messageBox is TimeStampBox)
            {
                return (TimeStampBox)GameObject.Instantiate<TimeStampBox> (UIController.Instance.assetsReferences.timeStampBox);
            }
            else if (messageBox is AcceptOrRejectChalleng)
            {
                return (AcceptOrRejectChalleng)GameObject.Instantiate<AcceptOrRejectChalleng> (UIController.Instance.assetsReferences.AcceptRejectChallenge);
            }
            else if (messageBox is GameReferralAcceptReject)
            {
                return (GameReferralAcceptReject)GameObject.Instantiate<GameReferralAcceptReject> (UIController.Instance.assetsReferences.GameReferralAcceptReject);
            }
            else if (messageBox is AudioBuzzMessageBox)
            {
                return (AudioBuzzMessageBox)GameObject.Instantiate<AudioBuzzMessageBox> (UIController.Instance.assetsReferences.audioBuzzMessageBox);
            }
            else
            {
                return null;
            }
        }*/
    }
}
