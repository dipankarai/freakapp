﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using Tacticsoft;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;


namespace Freak.Chat
{
	public class MessageBoxBase : EnhancedScrollerCellView
    {
        public enum MessageBoxType
        {
            None = 0,
            Audio,
            Image,
            Video,
            Contact,
            Text,
            System,
            Broadcast,
            Others
        }

        public enum MessageSide
        {
            None,
            LeftSide,
            RightSide
        }

        public ChatMessage chatMessage { get { return mChatMessage; }}
		public MessageUIComponent CurrentSideBox{ get { return currentSideBox; } }

        protected ChatMessage mChatMessage;
        protected MessageBoxType messageBoxType;
        protected MessageSide messageSide;
        protected MessageUIComponent currentSideBox;
        protected bool isOwner;
        private string profileImageUrl;
        protected ChatWindowUI chatWindowUI;
        public LayoutElement layoutElement;

        public virtual string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        void Awake ()
        {
            if (layoutElement == null)
            {
                layoutElement = gameObject.AddComponent<LayoutElement>();
            }
        }

        void OnEnable ()
        {
            if (chatWindowUI == null)
            {
                chatWindowUI = UIController.Instance.chatWindowUI;
            }

            CheckMessageState ();
            CheckReceiptStatus ();
        }

        /*void SoundCheck ()
        {
            if (isOwner)
            {
                return;
            }
            else
            {
                if (ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount > 0)
                {
                    ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount = 0;

                    if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
                    {
                        if (ChatManager.Instance.IsGroupMuted(ChatManager.Instance.currentSelectedChatChannel.createdAt.ToString()) == false)
                        {
                            UIController.Instance.PlaySendMessageSound ();
                        }
                    }
                    else
                    {
                        if (ChatManager.Instance.IsUserMuted(mChatMessage.messageSender.userId) == false)
                        {
                            UIController.Instance.PlaySendMessageSound ();
                        }
                    }
                }
            }
        }*/

        // Use this for initialization
        protected ChatMessageTableData chatMessageTableData;
        public virtual void InitMessageInfo (ChatMessageTableData data)
        {
            chatMessageTableData = data;
            mChatMessage = chatMessageTableData.chatMessage;
            messageSide = GetSide(mChatMessage.messageSender.userId);

            // set the size of the cell in the model
            chatMessageTableData.cellSize = layoutElement.preferredHeight;
        }

        public virtual void InitMessage()
        {
            if (currentSideBox != null)
            {
                currentSideBox.gameObject.SetActive(true);
            }

            if (currentSideBox != null && currentSideBox.downloadingProgress != null)
                currentSideBox.downloadingProgress.gameObject.SetActive(false);

            CheckMessageState ();
            CheckReceiptStatus ();
        }

        public override void RefreshCellView()
        {
            InitMessage();
        }

		public void SetTimerPanelWidth()
		{
			if (currentSideBox != null && currentSideBox.timeerPanel != null)
			{
				if (currentSideBox.timeText.text.Length > 9) {
					//currentSideBox.timeerPanel.GetComponent<RectTransform> ().rect.size = new Vector2 (150, 20);
					currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (146, 33);
					if (isOwner == false) {
						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (197, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);//186
//						Debug.Log("Message panel -- >"+ currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition);
					} else {
						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (-201, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);
//						Debug.Log("Message panel RIGHT-- >"+ currentSideBox.timeerPanel.transform.position);
					}

				} else {
					//currentSideBox.timeerPanel.GetComponent<RectTransform> ().rect.size = new Vector2 (100, 20);
					if (isOwner == false) {
						currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (125, 33);
						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (188, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);//186
//						Debug.Log("Small Message panel -- >"+ currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition);
					} else {
						currentSideBox.timeerPanel.rectTransform.sizeDelta = new Vector2 (130, 33);
						//currentSideBox.timeerPanel.transform.localPosition = new Vector2 (0, 0);
						currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition = new Vector2 (-193, currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition.y);
//						Debug.Log("Small Message panel RIGHT-- >"+ currentSideBox.timeerPanel.transform.rectTransform().anchoredPosition);
					}
				}
			}
		}

        public MessageSide GetSide (string senderId)
        {
            if (senderId == ChatManager.Instance.UserId)
            {
                isOwner = true;
                return MessageSide.RightSide;
            }

            isOwner = false;
            return MessageSide.LeftSide;
        }

        public virtual void DownloadFile ()
        {
            Debug.Log ("DownloadFile Base Class");
        }

        public virtual void DownloadFile (FolderLocation folderLocation)
        {
            if (isOwner)
            {
                FileSavedCallback(true);
                return;
            }

            if (Path.HasExtension (mChatMessage.fileUrl))
            {
                DownloadManager.Instance.DownLoadFile (mChatMessage.fileUrl, folderLocation, mChatMessage, isOwner);
            }
            else
            {
                DownloadManager.Instance.DownLoadFile (mChatMessage.fileUrl, folderLocation, mChatMessage, isOwner, Path.GetExtension (mChatMessage.fileName));
            }

            Debug.Log ("DownLoadFile " + mChatMessage.fileUrl);

            if (currentSideBox != null && currentSideBox.downloadingProgress != null)
				currentSideBox.ProgressImage.fillAmount = 0.1f;

            /*if (currentSideBox != null && currentSideBox.fileLoadingObject != null)
            {
                ShowFileLoading(true);
            }*/

            mChatMessage.messageState = ChatMessage.MessageState.Downloading;
            CheckMessageState ();
        }

        public virtual void DownloadProgress (float progress)
        {
            /*if (currentSideBox != null && currentSideBox.fileLoadingObject != null)
            {
                ShowFileLoading(false);
            }*/

            if (currentSideBox != null && currentSideBox.downloadingProgress != null &&
                currentSideBox.downloadingProgress.gameObject.activeInHierarchy == false)
                currentSideBox.downloadingProgress.gameObject.SetActive(true);

			if (currentSideBox != null && currentSideBox.downloadingProgress != null && progress > currentSideBox.ProgressImage.fillAmount)
				currentSideBox.ProgressImage.fillAmount = progress;
        }

        public virtual void FileSavedCallback (bool success)
        {
            if (isOwner == false)
            {
                mChatMessage.messageState = ChatMessage.MessageState.Normal;
            }
            else
            {
                if (mChatMessage.messageState != ChatMessage.MessageState.Normal)
                {
                    if (mChatMessage.receiptStatus != ChatMessage.MessageReceiptType.Unknown)
                    {
                        mChatMessage.messageState = ChatMessage.MessageState.Normal;
                    }
                }
            }
                
            CheckMessageState ();
        }

        public virtual void ShowProfileImage (string imageUrl)
        {
            profileImageUrl = isOwner == true ? FreakAppManager.Instance.profileLink : imageUrl;
            ShowProfileImageURL();
            /*if (isOwner)
            {
                if (chatWindowUI.GetSprite(ChatManager.Instance.UserId) != null)
                {
                    currentSideBox.profileImage.sprite = chatWindowUI.GetSprite(ChatManager.Instance.UserId);
                }
                else
                {
                    currentSideBox.profileImage.FreakSetSprite(FreakAppManager.Instance.myUserProfile.userPic);
                    chatWindowUI.AddSprite(ChatManager.Instance.UserId, currentSideBox.profileImage.sprite);
                }
            }
            else
            {
                ShowProfile ();
            }*/
        }

        public void ShowProfileImageURL ()
        {
            Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(profileImageUrl, Freak.FolderLocation.Profile, null);
            if (texture != null)
            {
                currentSideBox.profileImage.FreakSetSprite(texture);
            }
            texture = null;
        }

        void ShowProfile ()
        {
            if (ChatManager.Instance.currentSelectedChatChannel == null || mChatMessage == null)
                return;

            if (ChatManager.Instance.currentSelectedChatChannel.GetMember(mChatMessage.messageSender.userId) != null)
            {
                ChatUser _chatUser = ChatManager.Instance.currentSelectedChatChannel.GetMember (mChatMessage.messageSender.userId);
                
                if (chatWindowUI.GetSprite(mChatMessage.messageSender.userId) != null)
                {
                    currentSideBox.profileImage.sprite = chatWindowUI.GetSprite(mChatMessage.messageSender.userId);
                }
                else
                {
                    if (_chatUser != null && _chatUser.texture != null)
                    {
                        //                    currentSideBox.profileImage.FreakSetTexture (_chatUser.imageCache);
                        currentSideBox.profileImage.FreakSetSprite(_chatUser.texture);
                        chatWindowUI.AddSprite(mChatMessage.messageSender.userId, currentSideBox.profileImage.sprite);
                    }
                    else
                    {
                        Invoke ("ShowProfile", 0.5f);
                    }
                }
            }
        }

//        void DownloadProfileImageCallback (bool success)
//        {
//            string filePath = StreamManager.LoadFilePath(Path.GetFileName(profileImageUrl), FolderLocation.Profile, false);
////          Debug.Log("FileSavedCallback" + filePath);
//
//            if (File.Exists(filePath))
//            {
//                if (chatWindowUI.imageHandler.GetResizedTexture (filePath, mChatMessage.messageSender.userId) != null)
//                {
//                    currentSideBox.profileImage.FreakSetSprite (chatWindowUI.imageHandler.GetResizedTexture (filePath, mChatMessage.messageSender.userId));
//                }
//                else
//                {
//                    LoadImage ();
//                    /*Texture2D texture = null;
//                    byte[] fileByte = System.IO.File.ReadAllBytes(filePath);
//                    texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
//                    texture.LoadImage(fileByte);
//                    
//                    Rect rect = new Rect(0, 0, texture.width, texture.height);
//                    currentSideBox.profileImage.sprite = Sprite.Create(texture, rect, new Vector2(0, 0), 1f);
//
//                    Texture2D resizedTexture = TextureResize.ResizeTexture (currentSideBox.profileImage.rectTransform, texture);
//                    chatWindowUI.imageHandler.AddResizedTexture (resizedTexture, filePath, mChatMessage.messageSender.userId);
//                    currentSideBox.profileImage.FreakSetSprite (resizedTexture);*/
//                }
//            }
//        }

//        string profileImageFilePath;
        /*void LoadImage()
        {
            profileImageFilePath = StreamManager.LoadFilePath(Path.GetFileName(profileImageUrl), FolderLocation.Profile, false);
            if (this.gameObject.activeInHierarchy)
            {
                StartCoroutine (LoadImageFromLocal (profileImageFilePath));
            }
            else
            {
                Invoke ("LoadImage", 0.5f);
            }
        }*/

        /*IEnumerator LoadImageFromLocal (string path)
        {
            var www = new WWW("file://" + path);
            yield return www;
            while (!www.isDone)
            {
                yield return null;
            }

            if (string.IsNullOrEmpty(www.error))
            {
                //Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
                //img.sprite = Sprite.Create(www.texture, rect, new Vector2(0, 0), 1f);

                Texture2D resizedTexture = TextureResize.ResizeTexture (currentSideBox.profileImage.rectTransform, www.texture);
                chatWindowUI.imageHandler.AddResizedTexture (resizedTexture, profileImageFilePath, mChatMessage.messageSender.userId);
                currentSideBox.profileImage.FreakSetSprite (resizedTexture);
            }
            else
            {
                Debug.LogError("url ::: " + path + www.error);
            }

//            Resources.UnloadUnusedAssets ();
        }*/

        #region MESSAGE RECEIPT

        public void CheckMessageState ()
        {
            if (mChatMessage == null)
                return;

            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("mChatMessage.messageState " + mChatMessage.messageState + " :: " + mChatMessage.messageId);
            #endif

            switch (mChatMessage.messageState)
            {
                case ChatMessage.MessageState.Normal:
                    {
                        ShowHideLoading (false);
                        ShowHideRetryButton (false);
                        ShowHideDownloadBar (false);
                    }
                break;

                case ChatMessage.MessageState.Loading:
                    {
                        ShowHideLoading (true);
                        ShowHideRetryButton (false);
                        ShowHideDownloadBar (false);
                    }
                break;

                case ChatMessage.MessageState.Downloading:
                    {
                        ShowHideLoading (false);
                        ShowHideRetryButton (false);
                        ShowHideDownloadBar (false);
                    }
                break;

                case ChatMessage.MessageState.Retry:
                    {
                        ShowHideLoading (false);
                        ShowHideRetryButton (true);
                        ShowHideDownloadBar (false);
                    }
                break;

                default:
                break;
            }

            ForAudioBuzzButton();
        }

        void Update ()
        {
            if (mChatMessage != null)
            {
                switch (mChatMessage.messageState)
                {
                    case ChatMessage.MessageState.Downloading:

                        if (mChatMessage.downloadingValue == 0)
                        {
                            ShowHideLoading (true);
                            ShowHideDownloadBar (false);
                        }
                        else if (mChatMessage.downloadingValue > 0)
                        {
                            ShowHideLoading (false);
                            
                            if (currentSideBox != null)
                            {
                                if (currentSideBox.downloadingProgress != null)
                                {
                                    if (currentSideBox.downloadingProgress.gameObject.activeInHierarchy == false)
                                        ShowHideDownloadBar (true);
                                    
                                    if (mChatMessage.downloadingValue > currentSideBox.ProgressImage.fillAmount)
                                        currentSideBox.ProgressImage.fillAmount = mChatMessage.downloadingValue;
                                }
                            }
                        }
                        else if (mChatMessage.downloadingValue >= 1)
                        {
                            ShowHideLoading (true);
                            ShowHideDownloadBar (false);
                        }

                    break;
                    case ChatMessage.MessageState.DownloadFail:
                        {
                            mChatMessage.messageState = Freak.Chat.ChatMessage.MessageState.Retry;
                            mChatMessage.downloadingValue = 0;
                            CheckMessageState ();
                        }
                    break;
                    case ChatMessage.MessageState.DownloadSuccess:
                        {
                            FileSavedCallback (true);
                            mChatMessage.downloadingValue = 0;
                        }
                    break;

                    case ChatMessage.MessageState.Loading:
                        {
                            if (ChatManager.Instance.isInternetConnected)
                            {
                                if (mChatMessage.loadingTime == 0)
                                {
                                    mChatMessage.loadingTime = SendBird.Utils.CurrentTimeMillis ();
                                }
                                else if ((SendBird.Utils.CurrentTimeMillis () - mChatMessage.loadingTime) >= 15000)
                                {
                                    mChatMessage.messageState = ChatMessage.MessageState.Retry;
                                    CheckMessageState ();
                                }
                            }
                            else
                            {


                                mChatMessage.messageState = ChatMessage.MessageState.Retry;
                                CheckMessageState ();
                            }
                        }

                    break;

                    case ChatMessage.MessageState.Retry:
                        {
                            if (currentSideBox != null && currentSideBox.retryButton != null && currentSideBox.retryButton.gameObject.activeInHierarchy == false)
                            {
                                mChatMessage.messageState = ChatMessage.MessageState.Retry;
                                CheckMessageState ();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        void OnDisable ()
        {
//            Resources.UnloadUnusedAssets ();
        }

        void CheckReceiptStatus ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            if (mChatMessage != null)
            {
                //Debug.Log(mChatMessage.messageId + " @*@*@*@*@*@*@ " +mChatMessage.receiptStatus);
            }
            #endif

            if (mChatMessage == null)
                return;

            if (isOwner == false)
            {
                if (mChatMessage.receiptStatus != ChatMessage.MessageReceiptType.Read)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
                    #endif
                    mChatMessage.receiptStatus = ChatMessage.MessageReceiptType.Read;
                    ChatChannelHandler.MarkLastMessageAsRead(ChatManager.Instance.currentSelectedChatChannel, mChatMessage.messageId);
                }
            }

            switch (mChatMessage.receiptStatus)
            {
                case  ChatMessage.MessageReceiptType.Unknown:
                    {
                        if (currentSideBox != null)
                        {
                            if (currentSideBox.msgReadStatusImage != null)
                            {
                                currentSideBox.msgReadStatusImage.enabled = false;
                            }
                        }
                    }
                break;
                case ChatMessage.MessageReceiptType.Send:
                    {
                        if (currentSideBox != null && currentSideBox.msgReadStatusImage != null) 
                        {
                            currentSideBox.msgReadStatusImage.enabled = true;
                            currentSideBox.msgReadStatusImage.sprite = currentSideBox.sentNotificationSprite;
                        }
                    }
                break;
                case ChatMessage.MessageReceiptType.Delivered:
                    if (currentSideBox != null && currentSideBox.msgReadStatusImage != null)
                    {
                        currentSideBox.msgReadStatusImage.enabled = true;
                        currentSideBox.msgReadStatusImage.sprite = currentSideBox.deliveredNotificationSprite;
                    }
                break;
                case ChatMessage.MessageReceiptType.Read:
                    if (currentSideBox != null && currentSideBox.msgReadStatusImage != null) 
                    {
                        currentSideBox.msgReadStatusImage.enabled = true;
                        currentSideBox.msgReadStatusImage.sprite = currentSideBox.readNotificationSprite;
                    }
                break;
                default:
                break;
            }
        }

        public bool IsReadReceipt ()
        {
            if (currentSideBox.msgReadStatusImage.sprite.name == currentSideBox.readNotificationSprite.name)
                return true;
            else
                return false;
        }

        public virtual void ForAudioBuzzButton ()
        {
            
        }

        void ShowHideLoading (bool show)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log(show + "ShowHideLoading " + mChatMessage.messageState + " :: " + mChatMessage.messageId);
            #endif
            if (currentSideBox != null && currentSideBox.fileLoadingObject != null)
            {
                currentSideBox.fileLoadingObject.SetActive(show);
            }
        }

        void ShowHideRetryButton (bool show)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log(show + "ShowHideRetryButton " + mChatMessage.messageState + " :: " + mChatMessage.messageId);
            #endif
            if (currentSideBox != null)
            {
                if (currentSideBox.retryButton != null)
                {
                    currentSideBox.retryButton.gameObject.SetActive(show);
                }
            }
        }

        void ShowHideDownloadBar (bool show)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log(show + "ShowHideDownloadBar " + mChatMessage.messageState + " :: " + mChatMessage.messageId);
            #endif
            if (currentSideBox != null)
            {
                if (currentSideBox.downloadingProgress != null)
                {
                    currentSideBox.downloadingProgress.gameObject.SetActive(show);
                    if (show == false)
                        currentSideBox.ProgressImage.fillAmount = 0;
                }
            }
        }

        public void MessageSendFailed ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HideLoading "+mChatMessage.messageId);
            #endif
//            mChatMessage.canShowRetry = true;
            if (currentSideBox  != null && currentSideBox.fileLoadingObject != null)
            {
                ShowFileLoading(false);
            }
        }

        void ShowFileLoading(bool show)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("ShowFileLoading " + show);
            #endif
            if (currentSideBox.fileLoadingObject != null)
                currentSideBox.fileLoadingObject.SetActive(show);
            
            if (mChatMessage.receiptStatus == ChatMessage.MessageReceiptType.Unknown)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Show Retry");
                #endif
                if (currentSideBox.retryButton != null)// && mChatMessage.canShowRetry)
                {
                    currentSideBox.retryButton.gameObject.SetActive(true);
                }
            }
        }

        public void OnClickRetryButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("On CLICK RETRY BUTTON");
            #endif

            if (isOwner)
            {
                if (Path.GetFileNameWithoutExtension(mChatMessage.fileUrl).Equals(Path.GetFileNameWithoutExtension(mChatMessage.fileName)))
                {
                    ChatFileType fileInfo = new ChatFileType(mChatMessage.filePath, mChatMessage.fileType);
                    MessageQueueHandler.Instance.SendFileMessage(ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, fileInfo, mChatMessage.customField, mChatMessage.data);
                }
                else
                {
                    MessageQueueHandler.Instance.SendFileMessageWithUrl(ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, mChatMessage.fileUrl, mChatMessage.fileName, 
                        ChatFileType.ConvertChatFileTypeToString(mChatMessage.fileType), mChatMessage.fileSize, mChatMessage.data, mChatMessage.customField);
                }
                
//                mChatMessage.canShowRetry = false;
                currentSideBox.retryButton.gameObject.SetActive(false);
                mChatMessage.loadingTime = 0;
                mChatMessage.messageState = ChatMessage.MessageState.Loading;
            }
            else
            {
                DownloadFile ();
            }

            CheckMessageState ();

            ChatManager.CheckInternetConnection();
        }

        public virtual void MessageSendCallback ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Message Send Callback========");
            #endif
//            mReceiptState = ReceiptState.Send;
//            CheckReceiptStatus ();
        }

        public virtual void MessageDeliveredCallback ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Message Delivered Callback-------->>> " + mChatMessage.receiptStatus);
            #endif
//            CheckReceiptStatus ();
        }

        public virtual void MessageReadCallback ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("MessageReadCallback Message has been read....");
            #endif
            Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
            mChatMessage.receiptStatus = ChatMessage.MessageReceiptType.Read;
            ChatChannelHandler.MarkLastMessageAsRead(ChatManager.Instance.currentSelectedChatChannel, mChatMessage.messageId);
            CheckReceiptStatus ();
        }

        #endregion

        #region TableViewCell
        public int rowNumber { get; set; }
        public Slider m_cellHeightSlider;

        [System.Serializable]
        public class CellHeightChangedEvent : UnityEngine.Events.UnityEvent<int, float> { }
        public CellHeightChangedEvent onCellHeightChanged;

        public void SliderValueChanged(Slider slider) {
            onCellHeightChanged.Invoke(rowNumber, slider.value);
        }

        public float height {
            get { return m_cellHeightSlider.value; }
            set { m_cellHeightSlider.value = value; }
        }

//        public override string reuseIdentifier
//        {
//            get {
//                return GetClassType;
//            }
//        }
        #endregion

        public virtual void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle BaseClass");
        }

        public virtual void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold BaseClass " + selected);
            if (selected)
            {
                ChatManager.Instance.OnMessageSelected (mChatMessage);
            }
            else
            {
                ChatManager.Instance.OnMessageDeselected (mChatMessage);
            }
        }

        public string LoadFilePath (FolderLocation location)
        {
            string filePath = StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), location, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            if (isOwner)
            {
                if (mChatMessage.fileName.Equals(Path.GetFileName (filePath)) == false)
                {
//                    Debug.Log (mChatMessage.fileUrl);
//                    Debug.Log (mChatMessage.fileName);
                    if (File.Exists(StreamManager.LoadFilePath (mChatMessage.fileName, location, isOwner)) && 
                        File.Exists(filePath))
                    {
//                        Debug.Log ("DELETE");
                        File.Delete (StreamManager.LoadFilePath (mChatMessage.fileName, location, isOwner));
                    }
                    else
                    {
                        if (File.Exists (filePath))
                            DownloadManager.Instance.DownLoadFile (mChatMessage.fileUrl, location, null, isOwner, Path.GetExtension (mChatMessage.fileName));
                    }
                }
                else
                {
//                    Debug.Log (mChatMessage.fileUrl);
//                    Debug.Log (mChatMessage.fileName);
                }
            }


            if (File.Exists (filePath) == false)
            {
                if (isOwner)
                {
                    if (File.Exists(StreamManager.LoadFilePath (Path.GetFileName (mChatMessage.fileName), location, isOwner)))
                    {
                        filePath = StreamManager.LoadFilePath (Path.GetFileName (mChatMessage.fileName), location, isOwner);
                    }
                }
            }

//            Debug.Log (filePath);
            return filePath;
        }
    }
}
