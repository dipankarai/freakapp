﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class SystemMessageBox : MessageBoxBase
    {
        public Text timeText;
        public Text messageText;

        // Use this for initialization
        void Start () {

        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData message)
        {
            messageBoxType = MessageBoxType.System;
            base.InitMessageInfo(message);

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            timeText.text = FreakChatUtility.GetTime(mChatMessage.createdAt);

            if (mChatMessage.normalMessagerMetaData != null)
            {
                messageText.text = mChatMessage.normalMessagerMetaData.message;
            }
            else
            {
                if (mChatMessage.messageCategory == ChatMessage.MessageCategory.UserBlocked)
                {
                    messageText.text = mChatMessage.messageSender.nickname + " is blocked! \n Unblock to chat!";
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.ChannelInvite){
                    messageText.text = "SYSTEM MESSAGE CHANNEL INVITE";
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.ChannelJoin){
                    messageText.text = "SYSTEM MESSAGE CHANNEL JOIN";
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.ChannelLeave){
                    messageText.text = "SYSTEM MESSAGE CHANNEL LEAVE";
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.None){
                    Debug.Log("SYSTEM MESSAGE NONE");
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.TooManyMsg){
                    messageText.text = "SYSTEM MESSAGE TOO MANY MESSSAGE";
                }
                else if (mChatMessage.messageCategory == ChatMessage.MessageCategory.UserDeactivated){
                    messageText.text = "SYSTEM MESSAGE USER DEACTIVATED";
                }
            }
        }
    }
}
