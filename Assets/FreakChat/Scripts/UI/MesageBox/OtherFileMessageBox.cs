﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class OtherFileMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;

        // Use this for initialization
        void Start () {

        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData data)
        {
            messageBoxType = MessageBoxType.Others;
            base.InitMessageInfo(data);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }
            
            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

			SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

            string filename = System.IO.Path.GetFileName(mChatMessage.fileUrl);
            currentSideBox.otherFileText.text = "FILE TYPE: " + Path.GetExtension (mChatMessage.fileName) + " \nFILENAME: " + filename;

			ShowFileType (filename);

            string filePath = LoadFilePath (FolderLocation.Contact);//StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Others, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            if (System.IO.File.Exists(filePath))
            {
                FileSavedCallback(true);
            }
            else
            {
                base.DownloadFile(FolderLocation.Others);
            }
        }

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
            base.DownloadFile (FolderLocation.Others);
        }

        public override void ShowProfileImage(string imageUrl)
        {
            base.ShowProfileImage(imageUrl);
        }

        public override void DownloadProgress (float progress)
        {
            base.DownloadProgress(progress);
        }

        public override void FileSavedCallback (bool success)
        {
            string filePath = LoadFilePath (FolderLocation.Contact);//StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Others, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            base.FileSavedCallback(success);
        }

		void ShowFileType (string path)
		{
			//Debug.Log("FilePathCallback " + path);

			if (System.IO.File.Exists(path))
			{
//				string _extension = System.IO.Path.GetExtension(path);
			}
			else
			{
				//Debug.LogError("File Doesn't exists " + path);
			}
		}

        void OpenFile ()
        {
            string filePath = LoadFilePath (FolderLocation.Others);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            if (File.Exists(filePath))
            {
                #if UNITY_EDITOR
                Debug.Log("OpenFile " + filePath);
                #elif UNITY_ANDROID
                ImageAndVideoPicker.AndroidPicker.OpenApplication(filePath);           
                #endif
            }
            else
            {
                base.DownloadFile(FolderLocation.Others);
                Debug.LogError("File not exits---->>>" + filePath);
            }
        }

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            OpenFile();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }
    }
}
