﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class AudioMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;

		private bool isAudioPause = false;
		private bool isAudioPlaying = false;

		private AudioSource audioSource;

		private bool waitForLoad = true;
        private IEnumerator updateRoutine;

        void Awake ()
        {
            audioSource = gameObject.GetComponent<AudioSource> ();
        }

        void OnDisable ()
        {
            playRecordAudioRoutine = null;
            audioSource.clip = null;
        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData message)
        {
            messageBoxType = MessageBoxType.Audio;
            base.InitMessageInfo(message);
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("Message"+ message);
            #endif

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

			SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);
//            currentSideBox.audipPlayButton.onClick.AddListener(OnClickPlayAudio);

			if (mChatMessage.fileType == ChatFileType.FileType.AudioBuzz)
            {
                LoadAudoBuzz();
            }
            else
            {
                string filePath = LoadFilePath (FolderLocation.Audio);// StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Audio, isOwner);

                if (System.IO.File.Exists(filePath))
                {
                    FileSavedCallback(true);
                }
                else
                {
//                    Debug.Log ("No File---->->-=> " + Path.GetFileName (filePath));
                    base.DownloadFile(FolderLocation.Audio);
                }
            }
		}

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
            base.DownloadFile (FolderLocation.Audio);
        }

        public override void ShowProfileImage(string imageUrl)
        {
            base.ShowProfileImage(imageUrl);
        }

        public override void DownloadProgress (float progress)
        {
            base.DownloadProgress(progress);
        }

        void LoadAudoBuzz ()
        {
            if (mChatMessage.audioClip == null)
            {
                if (System.IO.Path.HasExtension(mChatMessage.filePath))
                    mChatMessage.filePath = System.IO.Path.GetFileNameWithoutExtension(mChatMessage.filePath);
                
                if (mChatMessage.filePath.Contains("Audio/Buzz/") == false)
                {
                    mChatMessage.filePath = "Audio/Buzz/" + mChatMessage.filePath;
                }
                
                mChatMessage.audioClip = Resources.Load(mChatMessage.filePath) as AudioClip;
                
                if (mChatMessage.audioClip == null) {
                    Debug.LogError ("Connot find file in Resources/" + mChatMessage.filePath);
                } else {
                    waitForLoad = false;
                }
            }
            else
            {
                waitForLoad = false;
            }

            mChatMessage.messageState = ChatMessage.MessageState.Normal;
            base.CheckMessageState ();
        }

        public override void FileSavedCallback (bool success)
        {
            /*if (mChatMessage != null && mChatMessage.audioClip != null)
            {
                audioSource.clip = mChatMessage.audioClip;
            }
            else
            {
                LoadAudioFromLocalInvoke();
            }*/

            base.FileSavedCallback(success);
        }

        void LoadAudioFromLocalInvoke ()
        {
            string filePath = LoadFilePath (FolderLocation.Audio);// StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Audio, isOwner);

            if (this.gameObject.activeInHierarchy)
            {
                if (File.Exists(filePath))
                {
//                    playRecordAudioRoutine = StartCoroutine(LoadAudioFromLocal(filePath));
                }
            }
            else
            {
                Invoke("LoadAudioFromLocalInvoke", 0.5f);
            }
        }

        IEnumerator LoadAudioFromLocal (string filePath)
        {
            mChatMessage.messageState = ChatMessage.MessageState.Loading;
            base.CheckMessageState ();

            WWW audioFile = new WWW("file://" + filePath);

            while (audioFile.isDone == false)
            {
                yield return null;
            }

            yield return audioFile;

            if (string.IsNullOrEmpty(audioFile.error))
            {
                audioSource.clip = audioFile.audioClip;
//                mChatMessage.audioClip = audioFile.audioClip;
            }
            else
            {
                Debug.LogError("Error Loading Audio Record File: " + filePath);
            }

            Debug.Log("AUDIO DONE");
            waitForLoad = false;
            mChatMessage.messageState = ChatMessage.MessageState.Normal;
            base.CheckMessageState ();
            playRecordAudioRoutine = null;

            OnClickPlayAudio ();
        }

        Coroutine playRecordAudioRoutine;
        public void OnClickPlayAudio ()
        {
            Debug.Log ("Audio game object name--->>>"+ gameObject.name);

            string filePath = LoadFilePath (FolderLocation.Audio);
            if (File.Exists(filePath) == false)
                return;
            
            if (audioSource.clip == null)
            {
                if (playRecordAudioRoutine == null)
                {
                    playRecordAudioRoutine = StartCoroutine(LoadAudioFromLocal(filePath));
                }
                return;
            }

            isAudioPause = false;
			audioSource.Play ();
			isAudioPlaying = true;

            ResetAudioValue();
        }

        void Update ()
		{
            if (isAudioPlaying && audioSource.clip != null)
            {
                if (isAudioPlaying)
                {
                    SetAudioSlider();
                }
                else
                {
                    SetAudioSourceTimer();
                }
                
                float timeLeft = audioSource.clip.length - audioSource.time;
                if (timeLeft < 0.005f)
                {
                    isAudioPlaying = false;
                    ResetAudioValue();
                }
            }
		}

        void ResetAudioValue ()
        {
            if (currentSideBox != null)
            {
                if (isAudioPlaying)
                {
                    currentSideBox.audioSlider.maxValue = audioSource.clip.length;
                    currentSideBox.audioSlider.direction = Slider.Direction.LeftToRight;
                }

                currentSideBox.audipPlayButton.gameObject.SetActive(isAudioPlaying == false);
                currentSideBox.audioPauseButton.gameObject.SetActive(isAudioPlaying == true);

                currentSideBox.audioSlider.value = 0;
            }

        }

        void SetAudioSlider ()
        {
            if (currentSideBox != null && currentSideBox.audioSlider != null)
            {
                currentSideBox.audioSlider.value = audioSource.time;
            }
        }

        void SetAudioSourceTimer ()
        {
            if (currentSideBox != null && currentSideBox.audioSlider != null)
            {
                audioSource.time = currentSideBox.audioSlider.value;
            }
        }

		public void OnPauseAudio()
		{
			isAudioPause = true;
			isAudioPlaying = false;
			audioSource.Pause ();

            if (currentSideBox != null)
            {
                currentSideBox.audipPlayButton.gameObject.SetActive(isAudioPause == true);
                currentSideBox.audioPauseButton.gameObject.SetActive(isAudioPause == false);
            }
		}

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
//            OnClickPlayAudio ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            //base.OnClickMessageButtonHold (selected);
        }
    }
}
