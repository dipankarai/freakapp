﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using FreakGame;
using System.Collections.Generic;

namespace Freak.Chat
{
	public class AcceptOrRejectChalleng : MessageBoxBase
	{
		public MessageUIComponent leftBoxUI;
		public MessageUIComponent rightBoxUI;

		private float mainTextContentSize;
		private string gameID;

//		public GameObject ButtonGroup;
		// Use this for initialization

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData data)
		{
			messageBoxType = MessageBoxType.Text;
			base.InitMessageInfo(data);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);

            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
		}

		public override void InitMessage()
		{
			base.InitMessage();

			currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

			SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

			currentSideBox.htmlTextButton.gameObject.SetActive(false);
            currentSideBox.messageText.text = string.Empty;

			EnterMessages();
		}

		void EnterMessages()
		{
			if (this.gameObject.activeInHierarchy == false)
			{
				Invoke("EnterMessages", 0.1f);
				return;
			}
		
			if (messageSide == MessageSide.LeftSide)
			{
				currentSideBox = leftBoxUI;
				currentSideBox.messageText.text = mChatMessage.message;
			}
			else
			{
				currentSideBox = rightBoxUI;
				currentSideBox.messageText.text = "You have sent a challege";
			}
		
            /*FreakChatSerializeClass.NormalMessagerMetaData _metaData;
			_metaData = (FreakChatSerializeClass.NormalMessagerMetaData)
				Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(mChatMessage.data);*/
			
            gameID = mChatMessage.normalMessagerMetaData.gameId.ToString();

            ShowImage();
		}

        void ShowImage ()
        {
            if (string.IsNullOrEmpty(mChatMessage.normalMessagerMetaData.gameStoreInfo.description_imagePath) || mChatMessage.normalMessagerMetaData.gameStoreInfo.description_imagePath == "0")
            {
                return;
            }

            string _filename = System.IO.Path.GetFileName(mChatMessage.normalMessagerMetaData.gameStoreInfo.description_imagePath);
            string _path = Freak.StreamManager.LoadFilePath(_filename, FolderLocation.Games, false);

            if (System.IO.File.Exists(_path))
            {
                Debug.Log(_path);
                if (currentSideBox != null && currentSideBox.gameIcon != null)
                {
                    try
                    {
                        byte[] bytes = System.IO.File.ReadAllBytes (_path);
                        Texture2D texture = new Texture2D (1, 1, TextureFormat.ARGB32, false);// 4, 4
                        texture.LoadImage(bytes);
                        
                        if (texture != null)
                        {
                            currentSideBox.gameIcon.FreakSetSprite(texture);
                        }

                        texture = null;
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError(e.Message);
                    }
                }
            }
            else
            {
                Freak.DownloadManager.Instance.DownloadStoreGameTexture(mChatMessage.normalMessagerMetaData.gameStoreInfo.description_imagePath, mChatMessage.normalMessagerMetaData.gameStoreInfo);
                Invoke("ShowImage", 0.5f);
            }
        }

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
        }

		/// <summary>
		/// Raises the accept challenge clicked event.
		/// </summary>
		public void OnAcceptChallengeClicked()
		{
            if (ChatManager.CheckInternetConnection())
            {
                FreakAppManager.Instance.currentGameData.isGameSelected = true;
                FreakAppManager.Instance.currentGameData.is_single_mode = false;
                FreakAppManager.Instance.currentGameData.selectedStoreGame = mChatMessage.normalMessagerMetaData.gameStoreInfo;
                FreakAppManager.Instance.currentGameData.isChallengeAccepted = true;
                FreakAppManager.Instance.currentGameData.game_id = int.Parse(gameID);

                FreakAppManager.Instance.currentGameData.opponent_id = mChatMessage.messageSender.userId;
                FreakAppManager.Instance.currentGameData.opponent_name = mChatMessage.messageSender.nickname;
                FreakAppManager.Instance.currentGameData.opponent_profile_url = mChatMessage.messageSender.profileUrl;

                chatWindowUI.loadingObject.DisplayLoading();
                chatWindowUI.InvokePlayGame(mChatMessage.normalMessagerMetaData.gameStoreInfo.title, mChatMessage.normalMessagerMetaData.gameStoreInfo.inventory_id);
                chatWindowUI.DeleteMessage(mChatMessage.messageId);
//                Invoke("InvokeLoadGame", 0.5f);
            }
        }

        void InvokeLoadGame ()
        {
            
            FreakAppManager.Instance.PlayGame(mChatMessage.normalMessagerMetaData.gameStoreInfo.title, mChatMessage.normalMessagerMetaData.gameStoreInfo.inventory_id);
        }

		/// <summary>
		/// Raises the reject challenge clicked event.
		/// </summary>
		public void OnRejectChallengeClicked()
		{
            chatWindowUI.DeleteMessage (mChatMessage.messageId);
//			ButtonGroup.SetActive (false);
		}

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }
	}
}

