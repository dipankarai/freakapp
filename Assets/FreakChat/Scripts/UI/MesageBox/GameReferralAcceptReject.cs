﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using FreakGame;
using System.Collections.Generic;

namespace Freak.Chat
{
	public class GameReferralAcceptReject : MessageBoxBase
	{
		public MessageUIComponent leftBoxUI;
		public MessageUIComponent rightBoxUI;

		private float mainTextContentSize;
        private string referralId;

//		public GameObject ButtonGroup;
		// Use this for initialization
		void Start () {

		}

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData data)
		{
			messageBoxType = MessageBoxType.Text;
			base.InitMessageInfo(data);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
        }

        public override void InitMessage()
        {
			base.InitMessage();

			currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

			SetTimerPanelWidth ();

            ShowProfileImage(mChatMessage.messageSender.profileUrl);
			currentSideBox.htmlTextButton.gameObject.SetActive(false);
            currentSideBox.messageText.text = string.Empty;

			EnterMessages();
		}

		void EnterMessages()
		{
			if (this.gameObject.activeInHierarchy == false)
			{
				Invoke("EnterMessages", 0.1f);
				return;
			}

			if (messageSide == MessageSide.LeftSide)
			{
				currentSideBox = leftBoxUI;
				currentSideBox.messageText.text = mChatMessage.message;
			}
			else
			{
				currentSideBox = rightBoxUI;
				currentSideBox.messageText.text = "You have sent a game referral";
			}

            /*FreakChatSerializeClass.NormalMessagerMetaData _metaData;
			_metaData = (FreakChatSerializeClass.NormalMessagerMetaData)
				Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(mChatMessage.data);*/

            referralId =mChatMessage.normalMessagerMetaData.referralId;

			//			if(mChatMessage.normalMessagerMetaData == null || mChatMessage.normalMessagerMetaData.messageType == ChatMessage.MetaMessageType.None)
			//			{
			//				
			//			}
		}

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
        }

		/// <summary>
		/// Raises the accept challenge clicked event.
		/// </summary>
		public void OnGameReferralAcceptClicked()
		{
            if (ChatManager.CheckInternetConnection())
            {
                FindObjectOfType<StoreInventory> ().AcceptReferralAPICall (referralId);
                chatWindowUI.DeleteMessage (mChatMessage.messageId);
            }
		}

		/// <summary>
		/// Raises the reject challenge clicked event.
		/// </summary>
		public void OnClickGameReferralRejectClicked()
		{
            chatWindowUI.DeleteMessage (mChatMessage.messageId);
		}

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }
	}
}

