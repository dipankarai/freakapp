﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using Tacticsoft;
using ImageAndVideoPicker;

namespace Freak.Chat
{
    public class ImageMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;
        private Sprite defaultSprite;

        void Start () {
        }

        void OnDisable ()
        {
            if (currentSideBox != null && currentSideBox.messageRawImage != null && chatWindowUI.defaultImageMessageThumbnail != null)
            {
                if (currentSideBox.messageRawImage.texture == null)
                {
                    currentSideBox.messageRawImage.texture = chatWindowUI.defaultImageMessageThumbnail;
                    currentSideBox.messageRawImage.PreserveAspectToParent();
                }
            }
        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData message)
        {
            messageBoxType = MessageBoxType.Image;
            base.InitMessageInfo(message);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);

            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();

            /*if (currentSideBox != null && currentSideBox.messageImage != null)
            {
                if (defaultSprite == null)
                    defaultSprite = currentSideBox.messageImage.sprite;
                else
                    currentSideBox.messageImage.sprite = defaultSprite;
            }*/
        }

        private Image parentBackgorund;
        public override void InitMessage()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("InitMessage");
            #endif

            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

//            if (parentBackgorund == null)
            {
                parentBackgorund = currentSideBox.messageRawImage.transform.parent.parent.GetComponent<Image>();
            }
            Color color = parentBackgorund.color;

            if (mChatMessage.fileType == ChatFileType.FileType.Stickers)
            {
                color.a = 0f;
                parentBackgorund.color = color;
                mChatMessage.messageState = ChatMessage.MessageState.Normal;
                LoadSticker();
            }
            else
            {
                color.a = 1f;
                parentBackgorund.color = color;
                string filePath = LoadFilePath (FolderLocation.Image);//StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Image, isOwner);

//                Debug.Log (filePath);
                if (System.IO.File.Exists(filePath))
                {
                    FileSavedCallback(true);
                }
                else
                {
                    currentSideBox.messageRawImage.texture = chatWindowUI.defaultImageMessageThumbnail;
                    currentSideBox.messageRawImage.PreserveAspectToParent();
                    base.DownloadFile(FolderLocation.Image);
                }
            }

            SetTimerPanelWidth ();
        }

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
            base.DownloadFile (FolderLocation.Image);
        }

        public override void ShowProfileImage(string imageUrl)
        {
            base.ShowProfileImage(imageUrl);
        }

        public override void DownloadProgress (float progress)
        {
            base.DownloadProgress(progress);
        }

        private string mainFilePath;
        public override void FileSavedCallback (bool success)
        {
            if (mChatMessage.fileType == ChatFileType.FileType.Stickers)
                return;

            mainFilePath = LoadFilePath (FolderLocation.Image);
            
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("FileSavedCallback "+mainFilePath);
            #endif

            if (File.Exists(mainFilePath))
            {
                if (currentSideBox.messageRawImage != null)
                {
                    if (mChatMessage.imageMessageCache != null)
                    {
                        currentSideBox.messageRawImage.texture = null;
                        currentSideBox.messageRawImage.texture = mChatMessage.imageMessageCache;
                        currentSideBox.messageRawImage.PreserveAspectToParent();
                    }
                    else
                    {
                        DownloadManager.Instance.LoadImageMessage(mainFilePath, mChatMessage);
                        Invoke("LoadImage", 0.1f);
                    }
                }
                else
                {
                    Texture2D newImage = new Texture2D(4,4);
                    if (mChatMessage.imageByte != null && mChatMessage.imageByte.Length > 0)
                    {
                        newImage.LoadImage(mChatMessage.imageByte);
                        currentSideBox.messageImage.FreakSetSprite(newImage);
                    }
                    else
                    {
                        newImage.LoadImage(Freak.StreamManager.LoadFileDirect (mainFilePath));
                        currentSideBox.messageImage.FreakSetSprite (TextureResize.ResizeTexture(currentSideBox.messageImage.rectTransform, newImage));
                        mChatMessage.imageByte = currentSideBox.messageImage.sprite.texture.EncodeToJPG();
                    }
                    newImage = null;
                }

//                LoadImage ();
                /*try
                {
                    using (var file = new FileStream(mainFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var data = new byte[file.Length];
                        file.Read(data, 0, data.Length);
                        var newImage = new Texture2D(1, 1);
                        newImage.LoadImage(data);

                        Debug.Log(newImage.width +" ::: "+newImage.height);
                        Texture2D cachedImage = TextureResize.ResizeTexture (currentSideBox.messageImage.rectTransform, newImage);
                        Debug.Log(cachedImage.width +" ::: "+cachedImage.height);
                        currentSideBox.messageImage.FreakSetSprite (cachedImage);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                    return;
                }*/
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("File not exits... " + mainFilePath);
                #endif
            }
            base.FileSavedCallback(success);

            /*if (mChatMessage.imageMessageCache == null)
            {
                mChatMessage.messageState = ChatMessage.MessageState.Loading;
                base.CheckMessageState ();
            }*/
        }

//        private Texture2D cachedImage;
        void LoadImage()
        {
            /*if (cachedImage != null)
            {
                currentSideBox.messageImage.FreakSetSprite (cachedImage);
                return;
            }*/

            /*if (this.gameObject.activeInHierarchy)
            {
                StartCoroutine (LoadImageFromLocal (mainFilePath));
            }
            else
            {
                Invoke ("LoadImage", 0.5f);
            }*/

            FileSavedCallback(true);
        }

        IEnumerator LoadImageFromLocal (string path)
        {
//            Debug.Log ("LoadImageFromLocal " + path);

            /*if (File.Exists (GetCompresedImagePath(path)))
            {
                path = GetCompresedImagePath (path);
            }*/

            var www = new WWW("file://" + path);
            yield return www;
            while (!www.isDone)
            {
                yield return null;
            }

            mChatMessage.messageState = ChatMessage.MessageState.Normal;
            base.CheckMessageState ();
            if (string.IsNullOrEmpty(www.error))
            {
                Texture2D cachedImage = TextureResize.ResizeTexture (currentSideBox.messageImage.rectTransform, www.texture);
                currentSideBox.messageImage.FreakSetSprite (cachedImage);
//                mChatMessage.imageMessageCache = cachedImage;

                /*if (path.Contains("compresed_cached") == false)
                {
                    if (Path.GetFileNameWithoutExtension (path).Contains ("png"))
                        SaveCompresedImage (path, cachedImage.EncodeToPNG ());
                    else
                        SaveCompresedImage (path, cachedImage.EncodeToJPG ());
                }*/
            }
            else
            {
                Debug.LogError("url ::: " + path + www.error);
            }

//            Resources.UnloadUnusedAssets ();
        }

        string GetCompresedImagePath (string path)
        {
            string _filename = Path.GetFileName (path);
            string _path = path.Replace (_filename, string.Empty);
            string _compresedPath = _path + "compresed_cached/";

            if (Directory.Exists(_compresedPath) == false)
            {
                Directory.CreateDirectory (_compresedPath);
            }

            string _compresedFilePath = _compresedPath + _filename;

            return _compresedFilePath;
        }

        void SaveCompresedImage (string path, byte[] bytes)
        {
            try
            {
                using (var file = new FileStream(GetCompresedImagePath(path), FileMode.Create, FileAccess.Write))
                {
                    file.Write(bytes, 0, bytes.Length);
                    Debug.Log(file.Name);
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        private string gifFilepath;
        void LoadGIFImage ()
        {
            if (this.gameObject.activeInHierarchy == true)
            {
                if(gifCoroutine != null)
                {
                    StopCoroutine(gifCoroutine);
                }

                currentSideBox.uniGifImage.gameObject.SetActive(true);
                currentSideBox.messageImage.gameObject.SetActive(false);
                gifCoroutine = StartCoroutine(ViewGifCoroutine(gifFilepath));
            }
            else
            {
                Invoke("LoadGIFImage", 1f);
            }
        }

        private Coroutine gifCoroutine;
        private IEnumerator ViewGifCoroutine(string filePath)
        {
            yield return StartCoroutine(currentSideBox.uniGifImage.SetGifFromUrlCoroutine(filePath));
        }
    
        void LoadSticker ()
        {
            base.CheckMessageState ();
            if (currentSideBox.messageRawImage != null)
            {
                currentSideBox.messageRawImage.texture = null;
                currentSideBox.messageRawImage.texture = Resources.Load(mChatMessage.filePath) as Texture;
                currentSideBox.messageRawImage.PreserveAspectToParent();
            }
            else
            {
                if (mChatMessage.imageMessageCache != null)
                {
                    currentSideBox.messageImage.FreakSetSprite (mChatMessage.imageMessageCache);
                    return;
                }
                
                if (System.IO.Path.HasExtension(mChatMessage.filePath))
                {
                    mChatMessage.filePath = System.IO.Path.GetFileNameWithoutExtension(mChatMessage.filePath);
                }
                
                mChatMessage.imageMessageCache = Resources.Load(mChatMessage.filePath) as Texture2D;
                
                if (mChatMessage.imageMessageCache != null)
                {
                    currentSideBox.messageImage.FreakSetSprite (mChatMessage.imageMessageCache);
                }
                else
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("Connot find file in Resources/" + mChatMessage.filePath);
                    #endif
                }
            }
        }

		public void OpenImage()
		{
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Open Image called----------->>>> "+mainFilePath);
            #endif
			//AndroidPicker.ViewImageInGallery (mChatMessage.filePath);

            //UIController.Instance.imageHandler.gameObject.SetActive(true);
            if (mChatMessage.fileType == ChatFileType.FileType.Stickers)
            {
                chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.Sticker);
//                UIController.Instance.imageHandler.sprite = currentSideBox.messageImage.sprite;
            }
            else
            {
                if (string.IsNullOrEmpty(UIController.Instance.imageHandler.filePath))
                {
                    UIController.Instance.imageHandler.filePath = mainFilePath;
//                    UIController.Instance.imageHandler.sprite = currentSideBox.messageImage.sprite;
                    UIController.Instance.imageHandler.texture = currentSideBox.messageRawImage.texture as Texture2D;
                }
                else if (UIController.Instance.imageHandler.filePath.Equals(mainFilePath))
                {
                    UIController.Instance.imageHandler.loadImage = false;
                }
                else
                {
                    UIController.Instance.imageHandler.filePath = mainFilePath;
//                    UIController.Instance.imageHandler.sprite = currentSideBox.messageImage.sprite;
                    UIController.Instance.imageHandler.texture = currentSideBox.messageRawImage.texture  as Texture2D;
                    Resources.UnloadUnusedAssets ();
                }

                FindObjectOfType<HomeScreenUIPanelController> ().PushPanel (HomeScreenUIPanelController.PanelType.ChatImagePreviewPanel);
            }
            /*if (currentSideBox.messageImage.gameObject.activeInHierarchy)
            {
                UIController.Instance.imageHandler.viewPort.sprite = currentSideBox.messageImage.sprite;
            }
            else if (currentSideBox.uniGifImage.gameObject.activeInHierarchy)
            {
                UIController.Instance.imageHandler.LoadGIFImage(gifFilepath);
            }*/
		}

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
            OpenImage ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold " + selected);
            base.OnClickMessageButtonHold (selected);
        }

        void OnDestroy ()
        {
            if (currentSideBox.messageRawImage != null)
            {
                currentSideBox.messageRawImage.texture = null;
            }
        }
    }
}
