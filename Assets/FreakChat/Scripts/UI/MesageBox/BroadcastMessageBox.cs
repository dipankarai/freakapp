﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class BroadcastMessageBox : MessageBoxBase
    {
        public Text timeText;
        public Text messageText;

        // Use this for initialization
        void Start () {

        }

        public override void InitMessageInfo(ChatMessageTableData data)
        {
            messageBoxType = MessageBoxType.Broadcast;
            base.InitMessageInfo(data);

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            timeText.text = FreakChatUtility.GetTime(mChatMessage.createdAt);
            messageText.text = mChatMessage.message;
        }
    }
}
