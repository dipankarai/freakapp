﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Freak.Chat
{
	public class MessageUIComponent : MonoBehaviour
    {
        [Header("COMMON")]
        public Text timeText;
		public Image timeerPanel;
        public Image profileImage;
		public GameObject downloadingProgress;
		public Image ProgressImage;
        public GameObject fileLoadingObject;
        public Button retryButton;

        [Header("TEXT MESSAGE")]
        public Text messageText;
        public RectTransform textHolderPanel;
        public ContentSizeFitter contentSizeFitter;
        public Button htmlTextButton;

		public Image msgReadStatusImage;
		public Sprite sentNotificationSprite;
		public Sprite deliveredNotificationSprite;
		public Sprite readNotificationSprite;

		public GameObject selectedChatHighlightImage;

        [Header("IMAGE MESSAGE")]
        public Image messageImage;
        public RawImage messageRawImage;
        public UniGifImage uniGifImage;

        [Header("AUDIO MESSAGE")]
        public Button audipPlayButton;
		public Button audioPauseButton;
        public Slider audioSlider;

        [Header("VIDEO MESSAGE")]
        public Image videoPanel;
        public Image videoPlayImage;

        [Header("OTHER FILE MESSAGE")]
        public Text otherFileText;

        [Header("CONTACT MESSAGE")]
        public Text contactFileText;

        [Header("AUDIO BUZZ MESSAGE")]
        public Image audioBuzzIconImage;
        public Button audioBuzzPlayButton;

        [Header("GAME CHALLENGE")]
        public Image gameIcon;
    }
}
