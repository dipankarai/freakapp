﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using Tacticsoft;

namespace Freak.Chat
{
    public class VideoMessageBox : MessageBoxBase
    {
        public MessageUIComponent leftBoxUI;
        public MessageUIComponent rightBoxUI;

        // Use this for initialization
        void Start () {

        }

        public override string GetClassType
        {
            get {
                return this.GetType ().Name;
            }
        }

        public override void InitMessageInfo(ChatMessageTableData message)
        {
            messageBoxType = MessageBoxType.Video;
            base.InitMessageInfo(message);

            leftBoxUI.gameObject.SetActive(false);
            rightBoxUI.gameObject.SetActive(false);
            
            if (messageSide == MessageSide.LeftSide)
            {
                currentSideBox = leftBoxUI;
            }
            else
            {
                currentSideBox = rightBoxUI;
            }

            InitMessage();
        }

        public override void InitMessage()
        {
            base.InitMessage();

            currentSideBox.timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

            ShowProfileImage(mChatMessage.messageSender.profileUrl);

            string filePath = LoadFilePath (FolderLocation.Video);// StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Video, isOwner);

            if (System.IO.File.Exists(filePath))
            {
                FileSavedCallback(true);
            }
            else
            {
                base.DownloadFile(FolderLocation.Video);
            }

            SetTimerPanelWidth ();
        }

        public override void DownloadFile ()
        {
            Debug.Log ("DownLoadFile Derived Class");
            base.DownloadFile (FolderLocation.Video);
        }

        public override void ShowProfileImage(string imageUrl)
        {
            base.ShowProfileImage(imageUrl);
        }

        public override void DownloadProgress (float progress)
        {
            base.DownloadProgress(progress);
        }

        public override void FileSavedCallback (bool success)
        {
//            Debug.Log("FileSavedCallback" + success);
            string filePath = LoadFilePath (FolderLocation.Video);//StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Video, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

            if (File.Exists(filePath))
            {
//				if(this.gameObject.activeInHierarchy)
//                    StartCoroutine(LoadVideoFromLocal(filePath));

//                currentSideBox.videoPanel.color = Color.black;
//                currentSideBox.videoPlayImage.gameObject.SetActive(true);
            }

            base.FileSavedCallback(success);
        }

		public void OnVideoButtonClicked()
		{
            string filePath = LoadFilePath (FolderLocation.Video);// StreamManager.LoadFilePath(Path.GetFileName(mChatMessage.fileUrl), FolderLocation.Video, isOwner);

            if (Path.HasExtension (filePath) == false)
                filePath += Path.GetExtension (mChatMessage.fileName);

			if (File.Exists(filePath))
			{
                #if UNITY_EDITOR
                Debug.Log("OpenFile " + filePath);
                #else
                #if UNITY_ANDROID
                ImageAndVideoPicker.AndroidPicker.OpenVideo(filePath);
                #elif UNITY_IOS || UNITY_IPHONE
                Handheld.PlayFullScreenMovie ("file://" + filePath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
                #endif
                #endif
				//StartCoroutine(StartVideoPlay(filePath));
			}
            else
            {
                Debug.LogError("File not exits---->>>" + filePath);
            }
		}

		IEnumerator StartVideoPlay(string filePath){
			WWW videoFile = new WWW("file:///" + filePath);
            ChatManager.Instance.mConnectingUI.DisplayLoading ();

            while (videoFile.isDone == false)
            {
                yield return null;
            }

			yield return videoFile;

            if (string.IsNullOrEmpty(videoFile.error))
			{
				Debug.Log ("videoFile File path---->>>"+"file://" +filePath);
				Handheld.PlayFullScreenMovie ("file://" + filePath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
			}
			else
			{
				Debug.LogError("Error Loading Audio Record File: " + filePath);
			}

            ChatManager.Instance.mConnectingUI.HideLoading ();
		}

        public override void OnClickMessageButtonSingle ()
        {
            Debug.Log ("OnClickMessageButtonSingle");
            //base.OnClickMessageButtonSingle ();
            OnVideoButtonClicked ();
        }

        public override void OnClickMessageButtonHold (bool selected)
        {
            Debug.Log ("OnClickMessageButtonHold BaseClass " + selected);
            base.OnClickMessageButtonHold (selected);
        }
    }
}
