﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Threading;

namespace Freak.Chat
{
    public class AttachmentPanelUI : MonoBehaviour
    {
        [HideInInspector] public ChatWindowUI chatWindowUI;

		private string sendFilepath;
		private ChatFileType.FileType sendFileType;

        void FilePathCallback (string path)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("FilePathCallback " + path);
            #endif

            if (System.IO.File.Exists(path))
            {
                string _extension = System.IO.Path.GetExtension(path);
                ChatFileType.FileType _fileType = ChatFileType.FileType.Others;
                if (FreakChatUtility.IsImageExtension(_extension))
                {
                    _fileType = ChatFileType.FileType.Image;
                }
                else if (FreakChatUtility.IsAudioExtension(_extension))
                {
					_fileType = ChatFileType.FileType.Audio;
                }
                else if (FreakChatUtility.IsVideoExtension(_extension))
                {
                    _fileType = ChatFileType.FileType.Video;
                }
                else
                {
                    _fileType = ChatFileType.FileType.Others;
                }

				sendFilepath = path;
				sendFileType = _fileType;

                SaveFileToSendFolder(path, _fileType);

                chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.CloseAll); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("File uploaded successfully --------------->>>>>>>>>>>>>"+sendFilepath);
                #endif
            }
            else
            {
                Debug.LogError("File Doesn't exists " + path);
            }
        }

        public void OnClickAudioButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection())
            {
                #if UNITY_EDITOR
                string _Path = Application.persistentDataPath + "/TestEditor/Audio/temp.mp3";
                if (File.Exists( _Path))
                FilePathCallback(_Path);
                return;
                #else
                UIController.Instance.GetFile(FilePathCallback,ChatFileType.FileType.Audio);
                #endif
            }
        }

        public void OnClickContactsButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection())
            {
                FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.DisplayContactsToSend);
            }
        }

        public void OnClickFilesButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection())
            {
                #if UNITY_EDITOR
                string _Path = Application.persistentDataPath + "/TestEditor/Others/temp.pdf";
                if (File.Exists( _Path))
                FilePathCallback(_Path);
                return;
                #else
                UIController.Instance.GetFile(FilePathCallback,ChatFileType.FileType.Others);
                #endif
            }
        }

        public void OnClickGalleryButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection())
            {
                #if UNITY_EDITOR
                string _Path = Application.persistentDataPath + "/TestEditor/Image/temp.jpg";
                if (File.Exists( _Path))
                FilePathCallback(_Path);
                return;
                #else
                UIController.Instance.GetFile(FilePathCallback,ChatFileType.FileType.Image);
                #endif
            }
        }

        public void OnClickTakePhotoButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection ())
            {
                #if UNITY_EDITOR
                string _Path = Application.persistentDataPath + "/TestEditor/Camera/temp.jpg";
                if (File.Exists (_Path))
                    FilePathCallback (_Path);
                return;
                #endif

                #if UNITY_ANDROID
                UIController.Instance.GetFile(FilePathCallback,ChatFileType.FileType.Camera);
                #elif UNITY_IPHONE || UNITY_IOS
                Reign.StreamManager.LoadCameraPicker(Reign.CameraQuality.Med, 512, 512, CameraImageCallback);
                #endif
            }
        }

        public void OnClickVideoButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickButton");
            #endif
//            if (ChatManager.CheckInternetConnection())
            {
                #if UNITY_EDITOR
                string _Path = Application.persistentDataPath + "/TestEditor/Video/temp.mp4";
                if (File.Exists( _Path))
                {
                    FilePathCallback(_Path);
                }
                return;
                #else
                UIController.Instance.GetFile(FilePathCallback,ChatFileType.FileType.Video);
                #endif
            }
        }

//        void SendFile (string filepath, string fileName, ChatFileType.FileType type)
//        {
//            ChatFileType fileType = new ChatFileType(filepath, type);
////			ChatManager.Instance.UploadFileToSendBird(fileType, FileUploadToSBCallback);
//        }

        #if UNITY_IPHONE || UNITY_IOS
        void CameraImageCallback(Stream stream, bool succeeded)
        {
            if (!succeeded)
            {
                if (stream != null) stream.Dispose();
                return;
            }

            try
            {
                var data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
               
                string fileName = "Cam_"+DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".png";
                newSavedFilePath = StreamManager.LoadFilePath (fileName, FolderLocation.Image, true);
                Freak.StreamManager.SaveFile (newSavedFilePath, data, FolderLocation.Image, FileSavedCallback, true);
                Debug.Log(newSavedFilePath + File.Exists(newSavedFilePath));

//                string filepath = FreakChatUtility.SaveSendFileGetPath(fileName, data, FreakChatUtility.FolderType.Image);

//                SendFile(filepath, fileName, ChatFileType.FileType.Image);
//                SaveFileToSendFolder(filepath, ChatFileType.FileType.Image);

//                var newImage = new Texture2D(4, 4);
//                newImage.LoadImage(data);
//                newImage.Apply();

//                ReignImage.sprite = Sprite.Create(newImage, new Rect(0, 0, newImage.width, newImage.height), new Vector2(.5f, .5f));
            }
            catch (Exception e)
            {
               // MessageBoxManager.Show("Error", e.Message);
                Debug.Log("Camera image field Error "+e.Message);
            }
            finally
            {
                // NOTE: Make sure you dispose of this stream !!!
                if (stream != null) stream.Dispose();
            }
        }
		#endif

        private string newSavedFilePath;

        private void SaveFileToSendFolder(string filePath, ChatFileType.FileType fileType)
        {
            string _filename = FreakChatUtility.FileNameFromUrl(FreakChatUtility.GetCurrentTimeStamp(), filePath);
            Debug.Log(_filename);
            var bytes = File.ReadAllBytes(filePath);

            if (fileType == ChatFileType.FileType.Camera || fileType == ChatFileType.FileType.Image)
            {
                Texture2D texture = new Texture2D(1, 1);
                texture.LoadImage(bytes);

                if (texture.width > 1000 || texture.height > 1000)
                {
                    //                        texture.Compress(false);
                    texture = TextureResize.ResizeTexture(1000f, 1000f, texture);
                    bytes = texture.EncodeToPNG();
                }
            }

            newSavedFilePath = StreamManager.LoadFilePath(_filename, ChatFileType.GetFolderLocation(fileType), true);

            Debug.Log(newSavedFilePath + File.Exists(newSavedFilePath));

            StreamManager.SaveFile(_filename, bytes, ChatFileType.GetFolderLocation(fileType), FileSavedCallback, true);
        }

        void FileSavedCallback (bool success)
        {
            Debug.Log("SentCallback " + success);
            if (success)
            {
                ChatFileType chatFileToSend = new ChatFileType(newSavedFilePath, sendFileType);
                chatWindowUI.DuplicateMessageBox (chatFileToSend, string.Empty);
//                ChatManager.Instance.fileSendCallback = UIController.Instance.PlaySendMessageSound;
//                ChatManager.Instance.SendFileMessage(chatFileToSend, "");
            }
        }
    }
}
