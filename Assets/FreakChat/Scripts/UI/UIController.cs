﻿using UnityEngine;
using System.Collections;
using ImageAndVideoPicker;
using System.IO;
using System;

namespace Freak.Chat
{
    /// <summary>
    /// UIController class controls all the Canvas UI
    /// for switching from screen to screen.
    /// </summary>
    public class UIController : MonoBehaviour
    {
        private static UIController instance;
        public static UIController Instance
        {
            get { return instance; }
        }

        public References assetsReferences
        {
            get 
            {
                return references;//(References)Resources.Load<References>("References");
            }
        }

        public enum ChatUIState
        {
            None,
            Mainmenu,
            AllChats,
            Chat
        }

        public Transform parent;
        private GameObject currentObject;
        private ChatUIState currentState = ChatUIState.None;
		public ImageHandler imageHandler;
		public GameObject loadingGamePlay;
		public GameObject mPinPanel;
        public ConnectingUI connectingUI;
        public ChatWindowUI chatWindowUI;
        public Sprite selectedImage;
        public Sprite[] messageSpriteType;
        private References references;

        /// <summary>
        /// Initializes its instance.
        /// </summary>
        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }

            if (parent == null)
            {
                Debug.LogError("ASSIGN PARENT Canvas transform !!!");
            }

            /*if (assetsReferences.LoginDummy != null)
                Debug.Log ("NULL");*/

            ChatManager.Instance.mConnectingUI = connectingUI;

            GameObject go = Instantiate(Resources.Load("References")) as GameObject;
            go.SetActive(false);
            go.name = "References";
            references = go.GetComponent<References>();
        }

        // Use this for initialization
        void Start () {
        }

        private AudioSource mAudioSource;
        public AudioSource GetAudioSource ()
        {
            if (mAudioSource == null)
            {
                mAudioSource = gameObject.AddComponent<AudioSource>();
                mAudioSource.playOnAwake = false;
            }

            return mAudioSource;
        }

        public void PlaySendMessageSound ()
        {
            if (FreakPlayerPrefs.MuteAllChat == false)
            {
                if (Instance.GetAudioSource().clip == null || Instance.GetAudioSource().clip.name != Instance.assetsReferences.messageSound.name)
                {
                    Instance.GetAudioSource().clip = Instance.assetsReferences.messageSound;
                }

                if (Instance.GetAudioSource().isPlaying == false)
                {
                    Instance.GetAudioSource().Play();
                }
            }
        }

        public void PlayReceiveMessageSound ()
        {
            if (FreakPlayerPrefs.MuteAllChat == false)
            {
                if (Instance.GetAudioSource().clip == null || Instance.GetAudioSource().clip.name != Instance.assetsReferences.notificationSound.name)
                {
                    Instance.GetAudioSource().clip = Instance.assetsReferences.notificationSound;
                }

                if (Instance.GetAudioSource().isPlaying == false)
                {
                    Instance.GetAudioSource().Play();
                }
            }
        }

        System.Action<string> filePickerCallback;
        public void FilePickerCallback (string filePath)
        {
			Debug.Log ("Calling Call Back");
            if (filePickerCallback != null)
            {
				Debug.Log (" 1111111111111111 Calling Call Back");
                filePickerCallback(filePath);
				filePickerCallback = null;
            }
        }


		public void GetProfileImage(System.Action<string> callback, int type)
		{
            
        #if !UNITY_EDITOR
			filePickerCallback = callback;
            ChatManager.Instance.isFilePickerBackground = true;
			#if UNITY_ANDROID
			switch(type)
			{
			case 1:
				//FileBridge.PickImage(1); //Commnicate filebride class to get the file 
				AndroidPicker.BrowseImage(false);
				break;
			
			case 2:
				//FileBridge.PickCamera(1);
				AndroidPicker.CaptureImage();
				break;
			}
			#elif UNITY_IPHONE || UNITY_IOS
			Debug.LogError("FileLoadButtonClicked for iOS not set");
			switch(type)
			{
				case 1:
				IOSPicker.BrowseImage(false);
				break;
	
				case 2:
				Reign.StreamManager.LoadCameraPicker(Reign.CameraQuality.Med, 512, 512, CameraImageCallback);
				break;
			}
            #endif
		#endif
		}

        #if UNITY_EDITOR
        void FilePickerEditor()
        {
            string tempFilePAth = Application.persistentDataPath + "/temp.jpg";
            if (filePickerCallback != null)
            {
                if (File.Exists(tempFilePAth))
                {
                    filePickerCallback(tempFilePAth);
                    filePickerCallback = null;
                }
                else
                {
                    Debug.Log("Temp file doesn't exists: " + tempFilePAth);
                }
            }
        }
        #endif
			

		public void GetFile(System.Action<string> callback, ChatFileType.FileType type)
        {
            filePickerCallback = callback;

#if UNITY_EDITOR
            FilePickerEditor();
#else
            #if UNITY_ANDROID
            //            ChatManager.Instance.isFilePickerBackground = true;
            switch(type)
            {
            case ChatFileType.FileType.Others:
            AndroidPicker.BrowseFile();
            break;
            
            case ChatFileType.FileType.Video:
            AndroidPicker.BrowseVideo();
            break;
            
            case ChatFileType.FileType.Audio:
            AndroidPicker.BrowseForAudio();
            break;
            
            case ChatFileType.FileType.Image:
            AndroidPicker.BrowseImage(false);
            break;
            
            case ChatFileType.FileType.Camera:
            AndroidPicker.CaptureImage();
            break;
            
            default:
            break;
            }
            #endif

            #if UNITY_IPHONE || UNITY_IOS
            Debug.LogError("FileLoadButtonClicked for iOS not set");
            switch(type)
            {
            case ChatFileType.FileType.Others:
            IOSFileController.FilePickIOS();
            break;
            
            case ChatFileType.FileType.Video:
            IOSPicker.BrowseVideo();
            break;
            
            case ChatFileType.FileType.Audio:
            IOSFileController.FilePickIOS();
            break;
            
            case ChatFileType.FileType.Image:
            IOSPicker.BrowseImage(false);
            break;
            
            case ChatFileType.FileType.Contact:
            IOSPicker.BrowseContact();
            break;
            case ChatFileType.FileType.Camera:
            break;
            }
            #endif
#endif
        }

		#if UNITY_IPHONE || UNITY_IOS
		void CameraImageCallback(Stream stream, bool succeeded)
		{
		if (!succeeded)
		{
		if (stream != null) stream.Dispose();
		return;
		}

		try
		{
		var data = new byte[stream.Length];
		stream.Read(data, 0, data.Length);

		string fileName = "Cam_"+DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".png";
		string filepath = FreakChatUtility.SaveSendFileGetPath(fileName, data, FreakChatUtility.FolderType.Image);

		FilePickerCallback(filepath);
		//var newImage = new Texture2D(4, 4);
		//newImage.LoadImage(data);
		//newImage.Apply();

		//ReignImage.sprite = Sprite.Create(newImage, new Rect(0, 0, newImage.width, newImage.height), new Vector2(.5f, .5f));
		}
		catch (Exception e)
		{
		// MessageBoxManager.Show("Error", e.Message);
		Debug.Log("Camera image field Error "+e.Message);
		}
		finally
		{
		// NOTE: Make sure you dispose of this stream !!!
		if (stream != null) stream.Dispose();
		}
		}
		#endif

    }
}
