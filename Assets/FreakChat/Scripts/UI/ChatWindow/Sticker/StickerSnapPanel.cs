﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace Freak.Chat
{
    public class StickerSnapPanel : MonoBehaviour
    {
        public ScrollRect scrollRect;
        public StickerPanelUI stickerPanelUI;

        private int mCategoryId;
        private string mCategoryName;
        private AssetsRef.Stickers mStickers;

        void OnEnable () {
//            StartCoroutine(LoadStickerAsy());
        }
        
        public void InitStickerSnapPanel (int id, AssetsRef.Stickers stickers)
        {
            mCategoryId = id;
            mCategoryName = stickers.name;
            mStickers = stickers;
            float _time = (float)id;
            _time = _time / 10f;
            Invoke("LoadStickerAsync", _time);
            /*for (int i = 0; i < stickers.stickersList.Count; i++)
            {
                string _pathName = "Stickers/" + mCategoryName + "/" + stickers.stickersList [i].name;
                GameObject go = new GameObject (_pathName);

                go.transform.SetParent (scrollRect.content.transform, false);
                Image _img = go.AddComponent<Image> ();
                _img.sprite = stickers.stickersList [i];
                _img.preserveAspect = true;
                _img.raycastTarget = true;

                Button _button = go.AddComponent<Button> ();
                _button.onClick.AddListener (() => OnClickSticker (_button));
            }*/
        }

        void LoadStickerAsync ()
        {
            string resourcePath = "Stickers/" + mCategoryName;
            Sprite[] _sprites = Resources.LoadAll<Sprite>(resourcePath);
            for (int i = 0; i < _sprites.Length; i++)
            {
                string _pathName = "Stickers/" + mCategoryName + "/" + _sprites [i].name;
                GameObject go = new GameObject (_pathName);

                go.transform.SetParent (scrollRect.content.transform, false);
                Image _img = go.AddComponent<Image> ();
                _img.sprite = _sprites [i];
                _img.preserveAspect = true;
                _img.raycastTarget = true;

                Button _button = go.AddComponent<Button> ();
                _button.onClick.AddListener (() => OnClickSticker (_button));
            }
        }

        IEnumerator LoadStickerAsy ()
        {
            for (int i = 0; i < mStickers.stickersList.Count; i++)
            {
                string _pathName = "Stickers/" + mCategoryName + "/" + mStickers.stickersList [i].name;
                GameObject go = new GameObject (_pathName);

                go.transform.SetParent (scrollRect.content.transform, false);
                Image _img = go.AddComponent<Image> ();
                _img.sprite = mStickers.stickersList [i];
                _img.preserveAspect = true;
                _img.raycastTarget = true;

                Button _button = go.AddComponent<Button> ();
                _button.onClick.AddListener (() => OnClickSticker (_button));

                yield return new WaitForEndOfFrame();
            }
        }

        public void OnClickSticker (Button obj)
        {
//            Debug.Log (obj.gameObject.name);
            stickerPanelUI.OnClickSticker (obj);
        }
    }
}
