﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Freak.Chat
{
    public class StickerToggleButton : MonoBehaviour
    {
        [SerializeField] private StickerPanelUI stickerPanelUI;
        [SerializeField] private Toggle toggleButton;
        public int categoryID;
        public Image image, toogleOnImage, toggleOffImage;
        public string stickerName;

        void Awake ()
        {
            if (stickerPanelUI == null)
            {
                Debug.LogError("StickerPanelUI is not attached");
            }

            if (toggleButton == null)
            {
                toggleButton = GetComponent<Toggle>();
            }
        }

        // Use this for initialization
        void Start () {
            if (categoryID == 0)
            {
                stickerPanelUI.OnClickCategory(categoryID);
            }
        }
        
        public void OnValueChanged (bool isOn)
        {
            Debug.Log(this.gameObject.name + "OnValueChanged " + isOn);

            if (isOn)
            {
                stickerPanelUI.OnClickCategory(categoryID);
            }

            /*toogleOnImage.gameObject.SetActive(isOn == true);
            toggleOffImage.gameObject.SetActive(isOn == false);*/
        }

        public void OnSwipe (int screenId)
        {
            toogleOnImage.gameObject.SetActive(categoryID == screenId);
            toggleOffImage.gameObject.SetActive(categoryID != screenId);
        }
    }
}
