﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class StickerPanelUI : MonoBehaviour
    {
        [HideInInspector] public ChatWindowUI chatWindowUI;
        [SerializeField] private ScrollRect stickerCategoryScrollRect, stickerContentScrollRect, downloadStickerScrollRect, horizontalScrollSnap;
        [SerializeField] private GameObject downloadStickerPanel;
        [SerializeField] private Button downloadStickerButton;
        [SerializeField] private Text stickerNameText;
        [SerializeField] private StickerToggleButton mStickerToggleButton;

        public StickerSnapPanel stickerSnapPanel;
        public List<StoreStickers> inventoryStickersList = new List<StoreStickers>();
        private List<GameObject> stickerObjectList;
        private ImageController imageController;
        private string stickerType = "1";
        private HorizontalScrollSnap mHorizontalScrollSnap;

		int downloadedStickercatId = 0;

        void Awake ()
		{
			//FindObjectOfType<StoreInventory> ().GetStorePurchasedListAPICall (stickerType);
            if (horizontalScrollSnap != null)
            {
                mHorizontalScrollSnap = horizontalScrollSnap.GetComponent<HorizontalScrollSnap> ();
            }
        }

		void Start()
		{
            InstantiateCategory ();

            /*downloadedStickercatId = 0;
			imageController = FindObjectOfType<ImageController> (); 
            if (FindObjectOfType<StoreInventory> () != null)
			    FindObjectOfType<StoreInventory> ().GetStorePurchasedListAPICall (stickerType, EnableTableView);*/
		}

		public void EnableTableView(List<object> stickersList)
		{
			inventoryStickersList.Clear ();
			for (int i = 0; i < stickersList.Count; i++) 
			{
				Dictionary<string,object> data = (Dictionary<string,object>)stickersList [i];
				StoreStickers storeStickers = new StoreStickers (data);
				for(int j = 0; j < storeStickers.stickersList.Count; j++)
				{
					//Sprite spr = new Sprite ();
					//storeStickers.Stickerimages.Add (spr); 
					//imageController.GetSpriteFromLocalOrDownloadImage (storeStickers.stickersList [j].stickerImagePath, storeStickers.stickersList [j].stickerImageName, storeStickers.Stickerimages [j]);
				}
				inventoryStickersList.Add (storeStickers);
			}
		}


        public void OnClickCategory(int categoryId)
        {
            Debug.Log("OnClickCategory " + categoryId);
            if (mHorizontalScrollSnap.CurrentPage != categoryId)
                mHorizontalScrollSnap.GoToScreen (categoryId);


            /*if (UIController.Instance.assetsReferences.stickers[categoryId].stickersList.Count > 0)
            {
                InstantiateStickers(categoryId);
                downloadStickerPanel.gameObject.SetActive(false);
            }
            else
            {
                DownloadStickerPanel(categoryId);
            }*/
        }

        public void NewStickers ()
        {
            Debug.Log("Open Sticker download panel.....");
			DownloadStickerPanel (downloadedStickercatId);
			//if (downloadedStickercatId < inventoryStickersList.Count) 
			//{
				InstantiateDownloadedStickers ();
				//downloadedStickercatId++;
			//}
//			OnClickCategory(catId);
//          chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
        }

        void InstantiateStickers (int categoryId)
        {
            if (stickerObjectList != null)
            {
                for (int i = 0; i < stickerObjectList.Count; i++)
                {
                    if (stickerObjectList[i] != null)
                    {
                        Destroy(stickerObjectList[i]);
                    }
                }
                stickerObjectList.Clear();
            }

            stickerObjectList = new List<GameObject>();

            string stickerName = UIController.Instance.assetsReferences.stickers[categoryId].name;
            List<Sprite> stickerList = UIController.Instance.assetsReferences.stickers[categoryId].stickersList;
            for (int i = 0; i < stickerList.Count; i++)
            {
                string _pathName = "Stickers/" + stickerName + "/" + stickerList [i].name;
                GameObject go = new GameObject (_pathName);

                go.transform.SetParent(stickerContentScrollRect.content.transform, false);
                Image _img = go.AddComponent<Image>();
                _img.sprite = stickerList[i];
                _img.preserveAspect = true;
                _img.raycastTarget = true;

                Button _button = go.AddComponent<Button>();
                _button.onClick.AddListener(() => OnClickSticker(_button));

                stickerObjectList.Add(go);
            }
        }


		void InstantiateDownloadedStickers ()
		{
			if (stickerObjectList != null)
			{
				for (int i = 0; i < stickerObjectList.Count; i++)
				{
					if (stickerObjectList[i] != null)
					{
						Destroy(stickerObjectList[i]);
					}
				}
				stickerObjectList.Clear();
			}

			stickerObjectList = new List<GameObject>();

			for (int i = 0; i < inventoryStickersList.Count; i++) 
			{
				for (int j = 0; j < UIController.Instance.assetsReferences.stickers.Length; j++) 
				{
					string stickerName = UIController.Instance.assetsReferences.stickers[j].name;
					if ( stickerName.Equals( inventoryStickersList [i].title)) 
					{
						List<Sprite> stickerList = UIController.Instance.assetsReferences.stickers[j].stickersList;
						for (int k = 0; k < stickerList.Count; k++)
						{
                            string _pathName = "Stickers/" + stickerName + "/" + stickerList [i].name;
                            GameObject go = new GameObject(_pathName);
							go.transform.SetParent(downloadStickerScrollRect.content.transform, false);
							Image _img = go.AddComponent<Image>();
							_img.sprite = stickerList[k];
							_img.preserveAspect = true;
							_img.raycastTarget = true;

							Button _button = go.AddComponent<Button>();
							_button.onClick.AddListener(() => OnClickDownloadedSticker(_button));

							stickerObjectList.Add(go);
						}
					}
				}
			}

//			string stickerName = inventoryStickersList[categoryId].title;
//			//List<Sprite> stickerList = inventoryStickersList[categoryId].Stickerimages;
//			for (int i = 0; i < inventoryStickersList[categoryId].stickersList.Count; i++)
//			{
//				//Debug.Log ("Sticker Count --- >>>>" + inventoryStickersList [categoryId].Stickerimages[i]);
//				GameObject go = new GameObject(stickerName + "_" + i.ToString());
//				go.transform.SetParent(downloadStickerScrollRect.content.transform, false);
//				Image _img = go.AddComponent<Image>();
//				//_img.sprite = inventoryStickersList[categoryId].Stickerimages[i];
//				imageController.GetSpriteFromLocalOrDownloadImage (inventoryStickersList [categoryId].stickersList [i].stickerImagePath, inventoryStickersList [categoryId].stickersList[i].stickerImageName, _img);
//				_img.preserveAspect = true;
//				_img.raycastTarget = true;
//
//				Button _button = go.AddComponent<Button>();
//				_button.onClick.AddListener(() => OnClickSticker(_button));
//
//				stickerObjectList.Add(go);
//			}
		}

        void DownloadStickerPanel (int categoryId)
        {
            downloadStickerPanel.gameObject.SetActive(true);
            stickerNameText.text = UIController.Instance.assetsReferences.stickers[categoryId].name + " Stickers";
        }

        public void OnClickSticker (Button obj)
        {
//            Debug.Log (obj.gameObject.name);
//            if (ChatManager.CheckInternetConnection())
            {
                string filename = System.IO.Path.GetFileNameWithoutExtension(obj.GetComponent<Image>().sprite.name);
                //ChatManager.Instance.SendInAppFile(filename, ChatFileType.FileType.Stickers);
                
                FreakChatSerializeClass.NormalMessagerMetaData _metaData = 
                    new FreakChatSerializeClass.NormalMessagerMetaData();
                _metaData.messageType = ChatMessage.MetaMessageType.InAppFile;
                _metaData.fileType = ChatFileType.FileType.Stickers;
                _metaData.filePath = obj.gameObject.name;
                
                //            ChatManager.Instance.SendInAppFile(_metaData);
                string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
//                ChatManager.Instance.messageSendCallback = UIController.Instance.PlaySendMessageSound;
                chatWindowUI.DuplicateMessageBox ("Stickers", _data);
//                ChatManager.Instance.SendUserMessage("Stickers", _data);
            }

//            chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.CloseAll);
        }

		public void OnClickDownloadedSticker (Button obj)
		{
            Debug.Log (obj.GetComponent<Image> ().sprite.name);
            /*if (ChatManager.CheckInternetConnection())
            {
                string filename = System.IO.Path.GetFileNameWithoutExtension(obj.GetComponent<Image>().sprite.name);
                
                FreakChatSerializeClass.NormalMessagerMetaData _metaData = 
                    new FreakChatSerializeClass.NormalMessagerMetaData();
                _metaData.messageType = ChatMessage.MetaMessageType.InAppFile;
                _metaData.fileType = ChatFileType.FileType.Stickers;
                _metaData.filePath = "Stickers/" + "Common/" + filename;
                
                //          ChatManager.Instance.SendInAppFile(_metaData);
                string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
//                ChatManager.Instance.messageSendCallback = UIController.Instance.PlaySendMessageSound;
                chatWindowUI.DuplicateMessageBox ("Stickers", _data);
//                ChatManager.Instance.SendUserMessage("Stickers", _data);
            }

            chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.CloseAll);*/
		}

        private List<StickerToggleButton> toggleStickerToggleButton;
        void InstantiateCategory ()
        {
            if (toggleStickerToggleButton == null)
                toggleStickerToggleButton = new List<StickerToggleButton>();

//            Debug.Log("Time.time. "+Time.time);

            for (int i = 0; i < UIController.Instance.assetsReferences.stickers.Length; i++)
            {
//                Debug.Log("Time.time.. "+Time.time);
                StickerToggleButton _category = (StickerToggleButton)Instantiate (mStickerToggleButton);
                _category.transform.SetParent (stickerCategoryScrollRect.content.transform, false);
                _category.categoryID = i;
                _category.image.sprite = UIController.Instance.assetsReferences.stickers [i].main;
                _category.name = UIController.Instance.assetsReferences.stickers [i].name;
                _category.gameObject.SetActive (true);

                toggleStickerToggleButton.Add(_category);
                InstantiateStickersSnap (i, UIController.Instance.assetsReferences.stickers [i]);
            }

//            StartCoroutine(InstantiateCategoryAsyn());
            
            toggleStickerToggleButton[0].OnSwipe(0);

            Debug.Log("Time.time... "+Time.time);
        }

        IEnumerator InstantiateCategoryAsyn ()
        {
            for (int i = 0; i < UIController.Instance.assetsReferences.stickers.Length; i++)
            {
                Debug.Log("Time.time.. "+Time.time);
//                StickerToggleButton _category = (StickerToggleButton)Instantiate (mStickerToggleButton);
//                _category.transform.SetParent (stickerCategoryScrollRect.content.transform, false);
//                _category.categoryID = i;
//                _category.image.sprite = UIController.Instance.assetsReferences.stickers [i].main;
//                _category.name = UIController.Instance.assetsReferences.stickers [i].name;
//                _category.gameObject.SetActive (true);

//                toggleStickerToggleButton.Add(_category);

                InstantiateStickersSnap (i, UIController.Instance.assetsReferences.stickers [i]);

                yield return new WaitForEndOfFrame();
            }

            toggleStickerToggleButton[0].OnSwipe(0);
        }

        void InstantiateStickersSnap (int id, AssetsRef.Stickers stickers)
        {
            StickerSnapPanel _snapPanel = (StickerSnapPanel)Instantiate(stickerSnapPanel);
            _snapPanel.InitStickerSnapPanel(id, stickers);
            horizontalScrollSnap.GetComponent<HorizontalScrollSnap>().AddChild(_snapPanel.gameObject, false);
            _snapPanel.gameObject.SetActive(true);
        }

        public void OnSelectionChanged ()
        {
            Debug.Log("OnSelectionChanged");
        }

        public void OnSelectionPageChanged (int page)
        {
            Debug.Log("OnSelectionPageChanged " + page);
            if (toggleStickerToggleButton.Count > 0)
            {
                Debug.Log("toggleStickerToggleButton.Count " + toggleStickerToggleButton.Count);
                Debug.Log("toggleStickerToggleButton.Count " + mHorizontalScrollSnap.CurrentPage);

                for (int i = 0; i < toggleStickerToggleButton.Count; i++)
                {
                    toggleStickerToggleButton[i].OnSwipe(page);
                }
            }
        }

        public void OnSelectionChangedEnd (int page)
        {
            Debug.Log("OnSelectionChangedEnd " + page);
        }

        void OnDisable ()
        {
            Resources.UnloadUnusedAssets();
        }
    }
}
