﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace Freak.Chat
{
    public class TimeStampPopup : MonoBehaviour
    {
        public Text timeStampText;
        private string timestampString;

        // Use this for initialization
        void Start () {
            
        }
        
        public void ShowTimeStamp (long timestamp)
        {
            timestampString = FreakChatUtility.GetDisplayTimeStamp(timestamp);

            if (string.Equals (timeStampText.text, timestampString) == false)
            {
                timeStampText.text = timestampString;
                this.gameObject.SetActive(true);
                if (popupShowRoutine != null)
                {
                    StopCoroutine(popupShowRoutine);
                }
                popupShowRoutine = PopupShowTime();
                StartCoroutine(popupShowRoutine);
            }
        }

        IEnumerator popupShowRoutine;
        IEnumerator PopupShowTime ()
        {
            yield return new WaitForSeconds(2f);
            HideTimeStamp();
        }
        
        public void HideTimeStamp ()
        {
            if (popupShowRoutine != null)
            {
                StopCoroutine(popupShowRoutine);
            }
            this.gameObject.SetActive(false);
        }
    }
}
