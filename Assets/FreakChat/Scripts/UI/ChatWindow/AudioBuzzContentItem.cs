﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using Tacticsoft;

namespace Freak.Chat
{
    public class AudioBuzzContentItem : MonoBehaviour //TableViewCell
    {
        [SerializeField] private Image audioIconImage;
        [SerializeField] private Text audioName;
        private AudioClip mAudioClip;
        private string mAudioPath;
        private string mAudioIconPath;
        private AudioPanelUI mAudioPanelUI;
     
		public bool isPurchased;
		public string folderName;

        // Use this for initialization
        void Start () {
        }

        public void InitAudioBuzz (AudioClip clip, AudioPanelUI audioPanel, bool purchased)
        {
            //LOAD SPRITE IMAGE
            mAudioIconPath = "AudioBuzzIcon/" + clip.name;
            audioIconImage.sprite = (Sprite)Resources.Load<Sprite> (mAudioIconPath);

            mAudioPath = "Audio/AudioBuzz/" + clip.name;
            mAudioClip = clip;
            mAudioPanelUI = audioPanel;
            isPurchased = purchased;
        }
        
		public void InitAudioBuzzContent (string path, AudioClip clip, AudioPanelUI audioPanel, string clipName, bool purchased)
        {
            mAudioPanelUI = audioPanel;

            mAudioPath = path;
            mAudioClip = clip;

			audioName.text = clipName;
			isPurchased = purchased;
        }
        
        public void OnClickPlayButton ()
        {
            Debug.Log("OnClickPlayButton");
            UIController.Instance.GetAudioSource().PlayOneShot(mAudioClip);
        }
        public void OnClickSendButton ()
        {
            #if !OLD_AUDIO_BUZZ
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.InAppFile;
            _metaData.fileType = ChatFileType.FileType.AudioBuzz;
            _metaData.filePath = mAudioPath;

            string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);

            mAudioPanelUI.chatWindowUI.DuplicateMessageBox ("Audio Buzz", _data);

            #else
            string filename = System.IO.Path.GetFileNameWithoutExtension(mAudioClip.name);
            Debug.Log("OnClickSendButton " + filename);
            Debug.Log (mAudioClip.name);
            if (ChatManager.CheckInternetConnection())
            {
                FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
                _metaData.messageType = ChatMessage.MetaMessageType.InAppFile;
                _metaData.fileType = ChatFileType.FileType.AudioBuzz;
                
                if (isPurchased) 
                {
                    _metaData.filePath = "Audio/Buzz/"+folderName+"/"+filename;
                    Debug.Log("SEND Audio ------ >>" + _metaData.filePath);
                } 
                else
                {
                    _metaData.filePath = "Audio/Buzz/"+filename;
                }
                
                string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
//                ChatManager.Instance.messageSendCallback = UIController.Instance.PlaySendMessageSound;
                mAudioPanelUI.chatWindowUI.DuplicateMessageBox ("Audio Buzz", _data);
//                ChatManager.Instance.SendUserMessage("Audio Buzz", _data);
            }

            #endif

            //mAudioPanelUI.chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.CloseAll); //TTT/ mAudioPanelUI.chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);

        }
        
        /*#region TableViewCell
        public int rowNumber { get; set; }
        public Slider m_cellHeightSlider;
        
        [System.Serializable]
        public class CellHeightChangedEvent : UnityEngine.Events.UnityEvent<int, float> { }
        public CellHeightChangedEvent onCellHeightChanged;
        
        public void SliderValueChanged(Slider slider) {
            onCellHeightChanged.Invoke(rowNumber, slider.value);
        }
        
        public float height {
            get { return m_cellHeightSlider.value; }
            set { m_cellHeightSlider.value = value; }
        }
        #endregion*/
        
    }
}
