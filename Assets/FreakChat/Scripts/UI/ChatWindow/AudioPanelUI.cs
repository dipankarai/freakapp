﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using Tacticsoft;

namespace Freak.Chat
{
    public class AudioPanelUI : MonoBehaviour //, ITableViewDataSource
    {
        [HideInInspector] public ChatWindowUI chatWindowUI;

        [SerializeField] private RectTransform audioButtonPanel;
        [SerializeField] private RectTransform audioBuzzPanel;
        [SerializeField] private RectTransform audioRecordPanel;
        [SerializeField] private ScrollRect scrollRect;
//        [SerializeField] private TableView m_tableView;

        private List<GameObject> audioClipItem = new List<GameObject>();
        private AudioBuzzContentItem mAudioBuzzContentItem;

		public List<StoreAudioBuzz> inventoryAudioList = new List<StoreAudioBuzz> ();
		private string audioType = "2";

        // Use this for initialization
        void Start () {
			
        }

        public void ShowPanel()
        {
            if(this.gameObject.activeInHierarchy)
                this.gameObject.SetActive(false);
            
            this.gameObject.SetActive(true);
        }

		public void EnableAudio(List<object> audioList)
		{
			inventoryAudioList.Clear ();
			for (int i = 0; i < audioList.Count; i++) 
			{
				Dictionary<string,object> data = (Dictionary<string,object>)audioList [i];
				StoreAudioBuzz storeAudio = new StoreAudioBuzz (data);
				inventoryAudioList.Add (storeAudio);
			}

			InitAudioBuzzStart();
		}

		void DisplayDownloadedAudioBuzz()
		{
//			for (int i = 0; i < inventoryAudioList.Count; i++)
//			{
//				for (int j = 0; j < inventoryAudioList [i].audioBuzzList.Count; j++) 
//				{
//					GameObject go = Instantiate (UIController.Instance.assetsReferences.AudioBuzzContentItem);
//					go.transform.SetParent (scrollRect.content.transform, false);
//					//string _path = Application.dataPath + "/Resources/Audio/Buzz/" +
//					//UIController.Instance.assetsReferences.audioBuzzs[i].name + ".mp3";
//					//AudioClip audio = GetAudioClip(inventoryAudioList [i].audioBuzzList[j].audioPath);
//					//go.GetComponent<AudioBuzzContentItem>().InitAudioBuzzContent(audio.name,
//						//audio, this);
//					StartCoroutine(StartDownLoadingAudio(inventoryAudioList [i].audioBuzzList[j].audioPath, inventoryAudioList [i].audioBuzzList[j].audioName, go.GetComponent<AudioBuzzContentItem>()));
//					go.SetActive (true);
//
//					audioClipItem.Add (go);
//				}
//			}

			for (int i = 0; i < inventoryAudioList.Count; i++) 
			{
				for (int j = 0; j < UIController.Instance.assetsReferences.purchasedAudioBuzzs.Length; j++) 
				{
					string audioPackName = UIController.Instance.assetsReferences.purchasedAudioBuzzs[j].name;
					if ( audioPackName.Equals( inventoryAudioList [i].title)) 
					{
						List<AudioClip> audioList = UIController.Instance.assetsReferences.purchasedAudioBuzzs[j].audioList;
						for (int k = 0; k < audioList.Count; k++)
						{
							GameObject go = Instantiate(UIController.Instance.assetsReferences.AudioBuzzContentItem);
							go.transform.SetParent(scrollRect.content.transform, false);
							string _path = Application.dataPath + "/Resources/Audio/Buzz/"+audioPackName+"/"+
								UIController.Instance.assetsReferences.purchasedAudioBuzzs[j].audioList[k].name + ".mp3";
							go.GetComponent<AudioBuzzContentItem>().InitAudioBuzzContent(_path,
								UIController.Instance.assetsReferences.purchasedAudioBuzzs[j].audioList[k] , this, UIController.Instance.assetsReferences.purchasedAudioBuzzs[j].audioList[k].name, true);
							go.GetComponent<AudioBuzzContentItem> ().folderName = audioPackName;
							go.SetActive(true);


							audioClipItem.Add(go);
						}
					}
				}

			}

		}


//		AudioClip GetAudioClip(string url)
//		{
//			WWW www = new WWW(url);
//			Debug.Log ("AUDIOCLIP NAME ---- > " + www.audioClip.name);
//			return www.audioClip;
//		}
		IEnumerator StartDownLoadingAudio(string url, string audioName, AudioBuzzContentItem gObj)
		{
			// Start a download of the given URL
			WWW www = new WWW(url);
			// Wait for download to complete
			yield return www;
			// get text
			AudioClip _audio = www.GetAudioClip(false, false);
			Debug.Log ("NAME ---- > " + audioName + " URL -- > "+ url);
			//gObj.InitAudioBuzzContent (audioName, _audio, this, audioName);
			// having audio here. Do with it whatever you want...
		}

        void OnDisable ()
        {
            audioButtonPanel.gameObject.SetActive(true);
            audioBuzzPanel.gameObject.SetActive(false);
            audioRecordPanel.gameObject.SetActive(false);

            for (int i = 0; i < audioClipItem.Count; i++)
            {
                if (audioClipItem[i] != null)
                {
                    Destroy(audioClipItem[i]);
                }
            }

            audioClipItem.Clear();

            Resources.UnloadUnusedAssets();
        }
        
        public void OnClickAudioBuzzButton ()
        {
            Debug.Log("OnClickAudioBuzzButton");
            audioButtonPanel.gameObject.SetActive(false);
            audioBuzzPanel.gameObject.SetActive(true);
          
            #if !OLD_AUDIO_BUZZ
            InitAudioBuzzStart();
            #else
            if (ChatManager.CheckInternetConnection())
            {
                FindObjectOfType<StoreInventory> ().GetStorePurchasedListAPICall (audioType, EnableAudio);
            }
            else
            {
                InitAudioBuzzStart();
            }
            #endif
        }

        public void OnCLickAudioRecordButton ()
        {
            Debug.Log("OnCLickAudioRecordButton");
            audioButtonPanel.gameObject.SetActive(false);
            audioRecordPanel.gameObject.SetActive(true);
//            InitAudioRecordStart();
        }

        #region AUDIO BUZZ

        void InitAudioBuzzStart ()
        {
            #if !OLD_AUDIO_BUZZ

            /*string resourcePath = "Audio/AudioBuzz";
            AudioClip[] _clips = Resources.LoadAll<AudioClip> (resourcePath);
            for (int i = 0; i < _clips.Length; i++)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.AudioBuzzContentItem);
                go.transform.SetParent(scrollRect.content.transform, false);

                go.GetComponent<AudioBuzzContentItem>().InitAudioBuzz(_clips[i], this, false);
                go.SetActive(true);

                audioClipItem.Add(go);
            }*/

            Debug.Log (UIController.Instance.assetsReferences.audioBuzzsAssetsRef.audioBuzzs.Length);

            for (int i = 0; i < UIController.Instance.assetsReferences.audioBuzzsAssetsRef.audioBuzzs.Length; i++)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.AudioBuzzContentItem);
                go.transform.SetParent(scrollRect.content.transform, false);

                go.GetComponent<AudioBuzzContentItem>().InitAudioBuzz(UIController.Instance.assetsReferences.audioBuzzsAssetsRef.audioBuzzs[i], this, false);
                go.SetActive(true);

                audioClipItem.Add(go);
            }

            #else
            for (int i = 0; i < UIController.Instance.assetsReferences.audioBuzzs.Length; i++)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.AudioBuzzContentItem);
                go.transform.SetParent(scrollRect.content.transform, false);

                string _path = Application.dataPath + "/Resources/Audio/AudioBuzz/" +
                               UIController.Instance.assetsReferences.audioBuzzs[i].name + ".mp3";
                go.GetComponent<AudioBuzzContentItem>().InitAudioBuzzContent(_path,
                    UIController.Instance.assetsReferences.audioBuzzs[i], this, UIController.Instance.assetsReferences.audioBuzzs[i].name, false);
                go.SetActive(true);

                audioClipItem.Add(go);
            }

             //Also Check for purchased Audios..
            DisplayDownloadedAudioBuzz();
            #endif

            //ITableViewDataSource
//            m_customRowHeights = new Dictionary<int, float>();
//            m_tableView.dataSource = this;
//            mAudioBuzzContentItem = UIController.Instance.assetsReferences.AudioBuzzContentItem.GetComponent<AudioBuzzContentItem>();
        }
        #endregion

        #region AUDIO RECORD
        void InitAudioRecordStart ()
        {
            
        }
        #endregion

		void CheckForDownloadedAudioBuzz()
		{
			
		}

/*
        #region ITableViewDataSource
        private int m_numInstancesCreated = 0;
        private Dictionary<int, float> m_customRowHeights;

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView) {
            return UIController.Instance.assetsReferences.audioBuzzs.Length;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row) {
            return GetHeightOfRow(row);
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            AudioBuzzContentItem _audioBuzzContentItem = tableView.GetReusableCell(mAudioBuzzContentItem.reuseIdentifier) as AudioBuzzContentItem;
            if (_audioBuzzContentItem == null)
            {
                GameObject go = Instantiate(UIController.Instance.assetsReferences.AudioBuzzContentItem);
                _audioBuzzContentItem = go.GetComponent<AudioBuzzContentItem>();
                _audioBuzzContentItem.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
                _audioBuzzContentItem.onCellHeightChanged.AddListener(OnCellHeightChanged);
                go.SetActive(true);
            }

            _audioBuzzContentItem.rowNumber = row;
            string _path = Application.dataPath + "/FreakChat/Audio/Buzz/" +
                UIController.Instance.assetsReferences.audioBuzzs[row].name + "mp3";
            _audioBuzzContentItem.InitAudioBuzzContent(_path, UIController.Instance.assetsReferences.audioBuzzs[row]);
            _audioBuzzContentItem.height = GetHeightOfRow(row);

            return _audioBuzzContentItem;
        }

        private float GetHeightOfRow(int row) {
            if (m_customRowHeights.ContainsKey(row)) {
                return m_customRowHeights[row];
            } else {
                return mAudioBuzzContentItem.height;
            }
        }

        private void OnCellHeightChanged(int row, float newHeight) {
            if (GetHeightOfRow(row) == newHeight) {
                return;
            }
            //Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));
            m_customRowHeights[row] = newHeight;
            m_tableView.NotifyCellDimensionsChanged(row);
        }
        #endregion*/
    }


}
