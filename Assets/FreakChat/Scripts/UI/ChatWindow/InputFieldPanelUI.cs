﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


namespace Freak.Chat
{
	public class InputFieldPanelUI : NativeInput
    {
        public GameObject closeButton;
		[SerializeField] private GameObject audioButton, sendButton,ChatInputField;
		private InputField messageInputField;
		private NativeEditBox nativeInputField;
		private RectTransform inputTextTransform;

        [HideInInspector] public ChatWindowUI chatWindowUI;
		private GroupMemberSearch groupMemberSearch;
		public Transform parentObj;
		private int keyboardHeight;
		private bool isNativeKeyboolActivated;
//        private List<FreakChatSerializeClass.FreakMessagingChannel.FreakMember> memberList = new List<FreakChatSerializeClass.FreakMessagingChannel.FreakMember>();
        private List<ChatUser> membersList = new List<ChatUser>();

        private string _tempMsg=string.Empty;
		void Awake () 
		{
			messageInputField = ChatInputField.GetComponent<InputField> ();
			nativeInputField = ChatInputField.GetComponent<NativeEditBox> ();
			nativeInputField.InputReciver = this;
			inputTextTransform = ChatInputField.transform.FindChild("Text").GetComponent<RectTransform>();

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("KeyBoard------>> " + inputTextTransform.transform.name);
            #endif
            messageInputField.interactable = false;
		}

		public void ActivateNativeField()
		{
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("KeyBoard------>>  ActivateNativeField");
            #endif
//            Invoke("InvokeActivateNativeField", 0.5f);
            messageInputField.interactable = true;
            InvokeActivateNativeField();
		}

        void InvokeActivateNativeField()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("KeyBoard------>>  Initialization  INputfield");
            #endif
            if (!isNativeKeyboolActivated) 
            {
                isNativeKeyboolActivated = true;
                nativeInputField.Initialization ("Message");
            }
        }

        // Use this for initialization
        void Start () 
		{
            //TODO: For timing we are using single line message
            messageInputField.lineType = InputField.LineType.SingleLine;

            sendButton.SetActive(false);
            audioButton.SetActive(true);

            if (checkUpdateRoutine != null)
                StopCoroutine(checkUpdateRoutine);

            /*checkUpdateRoutine = CheckUpdate();
            StartCoroutine(checkUpdateRoutine);*/
        }

        void OnDisable ()
        {
            if (checkUpdateRoutine != null)
                StopCoroutine(checkUpdateRoutine);
        }

        private bool isTypingStarted;
        private float typingStartInterval;
        private IEnumerator checkUpdateRoutine;

        // Update is called once per frame
        void Update () {
			//messageInputField.shouldHideMobileInput = true;

//            while(true)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    chatWindowUI.OnEscapeButtonPressed();
                }

                if (messageInputField.isFocused || nativeInputField.IsFocused)
                {   
                    /*#if UNITY_IPHONE || UNITY_IOS
                    if (TouchScreenKeyboard.visible)
                    {
                        Debug.Log("TouchScreenKeyboard.area.height "+TouchScreenKeyboard.area.height);
                        if (PlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight) == false)
                        {
                            Debug.Log ("TouchScreenKeyboard.area.height ---------> "+TouchScreenKeyboard.area.height);
                            if (TouchScreenKeyboard.area.height > 0)
                                PlayerPrefs.SetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight, ScaleFactorCalculation (TouchScreenKeyboard.area.height));
                        }
                        else if (PlayerPrefs.GetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight) <= 0)
                        {
                            Debug.Log ("TouchScreenKeyboard.area.height ---------> "+TouchScreenKeyboard.area.height);
                            PlayerPrefs.SetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight, ScaleFactorCalculation (TouchScreenKeyboard.area.height));
                        }
                    }
                    #endif*/
                    
                    chatWindowUI.SwitchPanelState(ChatWindowUI.PanelState.Message); ///TTT/ chatWindowUI.OnFousedInputField(true);
                    
                    if(messageInputField.text.Length > 0 && isTypingStarted == false)
                    {
                        SendTypingStart();
                    }
                    else if (messageInputField.text.Length <= 0 && isTypingStarted)
                    {
                        SendTypingEnd();
                    }
                }
                else
                {
                    if (isTypingStarted)
                    {
                        SendTypingEnd();
                    }
                }

//                yield return null;
            }
        }

        float ScaleFactorCalculation (float keyboardHeight)
        {
            float m_AspectPercent = (keyboardHeight / (float)Screen.height);// (768f / 1024f);
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("KeyBoard------>> m_AspectRatio" + m_AspectPercent);
            #endif

            // Reference Resolution x=1024 Y=768 according to input in Canvas Scaler, Scale with screen size.
            float aspect_Height = 1024f * m_AspectPercent;
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("KeyBoard------>> aspect_Height" + aspect_Height);
            #endif

            return aspect_Height;
        }

//		int GetKeyboardHeight()
//		{
////			#if UNITY_ANDROID && !UNITY_EDITOR 
////			using(AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
////			{
////				AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
////
////				using(AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
////				{
////					View.Call("getWindowVisibleDisplayFrame", Rct);
////
////					return Screen.height - Rct.Call<int>("height");
////				}
////			}
////			#endif
//			return 40;
//		}


        public void OnValueChangeMessageInput (string text)
        {
            if (text.Length > 0)
            {
                sendButton.SetActive(true);
                audioButton.SetActive(false);
            }
            else
            {
                sendButton.SetActive(false);
                audioButton.SetActive(true);
            }

            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                if (string.IsNullOrEmpty(taggedString) == false)
                {
                    CheckForTaggedMember(text);
                }

                if (TagSearchCondition(text))
                {
                    TagSearchMember(text);
                }
                else
                {
                    if (groupMemberSearch != null)
                    {
                        Destroy (groupMemberSearch.gameObject);
                    }
                }
            }
        }

        public void EmojiSelected (string emoji)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(MiniJSON.Json.Serialize(emoji));
            #endif
        }

        void SendTypingStart()
        {
            isTypingStarted = true;
            ChatManager.Instance.TypeStart();
        }

        void SendTypingEnd()
        {
            isTypingStarted = false;
            ChatManager.Instance.TypeEnd();
        }

        void TagSearchMember (string searchText)
        {
            searchText = searchText.Replace("@", string.Empty);

            for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.channelMembers.Count; i++)
            {
                string _name = ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].nickname;
                if (_name.ToUpper ().Contains (searchText.ToUpper ())) {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Name:: " + _name + "Searched:: " + searchText);
                    #endif
                    if (membersList.Contains(ChatManager.Instance.currentSelectedChatChannel.channelMembers[i]) == false)
                    {
                        membersList.Add(ChatManager.Instance.currentSelectedChatChannel.channelMembers[i]);
                    }
                }
            }

            /*for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.groupMember.Count; i++)
            {
                string _name = ChatManager.Instance.currentSelectedChatChannel.groupMember[i].name;
                if (_name.ToUpper ().Contains (searchText.ToUpper ())) {
                    Debug.Log("Name:: " + _name + "Searched:: " + searchText);
                    if (memberList.Contains(ChatManager.Instance.currentSelectedChatChannel.groupMember[i]) == false)
                    {
                        memberList.Add(ChatManager.Instance.currentSelectedChatChannel.groupMember[i]);
                    }
                }
            }*/

            for (int i = 0; i < membersList.Count; i++)
            {
                string _name = membersList[i].nickname;
                if (_name.ToUpper().Contains (searchText.ToUpper()) == false)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Name== " + _name + "Searched== " + searchText);
                    #endif
                    membersList.RemoveAt(i--);
                }
            }

            /*for (int i = 0; i < memberList.Count; i++)
            {
                string _name = memberList[i].name;
                if (_name.ToUpper ().Contains (searchText.ToUpper ()) == false) {
                    Debug.Log("Name== " + _name + "Searched== " + searchText);
                    memberList.RemoveAt(i--);
                }
            }*/

            if (groupMemberSearch == null)
            {
                groupMemberSearch = (GroupMemberSearch)GameObject.Instantiate (UIController.Instance.assetsReferences.GroupSearchPopup) as GroupMemberSearch;
                groupMemberSearch.gameObject.transform.SetParent (parentObj, false);
            }

            groupMemberSearch.searchedMembersList = membersList;
//            groupMemberSearch.searchedMemberList = memberList;
            groupMemberSearch.m_tableView.ReloadData();
        }

        bool TagSearchCondition (string text)
        {
            if (text.Length == 0)
                return false;

            if (text.Substring(0, 1).Equals("@"))
                return true;

            return false;
        }

        public void OnClickSendButton ()
        {
            Debug.Log("OnClickSendButton");

            if (messageInputField.text.Length > 0)
            {
                if(tagMetaData != null)
                {
                    tagMetaData.message = messageInputField.text.Replace(taggedString, string.Empty);
                    string _metaData = Newtonsoft.Json.JsonConvert.SerializeObject(tagMetaData);
                    chatWindowUI.DuplicateMessageBox ("Tag Friend", _metaData);
                    
                    taggedString = string.Empty;
                    tagMetaData = null;
                }
                else
                {
                    chatWindowUI.DuplicateMessageBox (messageInputField.text, string.Empty);
                }
                
                messageInputField.text = string.Empty;
                nativeInputField.SetTextNative(string.Empty);

				//NativeSetFocus(false);
				//StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll));

                if (isTypingStarted)
                {
                    SendTypingEnd();
                }
            }
        }

        void MessageSendCallback ()
        {
            UIController.Instance.PlaySendMessageSound ();
        }

        public void OnSendTagFriend (FreakChatSerializeClass.NormalMessagerMetaData metaData)
        {
            Debug.Log("OnSendTagFriend");

            tagMetaData = metaData;

            if (groupMemberSearch != null)
            {
                Destroy (groupMemberSearch.gameObject);
            }

            taggedString = atText + colorOpen + tagMetaData.username + colorClose;
       		messageInputField.text = taggedString + " ";
            messageInputField.caretPosition = messageInputField.text.Length;
        }

        FreakChatSerializeClass.NormalMessagerMetaData tagMetaData;
        private string atText = "<color=grey>@</color>";
        private string colorOpen = "<color=blue>";
        private string colorClose = "</color>";
        private string taggedString;
        private string tempText;
        void CheckForTaggedMember(string text)
        {
            if (text.Contains(taggedString))
            {
                tempText = text.Replace(taggedString,string.Empty);
            }
            else
            {
                taggedString = string.Empty;
                tagMetaData = null;

                //Chekcing for whie space in first charatcer
                if(tempText.Substring(0,1) == " ")
                {
                    tempText = tempText.Remove(0, 1);
                }
                messageInputField.text = tempText;
            }
        }

        public void OnClickAudioButton ()
        {
			Debug.Log("OnClickAudioButton");
			NativeSetFocus(false);
//            chatWindowUI.OpenAudioPanel();
			StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.Audio)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.Audio);
        }

        public void OnClickStickerButton ()
		{
			NativeSetFocus(false);
            Debug.Log("OnClickStickerButton");
            if (chatWindowUI.stickerPanelUI.gameObject.activeInHierarchy)
            {
				StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
            }
            else
            {
				StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.Sticker)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.Sticker);
            }
        }

		public void OnClickSmileyButton ()
		{
			Debug.Log("OnClickSmileyButton");
			NativeSetFocus(false);
            if (chatWindowUI.smileysPanel.gameObject.activeInHierarchy)
            {

				StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
			}
            else
			{
				StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.Smileys)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
			}
		}

        public void OnClickAttachButton ()
        {
            Debug.Log("OnClickAttachButton");
			NativeSetFocus(false);
			StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.Attach)); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.Attach);
        }


		public override void OnBackButtonPress()
		{
			OnClickCloseButton ();
		}



        public void OnClickCloseButton ()
        {
			Debug.Log("OnClickCloseButton"); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
			nativeInputField.IsFocused=false;
			NativeSetFocus(false);
			StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll));
        }
		IEnumerator DeFocusPanel(ChatWindowUI.PanelState PanelState)
		{
			yield return null;
			chatWindowUI.SwitchPanelState(PanelState);
		}

		public void NativeSetVisible(bool bTempVisible)
		{
			nativeInputField.SetVisible(bTempVisible);
		}
		public void NativeSetFocus(bool bTempVisible)
		{
			nativeInputField.SetFocusNative(bTempVisible);
		}

		public void OnClickSettingButton()
		{
			_tempMsg=nativeInputField.GetTextNative ();
			NativeSetFocus(false);
			StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll));
			nativeInputField.CustomCreate (false);
		}

		public void OnClosingSetting()
		{
			nativeInputField.Initialization ("Message");
			nativeInputField.SetTextNative (_tempMsg);
		}

		public void NativeInputActivation()
		{		
//			if (!nativeInputField.enabled)
//				nativeInputField.enabled = true;

//			Debug.Log("Keyboard   NativeInputActivation ");
//			nativeInputField.SetSelection(true);
			nativeInputField.SetRectNative (inputTextTransform); 
		}

		public void NativeInputDeActivation()
		{		
//			Debug.Log("Keyboard   NativeInputDeActivation ");
			nativeInputField.SetRectNative (inputTextTransform);  
//			nativeInputField.SetSelection (false);
		}

		public override void InputfieldClicked()
		{
            #if UNITY_IOS || UNITY_IPHONE
			nativeInputField.SetVisible(true);
			nativeInputField.SetFocusNative(true);
			Debug.Log("Keyboard   InputfieldClicked ");
			#endif
//			StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.Message));
		}

		void OnApplicationPause( bool isPaused )
		{
			if (isPaused) 
			{
				_tempMsg = nativeInputField.GetTextNative ();
				OnBackButtonPress ();
				nativeInputField.CustomCreate (false);
			} 
			else 
			{
				if (isNativeKeyboolActivated) 
				{
					StartCoroutine (DeFocusPanel (ChatWindowUI.PanelState.CloseAll));
					nativeInputField.Initialization ("Message");
					nativeInputField.SetTextNative (_tempMsg);
				}					
			}				
		}

        public void NativeSetTextNative ()
        {
            nativeInputField.Initialization ("Message");
            nativeInputField.SetTextNative (_tempMsg);
        }

        /*void HandleNativeKeyboardWhenInBackground ()
        {
            Debug.Log("HandleNativeKeyboardWhenInBackground"); //TTT/ chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
            nativeInputField.IsFocused=false;
            NativeSetFocus(false);
            StartCoroutine (DeFocusPanel(ChatWindowUI.PanelState.CloseAll));
        }*/
    }
}
