﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using Tacticsoft.Examples;
using UnityEngine.EventSystems;
using System.Linq;
using Newtonsoft.Json;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;


namespace Freak.Chat
{
	public class ChatWindowUI : MonoBehaviour, IEnhancedScrollerDelegate /*ITableViewDataSource*/
    {
        public Texture2D defaultImageMessageThumbnail;
        public Image userProfileImage;
        [SerializeField] private Text usernameText, lastSeenText;
        public ScrollRect scrollRect;

        [Header("UI Panel")]
        public AttachmentPanelUI attachmentPanelUI;
        public InputFieldPanelUI inputFieldPanelUI;
        public AudioPanelUI audioPanelUI;
        public StickerPanelUI stickerPanelUI;
        public ChatUserSettingsUI chatUserSettingsUI;
        public ChatUserSettingsUI groupChatUserSettingsUI;
        public Emoji smileyPanelUI;

		public GameObject chatImagePreviewPanel;

        [Header("Tableview")]
        public MessageWindowUI m_cellPrefab;
        public TableView m_tableView;

		public EnhancedScroller m_EnhancedScrollView;

        public ConnectingUI loadingObject;
        public TimeStampPopup timeStampPopup;
        public Freak.ImageHandler imageHandler;

        private int m_numInstancesCreated = 0;
        private Dictionary<int, float> m_customRowHeights;
        private bool isScrolledToBottom;
		public GameObject loadingGamePanel;
		public GameObject smileysPanel;

//		public string[] imageCode;
		/// <summary>
		/// Image's group.
		/// </summary>
//		public Sprite[] image;

		/// <summary>
		/// The library of image and code,eg:("0000",image.png).
		/// </summary>
		public Dictionary<string, Sprite> imageList = new Dictionary<string, Sprite>();
		public bool useImageCode = true;

		public GameObject wallpaperObj;
		public Image wallpaper;
        public Sprite opponentSprite;

		public GameObject displayContactsToSend;
        public GameObject deleteForwarddropDownObj;
        private string tempLastSeenText;
        private IEnumerator checkUpdateRoutine;
        private bool onStart = false;

        private Dictionary<string,Sprite> opponentSpriteDict;

        public void AddSprite (string userid, Sprite sprite)
        {
            if (opponentSpriteDict == null)
            {
                opponentSpriteDict = new Dictionary<string, Sprite>();
            }
            else
            {
                bool isAdded = false;
                foreach (var item in opponentSpriteDict)
                {
                    if (item.Key == userid)
                    {
                        isAdded = true;
                        opponentSpriteDict[userid] = sprite;
                    }
                }

                if (isAdded == false)
                {
                    opponentSpriteDict.Add(userid, sprite);
                }
            }
        }

        public Sprite GetSprite (string userid)
        {
            if (opponentSpriteDict == null)
                return null;

            foreach (var item in opponentSpriteDict)
            {
                if (item.Key == userid)
                {
                    return opponentSpriteDict[userid];
                }
            }

            return null;
        }

#region UNITY DEFAULT METHOD

        void Awake ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Awake ------>>>>> " + Time.realtimeSinceStartup);
            #endif
//            Resources.UnloadUnusedAssets();

            UIController.Instance.chatWindowUI = this;
            attachmentPanelUI.chatWindowUI = this;
            inputFieldPanelUI.chatWindowUI = this;
            audioPanelUI.chatWindowUI = this;
            stickerPanelUI.chatWindowUI = this;
            chatUserSettingsUI.chatWindowUI = this;
            groupChatUserSettingsUI.chatWindowUI = this;
            smileyPanelUI.chatWindowUI = this;

            FreakAppManager.Instance.GetHomeScreenPanel().displayContactsToSend = displayContactsToSend;
            FreakAppManager.Instance.GetHomeScreenPanel().chatImagePreviewPanel =  chatImagePreviewPanel;
            UIController.Instance.imageHandler = chatImagePreviewPanel.GetComponent<ImageHandler>();
            UIController.Instance.imageHandler.chatWindowUI = this;

            /*if (chatImagePreviewPanel.activeInHierarchy == false)
            {
                chatImagePreviewPanel.SetActive(true);
            }*/
        }

        void OnEnable()
        {
            Invoke("InitialCall", FreakAppManager.Instance.timeToInit);
        }

        void InitialCall ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnEnable ------>>>>> " + Time.realtimeSinceStartup);
            #endif
            imageHandler = gameObject.AddComponent<Freak.ImageHandler> ();

            timeStampPopup.gameObject.SetActive(false);
            timeStampPopup.timeStampText.text = string.Empty;
            m_customRowHeights = new Dictionary<int, float>();
  //          m_tableView.dataSource = this;
			m_EnhancedScrollView.Delegate = this;
            InitializePanelPosition ();
            InitCallback ();

//            inputFieldPanelUI.Invoke("ActivateNativeField",1);
            Invoke("InitStart",0.1f);
            inputFieldPanelUI.ActivateNativeField();
        }

        void InitializeData ()
        {
            InitHeaderPanelProperty();
            ShowProfileImage();
            CheckForWallpaper ();
        }

        // Use this for initialization
        void InitStart ()
        {
            m_EnhancedScrollView.cellViewVisibilityChanged = HandleCellViewVisibilityChangedDelegate;

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Start ------>>>>> " + Time.realtimeSinceStartup);
            #endif
            ChatManager.Instance.ClearAllLocalNotification ();
            #if UNITY_ANDROID
            PushNotification.CloseAllNotification ();
            #endif
            UIController.Instance.loadingGamePlay = loadingGamePanel;

            SwitchPanelState (PanelState.CloseAll);
//            m_tableView.onScrollIsDrag.AddListener(OnScrollDrag);

            LoadCachedData();

            if (checkUpdateRoutine != null)
                StopCoroutine(checkUpdateRoutine);

            checkUpdateRoutine = CheckUpdate();
            StartCoroutine(checkUpdateRoutine);

//            Debug.Log(UIController.Instance.assetsReferences.stickers.Length);
        }

        void OnDisable()
        {
            RemoveCallback ();
            imageHandler = null;
            if (checkUpdateRoutine != null)
                StopCoroutine(checkUpdateRoutine);

            DownloadManager.Instance.DestroyLoadImageMessageCache();

            Destroy (this.gameObject);
            Resources.UnloadUnusedAssets ();
        }

        // Update is called once per frame
        IEnumerator CheckUpdate()
		{
            while(true)
            {
                if (!deleteForwarddropDownObj.activeInHierarchy) {
                    if (ChatManager.Instance.selectedMessages.Count > 0) {
                        deleteForwarddropDownObj.SetActive (true);
                    }
                }
                else
                {
                    if (ChatManager.Instance.selectedMessages.Count > 0) {
                    } else {
                        deleteForwarddropDownObj.SetActive (false);
                    }
                }

                yield return null;
            }
		}


    #region BUTTON EVENT

        public void OnClickBackButton ()
        {
//            ChatManager.Instance.EndOneToOneMessaging();
            ChatManager.Instance.currentSelectedChatChannel = null;

            if (deleteForwarddropDownObj.activeInHierarchy)
            {
                if (ChatManager.Instance.selectedMessages.Count > 0)
                {
                    ChatManager.Instance.selectedMessages.Clear ();
                }
            }

            FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
            // FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.AllChatsPanel);
            // gameObject.SetActive(false);
        }

        public void OnClickSettingsButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickSettingsButton");
            #endif
            inputFieldPanelUI.OnClickSettingButton ();
            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                groupChatUserSettingsUI.gameObject.SetActive(ChatManager.Instance.currentSelectedChatChannel.isGroupChannel == true);
                chatUserSettingsUI.gameObject.SetActive(ChatManager.Instance.currentSelectedChatChannel.isGroupChannel == false);
            }
            else
            {
                groupChatUserSettingsUI.gameObject.SetActive(ChatManager.Instance.currentSelectedChatChannel.isGroupChannel == true);
                chatUserSettingsUI.gameObject.SetActive(ChatManager.Instance.currentSelectedChatChannel.isGroupChannel == false);
            }
        }

        public void OnClickPlayGameButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickPlayGameButton");
            #endif
        }

        public void OnClickProfileButton ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnClickPlayGameButton");
            Debug.Log("Open Profile Screen of user: " + ChatManager.Instance.currentSelectedChatChannel.otherUserName + " ID:" + ChatManager.Instance.currentSelectedChatChannel.otherUserId);
            #endif
        }

//        public void OnClickPreviousMessage ()
//        {
//            if (ChatManager.Instance.currentSelectedChatChannel.messagesList.Count > 0)
//            {
//                //                ChatManager.Instance.GetPreviousMessage(messageBoxList[0].chatMessage.createdAt, 5, UpdateNewMessage);
//                ChatManager.Instance.LoadPreviousMessages(messageBoxList[0].chatMessage.createdAt, UpdateNewMessage);
//            }
//        }

        public void OnClickShowGroupProfile()
        {
            if(ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                ChatManager.Instance.groupUsersList.Clear();

                FreakAppManager.Instance.selectedGroupProfile = ChatManager.Instance.currentSelectedChatChannel;
                GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
//                go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (ChatManager.Instance.currentSelectedChatChannel);

//                FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
            }
			else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("Open User Profile NAME: " + ChatManager.Instance.currentSelectedChatChannel.otherUserName + " ID: " + ChatManager.Instance.currentSelectedChatChannel.otherUserId);
                #endif

//                if (ChatManager.CheckInternetConnection())
                {
                    FreakAppManager.Instance.tempCurrentChannel = ChatManager.Instance.currentSelectedChatChannel;
                    FreakAppManager.Instance.myUserProfile.memberId = ChatManager.Instance.currentSelectedChatChannel.otherUserId;
                    FreakAppManager.Instance.GetHomeScreenPanel().ReplacePanel (HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
                }
			}
        }

    #endregion  //BUTTON EVENT

#endregion

        void LoadCachedData ()
        {
            if (ChatManager.Instance.currentSelectedChatChannel.messagesList.Count > 0)
            {
//                DisplayMessages ();
                Invoke("IntitMessageFirstTime", 0.2f);
//                IntitMessageFirstTime();
            }

            if (isGettingChannel == false)
            {
                LoadChannel ();
            }

            // SEND ANY PENDING TEXT MESSAGE...
//            SendMessageAutomatically ();
        }

        void LoadChannel ()
        {
            if (ChatManager.Instance.isInternetConnected)
            {
                if (ChatManager.Instance.currentChannel != null && ChatManager.Instance.currentChannel.Url == ChatManager.Instance.currentSelectedChatChannel.channelUrl)
                {
                    GetLastMessageStatus ();

                    ChatManager.Instance.LoadMessage (DisplayMessages, _messageTimeStamp, _unreadMessage);

                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Load Channel "+ChatManager.Instance.currentChannel.Name);
                    #endif
                }
                else
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Create New Channel");
                    #endif
                    //Create New Channel......
                    ChatManager.Instance.CreateNewChannel(ChatManager.Instance.currentSelectedChatChannel.otherUserId, DisplayMessages);
                    loadingObject.gameObject.SetActive (false);
                }
            }


            MarkAsRead();
        }

        void MarkAsRead ()
        {
            ChatManager.Instance.newTotalMessages -= ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount;
            ChatManager.Instance.newTotalChats--;
            ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount = 0;
        }

        private bool fromBackground;
        void OnApplicationPause (bool paused)
        {
            if (paused == false)
            {
                fromBackground = true;
            }
        }

        public void FromBackground ()
        {
            if (fromBackground)
            {
                fromBackground = false;
                ChatManager.Instance.GetChannelByChannelUrl (ChatManager.Instance.currentSelectedChatChannel.channelUrl, GetGroupChannelCallback);
            }
        }

        private long _messageTimeStamp = SendBird.Utils.CurrentTimeMillis();
        private int _unreadMessage = 0;//ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount;
        void GetLastMessageStatus ()
        {
            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            _messageTimeStamp = SendBird.Utils.CurrentTimeMillis();
            _unreadMessage = ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount;
        }

		void CheckForWallpaper()
		{
            if (!string.IsNullOrEmpty (FreakAppManager.Instance.myUserProfile.wallpaper_image))
			{
                DownloadManager.Instance.DownLoadWallpaper(FreakAppManager.Instance.myUserProfile.wallpaper_image, FolderLocation.Wallpaper, LoadWallpaper);
            }
            else {
                wallpaperObj.gameObject.SetActive (false);
            }
        }

        void LoadWallpaper (Texture2D texture)
        {
            if (texture != null)
            {
                texture.Compress(false);
                wallpaper.FreakSetSprite(texture);
                wallpaperObj.gameObject.SetActive (true);
            }
            else
            {
                wallpaperObj.gameObject.SetActive (false);
            }
            /*string wallpaperPath = StreamManager.LoadFilePath(Path.GetFileName(FreakAppManager.Instance.myUserProfile.wallpaper_image), FolderLocation.Wallpaper, false);
            if (File.Exists(wallpaperPath))
            {
                StartCoroutine(LoadWallpaperRoutine(wallpaperPath));
            }
            else
            {
                wallpaperObj.gameObject.SetActive (false);
            }*/
        }

        IEnumerator LoadWallpaperRoutine (string path)
        {
            var www = new WWW("file://" + path);

            while (!www.isDone)
            {
                yield return null;
            }

            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                wallpaper.FreakSetSprite(www.texture);
                wallpaperObj.gameObject.SetActive (true);
            }
            else
            {
                wallpaperObj.gameObject.SetActive (false);
            }
        }

        void OnScrollDrag(bool isDrag)
        {
            LayoutElement[] layoutElement = scrollRect.content.GetComponentsInChildren<LayoutElement>();
            if(layoutElement.Length > 2)
            {
                if (layoutElement[1].GetComponent<MessageBoxBase>() != null)
                {
                    TimeStampBox timeStamp = layoutElement[1].GetComponent<MessageBoxBase>() as TimeStampBox;
                    if (timeStamp != null)
                    {
                        //Debug.Log("BASE CLASS");
                    }
                    else
                    {
                        timeStampPopup.ShowTimeStamp(layoutElement[1].GetComponent<MessageBoxBase>().chatMessage.createdAt);
                    }
                }
            }
        }

        private bool isGettingChannel = false;
        public void InitChat (ChatChannel userDetails, bool isGettingChannel = false)
        {
            this.isGettingChannel = isGettingChannel;
            ChatManager.Instance.currentSelectedChatChannel = userDetails;

            #if PER_CHECK
            ChatManager.Instance.currentSelectedChatChannel.messagesList = CacheManager.LoadMessageList(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
            #endif

            InitializeData ();
        }

        void InitHeaderPanelProperty ()
        {
            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                string _names = GetMembersNameCommaSeprated();
                if (_names.Length > 20)
                {
                    lastSeenText.text = _names.Substring(0, 18);
                    lastSeenText.text += ".......";
                }
                else
                {
                    lastSeenText.text = _names;
                }
				usernameText.text = WWW.UnEscapeURL( ChatManager.Instance.currentSelectedChatChannel.channelName);
            }
            else
            {
                usernameText.text = ChatManager.Instance.currentSelectedChatChannel.otherUserName;
                if (ChatManager.Instance.currentSelectedChatChannel.channelMembers != null && ChatManager.Instance.currentSelectedChatChannel.channelMembers.Count != 0)
                {
                    if (ChatManager.Instance.currentSelectedChatChannel.GetSender().connectionStatus ==  ChatUser.UserConnectionStatus.Online)
                    {
                        lastSeenText.text = "Online";
                    }
                    else
                    {
                        lastSeenText.text = "Last Seen: " + FreakChatUtility.LastSeenTime(ChatManager.Instance.currentSelectedChatChannel.GetSender().lastSeenAt);
                    }
                    
                }
            }

            tempLastSeenText = lastSeenText.text;
        }

        string GetMembersNameCommaSeprated ()
        {
            List<string> userName = new List<string>();
            for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.channelMembers.Count; i++)
            {
                userName.Add(ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].nickname);
            }

            return string.Join(", ", userName.ToArray());
        }

        #region User/Group Profile Image

        private string profileImageFilePath;
        void ShowProfileImage ()
        {
            if (FreakAppManager.Instance.senderCompressedPic != null)
            {
                userProfileImage.FreakSetSprite(FreakAppManager.Instance.senderCompressedPic);
                return;
            }

            Freak.FolderLocation _location = Freak.FolderLocation.Profile;

            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                _location = Freak.FolderLocation.GroupProfile;
                profileImageFilePath = ChatManager.Instance.currentSelectedChatChannel.coverUrl;
            }
            else
            {
                profileImageFilePath = FreakAppManager.Instance.profileLink;
            }

            if (!string.IsNullOrEmpty (profileImageFilePath)) 
            {
                Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache (profileImageFilePath, _location, null);

                if (texture != null) 
                {
                    userProfileImage.FreakSetSprite(texture);
                } 
                else
                {
                    Invoke ("ShowProfileImage", 0.5f);
                }
            }

            /*if (ChatManager.Instance.currentSelectedChatChannel.GetSender() != null && ChatManager.Instance.currentSelectedChatChannel.GetSender().texture != null)
            {
                userProfileImage.FreakSetSprite(ChatManager.Instance.currentSelectedChatChannel.GetSender().texture);
                return;
            }

            string _profileImageFilePath = string.Empty;
            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                _profileImageFilePath = StreamManager.LoadFilePath(Path.GetFileName(ChatManager.Instance.currentSelectedChatChannel.coverUrl), FolderLocation.GroupProfile, false);
            }
            else
            {
                if (ChatManager.Instance.currentSelectedChatChannel.GetSender() != null)
                {
                    _profileImageFilePath = StreamManager.LoadFilePath(Path.GetFileName(ChatManager.Instance.currentSelectedChatChannel.GetSender().profileUrl), FolderLocation.Profile, false);

                }
            }

            if (File.Exists(_profileImageFilePath))
            {
                if (string.IsNullOrEmpty(profileImageFilePath) || _profileImageFilePath.Equals(profileImageFilePath) == false)
                {
                    profileImageFilePath = _profileImageFilePath;
                    FileSavedCallback(true);
                }
            }
            else
            {
                if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
                {
                    DownloadManager.Instance.DownLoadFile(ChatManager.Instance.currentSelectedChatChannel.coverUrl, FolderLocation.GroupProfile, FileSavedCallback, ChatManager.Instance.currentSelectedChatChannel.channelUrl);
                }
                else
                {
                    DownloadManager.Instance.DownLoadFile(ChatManager.GetSender(ChatManager.Instance.currentSelectedChatChannel.channelMembers).profileUrl, FolderLocation.Profile, FileSavedCallback, ChatManager.Instance.currentSelectedChatChannel.channelUrl);
                }
            }*/
        }

        /*void FileSavedCallback (bool success)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("FileSavedCallback" + success);
            #endif

//            userProfileImage.FreakSetTexture(ChatManager.Instance.currentSelectedChatChannel.imageCache);
//            return;

            if (File.Exists(profileImageFilePath))
            {

                if (imageHandler.GetResizedTexture(profileImageFilePath, ChatManager.Instance.currentSelectedChatChannel.channelUrl) != null)
                {
                    userProfileImage.FreakSetSprite (imageHandler.GetResizedTexture (profileImageFilePath, ChatManager.Instance.currentSelectedChatChannel.otherUserId));
                }
                else
                {
                    LoadImage ();
//                    Texture2D texture = null;
//                    byte[] fileByte = File.ReadAllBytes(profileImageFilePath);
//                    texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
//                    texture.LoadImage(fileByte);
//                    
//                    Texture2D resizedTexture = TextureResize.ResizeTexture (userProfileImage.rectTransform, texture);
//                    imageHandler.AddResizedTexture (resizedTexture, profileImageFilePath, ChatManager.Instance.currentSelectedChatChannel.otherUserId);
//                    userProfileImage.FreakSetSprite (resizedTexture);
                }
            }
        }*/

//        void LoadImage()
//        {
//            if (this.gameObject.activeInHierarchy)
//            {
//                StartCoroutine (LoadImageFromLocal (profileImageFilePath));
//            }
//            else
//            {
//                Invoke ("LoadImage", 0.5f);
//            }
//        }

//        IEnumerator LoadImageFromLocal (string path)
//        {
//            var www = new WWW("file://" + path);
//            yield return www;
//            while (!www.isDone)
//            {
//                yield return null;
//            }
//
//            if (string.IsNullOrEmpty(www.error))
//            {
//                //            Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
//                //            img.sprite = Sprite.Create(www.texture, rect, new Vector2(0, 0), 1f);
//
//                Texture2D resizedTexture = TextureResize.ResizeTexture (150f, 150f, www.texture);
//                if (string.IsNullOrEmpty (ChatManager.Instance.currentSelectedChatChannel.otherUserId))
//                    imageHandler.AddResizedTexture (resizedTexture, profileImageFilePath, ChatManager.Instance.currentSelectedChatChannel.channelUrl);
//                else
//                    imageHandler.AddResizedTexture (resizedTexture, profileImageFilePath, ChatManager.Instance.currentSelectedChatChannel.otherUserId);
//                userProfileImage.FreakSetSprite (resizedTexture);
//                resizedTexture = null;
//            }
//            else
//            {
//                Debug.LogError("url ::: " + path + www.error);
//            }
//
////            Resources.UnloadUnusedAssets ();
//        }

        #endregion //User/Group Profile Image

        void DisplayMessages ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("CHECKTIME#### "+Time.realtimeSinceStartup);
            #endif

            InitHeaderPanelProperty();
            ShowProfileImage();
            ChatManager.Instance.MarkAsRead();
            IntitMessage();

            if (messageBoxList.Count == 0)
                return;

//            if (scrolledRow != (messageBoxList.Count -1))
//            {
//                ReloadOrRefreshCell ();
//                float cellHeight = GetHeightOfRow (messageBoxList.Count - 1);
//                Debug.Log("###################");
//                SetTableViewTOBottom();
//            }

            if (ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount > 0)
            {
                MarkAsRead();
                SaveChangedMessageToCache ();
            }

            if (ChatManager.Instance.forwardingMessages.Count > 0)
            {
                Invoke ("ForwardMessage", 1f);
            }

            if (PlayerPrefs.HasKey ("DirectFileSend") && ChatManager.Instance.directSendMessageFileType != null)
            {
                DuplicateMessageBox (ChatManager.Instance.directSendMessageFileType, string.Empty);
                PlayerPrefs.DeleteKey("DirectFileSend");
            }

            if (PlayerPrefs.HasKey ("DirectMessageSend"))
            {
                DuplicateMessageBox(PlayerPrefs.GetString("DirectMessageSend"), "");
                PlayerPrefs.DeleteKey("DirectMessageSend");
            }

            ChatManager.Instance.MarkAsRead();
            CheckForMessagePanel();
        }

        void SaveChangedMessageToCache ()
        {
            CacheManager.SaveNewMessage (ChatManager.Instance.currentSelectedChatChannel);
        }

        void NewMessageReceived (ChatMessage newMessage)
        {
            Debug.Log("NewMessageReceived " + newMessage.message);

            if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(ChatManager.Instance.currentSelectedChatChannel.channelUrl) == false)
            {
                UIController.Instance.PlayReceiveMessageSound ();
            }
            /*if (newMessage.messageSender.userId != ChatManager.Instance.UserId)
            {
            }*/

//            ChatManager.Instance.MarkAsRead();
        }

//        List<MessageBoxBase> messageBoxList = new List<MessageBoxBase>();
        List<ChatMessageTableData> messageBoxList = new List<ChatMessageTableData>();
        ChatMessage mChatMessage;
        bool isInitializing = false;
        void IntitMessageZZ ()
        {
            if (isInitializing)
            {
                Invoke ("IntitMessage", 1f);
            }
            isInitializing = true;

            if (messageBoxList == null)
                messageBoxList = new List<ChatMessageTableData>();

            mChatMessage = new ChatMessage();

            for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.messagesList.Count; i++)
            {
                ChatMessageTableData messageBox = MessageBoxHandler.GetMessageTableDataByType(ChatManager.Instance.currentSelectedChatChannel.messagesList[i]);

                if (messageBox != null)
                {
                    
                    if (mChatMessage != null)
                    {
                        if (FreakChatUtility.GetDisplayTimeStamp(ChatManager.Instance.currentSelectedChatChannel.messagesList[i].createdAt) !=
                        FreakChatUtility.GetDisplayTimeStamp(mChatMessage.createdAt))
                        {
                            ChatMessageTableData _messageBox = new ChatMessageTableData();
                            _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                            _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                            if (CheckMessageBoxInList(_messageBox) == false)
                            {
                                Debug.Log("***********************");
                                messageBoxList.Add(_messageBox);
                            }
                        }
                    }

                    mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];

                    if (messageBox.chatMessage != null)
                    {
                        if (CheckMessageBoxInList(messageBox) == false)
                        {
                            messageBoxList.Add(messageBox);
                        }
                    }
                }
            }

            isInitializing = false;

            Debug.Log("IntitMessage ------>>>>> " + Time.realtimeSinceStartup);
        }

        void IntitMessage ()
        {
            Debug.Log("###################");
            if (isInitializing)
            {
                Invoke ("IntitMessage", 1f);
            }
            isInitializing = true;

            if (messageBoxList == null)
                messageBoxList = new List<ChatMessageTableData>();

            mChatMessage = new ChatMessage();

            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            int msgCount = messageBoxList.Count;
            for (int i = count; i >= 0; i--)
            {
                ChatMessageTableData messageBox = MessageBoxHandler.GetMessageTableDataByType(ChatManager.Instance.currentSelectedChatChannel.messagesList[i]);

                if (messageBox != null)
                {
                    if (messageBox.chatMessage != null)
                    {
                        if (CheckMessageBoxInList(messageBox) == false)
                        {
                            messageBoxList.Insert(msgCount, messageBox);
                        }
                    }

                    mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];

                    if (mChatMessage != null)
                    {
                        if (i > 0)
                        {
                            if (FreakChatUtility.GetDisplayTimeStamp(ChatManager.Instance.currentSelectedChatChannel.messagesList[(i - 1)].createdAt) !=
                            FreakChatUtility.GetDisplayTimeStamp(mChatMessage.createdAt))
                            {
                                ChatMessageTableData _messageBox = new ChatMessageTableData();
                                _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                                _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                                if (CheckMessageBoxInList(_messageBox) == false)
                                {
                                    Debug.Log("***********************");
                                    messageBoxList.Insert(0, _messageBox);
                                }
                            }
                        }
                        else if (i == 0)
                        {
                            ChatMessageTableData _messageBox = new ChatMessageTableData();
                            _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                            _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                            if (CheckMessageBoxInList(_messageBox) == false)
                            {
                                Debug.Log("***********************");
                                messageBoxList.Insert(0, _messageBox);
                            }
                        }
                    }

                    if (count > 10 && messageBoxList.Count == 10)
                    {
//                        ReloadOrRefreshCell();
//                        float cellHeight = GetHeightOfRow(messageBoxList.Count - 1);
//                        SetTableViewTOBottom (10);
                    }
                //                mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                }
            }

            isInitializing = false;
            Debug.Log("###################");
            m_EnhancedScrollView.ReloadData();
            JumpToLast();
//            _reloadScrollerFrameCountLeft = 1;
        }

        void IntitMessageFirstTime ()
        {
            StartCoroutine(LoadAsync());
        }

        IEnumerator LoadAsync ()
        {
            isInitializing = true;
            
            if (messageBoxList == null)
                messageBoxList = new List<ChatMessageTableData>();
            
            mChatMessage = null;

            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            int loadCount = -1;
            while (true)
            {
                for (int i = count; i >= 0; i--)
                {
                    ChatMessageTableData messageBox = MessageBoxHandler.GetMessageTableDataByType(ChatManager.Instance.currentSelectedChatChannel.messagesList[i]);

                    if (messageBox != null)
                    {
                        if (messageBox.chatMessage != null)
                        {
                            if (CheckMessageBoxInList(messageBox) == false)
                            {
                                if (messageBoxList.Count == 0)
                                {
                                    messageBoxList.Add(messageBox);
                                }
                                else
                                {
                                    messageBoxList.Insert(0, messageBox);
                                }
                            }
                        }

                        mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];

                        if (mChatMessage != null)
                        {
                            if (i > 0)
                            {
                                if (FreakChatUtility.GetDisplayTimeStamp(ChatManager.Instance.currentSelectedChatChannel.messagesList[(i - 1)].createdAt) !=
                                    FreakChatUtility.GetDisplayTimeStamp(mChatMessage.createdAt))
                                {
                                    ChatMessageTableData _messageBox = new ChatMessageTableData();
                                    _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                                    _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                                    if (CheckMessageBoxInList(_messageBox) == false)
                                    {
                                        Debug.Log("***********************");
                                        messageBoxList.Insert(0, _messageBox);
                                    }
                                }
                            }
                            else if (i == 0)
                            {
                                ChatMessageTableData _messageBox = new ChatMessageTableData();
                                _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];
                                _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                                if (CheckMessageBoxInList(_messageBox) == false)
                                {
                                    Debug.Log("***********************");
                                    messageBoxList.Insert(0, _messageBox);
                                }
                            }
                        }

                        //                mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];

                        if (count > 5 && messageBoxList.Count == 5)
                        {
//                            ReloadOrRefreshCell();
//                            float cellHeight = GetHeightOfRow(messageBoxList.Count - 1);
//                            SetTableViewTOBottom (4);
                            _reloadScrollerFrameCountLeft = 1;
                        }
                    }

                    loadCount = i;
                }

                if (loadCount == 0)
                {
                    break;
                }
                yield return null;
            }

            InitHeaderPanelProperty();
            ShowProfileImage();
            ChatManager.Instance.MarkAsRead();

            isInitializing = false;

//            ReloadOrRefreshCell ();
//            float _cellHeight = GetHeightOfRow (messageBoxList.Count - 1);
//            SetTableViewTOBottom (messageBoxList.Count - 1);


            if (ChatManager.Instance.currentSelectedChatChannel.unreadMessageCount > 0)
            {
                MarkAsRead();
                SaveChangedMessageToCache ();
            }

            if (ChatManager.Instance.forwardingMessages.Count > 0)
            {
                Invoke ("ForwardMessage", 1f);
            }

            if (PlayerPrefs.HasKey ("DirectFileSend") && ChatManager.Instance.directSendMessageFileType != null)
            {
                DuplicateMessageBox (ChatManager.Instance.directSendMessageFileType, string.Empty);
                PlayerPrefs.DeleteKey("DirectFileSend");
            }

            if (PlayerPrefs.HasKey ("DirectMessageSend"))
            {
                DuplicateMessageBox(PlayerPrefs.GetString("DirectMessageSend"), "");
                PlayerPrefs.DeleteKey("DirectMessageSend");
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("IntitMessage__ @@@@ ------>>>>> " + Time.realtimeSinceStartup + "IntitMessage__Message Count --- >"+ messageBoxList.Count);
            #endif

            // set up our frame countdown so that we can reload the scroller on subsequent frames
            _reloadScrollerFrameCountLeft = 1;
        }

        bool CheckMessageBoxInList (ChatMessageTableData messageBox)
        {
            int count = messageBoxList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (messageBoxList[i].tableDataType == ChatMessageTableData.TableDataType.TimeStampBox)
                {
                    if (messageBox.tableDataType == ChatMessageTableData.TableDataType.TimeStampBox)
                    {
                        if (messageBoxList[i].chatMessage.messageId == messageBox.chatMessage.messageId ||
                            messageBoxList[i].chatMessage.customField == messageBox.chatMessage.customField)
                        {
                            messageBoxList [i].chatMessage = messageBox.chatMessage;
                            return true;
                        }
                    }
                }
                else if (messageBox.tableDataType != ChatMessageTableData.TableDataType.TimeStampBox)
                {
                    if (messageBoxList[i].chatMessage.messageId == 0)
                    {
                        if (messageBoxList[i].chatMessage.customField == messageBox.chatMessage.customField)
                        {
                            messageBoxList [i].chatMessage = messageBox.chatMessage;
                            return true;
                        }
                    }
                    else
                    {
                        if (messageBoxList[i].chatMessage.messageId == messageBox.chatMessage.messageId)
                        {
                            messageBoxList[i].chatMessage = messageBox.chatMessage;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        void SetTableViewTOBottom()
		{
//			m_tableView.scrollY = m_tableView.scrollableHeight + (cellHeight / 3f);
//            Debug.Log(m_EnhancedScrollView.isScrolling);
//            m_EnhancedScrollView.JumpToDataIndex(cellCount, 1f, 1f);

            CancelInvoke("JumpToLast");
            Invoke("JumpToLast", 0.05f);
		}

        void JumpToLast ()
        {
            if (messageBoxList.Count > 0)
            {
                m_EnhancedScrollView.RefreshActiveCellViews();
                m_EnhancedScrollView.JumpToDataIndex((messageBoxList.Count - 1), 1f, 1f, true, EnhancedScroller.TweenType.immediate, 0);
            }
        }

        void HandleCellViewVisibilityChangedDelegate (EnhancedScrollerCellView cellView)
        {
//            Debug.Log("cellView.cellIdentifier " + cellView.cellIdentifier + " ::cellView.cellIndex:: " + cellView.cellIndex);
        }

        void DisplayDuplicateMessage ()
        {
            if (messageBoxList == null)
                messageBoxList = new List<ChatMessageTableData>();

            mChatMessage = new ChatMessage();

            int lastCount = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            if (lastCount > 0)
            {
                mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[lastCount - 1];
            }

            ChatMessageTableData messageBox = MessageBoxHandler.GetMessageTableDataByType(ChatManager.Instance.currentSelectedChatChannel.messagesList[lastCount]);

            if (messageBox != null)
            {
                if (mChatMessage != null)
                {
                    if (FreakChatUtility.GetDisplayTimeStamp(ChatManager.Instance.currentSelectedChatChannel.messagesList[lastCount].createdAt) !=
                    FreakChatUtility.GetDisplayTimeStamp(mChatMessage.createdAt))
                    {
                        ChatMessageTableData _messageBox = new ChatMessageTableData();
                        _messageBox.chatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[lastCount];
                        _messageBox.tableDataType = ChatMessageTableData.TableDataType.TimeStampBox;
                        if (CheckMessageBoxInList(_messageBox) == false)
                        {
                            Debug.Log("***********************");
                            messageBoxList.Add(_messageBox);
                        }
                    }
                }

                if (messageBox.chatMessage != null)
                {
                    if (CheckMessageBoxInList(messageBox) == false)
                    {
                        messageBoxList.Add(messageBox);
                    }
                }

//                float cellHeight = GetHeightOfRow(messageBoxList.Count - 1);
                Debug.Log("###################");
//                SetTableViewTOBottom ();
                _reloadScrollerFrameCountLeft = 1;
//                JumpToLast();
     //           m_tableView.ReloadData();
                CheckForMessagePanel();

                Debug.Log("IntitMessage__ @@@@ ------>>>>> " + Time.realtimeSinceStartup);
            }
        }

        /*
        void IntitMessageZZ ()
        {
            if (isInitializing)
            {
                Invoke ("IntitMessage", 5f);
            }

            isInitializing = true;

            if (messageBoxList == null)
                messageBoxList = new List<MessageBoxBase>();
            
            mChatMessage = new ChatMessage();

            for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.messagesList.Count; i++)
            {
                MessageBoxBase messageBox = MessageBoxHandler.GetMessageByType(ChatManager.Instance.currentSelectedChatChannel.messagesList[i]);

                if (mChatMessage != null)
                {
                    if (FreakChatUtility.GetDisplayTimeStamp (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].createdAt) !=
                        FreakChatUtility.GetDisplayTimeStamp (mChatMessage.createdAt))
                    {
                        TimeStampBox _timeStampMsg = new TimeStampBox();
                        _timeStampMsg.InitMessageInfo(ChatManager.Instance.currentSelectedChatChannel.messagesList[i]);
                        
                        MessageBoxBase _messageBox = _timeStampMsg as MessageBoxBase;
                        if (CheckMessageBoxInList (_messageBox) == false)
                        {
                            messageBoxList.Add(_messageBox);
                        }
                    }
                }

                mChatMessage = ChatManager.Instance.currentSelectedChatChannel.messagesList[i];

                if (messageBox.chatMessage != null)
                {
                    if (CheckMessageBoxInList (messageBox) == false)
                    {
                        messageBoxList.Add(messageBox);
                    }
                }
            }

            isInitializing = false;
        }

        bool CheckMessageBoxInListZZ (MessageBoxBase messageBox)
        {
            for (int i = 0; i < messageBoxList.Count; i++)
            {
                if (messageBoxList[i] is TimeStampBox)
                {
                    if (messageBox is TimeStampBox)
                    {
                        if (messageBoxList[i].chatMessage.messageId == messageBox.chatMessage.messageId)
                        {
                            messageBoxList [i].InitMessageInfo (messageBox.chatMessage);
                            return true;
                        }
                    }
                }
                else
                {
                    if (messageBoxList[i].chatMessage.messageId == messageBox.chatMessage.messageId)
                    {
                        messageBoxList [i].InitMessageInfo (messageBox.chatMessage);
                        return true;
                    }
                }
            }

            return false;
        }
*/
        void CheckForTempMessageBox (ChatMessage chatMessage)
        {
            for(int j=0; j<tempFileMessageList.Count; j++)
            {
                if (tempFileMessageList[j].fileName == chatMessage.fileName &&
                    tempFileMessageList[j].fileSize == chatMessage.fileSize)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("CheckForTempMessageBox " + tempFileMessageList [j].fileName);
                    #endif

                    string urlFilePath = chatMessage.fileUrl;

                    if (System.IO.Path.HasExtension (urlFilePath) == false)
                        urlFilePath += System.IO.Path.GetExtension (chatMessage.fileName);

                    string filePath = StreamManager.LoadFilePath(System.IO.Path.GetFileName(urlFilePath), ChatFileType.GetFolderLocation(chatMessage.fileType), true);

                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("CheckForTempMessageBox " + filePath);
                    #endif
                    if (System.IO.File.Exists(tempFileMessageList[j].filePath))
                    {
                        System.IO.File.Move(tempFileMessageList[j].filePath, filePath);
                        System.IO.File.Delete(tempFileMessageList[j].filePath);
                    }

                    tempFileMessageList.RemoveAt(j);
                    break;
                }
            }
        }

        /*void UpdateNewMessage ()
        {
            IntitMessage();
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Update New Message >> " + messageBoxList.Count);
            #endif
            ReloadOrRefreshCell ();
            if (ChatManager.Instance.currentSelectedChatChannel.isPreviousMsg)
            {
				m_tableView.scrollY = m_tableView.GetScrollYForRow(0,true);
            }
            else
            {
//				m_tableView.scrollY = m_tableView.GetScrollYForRow(GetNumberOfRowsForTableView(m_tableView)-8,false) + 5;
                m_tableView.scrollY = m_tableView.GetScrollYForRow ((messageBoxList.Count - 1), false);
				scrollRect.verticalNormalizedPosition = 0;
				
            }
        }*/

#region Sending In Offline first

        private List<ChatMessage> tempFileMessageList = new List<ChatMessage>();

        public void DuplicateMessageBox (string message, string data = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("DuplicateMessageBox " + message);
            #endif

            ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage (data);
            _chatMsg.messageType = ChatMessage.MessageType.Message;
            _chatMsg.message = message;
            _chatMsg.data = data;

            ChatChannelHandler.OnSendMessage (ChatManager.Instance.currentSelectedChatChannel, _chatMsg);

//            DisplayMessages ();
            DisplayDuplicateMessage();

            MessageQueueHandler.Instance.SendUserMessage(ChatManager.Instance.currentChannel,
                ChatManager.Instance.currentSelectedChatChannel.channelUrl, 
                _chatMsg.message, _chatMsg.data, _chatMsg.customField, null);
        }

        public void DuplicateMessageBox (ChatFileType fileType, string data = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("DuplicateMessageBox " + fileType.fileType);
            #endif

            ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage (data);

            _chatMsg.messageType = ChatMessage.MessageType.FileLink;
            _chatMsg.fileName = System.IO.Path.GetFileName (fileType.filePath);
            _chatMsg.fileType = fileType.fileType;
            _chatMsg.fileSize = fileType.fileSize;
            _chatMsg.filePath = fileType.filePath;
            _chatMsg.fileUrl = fileType.filePath;

            ChatChannelHandler.OnSendMessage (ChatManager.Instance.currentSelectedChatChannel, _chatMsg);

//            DisplayMessages ();
            DisplayDuplicateMessage();

            MessageQueueHandler.Instance.SendFileMessage (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, fileType, _chatMsg.customField, data);
        }

#endregion //SHOW FAKE SEND MESSAGE

        public void OnEscapeButtonPressed ()
        {
//            ActivateBothPanel(false);
            SwitchPanelState(PanelState.CloseAll); //TTT/ SwitchPanel(PanelState.CloseAll);
        }

        public void TagFriendSelected (ChatUser mFreakMember)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _meta_Data = new FreakChatSerializeClass.NormalMessagerMetaData();
            _meta_Data.messageType = ChatMessage.MetaMessageType.TagFriend;
            _meta_Data.userid = mFreakMember.userId;
            _meta_Data.username = mFreakMember.nickname;

            inputFieldPanelUI.OnSendTagFriend(_meta_Data);
        }

#region Callback

        void InitCallback ()
        {
            ChatManager.Instance.NewMessageReceived                 = NewMessageReceived;
            ChatManager.Instance.onMessageTypingCallback            = OnMessageTyping;
//            ChatManager.Instance.OnUserMessageSendFail              = OnUserMessageSendFail;
//            ChatManager.Instance.OnFileMessageSendFail              = OnFileMessageSendFail;
            ChatManager.Instance.OnMessageSentCallback              = OnMessageSendCallback;
            ChatManager.Instance.onMessageDeliveredCallback         = OnMessageDeliveredCallback;
            ChatManager.Instance.onReadReceivedCallback             = OnMessageReadCallback;
            InternetConnection.Instance.OnInternetConnectionBack    = SendMessageWhenInternetBack;
        }

        void RemoveCallback ()
        {
            ChatManager.Instance.NewMessageReceived                 = null;
//            ChatManager.Instance.OnUserMessageSendFail              = null;
//            ChatManager.Instance.OnFileMessageSendFail              = null;
            ChatManager.Instance.onReadReceivedCallback             = null;
            ChatManager.Instance.OnMessageSentCallback              = null;
            ChatManager.Instance.onMessageTypingCallback            = null;
            ChatManager.Instance.onMessageDeliveredCallback         = null;
            InternetConnection.Instance.OnInternetConnectionBack    = null;
        }

        /*void OnUserMessageSendFail(ChatManager.MessageSendQueue message)
        {
            if (ChatManager.Instance.isInternetConnected)
            {
                StartCoroutine (SendOnUserMessageAfterSomeTime (message));
            }
            else
            {
                mMessageSendingQueueList.Add (message);
            }
        }*/

        public void GetGroupChannelCallback (SendBird.GroupChannel groupChannel)
        {
            if (groupChannel != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("GetGroupChannelCallback " + groupChannel.Url);
                #endif
                ChatManager.Instance.currentChannel = groupChannel;
                ChatManager.Instance.MarkAsRead();
            }

            LoadChannel ();

            if (onStart == false)
            {
                onStart = true;
                // SEND ANY PENDING TEXT MESSAGE...
                SendMessageAutomatically ();
            }
        }

        public void RefreshChannel ()//SendBird.GroupChannel groupChannel)
        {
            /*if (groupChannel.UnreadMessageCount > 0)
            {
                ChatManager.Instance.RefreshForNewMessages();
            }

            ChatChannelHandler.UpdateChatChannel (ChatManager.Instance.currentSelectedChatChannel, groupChannel);*/

            InitHeaderPanelProperty();
            ShowProfileImage();
        }

        void OnMessageSendCallback(SendBird.BaseMessage baseMessage)
        {
            long _MessageId = baseMessage.MessageId;
            string customType = string.Empty;
            if (baseMessage is SendBird.UserMessage)
            {
                SendBird.UserMessage msg = baseMessage as SendBird.UserMessage;
                customType = msg.CustomType;
            }
            else if (baseMessage is SendBird.FileMessage)
            {
                SendBird.FileMessage msg = baseMessage as SendBird.FileMessage;
                customType = msg.CustomType;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Mesage Is send to " + ChatManager.Instance.currentSelectedChatChannel.messagesList [ChatManager.Instance.currentSelectedChatChannel.messagesList.Count-1].customField);
            #endif
            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                //                Debug.Log (i + ". MESSAGE ID===== " + ChatManager.Instance.currentSelectedChatChannel.messagesList [i].message);
                if (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].customField == customType)
                {
                    ChatManager.Instance.currentSelectedChatChannel.messagesList [i].receiptStatus = ChatMessage.MessageReceiptType.Send;
                    ChatManager.Instance.currentSelectedChatChannel.messagesList [i].messageState = ChatMessage.MessageState.Normal;
                    RefreshMessageBoxListReceipt (customType, ChatMessage.MessageReceiptType.Send);
//                    ReloadOrRefreshCell ();
//                    m_EnhancedScrollView.RefreshActiveCellViews();
//                    float cellHeight = GetHeightOfRow (messageBoxList.Count - 1);
                    Debug.Log("###################");
                    JumpToLast();
                    break;
                }
            }

            if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(ChatManager.Instance.currentSelectedChatChannel.channelUrl) == false)
            {
                UIController.Instance.PlaySendMessageSound();
            }
        }

        void OnMessageDeliveredCallback(SendBird.BaseChannel baseChannel)
        {
            SendBird.GroupChannel _groupChannel = baseChannel as SendBird.GroupChannel;
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Mesage Is Delivered to " + JsonConvert.SerializeObject (_groupChannel));
            #endif

            if (_groupChannel.LastMessage != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Mesage Is Delivered to " + ChatManager.Instance.currentSelectedChatChannel.messagesList.Count);
                #endif
                for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.messagesList.Count; i++)
                {                    
                    if (ChatManager.Instance.currentSelectedChatChannel.messagesList [i].messageId == _groupChannel.LastMessage.MessageId)
                    {
                        ChatManager.Instance.currentSelectedChatChannel.messagesList [i].receiptStatus = ChatMessage.MessageReceiptType.Delivered;
                        RefreshMessageBoxListMetaReceipt (_groupChannel.LastMessage.MessageId, ChatMessage.MessageReceiptType.Delivered);
//                        ReloadOrRefreshCell ();
                        m_EnhancedScrollView.RefreshActiveCellViews();
                        SaveChangedMessageToCache ();
                        break;
                    }
                }
            }
        }

        void OnMessageReadCallback(SendBird.GroupChannel groupChannel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Mesage Is Read by " + JsonConvert.SerializeObject (groupChannel));
            #endif

            bool _messageUpdated = false;
            if (groupChannel.LastMessage != null && groupChannel.Url == ChatManager.Instance.currentSelectedChatChannel.channelUrl)
            {
                _reloadScrollerFrameCountLeft = 1;

//                int count = messageBoxList.Count - 1;
//                for (int i = count; i >= 0; i--)
//                {
//                    #if UNITY_EDITOR && UNITY_DEBUG
//                    Debug.Log(messageBoxList[i].chatMessage.message);
//                    #endif
//                    if (messageBoxList[i].tableDataType != ChatMessageTableData.TableDataType.TimeStampBox &&
//                        messageBoxList[i].chatMessage.messageSender.userId == ChatManager.Instance.UserId)
//                    {
//                        /*if (messageBoxList[i].chatMessage.receiptStatus == ChatMessage.MessageReceiptType.Read &&
//                            m_tableView.GetCellAtRow(i).GetComponent<MessageBoxBase>().IsReadReceipt ())
//                        {
//                            break;
//                        }*/
//
//                        if (messageBoxList[i].chatMessage.receiptStatus != ChatMessage.MessageReceiptType.Read)
//                        {
//                            #if UNITY_EDITOR && UNITY_DEBUG
//                            Debug.Log("==>> " + messageBoxList[i].chatMessage.message);
//                            Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
//                            #endif
//                            messageBoxList[i].chatMessage.receiptStatus = ChatMessage.MessageReceiptType.Read;
//                            ChatChannelHandler.MarkLastMessageAsRead(ChatManager.Instance.currentSelectedChatChannel, messageBoxList[i].chatMessage.messageId);
//                            _messageUpdated = true;
//
//                            break;
//                        }
//                    }
//                }
//
//                if (_messageUpdated)
//                {
////                    ReloadOrRefreshCell ();
////                    m_EnhancedScrollView.RefreshActiveCellViews();
//                    _reloadScrollerFrameCountLeft = 1;
//                }
            }
        }

        /*void UpdatePreviousMessageReceipt (int index)
        {
            Debug.Log ("UpdatePreviousMessageReceipt " + index);
            for (int i = index; i >= 0; i--)
            {
                if (ChatManager.Instance.currentSelectedChatChannel.messagesList [i].messageSender.userId == ChatManager.Instance.UserId)
                {
                    if (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].receiptStatus != ChatMessage.MessageReceiptType.Read)
                    {
                        ChatManager.Instance.currentSelectedChatChannel.messagesList [i].receiptStatus = ChatMessage.MessageReceiptType.Read;
                        RefreshMessageBoxListMetaReceipt (ChatManager.Instance.currentSelectedChatChannel.messagesList [i].messageId, ChatMessage.MessageReceiptType.Read);
                    }
                    else if (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].receiptStatus == ChatMessage.MessageReceiptType.Read)
                    {
                        break;
                    }
                }
            }

            ReloadOrRefreshCell ();
        }*/

        void OnMessageTyping(bool isTyping, List<SendBird.User> userIds)
        {
            if (isTyping)
            {
                for (int i = 0; i < userIds.Count; i++)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("OnMessageTyping " + isTyping + "By " + userIds[i].UserId);
                    #endif
                    
                    typingUsername = userIds [i].Nickname;
                    DisplayIsTyping ();
                }
            }
            else
            {
                if (typingRoutine != null)
                    StopCoroutine (typingRoutine);

                CancelInvoke ("DisplayIsTyping");
                this.isTyping = false;
                lastSeenText.text = tempLastSeenText;
            }
        }
#endregion

#region Resending Message
        /*IEnumerator SendOnUserMessageAfterSomeTime (ChatManager.MessageSendQueue message)
        {
            yield return new WaitForSeconds (3f);
            ChatManager.Instance.SendUserMessage(message.msg, message.data, message.customType);
        }*/

        /*void OnFileMessageSendFail(ChatManager.FileSendQueue message)
        {
            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].customField == message.customType)
                {
                    ChatManager.Instance.currentSelectedChatChannel.messagesList[i].messageState = ChatMessage.MessageState.Retry;
                    ReloadOrRefreshCell ();
                    break;
                }
            }

            if (ChatManager.Instance.isInternetConnected)
            {
                StartCoroutine (SendFileMessageAfterSomeTime (message));
            }
        }*/

        /*IEnumerator SendFileMessageAfterSomeTime (ChatManager.FileSendQueue message)
        {
            yield return new WaitForSeconds (3f);
            RefreshMessageBoxList (message.customType);
            if (message.fileInfo != null)
            {
                ChatManager.Instance.SendFileMessage(message.fileInfo, message.customType, message.data);
            }
            else
            {
                ChatManager.Instance.SendFileMessageWithUrl (message.fileUrl, message.name, message.type, message.size, message.data, message.customType);
            }
        }*/
#endregion

#region Update Message List

        void RefreshMessageBoxList (string customField)
        {
            int count = messageBoxList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (messageBoxList[i].tableDataType != ChatMessageTableData.TableDataType.TimeStampBox &&
                    messageBoxList[i].chatMessage.customField == customField)
                {
                    messageBoxList [i].chatMessage.messageState = ChatMessage.MessageState.Loading;
                    ReloadOrRefreshCell ();
                    break;
                }
            }
        }

        void RefreshMessageBoxListReceipt (string customType, ChatMessage.MessageReceiptType receiptType)
        {
            int count = messageBoxList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (messageBoxList[i].tableDataType != ChatMessageTableData.TableDataType.TimeStampBox &&
                    messageBoxList[i].chatMessage.customField == customType)
                {
                    messageBoxList [i].chatMessage.receiptStatus = receiptType;
                    messageBoxList [i].chatMessage.messageState = ChatMessage.MessageState.Loading;
                }
            }
        }

        void RefreshMessageBoxListMetaReceipt (long messageId, ChatMessage.MessageReceiptType receiptType)
        {
            int count = messageBoxList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (messageBoxList[i].tableDataType != ChatMessageTableData.TableDataType.TimeStampBox &&
                    messageBoxList[i].chatMessage.messageId == messageId)
                {
                    messageBoxList [i].chatMessage.receiptStatus = receiptType;
                }
            }
        }

#endregion

#region Message Typing

        private string typingUsername;
        private bool isTyping;
        private Coroutine typingRoutine;
        void DisplayIsTyping ()
        {
            if (isTyping) {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("_KOSA_+_SAS");
                #endif
                Invoke ("DisplayIsTyping", 1f);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Started "+isTyping);
            #endif
            isTyping = true;

            if (typingRoutine != null)
                StopCoroutine (typingRoutine);

            typingRoutine = StartCoroutine (IsTyping (typingUsername));
        }

        IEnumerator IsTyping(string username)
        {
            string _username = "typing";
            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                _username = username + " is typing";
            }

            lastSeenText.text = string.Empty;
            lastSeenText.text = _username + " .";
            yield return new WaitForSeconds (0.5f);
            lastSeenText.text = _username + " . " + ".";
            yield return new WaitForSeconds(0.5f);
            lastSeenText.text = _username + " . " + ". " + ".";
            yield return new WaitForSeconds(0.5f);

            isTyping = false;

            DisplayIsTyping ();
        }

#endregion

		//Register as the TableView's delegate (required) and data source (optional)
		//to receive the calls
#region ITableViewDataSource

//		//Will be called by the TableView to know how many rows are in this table
//		public int GetNumberOfRowsForTableView(TableView tableView) 
//		{
//            if (messageBoxList != null) {
//                return messageBoxList.Count;
//            }
//			return 0;
//		}
//
//		//Will be called by the TableView to know what is the height of each row
//		public float GetHeightForRowInTableView(TableView tableView, int row) {
//			return GetHeightOfRow(row);
//		}
//
//		//Will be called by the TableView when a cell needs to be created for display
//		public TableViewCell GetCellForRowInTableView(TableView tableView, int row) 
//		{
//            MessageBoxBase cell = tableView.GetReusableCell (messageBoxList [row].GetDataType) as MessageBoxBase;
//            cell = MessageBoxHandler.GetMessageBoxInstance (cell, messageBoxList [row]);
//            cell.InitMessageInfo(messageBoxList[row].chatMessage);
//            cell.InitMessage();
//			
//			cell.name = "DynamicHeightCellInstance_" + (++m_numInstancesCreated).ToString();
//			cell.onCellHeightChanged.AddListener(OnCellHeightChanged);
//			cell.rowNumber = row;
//
//            if (cell.GetType () == typeof(Freak.Chat.ImageMessageBox)) 
//			{
//				cell.height = cell.m_cellHeightSlider.maxValue;
//			}
//
//            /*if (cell is TextMessageBox) 
//			{
//                #if UNITY_EDITOR && UNITY_DEBUG
////				Debug.Log("Display Last Msg Size*** " );
//                #endif
//
//				//cell.m_cellHeightSlider.maxValue = cell.GetComponent<TextMessageBox>().
//				//cell.height = cell.m_cellHeightSlider.maxValue;
//			}*/
//
//            OnCellHeightChanged(row, cell.height);
//
//            ScrollDown (row);
//
//			return cell;
//		}

		#endregion

		#region EnhancedScroller Handlers
        private int _reloadScrollerFrameCountLeft = -1;

        /// <summary>
        /// In this function, we will reload the scroller on multiple frames.
        /// This is because the content size fitter in the cell view doesn't process
        /// the sizes until a frame after the text is set. Since we need the size of the
        /// fitter, we have to keep reloading until the data is available.
        /// </summary>
        void LateUpdate()
        {
            // only process if we have a countdown left
            if (_reloadScrollerFrameCountLeft != -1)
            {
                // skip the first frame (frame countdown 1) since it is the one where we set up the scroller text.
                if (_reloadScrollerFrameCountLeft < 1)
                {

                    // reload two times, the first to put the newly set content size fitter values into the model,
                    // the second to set the scroller's cell sizes based on the model.
                    m_EnhancedScrollView.ReloadData();
                    m_EnhancedScrollView.ReloadData();
                    JumpToLast();
                }

                // decrement the frame count
                _reloadScrollerFrameCountLeft--;

                m_EnhancedScrollView.ScrollRect.verticalNormalizedPosition = 0;

            }
        }

		/// <summary>
		/// This tells the scroller the number of cells that should have room allocated. This should be the length of your data array.
		/// </summary>
		/// <param name="scroller">The scroller that is requesting the data size</param>
		/// <returns>The number of cells</returns>
		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			// in this example, we just pass the number of our data elements
//			Debug.Log("mFreakChachedChannelList.Count--- >"+ messageBoxList.Count);
			return messageBoxList.Count;
			//return 100;
		}

		/// <summary>
		/// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
		/// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
		/// cell size will be the width.
		/// </summary>
		/// <param name="scroller">The scroller requesting the cell size</param>
		/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
		/// <returns>The size of the cell</returns>
		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			// in this example, even numbered cells are 30 pixels tall, odd numbered cells are 100 pixels tall
			//return (dataIndex % 2 == 0 ? 30f : 100f);
			return messageBoxList[dataIndex].cellSize;
			//return 10;
		}
		/// <summary>
		/// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
		/// Some examples of this would be headers, footers, and other grouping cells.
		/// </summary>
		/// <param name="scroller">The scroller requesting the cell</param>
		/// <param name="dataIndex">The index of the data that the scroller is requesting</param>
		/// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
		/// <returns>The cell for the scroller to use</returns>
		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			// first, we get a cell from the scroller by passing a prefab.
			// if the scroller finds one it can recycle it will do so, otherwise
			// it will create a new cell.

//            AllTesting cellView = scroller.GetCellView(testPrefab) as AllTesting;
//            cellView.cellIdentifier = messageBoxList [cellIndex].GetDataType.ToString ();

			MessageBoxBase cellView = scroller.GetCellView(MessageBoxHandler.GetMessageBoxPrefab(messageBoxList [cellIndex])) as MessageBoxBase;
			cellView.cellIdentifier = messageBoxList [cellIndex].GetDataType.ToString ();

			// set the name of the game object to the cell's data index.
			// this is optional, but it helps up debug the objects in 
			// the scene hierarchy.
			//cellView.name = "Cell " + dataIndex.ToString();

			// in this example, we just pass the data to our cell's view which will update its UI
			//cellView.SetData(mFreakChachedChannelList[dataIndex]);

			//cellView = MessageBoxHandler.GetMessageBoxInstance (dataIndex, messageBoxList [dataIndex]);
			cellView.InitMessageInfo(messageBoxList[dataIndex]);
//            Debug.Log(dataIndex+":::"+cellIndex);
//            cellView.InitTest(cellIndex);
//			cellView.InitMessage();

			// return the cell to the scroller
			return cellView;
		}

		#endregion



        private int previousMessageBoxCount;
        void ReloadOrRefreshCell ()
        {
            Debug.Log("ReloadOrRefreshCell");
            #if UNITY_EDITOR && UNITY_DEBUG
            #endif
//          m_tableView.ReloadData ();
			m_EnhancedScrollView.ReloadData();
            /*if (previousMessageBoxCount < messageBoxList.Count)
            {
                previousMessageBoxCount = messageBoxList.Count;
                    
//              m_tableView.ReloadData ();

				m_EnhancedScrollView.ReloadData();
            }
            else
            {
//                m_tableView.RefreshVisibleRows ();

				m_EnhancedScrollView.RefreshVisibleRows();
            }*/
        }

        private int scrolledRow;
        void ScrollDown(int row)
        {
            if(row == messageBoxList.Count - 1 && scrolledRow != row)
            {
                scrolledRow = row;
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("Display Last Msg *** " + row + "Total Msgs === > "+ messageBoxList.Count);
                #endif
//                float cellHeight = GetHeightOfRow (messageBoxList.Count - 1);
                Debug.Log("###################");
//                SetTableViewTOBottom();
            }
        }

//        public void DownloadComplete (MessageBoxBase msgBase, bool success)
//        {
//            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("DownloadComplete Object");
//            #endif
//            msgBase.FileSavedCallback(success);
//
//            /*if (m_tableView.GetCellAtRow(msgBase.rowNumber) != null)
//            {
//                for (int i = 0; i < messageBoxList.Count; i++)
//                {
//                    if (messageBoxList[i].chatMessage.messageId == msgBase.chatMessage.messageId)
//                    {
//                        Debug.Log(messageBoxList[i].chatMessage.fileUrl);
//                    }
//                }
//
//                MessageBoxBase _base = m_tableView.GetCellAtRow(msgBase.rowNumber) as MessageBoxBase;
//                Debug.Log(_base.chatMessage.fileUrl + "Row: " + msgBase.rowNumber);
//                _base.FileSavedCallback(success);
//            }*/
//        }

        private float GetHeightOfRow(int row) {
            if (m_customRowHeights.ContainsKey(row)) {
                return m_customRowHeights[row];
            } else {
                return m_cellPrefab.height;
            }
		}

		private void OnCellHeightChanged(int row, float newHeight) {
			
//            Debug.Log(string.Format("Cell {0} height changed to {1}", row, newHeight));

            if (GetHeightOfRow(row) == newHeight)
            {
                return;
            }
			
            m_customRowHeights[row] = newHeight;
//            m_tableView.NotifyCellDimensionsChanged(row);
        }

  #region Send failed message Automatically

        void SendMessageAutomatically ()
        {
            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            int total = (count > 50) ? 50 : count;
            for (int i = 0; i <= total; i++)
            {
                if (ChatManager.Instance.currentSelectedChatChannel.messagesList[count].receiptStatus == ChatMessage.MessageReceiptType.Unknown &&
                    ChatManager.Instance.currentSelectedChatChannel.messagesList[count].messageId == 0)
                {
                    if (ChatManager.Instance.currentSelectedChatChannel.messagesList[count].messageType == ChatMessage.MessageType.Message)
                    {
                        ChatManager.MessageSendQueue _messagesend = new ChatManager.MessageSendQueue ();
                        _messagesend.msg        = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].message;
                        _messagesend.data       = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].data;
                        _messagesend.customType = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].customField;
                        
                        /*if (ChatManager.Instance.isInternetConnected == true)
                        {
                            MessageQueueHandler.Instance.SendUserMessage (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, 
                                _messagesend.msg, _messagesend.data, _messagesend.customType, null);
                        }
                        else
                        {
                        }*/
                        mMessageSendingQueueList.Add (_messagesend);
                    }
                    else if (ChatManager.Instance.currentSelectedChatChannel.messagesList[count].messageType == ChatMessage.MessageType.FileLink)
                    {
                        ChatManager.FileSendQueue _filemessagesend = new ChatManager.FileSendQueue ();
                        _filemessagesend.customType = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].customField;
                        _filemessagesend.data       = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].data;

                        if (ChatManager.Instance.currentSelectedChatChannel.messagesList[count].filePath
                            .Equals(ChatManager.Instance.currentSelectedChatChannel.messagesList[count].fileUrl))
                        {
                            ChatFileType _fileType = new ChatFileType (ChatManager.Instance.currentSelectedChatChannel.messagesList [count].filePath,
                                ChatManager.Instance.currentSelectedChatChannel.messagesList [count].fileType);

                            _filemessagesend.fileInfo = _fileType;

                            /*if (ChatManager.Instance.isInternetConnected == true)
                            {
                                MessageQueueHandler.Instance.SendFileMessage (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, _fileType, _filemessagesend.customType, _filemessagesend.data);
                            }
                            else
                            {
                            }*/
                            mMessageSendingQueueList.Add (_filemessagesend);
                        }
                        else
                        {
                            _filemessagesend.fileUrl    = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].fileUrl;
                            _filemessagesend.name       = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].fileName;
                            _filemessagesend.size       = ChatManager.Instance.currentSelectedChatChannel.messagesList [count].fileSize;
                            _filemessagesend.type       = ChatFileType.ConvertChatFileTypeToString (ChatManager.Instance.currentSelectedChatChannel.messagesList [count].fileType);

                            /*if (ChatManager.Instance.isInternetConnected == true)
                            {
                                MessageQueueHandler.Instance.SendFileMessageWithUrl (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, _filemessagesend.fileUrl, _filemessagesend.name,
                                    _filemessagesend.type, _filemessagesend.size, _filemessagesend.data, _filemessagesend.customType);
                            }
                            else
                            {
                            }*/
                            mMessageSendingQueueList.Add (_filemessagesend);
                        }
                    }
                }
                count--;
            }

            mMessageSendingQueueList.Reverse();
            if (ChatManager.Instance.isInternetConnected == true)
            {
                SendMessageWhenInternetBack();
            }
        }

        private List<ChatManager.AllMessageQueue> mMessageSendingQueueList = new List<ChatManager.AllMessageQueue> ();
        void SendMessageWhenInternetBack ()
        {
            for (int i = 0; i < mMessageSendingQueueList.Count; i++)
            {
                if (mMessageSendingQueueList[i] is ChatManager.MessageSendQueue)
                {
                    ChatManager.MessageSendQueue _messageQueue = mMessageSendingQueueList [i] as ChatManager.MessageSendQueue;
                    MessageQueueHandler.Instance.SendUserMessage(ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, 
                        _messageQueue.msg, _messageQueue.data, _messageQueue.customType, null);
                }
                else if (mMessageSendingQueueList[i] is ChatManager.FileSendQueue)
                {
                    ChatManager.FileSendQueue _fileQueue = mMessageSendingQueueList [i] as ChatManager.FileSendQueue;
                    if (_fileQueue.fileInfo == null)
                    {
                        MessageQueueHandler.Instance.SendFileMessageWithUrl (_fileQueue.baseChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, _fileQueue.fileUrl, _fileQueue.name, 
                            _fileQueue.type, _fileQueue.size, _fileQueue.data, _fileQueue.customType);
                    }
                    else
                    {
                        MessageQueueHandler.Instance.SendFileMessage (_fileQueue.baseChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, _fileQueue.fileInfo, _fileQueue.customType, _fileQueue.data);
                    }
                }
            }

            mMessageSendingQueueList.Clear();
        }

#endregion

#region Delete Messsage
        public void EnableDropDown(){
			deleteForwarddropDownObj.SetActive(true);
		}

        public void DeleteMessage (long messageId)
        {
            DeleteChatMessage(messageId);
            SaveChangedMessageToCache();
            DisplayMessages();
        }

        void DeleteChatMessage (long messageId)
        {
			Debug.Log ("DELEEEEEEEEETTEEEEE MSG ------>" + messageId);

            int count = ChatManager.Instance.currentSelectedChatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0 ; i--)
            {
                if (ChatManager.Instance.currentSelectedChatChannel.messagesList[i].messageId == messageId)
                {
                    ChatManager.Instance.currentSelectedChatChannel.messagesList [i].messageType = ChatMessage.MessageType.Deleted;
                    break;
                }
            }

            for (int i = 0; i < messageBoxList.Count; i++)
            {
                if (messageBoxList[i].tableDataType != ChatMessageTableData.TableDataType.TimeStampBox &&
                    messageBoxList[i].chatMessage.messageId == messageId)
                {
                    messageBoxList.RemoveAt (i);
                    break;
                }
            }

//            DisplayMessages ();
        }

		public void DeleteSeletedMessage()
		{
			//deleteForwarddropDownObj.SetActive(true);
            if (ChatManager.Instance.selectedMessages != null && ChatManager.Instance.selectedMessages.Count > 0)
            {
                for (int i = 0; i < ChatManager.Instance.selectedMessages.Count; i++)
                {
                    DeleteChatMessage (ChatManager.Instance.selectedMessages [i].messageId);
                }

                SaveChangedMessageToCache ();
                ChatManager.Instance.selectedMessages.Clear();
//                ReloadOrRefreshCell ();

                DisplayMessages();
            }

            deleteForwarddropDownObj.SetActive(false);
		}
#endregion

#region Forwarding Message
		public void ForwardSelectedMessage()
		{
			Debug.Log("Forward Selected Message");

            if (ChatManager.Instance.selectedMessages != null && ChatManager.Instance.selectedMessages.Count > 0)
            {
                for (int i = 0; i < ChatManager.Instance.selectedMessages.Count; i++)
                {
                    ChatManager.Instance.forwardingMessages.Add (ChatManager.Instance.selectedMessages [i]);
                }
            }

            ChatManager.Instance.selectedMessages.Clear ();
            deleteForwarddropDownObj.SetActive(false);
            OnClickBackButton ();
		}

        void ForwardMessage ()
        {
            Debug.Log ("ForwardMessage");

            if (ChatManager.Instance.forwardingMessages != null && ChatManager.Instance.forwardingMessages.Count > 0)
            {
                for (int i = 0; i < ChatManager.Instance.forwardingMessages.Count; i++)
                {
                    if (ChatManager.Instance.forwardingMessages[i].messageType == ChatMessage.MessageType.Message)
                    {
                        ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage (ChatManager.Instance.forwardingMessages[i].data);

                        _chatMsg.messageType = ChatMessage.MessageType.Message;
                        _chatMsg.message = ChatManager.Instance.forwardingMessages[i].message;
                        _chatMsg.data = ChatManager.Instance.forwardingMessages[i].data;
                        
                        ChatChannelHandler.OnSendMessage (ChatManager.Instance.currentSelectedChatChannel, _chatMsg);
                        MessageQueueHandler.Instance.SendUserMessage (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, 
                            _chatMsg.message, _chatMsg.data, _chatMsg.customField, null);
                    }
                    else if (ChatManager.Instance.forwardingMessages[i].messageType == ChatMessage.MessageType.FileLink)
                    {
                        ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage (ChatManager.Instance.forwardingMessages[i].data);

                        _chatMsg.messageType = ChatMessage.MessageType.FileLink;
                        _chatMsg.message = ChatManager.Instance.forwardingMessages[i].message;
                        _chatMsg.data = ChatManager.Instance.forwardingMessages[i].data;
                        _chatMsg.fileName = ChatManager.Instance.forwardingMessages [i].fileName;
                        _chatMsg.filePath = ChatManager.Instance.forwardingMessages [i].filePath;
                        _chatMsg.fileSize = ChatManager.Instance.forwardingMessages [i].fileSize;
                        _chatMsg.fileType = ChatManager.Instance.forwardingMessages [i].fileType;
                        _chatMsg.fileUrl = ChatManager.Instance.forwardingMessages [i].fileUrl;
                        _chatMsg.customField = "MessageNotSend"+_chatMsg.createdAt.ToString();

                        ChatChannelHandler.OnSendMessage (ChatManager.Instance.currentSelectedChatChannel, _chatMsg);

                        MessageQueueHandler.Instance.SendFileMessageWithUrl (ChatManager.Instance.currentChannel, ChatManager.Instance.currentSelectedChatChannel.channelUrl, _chatMsg.fileUrl, _chatMsg.fileName, 
                            ChatFileType.ConvertChatFileTypeToString (_chatMsg.fileType), _chatMsg.fileSize, _chatMsg.data, _chatMsg.customField);
                    }
                }

                ChatManager.Instance.forwardingMessages.Clear ();
//                DisplayMessages ();
                DisplayDuplicateMessage();
            }
        }

#endregion

#region Panel Switching
        public enum PanelState
        {
            None = -1,
            Message = 0,
            Sticker,
            Audio,
            AudioBuzz,
            AudioRecord,
            Attach,
            Smileys,
            CloseAll
        }

        [Header("Panel Switch")]
        public float panelLevelHeight = 240f, inputPanelMoveTime = 0.5f;
        public LeanTweenType leanTweenType = LeanTweenType.linear;
        public RectTransform otherPanelHolder, scrollPanelHolder;
        public PanelState CurrenPanelState { get { return currentPanelState; } }
//        [HideInInspector]
        private Vector3 bottomPanelPosition, scrollPanelPosition;
        private PanelState currentPanelState = PanelState.None;
        private LTDescr moveTween, moveTweenScroolview;
        private bool isInputPanelUp;

        public void InitializePanelPosition ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("InitializePanelPosition ------>>>>> " + Time.realtimeSinceStartup);
            #endif
            InitPanelHeight();

            bottomPanelPosition = inputFieldPanelUI.rectTransform().localPosition;
            scrollPanelPosition = scrollPanelHolder.localPosition;

            inputFieldPanelUI.gameObject.SetActive(true);
            SetDeactiveOtherPanel(PanelState.Message);
        }

        void InitPanelHeight ()
        {
            if (PlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight))
            {
                panelLevelHeight = PlayerPrefs.GetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("InitHeightLevel " + panelLevelHeight);
                #endif
            }

            otherPanelHolder.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, GetPanelHeight(false));
        }

        /// <summary>
        /// Gets the height of the panel.
        /// </summary>
        /// <returns>The panel height.</returns>
        /// <param name="fullHeight">If set to <c>true</c> full height.</param>
        float GetPanelHeight (bool fullHeight, RectTransform panel = null)
		{
			float refHeight,screenRef ;
			if (Screen.height < 1280)
				screenRef = 1184;
			else
				screenRef = 1280;
			
            refHeight = (100 * Screen.height) / screenRef;

            float _tempPanelHeight = panelLevelHeight;

            if(fullHeight)
            {
//                _tempPanelHeight = panelLevelHeight + inputFieldPanelUI.rectTransform().rect.height;
//				_tempPanelHeight=panelLevelHeight+PluginMsgHandler.getInst().KeyboardHeight+(inputFieldPanelUI.rectTransform().rect.height*2);
                if (PlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight))
                {
                    _tempPanelHeight = PlayerPrefs.GetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight);
//                    Debug.Log("_tempPanelHeight " + _tempPanelHeight);
                }

                if (_tempPanelHeight < refHeight)
                    _tempPanelHeight = 594;

                _tempPanelHeight -= (inputFieldPanelUI.rectTransform().rect.height / 2f);
            }
            else
            {
                if (panel != null)
                {
                    _tempPanelHeight = panel.rectTransform().rect.height;// + (inputFieldPanelUI.rectTransform().rect.height / 2f);
                }
                else
                {
                    _tempPanelHeight = panelLevelHeight + (inputFieldPanelUI.rectTransform().rect.height / 2f);
                }
            }

            Debug.Log(fullHeight + "_tempPanelHeight.... " + _tempPanelHeight);
            return _tempPanelHeight;
        }

        public void SwitchPanelState (PanelState state)
        {
            if (currentPanelState == state)
            {
                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(currentPanelState + " :: " + state);
            #endif

            InitPanelHeight();

            currentPanelState = state;

            switch (state)
            {
                case PanelState.Message:
                    {
                        MoveInputMessagePanel(true, GetPanelHeight(true));
                    }
                    break;
                case PanelState.CloseAll:
                    {
                        MoveInputMessagePanel(false, GetPanelHeight(false));
                        inputFieldPanelUI.closeButton.SetActive(false);
                    }
                    break;
                case PanelState.Audio:
                    {
                        MoveInputMessagePanel(true, GetPanelHeight(false, audioPanelUI.rectTransform()));
                    }
                    break;
                case PanelState.Sticker:
                    {
                        MoveInputMessagePanel(true, GetPanelHeight(false, stickerPanelUI.rectTransform()));
                    }
                    break;
                default:
                    {
                        MoveInputMessagePanel(true, GetPanelHeight(false));
                    }
                    break;
            }
            SetDeactiveOtherPanel(currentPanelState);
        }

        void SetDeactiveOtherPanel (PanelState state)
        {
            audioPanelUI.gameObject.SetActive(state == PanelState.Audio);
            attachmentPanelUI.gameObject.SetActive(state == PanelState.Attach);
            stickerPanelUI.gameObject.SetActive(state == PanelState.Sticker);
            smileyPanelUI.gameObject.SetActive (state == PanelState.Smileys);
        }

        void CheckForMessagePanel ()
        {
            if (isInputPanelUp)
            {
                switch (currentPanelState)
                {
                    case PanelState.Message:
                        {
                            MoveScrollView(true, GetPanelHeight(true));
                        }
                        break;
                    case PanelState.Audio:
                        {
                            MoveScrollView(true, GetPanelHeight(false, audioPanelUI.rectTransform()));
                        }
                        break;
                    case PanelState.Sticker:
                        {
                            MoveScrollView(true, GetPanelHeight(false, stickerPanelUI.rectTransform()));
                        }
                        break;
                    default:
                        {
                            MoveScrollView(true, GetPanelHeight(false));
                        }
                        break;
                }
            }
        }

        void MoveInputMessagePanel (bool moveUp, float panelHeight)
        {
            /*#if UNITY_EDITOR
            panelHeight = 583.662f;
            #endif*/

            Vector3 _endPos = bottomPanelPosition;

            if (moveTween != null)
            {
                LeanTween.cancel (moveTween.id);
            }

            isInputPanelUp = false;

            if (moveUp)
            {
                isInputPanelUp = true;
                _endPos.y += panelHeight;
                inputFieldPanelUI.closeButton.SetActive(true);
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(panelHeight + " :: " + inputFieldPanelUI.rectTransform().rect.height);
            #endif

            moveTween = LeanTween.moveLocal(inputFieldPanelUI.gameObject, _endPos, inputPanelMoveTime);
            moveTween.setEase(leanTweenType);
            moveTween.setOnComplete(HandleOnCompleteMove, moveUp);
            MoveScrollView(moveUp, panelHeight);
        }

        void MoveScrollView (bool moveUp, float panelHeight)
        {
            Vector3 _scrollEndPos = scrollPanelPosition;

            if (moveTweenScroolview != null)
            {
                Debug.Log("NNNNNNNNN");
//                return;
                LeanTween.cancel (moveTweenScroolview.id);
            }

            if (moveUp)
            {
                Debug.Log(GetTotalContentItemHeight() + ":;:" + GetContentHeight());
                if (GetTotalContentItemHeight () < GetContentHeight())
                {
                    float _leftHeight = GetContentHeight() - GetTotalContentItemHeight();
                    if (panelHeight > _leftHeight)
                    {
                        _scrollEndPos.y += (panelHeight - (_leftHeight));// panelHeight + PluginMsgHandler.getInst().KeyboardHeight;
                        _scrollEndPos.y += (inputFieldPanelUI.rectTransform().rect.height / 2f);
                    }

                }
                else
                {
                    _scrollEndPos.y += panelHeight + PluginMsgHandler.getInst().KeyboardHeight;// + (inputFieldPanelUI.rectTransform ().rect.height * 0.25f);
                }
            }

            moveTweenScroolview = LeanTween.moveLocal(scrollPanelHolder.gameObject, _scrollEndPos, inputPanelMoveTime);
            moveTweenScroolview.setEase(leanTweenType);
            moveTweenScroolview.setOnComplete(HandleOnCompleteMoveScrollView, moveUp);
        }

		void HandleOnCompleteMove (object moveUp)
		{
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnCompleteMove " + moveUp);
            #endif
			bool _moveUp = (bool)moveUp;
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Keyboard   HandleOnCompleteMove " + moveUp);
            #endif
			if (_moveUp)
				inputFieldPanelUI.NativeInputActivation ();
			else
				inputFieldPanelUI.NativeInputDeActivation ();
			moveTween = null;
		}

        void HandleOnCompleteMoveScrollView (object moveUp)
        {
            moveTweenScroolview = null;
        }

        float GetContentHeight ()
        {
            //return m_tableView.GetComponent<RectTransform>().rect.height;
			return m_EnhancedScrollView.GetComponent<RectTransform>().rect.height;
        }

        private Transform tableViewContent;
        float GetTotalContentItemHeight()
        {
            if (tableViewContent == null)
            {
               // tableViewContent = m_tableView.transform.GetChild (0);
                tableViewContent = scrollRect.transform.GetChild (0);
            }
            
            float _layout_Top_Padding = (float) tableViewContent.GetComponent<VerticalLayoutGroup>().padding.top;
            float _layout_Spacing = tableViewContent.GetComponent<VerticalLayoutGroup>().spacing;

            float totalHeight = 0;
            int _totalChild = -1;
            foreach (Transform child in tableViewContent)
			{
                if (child.GetComponent<MessageBoxBase>() != null && child.gameObject.activeInHierarchy)
                {
                    _totalChild++;
                    totalHeight += child.GetComponent<LayoutElement> ().preferredHeight;
                }
            }

            totalHeight = totalHeight + _layout_Top_Padding + (_totalChild * _layout_Spacing);

			return totalHeight;
		}
#endregion

        #region LOAD PLAY GAMES
        private string playGameTitle;
        private string playGameinventory_id;

        public void InvokePlayGame (string gameTitle, string inventoryID)
        {
            playGameTitle = gameTitle;
            playGameinventory_id = inventoryID;

            Invoke("PlayGame", 1f);
        }

        void PlayGame ()
        {
            FreakAppManager.Instance.PlayGame(playGameTitle, playGameinventory_id);
            loadingObject.HideLoading();
            playGameTitle = string.Empty;
            playGameinventory_id = string.Empty;
        }

        #endregion

    }
}
