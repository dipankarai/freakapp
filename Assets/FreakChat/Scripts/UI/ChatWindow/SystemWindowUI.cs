﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Freak.Chat
{
    public class SystemWindowUI : MonoBehaviour
    {
        [SerializeField] private Text systemMessageText;
        
        // Use this for initialization
        void Start () {
            
        }
        
        // Update is called once per frame
        void Update () {
            
        }

        public void InitSystemMessage (ChatMessage chatMessage)
        {
            systemMessageText.text = chatMessage.messageSender.nickname + " is blocked! \n Unblock to chat!";
        }
    }
}
