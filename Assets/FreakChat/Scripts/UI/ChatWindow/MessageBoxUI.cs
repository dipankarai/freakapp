﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Freak.Chat
{
    public class MessageBoxUI : MonoBehaviour
    {
        [SerializeField] private RectTransform textMessagePanel, imageMessagePanel, audioMesssagePanel, otherMessagePanel;
        [SerializeField] private Image profileImage, messageImage;
        [SerializeField] private Text timeText, messageText, otherMessageText;

        private AudioClip audioClip;
        private ChatMessage mChatMessage;

        // Use this for initialization
        void Start () {
            
        }

        public void InitMessageBoxDetail (ChatMessage message)
        {
            DisableAllObject();

            mChatMessage = message;

            timeText.text = FreakChatUtility.GetTime (mChatMessage.createdAt);

            if (mChatMessage.messageType == ChatMessage.MessageType.Message)
            {
                if (string.IsNullOrEmpty(mChatMessage.data))
                {
                    textMessagePanel.gameObject.SetActive(true);
                    messageText.text = mChatMessage.message;
                }
                else
                {
					if (mChatMessage.fileType == ChatFileType.FileType.Stickers) {
						string path = "Stickers/" + System.IO.Path.GetFileNameWithoutExtension (mChatMessage.filePath);
						Texture2D _texture = Resources.Load (path) as Texture2D;

						if (_texture != null) {
							Rect rect = new Rect (0, 0, _texture.width, _texture.height);
							messageImage.sprite = Sprite.Create (_texture, rect, new Vector2 (0, 0), 1f);                            
                            
							imageMessagePanel.gameObject.SetActive (true);
						} else {
							Debug.LogError ("Connot find file in Resources/" + path);
						}
					} else if (mChatMessage.fileType == ChatFileType.FileType.AudioBuzz) {
						string path = "Audio/Buzz/" + System.IO.Path.GetFileNameWithoutExtension (mChatMessage.filePath);
						audioClip = Resources.Load (path) as AudioClip;


						if (audioClip != null) {
							audioMesssagePanel.gameObject.SetActive (true);
						} else {
							Debug.LogError ("Connot find file in Resources/" + path);
						}
					}
					else if (mChatMessage.fileType == ChatFileType.FileType.Challenge) 
					{
						string path = "Challenge/" + System.IO.Path.GetFileNameWithoutExtension (mChatMessage.filePath);
						Debug.Log ("Challege Message"+ message);
					}
                }

            }
            else if (mChatMessage.messageType == ChatMessage.MessageType.FileLink)
            {
                StartCoroutineInActive();
            }
        }

        public void LoadImage()
        {
//            StartCoroutine((LoadProfileImage(mChatMessage.senderImageUrl)));
            if (mChatMessage.messageType == ChatMessage.MessageType.FileLink)
            {
                StartCoroutine(LoadMessageImage(mChatMessage.fileUrl));
            }
        }

        IEnumerator LoadProfileImage (string url)
        {
            WWW image = new WWW(url);
            yield return image;

            if (string.IsNullOrEmpty(image.error))
            {
                Rect rect = new Rect(0, 0, image.texture.width, image.texture.height);
                profileImage.sprite = Sprite.Create(image.texture, rect, new Vector2(0, 0), 1f);
                yield return 0;
            }
            else
            {
                Debug.Log("url ::: " + image.error);
            }
        }

        IEnumerator LoadMessageImage (string url)
        {
            WWW image = new WWW(url);
            yield return image;

            if (string.IsNullOrEmpty(image.error))
            {
                Rect rect = new Rect(0, 0, image.texture.width, image.texture.height);
                messageImage.sprite = Sprite.Create(image.texture, rect, new Vector2(0, 0), 1f);
                yield return 0;
            }
            else
            {
                Debug.Log("url ::: " + image.error);
            }
        }

        void StartCoroutineInActive ()
        {
            if (this.gameObject.activeInHierarchy)
            {
                if (mChatMessage.messageSender.userId == ChatManager.Instance.UserId)
                {
                    LoadFromLocal();
                }
                else
                {
                    StartCoroutine("LoadMedia");
                }
            }
            else
            {
                Invoke("StartCoroutineInActive", 2f);
            }
        }

        IEnumerator LoadMedia ()
        {
            WWW request = new WWW(mChatMessage.fileUrl);
            yield return request;

            if (string.IsNullOrEmpty(request.error))
            {
                switch (mChatMessage.fileType)
                {
                    case ChatFileType.FileType.AudioBuzz:
                    case ChatFileType.FileType.AudioRecord:
                        {
                            audioClip = request.audioClip;
                            audioMesssagePanel.gameObject.SetActive(true);
                        }
                        break;

                    case ChatFileType.FileType.Contact:
                        {
                            otherMessageText.text = "Contact Received";
                            otherMessagePanel.gameObject.SetActive(true);
                        }
                        break;

                    case ChatFileType.FileType.Image:
                    case ChatFileType.FileType.Stickers:
                        {
                            Rect rect = new Rect(0, 0, request.texture.width, request.texture.height);
                            messageImage.sprite = Sprite.Create(request.texture, rect, new Vector2(0, 0), 1f);                            
                        
                            imageMessagePanel.gameObject.SetActive(true);
                        }
                        break;

                    case ChatFileType.FileType.Others:
                        {
                            otherMessageText.text = "Others Received";
                            otherMessagePanel.gameObject.SetActive(true);
                        }
                        break;

                    case ChatFileType.FileType.Video:
                        {
                            otherMessageText.text = "Video File Received";
                            otherMessagePanel.gameObject.SetActive(true);
                        }
                        break;
                }
            }
            else
            {
                Debug.LogError("url ::: " + mChatMessage.fileUrl + request.error);
            }
        }

        void LoadFromLocal ()
        {
            switch (mChatMessage.fileType)
            {
                case ChatFileType.FileType.AudioBuzz:
                    break;
                case ChatFileType.FileType.AudioRecord:
                    {
                        string filename = mChatMessage.customField;
                        string filepath = FreakChatUtility.GetSendFilePath(filename, FreakChatUtility.FolderType.Audio);

                        if (string.IsNullOrEmpty(filepath))
                        {
                            StartCoroutine("LoadMedia");
                            return;
                        }


                        StartCoroutine(LoadAudioFromLocal("file:///" + filepath));

                    }
                    break;

                case ChatFileType.FileType.Contact:
                    {
                        otherMessageText.text = "Contact Received";
                        otherMessagePanel.gameObject.SetActive(true);
                    }
                    break;

                case ChatFileType.FileType.Doc:
                    {
                        otherMessageText.text = "Document Received";
                        otherMessagePanel.gameObject.SetActive(true);
                    }
                    break;

                case ChatFileType.FileType.Image:
                    {
                        string filename = mChatMessage.customField;
                        string filepath = FreakChatUtility.GetSendFilePath(filename, FreakChatUtility.FolderType.Image);

                        if (string.IsNullOrEmpty(filepath))
                        {
                            StartCoroutine("LoadMedia");
                            return;
                        }

                        Texture2D tex = null;
                        byte[] fileByte = System.IO.File.ReadAllBytes(filepath);
                        tex = new Texture2D(2, 2, TextureFormat.ARGB32, false);
                        tex.LoadImage(fileByte); //..this will auto-resize the texture dimensions.

                        Rect rect = new Rect(0, 0, tex.width, tex.height);
                        messageImage.sprite = Sprite.Create(tex, rect, new Vector2(0, 0), 1f);
                        imageMessagePanel.gameObject.SetActive(true);

                    }
                    break;
                case ChatFileType.FileType.Stickers:
                    {
//                        Rect rect = new Rect(0, 0, request.texture.width, request.texture.height);
//                        messageImage.sprite = Sprite.Create(request.texture, rect, new Vector2(0, 0), 1f);                            
//
//                        imageMessagePanel.gameObject.SetActive(true);
                    }
                    break;

                case ChatFileType.FileType.Location:
                    {
                        otherMessageText.text = "Location Received";
                        otherMessagePanel.gameObject.SetActive(true);
                    }
                    break;

                case ChatFileType.FileType.Others:
                    {
                        otherMessageText.text = "Others Received";
                        otherMessagePanel.gameObject.SetActive(true);
                    }
                    break;

                case ChatFileType.FileType.Video:
                    {
                        otherMessageText.text = "Video File Received";
                        otherMessagePanel.gameObject.SetActive(true);
                    }
                    break;
            }
        }

        IEnumerator LoadAudioFromLocal (string path)
        {
            WWW audioFile = new WWW(path);
            yield return audioFile;

            if (audioFile.isDone)
            {
                audioClip = audioFile.audioClip;
                audioMesssagePanel.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("Error Loading Audio Record File: " + path);
            }
        }

        void DisableAllObject ()
        {
            textMessagePanel.gameObject.SetActive(false);
            imageMessagePanel.gameObject.SetActive(false);
            audioMesssagePanel.gameObject.SetActive(false);
            otherMessagePanel.gameObject.SetActive(false);
        }

        void SaveFile ()
        {
            
        }

        public void OnClickPlayAudio ()
        {
            UIController.Instance.GetAudioSource().PlayOneShot(audioClip);
        }
    }
}
