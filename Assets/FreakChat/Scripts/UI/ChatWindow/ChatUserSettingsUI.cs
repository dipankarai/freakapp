﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class ChatUserSettingsUI : MonoBehaviour
    {
        [HideInInspector] public ChatWindowUI chatWindowUI;
        [SerializeField] private Button blockButton, muteButton;
        [SerializeField] private Text blockText, muteText;

        private bool canBlock = true;
        private bool canMute = true;

        void OnEnable()
        {
            RefreshData();
        }

        // Use this for initialization
        void Start () {
            
        }
        
        void RefreshData ()
        {
            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                GroupRefreshData();
            }
            else
            {
                SingleRefreshData();
            }
        }

        void BlockUserCallback (bool blocked)
        {
            Debug.Log("BlockUserCallback " + blocked);
            blockButton.interactable = true;
            if (blocked)
            {
                Debug.Log(ChatManager.Instance.currentSelectedChatChannel.otherUserName + "is Blocked" +
                    "UserID: "+ChatManager.Instance.currentSelectedChatChannel.otherUserId);
//                this.gameObject.SetActive(true);

                FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
                blockUserList.AddUser(ChatManager.Instance.currentSelectedChatChannel.otherUserId);
                FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;
            }

            RefreshData();
        }

        void UnblockUserCallback (bool blocked)
        {
            Debug.Log("UnblockUserCallback");
            blockButton.interactable = true;
            if (blocked)
            {
                Debug.Log(ChatManager.Instance.currentSelectedChatChannel.otherUserName + "is Blocked" +
                    "UserID: "+ChatManager.Instance.currentSelectedChatChannel.otherUserId);
                this.gameObject.SetActive(true);

                FreakChatSerializeClass.BlockedUser blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
                blockUserList.RemoveUser(ChatManager.Instance.currentSelectedChatChannel.otherUserId);
                FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;
            }

            RefreshData();
        }

        public void OnClickReportButton ()
        {
            Debug.Log("OnClickReportButton");
            if (ChatManager.CheckInternetConnection())
            {
                ReportChatUser (ChatManager.Instance.currentSelectedChatChannel.otherUserId, string.Empty);
            }
//            else
//            {
//                FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Network Problem!");
//            }
        }

        public void OnClickBlockButton ()
        {
            Debug.Log("OnClickBlockButton");
            if (ChatManager.CheckInternetConnection())
            {
                blockButton.interactable = false;
                
                if (canBlock)
                {
                    canBlock = false;
                    
                    ChatManager.Instance.BlockUser(ChatManager.Instance.currentSelectedChatChannel.otherUserId, BlockUserCallback);
                }
                else
                {
                    canBlock = true;
//                    ChatManager.Instance.UnblockUser (ChatManager.Instance.currentSelectedChatChannel.otherUserId, UnblockUserCallback);
                    ChatServerAPI.Instance.UnblockUser(ChatManager.Instance.UserId,
                        ChatManager.Instance.currentSelectedChatChannel.otherUserId, UnblockUserCallback);
                }
            }
        }

        public void OnClickMuteButton ()
        {
            Debug.Log("OnClickMuteButton");

            if (ChatManager.Instance.currentSelectedChatChannel.isGroupChannel)
            {
                GroupMuteButton();
            }
            else
            {
                SingleMuteButton();
            }
			//chatWindowUI.inputFieldPanelUI.OnClosingSetting();
        }

        public void OnClickCancelButton ()
        {
			Debug.Log("OnClickReportButton");
			chatWindowUI.inputFieldPanelUI.OnClosingSetting();
            this.gameObject.SetActive(false);
        }
    
        public void OnClickDeleteButton ()
        {
            if (ChatManager.CheckInternetConnection())
            {
                ChatManager.Instance.DeleteChannel(ChatManager.Instance.currentSelectedChatChannel.channelUrl, ChannelDeleteHandler);
            }
        }

        void ChannelDeleteHandler (bool deleted)
        {
            if (deleted)
            {
                CacheManager.DeleteSingleChannel(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
                chatWindowUI.OnClickBackButton();
            }
        }

        private IEnumerator waitRoutine;
        private string removed_id;
        private string removed_name;
        public void OnClickExitGroup ()
        {
            ChatManager.Instance.mConnectingUI.DisplayLoading ();

            removed_id = ChatManager.Instance.UserId;
            removed_name = ChatManager.Instance.UserName;

            if (removed_id == ChatManager.Instance.currentSelectedChatChannel.adminMetaData.createdBy)
            {
                if (ChatManager.Instance.currentSelectedChatChannel.channelMembers.Count == 1 && ChatManager.Instance.currentSelectedChatChannel.channelMembers[0].userId == removed_id)
                {
                    // Delete group....
                    ChatManager.Instance.DeleteChannel(ChatManager.Instance.currentSelectedChatChannel.channelUrl, DeleteGroupCallback);
                }
                else
                {
                    for (int i = 0; i < ChatManager.Instance.currentSelectedChatChannel.channelMembers.Count; i++)
                    {
                        if(ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].userId != removed_id)
                        {

                            ChatManager.Instance.currentSelectedChatChannel.adminMetaData.ChangeMainAdmin(ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].userId);

                            removed_id = ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].userId;
                            removed_name = ChatManager.Instance.currentSelectedChatChannel.channelMembers[i].nickname;

                            GroupSerializableRequestClass.UpdateRequest _updateRequest = new GroupSerializableRequestClass.UpdateRequest();
                            _updateRequest.cover_url = ChatManager.Instance.currentSelectedChatChannel.coverUrl;
                            _updateRequest.name = ChatManager.Instance.currentSelectedChatChannel.channelName;
                            _updateRequest.data = Newtonsoft.Json.JsonConvert.SerializeObject(ChatManager.Instance.currentSelectedChatChannel.adminMetaData);

                            ChatManager.Instance.UpdateGroup(ChatManager.Instance.currentSelectedChatChannel.channelUrl, _updateRequest, OnGroupAdminChange);
                            return;
                        }
                    }
                }
            }
            else
            {
                FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
                _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
                _metaData.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.leftGroup;

                ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "Group Member Left", Newtonsoft.Json.JsonConvert.SerializeObject(_metaData));

                ChatManager.Instance.LeaveCurrentChannel(LeaveCurrentChannelCallback);
            }
        }

        void OnGroupAdminChange (GroupSerializableResponseClass.GroupChannel updateGroupCallback)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData1 = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData1.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData1.message = ChatManager.Instance.UserName + " changed admin to " + removed_name;

            ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "Group Admin Changed", Newtonsoft.Json.JsonConvert.SerializeObject(_metaData1));

            FreakChatSerializeClass.NormalMessagerMetaData _metaData2 = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData2.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData2.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.leftGroup;

            ChatManager.Instance.SendUserMessage(ChatManager.Instance.currentChannel, "Group Member Left", Newtonsoft.Json.JsonConvert.SerializeObject(_metaData2));

            GroupSerializableRequestClass.LeaveGroup _leaveGroup = new GroupSerializableRequestClass.LeaveGroup();
            _leaveGroup.user_ids.Add(ChatManager.Instance.UserId);

            ChatManager.Instance.LeaveCurrentChannel(ChatManager.Instance.currentSelectedChatChannel.channelUrl, _leaveGroup, DeleteGroupCallback);
        }

        void DeleteGroupCallback (bool success)
        {
            if (success)
            {
                LeaveCurrentChannelCallback();
            }
        }

        void UpdateGroupMemberCallback ()
        {
            ChatManager.Instance.LeaveCurrentChannel(LeaveCurrentChannelCallback);
        }

        void LeaveCurrentChannelCallback ()
        {
            Debug.Log("LeaveCurrentChannelCallback");
            ChatManager.Instance.currentChannel = null;
            ChatManager.Instance.OnChannelDeleted(ChatManager.Instance.currentSelectedChatChannel.channelUrl);

            ChatManager.Instance.mConnectingUI.HideLoading ();

            this.gameObject.SetActive(false);
            chatWindowUI.OnClickBackButton();
        }

		#region Single User Properties
		//Report the Chat User
		public void ReportChatUser(string chatUserId, string message)
		{
			FreakApi api = new FreakApi ("report.post", ServerResponseForReportChatUser);
			api.param ("chat_user_id", chatUserId);
			api.param ("message", message);
			api.get (this);

		}

		void ServerResponseForReportChatUser(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
		{
			// check for errors
			if (output!=null)
			{
				string responseInfo =(string)output["responseInfo"];
				switch(responseCode)
				{
				case 001:
					/**
						
					*/
					//IDictionary userDetailsDict = (IDictionary)output ["responseMsg"];
					OnClickCancelButton();
					AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.reportUser);
					break;
				case 234:
					/**
						
			 		*/
					OnClickCancelButton();
					AlertMessege.Instance.ShowAlertWindow(AlertMessege.AlertType.MessegeWithOkButton, FreakAppConstantPara.MessageDescription.alreadyReported);
					break;
				default:
					/**
						
					*/
					break;
				}	
			} 
			else {
				Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
				////inapi.get(this);
			}  
		}
		#endregion


#region Single User Properties

        void SingleMuteButton ()
        {
            muteButton.interactable = false;

            FreakChatSerializeClass.MuteUsers muteUserList = FreakChatUtility.FreakChatPlayerPrefs.MuteUserList;
            if (canMute)
            {
                canMute = false;
                muteUserList.AddUser(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
            }
            else
            {
                canMute = true;
                muteUserList.RemoveUser(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
            }
            ResetMuteUserList(muteUserList);

            RefreshData();
            muteButton.interactable = true;
        }

        void SingleRefreshData ()
        {
            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.BlockedUser))
            {
                if (FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList.IsBlocked(ChatManager.Instance.currentSelectedChatChannel.otherUserId))
                {
                    blockText.text = "Unblock";
                    canBlock = false;
                }
                else
                {
                    blockText.text = "Block";
                    canBlock = true;
                }
            }
            else
            {
                FreakChatSerializeClass.BlockedUser _blockedUser = new FreakChatSerializeClass.BlockedUser();
                FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = _blockedUser;

                blockText.text = "Block";
                canBlock = true;
            }

            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.MutedUser))
            {
                if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(ChatManager.Instance.currentSelectedChatChannel.channelUrl))
                {
                    muteText.text = "Unmute";
                    canMute = false;
                }
                else
                {
                    muteText.text = "Mute";
                    canMute = true;
                }
            }
            else
            {
                FreakChatSerializeClass.MuteUsers _muteUsers = new FreakChatSerializeClass.MuteUsers();
                ResetMuteUserList(_muteUsers);

                muteText.text = "Mute";
                canMute = true;
            }
        }

#endregion

#region Group Properties

        void GroupMuteButton ()
        {
            muteButton.interactable = false;

            FreakChatSerializeClass.MuteUsers muteGroupList = FreakChatUtility.FreakChatPlayerPrefs.MuteUserList;
            if (canMute)
            {
                canMute = false;

                muteGroupList.AddUser(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
            }
            else
            {
                canMute = true;
                muteGroupList.RemoveUser(ChatManager.Instance.currentSelectedChatChannel.channelUrl);
            }
            ResetMuteUserList(muteGroupList);

            RefreshData();
            muteButton.interactable = true;
        }

        void GroupRefreshData ()
        {
            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.MutedGroup))
            {
                if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(ChatManager.Instance.currentSelectedChatChannel.channelUrl))
                {
                    muteText.text = "Unmute";
                    canMute = false;
                }
                else
                {
                    muteText.text = "Mute";
                    canMute = true;
                }
            }
            else
            {
                FreakChatSerializeClass.MuteUsers _muteUsers = new FreakChatSerializeClass.MuteUsers();
                ResetMuteUserList(_muteUsers);

                muteText.text = "Mute";
                canMute = true;
            }
        }
#endregion


        void ResetMuteUserList (FreakChatSerializeClass.MuteUsers muteUsers)
        {
            FreakChatUtility.FreakChatPlayerPrefs.MuteUserList = muteUsers;

            if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.muteUserIDList.Count != null)
            {
                string stringList = Newtonsoft.Json.JsonConvert.SerializeObject (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.muteUserIDList);
                #if !UNITY_EDITOR
                PushNotification.MuteNotification(stringList);
                #endif
            }
        }
    }
}
