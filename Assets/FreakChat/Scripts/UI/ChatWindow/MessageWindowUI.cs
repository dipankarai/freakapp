﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft.Examples;
using Tacticsoft;
using UnityEngine.Events;


namespace Freak.Chat
{
	public class MessageWindowUI : TableViewCell {

        [SerializeField] private MessageBoxUI leftMessageBoxUI;
        [SerializeField] private MessageBoxUI rightMessageBoxUI;

        private ChatMessage mChatMessage;
        private AudioSource mAudioSource;

		public Slider m_cellHeightSlider;

		public int rowNumber { get; set; }

		[System.Serializable]
		public class CellHeightChangedEvent : UnityEvent<int, float> { }
		public CellHeightChangedEvent onCellHeightChanged;

		public void SliderValueChanged(Slider slider) {
			onCellHeightChanged.Invoke(rowNumber, slider.value);
		}

		public float height {
			get { return m_cellHeightSlider.value; }
			set { m_cellHeightSlider.value = value; }
		}


        void OnEnable ()
        {
            if (mChatMessage.messageSender.userId == ChatManager.Instance.UserId)
            {
                PlaySound();
                return;
            }


            /*if (mChatMessage.isPresent == true)
            {
                if (ChatManager.Instance.IsUserMuted(mChatMessage.messageSender.userId) == false)
                {
                    PlaySound();
                }
            }*/
        }

        void PlaySound ()
        {
            if (mAudioSource == null)
            {
                mAudioSource = UIController.Instance.GetAudioSource();
                mAudioSource.PlayOneShot(UIController.Instance.assetsReferences.messageSound);
            }
            else if (mAudioSource.isPlaying == false)
            {
                mAudioSource.PlayOneShot(UIController.Instance.assetsReferences.messageSound);
            }
        }
        
        public void InitChatMessage (ChatMessage message)
        {
            mChatMessage = message;

            if (mChatMessage.messageType == ChatMessage.MessageType.Message)
            {
                if (mChatMessage.messageSender.userId == ChatManager.Instance.UserId)
                {
                    // Its own send message display at right side.
                    leftMessageBoxUI.gameObject.SetActive(false);
                    rightMessageBoxUI.gameObject.SetActive(true);
                }
                else
                {
                    // Message recived from other dislpay at left side.
                    leftMessageBoxUI.gameObject.SetActive(true);
                    rightMessageBoxUI.gameObject.SetActive(false);
                }
            }
            else if (mChatMessage.messageType == ChatMessage.MessageType.FileLink)
            {
                if (mChatMessage.messageSender.userId == ChatManager.Instance.UserId)
                {
                    // Its own send message display at right side.
                    leftMessageBoxUI.gameObject.SetActive(false);
                    rightMessageBoxUI.gameObject.SetActive(true);
                }
                else
                {
                    // Message recived from other dislpay at left side.
                    leftMessageBoxUI.gameObject.SetActive(true);
                    rightMessageBoxUI.gameObject.SetActive(false);
                }
            }

            if (leftMessageBoxUI.gameObject.activeSelf == true)
            {
                leftMessageBoxUI.InitMessageBoxDetail(mChatMessage);
            }
            else if (rightMessageBoxUI.gameObject.activeSelf == true)
            {
                rightMessageBoxUI.InitMessageBoxDetail(mChatMessage);
            }
        }
    }
}
