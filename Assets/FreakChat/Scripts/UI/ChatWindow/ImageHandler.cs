﻿using UnityEngine;
using System.IO;
using System.Collections;
using UnityEngine.UI;

namespace Freak.Chat
{
	public class ImageHandler : MonoBehaviour 
	{
        public Image viewPort;
        public UniGifImage uniGifImage;
        public Sprite sprite;
        public Texture2D texture;
        private Coroutine gifCoroutine;
        public string filePath;
        public bool loadImage = true;
        public ChatWindowUI chatWindowUI;
        public NewsFeedComponent newsFeedComponent;

		void Awake()
		{
//			UIController.Instance.imageHandler = this;
//			gameObject.SetActive (false);
		}

		void OnEnable()
		{
            FreakAppManager.Instance.GetHomeScreenPanel().ChangeBackGround(FreakAppManager.StatusBarStyle.StatusBarStyleDefault);
            if (loadImage)
            {
                viewPort.color = Color.black;
//                viewPort.sprite = sprite;
                viewPort.FreakSetSprite(texture);
                viewPort.color = Color.white;
                StartCoroutine (LoadImage());
            }

            if (chatWindowUI != null)
            {
                //chatWindowUI.inputFieldPanelUI.gameObject.SetActive(false);
				chatWindowUI.inputFieldPanelUI.OnClickSettingButton();
            }

            if (newsFeedComponent != null)
            {
//                newsFeedComponent.DisableInputField();
                newsFeedComponent.DisableInputFieldOnSettingsPanel();
            }
		}

        void LoadImageFromLocal()
        {
            Texture2D newImage = new Texture2D(4,4);
            newImage.LoadImage(Freak.StreamManager.LoadFileDirect (filePath));
            viewPort.FreakSetSprite (newImage);
            viewPort.color = Color.white;
//            viewPort.FreakSetSprite (TextureResize.ResizeTexture(viewPort.rectTransform, newImage));
        }

        IEnumerator LoadImage ()
        {
            Debug.Log ("Path " +filePath);
            var www = new WWW("file://" + filePath);

            while (!www.isDone)
            {
                yield return null;
                Debug.Log (www.bytesDownloaded);
            }

            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                Debug.Log ("Path " +filePath);
                Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
                viewPort.sprite = Sprite.Create(www.texture, rect, new Vector2(0, 0), 1f);
            }
            else
            {
                Debug.LogError("url ::: " + filePath + www.error);
            }
        }

		public void BackButtonClicked(){
			//gameObject.SetActive (false);
			FindObjectOfType<HomeScreenUIPanelController>().PopPanel();
		}

        public void DeleteButtonClicked(){

		}

#region GIF Image
        public void LoadGIFImage (string filePath)
        {
            viewPort.gameObject.SetActive(false);
            uniGifImage.gameObject.SetActive(true);

            if (File.Exists(filePath))
            {
                Debug.Log("File Path: "+filePath);
                if (this.gameObject.activeInHierarchy == true)
                {
                    if(gifCoroutine != null)
                    {
                        StopCoroutine(gifCoroutine);
                    }

                    gifCoroutine = StartCoroutine(ViewGifCoroutine(filePath));
                }
                else
                {
                    Invoke("LoadGIFImage", 1f);
                }
            }
            else
            {
                Debug.LogError("File Not Exists: " + filePath);
            }
        }

        private IEnumerator ViewGifCoroutine(string filePath)
        {
            yield return StartCoroutine(uniGifImage.SetGifFromUrlCoroutine(filePath));
        }

#endregion GIF Image

        void OnDisable ()
        {
            FreakAppManager.Instance.GetHomeScreenPanel().SwitchStatusBarColor();

//            viewPort.sprite = null;
//            sprite = null;
            /*viewPort.gameObject.SetActive(true);
            uniGifImage.gameObject.SetActive(false);*/

            if (chatWindowUI != null)
            {
                //chatWindowUI.inputFieldPanelUI.gameObject.SetActive(true);
				//chatWindowUI.inputFieldPanelUI.NativeSetVisible (true);
				chatWindowUI.inputFieldPanelUI.OnClosingSetting();
                //chatWindowUI.inputFieldPanelUI.NativeSetTextNative();
            }

            if (newsFeedComponent != null)
            {
                newsFeedComponent.InitForNewsFeed();
            }

            loadImage = true;
//            Resources.UnloadUnusedAssets ();
        }
	}
}
