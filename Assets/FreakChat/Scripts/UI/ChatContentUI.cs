﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
//using Tacticsoft;

namespace Freak.Chat
{
    ///THIS CLASS ************************ NOT USING

    /// <summary>
    /// This class holds the user basic data which is viewed in All Chat View panel
    /// It will get Instantiate as a gameobject.
    /// </summary>
    public class ChatContentUI : MonoBehaviour //TableViewCell
    {
        [SerializeField] private Image profileImage;
        [SerializeField] private Text usernameText, userDescriptionText, timeText, countText;
        [SerializeField] private Button openChatButton;
        public string userid;
        public ChatChannel userInfo;
        
        // Use this for initialization
        void Start () {

            openChatButton.onClick.AddListener(OnClickOpenChatButton);

            if (userInfo != null)
            {
                //TEMP                 StartCoroutine(LoadImage(userInfo.imageUrl));
            }
        }

        public void InitChatDetails (ChatChannel userInfo)
        {
            this.userInfo = userInfo;
            userid = userInfo.otherUserId;
            usernameText.text = userInfo.otherUserName;

            DateTime dateTime = new DateTime(userInfo.lastSeenAt, DateTimeKind.Utc);
            timeText.text = dateTime.ToShortTimeString();
        }
        
        /// <summary>
        /// Trigger event when Button is clicked.
        /// </summary>
        void OnClickOpenChatButton ()
        {
            Debug.Log("OnClickOpenChatButton");
//TEMP            userInfo.profileImageTexture = profileImage.sprite.texture;

            GameObject go = Instantiate(UIController.Instance.assetsReferences.ChatWindow);
            go.GetComponent<ChatWindowUI>().InitChat(userInfo);
            go.transform.SetParent(UIController.Instance.parent, false);
            go.SetActive(true);
        }

        IEnumerator LoadImage (string url)
        {
            WWW image = new WWW(url);
            yield return image;

            if (string.IsNullOrEmpty(image.error))
            {
                Rect rect = new Rect(0, 0, image.texture.width, image.texture.height);
                profileImage.sprite = Sprite.Create(image.texture, rect, new Vector2(0, 0), 1f);
                yield return 0;
            }
            else
            {
                Debug.Log("url ::: " + image.error);
            }
        }

        #region TableViewCell
        public int rowNumber { get; set; }
        public Slider m_cellHeightSlider;

        [System.Serializable]
        public class CellHeightChangedEvent : UnityEngine.Events.UnityEvent<int, float> { }
        public CellHeightChangedEvent onCellHeightChanged;

        public void SliderValueChanged(Slider slider) {
            onCellHeightChanged.Invoke(rowNumber, slider.value);
        }

        public float height {
            get { return m_cellHeightSlider.value; }
            set { m_cellHeightSlider.value = value; }
        }
        #endregion
    }
}
