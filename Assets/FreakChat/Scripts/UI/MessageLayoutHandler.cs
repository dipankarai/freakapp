﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageLayoutHandler : MonoBehaviour {

    [SerializeField] private Text text;
    private LayoutElement mLayoutElement;
    private RectTransform mRectTransform;
    public float widthOffset = 10f;

	// Use this for initialization
	void Start () {
	    
//        InitLayoutPreferredSize();
	}
	
	// Update is called once per frame
	void Update () {
        InitLayoutPreferredSize();
	}

    Vector2 currentAnchoredPosition;
    void InitLayoutPreferredSize ()
    {
        if (mLayoutElement == null)
        {
            mLayoutElement = GetComponent<LayoutElement>();
        }

        if (mRectTransform == null)
        {
            mRectTransform = GetComponent<RectTransform>();
            currentAnchoredPosition = mRectTransform.anchoredPosition;
        }

        if (Input.GetKeyDown (KeyCode.Q))
        {
            if (mLayoutElement.preferredWidth > 0)
            {
                mLayoutElement.preferredWidth = text.preferredWidth + widthOffset;
                Vector2 textAnchoredPosition = text.GetComponent<RectTransform>().anchoredPosition;
                textAnchoredPosition.x = text.preferredWidth / 2f;
                text.GetComponent<RectTransform>().anchoredPosition = textAnchoredPosition;

//                float widthDifference = text.preferredWidth - mRectTransform.sizeDelta.x;
                Vector2 anchoredPosition = mRectTransform.anchoredPosition;
                anchoredPosition.x = currentAnchoredPosition.x + (text.preferredWidth / 2f);
                mRectTransform.anchoredPosition = anchoredPosition;

                Vector2 sizeDelta = mRectTransform.sizeDelta;
                sizeDelta.x = mLayoutElement.preferredWidth;
                mRectTransform.sizeDelta = sizeDelta;
            }
            
            if (mLayoutElement.preferredHeight > 0)
            {
                mLayoutElement.preferredHeight = text.preferredHeight;
            }
        }
    }
}
