﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using Q.Utils;

namespace Freak.Chat
{
    public static class FreakChatUtility
    {
        public static string TimeStampToTime (long timeStamp)
        {
//            string time = new DateTime(timeStamp, DateTimeKind.Local).ToLongTimeString();
            string time = TimeFromUnixTimeStamp (timeStamp).ToLongTimeString();
//            //Debug.Log("Time: "+time);

            return time;
        }

        public static string TimeStampToDate (long timeStamp)
        {
//            string date = new DateTime(timeStamp, DateTimeKind.Local).ToShortDateString();
            string date = TimeFromUnixTimeStamp (timeStamp).ToShortDateString();
//            //Debug.Log("Date: "+date);

//            TimeSpan elapsedSpan = new TimeSpan(timeStamp);

            return date;
        }

        public static string TimeStampToFilename (long timeStamp)
        {
            string filename = TimeFromUnixTimeStamp(timeStamp).ToString("yyyyMMddhhmmsstt");// new DateTime(timeStamp, DateTimeKind.Utc).ToString("yyyyMMddhhmmsstt");

            return filename;
        }

        public static string FileNameFromUrl (long timeStamp, string url)
        {
            string filename = TimeFromUnixTimeStamp(timeStamp).ToString("yyyyMMddhhmmsstt");// new DateTime(timeStamp, DateTimeKind.Utc).ToString("yyyyMMddhhmmsstt");
            string fileExtension = Path.GetExtension(url);

            return filename + fileExtension;
        }

        public static DateTime TimeFromUnixTimeStamp (long timeStamp)
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(timeStamp).ToLocalTime();

//            long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
//            DateTime date = new DateTime(beginTicks + timeStamp * 10000, DateTimeKind.Utc);

            return date;
        }

        public static string LastSeenTime (long timeStamp)
        {
            DateTime dateTime = TimeFromUnixTimeStamp(timeStamp);
            DateTime nowTime = TimeFromUnixTimeStamp(GetCurrentTimeStamp());
            string time = dateTime.ToString("F");

            if(dateTime.Day == nowTime.Day && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                time = "Today at " + dateTime.ToString("t");
                return time;
            }
            else if(dateTime.Date == nowTime.Date.AddDays(-1) && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                time = "Yesterday at " + dateTime.ToString("t");
                return time;
            }
            else
            {
                time = dateTime.ToString("dd-MM-yyyy ddd");
            }

            return time;
        }

        public static string GetTime (long timeStamp)
        {
            DateTime dateTime = TimeFromUnixTimeStamp(timeStamp);
            DateTime nowTime = TimeFromUnixTimeStamp(GetCurrentTimeStamp());
            string time = dateTime.ToString("F");

            if(dateTime.Day == nowTime.Day && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                time = dateTime.ToString("t");
                return time;
            }
            else if(dateTime.Date == nowTime.Date.AddDays(-1) && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                time = "Yesterday";// + dateTime.ToString("T");
                return time;
            }
            else
            {
                time = dateTime.ToString("dd-MM-yyyy ddd");
            }

            return time;
        }

        public static string GetDisplayTimeStamp (long timeStamp)
        {
            DateTime dateTime = TimeFromUnixTimeStamp(timeStamp);
            DateTime nowTime = TimeFromUnixTimeStamp(GetCurrentTimeStamp());
            string timestampString = dateTime.ToString("F");

            if(dateTime.Day == nowTime.Day && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                timestampString = "TODAY";
            }
            else if(dateTime.Date == nowTime.Date.AddDays(-1) && dateTime.Month == nowTime.Month && dateTime.Year == nowTime.Year)
            {
                timestampString = "YESTERDAY";
            }
            else
            {
                timestampString = dateTime.ToString("d");
            }

            return timestampString;
        }

        //Get 13 digit TimeStamp.....
        public static long GetCurrentTimeStamp ()
        {
//            long epochTicks = new DateTime(1970, 1, 10, 0, 0, 0, DateTimeKind.Utc).Ticks;
            long unixTime = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;

            return unixTime;
        }

        private static readonly string[] validImageExtensions = {".jpg",".bmp",".gif",".png"}; //  etc
        private static readonly string[] validAudioExtensions = {".mp3",".wav",".acc",".m4a"}; //  etc
        private static readonly string[] validVideoExtensions = {".mp4",".avi",".mov"}; //  etc


        public static bool IsImageExtension(string ext)
        {
            for (int i = 0; i <= validImageExtensions.Length - 1; i++)
            {
                if (validImageExtensions[i] == ext.ToLower())
                    return true;
            }

            return false;
        }

        public static bool IsAudioExtension(string ext)
        {
            for (int i = 0; i <= validAudioExtensions.Length - 1; i++)
            {
                if (validAudioExtensions[i] == ext.ToLower())
                    return true;
            }

            return false;
        }

        public static bool IsVideoExtension(string ext)
        {
            for (int i = 0; i <= validVideoExtensions.Length - 1; i++)
            {
                if (validVideoExtensions[i] == ext.ToLower())
                    return true;
            }

            return false;
        }

        public static void FreakSetSprite (this UnityEngine.UI.Image image, Texture2D texture)
        {
            Rect rect = new Rect(0, 0, texture.width, texture.height);
            image.sprite = null;
            image.sprite = Sprite.Create (texture, rect, new Vector2 (0, 0), 1f);
        }

        public static void FreakSetTexture (this UnityEngine.UI.Image image, byte[] textureArray)
        {
            if (textureArray == null || textureArray.Length <= 0)
                return;

            Texture2D texture = new Texture2D ((int)image.rectTransform.rect.width, (int)image.rectTransform.rect.height, TextureFormat.ARGB32, false);
            texture.LoadImage(textureArray);
            image.FreakSetSprite (texture);
            textureArray = null;
            texture = null;
//            Resources.UnloadUnusedAssets();
        }

        public static void FreakSetTexture (this UnityEngine.UI.RawImage image, byte[] textureArray)
        {
            if (textureArray == null || textureArray.Length <= 0)
                return;

            try
            {
                Texture2D texture = new Texture2D ((int)image.rectTransform.rect.width, (int)image.rectTransform.rect.height);
                texture.LoadImage(textureArray);
                image.texture = texture as Texture;
                image.PreserveAspectToParent ();

                textureArray = null;
                texture = null;
//                Resources.UnloadUnusedAssets();
            }
            catch (Exception e)
            {
                Debug.LogError (e.Message);
            }
        }

        public static void FreakSetTextBackground (this UnityEngine.UI.Text uiText, string textData, float bgAlpha)
        {
            uiText.text = textData;
            UnityEngine.UI.Image parentImage = uiText.rectTransform().parent.GetComponent<UnityEngine.UI.Image>();

            parentImage.rectTransform().sizeDelta = new Vector2(uiText.preferredWidth + 1f, uiText.preferredHeight + 1f);

            if (parentImage.rectTransform().sizeDelta.y > (uiText.preferredHeight + 1f))
            {
                parentImage.rectTransform().sizeDelta = new Vector2(uiText.preferredWidth + 1f, uiText.preferredHeight + 1f);
            }

            parentImage.enabled = (string.IsNullOrEmpty(textData) == false);

            Color color = parentImage.color;
            color.a = 0.4f;
            parentImage.color = color;
        }

        /// <summary>
        /// Make raw Image Preserves the aspect to parent.
        /// Note: Attach AspectRatioFitter with RawImage
        /// </summary>
        /// <param name="image">RawImage.</param>
        public static void PreserveAspectToParent (this UnityEngine.UI.RawImage image)
        {
            if (image.GetComponent<UnityEngine.UI.AspectRatioFitter>() != null)
            {
                UnityEngine.UI.AspectRatioFitter aspectFitter = image.GetComponent<UnityEngine.UI.AspectRatioFitter> ();
                aspectFitter.aspectMode = UnityEngine.UI.AspectRatioFitter.AspectMode.FitInParent;

                float ratioTexture = (float)image.texture.width / (float)image.texture.height;

                /*Rect _rect = image.transform.parent.GetComponent<RectTransform> ().rect;
                float ratioParentRect = _rect.width / _rect.height;
                aspectFitter.aspectRatio = ratioTexture / ratioParentRect;*/

                aspectFitter.aspectRatio = ratioTexture;
            }
        }

        public static void ClearLogs ()
        {
            #if UNITY_EDITOR
            var assembly = System.Reflection.Assembly.GetAssembly (typeof(UnityEditor.ActiveEditorTracker));
            var type = assembly.GetType ("UnityEditorInternal.LogEntries");
            var method = type.GetMethod ("Clear");
            method.Invoke (new object(), null);
            #endif
        }


        public static class FreakChatPlayerPrefs
        {
            public static bool HasKey (string key)
            {
                return PlayerPrefs.HasKey (key);
            }

            public static void DeleteKey(string key)
            {
                PlayerPrefs.DeleteKey(key);
            }

            public static int FreakChatLocalNotificationID
            {
                get
                {
                    if (HasKey(FreakAppConstantPara.PlayerPrefsName.LocalNotificationID))
                    {
                        int not_id = PlayerPrefs.GetInt (FreakAppConstantPara.PlayerPrefsName.LocalNotificationID);
                        PlayerPrefs.SetInt (FreakAppConstantPara.PlayerPrefsName.LocalNotificationID, (not_id++));
                        return not_id;
                    }
                    else
                    {
                        PlayerPrefs.SetInt (FreakAppConstantPara.PlayerPrefsName.LocalNotificationID, 0);
                        return 0;
                    }
                }
            }

            public static FreakChatSerializeClass.BlockedUser BlockedUserList
            {
                get
                {
                    if (HasKey(FreakAppConstantPara.PlayerPrefsName.BlockedUser) == false)
                    {
                        FreakChatSerializeClass.BlockedUser newBlockUsers = new FreakChatSerializeClass.BlockedUser();
                        return newBlockUsers;
                    }

                    return (FreakChatSerializeClass.BlockedUser)PlayerPrefsSerializer.LoadData<FreakChatSerializeClass.BlockedUser>(FreakAppConstantPara.PlayerPrefsName.BlockedUser);
                }
                set
                {
                    PlayerPrefsSerializer.SaveData(FreakAppConstantPara.PlayerPrefsName.BlockedUser, value);
                }
            }

            public static FreakChatSerializeClass.MuteUsers MuteUserList
            {
                get
                {
                    if (HasKey(FreakAppConstantPara.PlayerPrefsName.MutedUser) == false)
                    {
                        FreakChatSerializeClass.MuteUsers newMuteUsers = new FreakChatSerializeClass.MuteUsers();
                        return newMuteUsers;
                    }

                    return (FreakChatSerializeClass.MuteUsers)PlayerPrefsSerializer.LoadData<FreakChatSerializeClass.MuteUsers>(FreakAppConstantPara.PlayerPrefsName.MutedUser);
                }
                set
                {
                    PlayerPrefsSerializer.SaveData(FreakAppConstantPara.PlayerPrefsName.MutedUser, value);
                }
            }

            /*public static FreakChatSerializeClass.MuteUsers MuteGroupList
            {
                get
                {
                    if (HasKey(FreakAppConstantPara.PlayerPrefsName.MutedGroup) == false)
                    {
                        FreakChatSerializeClass.MuteUsers newMuteUsers = new FreakChatSerializeClass.MuteUsers();
                        return newMuteUsers;
                    }

                    return (FreakChatSerializeClass.MuteUsers)PlayerPrefsSerializer.LoadData<FreakChatSerializeClass.MuteUsers>(FreakAppConstantPara.PlayerPrefsName.MutedGroup);
                }
                set
                {
                    PlayerPrefsSerializer.SaveData(FreakAppConstantPara.PlayerPrefsName.MutedGroup, value);
                }
            }*/
        }

#region FILE IO
        public enum FolderType
        {
            Image,
            Sticker,
            Audio,
            Video,
            Documents,
            Contacts,
            Others
        }

        private static bool savingFile;
        const string AppFolderName = "FreakApp";
        public delegate void SavedCallbackMethod(bool succeeded);
        public delegate void LoadedCallbackMethod(byte[] data, bool succeeded);

        public static void SaveFile (string filename, byte[] data, FolderType type, SavedCallbackMethod savedCallback)
        {
            if (savingFile)
            {
                //Debug.LogError("You must wait for the last saved file to finish!");
                if(savedCallback != null)
                {
                    savedCallback(false);
                    return;
                }

            }

            string _directoryPath = FreakAppManager.ApplicationPath;

            if(Directory.Exists(_directoryPath) == false)
            {
                Directory.CreateDirectory(_directoryPath);
            }

            _directoryPath += "/" + type.ToString();

            if(Directory.Exists(_directoryPath) == false)
            {
                Directory.CreateDirectory(_directoryPath);
            }

            string _filepath = _directoryPath + "/" + filename;

            File.WriteAllBytes(_filepath, data);

            if(savedCallback != null)
            {
                savedCallback(true);
            }
        }

        public static string SaveSendFileGetPath (string filename, byte[] data, FolderType type)
        {
            /*if (savingFile)
            {
                //Debug.LogError("You must wait for the last saved file to finish!");
            }*/

            string _directoryPath = FreakAppManager.ApplicationPath;
            _directoryPath += "/" + type.ToString() + "/Sent";

            if(Directory.Exists(_directoryPath) == false)
            {
                Directory.CreateDirectory(_directoryPath);
            }

            string _filepath = _directoryPath + "/" + filename;

            File.WriteAllBytes(_filepath, data);

            return _filepath;
        }

        public static void LoadFile (string filename, FolderType type, LoadedCallbackMethod loadCallBack)
        {
            string _filePath = FreakAppManager.ApplicationPath + "/" + type.ToString() + "/" + filename;

            if (File.Exists (_filePath))
            {
                byte[] bytes = File.ReadAllBytes(_filePath);
                loadCallBack(bytes, true);
            }
            else
            {
                //Debug.LogError(filename + "Doesn't exits");
                loadCallBack(null, false);
            }
        }

        public static string GetSendFilePath (string filename, FolderType type)
        {
            string _filePath = FreakAppManager.ApplicationPath;
            _filePath += "/" + type.ToString() + "/Sent/" + filename;

            if (File.Exists(_filePath))
            {
                return _filePath;
            }
            else
            {
                //Debug.LogError(_filePath + "Doesn't exits");
            }

            return string.Empty;
        }

        public static string GetSavedFilePath (string filename, FolderType type)
        {
            string _filePath = FreakAppManager.ApplicationPath;
            _filePath += "/" + type.ToString() + "/" + filename;

            if (File.Exists(_filePath))
            {
                return _filePath;
            }
            else
            {
                //Debug.LogError(_filePath + "Doesn't exits");
            }

            return string.Empty;
        }
#endregion
    }
}
