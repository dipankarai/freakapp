﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

namespace Freak
{
    public class DownloadRoutine : MonoBehaviour
    {
        public string FileName { get { return downloadFileUrl; } }

        IEnumerator downloadRoutine;
        Freak.Chat.ChatMessage mChatMessage;
        string downloadFileUrl;
        FolderLocation saveFolderLocation;
        bool isSendData;
//        Freak.Chat.ChatWindowUI mChatWindowUI;
        string fileName;
        string extension;

        public void StartDownloadRoutine(string fileUrl, FolderLocation folderLocation, Chat.ChatMessage chatMessage = null, bool isSentData = false, string extension = "")
        {
            fileName = Path.GetFileName(fileUrl);
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("StartDownloadRoutine " + fileName + " EXTENSION " + extension);
            #endif

            /*if (mChatWindowUI == null)
            {
                mChatWindowUI = FindObjectOfType<Freak.Chat.ChatWindowUI>();
            }*/

            downloadFileUrl = fileUrl;
            saveFolderLocation = folderLocation;
            mChatMessage = chatMessage;
            isSendData = isSentData;
            this.extension = extension;

            if (downloadRoutine != null)
            {
                StopCoroutine(downloadRoutine);
            }

            downloadRoutine = DownloadFileRoutine();
            StartCoroutine(downloadRoutine);
        }

        public void StartDownloadRoutine (string fileUrl, FolderLocation folderLocation, StreamSavedCallbackMethod streamSavedCallback = null, string channelId = "")
        {
            downloadFileUrl = fileUrl;

            if (downloadRoutine != null)
            {
                StopCoroutine(downloadRoutine);
            }

            downloadRoutine = DownloadFileRoutine(fileUrl, folderLocation, streamSavedCallback, channelId);
            StartCoroutine(downloadRoutine);
        }

        IEnumerator DownloadFileRoutine ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(saveFolderLocation + " :: " + downloadFileUrl);
            #endif
            WWW request = new WWW(downloadFileUrl);

            while(!request.isDone)
            {
                if (mChatMessage != null)
                {
                    mChatMessage.downloadingValue = request.progress;
                }

                yield return null;
            }

            yield return request;


            if (string.IsNullOrEmpty(request.error))
            {
                Texture2D texture = new Texture2D(4, 4);
                byte[] bytes = request.bytes;
                texture = request.texture;

                if (Path.HasExtension (fileName) == false)
                    fileName += this.extension;
                
//                StreamManager.SaveFile(fileName, request.bytes, saveFolderLocation, FileSavedCallback, isSendData);

                if (saveFolderLocation == FolderLocation.Image)
                {
                    if (texture.width > 1000 || texture.height > 1000)
                    {
//                        texture.Compress(false);
                        texture = TextureResize.ResizeTexture(1000f, 1000f, texture);
                        bytes = texture.EncodeToPNG();
                    }
                }

                if (mChatMessage != null)
                {
                    mChatMessage.downloadingValue = 1f;
                    mChatMessage.imageMessageCache = texture;
                }

                SaveFile (bytes);
                texture = null;
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("url ::: " + downloadFileUrl + request.error);
                #endif

                if (mChatMessage != null)
                {
                    mChatMessage.messageState = Freak.Chat.ChatMessage.MessageState.DownloadFail;
                }
            }
        }

        public void StartDownloadWallpaperRoutine (string fileUrl, FolderLocation folderLocation, StreamSavedTextureCallbackMethod streamSavedCallback = null)
        {
            downloadFileUrl = fileUrl;
            StartCoroutine(DownloadWallpaperRoutine(fileUrl, folderLocation, streamSavedCallback));
        }

        private StreamSavedTextureCallbackMethod fileSavedTextureCallback;
        IEnumerator DownloadWallpaperRoutine (string url, FolderLocation folderLocation, StreamSavedTextureCallbackMethod streamSavedCallback)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(folderLocation + " :: " + url);
            #endif
            saveFolderLocation = folderLocation;
//            fileSavedTextureCallback = streamSavedCallback;

            string wallpaperPath = StreamManager.LoadFilePath(Path.GetFileName(url), folderLocation, false);
            WWW request = null;

            if (File.Exists (wallpaperPath))
            {
                request = new WWW("file://" + wallpaperPath);
            }
            else
            {
                request = new WWW(url);
            }

            while(!request.isDone)
            {
                yield return null;
            }

            yield return request;

            if (string.IsNullOrEmpty(request.error))
            {
                if (File.Exists (wallpaperPath) == false)
                {
                    string _filename = System.IO.Path.GetFileName(url);
                    StreamManager.SaveFile (_filename, request.bytes, folderLocation, null, false);
                }

                if (streamSavedCallback != null)
                {
                    streamSavedCallback(request.texture);
                }
            }
            else
            {
                if (streamSavedCallback != null)
                {
                    streamSavedCallback(null);
                }
                Debug.LogError("url ::: " + url + request.error);
            }

            request.Dispose();
            request = null;
            DownloadManager.Instance.RemoveDownloadRoutine(this);
            Destroy(this.gameObject);
        }

        private byte[] profileImageByte;
        private Texture2D profileImageTexture;
        private string channelUrl;
        private StreamSavedCallbackMethod fileSavedCallback;
        IEnumerator DownloadFileRoutine (string url, FolderLocation folderLocation, StreamSavedCallbackMethod streamSavedCallback, string channelId)
        {
            Debug.Log(folderLocation + " :: " + url);

            string _filename = System.IO.Path.GetFileName(url);
            channelUrl = channelId;
            saveFolderLocation = folderLocation;
            fileSavedCallback = streamSavedCallback;

            WWW request = new WWW(url);

			while(!request.isDone)
			{
				yield return null;
			}

			yield return request;
            
			if (string.IsNullOrEmpty(request.error))
            {
//                profileImageByte = TextureResize.ResizeTexture(150f, 150f, request.texture).EncodeToPNG();
//				StreamManager.SaveFileSecond (_filename, request.bytes, folderLocation, streamSavedCallback, false);
                profileImageTexture = request.texture;
				SaveFile (_filename, request.bytes, folderLocation, streamSavedCallback);
                ProfileFileSavedCallback(true);
            }
            else
            {
                streamSavedCallback(false);
                Debug.LogError("url ::: " + url + request.error);
            }

            DownloadManager.Instance.RemoveDownloadRoutine(this);

            request.Dispose();
            request = null;

            Destroy(this.gameObject);
        }

        void ProfileFileSavedCallback (bool succes)
        {
            if (saveFolderLocation == FolderLocation.Profile || saveFolderLocation == FolderLocation.GroupProfile)
            {
                if (string.IsNullOrEmpty (channelUrl) && profileImageTexture != null)
                {
                    Chat.ChatManager.Instance.SaveprofileByteImage(channelUrl, profileImageTexture);
                }
            }
           
            fileSavedCallback(succes);
        }

		void SaveFile (string filename, byte[] data, FolderLocation location, StreamSavedCallbackMethod callback)
		{
			string filepath = StreamManager.LoadFilePath(filename, location, false);
			try
			{
				using (var file = new FileStream(filepath, FileMode.Create, FileAccess.Write))
				{
					file.Write(data, 0, data.Length);
				}
			}
			catch (Exception e)
			{
				#if UNITY_EDITOR && UNITY_DEBUG
				Debug.LogError(e.Message);
				#endif
				if (callback != null)
					callback (false);
				return;
			}
			if (callback != null)
				callback (true);
		}

        void SaveFile (byte[] data)
        {
            string filename = StreamManager.getCorrectFreakAppPath(fileName, saveFolderLocation, isSendData);
            try
            {
                using (var file = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    file.Write(data, 0, data.Length);
                }
            }
            catch (Exception e)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError(e.Message);
                #endif
                FileSavedCallback (false);
                return;
            }

            FileSavedCallback (true);
        }

        void FileSavedCallback (bool success)
        {
            if (success)
            {
                if (mChatMessage != null)
                {
                    mChatMessage.messageState = Freak.Chat.ChatMessage.MessageState.DownloadSuccess;
                }
            }
            else
            {
                if (mChatMessage != null)
                {
                    mChatMessage.messageState = Freak.Chat.ChatMessage.MessageState.DownloadFail;
                }
            }

            DownloadManager.Instance.RemoveDownloadRoutine(this);
            Destroy(this.gameObject);
        }

        public void LoadImage(string imagePath, StreamSavedCallbackMethod streamSavedCallback, Chat.ChatChannel chatChannel = null, ServerUserContact serverUserContact = null)
        {
            Debug.Log("LoadImage....");
            downloadFileUrl = imagePath;

//            Texture2D newImage = new Texture2D(4,4);
//            newImage.LoadImage(Freak.StreamManager.LoadFileDirect (imagePath));

            WWW www = new WWW("file://" + imagePath);
//            newImage = TextureResize.ResizeTexture(150f, 150f, www.texture);

            //byte[] bytes = Freak.StreamManager.LoadFileDirect(imagePath);//TextureResize.ResizeTexture(150f, 150f, newImage).EncodeToPNG();
//            newImage = null;

            if (chatChannel != null)
            {
                chatChannel.textureCache = www.texture;
//                chatChannel.imageCache = bytes;
            }

            if (serverUserContact != null)
            {
                //serverUserContact.imageCache = bytes;
                serverUserContact.userImage = www.texture;
            }

            if (streamSavedCallback != null)
            {
                streamSavedCallback(true);
                //streamSavedCallback (bytes != null);
            }

            DownloadManager.Instance.RemoveLoadImageRoutine(this);

            //bytes = null;
            www.Dispose();
            www = null;
//            this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }

        public void DownloadProfile (string url, FolderLocation location, object chatChannel)
        {
            downloadFileUrl = url;
            StartCoroutine (DownloadProfileImage (url, location, chatChannel));
        }

        IEnumerator DownloadProfileImage (string url, FolderLocation location, object chatChannel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("DownloadProfileImage******* " + url);
            #endif
            string _filename = System.IO.Path.GetFileName(url);
            string _filePath = StreamManager.LoadFilePath (_filename, location, false);
            Texture2D texture = new Texture2D(1, 1);
            WWW request = null;

            if (File.Exists(_filePath))
            {
                request = new WWW("file://" + _filePath);
            }
            else
            {
                request = new WWW(url);
            }

            yield return request;

            while (request.isDone == false)
            {
                yield return null;
            }

            if (string.IsNullOrEmpty(request.error))
            {
                texture = request.texture;

                if (texture.width > 100 || texture.height > 100)
                {
                    texture = TextureResize.ResizeTexture(100f, 100f, texture);
                }

                byte[] bytes = texture.EncodeToPNG();
                DownloadManager.Instance.AddProfileTexture2DInCache(url, texture);

                if (File.Exists(_filePath) == false)
                {
                    StreamManager.SaveFile(_filename, bytes, location, null, false);

                    if (location == FolderLocation.Profile)
                    {
                        StreamManager.SaveFile(_filename, request.bytes, FolderLocation.ProfileHD, null, false);
                    }
                    else
                    {
                        StreamManager.SaveFile(_filename, request.bytes, FolderLocation.GroupProfileHD, null, false);
                    }
                }

                /*if (location == FolderLocation.GroupProfile)
                {
                    Freak.Chat.ChatChannel _chatChannel = chatChannel as Freak.Chat.ChatChannel;

                    _chatChannel.textureCache = texture;
                    _chatChannel.coverUrl = url;
                }
                else if (location == FolderLocation.Profile)
                {
                    if (chatChannel is Freak.Chat.ChatChannel)
                    {
                        Freak.Chat.ChatChannel _chatChannel = chatChannel as Freak.Chat.ChatChannel;
                        _chatChannel.GetSender().texture = texture;
                        _chatChannel.GetSender().profileUrl = url;
                    }
                    else
                    {
                        ServerUserContact _chatChannel = chatChannel as ServerUserContact;
                        _chatChannel.userImage = texture;
                    }
                }*/
            }
            else
            {
                Debug.LogError("url ::: " + url + request.error);
            }

            request.Dispose();
            request = null;
            texture = null;

            DownloadManager.Instance.RemoveDownloadProfile(this);
//            this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }

        public void DownloadStoreGameTexture (string url, StoreGames storeGames)
        {
            downloadFileUrl = url;
            StartCoroutine (DownloadStoreGameTextureRoutine (url, storeGames));
        }

        IEnumerator DownloadStoreGameTextureRoutine (string url, StoreGames storeGames)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("DownloadImageTextureRoutine..===.." + url);
            #endif
            string _filename = System.IO.Path.GetFileName(url);
            string _filePath = StreamManager.LoadFilePath (_filename, FolderLocation.Games, false);
            WWW request = null;
            Texture2D _texture = new Texture2D(1,1);

            if (File.Exists(_filePath))
            {
                request = new WWW("file://" + _filePath);
            }
            else
            {
                request = new WWW(url);
            }

			yield return request;

            while (request.isDone == false)
            {
                yield return null;
            }

            if (string.IsNullOrEmpty(request.error))
            {
                _texture = request.texture;

                if (_texture.width > 150 || _texture.height > 150)
                {
                    _texture = TextureResize.ResizeTexture(150f, 150f, _texture);
                }

                byte[] bytes = _texture.EncodeToPNG();

                if (File.Exists(_filePath) == false)
                {
                    StreamManager.SaveFile(_filename, bytes, FolderLocation.Games, null, false);
                }

                _texture.Compress(false);
                storeGames.gameTexture = _texture;
            }
            else
            {
                Debug.LogError("url ::: " + url + request.error);
            }

            request.Dispose();
            request = null;
            _texture = null;

            DownloadManager.Instance.RemoveDownloadRoutine(this);
            Destroy(this.gameObject);
        }

        void ProfileFileSaved (bool success)
        {
            DownloadManager.Instance.RemoveDownloadRoutine(this);
            Destroy(this.gameObject);
        }

        #region Image Message Handler

        public void LoadImageMessage (string path, Freak.Chat.ChatMessage chatMessage)
        {
            downloadFileUrl = path;
            StartCoroutine (LoadImageMessageRoutine (path, chatMessage));
        }

        IEnumerator LoadImageMessageRoutine (string _filePath, Freak.Chat.ChatMessage chatMessage)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("LoadImageMessageRoutine..===.." + _filePath);
            #endif
            WWW request = null;
            Texture2D texture = new Texture2D (1, 1, TextureFormat.ARGB32, false);

            if (File.Exists(_filePath))
            {
                request = new WWW("file://" + _filePath);

                yield return request;

                while(!request.isDone)
                {
                    yield return null;
                }

                if (string.IsNullOrEmpty(request.error))
                {
                    texture = request.texture;
//                    if (texture.width > 250 || texture.height > 250)
//                    {
//                        texture.Compress(false);
//                    }

                    chatMessage.imageMessageCache = texture;
                }
                else
                {
                    Debug.LogError("url ::: " + _filePath + request.error);
                }
            }

            DownloadManager.Instance.FinishImageMessageRoutine(this, chatMessage);

            request.Dispose();
            request = null;
            texture = null;

            Destroy(this.gameObject);
        }

        #endregion

        void OnDisable ()
        {
            downloadFileUrl = string.Empty;
            Resources.UnloadUnusedAssets();
        }
    }
}
