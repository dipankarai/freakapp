﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Freak
{
    public class DownloadManager : MonoBehaviour
    {

#region Init Instance
        private static DownloadManager instance;
        public static DownloadManager Instance
        {
            get
            {
                return InitInstance();
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public static DownloadManager InitInstance()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("~DownloadManager");
                instance = instGO.AddComponent<DownloadManager>();

                DontDestroyOnLoad(instGO);
            }
            return instance;
        }
#endregion

//        [System.Serializable]
//        class DownloadManagerQueue
//        {
//            public string downloadFileUrl;
//            public string url;
//            public FolderLocation folderLocation;
//            public Chat.MessageBoxBase messageBoxBase;
//            public bool isSentData;
//            public string extension;
//        }

        List<DownloadRoutine> downloadRoutineList = new List<DownloadRoutine>();

//        IEnumerator downloadRoutine;
//        Freak.Chat.MessageBoxBase mMessageBoxBase;
//        string downloadFileUrl;
//        FolderLocation saveFolderLocation;
//        bool isSendData;
//        Freak.Chat.ChatWindowUI mChatWindowUI;

        bool isDownloading;
//        List<DownloadManagerQueue> downloadManagerQueueList = new List<DownloadManagerQueue>();

        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
        }

        // Use this for initialization
//        void Start () {
//            
//        }

        /*void Update ()
        {
            if (downloadManagerQueueList.Count == 0)
                return;

            foreach (var que in downloadManagerQueueList.ToArray())
            {
                if (isDownloading == false)
                {
                }
            }
        }*/
        
        public void DownLoadFile (string fileUrl, FolderLocation folderLocation, Chat.ChatMessage chatMessage = null, bool isSentData = false, string extension = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log (folderLocation + " :: " + fileUrl + "::" + isSentData + " :: " + extension);
            #endif

            if (CheckForExistDownloadRoutine(fileUrl) == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Ready for Downloading-------> "+Path.GetFileNameWithoutExtension(fileUrl));
                #endif 

                /*if(isDownloading)
                {
                    var que = new DownloadManagerQueue();
                    que.url = fileUrl;
                    que.folderLocation = folderLocation;
                    que.messageBoxBase = msgBoxBase;
                    que.isSentData = isSentData;
                    que.extension = extension;

                    downloadManagerQueueList.Add(que);
                    return;
                }

                isDownloading = true;*/

                GameObject go = new GameObject(Path.GetFileNameWithoutExtension(fileUrl));
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                downloadRoutineList.Add(_routine);
                _routine.StartDownloadRoutine (fileUrl, folderLocation, chatMessage, isSentData, extension);
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Its Already Downloading------->");
                #endif
            }
        }

        public void DownLoadFile (string fileUrl, FolderLocation folderLocation, StreamSavedCallbackMethod streamSavedCallback = null, string channelId = "")
        {
            if (CheckForExistDownloadRoutine(fileUrl) == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Ready for Downloading-------> "+Path.GetFileNameWithoutExtension(fileUrl));
                #endif 

                GameObject go = new GameObject(Path.GetFileNameWithoutExtension(fileUrl));
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                downloadRoutineList.Add(_routine);
                _routine.StartDownloadRoutine (fileUrl, folderLocation, streamSavedCallback, channelId);
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Its Already Downloading------->");
                #endif
            }
        }

        public void DownLoadWallpaper (string fileUrl, FolderLocation folderLocation, StreamSavedTextureCallbackMethod streamSavedCallback = null)
        {
//            if (CheckForExistDownloadRoutine(fileUrl) == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("Ready for Downloading-------> "+Path.GetFileNameWithoutExtension(fileUrl));
                #endif 

                GameObject go = new GameObject(Path.GetFileNameWithoutExtension(fileUrl));
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                downloadRoutineList.Add(_routine);
                _routine.StartDownloadWallpaperRoutine (fileUrl, folderLocation, streamSavedCallback);
            }
//            else
//            {
//                #if UNITY_EDITOR && UNITY_DEBUG
//                Debug.Log ("Its Already Downloading------->");
//                #endif
//                if (streamSavedCallback != null)
//                {
//                    streamSavedCallback(null);
//                }
//            }
        }

        public List<DownloadRoutine> poolObjectList = new List<DownloadRoutine>();

        DownloadRoutine GetPoolObject (string objectName)
        {
            if (poolObjectList == null || poolObjectList.Count == 0)
            {
                GameObject go = new GameObject(objectName);
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                poolObjectList.Add(_routine);
                return _routine;
            }

            for (int i = 0; i < poolObjectList.Count; i++)
            {
                if (poolObjectList[i].gameObject.activeInHierarchy == false)
                {
                    poolObjectList[i].name = objectName;
                    poolObjectList[i].gameObject.SetActive(true);
                    return poolObjectList[i];
                }
                else
                {
                    GameObject go = new GameObject(objectName);
                    go.transform.SetParent(this.transform);
                    DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                    poolObjectList.Add(_routine);
                    return _routine;
                }
            }

            return null;
        }

        private List<DownloadRoutine> loadImageRoutineList = new List<DownloadRoutine>();
        public void LoadImage(string imagePath, StreamSavedCallbackMethod streamSavedCallback, Chat.ChatChannel chatChannel = null, ServerUserContact serverUserContact = null)
        {
            if (CheckForExistLoadImageRoutine(imagePath) == false)
            {
                GameObject go = new GameObject(Path.GetFileNameWithoutExtension(imagePath));
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();

//                DownloadRoutine _routine = GetPoolObject(Path.GetFileNameWithoutExtension(imagePath));
                loadImageRoutineList.Add(_routine);
                _routine.LoadImage(imagePath, streamSavedCallback, chatChannel, serverUserContact);
            }
        }

        public struct DownloadProfileWaiting
        {
            public string url; 
            public FolderLocation location;
            public object channel;
        }
        List<DownloadProfileWaiting> downloadProfileWaitingList = new List<DownloadProfileWaiting>();
        List<DownloadRoutine> downloadProfileList = new List<DownloadRoutine>();

        public void DownloadProfile(string url, FolderLocation location, object channel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("DownloadProfileImage.... " +url);
            #endif

            if (CheckForExistDownloadProfile (url) == false)
            {
                if (downloadProfileList.Count > 5)
                {
                    DownloadProfileWaiting _waiting;
                    _waiting.url = url;
                    _waiting.location = location;
                    _waiting.channel = channel;
                    
                    downloadProfileWaitingList.Add(_waiting);
                }
                else
                {
                    GameObject go = new GameObject(Path.GetFileNameWithoutExtension(url));
                    go.transform.SetParent(this.transform);
                    DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                    downloadProfileList.Add(_routine);
                    //                downloadRoutineList.Add(_routine);
                    _routine.DownloadProfile(url, location, channel);
                }
            }
        }

        bool CheckForExistDownloadProfile (string fileUrl)
        {
            for (int i = 0; i < downloadProfileList.Count; i++)
            {
                if (downloadProfileList[i].FileName == fileUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
//                    Debug.Log ("Already In Queqe " + fileUrl);
                    #endif
                    return true;
                }
            }

            return false;
        }

        public void RemoveDownloadProfile (DownloadRoutine downloadRoutine)
        {
            downloadProfileList.Remove(downloadRoutine);
            if (downloadProfileWaitingList.Count > 0)
            {
                DownloadProfile(downloadProfileWaitingList[0].url, downloadProfileWaitingList[0].location, downloadProfileWaitingList[0].channel);
                downloadProfileWaitingList.RemoveAt(0);
            }
        }

        List<Dictionary<string,Texture2D>> profileImageCacheList = new List<Dictionary<string, Texture2D>>();
        public void AddProfileTexture2DInCache (string url, Texture2D texture)
        {
            for (int i = 0; i < profileImageCacheList.Count; i++)
            {
                if (profileImageCacheList[i].ContainsKey(url))
                {
                    return;
                }
            }

            Dictionary<string, Texture2D> _tempCacheDict = new Dictionary<string, Texture2D>();
            _tempCacheDict.Add(url, texture);
            profileImageCacheList.Add(_tempCacheDict);
        }

        public Texture2D GetProfileTexture2DFromCache (string url, FolderLocation location, object channel)
        {
            if (Path.GetFileName(url).Contains("avatar.png"))
            {
                return FreakAppManager.Instance.defaultProfileTexture;
            }

            for (int i = 0; i < profileImageCacheList.Count; i++)
            {
                if (profileImageCacheList[i].ContainsKey(url))
                {
                    Dictionary<string, Texture2D> _tempCacheDict = new Dictionary<string, Texture2D>(profileImageCacheList[i]);
                    return (Texture2D)_tempCacheDict[url];
                }
            }

            DownloadManager.Instance.DownloadProfile(url, location, channel);
            return null;
        }

        public void DownloadStoreGameTexture(string url, StoreGames storeGames)
        {
            if (CheckForExistDownloadRoutine (url) == false)
            {
                //                DownloadRoutine _routine = GetPoolObject(Path.GetFileNameWithoutExtension(url));
                GameObject go = new GameObject(Path.GetFileNameWithoutExtension(url));
                go.transform.SetParent(this.transform);
                DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                downloadRoutineList.Add(_routine);
                _routine.DownloadStoreGameTexture (url, storeGames);
            }
        }

        bool CheckForExistDownloadRoutine (string fileUrl)
        {
            for (int i = 0; i < downloadRoutineList.Count; i++)
            {
                if (downloadRoutineList[i].FileName == fileUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    //                    Debug.Log ("Already In Queqe " + fileUrl);
                    #endif
                    return true;
                }
            }

            return false;
        }

        bool CheckForExistLoadImageRoutine (string fileUrl)
        {
            for (int i = 0; i < loadImageRoutineList.Count; i++)
            {
                if (loadImageRoutineList[i].FileName == fileUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
//                    Debug.Log ("Already In Queqe " + fileUrl);
                    #endif
                    return true;
                }
            }

            return false;
        }

        public void RemoveDownloadRoutine (DownloadRoutine downloadRoutine)
        {
            downloadRoutineList.Remove(downloadRoutine);
            isDownloading = false;
        }

        public void RemoveLoadImageRoutine (DownloadRoutine downloadRoutine)
        {
            loadImageRoutineList.Remove(downloadRoutine);
            isDownloading = false;
        }

        public struct LoadImageQueueStruct
        {
            public byte[] imageByte;
            public string userid;
            public Action<string,Sprite> callback;
        }

        private List<LoadImageQueueStruct> mLoadImageQueueStructList;
        private bool isImageLoading;
        public void LoadImageQueue (byte[] imageByte, string userid, Action<string,Sprite> callback)
        {
            if (mLoadImageQueueStructList == null)
                mLoadImageQueueStructList = new List<LoadImageQueueStruct> ();

            if (isImageLoading)
            {
                if (CheckForPreviousImageQueue(userid) == false)
                {
                    LoadImageQueueStruct _Queue;
                    _Queue.callback = callback;
                    _Queue.imageByte = imageByte;
                    _Queue.userid = userid;
                    
                    mLoadImageQueueStructList.Add (_Queue);
                }
                return;
            }

            isImageLoading = true;
            StartCoroutine (MakeSprite (imageByte, userid, callback));
        }

        IEnumerator MakeSprite (byte[] imageByte, string userid, Action<string,Sprite> callback)
        {
            Sprite newSprite = null;
            while (newSprite == null)
            {
                Texture2D texture = new Texture2D (4,4);
                texture.LoadImage(imageByte);
                
                Rect rect = new Rect(0, 0, texture.width, texture.height);
                newSprite = Sprite.Create (texture, rect, new Vector2 (0, 0), 1f);
                
                texture = null;
                yield return null;
            }

            if (callback != null)
            {
                callback (userid, newSprite);
            }

            isImageLoading = false;
            newSprite = null;
            if (mLoadImageQueueStructList.Count > 0)
            {
                LoadImageQueue (mLoadImageQueueStructList [0].imageByte, mLoadImageQueueStructList [0].userid, mLoadImageQueueStructList [0].callback);
                mLoadImageQueueStructList.RemoveAt (0);
            }
        }

        bool CheckForPreviousImageQueue (string userid)
        {
            for (int i = 0; i < mLoadImageQueueStructList.Count; i++)
            {
                if (mLoadImageQueueStructList[i].userid == userid)
                {
                    return true;
                }
            }

            return false;
        }

		public struct LoadCompressByte
		{
			public string userId;
			public string urlLink;
			public Action<string,byte[]> callback;
		}

		List<ProfileImageRoutine> profileImageRoutineList = new List<ProfileImageRoutine>();
		List<LoadCompressByte> loadCompressByteQueue = new List<LoadCompressByte>();
		public void LoadImageAsCompressedByte (string urlLink, string userId, Action<string,byte[]> callback)
		{
			if (profileImageRoutineList.Count < 5)
			{
				GameObject go = new GameObject(Path.GetFileNameWithoutExtension(urlLink));
				go.transform.SetParent(this.transform);
				ProfileImageRoutine _routine = go.AddComponent<ProfileImageRoutine>();
				profileImageRoutineList.Add(_routine);
				_routine.LoadImageAsCompressedByte (urlLink, userId, callback);
			}
			else
			{
				LoadCompressByte _LoadCompressByte;
				_LoadCompressByte.userId = userId;
				_LoadCompressByte.urlLink = urlLink;
				_LoadCompressByte.callback = callback;

				loadCompressByteQueue.Add (_LoadCompressByte);
			}
		}

		public void LoadImageAsCompressedByteDone ()
		{
			if (loadCompressByteQueue.Count > 0)
			{
				LoadImageAsCompressedByte (loadCompressByteQueue [0].urlLink, loadCompressByteQueue [0].userId, loadCompressByteQueue [0].callback);
				loadCompressByteQueue.RemoveAt (0);
			}
		}

        #region Image Message Handler

        private Dictionary<string, Chat.ChatMessage> imageMessageCacheList = new Dictionary<string, Chat.ChatMessage>();
        private List<DownloadRoutine> loadImageMessageList = new List<DownloadRoutine>();
        public void LoadImageMessage (string path, Freak.Chat.ChatMessage chatMessage)
        {
//            if (CheckForLoadImageMessagePreviousCache (path, chatMessage) == false)
            {
                if (CheckForLoadImageMessageQueue(path) == false)
                {
                    GameObject go = new GameObject(Path.GetFileNameWithoutExtension(path));
                    go.transform.SetParent(this.transform);
                    DownloadRoutine _routine = go.AddComponent<DownloadRoutine>();
                    loadImageMessageList.Add(_routine);
                    _routine.LoadImageMessage (path, chatMessage);
                }
            }
        }

        bool CheckForLoadImageMessageQueue (string path)
        {
            for (int i = 0; i < loadImageMessageList.Count; i++)
            {
                if (loadImageMessageList[i].FileName == path)
                {
                    return true;
                }
            }

            return false;
        }

        bool CheckForLoadImageMessagePreviousCache (string path, Freak.Chat.ChatMessage chatMessage)
        {
            foreach (var item in imageMessageCacheList)
            {
                if (item.Key == path)
                {
                    chatMessage.imageMessageCache = imageMessageCacheList[path].imageMessageCache;
                    return true;
                }
            }

            return false;
        }

        public void FinishImageMessageRoutine (DownloadRoutine downloadRoutine, Chat.ChatMessage chatTexture)
        {
            bool alreadyContain = false;

//            if (imageMessageCacheList.Count > 10)
//            {
//                imageMessageCacheList.Clear();
//                Resources.UnloadUnusedAssets();
//            }

            foreach (var item in imageMessageCacheList)
            {
                if (item.Key == downloadRoutine.FileName)
                {
//                    imageMessageCacheList[downloadRoutine.FileName] = chatTexture;
                    alreadyContain = true;
                }
            }

            if (alreadyContain == false)
            {
                imageMessageCacheList.Add(downloadRoutine.FileName, chatTexture);
            }

            loadImageMessageList.Remove(downloadRoutine);
        }

        public void DestroyLoadImageMessageCache ()
        {
            foreach (var item in imageMessageCacheList)
            {
                item.Value.imageMessageCache = null;
            }

            imageMessageCacheList.Clear();
            Resources.UnloadUnusedAssets();
        }

        #endregion
    }
}
