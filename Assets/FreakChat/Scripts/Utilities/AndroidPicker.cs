using UnityEngine;

namespace ImageAndVideoPicker
{

	public class AndroidPicker
	{
		#if UNITY_ANDROID
		static AndroidJavaClass _plugin;

		static AndroidPicker()
		{
			_plugin = new AndroidJavaClass("com.astricstore.imageandvideopicker.AndroidPicker");
		}

		public static void BrowseImage()
		{
			_plugin.CallStatic("BrowseForImage",false,1,1);
			
		}

		public static void BrowseImage(bool cropping, int aspectX = 1, int aspectY = 1)
		{
			_plugin.CallStatic("BrowseForImage",cropping,aspectX,aspectY);

		}

		public static void BrowseVideo()
		{
			_plugin.CallStatic("BrowseForVideo");

		}

		public static void BrowseContact()
		{
			_plugin.CallStatic("BrowseForContact");
			
		}

		public static void BrowseFile()
		{
			_plugin.CallStatic("BrowseForFile");
		}

		public static void BrowseForAudio()
		{
		_plugin.CallStatic("BrowseForAudio");
		}

		public static void CaptureImage()
		{
		_plugin.CallStatic("CaptureImage");
		}

		public static void ViewImageInGallery(string path)
		{
			_plugin.CallStatic ("ViewImageInGallery", path);
		}

		public static void ReadPdf(string path)
		{
			_plugin.CallStatic ("ReadPdf", path);
		}

		public static void ReadVCF(string path)
		{
			
			_plugin.CallStatic ("openBackup", path);
		}

		public static void CheckIfFileComesFromOtherApp()
		{
			_plugin.CallStatic ("CheckIfFileComesFromOtherApp");
		}

        public static void OpenVideo (string path)
        {
            _plugin.CallStatic("PlayVideo", path);
        }

        public static void OpenApplication (string path)
        {
            _plugin.CallStatic("OpenApplication", path);
        }
#endif
	}
}


