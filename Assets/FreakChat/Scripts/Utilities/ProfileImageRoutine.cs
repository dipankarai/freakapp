﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Freak
{
	public class ProfileImageRoutine : MonoBehaviour
	{

		public string Url;
		private string UserId;
		private Action<string,byte[]> Callback;
		private byte[] ImageByte;

		// Use this for initialization
		void Start () {
		
		}

		public void LoadImageAsCompressedByte (string urlLink, string userId, Action<string,byte[]> callback)
		{
			Url = urlLink;
			UserId = userId;
			Callback = callback;

			string filePath = StreamManager.LoadFilePath (Path.GetFileName(urlLink), FolderLocation.Profile, false);

			if (File.Exists(filePath))
			{
				Byte[] imageByte = Freak.StreamManager.LoadFileDirect (filePath);
				ImageByte = TextureResize.GetResizedByteFromByte (150f, 150f, imageByte);

				Done (true);
			}
			else
			{
				StartCoroutine (DownloadImage (urlLink));
			}
		}

		IEnumerator DownloadImage(string urlLink)
		{
			WWW request = new WWW(urlLink);

			yield return request;

			if (string.IsNullOrEmpty(request.error))
			{
				string _filename = StreamManager.LoadFilePath (Path.GetFileName(urlLink), FolderLocation.Profile, false);
//				StreamManager.SaveFileSecond (_filename, request.bytes, FolderLocation.Profile, null, false);
				SaveFile (_filename, request.bytes, FolderLocation.Profile, null);
				ImageByte = TextureResize.GetResizedByteFromByte (150f, 150f, request.bytes);

				Done (true);
			}
			else
			{
				#if UNITY_EDITOR && UNITY_DEBUG
				Debug.LogError (request.error + " => " + urlLink);
				#endif


				Done (false);
			}
		}

		void SaveFile (string filename, byte[] data, FolderLocation location, StreamSavedCallbackMethod callback)
		{
			string filepath = StreamManager.LoadFilePath(filename, location, false);
			try
			{
				using (var file = new FileStream(filepath, FileMode.Create, FileAccess.Write))
				{
					file.Write(data, 0, data.Length);
				}
			}
			catch (Exception e)
			{
				#if UNITY_EDITOR && UNITY_DEBUG
				Debug.LogError(e.Message);
				#endif
				if (callback != null)
					callback (false);
				return;
			}
			if (callback != null)
				callback (true);
		}

		void Done (bool success)
		{
			if (success && Callback != null)
			{
				Callback (UserId, ImageByte);
			}

			Destroy (this.gameObject);
		}
	}
}