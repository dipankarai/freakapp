﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    /// <summary>
    /// References class holds all the references of prefrabs and others assets
    /// which is used for instantiating at runtime.
    /// </summary>
    public class References : MonoBehaviour
    {
        [Header("Freak Chat Properties")]
        #region Chat Properties
        const string CHAT_PREFAB = "Prefab/Chat/";

        [SerializeField]
        private string chatWindowPrefab;
        public GameObject ChatWindow { get { return ResourcesLoadChatPrefab(chatWindowPrefab); } }

        [SerializeField]
        private string chatContentPrefab;
        public GameObject ChatContent { get { return ResourcesLoadChatPrefab(chatContentPrefab); } }

        [SerializeField]
        private string messageWindowPrefab;
        public GameObject MessageWindow { get { return ResourcesLoadChatPrefab(messageWindowPrefab); } }

        [SerializeField]
        private string systemWindowPrefab;//SystemWindow
        public GameObject SystemWindow { get { return ResourcesLoadChatPrefab(systemWindowPrefab); } }

        [SerializeField]
        private string audioBuzzContentItemPrefab;//AudioBuzzContentItem
        public GameObject AudioBuzzContentItem { get { return ResourcesLoadChatPrefab(audioBuzzContentItemPrefab); } }

        [SerializeField]
        private string acceptRejectChallenge; // AcceptOrRejectChallenge
        public AcceptOrRejectChalleng AcceptRejectChallenge { get { return ResourcesLoadMessageBoxPrefab(acceptRejectChallenge).GetComponent<AcceptOrRejectChalleng>(); } }

		[SerializeField]
        private string gameReferralAcceptReject;//GameReferralAcceptReject
        public GameReferralAcceptReject GameReferralAcceptReject { get { return ResourcesLoadMessageBoxPrefab(gameReferralAcceptReject).GetComponent<GameReferralAcceptReject>(); } }

        [SerializeField]
        private string groupSearchPopup;
        public GroupMemberSearch GroupSearchPopup { get { return (GroupMemberSearch) Resources.Load<GroupMemberSearch>(groupSearchPopup); } }

        public string notificationSoundPath = "Audio/SFX/messagereceived"; 
        public AudioClip notificationSound { get { return Resources.Load(notificationSoundPath) as AudioClip; } }
        public string messageSoundPath = "Audio/SFX/messagesend";
        public AudioClip messageSound { get { return Resources.Load(messageSoundPath) as AudioClip; } }

        [Header("Chat Message Box")]
        public string audioMessageBoxPath = "AudioMessageBox";
        public AudioMessageBox audioMessageBox { get { return ResourcesLoadMessageBoxPrefab(audioMessageBoxPath).GetComponent<AudioMessageBox>(); } }
        public string audioBuzzMessageBoxpath = "AudioBuzzMessageBox";
        public AudioBuzzMessageBox audioBuzzMessageBox { get { return ResourcesLoadMessageBoxPrefab(audioBuzzMessageBoxpath).GetComponent<AudioBuzzMessageBox>(); } }
        public string broadcastMessageBoxPath = "BroadcastMessageBox";
        public BroadcastMessageBox broadcastMessageBox { get { return ResourcesLoadMessageBoxPrefab(broadcastMessageBoxPath).GetComponent<BroadcastMessageBox>(); } }
        public string contactMessageBoxPath = "ContactMessageBox";
        public ContactMessageBox contactMessageBox { get { return ResourcesLoadMessageBoxPrefab(contactMessageBoxPath).GetComponent<ContactMessageBox>(); } }
        public string imageMessageBoxPath = "ImageMessageBox";
        public ImageMessageBox imageMessageBox { get { return ResourcesLoadMessageBoxPrefab(imageMessageBoxPath).GetComponent<ImageMessageBox>(); } }
        public string otherFileMessageBoxPath = "OtherFileMessageBox";
        public OtherFileMessageBox otherFileMessageBox { get { return ResourcesLoadMessageBoxPrefab(otherFileMessageBoxPath).GetComponent<OtherFileMessageBox>(); } }
        public string systemMessageBoxPath = "SystemMessageBox";
        public SystemMessageBox systemMessageBox { get { return ResourcesLoadMessageBoxPrefab(systemMessageBoxPath).GetComponent<SystemMessageBox>(); } }
        public string textMessageBoxPath = "TextMessageBox";
        public TextMessageBox textMessageBox { get { return ResourcesLoadMessageBoxPrefab(textMessageBoxPath).GetComponent<TextMessageBox>(); } }
        public string videoMessageBoxPath = "VideoMessageBox";
        public VideoMessageBox videoMessageBox { get { return ResourcesLoadMessageBoxPrefab(videoMessageBoxPath).GetComponent<VideoMessageBox>(); } }
        public string timeStampBoxPath = "TimeStampBox";
        public TimeStampBox timeStampBox { get { return ResourcesLoadMessageBoxPrefab(timeStampBoxPath).GetComponent<TimeStampBox>(); } }

        GameObject ResourcesLoadChatPrefab (string prefabName)
        {
            return Resources.Load(CHAT_PREFAB + prefabName) as GameObject;
        }

        GameObject ResourcesLoadMessageBoxPrefab (string prefabName)
        {
            return Resources.Load(CHAT_PREFAB + "MessageBox/" + prefabName) as GameObject;
        }

        #endregion

        [Header("Store Properties")]
		#region Store
		[SerializeField]
        private string storePanel = "StorePanel";
        public GameObject StorePanel { get { return ResourceLoadStorePrefab(storePanel); } }

		[SerializeField]
        private string storeGamesPanel = "StoreGamesUiPanel";
        public GameObject StoreGamesPanel { get { return ResourceLoadStorePrefab(storeGamesPanel); } }

		[SerializeField]
        private string storeStickersPanel = "StoreStickersPanel";
        public GameObject StoreStickersPanel { get { return ResourceLoadStorePrefab(storeStickersPanel); } }

		[SerializeField]
        private string storeAudioBuzzPanel = "StoreAudioBuzzPanel";
        public GameObject StoreAudioBuzzPanel { get { return ResourceLoadStorePrefab(storeAudioBuzzPanel); } }

		[SerializeField]
        private string storeFreakCoinPanel = "StoreFreaksCoinUIPanel";
        public GameObject StoreFreakCoinPanel { get { return ResourceLoadStorePrefab(storeFreakCoinPanel); } }

		[SerializeField]
        private string selectPlayerPanel = "SelectPlayerPanel";
        public GameObject SelectPlayerPanel { get { return ResourceLoadStorePrefab(selectPlayerPanel); } }

        GameObject ResourceLoadStorePrefab (string prefabName)
        {
            return Resources.Load("Prefab/Store/" + prefabName) as GameObject;
        }
		#endregion

		#region Settings
		[Header("Freaks Properties")]
		[SerializeField]
        private string freaksPanel;
        public GameObject FreaksPanel { get { return Resources.Load(freaksPanel) as GameObject; } }
		#endregion

        [Header("Settings Properties")]
		#region Settings
		[SerializeField]
		private string settingsPanel;
        public GameObject SettingsPanel { get { return ResourceLoadSettingsPrefab(settingsPanel); } }

		[SerializeField]
        private string setWallpaperPanel;
        public GameObject SetWallpaperPanel { get { return ResourceLoadSettingsPrefab(setWallpaperPanel); } }

		[SerializeField]
        private string privacyPanel;
        public GameObject PrivacyPanel { get { return ResourceLoadSettingsPrefab(privacyPanel); } }

		[SerializeField]
        private string myTitlesPanel;
        public GameObject MyTitlesPanel { get { return ResourceLoadSettingsPrefab(myTitlesPanel); } }

		[SerializeField]
        private string mpinPanel;
        public GameObject MPINPanel { get { return ResourceLoadSettingsPrefab(mpinPanel); } }

		[SerializeField]
        private string mpinDetailsEnterPanel;
        public GameObject MPINDetailsEnterPanel { get { return ResourceLoadSettingsPrefab(mpinDetailsEnterPanel); } }
		
		[SerializeField]
        private string notificationsPanel;
        public GameObject NotificationsPanel { get { return ResourceLoadSettingsPrefab(notificationsPanel); } }

		[SerializeField]
        private string privacyPolicyPanel;
        public GameObject PrivacyPolicyPanel { get { return ResourceLoadSettingsPrefab(privacyPolicyPanel); } }

		[SerializeField]
        private string blockedUsersSettingPanel;
        public GameObject BlockedUsersSettingPanel { get { return ResourceLoadSettingsPrefab(blockedUsersSettingPanel); } }


        GameObject ResourceLoadSettingsPrefab (string prefabName)
        {
            return Resources.Load("Prefab/Settings/" + prefabName) as GameObject;
        }
		#endregion

        [Header("Group Chat Properties")]
		#region Profile
		[SerializeField]
        private string groupProfilePanel;
        public GameObject GroupProfilePanel { get { return Resources.Load( groupProfilePanel) as GameObject; } }

		[SerializeField]
        private string profilePanel;
        public GameObject ProfilePanel { get { return Resources.Load(profilePanel) as GameObject; } }

        [Header("User Profile Properties")]
		[SerializeField]
        private string userProfilePanel;
        public GameObject UserProfilePanel { get { return Resources.Load(userProfilePanel) as GameObject; } }
		#endregion

        [Header("Game Properties")]
		#region Game
		[SerializeField]
        private string gameHistoryPanel;
        public GameObject GameHistoryPanel { get { return Resources.Load(gameHistoryPanel) as GameObject; } }


		[SerializeField]
        private string gameSelectionPanel;
        public GameObject GameSelectionPanel { get { return Resources.Load(gameSelectionPanel) as GameObject; } }

        [SerializeField]
        private string gameSelectionUnLockedPanel;
        public GameObject GameSelectionUnLockedPanel { get { return Resources.Load(gameSelectionUnLockedPanel) as GameObject; } }

        [SerializeField]
        private string gameSelectionLockedPanel;
        public GameObject GameSelectionLockedPanel { get { return Resources.Load(gameSelectionLockedPanel) as GameObject; } }

        [SerializeField]
        private string gameSelectionOpponentPanel;
        public GameObject GameSelectionOpponentPanel { get { return Resources.Load(gameSelectionOpponentPanel) as GameObject; } }
		#endregion

        [Header("Others Properties")]
        #region Other Properties
		[SerializeField]
		private string generalPopupPanel;
        public GameObject GeneralPopupPanel { get { return Resources.Load(generalPopupPanel) as GameObject; } }

		[SerializeField]
        private string allContactsPanel;
        public GameObject AllContactsPanel { get { return Resources.Load(allContactsPanel) as GameObject; } }
        #endregion

		[Header("News Feed")]
		#region NewsFeed
		[SerializeField]
        private string postNewsFeedPanel;
        public GameObject PostNewsFeedPanel { get { return Resources.Load(postNewsFeedPanel) as GameObject; } }

		[SerializeField]
        private string newsFeedCommentPanel;
        public GameObject NewsFeedCommentPanel { get { return Resources.Load(newsFeedCommentPanel) as GameObject; } }

		[SerializeField]
        private string postImageNewsFeedPanel;
        public GameObject PostImageNewsFeedPanel { get { return Resources.Load(postImageNewsFeedPanel) as GameObject; } }

		[SerializeField]
        private string tagFriendInCommentPanel;
        public GameObject TagFriendPopup { get { return Resources.Load(tagFriendInCommentPanel) as GameObject; } }

		[SerializeField]
		private string likesDisplayPanel;
		public GameObject LikesDisplayPanel { get { return Resources.Load(likesDisplayPanel) as GameObject; } }

		#endregion

		[Header("Contacts")]
		#region NewsFeed
		[SerializeField]
		private string displayContactsPanelToSend;
        public GameObject DisplayContactsPanelToSend { get { return Resources.Load(displayContactsPanelToSend) as GameObject; } }

		[SerializeField]
		private string allContactsPanelToInvite;
        public GameObject AllContactsPanelToInvite { get { return Resources.Load(allContactsPanelToInvite) as GameObject; } }
		#endregion

        #region ASSETS
        public AssetsRef audioBuzzsAssetsRef;

        public AudioClip[] audioBuzzs
        {
            get
            {
                AssetsRef _ref = Resources.Load<AssetsRef>("AssetsRef_Audio");
                return _ref.audioBuzzs;
            }
        }

        public AssetsRef.PuchasedAudio[] purchasedAudioBuzzs
        {
            get
            {
                AssetsRef _ref = Resources.Load<AssetsRef>("AssetsRef_PurchaseAudio");
                return _ref.purchasedAudioBuzzs;
            }
        }

        public AssetsRef.Stickers[] stickers
        {
            get
            {
                AssetsRef _ref = Resources.Load<AssetsRef>("AssetsRef_Stickers");
                return _ref.stickers;
            }
        }

//        public AudioClip[] audioBuzzs;
//        public PuchasedAudio[] purchasedAudioBuzzs;
//        public Stickers[] stickers;
        #endregion
    }

    /*[System.Serializable]
    public class Stickers
    {
        public string name;
        public Sprite main;
        public List<Sprite> stickersList;
    }

	[System.Serializable]
	public class PuchasedAudio
	{
		public string name;
		public List<AudioClip> audioList;
	}*/
}
