﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AssetsRef : MonoBehaviour
{
    [System.Serializable]
    public class Stickers
    {
        public string name;
        public Sprite main;
        public List<Sprite> stickersList;
    }

    [System.Serializable]
    public class PuchasedAudio
    {
        public string name;
        public List<AudioClip> audioList;
    }

    [Header("Buzz Audio Clip")]
    public AudioClip[] audioBuzzs;

    [Header("Purchased Buzz Audio Clip")]
    public PuchasedAudio[] purchasedAudioBuzzs;

    [Header("Stickers")]
    public Stickers[] stickers;
}
