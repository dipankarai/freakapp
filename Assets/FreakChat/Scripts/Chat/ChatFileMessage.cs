﻿using UnityEngine;
using System.Collections;

namespace Freak.Chat
{
    public class ChatFileMessage
    {
        public string RequestId;
        public ChatUser Sender;
        public string Url;
        public string Name;
        public int Size;
        public string Type;
        public string Data;
        public string CustomType;
    }
}
