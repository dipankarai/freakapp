﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class ChatUserMessage
    {
        public string Message = string.Empty;
        public string Data = string.Empty;
        public ChatUser Sender;
        public string RequestId = string.Empty;
        public string CustomType = string.Empty;
        public Dictionary<string, string> Translations = new Dictionary<string, string>();

        public ChatUserMessage ()
        {
            
        }
    }
}
