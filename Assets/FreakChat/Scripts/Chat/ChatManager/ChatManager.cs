﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SendBird;
using Newtonsoft.Json;

namespace Freak.Chat
{
    /// <summary>
    /// Chat manager will manage request and response for chat.
    /// </summary>
    public partial class ChatManager : GroupChatHandler
    {
#region Init Instance
        private static ChatManager instance;
        public static ChatManager Instance
        {
            get
            {
                return InitInstance();
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public static ChatManager InitInstance()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("ChatManager");
                instance = instGO.AddComponent<ChatManager>();

                DontDestroyOnLoad(instGO);
            }
            return instance;
        }
#endregion

#region Sendbird Main Id's

        public const string Appid                   = "F8E950CD-D2A3-48A7-A07A-3CA8CEC95879";        //SeandBird App ID.
        public const string groupChannelEndPoint    = "https://api.sendbird.com/v3/group_channels/";
        public const string openChannelEndPoint     = "https://api.sendbird.com/v3/open_channels";
        public const string apiServerMethod         = "sendbird.api";
        public const string sendbirdAPIToken        = "dd89cd12b72c48286a1ce097ec95b1838e22ad14";
       
        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; } 
        }
#endregion

        /// <summary>
        /// Wheather sendbird is logged in ?
        /// </summary>
        public bool IsSeandbirdLogined { get { return isSeandbirdLogined; } }
        public bool IsInitLogin { get { return isInitLogin; } }
        public int newTotalMessages;
        public int newTotalChats;
        public List<User> memberList = new List<User> ();

        public List<ChatChannel> chatUsersList = new List<ChatChannel>();
        public List<ServerContactsSelectedForGroups> groupUsersList = new List<ServerContactsSelectedForGroups>();
        public ChatChannel currentSelectedChatChannel;
        public List<ChatChannel> cachedGroupChannelData;
        public List<ChatMessage> selectedMessages = new List<ChatMessage> ();
        public List<ChatMessage> forwardingMessages = new List<ChatMessage> ();
        public GetContactListApiManager getContactListApiManager;

        private UserListQuery mUserListQuery;
        private string userId = string.Empty;
        private string userName = string.Empty;
        public string profileLink = string.Empty;
        private string selectedChannelUrl;
        private string textMessages;
        private AudioSource mAudioSource;
        private bool isSeandbirdLogined;
        private bool isInitLogin;
        private bool isDeviceTokenRegistered;

        // Callback Action
        public System.Action loadMessageCallback;

        #region DELEGATE
        public delegate void OnSendBirdLogin (bool login);
        #endregion

        #region EVENT
        public static event OnSendBirdLogin SendBirdLogin = new OnSendBirdLogin((login) => {});
        #endregion

        /// <summary>
        /// Initializes its instance.
        /// </summary>
        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }

            SendBirdClient.SetupUnityDispatcher (gameObject);
            StartCoroutine (SendBirdClient.StartUnityDispatcher);
        }

        public bool IsChannelStarted ()
        {
            if(ChatManager.Instance.currentSelectedChatChannel != null &&
                string.IsNullOrEmpty(ChatManager.Instance.currentSelectedChatChannel.channelUrl) == false)
            {
                return true;
            }

            return false;
        }

        void OnApplicationQuit()
        {
            Disconnect ();
        }

        public void GetChannelTexture (string channelUrl)
        {
            for (int i = 0; i < cachedData.allChannelList.Count; i++)
            {
                if (cachedData.allChannelList[i].channelUrl == channelUrl)
                {
                    if (cachedData.allChannelList[i].isGroupChannel)
                    {
//                        if (cachedData.allChannelList[i].textureCache != null)
//                        {
//                            return cachedData.allChannelList[i].textureCache;
//                        }
//                        else
                        if (Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(cachedData.allChannelList[i].coverUrl, Freak.FolderLocation.GroupProfile, cachedData.allChannelList[i]) == null)
                        {
                            Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(cachedData.allChannelList[i].coverUrl, Freak.FolderLocation.GroupProfile, cachedData.allChannelList[i]);
//                            Freak.DownloadManager.Instance.DownloadProfile(cachedData.allChannelList[i].coverUrl, Freak.FolderLocation.GroupProfile, cachedData.allChannelList[i]);
//                            return null;
                        }
                    }
                    else
                    {
                        if (cachedData.allChannelList[i].GetSender() != null)
                        {
//                            if (cachedData.allChannelList[i].GetSender().texture != null)
//                            {
//                                return cachedData.allChannelList[i].GetSender().texture;
//                            }
//                            else
                            if (Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(cachedData.allChannelList[i].GetSender().profileUrl, Freak.FolderLocation.Profile, cachedData.allChannelList[i]) == null)
                            {
                                Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(cachedData.allChannelList[i].GetSender().profileUrl, Freak.FolderLocation.Profile, cachedData.allChannelList[i]);
//                                Freak.DownloadManager.Instance.DownloadProfile(cachedData.allChannelList[i].GetSender().profileUrl, Freak.FolderLocation.Profile, cachedData.allChannelList[i]);
//                                return null;
                            }
                        }
                    }
                }
            }

//            return null;
        }

        public bool isFilePickerBackground;
        public bool testNotification;
        void OnApplicationPause (bool paused)
        {
            if (paused)
            {
                if (IsSeandbirdLogined)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("OnApplicationPause " + paused + "LOGOUT SendBird");
                    #endif
                    isInitLogin = false;
                    isSeandbirdLogined = false;
                    SendBirdClient.Disconnect(HandleDisconnectHandler);
                    RemoveChannelhandler ();
                    RemoveConnectionHandler ();
                }
            }
            else
            {
                if (IsSeandbirdLogined == false)
                {
                    isInitLogin = false;
                    Instance.InitSendBird ();
                    Instance.LoginToSendbird ();

                    ChannelHandler ();
                    ConnectionHandler ();
                }

            #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("GetNotificationChatChannel");
            #endif

//                Debug.Log("CURRENT SCENE " + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);

//                if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == FreakAppConstantPara.UnitySceneName.FreakHomeScene)
//                {
            #if UNITY_EDITOR
                if (testNotification)
                {
                    PlayerPrefs.SetString(FreakAppConstantPara.PlayerPrefsName.OpenNotificationChatChannel, "sendbird_group_channel_24355757_13a60f5ff2cda1abb227134d6c023f055c9eae1d");
                    OpenChatChannelFromNotification();
                    testNotification = false;
                }

            #else
                #if !DIRECT_BUILD
                PushNotification.GetNotificationChannelUrl();
                #endif
            #endif
//                }
            }
        }

        #if UNITY_EDITOR_S
        void Update ()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                SendBirdClient.Disconnect(() =>
                    {
                        Debug.Log("SENDBIRD Logout");
                    });
                RemoveChannelhandler ();
                RemoveConnectionHandler ();
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                {
                    Instance.InitSendBird ();
                    Instance.LoginToSendbird ();

                    ChannelHandler ();
                    ConnectionHandler ();
                }
            }
        }
        #endif

        public ConnectingUI mConnectingUI;
        void LoadLoaderForSBLogin ()
        {
            if (mConnectingUI != null)
            {
                mConnectingUI.WaitForSendBirdLogin (LoadLoaderForSBLoginCallback);
            }
        }

        void LoadLoaderForSBLoginCallback ()
        {
            mConnectingUI.gameObject.SetActive (false);
        }

        void OnFromBackgroundConnect (User user, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("OnFromBackgroundConnect");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnFromBackgroundConnect " + user.Nickname);
            #endif
            isSeandbirdLogined = true;
        }

        void HandleDisconnectHandler ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleDisconnectHandler SendBird");
            #endif
        }

        public void Disconnect () {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("Disconnected from SendBird");
            #endif
            SendBirdClient.Disconnect (HandleDisconnectHandler);
            RemoveChannelhandler ();
            RemoveConnectionHandler ();
        }

        // Use this for initialization
        public void Start ()
        {
//            mEventProcessor = gameObject.AddComponent<EventProcessor> ();
            base.Start();
            InternetConnection.Init();
            InternetConnection.Instance.OnInternetConnectionStatus = InternetCallback;

            if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.muteUserIDList != null)
            {
                string stringList = JsonConvert.SerializeObject (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.muteUserIDList);
                #if !UNITY_EDITOR
                PushNotification.MuteNotification(stringList);
                #endif
            }
                
            if (SettingsManager.Instance.isDisableFreaks == false)
            {
                ChannelHandler();
                ConnectionHandler ();

                CacheManager.LoadData ();
                UnreadMessageCountNew();
                CacheManager.OnlyGroupChannel();

                //LOAD PROFILE IMAGES...
                for (int i = 0; i < cachedData.allChannelList.Count; i++)
                {
                    ChatManager.Instance.GetChannelTexture(cachedData.allChannelList[i].channelUrl);
                }
            }
        }

#region Refresh SendBird

        private Coroutine refreshCoroutine;
        private bool onReceiveNewMessage;
        const string refreshInvokeString = "StartRefreshCoroutine";
        private ChatWindowUI mChatWindowUI;
        private int count = 0;
        public void StartRefreshCoroutine ()
        {
            if (SettingsManager.Instance.isDisableFreaks)
            {
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("StartRefreshCoroutine");
            #endif

            if (Instance.isInternetConnected)
            {
                    
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("StartRefreshCoroutine " + isSeandbirdLogined);
                #endif
                if (isSeandbirdLogined)
//                if (FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState != HomeScreenUIPanelController.PanelType.ChatWindow)
                {
                    if (ChatManager.Instance.cachedData.allChannelList.Count == 0)
                    {
                        InvokeRefreshCoroutine (5f);
                    }
                    else
                    {
                        InvokeRefreshCoroutine (15f);
                    }

                    RefreshForNewMessages ();
                }
                else
                {
                    count++;
                    InvokeRefreshCoroutine (15f);
                    if (isInitLogin && count == 2)
                    {
                        count = 0;
                        isInitLogin = false;
                    }
                    LoginToSendbird();
                }

//                {
//                    InvokeRefreshCoroutine(15f);
//                    GetChannelByChannelUrl(currentSelectedChatChannel.channelUrl,
//                        (_groupChannel) =>
//                        {
//                            if (mChatWindowUI == null)
//                            {
//                                mChatWindowUI = FreakAppManager.Instance.GetHomeScreenPanel().GetCurrentObject.GetComponent<ChatWindowUI>();
//                            }
//                            
//                            mChatWindowUI.RefreshChannel(_groupChannel);
//                        });
//                }
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("StartRefreshCoroutine No Internet");
                #endif
                InternetConnection.Instance.OnInternetConnectionCallBack = OnInternetReConnected;
            }
        }

        void OnSendBirdLogined (bool login)
        {
            if (login)
            {
                InvokeRefreshCoroutine (1f);
            }
        }

        public void ResetRefreshCoroutine (float time)
        {
            CancelInvoke (refreshInvokeString);
            InvokeRefreshCoroutine (time);
        }

        void InvokeRefreshCoroutine (float time)
        {
            if (IsInvoking(refreshInvokeString))
            {
                Debug.Log("IS AlREADY refreshInvokeString " + time);
                ResetRefreshCoroutine(time);
            }
            else
            {
                Debug.Log("InvokeRefreshCoroutine " + time);
                Invoke (refreshInvokeString, time);
            }
        }

        void OnInternetReConnected ()
        {
            InvokeRefreshCoroutine (2f);
        }

        public void StopRefreshCoroutine ()
        {
            if (refreshCoroutine != null)
            {
                StopCoroutine (refreshCoroutine);
            }
        }

        public Action refreshDoneCallback;
        public void RefreshForNewMessages (Action callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("RefreshForNewMessages " + isSeandbirdLogined);
            #endif
            if (InternetConnection.Check () == false)
            {
                UnreadMessageCountNew ();
                return;
            }

            if (callback != null)
                refreshDoneCallback = callback;
            
            if (isSeandbirdLogined == false)
            {
                LoginToSendbird();
            }
            else
            {
//                cannnotDoSorting = true;
                ChannelListQuery();
            }
        }

#endregion

#region Sendbird Login User Update

        public void InitSendBird()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Initialize SendBird");
            #endif
            SendBirdClient.Init(Appid);
            SendBirdClient.Log += (message) => {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log (message);
                #endif
            };
            
//            InitSenbirdUserDetails ();
        }

        /*public void FirstTimeLoginToSendbird (string userid)
        {
            Instance.UserId = userid;
            SendBirdClient.Connect(Instance.UserId, HandleFirstTimeLoginToSendbird);
        }

        void HandleFirstTimeLoginToSendbird (User user, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleFirstTimeLoginToSendbird");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            isSeandbirdLogined = true;
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleFirstTimeLoginToSendbird " +user.Nickname);
            Debug.Log ("HandleFirstTimeLoginToSendbird " + user.ProfileUrl);
            #endif
        }*/

        public void UpdateUserInfo ()
        {
            SendBirdClient.UpdateCurrentUserInfo(Instance.UserName, Instance.profileLink, HandleUserInfoUpdateHandler);
        }

        void HandleUserInfoUpdateHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleUserInfoUpdateHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleUserInfoUpdateHandler*********");
            #endif
        }

        public void InitSenbirdUserDetails ()
        {
            // Login to sendbird if user id is already saved....
            if (FreakAppManager.Instance.userInfoPlayerPrefs != null)
            {
                LoginToSendbird ();
            }
        }

        public void LoginToSendbird ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError("Login to SendBird " +isInitLogin);
            #endif

            if (isInitLogin == true || (string.IsNullOrEmpty (Instance.UserId) && string.IsNullOrEmpty(Instance.UserName)))
            {
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError("Login to SendBird");
            #endif

            isInitLogin = true;
            SendBirdClient.Connect(Instance.UserId, HandleConnectHandler);
        }

        void HandleConnectHandler (User user, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleConnectHandler error");
                #endif
                isInitLogin = false;
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleConnectHandler");
            #endif
            isSeandbirdLogined = true;
            isInitLogin = false;

            //Login Events
            SendBirdLogin(isSeandbirdLogined);

            if (string.IsNullOrEmpty (Instance.profileLink) == false)
            {
                SendBirdClient.UpdateCurrentUserInfo(Instance.UserName, Instance.profileLink, HandleUserInfoUpdated);
            }

            if (isDeviceTokenRegistered == false)
            {
                RegisterPushNotification ();
            }
        }

        void HandleUserInfoUpdated (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleUserInfoUpdated");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleUserInfoUpdateHandler");
            #endif
//            ChannelListQuery();
        }

        /// <summary>
        /// Initializes the send bird chat sdk.
        /// </summary>
        public void LoginSendbirdChat ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("LoginSendbirdChat with query");
            #endif
            if (isSeandbirdLogined == false)
                LoginToSendbird();
        }

        private Action ReConnectSendBirdCallback;
        public void ReConnectSendBird (Action callback)
        {
            ReConnectSendBirdCallback = callback;
            Instance.InitSendBird ();
            SendBirdClient.Connect (Instance.UserId, ReConnectSendBirdHandler);
        }

        void ReConnectSendBirdHandler (User user, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("ReConnectSendBirdHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                SendBirdClient.Connect (Instance.UserId, ReConnectSendBirdHandler);
                ChannelHandler ();
                ConnectionHandler ();
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("ReConnectSendBirdHandler " + user.Nickname);
            #endif
            isSeandbirdLogined = true;

            if (ReConnectSendBirdCallback != null)
            {
                ReConnectSendBirdCallback ();
                ReConnectSendBirdCallback = null;
            }
        }

#endregion

#region GET USER LIST
        public void GetUserList ()
        {
            mUserListQuery = SendBirdClient.CreateUserListQuery();
            mUserListQuery.Next(HandleUserListQueryResultHandler);
        }

        void HandleUserListQueryResultHandler (List<User> queryResult, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleUserListQueryResultHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleUserListQueryResultHandler " + JsonConvert.SerializeObject(queryResult));
            #endif
        }
#endregion   //GET USER LIST

#region Invite Users
        private System.Action inviteUserCallback;
        public void InviteUser(List<string> users, System.Action callback)
        {
            inviteUserCallback = callback;

            GroupChannel _groupChannel = currentChannel as GroupChannel;

            if (users.Count == 1)
            {
                _groupChannel.InviteWithUserId(users[0], HandleInviteUserHandler);
            }
            else
            {
                _groupChannel.InviteWithUserIds(users, HandleInviteUserHandler);
            }
        }

        void HandleInviteUserHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleInviteUserHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleInviteUserHandler");
            #endif
            inviteUserCallback();
        }

#endregion

#region Leave Current Channel

        private System.Action leaveChannelCallback; 
        public void LeaveCurrentChannel(System.Action callback)
        {
            leaveChannelCallback = callback;

            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.UserName + " left the group";

            string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);

            currentChannel.SendUserMessage("Group Left", _data, HandleSendUserMessageLeaveChannel);
        }

        void HandleSendUserMessageLeaveChannel (UserMessage message, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleSendUserMessageLeaveChannel");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            GroupChannel _groupChannel = currentChannel as GroupChannel;
            _groupChannel.Leave(HandleLeaveChannelHandler);
        }

        void HandleLeaveChannelHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleLeaveChannelHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            leaveChannelCallback();

            leaveChannelCallback = null;
        }
#endregion

#region Block and Unblock User

        private System.Action<bool> blockUserCallback;

        public void BlockUser (string userId, System.Action<bool> callback)
        {
            blockUserCallback = callback;

            SendBirdClient.BlockUserWithUserId(userId, HandleUserBlockHandler);
        }

        void HandleUserBlockHandler (User blockedUser, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleUserBlockHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                if (blockUserCallback != null)
                    blockUserCallback(false);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleUserBlockHandler " + JsonConvert.SerializeObject(blockedUser));
            #endif
            if (blockUserCallback != null)
                blockUserCallback(true);
        }

        private System.Action<bool> unblockUserCallback;
        public void UnblockUser (string userId, System.Action<bool> callback)
        {
            unblockUserCallback = callback;
            SendBirdClient.UnblockUserWithUserId (userId, HandleUserUnblockHandler);
        }

        void HandleUserUnblockHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleUserUnblockHandler");
                #endif
                if (unblockUserCallback != null)
                    unblockUserCallback(false);
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleUserBlockHandler");
            #endif
            if (unblockUserCallback != null)
                unblockUserCallback(true);
        }

        private System.Action<List<User>> blockedUserListCallback;

        public void GetBlockedUserList (System.Action<List<User>> callback)
        {
            if (Instance.isInternetConnected && IsSeandbirdLogined)
            {
                blockedUserListCallback = callback;
                
                UserListQuery blockedUserList = SendBirdClient.CreateBlockedUserListQuery();
                blockedUserList.Next(HandleBlockedUserListQuery);
            }
        }

        void HandleBlockedUserListQuery (List<User> queryResult, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleBlockedUserListQuery");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleBlockedUserListQuery " + JsonConvert.SerializeObject(queryResult));
            #endif
            if (blockedUserListCallback != null)
            {
                blockedUserListCallback(queryResult);
            }
        }

        void GetBlockedUserListCallback (List<User> userList)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log (JsonConvert.SerializeObject (userList));
            #endif

            FreakChatSerializeClass.BlockedUser blockUserList = new FreakChatSerializeClass.BlockedUser();
            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.BlockedUser))
            {
                blockUserList = FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList;
            }

            for (int i = 0; i < userList.Count; i++)
            {
                if (blockUserList.IsBlocked (userList[i].UserId) == false)
                {
                    blockUserList.AddUser (userList [i].UserId);
                }
            }

            FreakChatUtility.FreakChatPlayerPrefs.BlockedUserList = blockUserList;
        }

#endregion

#region Others
        public void MarkAsRead()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("MarkAsRead");
            #endif
            if (InternetConnection.Check () == false || ChatManager.Instance.currentChannel == null)
                return;

            GroupChannel _groupChannel = currentChannel as GroupChannel;
            _groupChannel.MarkAsRead();
        }

//        public void EndOneToOneMessaging ()
//        {
//            /*if (Instance.currentSelectedChatChannel == null)
//                return;
//
//            if (Instance.currentSelectedChatChannel.messagesList != null)
//                Instance.currentSelectedChatChannel.messagesList.Clear();
//
//            */
//            Instance.currentSelectedChatChannel = null;
//        }
#endregion

        /*void PlayNewMessageSound (string senderId)
        {
            if (Instance.IsUserMuted (senderId) == false)
            {
                if (mAudioSource == null)
                {
                    mAudioSource = UIController.Instance.GetAudioSource();
                    mAudioSource.PlayOneShot(UIController.Instance.assetsReferences.notificationSound);
                }
                else if (mAudioSource.isPlaying == false)
                {
                    mAudioSource.PlayOneShot(UIController.Instance.assetsReferences.notificationSound);
                }
            }
        }*/

        /*public bool IsUserMuted (string userId)
        {
            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.MutedUser))
            {
                if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(userId))
                {
                    return true;
                }
            }
            else
            {
                FreakChatSerializeClass.MuteUsers _muteUsers = new FreakChatSerializeClass.MuteUsers();
                FreakChatUtility.FreakChatPlayerPrefs.MuteUserList = _muteUsers;
            }

            return false;
        }*/

        /*public bool IsGroupMuted (string groupId)
        {
            if (FreakChatUtility.FreakChatPlayerPrefs.HasKey(FreakAppConstantPara.PlayerPrefsName.MutedGroup))
            {
                if (FreakChatUtility.FreakChatPlayerPrefs.MuteGroupList.IsMuted(groupId))
                {
                    return true;
                }
            }
            else
            {
                FreakChatSerializeClass.MuteUsers _muteUsers = new FreakChatSerializeClass.MuteUsers();
                FreakChatUtility.FreakChatPlayerPrefs.MuteGroupList = _muteUsers;
            }

            return false;
        }*/

        public void TypeStart()
        {
            if (Instance.isInternetConnected && IsSeandbirdLogined)
            {
                GroupChannel _groupChannel = currentChannel as GroupChannel;
                if (_groupChannel != null)
                    _groupChannel.StartTyping();
            }
        }

        public void TypeEnd()
        {
            if (Instance.isInternetConnected && IsSeandbirdLogined)
            {
                GroupChannel _groupChannel = currentChannel as GroupChannel;
                if (_groupChannel != null)
                    _groupChannel.EndTyping();
            }   
        }

        public ChatMessage GetChatCurrentMessage (long messageId)
        {
            for (int i = 0; i < Instance.currentSelectedChatChannel.messagesList.Count; i++)
            {
                if (Instance.currentSelectedChatChannel.messagesList[i].messageId == messageId)
                {
                    return Instance.currentSelectedChatChannel.messagesList[i];
                }
            }

            return null;
        }
#region COMMON

        public string CreateGroupFileName (string extension)
        {
            return Instance.UserId + "_" + FreakChatUtility.GetCurrentTimeStamp().ToString() + extension;
        }

        public static SendBird.User GetSenderId (List<SendBird.User> users)
        {
            if (users.Count == 2)
            {
                if (users[0].UserId == Instance.UserId)
                    return users[1];
                else if (users[1].UserId == Instance.UserId)
                    return users[0];
                else
                    return null;
            }

            return null;
        }

        /*public static Freak.Chat.Serializable.User GetCachedSenderId (List<Freak.Chat.Serializable.User> users)
        {
            if (users.Count == 2)
            {
                if (users[0].UserId == Instance.UserId)
                    return users[1];
                else if (users[1].UserId == Instance.UserId)
                    return users[0];
                else
                    return null;
            }
            
            return null;
        }*/

        public static ChatUser GetSender (List<ChatUser> channelMembers)
        {
            if (channelMembers == null)
                return null;
            
            if (channelMembers.Count == 2)
            {
                if (channelMembers[0].userId == Instance.UserId)
                    return channelMembers[1];
                else if (channelMembers[1].userId == Instance.UserId)
                    return channelMembers[0];
                else
                    return null;
            }
            
            return null;
        }
        
        private static string[] ValidateSingleChannel = { "Single_Channel", "Messaging Channel", "ASDAB123" };
        public static bool IsSingleChannel (string ext)
        {
            for (int i = 0; i < ValidateSingleChannel.Length; i++)
            {
                if (ValidateSingleChannel[i].Contains(ext))
                    return true;
            }
            
            return false;
        }

        private static AlertMessege mAlertMesage;
        public static bool CheckInternetConnection ()
        {
            if (Instance.isInternetConnected == false)
            {
                if (mAlertMesage == null)
                {
                    mAlertMesage = FindObjectOfType<AlertMessege> ();
                }

                if (mAlertMesage != null)
                {
                    mAlertMesage.ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Check your Internet Connection.");
                }
            }

            return InternetConnection.Check ();
        }

        public bool isInternetConnected;
        void InternetCallback (bool isConnected)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError ("InternetCallback " + isConnected);
            #endif
            if (isInternetConnected != isConnected)
            {
                if (isConnected)
                {
                    if (isReconnectStarted == false)
                    {
                        if (FreakAppManager.Instance.GetHomeScreenPanel () != null &&
                            FreakAppManager.Instance.GetHomeScreenPanel ().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
                        {
                            RefreshForNewMessages ();
                        }
                    }
                }
                else
                {
                    isSeandbirdLogined = false;
                    MakeAllOffline();
                }

                isInternetConnected = isConnected;
            }
        }

#endregion

        #region Open Chat

        public void OpenChatChannelFromNotification ()
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == FreakAppConstantPara.UnitySceneName.FreakHomeScene)
            {
                notificationChannel = PlayerPrefs.GetString(FreakAppConstantPara.PlayerPrefsName.OpenNotificationChatChannel);
                
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OpenNotificationChatChannel----->>> " + isInternetConnected);
                #endif
                
                if (isInternetConnected)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("OpenNotificationChatChannel----->>>> " + IsSeandbirdLogined);
                    #endif
                    
                    UIController.Instance.connectingUI.DisplayLoading();
                    
                    if (IsSeandbirdLogined)
                    {
                        ResetRefreshCoroutine(1f);
                        LoadMessageCallbackHandler = LoadUnReadMessage;
                    }
                    else
                    {
                        Invoke("OpenChatChannelFromNotification", 0.5f);
                    }
                }
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(FreakAppConstantPara.UnitySceneName.FreakHomeScene);
            }
        }

        private System.Action LoadMessageCallbackHandler;
        private string notificationChannel;
        void LoadUnReadMessage ()
        {
            ChatChannel _chatChannel = CacheManager.GetChatMessage(notificationChannel);

            if (_chatChannel != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OpenNotificationChatChannel----->>>> " + _chatChannel.channelName);
                #endif
                UIController.Instance.connectingUI.HideLoading();
                
                PlayerPrefs.DeleteKey(FreakAppConstantPara.PlayerPrefsName.OpenNotificationChatChannel);

                FreakAppManager.Instance.senderCompressedPic = null;
                GameObject go = null;
                if (FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
                {
                    go = FreakAppManager.Instance.GetHomeScreenPanel().GetReplacedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
                }
                else
                {
                    go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
                }

                go.GetComponent<ChatWindowUI>().InitChat(_chatChannel, true);
                ChatManager.Instance.GetChannelByChannelUrl(notificationChannel, go.GetComponent<ChatWindowUI>().GetGroupChannelCallback);
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OpenNotificationChatChannel-----==== No Channel");
                #endif
                
                PlayerPrefs.DeleteKey(FreakAppConstantPara.PlayerPrefsName.OpenNotificationChatChannel);
                ChatManager.Instance.GetChannelByChannelUrl(notificationChannel, CallbcakGetChannelByChannelUrlHandler);
            }
        }

        void CallbcakGetChannelByChannelUrlHandler (GroupChannel groupChannel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OpenNotificationChatChannel-----== " + groupChannel.Url);
            #endif
            UIController.Instance.connectingUI.HideLoading();

            FreakAppManager.Instance.senderCompressedPic = null;
            GameObject go = null;
            if (FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
            {
                go = FreakAppManager.Instance.GetHomeScreenPanel().GetReplacedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
            }
            else
            {
                go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
            }

            go.GetComponent<ChatWindowUI>().InitChat(CacheManager.UpdateChannel(groupChannel), true);
            go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(groupChannel);
        }

        #endregion

#region Send File Direct

        public ChatFileType directSendMessageFileType;
        private ChatFileType.FileType sendFileType;
        public void DirectFileSendPathHandler (string path)
        {
            string filepath = path;

            if (path.Contains("file://"))
            {
                filepath = path.Replace("file://", string.Empty);
                filepath = WWW.UnEscapeURL (filepath);
            }

            if (System.IO.File.Exists(filepath))
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("File path --------------->>>>>>>>>>>>>"+filepath);
                #endif

                string _extension = System.IO.Path.GetExtension(filepath);
                if (FreakChatUtility.IsImageExtension(_extension))
                {
                    sendFileType = ChatFileType.FileType.Image;
                }
                else if (FreakChatUtility.IsAudioExtension(_extension))
                {
                    sendFileType = ChatFileType.FileType.Audio;
                }
                else if (FreakChatUtility.IsVideoExtension(_extension))
                {
                    sendFileType = ChatFileType.FileType.Video;
                }
                else
                {
                    sendFileType = ChatFileType.FileType.Others;
                }

                SaveFileMessage(filepath, sendFileType);
            }
            else
            {
                Debug.LogError("File Doesn't exists " + filepath);
            }
        }

        private string newSavedFilePath;
        void SaveFileMessage (string filePath, ChatFileType.FileType fileType)
        {
            string _filename = FreakChatUtility.FileNameFromUrl(FreakChatUtility.GetCurrentTimeStamp(), filePath);
            var bytes = System.IO.File.ReadAllBytes(filePath);

            newSavedFilePath = Freak.StreamManager.LoadFilePath(_filename, ChatFileType.GetFolderLocation(fileType), true);

            Freak.StreamManager.SaveFile(_filename, bytes, ChatFileType.GetFolderLocation(fileType), SendFileSavedCallback, true);
        }

        void SendFileSavedCallback (bool success)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("FileSavedCallback " + success);
            #endif
            directSendMessageFileType = new ChatFileType(newSavedFilePath, sendFileType);
        }
#endregion

#region SendBird ERROR Code

        public void SendBirdExceptionHandler (int errorCode, string message = "")
        {
            Debug.LogError ("SendBirdExceprion:: " + errorCode + " :Message: " + message);
            
            switch (errorCode)
            {
                case 400401:    //Invalid API-Token
                    {
                        InitSendBird ();
                        SendBirdClient.Connect(Instance.UserId, (user, e) => {
                            if (e != null)
                            {
                                #if UNITY_EDITOR && UNITY_DEBUG
                                Debug.Log (e.Code + ": " + e.Message);
                                #endif
                                return;
                            }
                            
                            LoadMessage (loadMessageCallback, mTimeStamp, mUnreadMessage);
                        });

                        InvokeForNetCheck();
                        
                    }
                break;
                    
                case 900080:
                    {
                        FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    }
                break;

                case 800220:
                    {
                        InvokeForNetCheck ();
                    }
                break;
                    
                case 800101:
                    {
                        //800101: Connection must be made before you send message.
                        //TODO: if there is no sendbird connection.....
                        InvokeForNetCheck ();
                    }
                break;

                case 400108:
                case 900020:
                    {
                        //Not authorized. User must be a member of this channel.
                        if(FreakAppManager.Instance.GetHomeScreenPanel ().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
                        {
                            FindObjectOfType<AlertMessege> ().onClickOkButtonCallback = CloseChatWindow;
                            FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "You are removed from this Group. You cannot send message in this group.");
                        }
                    }
                break;
            }

            if (UIController.Instance.connectingUI.gameObject.activeInHierarchy)
            {
                UIController.Instance.connectingUI.HideLoading();
            }
        }

        void CloseChatWindow ()
        {
            FreakAppManager.Instance.GetHomeScreenPanel ().PopPanel ();
        }

        private int invokeCalledCount;
        void InvokeForNetCheck ()
        {
            invokeCalledCount++;
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("InvokeForNetCheck");
            #endif
            if (Instance.isInternetConnected == false)
            {
                if (invokeCalledCount < 1000)
                {
                    Invoke ("InvokeForNetCheck", 1f);
                }
            }
            else
            {
                invokeCalledCount = 0;

                if (IsInitLogin)
                    return;
                
                Instance.InitSendBird ();
                Instance.LoginToSendbird ();

                ChannelHandler ();
                ConnectionHandler ();
            }
        }

#endregion

        #region Message Selected

        public void OnMessageSelected (ChatMessage message)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("OnMessageSelected " + message.messageId);
            #endif
            Instance.selectedMessages.Add (message);
        }

        public void OnMessageDeselected (ChatMessage message)
        {
            for (int i = 0; i < Instance.selectedMessages.Count; i++)
            {
                if (Instance.selectedMessages[i].messageId == message.messageId)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("OnMessageDeselected " + message.messageId);
                    #endif
                    Instance.selectedMessages.RemoveAt (i);
                    break;
                }
            }
        }

        #endregion

        public void SaveprofileByteImage (string channelUrl, Texture2D textureImage)
        {
            for (int i = 0; i < Chat.ChatManager.Instance.cachedData.allChannelList.Count; i++)
            {
                if (Chat.ChatManager.Instance.cachedData.allChannelList[i].channelUrl == channelUrl)
                {
                    Chat.ChatManager.Instance.cachedData.allChannelList[i].textureCache = textureImage;
                    break;
                }
            }
        }

        public void MakeAllOffline ()
        {
            for (int i = 0; i < ChatManager.Instance.cachedData.allChannelList.Count; i++)
            {
                if (ChatManager.Instance.cachedData.allChannelList[i].GetSender() != null && 
                    ChatManager.Instance.cachedData.allChannelList[i].isGroupChannel == false)
                {
                    ChatManager.Instance.cachedData.allChannelList [i].GetSender ().connectionStatus = ChatUser.UserConnectionStatus.Offline;
                }
            }

            if (newMessageUpdateCallback != null)
            {
                newMessageUpdateCallback ();
            }
        }
    }
}
