﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SendBird;

namespace Freak.Chat
{
    public partial class ChatManager
    {
        public System.Action<SendBird.BaseMessage> OnMessageSentCallback;
//        public System.Action<MessageSendQueue> OnUserMessageSendFail;
//        public System.Action<FileSendQueue> OnFileMessageSendFail;

        #region Load Message
        private List<BaseMessage> currentMessageList;
        private MessageListQuery messageListQuery;
        private IEnumerator messageQueryRoutine;
        private long mTimeStamp;
        private int mUnreadMessage;
        public void LoadMessage (System.Action callback, long timestamp, int unreadmessage)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("LoadMessage " + FreakChatUtility.GetTime (timestamp) + " ::: " + unreadmessage);
            #endif
            mTimeStamp = timestamp;
            mUnreadMessage = unreadmessage;

            if (callback != null)
                loadMessageCallback = callback;

            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                messageListQuery = currentChannel.CreateMessageListQuery ();
//                messageListQuery.Prev (timestamp, unreadmessage, false, HandleMessageListQueryResult);
                messageListQuery.Load (timestamp, unreadmessage, unreadmessage, false, HandleMessageListQueryResult);
            }
        }

        public void LoadPrevMessage (long timestamp, int unreadmessage)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("LoadMessage " + FreakChatUtility.GetTime (timestamp) + " ::: " + unreadmessage);
            #endif
            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                messageListQuery = currentChannel.CreateMessageListQuery ();
                messageListQuery.Load(timestamp, unreadmessage, 0, false, HandleMessageListQueryResult);
            }
        }

        void HandleMessageListQueryResult (List<BaseMessage> queryResult, SendBird.SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleMessageListQueryResult error");
                #endif
//                StopCoroutine(messageQueryRoutine);
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleMessageListQueryResult " + JsonConvert.SerializeObject (queryResult));
            #endif

            if (currentMessageList == null)
            {
                currentMessageList = new List<BaseMessage> ();
            }
            currentMessageList = queryResult;

            bool isNewMessage = ChatChannelHandler.AddMessage(Instance.currentSelectedChatChannel, queryResult);

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleMessageListQueryResult " + isNewMessage);
            #endif

            if (isNewMessage)
            {
                if (queryResult.Count > 0)
                    CacheManager.SaveNewMessage (Instance.currentSelectedChatChannel);
            }

            CheckForReceipt ();

            if (isNewMessage && loadMessageCallback != null)
                loadMessageCallback();
        }

        void CheckForReceipt ()
        {
            if (currentMessageList.Count > 0)
            {
                for (int i = 0; i < currentMessageList.Count; i++)
                {
                    GroupChannel _groupChannel = currentChannel as GroupChannel;
                    int _readStatus = _groupChannel.GetReadReceipt (currentMessageList [i]);
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log (currentMessageList [i].MessageId + " :: " + _readStatus);
                    #endif
                    if (_readStatus == 0)
                    {
                        ChangeReadReceipt (currentMessageList [i].MessageId);
                    }
                    else if (IsSingleChannel (_groupChannel.Name) == false)
                    {
                        if (_readStatus < (_groupChannel.MemberCount - 1))
                        {
                            ChangeReadReceipt (currentMessageList [i].MessageId);
                        }
                    }
                }
                
                CacheManager.SaveNewMessage (Instance.currentSelectedChatChannel);
            }
        }

        void ChangeReadReceipt (long messageId)
        {
            int count = Instance.currentSelectedChatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (Instance.currentSelectedChatChannel.messagesList[i].messageId == messageId)
                {
                    Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
                    Instance.currentSelectedChatChannel.messagesList [i].receiptStatus = ChatMessage.MessageReceiptType.Read;
                    ChatChannelHandler.MarkLastMessageAsRead(Instance.currentSelectedChatChannel, Instance.currentSelectedChatChannel.messagesList[i].messageId);
                }
            }
        }
        #endregion

        #region Load Previous Message

        private System.Action loadPreviousMsgCallback;
        public void LoadPreviousMessages (long createdAt, System.Action callback)
        {
            loadPreviousMsgCallback = callback;

            MessageListQuery msgListQuery = currentChannel.CreateMessageListQuery();
            msgListQuery.Prev(createdAt, 10, false, HandlePreviousMessageList);
        }

        void HandlePreviousMessageList (List<BaseMessage> queryResult, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandlePreviousMessageList error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandlePreviousMessageList " + JsonConvert.SerializeObject(queryResult));
            #endif
            #if SB_NON_SER
            bool added = currentSelectedChatChannel.AddMessage(queryResult);
            #else
            bool added = ChatChannelHandler.AddMessage(Instance.currentSelectedChatChannel, queryResult);
            #endif
            loadPreviousMsgCallback();
        }

        #endregion //Load Previous Message

        #region Send Message

        /*public Action messageSendCallback;
        public Action fileSendCallback;*/

        public class AllMessageQueue
        {
            public SendBird.BaseChannel baseChannel;
            public string data;
            public string customType;
        }

        public class MessageSendQueue : AllMessageQueue
        {
            public string msg;
            public Action callback;
        }

        public class FileSendQueue : AllMessageQueue
        {
            public string fileUrl;
            public string name;
            public string type;
            public int size;
            public ChatFileType fileInfo;
        }

//        private List<FileSendQueue> mFileWithUrlSendingQueueList = new List<FileSendQueue> ();
        private List<AllMessageQueue> mAllMessageQueue = new List<AllMessageQueue> ();
//        private List<FileSendQueue> mFileSendingQueueList = new List<FileSendQueue> ();
//        private List<MessageSendQueue> mMessageSendingQueueList = new List<MessageSendQueue> ();
        private bool isFileWithUrlSending = false;
        private bool isFileSending = false;
        private bool isMessageSending = false;

        // Text message with metadata...
        public void SendUserMessage(BaseChannel baseChannel, string msg, string data = "", string customType = "", Action callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendUserMessage " + msg);
            #endif

            if (CheckForPreviousUserMsgQueue(customType) == false)
            {
                MessageSendQueue _MessageSendQueue = new MessageSendQueue ();
                _MessageSendQueue.msg = msg;
                _MessageSendQueue.data = data;
                _MessageSendQueue.customType = customType;
                _MessageSendQueue.callback = callback;
                _MessageSendQueue.baseChannel = baseChannel;
                
//                mMessageSendingQueueList.Add (_MessageSendQueue);
                mAllMessageQueue.Add (_MessageSendQueue);
            }

            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined || baseChannel != null)
            {
                if (isMessageSending)
                {
                    return;
                }

                if (callback != null) {
                    loadMessageCallback = callback;
                }

                isMessageSending = true;
                try
                {
                    baseChannel.SendUserMessage (msg, data, customType, HandleSendUserMessageHandler);
                }
                catch(Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    isMessageSending = false;
                    SendUserMessageFailed();
                }
            }
            else
            {
                SendUserMessageFailed();
            }
        }

        bool CheckForPreviousUserMsgQueue (string customType)
        {
            for (int i = 0; i < mAllMessageQueue.Count; i++)
            {
                if (mAllMessageQueue[i].customType == customType)
                {
                    return true;
                }
            }

            return false;
        }

        void HandleSendUserMessageHandler (UserMessage message, SendBirdException e)
        {
            isMessageSending = false;

            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("HandleSendUserMessageHandler error");
                #endif

                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    return;
                }
                SendBirdExceptionHandler (e.Code, e.Message);
                SendUserMessageFailed();
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendUserMessageHandler " + JsonConvert.SerializeObject(message));
            #endif

            TriggerCallback(message);
//            mMessageSendingQueueList.RemoveAt (0);
            RemoveTopMsg<MessageSendQueue> ();
            SendQueuedUserMessage ();
        }

        AllMessageQueue GetTopUserMsg<T> ()
        {
            for (int i = 0; i < mAllMessageQueue.Count; i++)
            {
                if (mAllMessageQueue[i] is T)
                {
                    return mAllMessageQueue[i];
                }
            }

            return null;
        }

        void RemoveTopMsg<T> ()
        {
            for (int i = 0; i < mAllMessageQueue.Count; i++)
            {
                if (mAllMessageQueue[i] is T)
                {
                    mAllMessageQueue.RemoveAt (i);
                    break;
                }
            }
        }

        /*void RemoveTopFileMsg ()
        {
            for (int i = 0; i < mAllMessageQueue.Count; i++)
            {
                if (mAllMessageQueue[i] is FileSendQueue)
                {
                    mAllMessageQueue.RemoveAt (i);
                    break;
                }
            }
        }*/

        void SendUserMessageFailed ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendUserMessageFailed");
            #endif

            if (sendQueuedUserMessageCoroutine == null)
            {
                sendQueuedUserMessageCoroutine = SendQueuedUserMessage ();
                StartCoroutine (sendQueuedUserMessageCoroutine);
            }
        }

        IEnumerator sendQueuedUserMessageCoroutine = null;
        WaitForSeconds mWaitForSecondsUserMessageShort = new WaitForSeconds(2f);
        WaitForSeconds mWaitForSecondsUserMessageLong = new WaitForSeconds(8f);
        IEnumerator SendQueuedUserMessage ()
        {

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendQueuedUserMessage");
            #endif

            if (ChatManager.Instance.isInternetConnected == false)
            {
                yield return mWaitForSecondsUserMessageLong;
            }
            else
            {
                yield return mWaitForSecondsUserMessageShort;
            }

            sendQueuedUserMessageCoroutine = null;
            if (mAllMessageQueue.Count > 0)
            {
                AllMessageQueue _getMsg = GetTopUserMsg<MessageSendQueue> ();
                MessageSendQueue _allMsg = _getMsg as MessageSendQueue;

                if (_allMsg != null)
                {
                    SendUserMessage (_allMsg.baseChannel, _allMsg.msg, _allMsg.data, 
                        _allMsg.customType, _allMsg.callback);
                }
            }
        }

        public void SendFileMessage (BaseChannel baseChannel, ChatFileType fileInfo, string customType, string data = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessage " + fileInfo.fileName);
            #endif

//            if (CheckForPreviousFileMsgQueue(customType) == false)
            if (CheckForPreviousUserMsgQueue(customType) == false)
            {
                FileSendQueue _mFileSendQueue = new FileSendQueue ();
                _mFileSendQueue.fileInfo = fileInfo;
                _mFileSendQueue.data = data;
                _mFileSendQueue.customType = customType;
                _mFileSendQueue.baseChannel = baseChannel;
                
//                mFileSendingQueueList.Add (_mFileSendQueue);
                mAllMessageQueue.Add (_mFileSendQueue);
            }

            if (ChatManager.Instance.isInternetConnected || baseChannel != null)
            {
                if (isFileSending)
                {
                    return;
                }

                try
                {
                    SBFile _SendBirdFile = new SBFile(fileInfo.filePath);
                    
                    isFileSending = true;
                    baseChannel.SendFileMessage(_SendBirdFile, fileInfo.fileName,
                        ChatFileType.ConvertChatFileTypeToString(fileInfo.fileType), fileInfo.fileSize, data, customType, HandleSendFileMessageHandler);
                }
                catch (Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    isFileSending = false;
                    SendFileMessageFailed();
                }
            }
            else
            {
                SendFileMessageFailed();
            }
        }

        /*bool CheckForPreviousFileMsgQueue (string customType)
        {
            for (int i = 0; i < mFileSendingQueueList.Count; i++)
            {
                if (mFileSendingQueueList[i].customType == customType)
                {
                    return true;
                }
            }

            return false;
        }*/

        public void SendFileMessageWithUrl (BaseChannel baseChannel, string fileUrl, string name, string type, int size, string data, string customType)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessageWithUrl " + fileUrl);
            #endif

//            if (CheckForPreviousFileMsgQueue(customType) == false)
            if (CheckForPreviousUserMsgQueue(customType) == false)
            {
                FileSendQueue _mFileSendQueue = new FileSendQueue ();
                _mFileSendQueue.data = data;
                _mFileSendQueue.fileUrl = fileUrl;
                _mFileSendQueue.name = name;
                _mFileSendQueue.size = size;
                _mFileSendQueue.type = type;
                _mFileSendQueue.customType = customType;
                _mFileSendQueue.baseChannel = baseChannel;

//                mFileSendingQueueList.Add (_mFileSendQueue);
                mAllMessageQueue.Add (_mFileSendQueue);
            }
            
            if (ChatManager.Instance.isInternetConnected || baseChannel != null)
            {
                if (isFileWithUrlSending)
                {
                    return;
                }

                try
                {
                    isFileWithUrlSending = true;
                    baseChannel.SendFileMessageWithURL (fileUrl, name, type, size, data, customType, HandleSendFileMessageHandler);
                }
                catch (Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    isFileWithUrlSending = false;
                    SendFileMessageFailed();
                }
            }
            else
            {
                SendFileMessageFailed();
            }
        }

        void HandleSendFileMessageHandler (FileMessage message, SendBirdException e)
        {
            isFilePickerBackground = false;
            isFileWithUrlSending = true;
            isFileSending = false;

            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleSendFileMessageHandler error");
                #endif
                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    return;
                }
                SendBirdExceptionHandler (e.Code, e.Message);
                SendFileMessageFailed();
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendFileMessageHandler " + JsonConvert.SerializeObject(message));
            #endif

            TriggerCallback(message);
//            mFileSendingQueueList.RemoveAt (0);
            RemoveTopMsg<FileSendQueue> ();
            SendQueuedFileMessage();
        }

        void SendFileMessageFailed ()
        {
//            OnFileMessageSendFail(mFileSendingQueueList[0]);

            if(sendQueuedFileMessageCoroutine == null)
            {
                sendQueuedFileMessageCoroutine = SendQueuedFileMessage ();
                StartCoroutine (sendQueuedFileMessageCoroutine);
            }
        }

        IEnumerator sendQueuedFileMessageCoroutine;
        WaitForSeconds mWaitForSecondsFileMessageShort = new WaitForSeconds(2f);
        WaitForSeconds mWaitForSecondsFileMessageLong = new WaitForSeconds(8f);
        IEnumerator SendQueuedFileMessage ()
        {
            if (ChatManager.Instance.isInternetConnected)
            {
                yield return mWaitForSecondsFileMessageShort;
            }
            else
            {
                yield return mWaitForSecondsFileMessageLong;
            }

            sendQueuedFileMessageCoroutine = null;
            if (mAllMessageQueue.Count > 0)
//            if (mFileSendingQueueList.Count > 0)
            {
                AllMessageQueue _getMsg = GetTopUserMsg<MessageSendQueue> ();
                FileSendQueue _fileQueue = _getMsg as FileSendQueue;

                if (_fileQueue.fileInfo == null)
                {
                    SendFileMessageWithUrl (_fileQueue.baseChannel, _fileQueue.fileUrl, _fileQueue.name, 
                        _fileQueue.type, _fileQueue.size, _fileQueue.data, _fileQueue.customType);
                }
                else
                {
                    SendFileMessage (_fileQueue.baseChannel, _fileQueue.fileInfo, _fileQueue.customType, _fileQueue.data);
                }
            }
        }

        #endregion  //Send Message

        #region Callback

        void TriggerCallback (SendBird.BaseMessage message)
        {
            ChatChannelHandler.OnMessageReceived(Instance.currentSelectedChatChannel, message);

            if (OnMessageSentCallback != null)
            {
                OnMessageSentCallback (message);
            }

            if (loadMessageCallback != null)
                loadMessageCallback();
        }

        #endregion

        #region Delete Message
        private System.Action deleteMessageCallback;
        public void DeleteMessage (BaseMessage baseMessage, System.Action callback)
        {
            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                deleteMessageCallback = callback;
                currentChannel.DeleteMessage(baseMessage, HandleDeleteMessageHandler);
            }
        }

        void HandleDeleteMessageHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleDeleteMessageHandler error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            if (deleteMessageCallback != null)
                deleteMessageCallback();

            deleteMessageCallback = null;
        }

        #endregion


        public void SendUserMessageNew (BaseChannel baseChannel, string msg, string data = "", string customType = "", Action callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendUserMessage " + msg);
            #endif

            if (ChatManager.Instance.isInternetConnected && baseChannel != null)
            {
                try
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("MESSAGE "+msg);
                    #endif
                    baseChannel.SendUserMessage (msg, data, customType, HandleSendUserMessageHandlerNew);
                }
                catch(Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    MessageQueueHandler.Instance.OnMessagesendCallback(false);
                }
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("@@@@SendUserMessage " + ChatManager.Instance.isInternetConnected);
                #endif
                if (baseChannel == null)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("@@@@SendUserMessage IS NULL");
                    #endif
                }
                else
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("@@@@SendUserMessage " + baseChannel.Url);
                    #endif
                }
                MessageQueueHandler.Instance.OnMessagesendCallback(false);
            }
        }

        void HandleSendUserMessageHandlerNew (UserMessage message, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("HandleSendUserMessageHandler " + message.CustomType);
                #endif

                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("HandleSendUserMessageHandler error");
                #endif

                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    MessageQueueHandler.Instance.OnMessagesendCallbackStopAll();
                    return;
                }
                SendBirdExceptionHandler (e.Code, e.Message);
                MessageQueueHandler.Instance.OnMessagesendCallback(false);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendUserMessageHandler " + JsonConvert.SerializeObject(message));
            #endif

            MessageQueueHandler.Instance.OnMessagesendCallback(true);
            TriggerCallback(message);
        }

        public void SendFileMessageNew (BaseChannel baseChannel, ChatFileType fileInfo, string customType, string data = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessage " + fileInfo.fileName);
            #endif

            if (ChatManager.Instance.isInternetConnected && baseChannel != null)
            {
                try
                {
                    SBFile _SendBirdFile = new SBFile(fileInfo.filePath);

                    isFileSending = true;
                    baseChannel.SendFileMessage(_SendBirdFile, fileInfo.fileName,
                        ChatFileType.ConvertChatFileTypeToString(fileInfo.fileType), fileInfo.fileSize, data, customType, HandleSendFileMessageHandlerNew);
                }
                catch (Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    MessageQueueHandler.Instance.OnMessagesendCallback(false);
                }
            }
            else
            {
                MessageQueueHandler.Instance.OnMessagesendCallback(false);
            }
        }

        public void SendFileMessageWithUrlNew (BaseChannel baseChannel, string fileUrl, string name, string type, int size, string data, string customType)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessageWithUrl " + fileUrl);
            #endif

            if (ChatManager.Instance.isInternetConnected && baseChannel != null)
            {
                try
                {
                    isFileWithUrlSending = true;
                    baseChannel.SendFileMessageWithURL (fileUrl, name, type, size, data, customType, HandleSendFileMessageHandlerNew);
                }
                catch (Exception e)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogError("error "+e.Message);
                    #endif
                    MessageQueueHandler.Instance.OnMessagesendCallback(false);
                }
            }
            else
            {
                MessageQueueHandler.Instance.OnMessagesendCallback(false);
            }
        }

        void HandleSendFileMessageHandlerNew (FileMessage message, SendBirdException e)
        {
            isFilePickerBackground = false;

            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleSendFileMessageHandler error");
                #endif
                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    return;
                }
                SendBirdExceptionHandler (e.Code, e.Message);
                MessageQueueHandler.Instance.OnMessagesendCallback(false);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendFileMessageHandler " + JsonConvert.SerializeObject(message));
            #endif

            TriggerCallback(message);
            MessageQueueHandler.Instance.OnMessagesendCallback(true);
        }
    }
}
