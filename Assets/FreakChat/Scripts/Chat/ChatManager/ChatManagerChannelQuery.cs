﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SendBird;

namespace Freak.Chat
{
    public partial class ChatManager
    {
        public CacheSeralizeClass cachedData;
        public System.Action newMessageUpdateCallback;
        public List<GroupChannel> userAllChannelList = new List<GroupChannel>();
        public List<GroupChannel> groupChannelList = new List<GroupChannel> ();

        #region Channel List Query
        public GroupChannelListQuery mGroupChannelListQuery;
        private List<GroupChannel> mGroupChannelList;

        #if CACHE_TEST2
        public List<Serializable.GroupChannel> allChannelList = new List<Freak.Chat.Serializable.GroupChannel> ();
        #endif

        void ChannelListQuery ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("ChannelListQuery "+isSeandbirdLogined);
            #endif

            if (isSeandbirdLogined == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("isSeandbirdLogined " + isSeandbirdLogined);
                #endif
                return;
            }

            if (mGroupChannelListQuery != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("mGroupChannelListQuery is not NULL");
                #endif
                if (mGroupChannelListQuery.IsLoading () == true)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("mGroupChannelListQuery IsLoading " + mGroupChannelListQuery.IsLoading());
                    #endif
                    return;
                }
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("ChannelListQuery");
            #endif

            mGroupChannelListQuery = GroupChannel.CreateMyGroupChannelListQuery();
            List<string> userid = new List<string> ();
            userid.Add (ChatManager.Instance.UserId);

            if (mGroupChannelListQuery == null)
            {
                LoginToSendbird ();
                return;
            }

            mGroupChannelListQuery.SetUserIdsIncludeFilter (userid, GroupChannelListQuery.QueryType.AND);
            mGroupChannelListQuery.Limit = 50;
            mGroupChannelListQuery.Next(HandleGroupChannelListQueryResultHandler);
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("ChannelListQuery "+mGroupChannelListQuery.IsLoading());
            #endif
        }

        void HandleGroupChannelListQueryResultHandler (List<GroupChannel> queryResult, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleGroupChannelListQueryResultHandler Error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("queryResult Count " + queryResult.Count);
            #endif
            mGroupChannelList = new List<GroupChannel>(queryResult);
            GetBlockedUserList (GetBlockedUserListCallback);
            UpdateChannelQueryListNew ();

            if (FreakAppManager.Instance.GetHomeScreenPanel() != null &&
                FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                SendBird.UserMessage msg = currentSelectedChatChannel.groupChannel.LastMessage as UserMessage;
                Debug.Log("USERMESSAGE");
                #endif

                if (mChatWindowUI == null)
                {
                    mChatWindowUI = FreakAppManager.Instance.GetHomeScreenPanel().GetCurrentObject.GetComponent<ChatWindowUI>();
                }
                mChatWindowUI.RefreshChannel();

                LoadPrevMessage(currentSelectedChatChannel.lastMessage.CreatedAt, currentSelectedChatChannel.unreadMessageCount);
            }

            if (LoadMessageCallbackHandler != null)
            {
                LoadMessageCallbackHandler();
                LoadMessageCallbackHandler = null;
            }
        }

        void UpdateChannelQueryListNew ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("UpdateChannelQueryListNew");
            #endif

            CacheManager.UpdateChannelList (mGroupChannelList);
            CheckForLastMessageReceipt();
            Instance.newTotalMessages = Instance.newTotalChats = 0;
            UnreadMessageCountNew ();
            CacheManager.OnlyGroupChannel ();

            if(newMessageUpdateCallback != null)
            {
                newMessageUpdateCallback();
            }

            if (refreshDoneCallback != null)
            {
                refreshDoneCallback ();
                refreshDoneCallback = null;
            }

            CacheManager.SaveChannelData ();
        }

        void CheckForLastMessageReceipt ()
        {
            if (isInternetConnected && IsSeandbirdLogined)
            {
                for (int i = 0; i < mGroupChannelList.Count; i++)
                {
                    if (mGroupChannelList[i].LastMessage != null)
                    {
                        int _readStatus = mGroupChannelList[i].GetReadReceipt(mGroupChannelList[i].LastMessage);

                        if (_readStatus == 0)
                        {
//                            Debug.Log("LAST MESSAGE " + mGroupChannelList[i].LastMessage.MessageId);
                            CacheManager.ChangeLastMessageReadReceipt(mGroupChannelList[i].Url, mGroupChannelList[i].LastMessage.MessageId);
                        }
                        else if (IsSingleChannel(mGroupChannelList[i].Name) == false)
                        {
                            if (_readStatus < (mGroupChannelList[i].MemberCount - 1))
                            {
                                CacheManager.ChangeLastMessageReadReceipt(mGroupChannelList[i].Url, mGroupChannelList[i].LastMessage.MessageId);
                            }
                        }
                    }
                }
            }
        }

        public void NewMessageReceivedQueryNew (BaseChannel channel, BaseMessage message)
        {
            CacheManager.SaveNewMessage (channel, message);
            UnreadMessageCountNew ();
//            UpdateReceivedMetaDataCounter (channel, message);
            CacheManager.OnlyGroupChannel ();

            if(newMessageUpdateCallback != null)
            {
                newMessageUpdateCallback();
            }
        }

        /*private BaseChannel tempChannel;
        private BaseMessage tempMessage;
        void UpdateReceivedMetaDataCounter (BaseChannel channel, BaseMessage message)
        {
            tempChannel = channel;
            tempMessage = message;
            Instance.GetMetaDataCounter (tempChannel, MetaDataCounterCallback);
        }*/

        /*void MetaDataCounterCallback (Dictionary<string, int> metaData)
        {
            Debug.Log ("MetaDataCounterCallback " + MiniJSON.Json.Serialize (metaData));

            long _MessageId = tempMessage.MessageId;
            if (tempMessage is SendBird.UserMessage)
            {
                SendBird.UserMessage _userMessage = tempMessage as SendBird.UserMessage;
                _MessageId = _userMessage.MessageId;
            }
            else if (tempMessage is SendBird.FileMessage)
            {
                SendBird.FileMessage _fileMessage = tempMessage as SendBird.FileMessage;
                _MessageId = _fileMessage.MessageId;
            }

            if (metaData.ContainsKey(_MessageId.ToString()))
            {
                metaData [_MessageId.ToString()] = (int)ChatMessage.MessageReceiptType.Delivered;
            }
            else
            {
                metaData.Add (_MessageId.ToString(), (int)ChatMessage.MessageReceiptType.Delivered);
            }

            Instance.UpdateMetaDataCounter (tempChannel, metaData, MetaDataCounterUpdateCallback);
        }*/

        /* void MetaDataCounterUpdateCallback (Dictionary<string, int> metaData)
        {
            Debug.Log ("MetaDataCounterUpdateCallback");
            CacheManager.UpdateMetaDataCounter (tempChannel.Url, metaData);
        }*/

        void UnreadMessageCountNew ()
        {
            int _newTotalMessages = 0;
            int _newTotalChats = 0;

            for (int i = 0; i < cachedData.allChannelList.Count; i++)
            {
                //Message and Chats Count
                if (cachedData.allChannelList[i].unreadMessageCount > 0)
                {
                    _newTotalMessages += cachedData.allChannelList[i].unreadMessageCount;
                    _newTotalChats++;
                }
            }

            newTotalMessages = _newTotalMessages;
            newTotalChats = _newTotalChats;

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("newTotalMessages " + newTotalMessages);
            Debug.Log("newTotalChats " + newTotalChats);
            #endif
        }

//        private bool cannnotDoSorting = true;
        #if CACHE_TEST2
        void UpdateChannelQueryList()
        {
//            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.LogError("UpdateChannelQueryList " + (Time.time - testTimeTaken));
//            #endif

            // FIRST CHECK FOR PREVIOUS CHANNEL IF ANY CHANNEL IS DELETED.....
            /*if (userAllChannelList.Count > 0)
            {
                CheckForDeletedChannel ();
            }*/

            userAllChannelList = new List<GroupChannel> (mGroupChannelList);
//            #if CACHE_TEST2
//            if (allChannelList != null)
//            {
//            allChannelList.Clear ();
//            }
//            Debug.Log("DONE:: " + JsonConvert.SerializeObject (mGroupChannelList));
//            allChannelList = CacheManager.GetNewAllChannelList (mGroupChannelList);
//            #endif

            for (int i = 0; i < mGroupChannelList.Count; i++)
            {
                if (mGroupChannelList[i].MemberCount > 2 || ChatManager.IsSingleChannel(mGroupChannelList[i].Name) == false)
                {
                    if (IsGroupAlreadyContain(mGroupChannelList [i]) == false)
                    {
                        if (cannnotDoSorting)
                        {
                            groupChannelList.Add (mGroupChannelList [i]);
                        }
                        else
                        {
                            groupChannelList.Insert (0, mGroupChannelList [i]);
                        }
                    }
                }

                /*if (mGroupChannelList[i].LastMessage != null)
                {
                    if (mGroupChannelList[i].LastMessage.IsGroupChannel())
                    {
                        if (IsChannelAlreadyContain(mGroupChannelList[i]) == false)
                        {
                            if (cannnotDoSorting)
                            {
                                userAllChannelList.Add (mGroupChannelList [i]);
                            }
                            else
                            {
                                userAllChannelList.Insert (0, mGroupChannelList [i]);
                            }
                        }

                        if (mGroupChannelList[i].MemberCount > 2 || ChatManager.IsSingleChannel(mGroupChannelList[i].Name) == false)
                        {
                            if (IsGroupAlreadyContain(mGroupChannelList [i]) == false)
                            {
                                if (cannnotDoSorting)
                                {
                                    groupChannelList.Add (mGroupChannelList [i]);
                                }
                                else
                                {
                                    groupChannelList.Insert (0, mGroupChannelList [i]);
                                }
                            }
                        }
                    }
                }
                else
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log (mGroupChannelList [i].Name);
                    #endif
                }*/
            }

            UnreadMessageCount ();

            updateQueryMessagingChannelList = true;
            if(newMessageUpdateCallback != null)
            {
                newMessageUpdateCallback();
            }

//            #if UNITY_EDITOR && UNITY_DEBUG
//            //            Debug.Log("DONE:: ALL TOTAL " + mGroupChannelList.Count);
//            //            Debug.Log("DONE:: ALL USER CHANNEL " + userAllChannelList.Count);
//            Debug.Log("DONE:: ONLY GROUP CHANNEL " + groupChannelList.Count);
//            #if CACHE_TEST2
//            Debug.Log("DONE:: ONLY GROUP CHANNEL " + allChannelList.Count);
//            #endif
//            #endif

//            #if UNITY_EDITOR
//            //            Debug.Log("DONE:: " + JsonConvert.SerializeObject (mGroupChannelList));
//            Debug.Log("DONE:: " + JsonConvert.SerializeObject (userAllChannelList));
//            #if CACHE_TEST2
//            Debug.Log("DONE:: " + JsonConvert.SerializeObject (allChannelList));
//            #endif
//            #endif

            if (refreshDoneCallback != null)
            {
                refreshDoneCallback ();
                refreshDoneCallback = null;
            }
            cannnotDoSorting = false;
        }
        #endif

        #if CACHE_TEST2
        public void NewMessageReceivedQuery (BaseChannel channel, BaseMessage message)
        {
//            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("channel.IsGroupChannel () " + channel.IsGroupChannel ());
//            #endif

            if (channel.IsGroupChannel ())
            {
                GroupChannel _newGroupChannel = channel as GroupChannel;

//                #if UNITY_EDITOR && UNITY_DEBUG
//                Debug.Log ("GROUP CHANNEL "+ _newGroupChannel.Name);
//                Debug.Log ("GROUP CHANNEL "+ _newGroupChannel.MemberCount);
//                Debug.Log ("GROUP CHANNEL " + _newGroupChannel.UnreadMessageCount);
//                #endif

                if (IsChannelAlreadyContain(_newGroupChannel) == false)
                {
                    userAllChannelList.Insert (0, _newGroupChannel);
//                    #if CACHE_TEST2
//                    allChannelList.Insert (0, CacheManager.GetGroupChannel (_newGroupChannel));
//                    #endif
                }

                if (_newGroupChannel.MemberCount > 2 || ChatManager.IsSingleChannel(_newGroupChannel.Name) == false)
                {
                    if (IsGroupAlreadyContain(_newGroupChannel) == false)
                    {
                        groupChannelList.Insert (0, _newGroupChannel);
                    }
                }

                UnreadMessageCount ();
            }

            if(newMessageUpdateCallback != null)
            {
                newMessageUpdateCallback();
            }
        }
        #endif

        #if CACHE_TEST2
        void UnreadMessageCount ()
        {
            int _newTotalMessages = 0;
            int _newTotalChats = 0;

//            #if CACHE_TEST2
            for (int i = 0; i < allChannelList.Count; i++)
            {
            if (allChannelList[i].UnreadMessageCount > 0)
            {
            _newTotalMessages += allChannelList[i].UnreadMessageCount;
            _newTotalChats++;
            }
            }
//            #else
            for (int i = 0; i < userAllChannelList.Count; i++)
            {
                //Message and Chats Count
                if (userAllChannelList[i].UnreadMessageCount > 0)
                {
                    _newTotalMessages += userAllChannelList[i].UnreadMessageCount;
                    _newTotalChats++;
                }
            }
//            #endif

            newTotalMessages = _newTotalMessages;
            newTotalChats = _newTotalChats;

//            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("newTotalMessages " + newTotalMessages);
//            Debug.Log("newTotalChats " + newTotalChats);
//            #endif
        }
        #endif

        public void OnChannelDeleted (string channelId)
        {
            for (int i = 0; i < userAllChannelList.Count; i++)
            {
                if (userAllChannelList[i].Url == channelId)
                {
                    DeleteChannelFromGroup (userAllChannelList [i]);
                    userAllChannelList.RemoveAt (i);
                    break;
                }
            }
            #if CACHE_TEST2
            for (int i = 0; i < allChannelList.Count; i++)
            {
            if (allChannelList[i].Url == channelId)
            {
            //                    DeleteChannelFromGroup ();
            allChannelList.RemoveAt(i);
            break;
            }
            }
            #endif
        }

        void CheckForDeletedChannel ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("CheckForDeletedChannel");
            #endif
            for (int i = 0; i < userAllChannelList.Count; i++)
            {
                if (CheckForPreviousChannel(userAllChannelList[i]) == false)
                {
                    DeleteChannelFromGroup (userAllChannelList [i]);
                    userAllChannelList.RemoveAt (i);
                    continue;
                }
            }

            #if CACHE_TEST2
            for (int i = 0; i < allChannelList.Count; i++)
            {
            if (CheckForPreviousChannel(allChannelList[i]) == false)
            {
            //                    DeleteChannelFromGroup (allChannelList [i]);
            allChannelList.RemoveAt (i);
            continue;
            }
            }
            #endif
        }

        void DeleteChannelFromGroup (GroupChannel channel)
        {
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                if (groupChannelList[i].Url.Equals (channel.Url))
                {
                    groupChannelList.RemoveAt (i);
                    break;
                }
            }
        }

        /*bool IsChannelAlreadyContain (GroupChannel groupChannel)
        {
            for (int i = 0; i < userAllChannelList.Count; i++)
            {
                if (userAllChannelList[i].Url == groupChannel.Url)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("***** "+userAllChannelList[i].Name);
                    #endif

                    if (cannnotDoSorting)
                    {
                        userAllChannelList [i] = groupChannel;
                    }
                    else
                    {
                        userAllChannelList.RemoveAt (i);
                        userAllChannelList.Insert (0, groupChannel);
                    }
                    return true;
                }
            }

            return false;
        }*/

        bool IsGroupAlreadyContain (GroupChannel groupChannel)
        {
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                if (groupChannelList[i].Url == groupChannel.Url)
                {
                    groupChannelList.RemoveAt (i);
                    groupChannelList.Insert (0, groupChannel);
                    return true;
                }
            }

            return false;
        }

        bool CheckForPreviousChannel (GroupChannel channel)
        {
            for (int i = 0; i < mGroupChannelList.Count; i++)
            {
                if (mGroupChannelList[i].Url.Equals (channel.Url))
                {
                    return true;
                }
            }

            return false;
        }

        /*bool CheckForPreviousChannel (Freak.Chat.Serializable.GroupChannel channel)
        {
            for (int i = 0; i < mGroupChannelList.Count; i++)
            {
                if (mGroupChannelList[i].Url.Equals (channel.Url))
                {
                    return true;
                }
            }

            return false;
        }*/

        /*bool CheckUserInContactList (List<ServerUserContact> data, string chatUserId)
        {
            if (data == null)
                return false;

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].user_id.ToString() == chatUserId)
                {
                    return true;
                }
            }

            return false;
        }*/

        #endregion //Message Channel List Query	
    }
}
