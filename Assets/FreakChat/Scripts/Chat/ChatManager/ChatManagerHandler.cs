﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SendBird;

namespace Freak.Chat
{
    public partial class ChatManager
    {
        public System.Action<ChatMessage> NewMessageReceived;
        public System.Action<bool, List<SendBird.User>> onMessageTypingCallback;
        public System.Action<bool, SendBird.GroupChannel> onMessageTypingAllChatCallback;
        public System.Action<SendBird.BaseChannel> onMessageDeliveredCallback;
        public System.Action<SendBird.GroupChannel> onReadReceivedCallback;
        public BaseChannel currentChannel;
        const string UNIQUE_HANDLER_ID = "defaultHandlerID";

        #region Channel Handler
        public void ChannelHandler()
        {
            SendBirdClient.ChannelHandler channelHandler = new SendBirdClient.ChannelHandler ();

            channelHandler.OnChannelChanged = HandleOnChannelChangedDelegate;
            channelHandler.OnChannelDeleted = HandleOnChannelDeletedDelegate;

            channelHandler.OnMessageDeleted = HandleOnMessageDeletedDelegate;
            channelHandler.OnMessageReceived = HandleOnMessageReceivedDelegate;

            channelHandler.OnReadReceiptUpdated = HandleOnReadReceiptUpdatedDelegate;

            channelHandler.OnTypingStatusUpdated = HandleOnTypingStatusUpdatedDelegate;

            channelHandler.OnUserJoined = HandleOnUserJoinedDelegate;
            channelHandler.OnUserLeft = HandleOnUserLeftDelegate;

            SendBirdClient.AddChannelHandler (UNIQUE_HANDLER_ID, channelHandler);
        }

        public void RemoveChannelhandler ()
        {
            SendBirdClient.RemoveChannelHandler(UNIQUE_HANDLER_ID);
        }

        void HandleOnMessageReceivedDelegate (BaseChannel channel, BaseMessage message)
        {
            ChatManager.Instance.ResetRefreshCoroutine (10f);

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnMessageReceivedDelegate " + channel.Url + "ID: " + message.MessageId);
            #endif

            CheckLocalNotification (channel, message);

            #if !PER_CHECK
            NewMessageReceivedQueryNew (channel, message);
            #endif

            if (currentChannel.Url == channel.Url)
            {
                #if PER_CHECK
                NewMessageReceivedQueryNew (channel, message);
                #endif

                if (leaveChannelCallback == null)
                {
                    if (ChatManager.Instance.NewMessageReceived != null)
                    {
                        ChatManager.Instance.NewMessageReceived (currentSelectedChatChannel.newMessage);
                    }

                    if (loadMessageCallback != null)
                    {
                        loadMessageCallback();
                    }
                }
            }
        }

        void HandleOnUserLeftDelegate (GroupChannel channel, User user)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnUserLeftDelegate " + channel.Name + "User: " + user.Nickname);
            #endif
            if (currentChannel.Url == channel.Url)
            {
                currentChannel = channel;
            }
        }

        void HandleOnUserJoinedDelegate (GroupChannel channel, User user)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnUserJoinedDelegate " + channel.Name + "User: " + user.Nickname);
            #endif
            if (currentChannel.Url == channel.Url)
            {
                currentChannel = channel;

                if (createNewGroupCallback != null)
                {
                    createNewGroupCallback(channel);
                    createNewGroupCallback = null;
                }
            }
        }

        void HandleOnTypingStatusUpdatedDelegate (GroupChannel channel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnTypingStatusUpdatedDelegate " + channel.IsTyping());
            #endif

            if (onMessageTypingAllChatCallback != null)
            {
                onMessageTypingAllChatCallback(channel.IsTyping(), channel);
            }

            if (currentChannel.Url == channel.Url)
            {
                if (onMessageTypingCallback != null)
                    onMessageTypingCallback(channel.IsTyping(), channel.TypingMembers);
            }
        }

        void HandleOnReadReceiptUpdatedDelegate (GroupChannel channel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnReadReceiptUpdatedDelegate " + JsonConvert.SerializeObject (channel));
            #endif
            if (currentChannel.Url == channel.Url)
            {
                currentChannel = channel;
            }

            #if !PER_CHECK
            CacheManager.UpdateReadReceipt (channel);
            #endif

            if (onReadReceivedCallback != null)
            {
                onReadReceivedCallback (channel);
            }
        }

        public List<SendBird.User> GetReadMembers(BaseMessage baseMessage)
        {
            GroupChannel _groupChannel = currentChannel as GroupChannel;
            return _groupChannel.GetReadMembers(baseMessage);
        }

        void HandleOnMessageDeletedDelegate (BaseChannel channel, long msgId)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnMessageDeletedDelegate " + channel.Name + "ID: " + msgId);
            #endif
        }

        void HandleOnChannelDeletedDelegate (string channelUrl, BaseChannel.ChannelType channelType)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleOnChannelDeletedDelegate " + channelUrl);
            #endif
        }

        void HandleOnChannelChangedDelegate (BaseChannel channel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("HandleOnChannelChangedDelegate " + channel.Name);
            Debug.Log("HandleOnChannelChangedDelegate " + JsonConvert.SerializeObject (channel));
            #endif

            /*if (onMessageDeliveredCallback != null)
            {
                onMessageDeliveredCallback (channel);
            }*/
        }
        #endregion

        #region Connection Handler

        private bool isReconnectStarted = false;
        void ConnectionHandler ()
        {
            SendBirdClient.ConnectionHandler ch = new SendBirdClient.ConnectionHandler ();

            ch.OnReconnectFailed = () => {
                // Auto reconnecting failed. You should call `connect` to reconnect to SendBird.
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OnReconnectFailed =====>>>>");
                isReconnectStarted = false;
                #endif
            };

            ch.OnReconnectStarted = () => {
                // Network has been disconnected. Auto reconnecting starts.
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OnReconnectStarted =====>>>>");
                isReconnectStarted = true;
                isSeandbirdLogined = false;
                #endif
            };

            ch.OnReconnectSucceeded = () => {
                // Auto reconnecting succeeded.
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("OnReconnectSucceeded =====>>>>");
                isReconnectStarted = false;
                isSeandbirdLogined = true;

                if (FreakAppManager.Instance.GetHomeScreenPanel () != null &&
                    FreakAppManager.Instance.GetHomeScreenPanel ().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
                {
                    RefreshForNewMessages ();
                }
                #endif
            };

            SendBirdClient.AddConnectionHandler(UNIQUE_HANDLER_ID, ch);
        }

        void RemoveConnectionHandler ()
        {
            SendBirdClient.RemoveConnectionHandler(UNIQUE_HANDLER_ID);
        }

        #endregion
    }
}
