﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SendBird;

namespace Freak.Chat
{
    public partial class ChatManager
    {
    #region PUSH NOTIFICATION
        private System.Action UnRegisterCallback;

        public void RegisterPushNotification ()
        {
            if (string.IsNullOrEmpty (FreakPlayerPrefs.DeviceToken) == false)
            {
                //FIRST UNREGISTER FOR OTHER DEVICES....
//                UnRegisterCallback = RegisterNewToken;
//                SetPushNotification (false);

                SetPushNotification (FreakPlayerPrefs.AllNotification);
            }
        }

        void RegisterNewToken ()
        {
            UnRegisterCallback = null;
            SetPushNotification (FreakPlayerPrefs.AllNotification);
        }

        void RegisterPushTokenWithStatusHandler (SendBirdClient.PushTokenRegistrationStatus status, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("RegisterPushTokenWithStatusHandler error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            isDeviceTokenRegistered = false;

            switch(status)
            {
                case SendBirdClient.PushTokenRegistrationStatus.ERROR:
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Register Token Error");
                    #endif
                break;
                case SendBirdClient.PushTokenRegistrationStatus.PENDING:
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Register Token Pending");
                    #endif
                break;
                case SendBirdClient.PushTokenRegistrationStatus.SUCCESS:
                    isDeviceTokenRegistered = true;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("Register Token Success");
                    #endif
                break;
            }
        }

        public void SetPushNotification(bool enable)
        {
            if (enable)
            {
                if (ChatManager.Instance.isInternetConnected)
                {
                    #if UNITY_IOS || UNITY_IPHONE
                    SendBirdClient.RegisterAPNSPushTokenForCurrentUser(FreakPlayerPrefs.DeviceToken, RegisterPushTokenWithStatusHandler);
                    #elif UNITY_ANDROID && !UNITY_EDITOR
                    SendBirdClient.RegisterFCMPushTokenForCurrentUser(FreakPlayerPrefs.DeviceToken, RegisterPushTokenWithStatusHandler);
                    #endif
                }
            }
            else 
            {
                if (ChatManager.Instance.isInternetConnected)
                {
                    // If you want to unregister the current device only, invoke this method.
                    SendBirdClient.UnregisterFCMPushTokenForCurrentUser(FreakPlayerPrefs.DeviceToken, UnregisterPushTokenHandler);
                    // If you want to unregister the all devices of the user, invoke this method.
//                    SendBirdClient.UnregisterPushTokenAllForCurrentUser(UnregisterPushTokenHandler);
                }
            }
        }

//        public void TestUnRegisterAllPushNotification ()
//        {
//            SendBirdClient.UnregisterPushTokenAllForCurrentUser(UnregisterPushTokenHandler);
//        }

//        void UnRegisterAllPushNotification (System.Action callback)
//        {
//            if (callback != null)
//            {
//                UnRegisterCallback = callback;
//            }
//
//            // If you want to unregister the all devices of the user, invoke this method.
//            SendBirdClient.UnregisterPushTokenAllForCurrentUser(UnregisterPushTokenHandler);
//        }

        void UnregisterPushTokenHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("UnregisterPushTokenHandler error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            isDeviceTokenRegistered = false;
            if (UnRegisterCallback != null)
            {
                UnRegisterCallback();
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("UnregisterPushTokenHandler success");
            #endif
        }

        /*void UnregisterAllPushTokenHandler (SendBirdException e)
        {
            if (e != null)
            {
                SendBirdException (e.Code, e.Message);
                return;
            }

            Debug.Log("UnregisterAllPushTokenHandler");
        }*/

        public void SetPushNotificationForCurrentChannel (bool enable)
        {
            GroupChannel _groupChannel = currentChannel as GroupChannel;
            _groupChannel.SetPushPreference(enable, SetPushNotificationForCurrentChannelHandler);
        }

        void SetPushNotificationForCurrentChannelHandler (SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("SetPushNotificationForCurrentChannelHandler error");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SetPushNotificationForCurrentChannelHandler");
            #endif
        }

    #endregion

    #region Local Notification
        //Before Sneding local notification first check
        //if Chat window is already opened....

        class LocalNotificationDetails
        {
            public int id;
            public SendBird.BaseMessage baseMessage;
            public SendBird.BaseChannel baseChannel;
        }

        private List<LocalNotificationDetails> mLocalNotificationDetailsList = new List<LocalNotificationDetails>();

        public void CheckLocalNotification (BaseChannel channel, BaseMessage message)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("CheckLocalNotification");
            Debug.Log ("Notification is turned " + FreakPlayerPrefs.AllNotification);
            #endif

            if (FreakPlayerPrefs.AllNotification == false)
            {
                return;
            }

            bool notificationSound = true;
            if (FreakChatUtility.FreakChatPlayerPrefs.MuteUserList.IsMuted(channel.Url) == true)
            {
                notificationSound = false;
            }
            else if (FreakPlayerPrefs.MuteAllChat)
            {
                notificationSound = false;
            }

            if (CanShowLocalNotification())
            {
                if (message is SendBird.AdminMessage)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("HANDLE ADMIN MESSAGE");
                    #endif
                }
                else
                {
                    string _message = string.Empty, _sender = string.Empty;
                    
                    if (message is SendBird.UserMessage)
                    {
                        SendBird.UserMessage userMessage = message as SendBird.UserMessage;
                        _message = userMessage.Message;
                        _sender = userMessage.Sender.Nickname;
                    }
                    else if (message is SendBird.FileMessage)
                    {
                        SendBird.FileMessage fileMessage = message as SendBird.FileMessage;
                        _sender = fileMessage.Sender.Nickname;
                        _message = _sender + " send you " + ChatFileType.ConvertChatFileTypeFromString (fileMessage.Type) + " file.";
                    }
                    
                    int id = 0;//FreakChatUtility.FreakChatPlayerPrefs.FreakChatLocalNotificationID;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("Notification ID----> " + id);
                    #endif
                    
                    if (CheckNotificatioStageMessage(channel.Url) == null)
                    {
                        LocalNotificationDetails _details = new LocalNotificationDetails ();
                        _details.id = id;
                        _details.baseMessage = message;
                        _details.baseChannel = channel;
                        
                        mLocalNotificationDetailsList.Add (_details);
                    }
                    else
                    {
                        CheckNotificatioStageMessage(channel.Url).baseMessage = message;
                        CheckNotificatioStageMessage(channel.Url).baseChannel = channel;
                    }
                    //                        #if UNITY_ANDROID && !UNITY_EDITOR
                    
                    if (mLocalNotificationDetailsList.Count > 1)
                    {
                        LocalNotification.SendLocalNotification (id, Application.productName, mLocalNotificationDetailsList.Count.ToString (),
                            "Unread Message", message.MessageId.ToString (), notificationSound);
                    }
                    else
                    {
                        LocalNotification.SendLocalNotification (id, Application.productName, _message, _sender, message.MessageId.ToString(), notificationSound);
                    }
                    //                        #endif
                    
                    #if UNITY_EDITOR
//                                            OnReceiveNotification(message.MessageId);
                    #endif
                }
            }
        }

        bool CanShowLocalNotification ()
        {
            if (FreakAppManager.Instance.GetHomeScreenPanel() != null)
            {
                if (FreakAppManager.Instance.GetHomeScreenPanel().currentpanelState == HomeScreenUIPanelController.PanelType.ChatWindow)
                {
                    return false;
                }
            }

            return true;
        }

        LocalNotificationDetails CheckNotificatioStageMessage (string channelUrl)
        {
            for (int i = 0; i < mLocalNotificationDetailsList.Count; i++)
            {
                if (mLocalNotificationDetailsList[i].baseChannel.Url == channelUrl)
                {
                    return mLocalNotificationDetailsList[i];
                }
            }

            return null;
        }

        public void ClearAllLocalNotification ()
        {
            mLocalNotificationDetailsList.Clear ();
        }

        /// <summary>
        /// Raises the receive notification event.
        /// </summary>
        /// <param name="id">Notification ID.</param>
        public void OnReceiveNotification (long messageId)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("OnReceiveNotification-----> " + messageId);
            #endif

            Debug.Log("CURRENT SCENE " + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);

            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == FreakAppConstantPara.UnitySceneName.FreakHomeScene)
            {
                for (int i = 0; i < mLocalNotificationDetailsList.Count; i++)
                {
                    if (mLocalNotificationDetailsList[i].baseMessage.MessageId == messageId)
                    {
                        #if UNITY_EDITOR && UNITY_DEBUG
                        Debug.Log("OPEN CHAT " + mLocalNotificationDetailsList[i].baseChannel.Url);
                        Debug.Log ("OPEN CHAT " + mLocalNotificationDetailsList [i].baseMessage.MessageId);
                        #endif

                        currentChannel = mLocalNotificationDetailsList [i].baseChannel;
                        //                    ChatChannel _chatChannel = new ChatChannel ();
                        //                    _chatChannel = ChatChannelHandler.InitChatChannel (mLocalNotificationDetailsList [i].baseChannel as GroupChannel);

                        GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
                        go.GetComponent<ChatWindowUI>().InitChat(CacheManager.GetInitChatChannel(mLocalNotificationDetailsList[i].baseChannel as GroupChannel), true);
                        ChatManager.Instance.GetChannelByChannelUrl (mLocalNotificationDetailsList[i].baseChannel.Url, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);

                        mLocalNotificationDetailsList.RemoveAt (i);
                        break;
                    }
                }
            }
            else
            {
                if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == FreakAppConstantPara.UnitySceneName.TicTacToe)
                {
                    PlayerPrefs.SetString(FreakAppConstantPara.PlayerPrefsName.LocalNotificationFromDifferentScene, messageId.ToString());
                    FindObjectOfType<TTTUiManager>().m_mainMenuController.loadingPanle.SetActive(true);
                }
            }
        }
    #endregion
    }
}
