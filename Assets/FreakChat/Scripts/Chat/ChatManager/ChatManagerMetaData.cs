﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SendBird;
using Freak.Chat;

namespace Freak.Chat
{
    public partial class ChatManager
    {
        //Handle SendBird Meta Data.....

        private Action<Dictionary<string,int>> metaDataCounterCallback;
        private Action<bool> deleteMetaDataCounterCallback;

        public void GetMetaDataCounter (BaseChannel baseChannel, Action<Dictionary<string,int>> callback)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetMetaDataCounter");
            #endif
            if (ChatManager.Instance.isInternetConnected)
            {
                metaDataCounterCallback = callback;
                baseChannel.GetAllMetaCounters (MetaDataCounterHandler);
            }
        }

        public void CreateMetaDataCounter (BaseChannel baseChannel, Dictionary<string,int> metaData, Action<Dictionary<string,int>> callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("CreateMetaDataCounter " + MiniJSON.Json.Serialize(metaData));
            #endif
            if (ChatManager.Instance.isInternetConnected)
            {
                metaDataCounterCallback = callback;
                baseChannel.CreateMetaCounters (metaData, MetaDataCounterHandler);
            }
        }

        public void UpdateMetaDataCounter (BaseChannel baseChannel, Dictionary<string,int> metaData, Action<Dictionary<string,int>> callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("UpdateMetaDataCounter " + MiniJSON.Json.Serialize(metaData));
            #endif
            if (ChatManager.Instance.isInternetConnected)
            {
                metaDataCounterCallback = callback;
                baseChannel.UpdateMetaCounters (metaData, MetaDataCounterHandler);
            }
        }

        [System.Serializable]
        public class DeleteMetaDataQueue
        {
            public BaseChannel baseChannel;
            public string metaKey;
            public Action<bool> metaCallback;
        }
        private List<DeleteMetaDataQueue> mDeleteMetaDataQueueList = new List<DeleteMetaDataQueue> ();
        private bool isDeleting;
        public void DeleteMetaDataCounter (BaseChannel baseChannel, string metaKey, Action<bool> callback = null)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("DeleteMetaDataCounter " + metaKey);
            #endif
            if (InternetConnection.Check())
            {
                if (isDeleting)
                {
                    return;
                    for (int i = 0; i < mDeleteMetaDataQueueList.Count; i++)
                    {
                        if (mDeleteMetaDataQueueList[i].metaKey == metaKey)
                        {
                            return;
                        }
                    }

                    DeleteMetaDataQueue _queue = new DeleteMetaDataQueue ();
                    _queue.baseChannel = baseChannel;
                    _queue.metaKey = metaKey;
                    _queue.metaCallback = callback;

                    mDeleteMetaDataQueueList.Add (_queue);
                    return;
                }

                isDeleting = true;
//                deleteMetaDataCounterCallback = callback;
//                baseChannel.DeleteMetaCounter (metaKey, DeleteMetaDataCounterHandler);
            }
        }

        void DeleteMetaDataCounterHandler (SendBird.SendBirdException e)
        {
            isDeleting = false;

            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("DeleteMetaDataCounterHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                if (deleteMetaDataCounterCallback != null)
                {
                    deleteMetaDataCounterCallback (false);
                }

                if (mDeleteMetaDataQueueList.Count > 0)
                {
                    DeleteMetaDataCounter (mDeleteMetaDataQueueList [0].baseChannel, mDeleteMetaDataQueueList [0].metaKey, mDeleteMetaDataQueueList [0].metaCallback);
                    mDeleteMetaDataQueueList.RemoveAt (0);
                }

                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("DeleteMetaDataCounterHandler");
            #endif
            if (deleteMetaDataCounterCallback != null)
            {
                deleteMetaDataCounterCallback (true);
            }

            if (mDeleteMetaDataQueueList.Count > 0)
            {
                DeleteMetaDataCounter (mDeleteMetaDataQueueList [0].baseChannel, mDeleteMetaDataQueueList [0].metaKey, mDeleteMetaDataQueueList [0].metaCallback);
                mDeleteMetaDataQueueList.RemoveAt (0);
            }
        }

        void MetaDataCounterHandler (Dictionary<string, int> metaCounterMap, SendBird.SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("MetaDataCounterHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("MetaDataCounterHandler " + MiniJSON.Json.Serialize (metaCounterMap));
            #endif
            if (metaDataCounterCallback != null)
            {
                metaDataCounterCallback (metaCounterMap);
            }
        }

        #region META DATA

        public void GetMetaData (SendBird.BaseChannel baseChannel, List<string> keys)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetMetaData");
            #endif
            baseChannel.GetMetaData(keys, GetMetaDataHandler);
        }

        void GetMetaDataHandler (Dictionary<string, string> metaDataMap, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("GetMetaDataHandler " + e.Message);
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(Newtonsoft.Json.JsonConvert.SerializeObject(metaDataMap));
            #endif
        }

        #endregion
    }
}
