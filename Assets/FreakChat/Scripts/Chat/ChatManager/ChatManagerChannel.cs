﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SendBird;

namespace Freak.Chat
{
    public partial class ChatManager
    {
        #region Create New Channel

        public const string SingleChannel = "Single_Channel";

        public void CreateNewChannel (string userid, System.Action callback)
        {
            loadMessageCallback = callback;
            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                List<string> users = new List<string>();
                users.Add(userid);

                GroupChannel.CreateChannelWithUserIds(users, true, ChatManager.SingleChannel, string.Empty, string.Empty, HandleNewChannel);
            }
        }

        void HandleNewChannel (GroupChannel channel, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleNewChannel");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleCreateNewChannel " + JsonConvert.SerializeObject(channel));
            #endif
            Instance.currentChannel = channel;

            #if SB_NON_SER
            Instance.currentSelectedChatChannel.NewChannel(channel);
            #else
            Instance.currentSelectedChatChannel = CacheManager.GetInitChatChannel(channel);
            #endif

            if (channel.LastMessage != null)
            {
                // If current channel already exits get for previous messages.
                //                LoadPreviousMessages(channel.LastMessage.CreatedAt, loadMessageCallback);
                LoadMessage (loadMessageCallback, channel.LastMessage.CreatedAt, channel.UnreadMessageCount);
            }
            else
            {
                loadMessageCallback();
            }
        }

        public void CreateChannelForGame (string userid, System.Action callback)
        {
            loadMessageCallback = callback;
            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                List<string> users = new List<string>();
                users.Add(userid);

                GroupChannel.CreateChannelWithUserIds(users, true, ChatManager.SingleChannel, string.Empty, string.Empty, CreateChannelForGameHandler);
            }
        }

        void CreateChannelForGameHandler (GroupChannel channel, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleNewChannel");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleCreateNewChannel " + JsonConvert.SerializeObject(channel));
            #endif
            Instance.currentChannel = channel;

            loadMessageCallback();
        }
        #endregion //Create New Channel

        #region Create New Group Channel
        private Action<GroupChannel> createNewGroupCallback;
        public void CreateNewGroupChannel (List<string> userIds, string name, SBFile cover, GroupSerializableResponseClass.MetaData data, Action<GroupChannel> callback)
        {
            createNewGroupCallback = callback;
            GroupChannel.CreateChannelWithUserIds(userIds, false, name, cover, JsonConvert.SerializeObject(data), HandleNewGroupChannelCreateHandler);
        }

        void HandleNewGroupChannelCreateHandler (GroupChannel channel, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleNewGroupChannelCreateHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                if (createNewGroupCallback != null)
                    createNewGroupCallback(null);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleNewGroupChannelCreateHandler " + JsonConvert.SerializeObject(channel));
            #endif

            Instance.currentChannel = channel;

            Instance.currentSelectedChatChannel = CacheManager.GetInitChatChannel(channel);
//            CheckForPreviousChannelFirst (channel);

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("HandleNewGroupChannelCreateHandler");
            #endif
        }
        #endregion  //Create New Group Channel

        #region Update Group

        private Action<GroupChannel> mUpdateGroupCallback;
        public void UpdateGroup (string groupName, object sendBirdFile, GroupSerializableResponseClass.MetaData data, Action<GroupChannel> callback)
        {
            mUpdateGroupCallback = callback;

            GroupChannel groupChannel = currentChannel as GroupChannel;
            groupChannel.UpdateChannel(groupName, sendBirdFile, JsonConvert.SerializeObject(data), UpdateGroupHandler);
        }

        /*public void UpdateGroupWithLink (string groupName, object imageUrl, GroupSerializableResponseClass.MetaData data, Action<GroupChannel> callback)
        {
            Debug.Log (groupName);
            mUpdateGroupCallback = callback;

            GroupChannel groupChannel = currentChannel as GroupChannel;
            groupChannel.UpdateChannel(true, groupName, imageUrl, JsonConvert.SerializeObject(data), UpdateGroupHandler);
        }*/

        void UpdateGroupHandler (GroupChannel channel, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("UpdateGroupHandler");
                #endif
                SendBirdExceptionHandler (e.Code, e.Message);
                if (mUpdateGroupCallback != null)
                    mUpdateGroupCallback(null);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("UpdateGroupHandler " + channel.Name);
            #endif
            if (mUpdateGroupCallback != null)
                mUpdateGroupCallback(channel);
        }
        #endregion

        #region Get Channel
        private System.Action<GroupChannel> getGroupChannelCallback;
        public void GetChannelByChannelUrl (string channelUrl, System.Action<GroupChannel> callback)
        {
            if (ChatManager.Instance.isInternetConnected && IsSeandbirdLogined)
            {
                getGroupChannelCallback = callback;
                GroupChannel.GetChannel(channelUrl, GetChannelByChannelUrlCallback);
            }
            else
            {
                callback (null);
            }
        }

        void GetChannelByChannelUrlCallback (GroupChannel channel, SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("GetChannelByChannelUrlCallback error");
                #endif
                SendBirdExceptionHandler (e.Code);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("GetChannelByChannelUrlCallback " + JsonConvert.SerializeObject(channel));
            #endif

//            currentChannel = channel;
            if(getGroupChannelCallback != null)
            {
                getGroupChannelCallback(channel);
            }
        }

        #endregion //Get Channel

        /*void CheckForPreviousChannelFirst (GroupChannel channel)
        {
            if (string.IsNullOrEmpty (Instance.currentSelectedChatChannel.channelUrl) || Instance.currentSelectedChatChannel.channelUrl != channel.Url)
            {
                for (int i = 0; i < Instance.cachedData.allChannelList.Count; i++)
                {
                    if (Instance.cachedData.allChannelList[i].channelUrl == channel.Url)
                    {
                        Instance.currentSelectedChatChannel = Instance.cachedData.allChannelList [i];
                    }
                }

                Instance.currentSelectedChatChannel = ChatChannelHandler.InitChatChannel(channel);
            }
            else
            {
                Instance.currentSelectedChatChannel = ChatChannelHandler.InitChatChannel(channel);
            }
        }*/
	}
}
