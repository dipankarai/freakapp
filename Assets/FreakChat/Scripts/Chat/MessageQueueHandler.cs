﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
	public class MessageQueueHandler : MonoBehaviour
	{
		#region Init Instance
		private static MessageQueueHandler instance;
		public static MessageQueueHandler Instance
		{
			get
			{
				return InitInstance();
			}
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		/// <returns></returns>
		public static MessageQueueHandler InitInstance()
		{
			if (instance == null)
			{
				GameObject instGO = new GameObject("~MessageQueueHandler");
				instance = instGO.AddComponent<MessageQueueHandler>();

				DontDestroyOnLoad(instGO);
			}
			return instance;
		}
		#endregion

//        public delegate void OnMessageSendHandler (SendBird.BaseMessage baseMessage);
//        public delegate void OnMessageFailHandler (MessageSendQueue queueMessage);

		public class AllMessageQueue
		{
            public SendBird.BaseChannel baseChannel = null;
            public string channelUrl;
			public string data;
			public string customType;
		}

		public class MessageSendQueue : AllMessageQueue
		{
			public string msg;
            public Action callback = null;
		}

		public class FileSendQueue : AllMessageQueue
		{
			public string fileUrl;
			public string name;
			public string type;
			public int size;
			public ChatFileType fileInfo;
		}

        private List<MessageQueue> mMessageQueue = new List<MessageQueue>();
        private MessageQueue currentMessage;

		// Use this for initialization
		void Start ()
		{
	
		}

        public void SendUserMessage(SendBird.BaseChannel baseChannel, string channelUrl, string msg, string data, string customType, Action callback)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendUserMessage :::: " + msg);
            #endif

            MessageQueueHandler.MessageSendQueue _MessageSendQueue = new MessageQueueHandler.MessageSendQueue ();
            _MessageSendQueue.channelUrl = channelUrl;
            _MessageSendQueue.msg = msg;
            _MessageSendQueue.data = data;
            _MessageSendQueue.customType = customType;
            _MessageSendQueue.callback = callback;
            _MessageSendQueue.baseChannel = baseChannel;

            AddInQueue(_MessageSendQueue);
        }

        public void SendFileMessage (SendBird.BaseChannel baseChannel, string channelUrl, ChatFileType fileInfo, string customType, string data)
        {
            FileSendQueue _mFileSendQueue = new FileSendQueue ();
            _mFileSendQueue.channelUrl = channelUrl;
            _mFileSendQueue.fileInfo = fileInfo;
            _mFileSendQueue.data = data;
            _mFileSendQueue.customType = customType;
            _mFileSendQueue.baseChannel = baseChannel;

            AddInQueue (_mFileSendQueue);
        }

        public void SendFileMessageWithUrl (SendBird.BaseChannel baseChannel, string channelUrl, string fileUrl, string name, string type, int size, string data, string customType)
        {
            FileSendQueue _mFileSendQueue = new FileSendQueue ();
            _mFileSendQueue.channelUrl = channelUrl;
            _mFileSendQueue.data = data;
            _mFileSendQueue.fileUrl = fileUrl;
            _mFileSendQueue.name = name;
            _mFileSendQueue.size = size;
            _mFileSendQueue.type = type;
            _mFileSendQueue.customType = customType;
            _mFileSendQueue.baseChannel = baseChannel;

            AddInQueue (_mFileSendQueue);
        }

		void AddInQueue (AllMessageQueue queue)
		{
            if (CheckForPreviousQueue(queue) == false)
            {
                GameObject go = new GameObject(queue.channelUrl);
                go.transform.SetParent(this.transform);

                CacheManager.MoveChannelToTopList(queue.channelUrl);

                MessageQueue _MessageQueue = go.AddComponent<MessageQueue>();
                _MessageQueue.AddInQueue(queue);
                mMessageQueue.Add(_MessageQueue);
            }

            //SendMessage();
        }

        bool CheckForPreviousQueue (AllMessageQueue queue)
        {
            for (int i = 0; i < mMessageQueue.Count; i++)
            {
                if (mMessageQueue[i].ChannelUrl == queue.channelUrl)
                {
                    mMessageQueue[i].AddInQueue(queue);
                    return true;
                }
            }

            return false;
        }

        bool isSendingMessag;
        float previousSendMessageTime;
        void SendMessage ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessage " + isSendingMessag);
            #endif

            if (isSendingMessag)
                return;

            if (mMessageQueue.Count > 0)
            {
                isSendingMessag = true;
                if (ChatManager.Instance.currentChannel == null)
                {
                    if (mMessageQueue.Count > 0)
                    {
                        currentMessage = mMessageQueue[0];
                    }
                }
                else
                {
                    if (CheckForCurrentChannel() == null)
                    {
                        if (mMessageQueue.Count > 0)
                        {
                            currentMessage = mMessageQueue[0];
                        }
                    }
                    else
                    {
                        currentMessage = CheckForCurrentChannel();
                    }
                }

                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("SendMessage " + currentMessage.ChannelUrl);
                #endif
                previousSendMessageTime = Time.time;
                Debug.Log("OnMessagesendCallback " + previousSendMessageTime);
                Debug.Log("OnMessagesendCallback IsSeandbirdLogined " + ChatManager.Instance.IsSeandbirdLogined);

                StartCoroutine(SendMessageCoroutine());

                /*currentMessage.SendMessage();

                if (checkForMessageCallbackRoutine != null)
                {
                    StopCoroutine(checkForMessageCallbackRoutine);
                }

                checkForMessageCallbackRoutine = CheckForMessageCallback();
                StartCoroutine(checkForMessageCallbackRoutine);*/
            }
        }

        IEnumerator SendMessageCoroutine ()
        {
            float elapsedTime = 0f;
            while (ChatManager.Instance.IsSeandbirdLogined == false)
            {
                elapsedTime += Time.deltaTime;
                yield return null;

                if (elapsedTime > 3f)
                {
                    break;
                }

                if (ChatManager.Instance.IsSeandbirdLogined)
                    break;
            }

            currentMessage.SendMessage();
            if(checkForAnyUnSendMessageRoutine == null)
            {
                checkForAnyUnSendMessageRoutine = CheckForAnyUnSendMessage();
                StartCoroutine(checkForMessageCallbackRoutine);
            }

            if (checkForMessageCallbackRoutine != null)
            {
                StopCoroutine(checkForMessageCallbackRoutine);
            }

            checkForMessageCallbackRoutine = CheckForMessageCallback();
            StartCoroutine(checkForMessageCallbackRoutine);
        }

        IEnumerator checkForMessageCallbackRoutine;
        IEnumerator CheckForMessageCallback ()
        {
            float elapsedTime = 0f;
            while (isSendingMessag)
            {
                elapsedTime += Time.deltaTime;
                yield return null;

                if (elapsedTime > 3f)
                {
                    OnMessagesendCallback(false);
                    break;
                }
            }
        }

        IEnumerator checkForAnyUnSendMessageRoutine;
        IEnumerator CheckForAnyUnSendMessage ()
        {
            while (true)
            {
                if (mMessageQueue != null && mMessageQueue.Count > 0)
                {
                    if (isSendingMessag == false)
                    {
                        SendMessage();
                    }
                }
                else
                {
                    StopCheckForAnyUnSendMessageRoutine();
                }
                yield return null;
            }
        }

        void StopCheckForAnyUnSendMessageRoutine ()
        {
            StopCoroutine(checkForAnyUnSendMessageRoutine);
            checkForAnyUnSendMessageRoutine = null;
        }

        MessageQueue CheckForCurrentChannel ()
        {
            for (int i = 0; i < mMessageQueue.Count; i++)
            {
                if (mMessageQueue[i].ChannelUrl == ChatManager.Instance.currentChannel.Url)
                {
                    return mMessageQueue[i];
                }
            }

            return null;
        }

        private int invokeCount = 0;
        public void OnMessagesendCallback (bool success)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError("OnMessagesendCallback " + success);
            #endif
            isSendingMessag = false;

            if (success)
            {
                currentMessage.MesageSend();
            }
           
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("OnMessagesendCallback " + previousSendMessageTime);
            #endif

            if (ChatManager.Instance.isInternetConnected)
            {
                if (previousSendMessageTime + 1f > Time.time)
                {
                    SendMessage();
                }
                else
                {
                    Invoke("SendMessage", 2f);
                }
                invokeCount = 0;
            }
            /*else
            {
                if (invokeCount < 15)
                {
                    Invoke("SendMessage", 4f);
                    invokeCount++;
                }
                else
                {
                    currentMessage.DiscardAllMesage();
                    invokeCount = 0;
                }
            }*/
        }

        public void OnMessagesendCallbackStopAll ()
        {
            isSendingMessag = false;
            currentMessage.DiscardAllMesage();
        }

        public void RemovedQueueMessage (MessageQueue queue)
        {
            for (int i = 0; i < mMessageQueue.Count; i++)
            {
                if (mMessageQueue[i].ChannelUrl == queue.ChannelUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("RemovedQueueMessage " + mMessageQueue[i].ChannelUrl);
                    #endif
                    Destroy(mMessageQueue[i].gameObject);
                    mMessageQueue.RemoveAt(i);
                    break;
                }
            }
        }
	}
}
