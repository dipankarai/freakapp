﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class ChatServerAPI : MonoBehaviour
    {
#region Initializing Instance
        private static ChatServerAPI instance;
        public static ChatServerAPI Instance
        {
            get
            {
                return InitInstance();
            }
        }
        
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public static ChatServerAPI InitInstance()
        {
            if (instance == null)
            {
                GameObject instGO = new GameObject("ChatServerAPI");
                instance = instGO.AddComponent<ChatServerAPI>();
                
                DontDestroyOnLoad(instGO);
            }
            return instance;
        }
#endregion
        
        public delegate void BlockUserCallbackMethod(bool success);
        public delegate void BlockUserListCallbackMethod(List<FreakChatSerializeClass.BlockedUserList> userList);

        private BlockUserCallbackMethod blockUserCallbackMethod;
        private BlockUserCallbackMethod unblockUserCallbackMethod;
        private BlockUserListCallbackMethod blockUserListCallbackMethod;

        /// <summary>
        /// Initializes its instance.
        /// </summary>
        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
        }
        

        public void BlockUser (string userId, string otherUserId, BlockUserCallbackMethod blockUserCallback)
        {
            blockUserCallbackMethod = blockUserCallback;

            Debug.Log("Block User");
            FreakApi api = new FreakApi("user.block", ServerResponseBlockUser);
            api.param("userId", userId);
            api.param("other_user_id", otherUserId);
            api.get(this);
        }
        
        void ServerResponseBlockUser (IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
        {
            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];
                
                switch(responseCode){
                    case 001:
                        //Response Success:::
                        
                        /*IDictionary userDetailsDict = (IDictionary)output["responseMsg"];
                        string name = userDetailsDict["nickname"].ToString();
                        string lastSeenAt = userDetailsDict["last_seen_at"].ToString();
                        string userId = userDetailsDict["user_id"].ToString();
                        string profileUrl = userDetailsDict["profile_url"].ToString();
                        string isOnline = userDetailsDict["is_online"].ToString();*/

                        if (blockUserCallbackMethod != null)
                            blockUserCallbackMethod(true);

                        break;
                        
                    case 400201:
                        //User not found
                        Debug.LogError("USER NOT FOUND....");
                        if (blockUserCallbackMethod != null)
                            blockUserCallbackMethod(false);
                        break;
                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
                if (blockUserCallbackMethod != null)
                    blockUserCallbackMethod(false);
            }

            blockUserCallbackMethod = null;
        }

        public void UnblockUser (string userId, string otherUserId, BlockUserCallbackMethod unblockUserCallback)
        {
            unblockUserCallbackMethod = unblockUserCallback;

            Debug.Log("Unblock User");
            FreakApi api = new FreakApi("user.unBlock", ServerResponseUnblockUser);
            api.param("userId", userId);
            api.param("other_user_id", otherUserId);
            api.get(this);
        }

        void ServerResponseUnblockUser (IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
        {
            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::

                        if (unblockUserCallbackMethod != null)
                            unblockUserCallbackMethod(true);

                        break;

                    case 400201:
                        //User not found
                        Debug.LogError("USER NOT FOUND....");
                        if (unblockUserCallbackMethod != null)
                            unblockUserCallbackMethod(false);
                        break;
                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
                if (unblockUserCallbackMethod != null)
                    unblockUserCallbackMethod(false);
            }

            unblockUserCallbackMethod = null;
        }

        public void GetBlockList (string userId, BlockUserListCallbackMethod blockListUserCallback)
        {
            blockUserListCallbackMethod = blockListUserCallback;

            Debug.Log("Unblock User");
            FreakApi api = new FreakApi("user.blockList", ServerResponseBlockList);
            api.param("userId", userId);
            api.get(this);
        }

        void ServerResponseBlockList (IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
        {
            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::

                        IDictionary responseMsgDict = (IDictionary)output["responseMsg"];
                        IList userList = (IList)responseMsgDict["users"];

                        List<FreakChatSerializeClass.BlockedUserList> blockedUserList = new List<FreakChatSerializeClass.BlockedUserList>();

                        for (int i = 0; i < userList.Count; i++)
                        {
                            FreakChatSerializeClass.BlockedUserList blockerUser = new FreakChatSerializeClass.BlockedUserList();
                            blockerUser = (FreakChatSerializeClass.BlockedUserList)
                                JsonUtility.FromJson<FreakChatSerializeClass.BlockedUserList>(MiniJSON.Json.Serialize(userList[i]));

                            blockedUserList.Add(blockerUser);
                        }

                        if (blockUserListCallbackMethod != null)
                            blockUserListCallbackMethod(blockedUserList);

                        break;

                    case 400201:
                        //User not found
                        Debug.LogError("USER NOT FOUND....");
                        if (blockUserListCallbackMethod != null)
                            blockUserListCallbackMethod(null);
                        break;
                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if (blockUserListCallbackMethod != null)
                            blockUserListCallbackMethod(null);
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
                if (blockUserListCallbackMethod != null)
                    blockUserListCallbackMethod(null);
            }

            unblockUserCallbackMethod = null;
        }

        public void UploadFileToServer (byte[] bytes, System.Action<string> callback)
        {
            WWWForm form = new WWWForm();
            form.AddField("methodName", "testFile.upload");
            form.AddField("userId", FreakAppManager.Instance.userID);
            form.AddField("accessToken", FreakAppManager.Instance.APIkey );
            form.AddField ("applicationKey", "12345");
            form.AddBinaryData("avatar", bytes);

            WWW www = new WWW(FreakAppManager.baseURL, form);

            StartCoroutine(UploadFileCallBack(www, callback));
        }

        IEnumerator UploadFileCallBack (WWW www, System.Action<string> callback)
        {
            yield return www;

            Debug.Log("UploadFileCallBack:: " + www.text);
            Debug.Log("UploadFileCallBack>> " + www.error);
        }
    }
}
