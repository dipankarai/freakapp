﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    [System.Serializable]
    public class ChatBaseMessage
    {
        public enum ChatMessageType
        {
            UserMessage,
            FileMessage,
            AdminMessage
        }

        //Common
        public string Data;
        public ChatUser Sender;
        public string RequestId = string.Empty;
        public string CustomType = string.Empty;
        public ChatMessageType chatMessageType;

        //UserMesage
        public string Message = string.Empty;
        public Dictionary<string, string> Translations = new Dictionary<string, string>();

        //FileMessage
        public string fileUrl;
        public string fileName;
        public int fileSize;
        public string fileType;

        //BaseMessage
        public long MessageId;
        public long CreatedAt;
        public string CoverUrl;
        public string Name;
        public string Url;

        public bool IsGroupChannel;
        public bool IsOpenChannel;

        public FreakChatSerializeClass.NormalMessagerMetaData metaData;

        public ChatMessage.MessageReceiptType receiptStatus = ChatMessage.MessageReceiptType.Unknown;

        public ChatBaseMessage ()
        {
//            Debug.Log ("ChatBaseMessage");
        }

        public ChatBaseMessage (SendBird.BaseMessage baseMessage)
        {
            Url = baseMessage.ChannelUrl;
            CreatedAt = baseMessage.CreatedAt;
            IsGroupChannel = baseMessage.IsGroupChannel();
            IsGroupChannel = baseMessage.IsOpenChannel();
            MessageId = baseMessage.MessageId;

            if (baseMessage is SendBird.UserMessage)
            {
                SendBird.UserMessage userMessage = baseMessage as SendBird.UserMessage;
                chatMessageType = ChatMessageType.UserMessage;

                CustomType = userMessage.CustomType;
                Data = userMessage.Data;
                Message = userMessage.Message;
                RequestId = userMessage.RequestId;
                Sender = new ChatUser (userMessage.Sender);
                Translations = userMessage.Translations;
            }
            else if (baseMessage is SendBird.FileMessage)
            {
                chatMessageType = ChatMessageType.FileMessage;

                SendBird.FileMessage userMessage = baseMessage as SendBird.FileMessage;
                CustomType = userMessage.CustomType;
                Data = userMessage.Data;
                fileName = userMessage.Name;
                RequestId = userMessage.RequestId;
                Sender = new ChatUser (userMessage.Sender);
                fileSize = userMessage.Size;
                fileType = userMessage.Type;
                fileUrl = userMessage.Url;
            }
            else
            {
                chatMessageType = ChatMessageType.AdminMessage;
            }

            if (string.IsNullOrEmpty(Data) == false)
            {
                InitData();
            }
        }

        void InitData ()
        {
            if (Data.Contains("messageType")  && Data.Contains("fileType"))
            {
                if (MessageId == 728415135)
                {
                    Data = "";
                }
                else
                {
                    try
                    {
                        metaData = (FreakChatSerializeClass.NormalMessagerMetaData) Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(Data);
                    }
                    catch (System.Exception e)
                    {
//                        Debug.LogError (Data);
//                        Debug.LogError (e.Message);
                        Data = "";
                    }
                }
            }

        }
    }
}
