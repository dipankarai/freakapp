﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class FreakChatSerializeClass
    {
        [Serializable]
        public class BlockedUser
        {
            public List<string> blockedUserIDList;

            public BlockedUser ()
            {
                blockedUserIDList = new List<string>();
            }

            public bool IsBlocked (string userid)
            {
                for (int i = 0; i < blockedUserIDList.Count; i++)
                {
                    if (blockedUserIDList[i] == userid)
                    {
                        return true;
                    }
                }

                return false;
            }

            public void AddUser(string userid)
            {
                if (blockedUserIDList.Contains(userid) == false)
                {
                    blockedUserIDList.Add(userid);
                }
            }

            public void RemoveUser(string userid)
            {
                if (blockedUserIDList.Contains(userid) == true)
                {
                    blockedUserIDList.Remove(userid);
                }
            }
        }

        [Serializable]
        public class MuteUsers
        {
            public List<string> muteUserIDList = new List<string>();

            public MuteUsers ()
            {
                muteUserIDList = new List<string>();
            }

            public bool IsMuted (string userid)
            {
                for (int i = 0; i < muteUserIDList.Count; i++)
                {
                    if (muteUserIDList[i] == userid)
                    {
                        return true;
                    }
                }

                return false;
            }

            public void AddUser(string userid)
            {
                if (muteUserIDList.Contains(userid) == false)
                {
                    muteUserIDList.Add(userid);
                }
            }

            public void RemoveUser(string userid)
            {
                if (muteUserIDList.Contains(userid) == true)
                {
                    muteUserIDList.Remove(userid);
                }
            }
        }

        [Serializable]
        public class BlockedUserList
        {
            public string nickname;
            public long last_seen_at;
            public string user_id;
            public string profile_url;
            public bool is_online;
        }

        [Serializable]
        public class NormalMessagerMetaData
        {
            public enum GameType
            {
                None,
                Challenge,
                Referral
            }

            public ChatMessage.MetaMessageType messageType = ChatMessage.MetaMessageType.None;
            public string message = string.Empty;
            public string userid = string.Empty;
            public string username = string.Empty;

            public ChatFileType.FileType fileType;
            public string filePath = string.Empty;

            public string contactName = string.Empty;
            public string contactNumber = string.Empty;

            public GameType gameType = GameType.None;
            public string gameData = string.Empty;
            public string referralId = string.Empty;
            public int gameId = -1;
            public StoreGames gameStoreInfo;
        }
    }
}
