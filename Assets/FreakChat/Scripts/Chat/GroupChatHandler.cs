﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SendBird;
//using SendBird.Model;
//using SendBird.Query;
using Newtonsoft.Json;

namespace Freak.Chat
{
    public class GroupChatHandler : MonoBehaviour
    {
        
        // Use this for initialization
        public virtual void Start () {
        }


        //You should always use the following Content-Type for all requests.
        //Content-Type: application/json, charset=utf8

        #region Message

        private Action<IDictionary> CreateSingleChannelAPIDirectCallback;
        public void CreateSingleChannelAPIDirect (List<string> user_list, string profile_url, Action<IDictionary> callback)
        {
            CreateSingleChannelAPIDirectCallback = callback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, CreateSingleChannelAPIDirectHandler);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint);
            api.param("method", "POST");

            Dictionary<string, object> _Data = new Dictionary<string, object>();
            _Data.Add("name", "Single_Channel");
            _Data.Add("cover_url", profile_url);
            _Data.Add("custom_type", "");
            _Data.Add("data", "");
            _Data.Add("user_ids", user_list);
            _Data.Add("is_distinct", true);

            string _sendBirdData = MiniJSON.Json.Serialize(_Data);

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(_sendBirdData);
            #endif

            api.param("data", _sendBirdData);
            api.get(this);
        }

        void CreateSingleChannelAPIDirectHandler (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("CreateSingleChannelAPIDirectHandler ==== >>>> " + MiniJSON.Json.Serialize(output));
            #endif

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::

                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            IDictionary userDetailsDict = (IDictionary)output["responseMsg"];

                            if (CreateSingleChannelAPIDirectCallback != null)
                            {
                                CreateSingleChannelAPIDirectCallback(userDetailsDict);
                            }
                        }
                        else
                        {
                            if (CreateSingleChannelAPIDirectCallback != null)
                            {
                                CreateSingleChannelAPIDirectCallback(null);
                            }
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (CreateSingleChannelAPIDirectCallback != null)
                {
                    CreateSingleChannelAPIDirectCallback(null);
                }
            }
        }

        private Action<IDictionary> SendMessageAPIDirectCallback;
        public void SendMessageAPIDirect (string channel_url, string user_id, string message, string data, Action<IDictionary> callback)
        {
            SendMessageAPIDirectCallback = callback;

            string _endPoint = ChatManager.groupChannelEndPoint + channel_url + "/messages";

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("EndPoint: " + _endPoint);
            #endif

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SendMessageAPIDirectHandler);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", _endPoint);
            api.param("method", "POST");

            Dictionary<string, object> _Data = new Dictionary<string, object>();
            _Data.Add("message_type", "MESG");
            _Data.Add("user_id", user_id);
            _Data.Add("message", message);
            _Data.Add("custom_type", "");
            _Data.Add("data", data);
            _Data.Add("mark_as_read", true);

            string _sendBirdData = MiniJSON.Json.Serialize(_Data);

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log(_sendBirdData);
            #endif

            api.param("data", _sendBirdData);
            api.get(this);
        }

        void SendMessageAPIDirectHandler (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessageAPIDirectHandler ==== >>>> " + MiniJSON.Json.Serialize(output));
            #endif

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::

                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            IDictionary userDetailsDict = (IDictionary)output["responseMsg"];

                            if (SendMessageAPIDirectCallback != null)
                            {
                                SendMessageAPIDirectCallback(userDetailsDict);
                            }
                        }
                        else
                        {
                            if (SendMessageAPIDirectCallback != null)
                            {
                                SendMessageAPIDirectCallback(null);
                            }
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (SendMessageAPIDirectCallback != null)
                {
                    SendMessageAPIDirectCallback(null);
                }
            }
        }
        #endregion

#region New Chnages
        public enum GroupState
        {
            NewGroup = 0,
            GroupIcon,
            GroupName,
            GroupMemberAdd,
            GroupMemberRemoved
        }

        /*public void CreateGroup (GroupSerializableRequestClass.CreateGroup createGroup, GroupDelegate.SuccessCallbackMethod createGroupCallback)
        {
            mSuccessCallback = createGroupCallback;

            string senbirdUrl = ChatManager.groupChannelEndPoint;

            WWWForm wwwForm = new WWWForm();
            wwwForm.AddField("name", createGroup.name);
            wwwForm.AddField("data", createGroup.data);
            wwwForm.AddField("user_ids", createGroup.User_IDs);
            wwwForm.AddField ("is_distinct", createGroup.is_distinct.ToString ());
			if (createGroup.groupImageByte != null) {
				wwwForm.AddBinaryData ("cover_file", createGroup.groupImageByte);
			}

            Dictionary<string,string> headers = wwwForm.headers;
            headers["Api-Token"] = ChatManager.sendbirdAPIToken;

            byte[] rawData = wwwForm.data;

            WWW www = new WWW(senbirdUrl, rawData, headers);

            StartCoroutine(CreateGroupRoutine(www, GroupState.NewGroup));
        }*/

        /*public void UpdateGroupCover (string channelUrl, GroupState state, GroupSerializableRequestClass.CreateGroup createGroup, GroupDelegate.SuccessCallbackMethod createGroupCallback)
        {
            mSuccessCallback = createGroupCallback;

            string senbirdUrl = ChatManager.groupChannelEndPoint + "/" + channelUrl;

            WWWForm wwwForm = new WWWForm();
            wwwForm.AddField("name", createGroup.name);
            wwwForm.AddField("data", createGroup.data);
            wwwForm.AddField("user_ids", createGroup.User_IDs);
            wwwForm.AddField("is_distinct", createGroup.is_distinct.GetHashCode());
            wwwForm.AddBinaryData("cover_file", createGroup.groupImageByte);

            Dictionary<string,string> headers = wwwForm.headers;
            headers["Api-Token"] = ChatManager.sendbirdAPIToken;

            byte[] rawData = wwwForm.data;

            WWW www = new WWW(senbirdUrl, rawData, headers);

            StartCoroutine(CreateGroupRoutine(www, state));
        }*/

        /*IEnumerator CreateGroupRoutine (WWW www, GroupState state)
        {
            Debug.Log("CreateGroupCallback");
            yield return www;
            if(www.error == null)
            {
                Debug.Log("Group Created " + www.text);
                if (www.text.Contains("Error") == false)
                {
                    switch (state)
                    {
                        case GroupState.NewGroup:
                            {
                                IDictionary groupChannelDict = (IDictionary)MiniJSON.Json.Deserialize(www.text);
                                ChatManager.Instance.GetChannelByChannelUrl(groupChannelDict["channel_url"].ToString(),GetGroupChannelCallback);
                            }
                            break;
                    }
                }
                else
                {
                    mSuccessCallback(false);
                }
            }
            else
            {
                Debug.LogError(www.error);
                Debug.LogError(www.text);
                mSuccessCallback(false);
            }
        }*/

        /*void GetGroupChannelCallback (SendBird.GroupChannel channel)
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.SystemNewGroup;
            _metaData.message = ChatManager.Instance.UserName + FreakAppConstantPara.MessageDescription.createdNewGroup;

            string _data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
            ChatManager.Instance.SendUserMessage (channel, FreakAppConstantPara.MessageDescription.newGroup, _data, string.Empty, MessageSend);
        }*/

        void MessageSend ()
        {
            mSuccessCallback(true);
        }

        #endregion

#region Create, Update GROUP
        GroupDelegate.CreateGroupCallbackMethod mCreateGroupCallback;

        public void CreateGroup (GroupSerializableRequestClass.CreateGroup createGroup, GroupDelegate.CreateGroupCallbackMethod createGroupCallback)
        {
            mCreateGroupCallback = createGroupCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, CreateGroupCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint);
            api.param("method", "POST");
            string _sendBirdData = JsonConvert.SerializeObject(createGroup);
            api.param("data", _sendBirdData);

            GroupSerializableRequestClass.UploadFile _UploadFile = new GroupSerializableRequestClass.UploadFile();
            _UploadFile.cover_file = createGroup.cover_file;

            api.param("upload_files", JsonConvert.SerializeObject(_UploadFile));
            api.get(this);

            Debug.Log("Create Group Data "+_sendBirdData);
        }

        public void UpdateGroup (string channelUrlId, GroupSerializableRequestClass.UpdateRequest updateRequest, GroupDelegate.CreateGroupCallbackMethod updateGroupCallback)
        {
            mCreateGroupCallback = updateGroupCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, CreateGroupCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint +"/"+ channelUrlId );
            api.param("method", "PUT");
            string _sendBirdData = JsonConvert.SerializeObject(updateRequest);
            api.param("data", _sendBirdData);
            api.get(this);

            Debug.Log("Update Group Data "+_sendBirdData);
        }

        void CreateGroupCallBack (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            Debug.Log("ChannelCallBack ==== >>>> " + MiniJSON.Json.Serialize(output));

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::

                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            IDictionary userDetailsDict = (IDictionary)output["responseMsg"];
                            string _groupChannelString = JsonConvert.SerializeObject(output["responseMsg"]);
                            GroupSerializableResponseClass.GroupChannel _groupChannel =
                                (GroupSerializableResponseClass.GroupChannel)JsonConvert.DeserializeObject<GroupSerializableResponseClass.GroupChannel>(_groupChannelString);
                            
                            mCreateGroupCallback(_groupChannel);
                        }
                        else
                        {
                            if (mCreateGroupCallback != null)
                            {
                                mCreateGroupCallback(null);
                            }
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if (mCreateGroupCallback != null)
                        {
                            mCreateGroupCallback(null);
                        }
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (mCreateGroupCallback != null)
                {
                    mCreateGroupCallback(null);
                }
            }

//            mCreateGroupCallback = null;
        }

#endregion

#region List Group
        GroupDelegate.ChannelDetailsCallbackMethod mChannelDetailsGroupCallback;

        public void ListGroupChannels (GroupSerializableRequestClass.ListGroup listGroup, GroupDelegate.ChannelDetailsCallbackMethod listGroupCallback)
        {
            mChannelDetailsGroupCallback = listGroupCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, ChannelDetailsCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint);
            api.param("method", "GET");
            string _sendBirdData = JsonConvert.SerializeObject(listGroup);
            api.param("data", _sendBirdData);
            api.get(this);

            Debug.Log("List Group Data "+_sendBirdData);
        }

        void ChannelDetailsCallBack (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            Debug.Log("ChannelDetailsCallBack ==== >>>> " + MiniJSON.Json.Serialize(output));

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::
                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            IDictionary userDetailsDict = (IDictionary)output["responseMsg"];
                            string _groupChannelString = MiniJSON.Json.Serialize(output["responseMsg"]);
                            GroupSerializableResponseClass.ChannelList _groupChannel = JsonConvert.DeserializeObject<GroupSerializableResponseClass.ChannelList>(_groupChannelString);
                            
                            mChannelDetailsGroupCallback(_groupChannel);
                        }
                        else
                        {
                            mChannelDetailsGroupCallback(null);
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if(mChannelDetailsGroupCallback != null)
                        {
                            mChannelDetailsGroupCallback(null);
                        }
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if(mChannelDetailsGroupCallback != null)
                {
                    mChannelDetailsGroupCallback(null);
                }
            }

//            mChannelDetailsGroupCallback = null;
        }

#endregion

#region Delete Group

        public void DeleteChannel (string channelUrlId, GroupDelegate.SuccessCallbackMethod deleteChannelCallback)
        {
            Debug.Log ("Delete Group " + channelUrlId);

            mSuccessCallback = deleteChannelCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint + channelUrlId);
            api.param("method", "DELETE");
            api.get(this);

        }


#endregion

#region Get Member List
        GroupDelegate.MemberListCallbackMethod mMemberListCallback;

        public void GetMemberList (string channelUrlId, GroupSerializableRequestClass.GetMember getMember, GroupDelegate.MemberListCallbackMethod getMemberChannelCallback)
        {
            mMemberListCallback = getMemberChannelCallback;
            
            FreakApi api = new FreakApi(ChatManager.apiServerMethod, GetMemberListCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint + "/" +channelUrlId + "/members" );
            api.param("method", "GET");
            api.param("data", JsonConvert.SerializeObject(getMember));
            api.get(this);
            
            Debug.Log("GetMemberList " + JsonConvert.SerializeObject(getMember));
        }

        void GetMemberListCallBack (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            Debug.Log("ChannelCallBack ==== >>>> " + MiniJSON.Json.Serialize(output));

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::
                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            string _memberListString = JsonConvert.SerializeObject(output["responseMsg"]);
                            GroupSerializableResponseClass.MemberList _memberList = 
                                (GroupSerializableResponseClass.MemberList)JsonConvert.DeserializeObject<GroupSerializableResponseClass.MemberList>(_memberListString);
                            
                            if (mMemberListCallback != null)
                            {
                                mMemberListCallback(_memberList);
                            }
                        }
                        else
                        {
                            if (mMemberListCallback != null)
                            {
                                mMemberListCallback(null);
                            }
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if (mMemberListCallback != null)
                        {
                            mMemberListCallback(null);
                        }
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (mMemberListCallback != null)
                {
                    mMemberListCallback(null);
                }
            }

//            mMemberListCallback = null;
        }

        public void IsMember (string channelUrlId, string userid, GroupDelegate.SuccessCallbackMethod successCallback)
        {
            mSuccessCallback = successCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, IsMemberCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint + "/" + channelUrlId + "/members/" + userid);
            api.param("method", "GET");
            api.get(this);

            Debug.Log("IsMember Group");
        }

        void IsMemberCallBack (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            Debug.Log("ChannelCallBack ==== >>>> " + MiniJSON.Json.Serialize(output));

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::
                        if (HasError((IDictionary)output["responseMsg"]) == false)
                        {
                            IDictionary responseMsgDict = (IDictionary)output["responseMsg"];
                            bool _ismember = (bool)responseMsgDict["is_member"];
                            if (mSuccessCallback != null)
                            {
                                mSuccessCallback(_ismember);
                            }
                        }
                        else
                        {
                            if (mSuccessCallback != null)
                            {
                                mSuccessCallback(false);
                            }
                        }

                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if (mSuccessCallback != null)
                        {
                            mSuccessCallback(false);
                        }
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (mSuccessCallback != null)
                {
                    mSuccessCallback(false);
                }
            }

//            mSuccessCallback = null;
        }

        public void ViewChannelInformation(string channelUrlId, GroupSerializableRequestClass.ViewRequest viewRequest, GroupDelegate.SuccessCallbackMethod successCallback)
        {
            mSuccessCallback = successCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint + "/" + channelUrlId);
            api.param("method", "GET");

            string _sendBirdData = JsonConvert.SerializeObject(viewRequest);
            api.param("data", _sendBirdData);

            api.get(this);

            Debug.Log("View Channel Information");
        }

#endregion

#region Invite, Hide and Leave Users

        public void InviteUsers (string channelUrlId, GroupSerializableResponseClass.UserIdList userIdArray, GroupDelegate.MemberListCallbackMethod inviteUserCallback)
        {
            Debug.Log("Invite Users. Channel: " + channelUrlId + " Users: " + MiniJSON.Json.Serialize(userIdArray));

            mMemberListCallback = inviteUserCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, GetMemberListCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint +"/"+ channelUrlId + "/invite" );
            api.param("method", "POST");
            api.param("data", JsonConvert.SerializeObject(userIdArray));
            api.get(this);
        }

        public void HideUser (string channelUrlId, string userId, GroupDelegate.SuccessCallbackMethod hideUserCallback)
        {
            Debug.Log("Hide Users. Channel: " + channelUrlId + " Users: " + userId);

            mSuccessCallback = hideUserCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint +"/"+ channelUrlId + "/hide" );
            api.param("method", "PUT");
            api.get(this);
        }

        public void LeaveCurrentChannel (string channelUrlId, GroupSerializableRequestClass.LeaveGroup userId, GroupDelegate.SuccessCallbackMethod leaveChannelCallback)
        {
            /*{"responseCode":1,"responseMsg":[],"responseInfo":"Everything worked as expected"}*/

            Debug.Log("LeaveChannel Users. Channel: " + channelUrlId + " Users: " + JsonConvert.SerializeObject(userId));

            mSuccessCallback = leaveChannelCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.groupChannelEndPoint +"/"+ channelUrlId + "/leave" );
            api.param("method", "PUT");
            api.param("data", JsonConvert.SerializeObject(userId));
            api.get(this);
        }

#endregion

#region Delete Message

        public void DeleteMessage (bool isGroupMsg, string channelUrl, long messageId, GroupDelegate.SuccessCallbackMethod successCallback)
        {
            Debug.Log("Delete Messages. Channel: " + channelUrl + " MessageId: " + messageId);
            mSuccessCallback = successCallback;

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            if (isGroupMsg)
            {
                api.param("end_point", ChatManager.groupChannelEndPoint +"/"+ channelUrl + "/messages/" + messageId );
            }
            else
            {
                api.param("end_point", ChatManager.openChannelEndPoint +"/"+ channelUrl + "/messages/" + messageId );
            }
            api.param("method", "DELETE");
            api.get(this);
        }

#endregion

#region Common Method
        GroupDelegate.SuccessCallbackMethod mSuccessCallback;

        void SuccessCallBack (IDictionary output, string resultText, string error, int responseCode, IDictionary inCustomData)
        {
            Debug.Log("ChannelCallBack ==== >>>> " + MiniJSON.Json.Serialize(output));

            //Check for errors
            if (output!=null)
            {
                string responseInfo =(string)output["responseInfo"];

                switch(responseCode){
                    case 001:
                        //Response Success:::
                        if (JsonConvert.SerializeObject(output["responseMsg"]) == "[]")
                        {
                            if (mSuccessCallback != null)
                            {
                                mSuccessCallback(true);
                            }
                        }
                        else
                        {
                            IDictionary userDetailsDict = (IDictionary)output["responseMsg"];
                            if (HasError(userDetailsDict) == false)
                            {
                                if (mSuccessCallback != null)
                                {
                                    mSuccessCallback(true);
                                }
                            }
                            else
                            {
                                if (mSuccessCallback != null)
                                {
                                    mSuccessCallback(false);
                                }
                            }
                        }
                        break;

                    default:
                        //Show Popup to let user know if anything has gone wrong while login in...
                        if (mSuccessCallback != null)
                        {
                            mSuccessCallback(false);
                        }
                        break;
                }   
            } 
            else {
                Debug.LogError("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + resultText);
                if (mSuccessCallback != null)
                {
                    mSuccessCallback(false);
                }
            }

//            mSuccessCallback = null;
        }

        bool HasError (IDictionary respongeMsg)
        {
            string _msg = JsonConvert.SerializeObject(respongeMsg);
            GroupSerializableResponseClass.ErrorMessage _ErrorMessage
            = (GroupSerializableResponseClass.ErrorMessage)JsonConvert.DeserializeObject<GroupSerializableResponseClass.ErrorMessage>(_msg);

            if (_ErrorMessage != null)
            {
                if (_ErrorMessage.error == true)
                {
                    Debug.LogError(_ErrorMessage.message + " ErrorCode: " + _ErrorMessage.code);
                }
                return _ErrorMessage.error;
            }

            return false;
        }

#endregion

        public void SendMessageInGroup (string channelUrlId, GroupSerializableRequestClass.SendMessage sendMessage)
        {
            Debug.Log("Invite Users. Channel: " + channelUrlId + " Users: " + MiniJSON.Json.Serialize(sendMessage));

            FreakApi api = new FreakApi(ChatManager.apiServerMethod, SuccessCallBack);
            api.param("userId", ChatManager.Instance.UserId);
            api.param("end_point", ChatManager.openChannelEndPoint +"/"+ channelUrlId + "/messages" );
            api.param("method", "POST");
            api.param("data", JsonConvert.SerializeObject(sendMessage));
            api.get(this);
        }

        public class GroupDelegate
        {
            public delegate void CreateGroupCallbackMethod (GroupSerializableResponseClass.GroupChannel groupChannel);
            public delegate void ListGroupCallbackMethod ();

            public delegate void ChannelDetailsCallbackMethod (GroupSerializableResponseClass.ChannelList channel);
            public delegate void ChannelCallbackMethod (string channel);
            public delegate void SuccessCallbackMethod (bool succeess);
            public delegate void MemberListCallbackMethod (GroupSerializableResponseClass.MemberList memberList);
        }
    }

    [Serializable]
    public class GroupSerializableResponseClass
    {
        [Serializable]
        public class ChannelList
        {
            public List<GroupChannel> channels;
            public string next;
        }

        [Serializable]
        public class GroupChannel
        {
            public string name;
            public string channel_url;
            public string cover_url;
            public string data;
            public bool is_distinct;
            public int member_count;
            public int max_length_message;
            public int unread_message_count;
            public long created_at;

            public object last_message;
            public List<ChannelMember> members;
            public Channel channel;
        }

        [Serializable]
        public class ChannelMember
        {
            public string nickname;
            public string user_id;
            public string profile_url;
            public long last_seen_at;
            public bool is_online;
        }

        [Serializable]
        public class Channel
        {
            public string channel_url;
            public string data;
            public string name;
            public string cover_url;
            public int member_count;
            public int max_length_message;
            public long created_at;
        }

        [Serializable]
        public class MemberList
        {
            public List<ChannelMember> members =  new List<ChannelMember>();
            public string next;
        }

        [Serializable]
        public class UserIdList
        {
            public List<string> user_ids;
        }

        [Serializable]
        public class MetaData
        {
            public string createdBy;
            public List<string> adminList;
            public string profileFilename;

            public MetaData (string userid)
            {
                createdBy = userid;
                adminList = new List<string>();
                adminList.Add(userid);
            }

            public void ChangeMainAdmin (string newAdmin)
            {
                adminList.Remove(createdBy);
                createdBy = newAdmin;
                adminList.Add(createdBy);
            }

            bool IsAdmin (string id)
            {
                for (int i = 0; i < adminList.Count; i++)
                {
                    if (adminList[i] == id)
                        return true;
                }

                return false;
            }

        }

        [Serializable]
        public class ErrorMessage
        {
            public string message;
            public int code;
            public bool error;
        }
    }

    [Serializable]
    public class GroupSerializableRequestClass
    {
        [Serializable]
        public class CreateGroup
        {
            public string name = string.Empty ;           // (Optional) Topic
            public string cover_file = string.Empty;       // (Optional) Cover Image Url
            public string data = string.Empty;            // (Optional) Custom Channel Data
            public readonly List<string> user_ids;       // (Optional) Be invited user_ids 
            public bool is_distinct = false;    // (Optional) Create
            public byte[] groupImageByte;
            public string User_IDs
            {
                get
                {
                    return string.Join(",", user_ids.ToArray());
                }
            }

            public List<string> useridList
            {
                set
                {
                    for (int i = 0; i < value.Count; i++)
                    {
                        if (user_ids.Contains(value[i]))
                            continue;

                        user_ids.Add(value[i]);
                    }
                }
                get
                {
                    return user_ids;
                }
            }
            /// <summary>
            /// Initiliazing the Group ID
            /// </summary>
            /// <param name="Chat ID">string Enter User chat ID.</param>
            public CreateGroup(string yourID)
            {
                user_ids = new List<string>();
                user_ids.Add(yourID);
            }
        }

        [Serializable]
        public class UploadFile
        {
            public string cover_file;
        }

        [Serializable]
        public class UpdateRequest
        {
            /// <summary>
            /// (Optional)New Name
            /// </summary>
            public string name = string.Empty;
            /// <summary>
            /// (Optional)New Cover Image Url
            /// </summary>
            public string cover_url = string.Empty;
            /// <summary>
            /// (Optional)New Custom Data
            /// </summary>
            public string data;
            /// <summary>
            /// (Optional) Create distinct channel option
            /// </summary>
            public bool is_distinct;
        }

        [Serializable]
        public class ListGroup
        {
            /*token=string(Optional)
             * limit=int(Optional)
             * &member=boolean(Optional)
             * &order=string(Optional)
             * &show_empty=boolean(Optional)*/

            /// <summary>
            /// (Optional)
            /// If you pass a empty string, a first page of list is returned in a response. As long as you pass the token, you will explore the next page of current page. Token will be only valid for one hour.
            /// </summary>
            public string token = string.Empty;
            /// <summary>
            /// (Required)
            /// Search channels including given user.
            /// </summary>
            public string user_id;
            /// <summary>
            /// (Optional)
            /// Number of users to be displayed per page. This value must be between 1 and 100. default: 10
            /// </summary>
            public int limit = 0;
            /// <summary>
            /// (Optional)
            /// Whether member is included in response body or not. default: false
            /// </summary>
            public bool member = false;
            /// <summary>
            /// (Optional)
            /// If this value is latest_last_message, sorted list by latest time of last message. This value must be chronological or latest_last_message. default: chronological
            /// </summary>
            public string order;
            /// <summary>
            /// (Optional)
            /// If this value is true, include never used channel. default: false
            /// </summary>
            public bool show_empty = false;
        }

        [Serializable]
        public class GetMember
        {
            /// <summary>
            /// (Optional) If you pass a empty string, a first page of list is returned in a response. As long as you pass the token, you will explore the next page of current page. Token will be only valid for one hour.
            /// </summary>
            public string token = string.Empty;
            /// <summary>
            /// (Optional)
            /// </summary>
            public int limit = 0;
        }

        [Serializable]
        public class ViewRequest
        {
            /// <summary>
            /// (Optional)
            /// Whether member is included in response body or not. default: false
            /// </summary>
            public bool member;
            /// <summary>
            /// (Optional)
            /// Whether read receipt is included in response body or not. default: false
            /// </summary>
            public bool read_receipt; // (Optional);
            /// <summary>
            /// (Optional)
            /// Retrieve unread count and last message about given user. The user_id should be urlencoded. e.g.) user_id=urlencoded_user_id
            /// </summary>
            public string user_id; // (Optional)
        }
    
        [Serializable]
        public class LeaveGroup
        {
            public List<string> user_ids = new List<string>();
        }

        [Serializable]
        public class SendMessage
        {
            public string message_type = "MESG";
            public string  user_id;
            public string  message;
            public string  data;
            public bool  mark_as_read;
        }
    }
}
