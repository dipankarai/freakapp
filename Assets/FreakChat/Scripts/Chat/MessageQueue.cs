﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class MessageQueue : MonoBehaviour
    {
        public string ChannelUrl
        {
            get { return mChannelUrl; }
        }

        private string mChannelUrl;
        private SendBird.BaseChannel mBaseChannel;
        private List<MessageQueueHandler.AllMessageQueue> mMessageQueue = new List<MessageQueueHandler.AllMessageQueue> ();
        private List<MessageQueueHandler.AllMessageQueue> sendMessageQueue = new List<MessageQueueHandler.AllMessageQueue> ();

        void OnEnable ()
        {
            ChatManager.SendBirdLogin += OnSendBirdLogin;
        }

        void OnDisable ()
        {
            ChatManager.SendBirdLogin -= OnSendBirdLogin;
        }

        void Start ()
        {
            StartCoroutine(UpdateRoutine());
        }

        void OnSendBirdLogin (bool login)
        {
            if (login)
            {
                InvokeSendMessage();
            }
        }

        IEnumerator UpdateRoutine ()
        {
            while (true)
            {
                if (ChatManager.Instance.isInternetConnected && ChatManager.Instance.IsSeandbirdLogined)
                {
                    if (mMessageQueue.Count > 0)
                    {
                        SendMessage();
                    }
                }
                yield return null;
            }
        }

        public void AddInQueue (MessageQueueHandler.AllMessageQueue queue)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("AddInQueue " + queue.customType);
            #endif

            if ((string.IsNullOrEmpty (mChannelUrl) == false) && (mChannelUrl != queue.channelUrl))
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("DIFFERENT URL " + mChannelUrl + " :::: " + queue.channelUrl);
                #endif
            }
            mChannelUrl = queue.channelUrl;

            if (CheckForPreviousMessage(queue.customType) == false)
            {
                mMessageQueue.Add(queue);
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("AddInQueue " + mMessageQueue.Count);
            #endif
        }

        bool CheckForPreviousMessage (string customType)
        {
            for (int i = 0; i < mMessageQueue.Count; i++)
            {
                if (mMessageQueue[i].customType == customType)
                {
                    return true;
                }
            }

            for (int i = 0; i < sendMessageQueue.Count; i++)
            {
                if (sendMessageQueue[i].customType == customType)
                {
                    return true;
                }
            }

            return false;
        }

        private bool isSendingMessage;
        private float elapsedTime;
        public void SendMessage ()
        {
            if (isSendingMessage)
            {
                /*#if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("SendMessage" + isSendingMessage);
                #endif
                if (elapsedTime == 0)
                {
                    elapsedTime = Time.time;
                }
                else if ((elapsedTime + 600f) < Time.time)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log(Time.time + "elapsedTime" + elapsedTime);
                    #endif
                    Invoke("InvokeSendMessage", 1f);
                }*/
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessage" + isSendingMessage);
            #endif
            isSendingMessage = true;

            Send();
            //StartCoroutine(SendMessageCoroutine());
        }

        IEnumerator SendMessageCoroutine ()
        {
            float elapsedTime = 0f;
            bool timeOver = false;
            while (ChatManager.Instance.IsSeandbirdLogined == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("SendMessageCoroutine 1 " + ChatManager.Instance.IsSeandbirdLogined);
                #endif
                yield return null;
                if (ChatManager.Instance.IsSeandbirdLogined)
                    break;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessageCoroutine 1 " + ChatManager.Instance.IsSeandbirdLogined);
            #endif

            while (ChatManager.Instance.mGroupChannelListQuery.IsLoading())
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("SendMessageCoroutine 2 " + ChatManager.Instance.mGroupChannelListQuery.IsLoading());
                #endif
                yield return null;
                if (ChatManager.Instance.mGroupChannelListQuery.IsLoading() == false)
                    break;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessageCoroutine 2 " + ChatManager.Instance.mGroupChannelListQuery.IsLoading());
            #endif

            if (timeOver)
            {
                InvokeSendMessage();
            }
            else
            {
                Send();
            }
        }

        float timeInternetDisconnect = 0;
        void Send ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("Send>>>>> " + ChatManager.Instance.isInternetConnected);
            #endif

            if (ChatManager.Instance.isInternetConnected == false || ChatManager.Instance.IsSeandbirdLogined == false)
            {
//                if (timeInternetDisconnect == 0)
//                    timeInternetDisconnect = Time.time;
//                
                Invoke("InvokeSendMessage", 1f);
                return;
            }

            /*if ((timeInternetDisconnect + 4f) < Time.time)
            {
                timeInternetDisconnect = 0;
                ChatManager.Instance.RefreshForNewMessages(InvokeSendMessage);
            }*/

            if (mMessageQueue[0].baseChannel != null)
            {
                if (mMessageQueue[0] is MessageQueueHandler.MessageSendQueue)
                {
                    MessageQueueHandler.MessageSendQueue _allMsg = mMessageQueue[0] as MessageQueueHandler.MessageSendQueue;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("SendMessage " + _allMsg.msg);
                    #endif
                    //                ChatManager.Instance.SendUserMessageNew(_allMsg.baseChannel, _allMsg.msg, _allMsg.data, _allMsg.customType, _allMsg.callback);
                    SendUserMessage(_allMsg.baseChannel, _allMsg.msg, _allMsg.data, _allMsg.customType);
                }
                else if (mMessageQueue[0] is MessageQueueHandler.FileSendQueue)
                {
                    MessageQueueHandler.FileSendQueue _allMsg = mMessageQueue[0] as MessageQueueHandler.FileSendQueue;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log("SendMessage " + _allMsg.name);
                    #endif
                    if (_allMsg.fileInfo == null)
                    {
//                        ChatManager.Instance.SendFileMessageWithUrlNew (_allMsg.baseChannel, _allMsg.fileUrl, _allMsg.name, _allMsg.type, _allMsg.size, _allMsg.data, _allMsg.customType);
                        SendFileMessageWithUrl (_allMsg.baseChannel, _allMsg.fileUrl, _allMsg.name, _allMsg.type, _allMsg.size, _allMsg.data, _allMsg.customType);
                    }
                    else
                    {
//                        ChatManager.Instance.SendFileMessageNew (_allMsg.baseChannel, _allMsg.fileInfo, _allMsg.customType, _allMsg.data);
                        SendFileMessage (_allMsg.baseChannel, _allMsg.fileInfo, _allMsg.customType, _allMsg.data);
                    }
                }
            }
            else
            {
                if (mBaseChannel == null)
                {
                    ChatManager.Instance.GetChannelByChannelUrl(mChannelUrl, GetChannelByChannelUrlCallback);
                }
                else
                {
                    if (mBaseChannel.Url == mChannelUrl)
                    {
                        mMessageQueue[0].baseChannel = mBaseChannel;
                        InvokeSendMessage();
                    }
                }
            }
        }

        void InvokeSendMessage ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("InvokeSendMessage " + isSendingMessage);
            #endif

            isSendingMessage = false;
        }

        void SendUserMessage (SendBird.BaseChannel baseChannel, string msg, string data = "", string customType = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendUserMessage " + msg);
            #endif

            try
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("MESSAGE "+msg);
                #endif
                baseChannel.SendUserMessage (msg, data, customType, HandleSendUserMessageHandlerNew);
            }
            catch(Exception e)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("error "+e.Message);
                #endif
                Invoke("InvokeSendMessage", 1f);
            }
        }

        void SendFileMessage (SendBird.BaseChannel baseChannel, ChatFileType fileInfo, string customType, string data = "")
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessage " + fileInfo.fileName);
            #endif

            try
            {
                SendBird.SBFile _SendBirdFile = new SendBird.SBFile(fileInfo.filePath);

                baseChannel.SendFileMessage(_SendBirdFile, fileInfo.fileName,
                    ChatFileType.ConvertChatFileTypeToString(fileInfo.fileType), fileInfo.fileSize, data, customType, HandleSendFileMessageHandlerNew);
            }
            catch (Exception e)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("error "+e.Message);
                #endif
                Invoke("InvokeSendMessage", 1f);
            }
        }

        void SendFileMessageWithUrl (SendBird.BaseChannel baseChannel, string fileUrl, string name, string type, int size, string data, string customType)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("SendFileMessageWithUrl " + fileUrl);
            #endif

            try
            {
                baseChannel.SendFileMessageWithURL (fileUrl, name, type, size, data, customType, HandleSendFileMessageHandlerNew);
            }
            catch (Exception e)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError("error "+e.Message);
                #endif
                Invoke("InvokeSendMessage", 1f);
            }
        }

        void HandleSendUserMessageHandlerNew (SendBird.UserMessage message, SendBird.SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("HandleSendUserMessageHandler " + message.CustomType);
                #endif

                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("HandleSendUserMessageHandler error");
                #endif

                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    DiscardAllMesage();
                    return;
                }
                ChatManager.Instance.SendBirdExceptionHandler (e.Code, e.Message);
                Invoke("InvokeSendMessage", 1f);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendUserMessageHandler " + Newtonsoft.Json.JsonConvert.SerializeObject(message));
            #endif

            MesageSend();
            TriggerCallback(message);
        }

        void HandleSendFileMessageHandlerNew (SendBird.FileMessage message, SendBird.SendBirdException e)
        {
            if (e != null)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("HandleSendFileMessageHandler error");
                #endif
                if(e.Code == 900080)
                {
                    FindObjectOfType<AlertMessege> ().ShowAlertWindow (AlertMessege.AlertType.MessegeWithOkButton, "Chat is blocked by User.");
                    DiscardAllMesage();
                    return;
                }
                ChatManager.Instance.SendBirdExceptionHandler (e.Code, e.Message);
                Invoke("InvokeSendMessage", 1f);
                return;
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("HandleSendFileMessageHandler " + Newtonsoft.Json.JsonConvert.SerializeObject(message));
            #endif

            MesageSend();
            TriggerCallback(message);
        }

        void GetChannelByChannelUrlCallback (SendBird.GroupChannel groupChannel)
        {
            if (groupChannel.Url == mChannelUrl)
            {
                mBaseChannel = groupChannel;
            }

            InvokeSendMessage();
        }

        void TriggerCallback (SendBird.BaseMessage message)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("TriggerCallback " + message.ChannelUrl);
            #endif

            for (int i = 0; i < ChatManager.Instance.cachedData.allChannelList.Count; i++)
            {
                if (ChatManager.Instance.cachedData.allChannelList[i].channelUrl == mChannelUrl)
                {
                    ChatChannelHandler.OnMessageReceived(ChatManager.Instance.cachedData.allChannelList[i], message);
                }
            }

            if (ChatManager.Instance.currentSelectedChatChannel.channelUrl == mChannelUrl &&
                ChatManager.Instance.OnMessageSentCallback != null)
            {
                ChatManager.Instance.OnMessageSentCallback (message);
            }

            if (ChatManager.Instance.currentSelectedChatChannel.channelUrl == mChannelUrl &&
                ChatManager.Instance.loadMessageCallback != null)
                ChatManager.Instance.loadMessageCallback();
        }

        public void MesageSend ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("MesageSend " + mMessageQueue[0].customType);
            #endif

            sendMessageQueue.Add(mMessageQueue[0]);
            mMessageQueue.RemoveAt(0);

            if (mMessageQueue.Count == 0)
            {
                Invoke("DestroyAfterSomeTime", 10f);
            }

            InvokeSendMessage();
        }

        void DestroyAfterSomeTime ()
        {
            if (mMessageQueue.Count == 0)
            {
                MessageQueueHandler.Instance.RemovedQueueMessage(this);
            }
        }

        public void DiscardAllMesage ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("MesageSend " + ChannelUrl);
            #endif

            mMessageQueue.Clear();
            MessageQueueHandler.Instance.RemovedQueueMessage(this);
        }
    }
}
