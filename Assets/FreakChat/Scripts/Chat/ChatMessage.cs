﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    [System.Serializable]
    public class ChatMessage
    {
        public enum MessageType
        {
            Message,
            BroadcastMessage,
            SystemMessage,
            FileLink,
            AdminMessage,
            Deleted
        }

        public enum MessageReceiptType
        {
            Unknown = 0,
            Send,
            Delivered,
            Read
        }

        public enum MessageState
        {
            Loading,
            Downloading,
            Retry,
            Normal,
            DownloadSuccess,
            DownloadFail
        }

        public enum MetaMessageType
        {
            None,
            InAppFile,      //While sharing Stickers and Audio Buzz....
            SystemNewGroup,
            TagFriend,
            Contact,
            Update,
            GameInvite
        }

        public enum MessageCategory
        {
            None = 0,
            UserDeactivated = 10400,
            UserBlocked = 10300,
            TooManyMsg = 10200,
            ChannelInvite = 10102,
            ChannelLeave = 10101,
            ChannelJoin = 10100
        }

        #if SB_NON_SER
        //SendBird
        public SendBird.BaseMessage baseMessage;
        #else
//        public Freak.Chat.Serializable.BaseMessage baseMessage;
        public ChatBaseMessage baseMessage;
        #endif

        public string data;
        public string message;
        public MessageType messageType;
        public FreakChatSerializeClass.NormalMessagerMetaData normalMessagerMetaData;
        
        //System Message
        public MessageCategory messageCategory = MessageCategory.None;

        //FileInfo Messages
        public string customField;
        public string fileName;
        public string filePath;
        public ChatFileType.FileType fileType;
        public string fileUrl;
        public int fileSize;

        public MessageReceiptType receiptStatus = MessageReceiptType.Unknown;
//        public bool canShowRetry = true;
        public long messageId;
        public long createdAt;

        public string requestId;
        public ChatUser messageSender;

        public Dictionary<string, string> translations;


        public MessageState messageState = MessageState.Loading;
        public float downloadingValue;
        public bool isFileSaved;
        public byte[] imageByte;
        public long loadingTime;

        [Newtonsoft.Json.JsonIgnore]
        public AudioClip audioClip;
        [Newtonsoft.Json.JsonIgnore]
        public Texture2D imageMessageCache;
    }
}
