﻿using UnityEngine;
using System.Collections;

namespace Freak.Chat
{
    public class ChatMessageTableData
    {
        public enum TableDataType
        {
            None,
            AudioMessageBox,
            AudioBuzzMessageBox,
            ContactMessageBox,
            ImageMessageBox,
            OtherFileMessageBox,
            SystemMessageBox,
            TagFriend,
            TextMessageBox,
            TimeStampBox,
            VideoMessageBox,
            AcceptOrRejectChalleng,
            GameReferralAcceptReject
        }

        public ChatMessage chatMessage;
        public TableDataType tableDataType = TableDataType.None;

		public float cellSize;

        public string GetDataType
        {
            get
            {
                return tableDataType.ToString ();
            }
        }
    }
}
