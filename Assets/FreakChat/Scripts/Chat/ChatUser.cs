﻿using UnityEngine;
using System.Collections;

namespace Freak.Chat
{
    [System.Serializable]
    public class ChatUser
    {
        public enum UserConnectionStatus
        {
            NonAvailable = 0,
            Online = 1,
            Offline = 2
        }

        public string userId;
        public string nickname;
        public string profileUrl;
        public UserConnectionStatus connectionStatus;
        public long lastSeenAt;

        [Newtonsoft.Json.JsonIgnore]
        public Texture2D texture;
        //public byte[] imageCache;

        public ChatUser ()
        {
            
        }

        public ChatUser (SendBird.User userInfo)
        {
            userId = userInfo.UserId;
            nickname = userInfo.Nickname;
            profileUrl = userInfo.ProfileUrl;
            int hashCode = (int) userInfo.ConnectionStatus;
            connectionStatus = (UserConnectionStatus)hashCode;
            lastSeenAt = userInfo.LastSeenAt;
        }
    }
}
