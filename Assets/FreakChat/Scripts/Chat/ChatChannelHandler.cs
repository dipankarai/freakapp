﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class ChatChannelHandler
    {

        private static void InitMembers(ChatChannel chatChannel, List<SendBird.User> members)
        {
            chatChannel.channelMembers = new List<ChatUser>();

            for (int i = 0; i < members.Count; i++)
            {
                ChatUser _chatUser = new ChatUser(members[i]);
                chatChannel.channelMembers.Add(_chatUser);
            }
        }

        public static void UpdateChatChannel (ChatChannel chatChannel, SendBird.GroupChannel groupChannel)
        {
            chatChannel.groupChannel = groupChannel;
            chatChannel.unreadMessageCount = groupChannel.UnreadMessageCount;
            chatChannel.coverUrl = groupChannel.CoverUrl;
            chatChannel.createdAt = groupChannel.CreatedAt;
            chatChannel.data = groupChannel.Data;
            chatChannel.channelName = groupChannel.Name;
            chatChannel.channelUrl = groupChannel.Url;
//            chatChannel.lastMessage = null;

            if (string.IsNullOrEmpty(chatChannel.data) == false)
            {
                try
                {
                    chatChannel.adminMetaData = (GroupSerializableResponseClass.MetaData)
                        Newtonsoft.Json.JsonConvert.DeserializeObject <GroupSerializableResponseClass.MetaData>(chatChannel.data);
                }
                catch (System.Exception e)
                {
                    Debug.LogError (e.Message);
                }
            }

            if (groupChannel.LastMessage != null)
            {
                #if PER_CHECK
                if (chatChannel.lastMessage.MessageId == groupChannel.LastMessage.MessageId)
                {
                chatChannel.unreadMessageCount = 0;
                }
                #else
                if (CheckLastMessage (chatChannel, groupChannel.LastMessage.MessageId))
                {
                    chatChannel.unreadMessageCount = 0;
                }
                #endif

                ChatBaseMessage baseMsg = new ChatBaseMessage(groupChannel.LastMessage);
                chatChannel.lastMessage = baseMsg;
            }

            chatChannel.isOpenChannel = groupChannel.IsOpenChannel();

            if (groupChannel.IsGroupChannel())
            {
                if (ChatManager.IsSingleChannel(groupChannel.Name))
                {
                    if (groupChannel.MemberCount > 2)
                        chatChannel.isGroupChannel = true;
                    else
                        chatChannel.isGroupChannel = false;
                }
                else
                {
                    chatChannel.isGroupChannel = true;
                }
            }

            if (chatChannel.isGroupChannel == false)
            {
                SendBird.User _sender = ChatManager.GetSenderId (groupChannel.Members);

                if (_sender != null)
                {
                    chatChannel.otherUserId = _sender.UserId;
                    chatChannel.otherUserName = _sender.Nickname;
                    chatChannel.lastSeenAt = _sender.LastSeenAt;
                }
            }

            if (chatChannel.channelMembers != null && chatChannel.channelMembers.Count > 0)
            {
                for (int i = 0; i < groupChannel.Members.Count; i++)
                {
                    if (UpdateUser(chatChannel.channelMembers, groupChannel.Members[i]) == false)
                    {
                        ChatUser _chatUser = new ChatUser(groupChannel.Members[i]);
                        chatChannel.channelMembers.Add(_chatUser);
                    }
                }
            }
            else
            {
                chatChannel.channelMembers = new List<ChatUser>();
                
                for (int i = 0; i < groupChannel.Members.Count; i++)
                {
                    ChatUser _chatUser = new ChatUser(groupChannel.Members[i]);
                    chatChannel.channelMembers.Add(_chatUser);
                }
            }

            #if !PER_CHECK
            if (chatChannel.lastMessage != null && chatChannel.unreadMessageCount == 0 && chatChannel.messagesList.Count == 0)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("UpdateChatChannel " + groupChannel.Name);
                #endif
                if (CheckForPreviousMsg(chatChannel, groupChannel.LastMessage) == false)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("UpdateChatChannel ******** " + groupChannel.Name);
                    #endif
                    OnMessageReceived (chatChannel, groupChannel.LastMessage);
                }
            }
            #endif
        }

        private static bool CheckLastMessage (ChatChannel chatChannel, long messageId)
        {
            if (chatChannel.lastMessage != null)
            {
                if(chatChannel.lastMessage.MessageId == messageId && chatChannel.lastMessage.receiptStatus == ChatMessage.MessageReceiptType.Read)
                {
                    return true;
                }
            }

            int count = chatChannel.messagesList.Count > 10 ? 10 : chatChannel.messagesList.Count - 1;
            for (int i = count; i <= 0 ; i++)
            {
                if (chatChannel.messagesList[i].messageId == messageId &&
                    chatChannel.messagesList[i].receiptStatus == ChatMessage.MessageReceiptType.Read)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool UpdateUser (List<ChatUser> members, SendBird.User user)
        {
            for (int i = 0; i < members.Count; i++)
            {
                if (members[i].userId == user.UserId)
                {
                    ChatUser _chatUser = new ChatUser (user);
                    members [i].connectionStatus = _chatUser.connectionStatus;
                    members [i].lastSeenAt = _chatUser.lastSeenAt;
                    members [i].nickname = _chatUser.nickname;

                    /*if (user.UserId != ChatManager.Instance.UserId)
                    {
                        if (members [i].texture == null || (members [i].profileUrl.Equals(user.ProfileUrl) == false))
                        {
                            DownloadManager.Instance.DownloadProfile (user.ProfileUrl, members [i]);
                        }
                    }
                    else*/
                        members [i].profileUrl = _chatUser.profileUrl;

                    return true;
                }
            }
            return false;
        }

        public static ChatChannel InitChatChannel (SendBird.GroupChannel channel)
        {
            ChatChannel chatChannel = new ChatChannel();

            chatChannel.groupChannel = channel;
            chatChannel.unreadMessageCount = channel.UnreadMessageCount;
            chatChannel.coverUrl = channel.CoverUrl;
            chatChannel.createdAt = channel.CreatedAt;
            chatChannel.data = channel.Data;
            chatChannel.channelName = channel.Name;
            chatChannel.channelUrl = channel.Url;

            if (string.IsNullOrEmpty(chatChannel.data) == false)
            {
                try
                {
                    chatChannel.adminMetaData = (GroupSerializableResponseClass.MetaData)
                        Newtonsoft.Json.JsonConvert.DeserializeObject <GroupSerializableResponseClass.MetaData>(chatChannel.data);
                }
                catch (System.Exception e)
                {
                    Debug.LogError (e.Message);
                }
            }

            chatChannel.lastMessage = null;
            if (channel.LastMessage != null)
            {
                ChatBaseMessage baseMsg = new ChatBaseMessage(channel.LastMessage);
                chatChannel.lastMessage = baseMsg;
            }

            chatChannel.isOpenChannel = channel.IsOpenChannel();

            if (channel.IsGroupChannel())
            {
                if (ChatManager.IsSingleChannel(channel.Name))
                {
                    if (channel.MemberCount > 2)
                        chatChannel.isGroupChannel = true;
                    else
                        chatChannel.isGroupChannel = false;
                }
                else
                {
                    chatChannel.isGroupChannel = true;
                }
            }

            if (chatChannel.isGroupChannel == false)
            {
                SendBird.User _sender = ChatManager.GetSenderId (channel.Members);

                if (_sender != null)
                {
                    chatChannel.otherUserId = _sender.UserId;
                    chatChannel.otherUserName = _sender.Nickname;
                    chatChannel.lastSeenAt = _sender.LastSeenAt;
                }
            }

            if (chatChannel.channelMembers != null && chatChannel.channelMembers.Count > 0)
            {
                for (int i = 0; i < channel.Members.Count; i++)
                {
                    if (UpdateUser(chatChannel.channelMembers, channel.Members[i]) == false)
                    {
                        ChatUser _chatUser = new ChatUser(channel.Members[i]);
                        chatChannel.channelMembers.Add(_chatUser);
                    }
                }
            }
            else
            {
                chatChannel.channelMembers = new List<ChatUser>();

                for (int i = 0; i < channel.Members.Count; i++)
                {
                    ChatUser _chatUser = new ChatUser(channel.Members[i]);
                    chatChannel.channelMembers.Add(_chatUser);
                }
            }

            #if !PER_CHECK
            if (chatChannel.lastMessage != null && chatChannel.unreadMessageCount == 0 && chatChannel.messagesList.Count == 0)
            {
                if (CheckForPreviousMsg(chatChannel, channel.LastMessage) == false)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("InitChatChannel ******* " + channel.LastMessage.MessageId);
                    #endif
                    OnMessageReceived (chatChannel, channel.LastMessage);
                }
            }
            #endif

            return chatChannel;
        }

        /*static Freak.Chat.Serializable.BaseMessage ParseMessage (SendBird.BaseMessage baseMessage)
        {
            string seralizeDict = Newtonsoft.Json.JsonConvert.SerializeObject (baseMessage);
            IDictionary message = (IDictionary)CacheManager.GetDictionary (seralizeDict);

            if (message.Contains("Url") && message.Contains("Name") && message.Contains("Size") && message.Contains("Type"))
            {
                UnityEngine.Debug.Log ("ParseMessage FileMesage");
                return new Freak.Chat.Serializable.FileMessage ((IDictionary)message);
            }
            else if (message.Contains("Message") && message.Contains("Translations"))
            {
                UnityEngine.Debug.Log ("ParseMessage UserMesage");
                return new Freak.Chat.Serializable.UserMessage ((IDictionary)message);
            }
            else
            {
                UnityEngine.Debug.Log ("ParseMessage AdminMesage");
                return null;
            }

            return null;
        }*/

        public static bool AddMessage (ChatChannel chatChannel, List<SendBird.BaseMessage> messages)
        {

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("AddMessage: " + messages.Count);
//            Debug.Log ("TIME.TIME:: " + Time.time);
            #endif

            if (messages.Count == 0)
            {
                return false;
            }

            bool hasNewMessage = false;
            for (int i = 0; i < messages.Count; i++)
            {
                if (CheckForPreviousMsg(chatChannel, messages[i]) == false)
                {
                    ChatChannelHandler.OnMessageReceived (chatChannel, messages [i]);
                    hasNewMessage = true;
                }
            }

            return hasNewMessage;
        }

        public static void OnMessageReceived (ChatChannel chatChannel, SendBird.BaseMessage newMsg)
        {
            ChatMessage chatMsg = new ChatMessage();
            chatMsg = NewChatMessage (newMsg);

            ChatBaseMessage baseMsg = new ChatBaseMessage(newMsg);
            chatChannel.lastMessage = baseMsg;

            if (chatMsg.messageId != 0 && chatMsg.receiptStatus == ChatMessage.MessageReceiptType.Unknown)
            {
                chatMsg.receiptStatus = ChatMessage.MessageReceiptType.Send;
            }

            if (chatMsg.messageSender.userId == ChatManager.Instance.UserId)
            {
                //Same USER
                if (CheckForUnSendMessage(chatChannel, chatMsg))
                {
                    return;
                }
            }

            chatChannel.newMessage = new ChatMessage ();
            chatChannel.newMessage = chatMsg;
            
            chatChannel.isPreviousMsg = false;
            chatChannel.messagesList.Add (chatMsg);
            /*if (CheckAccordingToTime(chatChannel.messagesList, chatMsg) == false)
            {
                chatChannel.messagesList.Add (chatMsg);
            }*/
        }

        static bool CheckAccordingToTime (List<ChatMessage> messagesList, ChatMessage chatMsg)
        {
            int count = messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (messagesList[i].createdAt > chatMsg.createdAt)
                {
                    messagesList.Insert(i, chatMsg);
                    return true;
                }
            }

            return false;
        }

        public static void OnSendMessage (ChatChannel chatChannel, ChatMessage newMsg)
        {
            chatChannel.newMessage = new ChatMessage ();
            chatChannel.newMessage = newMsg;
            chatChannel.messagesList.Add(newMsg);

            /*if (CheckAccordingToTime(chatChannel.messagesList, newMsg) == false)
            {
                chatChannel.messagesList.Add(newMsg);
            }*/
        }

        public const string MESSAGE_SEND = "MessageNotSend";
        public static bool CheckForUnSendMessage (ChatChannel chatChannel, ChatMessage chatMsg)
        {
            int count = chatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (chatChannel.messagesList[i].receiptStatus == ChatMessage.MessageReceiptType.Unknown)
                {
                    if (string.IsNullOrEmpty (chatChannel.messagesList [i].customField) == false && string.IsNullOrEmpty (chatMsg.customField) == false)
                    {
                        if (chatChannel.messagesList [i].customField.Contains (MESSAGE_SEND) && chatMsg.customField.Contains (MESSAGE_SEND))
                        {
                            if (chatChannel.messagesList [i].customField.Equals (chatMsg.customField))
                            {
                                chatChannel.messagesList [i] = chatMsg;
//                                chatChannel.messagesList [i].customField = "";
                                chatChannel.messagesList [i].receiptStatus = ChatMessage.MessageReceiptType.Send;

                                if (chatMsg.messageType == ChatMessage.MessageType.Message)
                                {
                                    return true;
                                }
                                else if (chatMsg.messageType == ChatMessage.MessageType.FileLink)
                                {
                                    string urlFilePath = chatMsg.fileUrl;

                                    if (System.IO.Path.HasExtension (urlFilePath) == false)
                                        urlFilePath += System.IO.Path.GetExtension (chatMsg.fileName);

                                    string filePath = StreamManager.LoadFilePath(System.IO.Path.GetFileName(urlFilePath), ChatFileType.GetFolderLocation(chatMsg.fileType), true);

                                    #if UNITY_EDITOR && UNITY_DEBUG
                                    Debug.LogError ("CheckForTempMessageBox " + filePath);
                                    #endif
                                    if (System.IO.File.Exists(chatChannel.messagesList[i].filePath))
                                    {
                                        System.IO.File.Copy (chatChannel.messagesList [i].filePath, filePath);
                                            /*System.IO.File.Delete(chatChannel.messagesList[i].filePath);*/
                                    }

                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        public static bool CheckForPreviousMsg (ChatChannel chatChannel, SendBird.BaseMessage newMsg)
        {
            if (chatChannel.messagesList.Count > 0)
            {
                int count = chatChannel.messagesList.Count - 1;
                for (int i = count; i >= 0; i--)
                {
                    if (chatChannel.messagesList[i].messageId == newMsg.MessageId)
                    {
                        #if UNITY_EDITOR && UNITY_DEBUG
                        Debug.Log ("UpdateChatChannel " + chatChannel.channelName);
                        #endif
                        if (chatChannel.messagesList [i].messageType != ChatMessage.MessageType.Deleted)
                        {
                            chatChannel.messagesList [i] = ChatChannelHandler.UpdateChatMessage (chatChannel.messagesList [i], newMsg);
                        }
                        return true;
                    }
                }
            }
                
            return false;
        }

        public static ChatMessage UpdateChatMessage (ChatMessage chatMsg, SendBird.BaseMessage newMsg)
        {
            ChatBaseMessage baseMsg = new ChatBaseMessage(newMsg);
            chatMsg.baseMessage = baseMsg;

            // Update message for previously delted item incase there is Undo for delete msg.
            if (chatMsg.messageType == ChatMessage.MessageType.Deleted)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("UpdateChatChannel " + chatMsg.messageId);
                #endif
                if (newMsg is SendBird.UserMessage)
                {
                    SendBird.UserMessage msg = newMsg as SendBird.UserMessage;
                    
                    chatMsg.messageType = ChatMessage.MessageType.Message;
                    chatMsg.customField = msg.CustomType;
                    ChatChannelHandler.ReceivedUserMessage(chatMsg, msg);
                }
                else if (newMsg is SendBird.FileMessage)
                {
                    SendBird.FileMessage msg = newMsg as SendBird.FileMessage;
                    
                    chatMsg.messageType = ChatMessage.MessageType.FileLink;
                    chatMsg.customField = msg.CustomType;
                    ChatChannelHandler.ReceivedFileMessage(chatMsg, msg);
                }
                else if (newMsg is SendBird.AdminMessage)
                {
                    SendBird.AdminMessage msg = newMsg as SendBird.AdminMessage;
                    
                    chatMsg.messageType = ChatMessage.MessageType.AdminMessage;
                    chatMsg.data = msg.Data;
                    chatMsg.message = msg.Message;
                    chatMsg.customField = msg.CustomType;
                }
            }

            return chatMsg;
        }

        public static ChatMessage NewChatMessage (SendBird.BaseMessage newMsg)
        {
            ChatMessage chatMsg = new ChatMessage();
            ChatBaseMessage baseMsg = new ChatBaseMessage(newMsg);
            chatMsg.baseMessage = baseMsg;

            chatMsg.messageId = newMsg.MessageId;
            chatMsg.createdAt = newMsg.CreatedAt;

            if (newMsg is SendBird.UserMessage)
            {
                SendBird.UserMessage msg = newMsg as SendBird.UserMessage;

                chatMsg.messageType = ChatMessage.MessageType.Message;
                chatMsg.customField = msg.CustomType;
                ChatChannelHandler.ReceivedUserMessage(chatMsg, msg);
            }
            else if (newMsg is SendBird.FileMessage)
            {
                SendBird.FileMessage msg = newMsg as SendBird.FileMessage;

                chatMsg.messageType = ChatMessage.MessageType.FileLink;
                chatMsg.customField = msg.CustomType;
                ChatChannelHandler.ReceivedFileMessage(chatMsg, msg);
            }
            else if (newMsg is SendBird.AdminMessage)
            {
                SendBird.AdminMessage msg = newMsg as SendBird.AdminMessage;

                chatMsg.messageType = ChatMessage.MessageType.AdminMessage;
                chatMsg.data = msg.Data;
                chatMsg.message = msg.Message;
                chatMsg.customField = msg.CustomType;
            }

            return chatMsg;
        }

        public static void ReceivedUserMessage (ChatMessage chatMsg, SendBird.UserMessage msg)
        {
            chatMsg.messageSender = new ChatUser(msg.Sender);
            chatMsg.message = msg.Message;
            chatMsg.data = msg.Data;
            chatMsg.requestId = msg.RequestId;

            if(string.IsNullOrEmpty(chatMsg.data) == false)
            {
                if (chatMsg.messageId == 728415135)
                {
                    chatMsg.data = "";
                }
                
                if (chatMsg.data.Contains("InAppFileType_"))
                {
                    string _typeString = chatMsg.data.Replace("InAppFileType_", string.Empty);
                    chatMsg.fileType = ChatFileType.ConvertChatFileTypeFromString(_typeString);
                }
                else
                {
                    if (msg.Data.Contains("messageType") && msg.Data.Contains("fileType"))
                    {
                        try
                        {
                            FreakChatSerializeClass.NormalMessagerMetaData _metaData;
                            _metaData = (FreakChatSerializeClass.NormalMessagerMetaData)
                                Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(chatMsg.data);
                            
                            if (_metaData != null)
                            {
                                chatMsg.normalMessagerMetaData = _metaData;
                                
                                if (_metaData.messageType == ChatMessage.MetaMessageType.InAppFile)
                                {
                                    chatMsg.filePath = _metaData.filePath;
                                    chatMsg.fileType = _metaData.fileType;
                                }
                            }
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError (e.Message);
                        }
                    }
                }
            }
        }

        public static void ReceivedFileMessage (ChatMessage chatMsg, SendBird.FileMessage msg)
        {
            chatMsg.messageSender = new ChatUser(msg.Sender);

            chatMsg.requestId = msg.RequestId;
            chatMsg.fileName = msg.Name;
            chatMsg.fileUrl =  msg.Url;
            chatMsg.fileSize = msg.Size;
            chatMsg.fileType = ChatFileType.ConvertChatFileTypeFromString(msg.Type);
            chatMsg.data = msg.Data;

            if (msg.Data.Contains("messageType") && msg.Data.Contains("fileType"))
            {
                FreakChatSerializeClass.NormalMessagerMetaData _metaData;
                _metaData = (FreakChatSerializeClass.NormalMessagerMetaData)
                    Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(chatMsg.data);

                if (_metaData != null)
                {
                    chatMsg.normalMessagerMetaData = _metaData;
                }
            }
        }

        public static void RemoveMessage (ChatChannel chatChannel, SendBird.BaseMessage baseMessage)
        {
            int count = chatChannel.messagesList.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (chatChannel.messagesList[i].baseMessage.Url == baseMessage.ChannelUrl)
                {
                    if (chatChannel.newMessage.baseMessage.Url == baseMessage.ChannelUrl)
                    {
                        if (i > 0)
                        {
                            chatChannel.newMessage = chatChannel.messagesList [i - 1];
                        }
                    }

                    chatChannel.messagesList.RemoveAt(i);

                    break;
                }
            }
        }

        public static ChatMessage DuplicateMessage (string data)
        {
            ChatMessage _chatMsg = new ChatMessage();
            _chatMsg.createdAt = FreakChatUtility.GetCurrentTimeStamp();
            _chatMsg.receiptStatus = ChatMessage.MessageReceiptType.Unknown;
            _chatMsg.messageState = ChatMessage.MessageState.Loading;

            _chatMsg.messageSender = new ChatUser();// ChatManager.Instance.UserId;
            _chatMsg.messageSender.userId = ChatManager.Instance.UserId;
            _chatMsg.messageSender.nickname = ChatManager.Instance.UserName;
            _chatMsg.customField = "MessageNotSend"+_chatMsg.createdAt.ToString();

            if(string.IsNullOrEmpty(data) == false)
            {
                if (data.Contains("InAppFileType_"))
                {
                    string _typeString = data.Replace("InAppFileType_", string.Empty);
                    _chatMsg.fileType = ChatFileType.ConvertChatFileTypeFromString(_typeString);
                }
                else
                {
                    if (data.Contains("messageType") && data.Contains("fileType"))
                    {
                        FreakChatSerializeClass.NormalMessagerMetaData _metaData;
                        _metaData = (FreakChatSerializeClass.NormalMessagerMetaData)
                            Newtonsoft.Json.JsonConvert.DeserializeObject<FreakChatSerializeClass.NormalMessagerMetaData>(data);

                        if (_metaData != null)
                        {
                            _chatMsg.normalMessagerMetaData = _metaData;

                            if (_metaData.messageType == ChatMessage.MetaMessageType.InAppFile)
                            {
                                _chatMsg.filePath = _metaData.filePath;
                                _chatMsg.fileType = _metaData.fileType;
                            }
                        }
                    }
                }
            }

            return _chatMsg;
        }

        public static void MarkLastMessageAsRead (ChatChannel chatChannel, long messageId)
        {
            if (chatChannel.lastMessage != null && chatChannel.lastMessage.MessageId == messageId)
            {
                chatChannel.lastMessage.receiptStatus = ChatMessage.MessageReceiptType.Read;
            }
        }

    }
}
