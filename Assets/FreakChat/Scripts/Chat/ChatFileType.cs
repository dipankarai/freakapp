﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Freak.Chat
{
    public class ChatFileType
    {
        public enum FileType
        {
            None    = 0,
            AudioBuzz,
            Contact,
            Doc,
            Image,
            Location,
            Video,
            AudioRecord,
            Stickers,
            Others,
			Audio,
			Camera,
			Challenge,
			Referral
        }

        private string mFilePath;
        private FileType mFileType;
        private int mFileSize;

        public string filePath
        {
            get { return mFilePath; }
        }
        public FileType fileType
        {
            get { return mFileType; }
        }
        public int fileSize
        {
            get { return mFileSize; }
        }
        public string fileName;

        public const string inappFileType = "InAppFileType_";

        public ChatFileType (string filePath, FileType type)
        {
            mFilePath = filePath;
            mFileType = type;

            FileInfo();
        }

        void FileInfo()
        {
            if (File.Exists(mFilePath))
            {
                fileName = Path.GetFileName(mFilePath);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("PATH: "+mFilePath);
                #endif
                System.IO.FileInfo info = new System.IO.FileInfo(mFilePath);
                mFileSize = System.Convert.ToInt32(info.Length);
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log("FILE SIZE "+mFileSize);
                #endif
            }
            else
            {
                Debug.LogError(mFilePath + " Doesn't Exits");
            }
        }

        public static string ConvertChatFileTypeToString (FileType type)
        {
            int _type = (int)type;


            return _type.ToString();
        }

        public static FileType ConvertChatFileTypeFromString (string type)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log("ConvertChatFileTypeFromString " + type);
            #endif
            int _type;
            bool isInt = int.TryParse(type, out _type);

            FileType _fileType = FileType.Others;
            if (isInt)
            {
                _fileType = (FileType)_type;
            }
            else
            {
                _fileType = (FileType)System.Enum.Parse(typeof(FileType), type);
            }

            return _fileType;
        }

        public static FolderLocation GetFolderLocation (FileType type)
        {
            switch (type)
            {
                case FileType.Audio:
                case FileType.AudioBuzz:
                case FileType.AudioRecord:

                    return FolderLocation.Audio;

                case FileType.Contact:
                    return FolderLocation.Contact;

                case FileType.Image:
                case FileType.Stickers:
                    return FolderLocation.Image;

                case FileType.Video:
                    return FolderLocation.Video;

                case FileType.Location:
                case FileType.Doc:
                case FileType.Others:
                    return FolderLocation.Others;

                default:
                    Debug.LogError("No such file type " + type);
                    return FolderLocation.Others;
            }
        }
    }
}
