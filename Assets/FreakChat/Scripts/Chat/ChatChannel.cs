﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Freak.Chat
{
    public class AllChatChannelDetails
    {
        public ChatUser Sender;
        public string MessageSendTime;
        public string MessageToDisplay = string.Empty;
        public ChatFileType.FileType FileType = ChatFileType.FileType.None;
        public string imageUrl;
        public Texture2D imageTexture;
        public string unReadMsg = string.Empty;
        public string displayName = string.Empty;
        public ChatUser.UserConnectionStatus status = ChatUser.UserConnectionStatus.NonAvailable;
    }

    [System.Serializable]
    public class ChatChannel
    {
        #region Group Param
        public GroupSerializableResponseClass.MetaData adminMetaData;
        #endregion

        public int unreadMessageCount;
        public long lastSeenAt;
        public string coverUrl = string.Empty;
        public long createdAt;
        public bool isGroupChannel;
        public bool isOpenChannel = false;
        public string data = string.Empty;
        public string channelName = string.Empty;     //Channel Name
        public string channelUrl = string.Empty;
        public bool isPreviousMsg;
        public string otherUserId = string.Empty;
        public string otherUserName = string.Empty;
        public ChatBaseMessage lastMessage;
        #if PER_CHECK
        public ChatBaseMessage lastReadMessage;
        #endif
        public ChatMessage newMessage;
        #if PER_CHECK
        public ChatMessage.MessageReceiptType lastMessageReadReceipt;
        #endif

        [Newtonsoft.Json.JsonIgnore]
        public byte[] imageCache;
        [Newtonsoft.Json.JsonIgnore]
        public Texture2D textureCache;

        public List<ChatUser> channelMembers = new List<ChatUser>();
        #if PER_CHECK
        [Newtonsoft.Json.JsonIgnore]
        public List<ChatMessage> messagesList = new List<ChatMessage>();
        #else
        public List<ChatMessage> messagesList = new List<ChatMessage>();
        #endif
        [Newtonsoft.Json.JsonIgnore]
        public Sprite profileImageCache;
        [Newtonsoft.Json.JsonIgnore]
        public SendBird.GroupChannel groupChannel;
        [Newtonsoft.Json.JsonIgnore]
        public AllChatChannelDetails allChatChannelDetails;

        public ChatChannel ()
        {
        }

        public ChatChannel (ChatChannel cloneChannel)
        {
            this.allChatChannelDetails = new AllChatChannelDetails();

            this.channelUrl = cloneChannel.channelUrl;
            this.groupChannel = cloneChannel.groupChannel;
            this.coverUrl = cloneChannel.coverUrl;
            this.unreadMessageCount = cloneChannel.unreadMessageCount;
            this.isGroupChannel = cloneChannel.isGroupChannel;
            this.channelName = WWW.UnEscapeURL(cloneChannel.channelName);
            this.otherUserName = cloneChannel.otherUserName;
            this.otherUserId = cloneChannel.otherUserId;

            if (cloneChannel.GetSender() != null)
            {
                this.allChatChannelDetails.Sender = cloneChannel.GetSender();
            }

            if (cloneChannel.unreadMessageCount > 0)
            {
                this.allChatChannelDetails.unReadMsg = cloneChannel.unreadMessageCount.ToString();
            }
            else
            {
                this.allChatChannelDetails.unReadMsg = "";
            }

            if(this.isGroupChannel)
            {
                this.allChatChannelDetails.displayName = cloneChannel.channelName;
                this.allChatChannelDetails.imageUrl = this.coverUrl;
            }
            else
            {
                this.allChatChannelDetails.displayName = cloneChannel.otherUserName;

                if (this.allChatChannelDetails.Sender != null)
                {
                    this.allChatChannelDetails.imageUrl = this.allChatChannelDetails.Sender.profileUrl;
                    this.allChatChannelDetails.status = this.allChatChannelDetails.Sender.connectionStatus;
                }
            }
            //        this.allChatChannelDetails.imageTexture = ChatManager.Instance.GetChannelTexture(this.channelUrl);

            string timeSend = FreakChatUtility.GetTime(cloneChannel.createdAt);
            if (cloneChannel.lastMessage != null)
            {
                timeSend = FreakChatUtility.GetTime(cloneChannel.lastMessage.CreatedAt);

                if (cloneChannel.lastMessage.chatMessageType == ChatBaseMessage.ChatMessageType.FileMessage)
                {
                    if (cloneChannel.isGroupChannel)
                    {
                        this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.Sender.nickname;
                    }
                    else
                    {
                        this.allChatChannelDetails.MessageToDisplay = string.Empty;
                    }

                    this.allChatChannelDetails.FileType = ChatFileType.ConvertChatFileTypeFromString(cloneChannel.lastMessage.fileType);
                }
                else if (cloneChannel.lastMessage.chatMessageType == ChatBaseMessage.ChatMessageType.UserMessage)
                {
                    if (string.IsNullOrEmpty(cloneChannel.lastMessage.Data))
                    {
                        if (cloneChannel.isGroupChannel)
                        {
                            this.allChatChannelDetails.MessageToDisplay = 
                                (cloneChannel.lastMessage.Sender.nickname + " : " + cloneChannel.lastMessage.Message);
                        }
                        else
                        {
                            this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.Message;
                        }

                        this.allChatChannelDetails.FileType = ChatFileType.FileType.None;
                    }
                    else
                    {
                        string _data = cloneChannel.lastMessage.Data;

                        if (_data.Contains("messageType")  && _data.Contains("fileType"))
                        {
                            if (cloneChannel.lastMessage.metaData != null)
                            {
                                if (cloneChannel.lastMessage.metaData.messageType == ChatMessage.MetaMessageType.SystemNewGroup)
                                {
                                    this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.metaData.message;
                                }
                                else if (cloneChannel.lastMessage.metaData.fileType != ChatFileType.FileType.None)
                                {
                                    if (cloneChannel.isGroupChannel)
                                    {
                                        this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.Sender.nickname;
                                        this.allChatChannelDetails.FileType = cloneChannel.lastMessage.metaData.fileType;
                                    }
                                    else
                                    {
                                        this.allChatChannelDetails.MessageToDisplay = string.Empty;
                                        this.allChatChannelDetails.FileType = cloneChannel.lastMessage.metaData.fileType;
                                    }
                                }
                                else
                                {
                                    this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.metaData.messageType.ToString();
                                }
                            }
                        }
                        else
                        {
                            this.allChatChannelDetails.MessageToDisplay = cloneChannel.lastMessage.Message;
                        }
                    }
                }

                if (this.allChatChannelDetails.FileType == ChatFileType.FileType.None)
                {
                    if (this.allChatChannelDetails.MessageToDisplay.Length > 50)
                    {
                        string _msg = this.allChatChannelDetails.MessageToDisplay.Substring(0, 49);
                        for (int i = (_msg.Length-1); i >= 0; i--)
                        {
                            if(Char.IsWhiteSpace (_msg[i]))
                            {
                                this.allChatChannelDetails.MessageToDisplay = _msg.Substring(0, (i - 1));
                                this.allChatChannelDetails.MessageToDisplay += " ......";
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(this.allChatChannelDetails.MessageToDisplay))
                    {
                        this.allChatChannelDetails.MessageToDisplay = ": " + this.allChatChannelDetails.FileType.ToString();
                    }
                    else
                    {
                        this.allChatChannelDetails.MessageToDisplay += " : " + this.allChatChannelDetails.FileType.ToString();
                    }
                }
            }
            else
            {
                if (cloneChannel.isGroupChannel)
                {
                    this.allChatChannelDetails.MessageToDisplay = ("Message " + this.channelName);
                }
                else
                {
                    this.allChatChannelDetails.MessageToDisplay = ("Message " + this.otherUserName);
                }
            }
        }

        public ChatUser GetMember (string memberId)
        {
            for (int i = 0; i < channelMembers.Count; i++)
            {
                if (channelMembers[i].userId == memberId)
                {
                    return channelMembers [i];
                }
            }

            return null;
        }

        public ChatUser GetSender ()
        {
            if (channelMembers == null)
                return null;

            if (isGroupChannel)
            {
                if (lastMessage!= null)
                {
                    return lastMessage.Sender;
                }

                return null;
            }
            else
            {
                if (channelMembers[0].userId == ChatManager.Instance.UserId)
                    return channelMembers[1];
                else if (channelMembers[1].userId == ChatManager.Instance.UserId)
                    return channelMembers[0];
                else
                    return null;
            }

            return null;
        }
    }
}
