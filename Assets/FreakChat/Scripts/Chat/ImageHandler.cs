﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Freak
{
    public class ImageHandler : MonoBehaviour
    {
        [System.Serializable]
        public class ImageDetail
        {
            public string filename;
            public string id;
            public Texture2D texture;
        }

        public List<ImageDetail> imageDetailList = new List<ImageDetail> ();

        // Use this for initialization
        void Start () {
            
        }

        public Texture2D GetResizedTexture (string filePath, string id)
        {
//            string filename = Path.GetFileName (filePath);
            for (int i = 0; i < imageDetailList.Count; i++)
            {
                if (imageDetailList[i].id.Equals(id))
                {
                    return imageDetailList [i].texture;
                }
            }

            return null;
        }

        public void AddResizedTexture (Texture2D texture, string filePath, string id)
        {
            ImageDetail _newImage = new ImageDetail ();
            string filename = Path.GetFileName (filePath);

            for (int i = 0; i < imageDetailList.Count; i++)
            {
                if (imageDetailList[i].id == id)
                {
                    imageDetailList [i].filename = filename;
                    imageDetailList [i].texture = texture;
                    return;
                }
            }

            _newImage.filename = filename;
            _newImage.id = id;
            _newImage.texture = texture;

            imageDetailList.Add (_newImage);
        }

        public void ClearList ()
        {
            imageDetailList.Clear ();
        }
    }
}
