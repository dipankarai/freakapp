﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace Freak.Chat
{
    public class CacheManager
    {
        public static CacheSeralizeClass cacheSeralizeClass { get { return mCacheSeralizeClass; } }
        private static CacheSeralizeClass mCacheSeralizeClass;

        public const string CACHED_MSG = "freak_cache_msg.gd";
        static string cachedDatabasePath = FreakAppManager.ApplicationPath + "/cache/";

        public static void SaveNewMessage (SendBird.BaseChannel baseChannel, SendBird.BaseMessage baseMessage)
        {
            if (baseChannel.IsGroupChannel())
            {
                SendBird.GroupChannel _mGroupChannel = baseChannel as SendBird.GroupChannel;

                ChatChannel _mChatChannel = new ChatChannel ();
                _mChatChannel = GetInitChatChannel (_mGroupChannel);

                InitCacheSeralizeClass ();

                if (UpdateForPreviousMessage(_mChatChannel) != null)
                {
                    _mChatChannel = UpdateForPreviousMessage (_mChatChannel);
                }
                else
                {
                    mCacheSeralizeClass.allChannelList.Add (_mChatChannel);
                }

                if (ChatChannelHandler.CheckForPreviousMsg(_mChatChannel, baseMessage) == false)
                {
                    ChatChannelHandler.OnMessageReceived (_mChatChannel, baseMessage);
                }

                //Sort New Message at top...
                for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
                {
                    if (mCacheSeralizeClass.allChannelList[i].channelUrl == _mChatChannel.channelUrl)
                    {
                        _mChatChannel = mCacheSeralizeClass.allChannelList [i];
                        mCacheSeralizeClass.allChannelList.RemoveAt (i);
                        mCacheSeralizeClass.allChannelList.Insert (0, _mChatChannel);
                        break;
                    }
                }

                UpdateCurrentCacheData ();
                SaveChannelData ();
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("IS OPEN CHANNEL");
                #endif
            }
        }

        public static void UpdateGroupChannel (SendBird.GroupChannel groupChannel)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == groupChannel.Url)
                {
                    ChatChannelHandler.UpdateChatChannel (mCacheSeralizeClass.allChannelList [i], groupChannel);
                    OnlyGroupChannel ();
                    break;
                }
            }

            UpdateCurrentCacheData ();
            SaveChannelData ();
        }

        public static void SaveNewMessage (ChatChannel channel)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("SaveChannel");
            #endif

            InitCacheSeralizeClass ();

            if (CheckForPreviousMessage(channel) == false)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("CheckForPreviousMessage");
                #endif
                mCacheSeralizeClass.allChannelList.Add (channel);
            }

            UpdateCurrentCacheData ();
            SaveChannelData ();
        }

        private static ChatChannel UpdateForPreviousMessage (ChatChannel chatChannel)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == chatChannel.channelUrl)
                {
                    List<ChatMessage> _messageList = mCacheSeralizeClass.allChannelList [i].messagesList;
                    mCacheSeralizeClass.allChannelList [i] = chatChannel;
                    mCacheSeralizeClass.allChannelList [i].messagesList = _messageList;
                    return mCacheSeralizeClass.allChannelList [i];
                }
            }

            return null;
        }

        private static bool CheckForPreviousMessage (ChatChannel chatChannel)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == chatChannel.channelUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogWarning(mCacheSeralizeClass.allChannelList[i].messagesList.Count + "=::=" + chatChannel.messagesList.Count);
                    #endif
//                    mCacheSeralizeClass.allChannelList [i].unreadMessageCount = chatChannel.unreadMessageCount;
                    mCacheSeralizeClass.allChannelList [i] = chatChannel;
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.LogWarning(mCacheSeralizeClass.allChannelList[i].messagesList.Count + "=++=" + chatChannel.messagesList.Count);
                    #endif
                    return true;
                }
            }

            return false;
        }

        private static void InitCacheSeralizeClass()
        {
            if (mCacheSeralizeClass == null)
            {
                string filePath = cachedDatabasePath + FreakAppManager.Instance.mobileNumber + CACHED_MSG;
                if (File.Exists(filePath))
                {
                    LoadData ();
                }
                else
                {
                    mCacheSeralizeClass = new CacheSeralizeClass ();
                }
            }
        }

        public static void LoadData ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("====> LoadData");
            #endif
            string filePath = cachedDatabasePath + FreakAppManager.Instance.mobileNumber + CACHED_MSG;

            if (File.Exists(filePath))
            {
                using (StreamReader file = new StreamReader (filePath))
                {
                    string data = file.ReadToEnd ();
                    mCacheSeralizeClass = (CacheSeralizeClass)Newtonsoft.Json.JsonConvert.DeserializeObject<CacheSeralizeClass> (data);
                    file.Close ();
                }
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError ("File Doesn't Exists " + filePath);
                #endif
                mCacheSeralizeClass = new CacheSeralizeClass ();
            }

            CheckForDuplicateChannel ();

            VerifyChannel ();
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("====> LoadData");
            #endif
        }

        static void VerifyChannel ()
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].isGroupChannel == false)
                {
                    if (mCacheSeralizeClass.allChannelList[i].channelMembers.Count == 1)
                    {
                        mCacheSeralizeClass.allChannelList.RemoveAt(i);
                        continue;
                    }
                    else if (mCacheSeralizeClass.allChannelList[i].channelMembers.Count == 2)
                    {
                        if (mCacheSeralizeClass.allChannelList[i].GetSender() == null)
                        {
                            mCacheSeralizeClass.allChannelList.RemoveAt(i);
                            continue;
                        }
                    }
                }
            }

            UpdateCurrentCacheData ();
        }

        static void CheckForDuplicateChannel ()
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                string channelUrl = mCacheSeralizeClass.allChannelList [i].channelUrl;
                int matched = 0;
                for (int j = 0; j < mCacheSeralizeClass.allChannelList.Count; j++) {
                    if (mCacheSeralizeClass.allChannelList[j].channelUrl == channelUrl)
                    {
                        matched++;
                        if (matched > 1)
                        {
                            mCacheSeralizeClass.allChannelList.RemoveAt (j);
                            continue;
                        }
                    }
                }
            }
        }
        
        public static void SaveChannelData ()
        {
            if (Directory.Exists (cachedDatabasePath) == false)
                Directory.CreateDirectory (cachedDatabasePath);

            string filePath = cachedDatabasePath + FreakAppManager.Instance.mobileNumber + CACHED_MSG;

            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log (filePath);
            #endif

            using (StreamWriter file = new StreamWriter (filePath))
            {
                file.WriteLine (Newtonsoft.Json.JsonConvert.SerializeObject (mCacheSeralizeClass));
                file.Close();
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError ("Saved Data " + (++saveCount));
            #endif
        }
        private static int saveCount;

        public static void DeleteData ()
        {
            string filePath = cachedDatabasePath + FreakAppManager.Instance.mobileNumber + CACHED_MSG;

            if (File.Exists(filePath))
            {
                File.Delete (filePath);
            }
        }

        public static void UpdateChannelList (List<SendBird.GroupChannel> groupChannelList)
        {
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                //                Debug.Log(Newtonsoft.Json.JsonConvert.SerializeObject(groupChannelList));
                if (UpdateForPreviousMessage(groupChannelList[i]) == false)
                {
                    ChatChannel _mChatChannel = new ChatChannel ();
                    _mChatChannel = ChatChannelHandler.InitChatChannel (groupChannelList [i]);

                    mCacheSeralizeClass.allChannelList.Add (_mChatChannel);
                    /*if (FreakPlayerPrefs.AppInstalledTime > groupChannelList[i].LastMessage.CreatedAt)
                    {
                        if (_mChatChannel.isGroupChannel)
                        {
                            mCacheSeralizeClass.allChannelList.Add (_mChatChannel);
                        }
                    }
                    else
                    {
                        mCacheSeralizeClass.allChannelList.Add (_mChatChannel);
                    }*/
                }
            }

//            CheckForRemovedChannel(groupChannelList);
            SortChannel (groupChannelList);
            VerifyChannel();
        }

        public static ChatChannel UpdateChannel (SendBird.GroupChannel groupChannel)
        {
            ChatChannel _chatChannel = new ChatChannel();

            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == groupChannel.Url)
                {
                    ChatChannelHandler.UpdateChatChannel (mCacheSeralizeClass.allChannelList [i], groupChannel);
                    return mCacheSeralizeClass.allChannelList[i];
                }
            }

            _chatChannel = ChatChannelHandler.InitChatChannel(groupChannel);
            mCacheSeralizeClass.allChannelList.Add (_chatChannel);

            UpdateCurrentCacheData ();

            return _chatChannel;
        }

        private static bool UpdateForPreviousMessage (SendBird.GroupChannel groupChannel)
        {
            InitCacheSeralizeClass ();

            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == groupChannel.Url)
                {
                    ChatChannelHandler.UpdateChatChannel (mCacheSeralizeClass.allChannelList [i], groupChannel);
                    return true;
                }
            }

            return false;
        }

        private static void CheckForRemovedChannel (List<SendBird.GroupChannel> groupChannelList)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (CheckingForRemovedChannel(groupChannelList, mCacheSeralizeClass.allChannelList[i].channelUrl) == false)
                {
                    mCacheSeralizeClass.allChannelList.RemoveAt(i);
                    continue;
                }
            }
        }

        private static bool CheckingForRemovedChannel (List<SendBird.GroupChannel> groupChannelList, string cachedChannel)
        {
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                if (groupChannelList[i].Url == cachedChannel)
                {
                    return true;
                }
            }

            return false;
        }

        public static void MoveChannelToTopList (string channelUrl)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == channelUrl && i != 0)
                {
                    ChatChannel temp = mCacheSeralizeClass.allChannelList [i];
                    mCacheSeralizeClass.allChannelList.RemoveAt(i);
                    mCacheSeralizeClass.allChannelList.Insert(0, temp);
                    break;
                }
            }

            UpdateCurrentCacheData ();
        }

        private static void SortChannel (List<SendBird.GroupChannel> groupChannelList)
        {
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                for (int j = 0; j < mCacheSeralizeClass.allChannelList.Count; j++)
                {
                    if (mCacheSeralizeClass.allChannelList[j].channelUrl == groupChannelList[i].Url)
                    {
                        ChatChannel temp = mCacheSeralizeClass.allChannelList [i];
                        mCacheSeralizeClass.allChannelList [i] = mCacheSeralizeClass.allChannelList [j];
                        mCacheSeralizeClass.allChannelList [j] = temp;
                        break;
                    }
                }
            }
        }

        public static void OnlyGroupChannel ()
        {
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("====> OnlyGroupChannel");
            #endif
            ChatManager.Instance.cachedGroupChannelData = new List<ChatChannel> ();

            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
//                Debug.Log ("====> OnlyGroupChannel " + mCacheSeralizeClass.allChannelList [i].channelName);
                #endif
                if (string.IsNullOrEmpty(mCacheSeralizeClass.allChannelList [i].channelName))
                {
                    mCacheSeralizeClass.allChannelList.RemoveAt(i);
                    continue;
                }

                if (ChatManager.IsSingleChannel(mCacheSeralizeClass.allChannelList[i].channelName) == false)
                {
                    ChatManager.Instance.cachedGroupChannelData.Add (mCacheSeralizeClass.allChannelList [i]);
                }
            }
            #if UNITY_EDITOR && UNITY_DEBUG
//            Debug.Log ("====> OnlyGroupChannel");
            #endif
        }

        #region Cache
        public static ChatChannel GetChatMessage (string channelUrl)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetChatMessage. " + channelUrl);
            #endif

            if (mCacheSeralizeClass == null || mCacheSeralizeClass.allChannelList.Count == 0)
                return null;

            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.Log ("GetChatMessage.. " + mCacheSeralizeClass.allChannelList [i].channelUrl);
                #endif

                if (mCacheSeralizeClass.allChannelList [i].channelUrl == channelUrl)
                {
                    #if UNITY_EDITOR && UNITY_DEBUG
                    Debug.Log ("GetChatMessage... " + mCacheSeralizeClass.allChannelList.Count);
                    #endif
                    return mCacheSeralizeClass.allChannelList [i];
                }
            }

            return null;
        }
        #endregion

        /*public static List<GroupChannel> GetNewAllChannelList (List<SendBird.GroupChannel> groupChannelList)
        {
            List<GroupChannel> newGroupChannelList = new List<GroupChannel> ();
            for (int i = 0; i < groupChannelList.Count; i++)
            {
                string seralizeDict = Newtonsoft.Json.JsonConvert.SerializeObject (groupChannelList [i]);
                GroupChannel newGroupChannel = new GroupChannel (GetDictionary (seralizeDict));
                newGroupChannelList.Add (newGroupChannel);
            }

            return newGroupChannelList;
        }

        public static GroupChannel GetGroupChannel (SendBird.GroupChannel groupChannel)
        {
            string seralizeDict = Newtonsoft.Json.JsonConvert.SerializeObject (groupChannel);
            GroupChannel newGroupChannel = new GroupChannel (GetDictionary (seralizeDict));
            return newGroupChannel;
        }*/

        public static IDictionary GetDictionary (string seralizedObject)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("GetDictionary " + seralizedObject);
            #endif

            return (IDictionary)MiniJSON.Json.Deserialize (seralizedObject);
        }

        public static ChatChannel GetInitChatChannel (SendBird.GroupChannel groupChannel)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == groupChannel.Url)
                {
                    ChatChannelHandler.UpdateChatChannel(mCacheSeralizeClass.allChannelList[i], groupChannel);
                    return mCacheSeralizeClass.allChannelList[i];
                }
            }

            return ChatChannelHandler.InitChatChannel(groupChannel);
        }

        public static ChatChannel GetChatChannel (string channelUrl)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == channelUrl)
                {
                    return mCacheSeralizeClass.allChannelList[i];
                }
            }

            return null;
        }
    
        public static void UpdateReadReceipt (SendBird.GroupChannel groupChannel)
        {
            if (groupChannel.LastMessage != null)
            {
                for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
                {
                    if (mCacheSeralizeClass.allChannelList[i].channelUrl == groupChannel.Url)
                    {
                        int count = mCacheSeralizeClass.allChannelList [i].messagesList.Count - 1;
                        for (int j = count; j >= 0; j++) {
                            if (mCacheSeralizeClass.allChannelList [i].messagesList[j].messageId == groupChannel.LastMessage.MessageId)
                            {
                                UpdatePreviousReadReceipt (j, groupChannel.Url);
                                break;
                            }
                        }
                    }
                }
                
                UpdateCurrentCacheData ();
            }
        }

        static void UpdatePreviousReadReceipt (int index, string channelUrl)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList [i].channelUrl == channelUrl)
                {
                    for (int j = index; j >= 0; j--)
                    {
                        if (mCacheSeralizeClass.allChannelList [i].messagesList[j].messageSender.userId == ChatManager.Instance.UserId)
                        {
                            if (mCacheSeralizeClass.allChannelList [i].messagesList[j].receiptStatus != ChatMessage.MessageReceiptType.Read)
                            {
                                Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
                                mCacheSeralizeClass.allChannelList [i].messagesList [j].receiptStatus = ChatMessage.MessageReceiptType.Read;
                                ChatChannelHandler.MarkLastMessageAsRead(mCacheSeralizeClass.allChannelList [i], mCacheSeralizeClass.allChannelList [i].messagesList[j].messageId);
                            }
                            else if (mCacheSeralizeClass.allChannelList [i].messagesList[j].receiptStatus == ChatMessage.MessageReceiptType.Read)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            UpdateCurrentCacheData ();
        }

        public static void ChangeLastMessageReadReceipt (string channelUrl, long messageId)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == channelUrl)
                {
                    #if PER_CHECK
                    mCacheSeralizeClass.allChannelList[i].lastMessage.receiptStatus = ChatMessage.MessageReceiptType.Read;
//                    ChatChannelHandler.MarkLastMessageAsRead(mCacheSeralizeClass.allChannelList [i], messageId);
                    #else
                    int count = mCacheSeralizeClass.allChannelList[i].messagesList.Count - 1;
                    for (int j = count; j >= 0; j--)
                    {
                        if (mCacheSeralizeClass.allChannelList[i].messagesList[j].messageId == messageId)
                        {
                            //Debug.Log("LAST MESSAGE " + mCacheSeralizeClass.allChannelList[i].messagesList[j].message);
                            //Debug.LogError("ChatMessage.MessageReceiptType.Read ==================");
                            mCacheSeralizeClass.allChannelList[i].messagesList[j].receiptStatus = ChatMessage.MessageReceiptType.Read;
                            mCacheSeralizeClass.allChannelList[i].messagesList[j].messageState = ChatMessage.MessageState.Normal;
                            ChatChannelHandler.MarkLastMessageAsRead(mCacheSeralizeClass.allChannelList [i], mCacheSeralizeClass.allChannelList [i].messagesList[j].messageId);
                            MarkPreviousMessage(channelUrl, mCacheSeralizeClass.allChannelList[i].messagesList, j);
                            break;
                        }
                    }
                    #endif
                }
            }
        }

        private static void MarkPreviousMessage (string channelUrl, List<ChatMessage> messageList, int index)
        {
            if (messageList.Count > 1 && index > 0)
            {
                if (messageList[index -1].receiptStatus != ChatMessage.MessageReceiptType.Read)
                {
                    ChangeLastMessageReadReceipt(channelUrl, messageList[index - 1].messageId);
                    return;
                }
            }
        }

        public static void DeleteSingleChannel (string channelUrl)
        {
            for (int i = 0; i < mCacheSeralizeClass.allChannelList.Count; i++)
            {
                if (mCacheSeralizeClass.allChannelList[i].channelUrl == channelUrl)
                {
                    mCacheSeralizeClass.allChannelList.RemoveAt(i);
                    UpdateCurrentCacheData ();
                    break;
                }
            }
        }

        private static void UpdateCurrentCacheData ()
        {
            ChatManager.Instance.cachedData = mCacheSeralizeClass;
        }

        public static void SaveMessageList (string channelUrl, List<ChatMessage> chatMessageList)
        {
            string _messageDir = cachedDatabasePath + channelUrl + "/";
            if (Directory.Exists (_messageDir) == false)
                Directory.CreateDirectory (_messageDir);

            string fileName = channelUrl + ".gd";
            string filePath = _messageDir + fileName;

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SaveMessageList " + filePath);
            #endif

            using (StreamWriter file = new StreamWriter (filePath))
            {
                file.WriteLine (Newtonsoft.Json.JsonConvert.SerializeObject (chatMessageList));
                file.Close();
            }

            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.LogError ("Saved Data " + (++saveCount));
            #endif
        }

        public static List<ChatMessage> LoadMessageList (string channelUrl)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("====> LoadMessageList");
            #endif

            string _messageDir = cachedDatabasePath + channelUrl + "/";
            string fileName = channelUrl + ".gd";
            string filePath = _messageDir + fileName;

            List<ChatMessage> _chatMessageList = new List<ChatMessage>();

            if (File.Exists(filePath))
            {
                using (StreamReader file = new StreamReader (filePath))
                {
                    string data = file.ReadToEnd ();
                    _chatMessageList = (List<ChatMessage>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<ChatMessage>> (data);
                    file.Close ();
                }
            }
            else
            {
                #if UNITY_EDITOR && UNITY_DEBUG
                Debug.LogError ("File Doesn't Exists " + filePath);
                #endif
            }

            return _chatMessageList;
        }
    }
}
