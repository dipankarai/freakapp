﻿using UnityEngine;

/// <summary>
/// Hops the object's position.
/// </summary>
using System.Collections;

public class TweenHop : UITweener
{
	public Vector3 from;
	public Vector3 to;
	public float curvature = .72f;
		
	float hopHeight;

	public float HopHeight {
		get {
			hopHeight = (value.y - to.y) * curvature;
			return hopHeight;
		}
		set {
			hopHeight = value;
		}
	}

	[HideInInspector]
	public bool
		worldSpace = false;
	
	Transform mTrans;

	public Transform cachedTransform {
		get {
			if (mTrans == null) {
				mTrans = transform;
			}
			return mTrans;
		}
	}

	[System.Obsolete ("Use 'value' instead")]
	public Vector3 position { get { return this.value; } set { this.value = value; } }

	/// <summary>
	/// Tween's current value.
	/// </summary>
	
	public Vector3 value {
		get {
			return worldSpace ? cachedTransform.position : cachedTransform.localPosition;
		}
		set {
			if (worldSpace) {
				cachedTransform.position = value;
			} else {
				cachedTransform.localPosition = value;
			}
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>
	
	protected override void OnUpdate (float factor, bool isFinished)
	{
		var height = Mathf.Atan (Mathf.PI * factor) * HopHeight;
		value = Vector3.Lerp (from, to, factor) + Vector3.up * height;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	
	static public TweenHop Begin (GameObject go, float duration, Vector3 pos)
	{
		TweenHop comp = UITweener.Begin<TweenHop> (go, duration);
		comp.from = comp.value;
		comp.to = pos;
		
		if (duration <= 0f) {
			comp.Sample (1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	[ContextMenu ("Set 'From' to current value")]
	public override void SetStartToCurrentValue ()
	{
		from = value;
	}

	[ContextMenu ("Set 'To' to current value")]
	public override void SetEndToCurrentValue ()
	{
		to = value;
	}

	[ContextMenu ("Assume value of 'From'")]
	public override void SetCurrentValueToStart ()
	{
		value = from;
	}

	[ContextMenu ("Assume value of 'To'")]
	public override void SetCurrentValueToEnd ()
	{
		value = to;
	}
}
