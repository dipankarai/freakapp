using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tween the object's alpha. Works with both UI widgets as well as renderers.
/// </summary>

public class TweenAlpha : UITweener
{
	[Range (0f, 1f)] public float from = 1f;
	[Range (0f, 1f)] public float to = 1f;

	bool mCached = false;
	Material mMat;
	SpriteRenderer mSr;
	CanvasGroup mGroup;
	Image mImg;
	Text txt;

	[System.Obsolete ("Use 'value' instead")]
	public float alpha { get { return this.value; } set { this.value = value; } }

	void Cache ()
	{
		mCached = true;
		mGroup = GetComponent<CanvasGroup> ();
		if (mGroup == null) {
			txt = GetComponent<Text> ();
		}
		if (txt == null) {
			mSr = GetComponent<SpriteRenderer> ();
		}
		if (mSr == null) {
			mImg = GetComponent<Image> ();
		}
		if (mImg == null) {
			Renderer ren = GetComponent<Renderer> ();
			if (ren != null)
				mMat = ren.material;
		}
	}

	/// <summary>
	/// Tween's current value.
	/// </summary>

	public float value {
		get {
			if (!mCached)
				Cache ();
			if (mGroup != null)
				return mGroup.alpha;
			else if (txt != null)
				return txt.color.a;
			else if (mSr != null)
				return mSr.color.a;
			else if (mImg != null)
				return mImg.color.a;
			return mMat != null ? mMat.color.a : 1f;
		}
		set {
			if (!mCached)
				Cache ();
			if (mGroup != null) {
				mGroup.alpha = value;
			} else if (txt != null) {
				Color c = txt.color;
				c.a = value;
				txt.color = c;
			} else if (mSr != null) {
				Color c = mSr.color;
				c.a = value;
				mSr.color = c;
			} else if (mImg != null) {
				Color c = mImg.color;
				c.a = value;
				mImg.color = c;
			} else if (mMat != null) {
				Color c = mMat.color;
				c.a = value;
				mMat.color = c;
			}
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished)
	{
		value = Mathf.Lerp (from, to, factor);
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenAlpha Begin (GameObject go, float duration, float alpha)
	{
		TweenAlpha comp = UITweener.Begin<TweenAlpha> (go, duration);
		comp.from = comp.value;
		comp.to = alpha;

		if (duration <= 0f) {
			comp.Sample (1f, true);
			comp.enabled = false;
		}
		return comp;
	}


	[ContextMenu ("Set 'From' to current value")]
	public override void SetStartToCurrentValue ()
	{
		from = value;
	}

	[ContextMenu ("Set 'To' to current value")]
	public override void SetEndToCurrentValue ()
	{
		to = value;
	}

	[ContextMenu ("Assume value of 'From'")]
	public override void SetCurrentValueToStart ()
	{
		value = from;
	}

	[ContextMenu ("Assume value of 'To'")]
	public override void SetCurrentValueToEnd ()
	{
		value = to;
	}
}
