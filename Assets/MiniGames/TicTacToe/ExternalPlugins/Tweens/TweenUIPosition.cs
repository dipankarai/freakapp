﻿using UnityEngine;

/// <summary>
/// Tween the object's position.
/// </summary>

public class TweenUIPosition : UITweener
{
	public Transform from;
	public Transform to;

	Transform mTrans;

	public Transform cachedTransform {
		get {
			if (mTrans == null)
				mTrans = transform;
			return mTrans;
		}
	}

	[System.Obsolete ("Use 'value' instead")]
	public Vector3 position { get { return this.value; } set { this.value = value; } }

	/// <summary>
	/// Tween's current value.
	/// </summary>

	public Vector3 value {
		get {
			return cachedTransform.position;
		}
		set {
			cachedTransform.position = value;
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished)
	{
		value = from.position * (1f - factor) + to.position * factor;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenUIPosition Begin (GameObject go, float duration, Transform from, Transform to)
	{
		TweenUIPosition comp = UITweener.Begin<TweenUIPosition> (go, duration);
		comp.from = from;
		comp.to = to;

		if (duration <= 0f) {
			comp.Sample (1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	[ContextMenu ("Set 'From' to current value")]
	public override void SetStartToCurrentValue ()
	{
		if (from != null)
			from.position = value;
	}

	[ContextMenu ("Set 'To' to current value")]
	public override void SetEndToCurrentValue ()
	{
		if (to != null)
			to.position = value;
	}

	[ContextMenu ("Assume value of 'From'")]
	public override void SetCurrentValueToStart ()
	{
		if (from != null)
			value = from.position;
	}

	[ContextMenu ("Assume value of 'To'")]
	public override void SetCurrentValueToEnd ()
	{
		if (to != null)
			value = to.position;
	}
}
