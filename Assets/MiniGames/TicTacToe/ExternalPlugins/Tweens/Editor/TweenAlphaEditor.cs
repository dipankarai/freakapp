using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(TweenAlpha))]
public class TweenAlphaEditor : UITweenerEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space (6f);
		MyEditorTools.SetLabelWidth (120f);

		TweenAlpha tw = target as TweenAlpha;
		GUI.changed = false;

		float from = EditorGUILayout.Slider ("From", tw.from, 0f, 1f);
		float to = EditorGUILayout.Slider ("To", tw.to, 0f, 1f);

		if (GUI.changed) {
			MyEditorTools.RegisterUndo ("Tween Change", tw);
			tw.from = from;
			tw.to = to;
			MyTools.SetDirty (tw);
		}

		DrawCommonProperties ();
	}
}
