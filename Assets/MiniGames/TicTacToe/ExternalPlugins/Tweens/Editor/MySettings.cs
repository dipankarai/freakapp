using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Unity doesn't keep the values of static variables after scripts change get recompiled. One way around this
/// is to store the references in EditorPrefs -- retrieve them at start, and save them whenever something changes.
/// </summary>

public class MySettings
{
	public enum ColorMode
	{
		Orange,
		Green,
		Blue,
	}

#region Generic Get and Set methods
	/// <summary>
	/// Save the specified boolean value in settings.
	/// </summary>

	static public void SetBool (string name, bool val) { EditorPrefs.SetBool(name, val); }

	/// <summary>
	/// Save the specified integer value in settings.
	/// </summary>

	static public void SetInt (string name, int val) { EditorPrefs.SetInt(name, val); }

	/// <summary>
	/// Save the specified float value in settings.
	/// </summary>

	static public void SetFloat (string name, float val) { EditorPrefs.SetFloat(name, val); }

	/// <summary>
	/// Save the specified string value in settings.
	/// </summary>

	static public void SetString (string name, string val) { EditorPrefs.SetString(name, val); }

	/// <summary>
	/// Save the specified color value in settings.
	/// </summary>

	static public void SetColor (string name, Color c) { SetString(name, c.r + " " + c.g + " " + c.b + " " + c.a); }

	/// <summary>
	/// Save the specified enum value to settings.
	/// </summary>

	static public void SetEnum (string name, System.Enum val) { SetString(name, val.ToString()); }

	/// <summary>
	/// Save the specified object in settings.
	/// </summary>

	static public void Set (string name, Object obj)
	{
		if (obj == null)
		{
			EditorPrefs.DeleteKey(name);
		}
		else
		{
			if (obj != null)
			{
				string path = AssetDatabase.GetAssetPath(obj);

				if (!string.IsNullOrEmpty(path))
				{
					EditorPrefs.SetString(name, path);
				}
				else
				{
					EditorPrefs.SetString(name, obj.GetInstanceID().ToString());
				}
			}
			else EditorPrefs.DeleteKey(name);
		}
	}

	/// <summary>
	/// Get the previously saved boolean value.
	/// </summary>

	static public bool GetBool (string name, bool defaultValue) { return EditorPrefs.GetBool(name, defaultValue); }

	/// <summary>
	/// Get the previously saved integer value.
	/// </summary>

	static public int GetInt (string name, int defaultValue) { return EditorPrefs.GetInt(name, defaultValue); }

	/// <summary>
	/// Get the previously saved float value.
	/// </summary>

	static public float GetFloat (string name, float defaultValue) { return EditorPrefs.GetFloat(name, defaultValue); }

	/// <summary>
	/// Get the previously saved string value.
	/// </summary>

	static public string GetString (string name, string defaultValue) { return EditorPrefs.GetString(name, defaultValue); }
	
	/// <summary>
	/// Get a previously saved color value.
	/// </summary>

	static public Color GetColor (string name, Color c)
	{
		string strVal = GetString(name, c.r + " " + c.g + " " + c.b + " " + c.a);
		string[] parts = strVal.Split(' ');

		if (parts.Length == 4)
		{
			float.TryParse(parts[0], out c.r);
			float.TryParse(parts[1], out c.g);
			float.TryParse(parts[2], out c.b);
			float.TryParse(parts[3], out c.a);
		}
		return c;
	}

	/// <summary>
	/// Get a previously saved enum from settings.
	/// </summary>

	static public T GetEnum<T> (string name, T defaultValue)
	{
		string val = GetString(name, defaultValue.ToString());
		string[] names = System.Enum.GetNames(typeof(T));
		System.Array values = System.Enum.GetValues(typeof(T));
		
		for (int i = 0; i < names.Length; ++i)
		{
			if (names[i] == val)
				return (T)values.GetValue(i);
		}
		return defaultValue;
	}

	/// <summary>
	/// Get a previously saved object from settings.
	/// </summary>

	static public T Get<T> (string name, T defaultValue) where T : Object
	{
		string path = EditorPrefs.GetString(name);
		if (string.IsNullOrEmpty(path)) return null;
		
		T retVal = MyEditorTools.LoadAsset<T>(path);
		
		if (retVal == null)
		{
			int id;
			if (int.TryParse(path, out id))
				return EditorUtility.InstanceIDToObject(id) as T;
		}
		return retVal;
	}
#endregion

#region Convenience accessor properties

	static public bool showTransformHandles
	{
		get { return GetBool("My Transform Handles", false); }
		set { SetBool("My Transform Handles", value); }
	}

	static public bool minimalisticLook
	{
		get { return GetBool("My Minimalistic", false); }
		set { SetBool("My Minimalistic", value); }
	}

	static public bool unifiedTransform
	{
		get { return GetBool("My Unified", false); }
		set { SetBool("My Unified", value); }
	}

	static public Color color
	{
		get { return GetColor("My Color", Color.white); }
		set { SetColor("My Color", value); }
	}

	static public Color foregroundColor
	{
		get { return GetColor("My FG Color", Color.white); }
		set { SetColor("My FG Color", value); }
	}

	static public Color backgroundColor
	{
		get { return GetColor("My BG Color", Color.black); }
		set { SetColor("My BG Color", value); }
	}

	static public ColorMode colorMode
	{
		get { return GetEnum("My Color Mode", ColorMode.Blue); }
		set { SetEnum("My Color Mode", value); }
	}

	static public Texture texture
	{
		get { return Get<Texture>("My Texture", null); }
		set { Set("My Texture", value); }
	}

	static public Sprite sprite2D
	{
		get { return Get<Sprite>("My Sprite2D", null); }
		set { Set("My Sprite2D", value); }
	}

	static public string selectedSprite
	{
		get { return GetString("My Sprite", null); }
		set { SetString("My Sprite", value); }
	}
		
	static public int layer
	{
		get
		{
			int layer = GetInt("My Layer", -1);
			if (layer == -1) layer = LayerMask.NameToLayer("UI");
			if (layer == -1) layer = LayerMask.NameToLayer("2D UI");
			return (layer == -1) ? 9 : layer;
		}
		set
		{
			SetInt("My Layer", value);
		}
	}

	static public TextAsset fontData
	{
		get { return Get<TextAsset>("My Font Data", null); }
		set { Set("My Font Data", value); }
	}

	static public Texture2D fontTexture
	{
		get { return Get<Texture2D>("My Font Texture", null); }
		set { Set("My Font Texture", value); }
	}

	static public int fontSize
	{
		get { return GetInt("My Font Size", 16); }
		set { SetInt("My Font Size", value); }
	}

	static public int FMSize
	{
		get { return GetInt("My FM Size", 16); }
		set { SetInt("My FM Size", value); }
	}

	static public bool fontKerning
	{
		get { return GetBool("My Font Kerning", true); }
		set { SetBool("My Font Kerning", value); }
	}

	static public FontStyle fontStyle
	{
		get { return GetEnum("My Font Style", FontStyle.Normal); }
		set { SetEnum("My Font Style", value); }
	}

	static public Font dynamicFont
	{
		get { return Get<Font>("My Dynamic Font", null); }
		set { Set("My Dynamic Font", value); }
	}

	static public Font FMFont
	{
		get { return Get<Font>("My FM Font", null); }
		set { Set("My FM Font", value); }
	}

	static public string partialSprite
	{
		get { return GetString("My Partial", null); }
		set { SetString("My Partial", value); }
	}

	static public int atlasPadding
	{
		get { return GetInt("My Padding", 1); }
		set { SetInt("My Padding", value); }
	}

	static public bool atlasTrimming
	{
		get { return GetBool("My Trim", true); }
		set { SetBool("My Trim", value); }
	}

	static public bool atlasPMA
	{
		get { return GetBool("My PMA", false); }
		set { SetBool("My PMA", value); }
	}

	static public bool unityPacking
	{
		get { return GetBool("My Packing", true); }
		set { SetBool("My Packing", value); }
	}

	static public bool trueColorAtlas
	{
		get { return GetBool("My Truecolor", true); }
		set { SetBool("My Truecolor", value); }
	}

	static public bool keepPadding
	{
		get { return GetBool("My KeepPadding", false); }
		set { SetBool("My KeepPadding", value); }
	}

	static public bool forceSquareAtlas
	{
		get { return GetBool("My Square", false); }
		set { SetBool("My Square", value); }
	}

	static public bool allow4096
	{
		get { return GetBool("My 4096", true); }
		set { SetBool("My 4096", value); }
	}

	static public bool showAllDCs
	{
		get { return GetBool("My DCs", true); }
		set { SetBool("My DCs", value); }
	}

	static public bool drawGuides
	{
		get { return GetBool("My Guides", false); }
		set { SetBool("My Guides", value); }
	}

	static public string charsToInclude
	{
		get { return GetString("My Chars", ""); }
		set { SetString("My Chars", value); }
	}

	static public string searchField
	{
		get { return GetString("My Search", null); }
		set { SetString("My Search", value); }
	}

	static public string currentPath
	{
		get { return GetString("My Path", "Assets/"); }
		set { SetString("My Path", value); }
	}
#endregion
}
