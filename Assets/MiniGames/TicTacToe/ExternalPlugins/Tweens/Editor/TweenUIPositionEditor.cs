﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof(TweenUIPosition))]
public class TweenUIPositionEditor : UITweenerEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space (6f);
		MyEditorTools.SetLabelWidth (120f);

		TweenUIPosition tw = target as TweenUIPosition;
		GUI.changed = false;

		Transform from = EditorGUILayout.ObjectField ("From", tw.from, typeof(Transform), true) as Transform;
		Transform to = EditorGUILayout.ObjectField ("To", tw.to, typeof(Transform), true) as Transform;

		if (GUI.changed) {
			MyEditorTools.RegisterUndo ("Tween Change", tw);
			tw.from = from;
			tw.to = to;
			MyTools.SetDirty (tw);
		}

		DrawCommonProperties ();
	}
}
