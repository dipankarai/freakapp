﻿using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;

[CustomPropertyDrawer (typeof(SimpleButtonAttribute))]
public class SimpleButtonDrawer : DecoratorDrawer
{
	//Unity GUI function for Property Drawer
	public override void OnGUI (Rect _Position)
	{
		// First get the attribute
		SimpleButtonAttribute tAttribute = attribute as SimpleButtonAttribute;

		Component comp = Selection.activeGameObject.GetComponent (tAttribute.ClassType);
		UnityEngine.Object theObject = null;
		if (comp != null)
			theObject = comp as UnityEngine.Object;
		else
			theObject = MonoBehaviour.FindObjectOfType (tAttribute.ClassType);

		if (GUI.Button (_Position, tAttribute.ButtonName)) {
			//Match found get the function in MethodInfo for Invoke
			//Debug.Log("Found match:  " + TargetObjects[i].GetType() + "   " + tFunction.ClassType);
			MethodInfo tMethod = theObject.GetType ().GetMethod (tAttribute.FunctionName,BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
			if (tMethod != null) {
				//Invoke the method if != null Note: It works only of you dont need special parameters! (null)
				tMethod.Invoke (theObject, null);
			}
		}
	}

	public override float GetHeight ()
	{
		return base.GetHeight () * 1f;
	}
}