﻿using UnityEngine;

namespace AnimationOrTween
{
	public enum Direction
	{
		Reverse = -1,
		Toggle = 0,
		Forward = 1,
	}

	public enum EnableCondition
	{
		DoNothing = 0,
		EnableThenPlay,
		IgnoreDisabledState,
	}

	public enum DisableCondition
	{
		DisableAfterReverse = -1,
		DoNotDisable = 0,
		DisableAfterForward = 1,
	}
}
