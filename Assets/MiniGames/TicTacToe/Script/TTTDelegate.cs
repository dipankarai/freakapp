﻿using System.Collections.Generic;

public class TTTDelegate  
{
	public delegate void DelegateTTTSendRequest(List<int> user_id);
	public delegate void DelegateTTTGameId(int game_id);
	public delegate void DelegateTTTGameStatus(string message);
}