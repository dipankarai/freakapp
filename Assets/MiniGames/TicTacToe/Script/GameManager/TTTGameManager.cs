﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using FreakGame;

public class TTTGameManager: GenericSingletonClass <TTTGameManager> 
{

//	public int user_id = 72;
//	public string global_accessToken = "6ba131e8666988ca37f104a65971dfef";

//	public int user_id = 22;
//	public string global_accessToken = "dc1c7d08385c9adaa90d8f922be7d493";
//
//	public int game_id = -1;

    public static bool canDestroyOnload = true;

    public int user_id = FreakAppManager.Instance.userID;// GameManager.Instance.user_Id;
    public string global_accessToken = FreakAppManager.Instance.APIkey;// GameManager.Instance.chat_Access_Token;
    public int game_id = FreakAppManager.Instance.currentGameData.game_id;// GameManager.Instance.game_id;

	public string user_name = "";
	public string user_imageURL = "";
	public Texture2D user_image_tex;
 
	public string accessToken;
//	public string accessToken = "31829d274659220fbf159138f18d62fb";

	public int opponent_user_id = -1;
	public int opponent_user_score = -1;
	public string opponent_user_name = "";
	public string opponent_user_imageURL = "";
	public Texture2D opponent_user_image_tex;

	public int last_status_id = 0;
	public string opponent_move = "";

	public gameMode game_Mode = gameMode.singlePlayer;

	public DateTime playerTurnStartTime;

	private System.Action<bool> m_callback;

	public List<ServerClass.ContactsData> contactsData = new List<ServerClass.ContactsData>();

	public void Init(System.Action<bool> callback,Texture2D userTex, Texture2D opponentTex)
	{
		m_callback = callback;
		user_image_tex = userTex;
		opponent_user_image_tex = opponentTex;

        ServerManager.Instance.GlobalLoginUser(OnUserLoggedIn);

        GetUserProfileFromFreak();
	}

    #region DIPANKER IMPLEMENTATION
    void GetUserProfileFromFreak ()
    {
        TTTGameManager.Instance.user_name = FreakAppManager.Instance.currentGameData.user_name;
        TTTGameManager.Instance.user_imageURL = FreakAppManager.Instance.profileLink;

        if (string.IsNullOrEmpty (TTTGameManager.Instance.user_imageURL) == false)
        {
            TTTGameManager.Instance.user_image_tex = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(TTTGameManager.Instance.user_imageURL, Freak.FolderLocation.Profile, null);
            
            if (TTTGameManager.Instance.user_image_tex == null)
            {
                Invoke("GetUserProfileFromFreak", 1f);
            }
        }
    }
    #endregion

	void OnUserLoggedIn(bool success)
	{
		if(success)
		{
			m_callback(true);
		}
		else
		{
			m_callback(false);
		}
	}

	public string GetFullTimeString ( double seconds )
	{
		TimeSpan t = TimeSpan.FromSeconds(seconds);

//		if (t.Hours > 0)
//			return string.Format("{0} hours",
//				t.Hours
//			);
//		else if (t.Minutes > 0)
//			return string.Format("{0} mins {1} secs",
//				t.Minutes,
//				t.Seconds
//			);
//		else if (t.Seconds > 0)
//			return string.Format("{0} secs",
//				t.Seconds
//			);
//		else
//			return "Ready";

		if (t.Hours < 1)
			return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds );
		else
			return string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds );
	}

	public string GetFullTimeSecondsString ( double seconds )
	{
		TimeSpan t = TimeSpan.FromSeconds(seconds);

		//		if (t.Hours > 0)
		//			return string.Format("{0} hours",
		//				t.Hours
		//			);
		//		else if (t.Minutes > 0)
		//			return string.Format("{0} mins {1} secs",
		//				t.Minutes,
		//				t.Seconds
		//			);
		//		else if (t.Seconds > 0)
		//			return string.Format("{0} secs",
		//				t.Seconds
		//			);
		//		else
		//			return "Ready";
		return string.Format("{0:D2}",  t.Seconds );
	}

	public void ResetOpponent()
	{
		opponent_user_id = -1;
		opponent_user_score = -1;
		opponent_user_name = "";
		opponent_move = "";
	}
}
