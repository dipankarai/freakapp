﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Freak.Chat;

public class uiMultiPlayerLobby : MonoBehaviour 
{
	[SimpleButton("EnableTween",typeof(uiMultiPlayerLobby))]
	public double timeOutTime = 45;
	private DateTime startTime;

	public Image player1Image;
	public Text player1Status;

	public Image player2Image;
	public Text player2Status;

	public Button challengeModeButton;
	public Button chooseAnotherOpponentButton;
	public Button backButton;

	public GameObject requestingGamePanel;
	public GameObject cancelledGamePanel;
	public Text timerText;

	public List<UITweener> uiTweens;

	private System.Action m_ChallengeModeButtonClickedCallback = null;
	private System.Action m_ChooseAnotherButtonClickedCallback = null;
	private System.Action m_CancelBButtonClickedCallback = null;
	private System.Action m_MultiplayerStartGameCallback = null;

	void Start()
	{
        /*if(TTTGameManager.Instance.user_image_tex != null)
		{
			Texture2D tex = TTTGameManager.Instance.user_image_tex;
			player1Image.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
		}*/

        LoadPlayerImage();
        LoadOpponentImage();

        if (string.IsNullOrEmpty(TTTGameManager.Instance.user_name) == false)
        {
            player1Status.text = TTTGameManager.Instance.user_name;
        }

        if (string.IsNullOrEmpty(TTTGameManager.Instance.opponent_user_name) == false)
        {
            player2Status.text = TTTGameManager.Instance.opponent_user_name;
        }

		challengeModeButton.onClick.AddListener(OnChallengeModeButtonClicked);
		chooseAnotherOpponentButton.onClick.AddListener(OnChooseAnotherButtonClicked);
		backButton.onClick.AddListener(OnCancelButtonClicked);
	}

	void OnEnable()
	{
        canShowTimer = false;
		EnableTween();
		player2Status.text = "";
//		player2Image.gameObject.SetActive(false);

		ServerManager.Instance.GetUserDetail(OnUserDetailReceived,TTTGameManager.Instance.opponent_user_id);
        /*startTime = DateTime.UtcNow;
		ActivateRequestingPanel(true);*/
	}

    bool canShowTimer;
    public void StartWaitingTimer ()
    {
        startTime = DateTime.UtcNow;
        canShowTimer = true;
        ActivateRequestingPanel(true);
    }

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void OnUserDetailReceived(bool success)
	{
		if(success)
		{
			player1Status.text = TTTGameManager.Instance.user_name;
			player2Status.text = TTTGameManager.Instance.opponent_user_name;

            /*if(TTTGameManager.Instance.opponent_user_image_tex != null)
			{
				Texture2D tex = TTTGameManager.Instance.opponent_user_image_tex;
				player2Image.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
			}

			player2Image.gameObject.SetActive(true);*/
            LoadOpponentImage();
		}

        /*if(TTTGameManager.Instance.user_image_tex != null)
		{
			Texture2D tex = TTTGameManager.Instance.user_image_tex;
			player1Image.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
		}*/

        LoadPlayerImage();
	}


    void LoadPlayerImage ()
    {
        if(TTTGameManager.Instance.user_image_tex != null)
        {
//            Texture2D tex = TTTGameManager.Instance.user_image_tex;
//            player1Image.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
            player1Image.FreakSetSprite(TTTGameManager.Instance.user_image_tex);
        }
    }

    void LoadOpponentImage ()
    {
        if(TTTGameManager.Instance.opponent_user_image_tex != null)
        {
//            Texture2D tex = TTTGameManager.Instance.opponent_user_image_tex;
//            player2Image.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
            player2Image.FreakSetSprite(TTTGameManager.Instance.opponent_user_image_tex);
        }

        player2Image.gameObject.SetActive(true);
    }

	public void Init(System.Action onChallengeModeButtonClickedCallback,System.Action onChooseAnotherButtonClickedCallback,System.Action onCancelBButtonClickedCallback,System.Action onMultiplayerStartGameCallback) 
	{
		m_ChallengeModeButtonClickedCallback = onChallengeModeButtonClickedCallback;
		m_ChooseAnotherButtonClickedCallback = onChooseAnotherButtonClickedCallback;
		m_CancelBButtonClickedCallback = onCancelBButtonClickedCallback;
		m_MultiplayerStartGameCallback = onMultiplayerStartGameCallback;
	}

	void OnChallengeModeButtonClicked()
	{
		CancelCurrentGame();
		if(m_ChallengeModeButtonClickedCallback != null)
		{
			m_ChallengeModeButtonClickedCallback();
		}
	}

	void OnChooseAnotherButtonClicked()
	{
		CancelCurrentGame();
		TTTGameManager.Instance.opponent_user_id = -1;

		if(m_ChooseAnotherButtonClickedCallback != null)
		{
			m_ChooseAnotherButtonClickedCallback();
		}

	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnCancelButtonClicked();
    }

	void OnCancelButtonClicked()
	{
		CancelCurrentGame();

		if(m_CancelBButtonClickedCallback != null)
		{
			m_CancelBButtonClickedCallback();
		}
	}

	public void OnRequestSent(int game_id)
	{
		ServerManager.Instance.GetGameStatus(OnGameStatusCallback,game_id);
	}

	void OnGameStatusCallback(string message)
	{
		if(message != "")
		{
			if(m_MultiplayerStartGameCallback != null)
			{
				m_MultiplayerStartGameCallback();
			}
		}
	}

	public void CancelCurrentGame()
	{
		ServerManager.Instance.CancelGame(OnGameCancelled,TTTGameManager.Instance.game_id);
	}

	void OnGameCancelled(bool success)
	{
	}

	void Update()
	{
        if (canShowTimer)
        {
            timerText.text = TTTGameManager.Instance.GetFullTimeSecondsString((DateTime.UtcNow - startTime).TotalSeconds);
            if((DateTime.UtcNow - startTime).TotalSeconds > timeOutTime && (!cancelledGamePanel.activeSelf))
            {
                CancelCurrentGame();
                ActivateRequestingPanel(false);
                canShowTimer = false;
            }
        }
        else
        {
            timerText.text = "0";
        }
	}

	void ActivateRequestingPanel(bool value)
	{
		requestingGamePanel.SetActive(value);
		cancelledGamePanel.SetActive(!value);
	}
}
