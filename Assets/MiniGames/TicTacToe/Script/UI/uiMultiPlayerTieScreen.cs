﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class uiMultiPlayerTieScreen : MonoBehaviour 
{
    public Button rematchButton;
	public Button inviteButton;
	public Button mainMenuButton;
	public Text statusText;
	public Text PlayerText;
	public Image LoserImage;

    private System.Action m_BackButtonClickedCallback = null;
    private System.Action m_InviteButtonClickedCallback = null;
	private System.Action m_RematchButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start()
	{
		if(TTTGameManager.Instance.user_image_tex != null)
		{
			Texture2D tex = TTTGameManager.Instance.user_image_tex;
			LoserImage.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
		}

		mainMenuButton.onClick.AddListener(OnQuitButtonClicked);
        inviteButton.onClick.AddListener(OnInviteButtonClicked);
        rematchButton.onClick.AddListener(OnRematchButtonClicked);
	}

	void OnQuitButtonClicked()
	{
		if(m_BackButtonClickedCallback != null)
		{
			m_BackButtonClickedCallback();
		}
	}

    void OnInviteButtonClicked()
    {
        TTTGameManager.Instance.opponent_user_id = -1;

        if(m_InviteButtonClickedCallback != null)
        {
            m_InviteButtonClickedCallback();
        }
    }

	void OnRematchButtonClicked()
	{
		if(m_RematchButtonClickedCallback != null)
		{
			m_RematchButtonClickedCallback();
		}
	}

    public void Init(System.Action onBackButtonClickedCallback, System.Action onInviteButtonClickedCallback, System.Action onRematchButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
        m_InviteButtonClickedCallback = onInviteButtonClickedCallback;
		m_RematchButtonClickedCallback = onRematchButtonClickedCallback;
	}

	public void SetStatusText(string status,string playerName)
	{
		statusText.text = status;
		PlayerText.text = playerName;
	}
}
