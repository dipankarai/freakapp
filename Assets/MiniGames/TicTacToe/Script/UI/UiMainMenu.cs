﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UiMainMenu : MonoBehaviour 
{

	public AudioSource bgMusic;
	public Sprite enabledMusicImage;
	public Sprite disabledMusicImage;

	public Button backButton;
	public Button musicButton;
	public Button singlePlayerModeButton;
	public Button challengeModeButton;

	public GameObject loadingPanle;

	public List<UITweener> uiTweens;

	private System.Action m_OnSinglePlayerModeButtonClickedCallback = null;
	private System.Action m_OnChallengeModeButtonClickedCallback = null;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start () 
	{
		backButton.onClick.AddListener(OnBackButtonClicked);
		musicButton.onClick.AddListener(OnMusicButtonClicked);
		singlePlayerModeButton.onClick.AddListener(OnSinglePlayerModeButtonClicked);
		challengeModeButton.onClick.AddListener(OnChallengeModeButtonClicked);

//        DirectPlay();
	}

    void DirectPlay ()
    {
        if (FreakAppManager.Instance.currentGameData != null)
        {
            if (FreakAppManager.Instance.currentGameData.isGameSelected)
            {
                Debug.Log("FreakGame.GameManager.Instance.GameIntegrateData.is_single_mode.. " + FreakAppManager.Instance.currentGameData.is_single_mode);
                OnSinglePlayerModeButtonClicked();
                FreakAppManager.Instance.currentGameData.isGameSelected = false;
            }
        }
    }

	public void Init(System.Action onSinglePlayerModeButtonClickedCallback,
		System.Action onChallengeModeButtonClickedCallback) 
	{
		m_OnSinglePlayerModeButtonClickedCallback = onSinglePlayerModeButtonClickedCallback;
		m_OnChallengeModeButtonClickedCallback = onChallengeModeButtonClickedCallback;
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnBackButtonClicked();
    }

	void OnBackButtonClicked () 
	{
		//Application.Quit();
		loadingPanle.SetActive (true);
	}

	void OnMusicButtonClicked()
	{
		if (bgMusic.enabled)
		{
			bgMusic.enabled = false;
			musicButton.GetComponent<Image>().sprite = disabledMusicImage;
		}
		else
		{
			if (!SettingsManager.Instance.isMuteGame) {
				bgMusic.enabled = true;
			}
			musicButton.GetComponent<Image>().sprite = enabledMusicImage;
		}
	}

	void OnSinglePlayerModeButtonClicked () 
	{
		if (m_OnSinglePlayerModeButtonClickedCallback != null)
			m_OnSinglePlayerModeButtonClickedCallback();
	}

	void OnChallengeModeButtonClicked () 
	{
		if (m_OnChallengeModeButtonClickedCallback != null)
			m_OnChallengeModeButtonClickedCallback();
	}
}
