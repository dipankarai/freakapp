﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class uiLoadingPanel : MonoBehaviour 
{
	public Image LoadingImage;

	void Update () 
	{
		if(LoadingImage.fillClockwise)
		{
			LoadingImage.fillAmount += 0.01f;
			if(LoadingImage.fillAmount == 1)
			{
				LoadingImage.fillClockwise = false;
			}
		}
		else
		{
			LoadingImage.fillAmount -= 0.01f;
			if(LoadingImage.fillAmount == 0)
			{
				LoadingImage.fillClockwise = true;
			}
		}
	}
}
