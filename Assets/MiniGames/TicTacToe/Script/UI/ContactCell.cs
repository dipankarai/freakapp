﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Freak.Chat;

public class ContactCell : MonoBehaviour 
{
//    private ServerUserContact mServerUserContact;
    private ServerClass.ContactsData mContactsData;

    public int userId;
    public string name;

	public Text nameText;
    public RawImage profileImage;
    public Sprite profilePicHollder;

	// Use this for initialization
	void Start () {
	
	}
	
    /*public void InitContactCellDetails (ServerUserContact data)
    {
        mServerUserContact = data;

        userId = mServerUserContact.user_id;
        this.name = mServerUserContact.name;

        InitContactCellView();
    }*/

    public void InitContactCellDetails (ServerClass.ContactsData data)
    {
        mContactsData = data;

        userId = mContactsData.user_id;
        this.name = mContactsData.name;

        InitContactCellView();
    }

    void InitContactCellView ()
    {
//        nameText.text = mServerUserContact.name;
        nameText.text = mContactsData.name;
        ShowProfile();
    }

    void ShowProfile ()
    {
//        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);
        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mContactsData.avatar, Freak.FolderLocation.Profile, null);
        if (texture != null)
        {
            if (profileImage != null)
            {
                profileImage.texture = texture;
                profileImage.PreserveAspectToParent();
            }
        }
        else
        {
//            Freak.DownloadManager.Instance.DownloadProfile(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);
            if (profileImage != null)
            {
                profileImage.texture = profilePicHollder.texture as Texture;
                profileImage.PreserveAspectToParent();
            }
            Invoke("ShowProfile", 0.1f);
        }
    }
}
