﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiNewChallengePopup : MonoBehaviour
{
    public Image profileImage;
    public Text senderNameText, descriptionText;
    public Button acceptButton, rejectButton;

    private System.Action mAcceptButtonClickedCallback = null;
    private System.Action mRejectButtonClickedCallback = null;

	// Use this for initialization
	void Start ()
    {
        acceptButton.onClick.AddListener(OnAcceptButtonClicked);
        rejectButton.onClick.AddListener(OnRejectButtonClicked);
	}

    public void Init (System.Action onAcceptButtonClickedCallback, System.Action onRejectButtonClickedCallback)
    {
        mAcceptButtonClickedCallback = onAcceptButtonClickedCallback;
        mRejectButtonClickedCallback = onRejectButtonClickedCallback;
    }

    public void NewChallengeRequest (string profileLink, string senderName)
    {
        senderNameText.text = senderName;
        descriptionText.text = senderName + " send you for challenge.";


    }

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnRejectButtonClicked();
    }

    void OnAcceptButtonClicked()
    {
        if(mAcceptButtonClickedCallback != null)
        {
            mAcceptButtonClickedCallback();
        }
    }

    void OnRejectButtonClicked()
    {
        if(mRejectButtonClickedCallback != null)
        {
            mRejectButtonClickedCallback();
        }
    }
}
