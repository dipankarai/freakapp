﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class uiLoseScreen : MonoBehaviour 
{
	public Button retryButton;
	public Button mainMenuButton;
	public Button quitButton;
	public Text LoserNameText;
	public Image LoserImage;
	public Text playerStatusText;

	private System.Action m_BackButtonClickedCallback = null;
	private System.Action m_RetryButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start()
	{
		if(TTTGameManager.Instance.user_image_tex != null)
		{
			Texture2D tex = TTTGameManager.Instance.user_image_tex;
			LoserImage.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
		}

		quitButton.onClick.AddListener(OnQuitButtonClicked);
		mainMenuButton.onClick.AddListener(OnQuitButtonClicked);
		retryButton.onClick.AddListener(OnRetryButtonClicked);
	}

	void OnQuitButtonClicked()
	{
		if(m_BackButtonClickedCallback != null)
		{
			m_BackButtonClickedCallback();
		}
	}

	void OnRetryButtonClicked()
	{
		if(m_RetryButtonClickedCallback != null)
		{
			m_RetryButtonClickedCallback();
		}
	}

	public void Init(System.Action onBackButtonClickedCallback,System.Action onRetryButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
		m_RetryButtonClickedCallback = onRetryButtonClickedCallback;
	}

	public void SetResultText(string resultText)
	{
		playerStatusText.text = resultText;
		LoserNameText.text = TTTGameManager.Instance.user_name;
	}


	public void DisplaySinglePlayerScreen()
	{
		retryButton.gameObject.SetActive(true);
		mainMenuButton.gameObject.SetActive(true);
		quitButton.gameObject.SetActive(false);
	}

	public void DisplayChallengeScreen(double score)
	{
		retryButton.gameObject.SetActive(false);
		mainMenuButton.gameObject.SetActive(false);
		quitButton.gameObject.SetActive(true);
	}
}
