﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class uiNoInternetPopup : MonoBehaviour 
{

	public Button retryBtn;
	public DateTime lastActiveTime;

	private System.Action m_RetryButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start()
	{
		retryBtn.onClick.AddListener(OnRetryButtonClicked);
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnRetryButtonClicked();
    }

	void OnRetryButtonClicked()
	{
		if(m_RetryButtonClickedCallback != null)
		{
			m_RetryButtonClickedCallback();
		}
	}

	public void Init(System.Action onRetryButtonClickedCallback) 
	{
		m_RetryButtonClickedCallback = onRetryButtonClickedCallback;
	}


}
