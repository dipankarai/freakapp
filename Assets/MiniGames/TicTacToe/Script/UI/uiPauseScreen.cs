﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class uiPauseScreen : MonoBehaviour 
{
	public Button resumeButton;
	public Button restartButton;
	public Button forfeitButton;

	private System.Action m_ResumeButtonClickedCallback = null;
	private System.Action m_RestartButtonClickedCallback = null;
	private System.Action m_ForfeitButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void Start () 
	{
		resumeButton.onClick.AddListener(OnResumeButtonClicked);
		restartButton.onClick.AddListener(OnRestartButtonClicked);
		forfeitButton.onClick.AddListener(OnForfeitButtonClicked);
	}

	void OnEnable()
	{
		EnableTween();

		if(TTTGameManager.Instance.game_Mode == gameMode.multiPlayer)
		{
			restartButton.interactable = false;
		}
		else
		{
			restartButton.interactable = true;
		}
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	public void Init(System.Action onResumeButtonClickedCallback,System.Action onRestartButtonClickedCallback,System.Action onForfeitButtonClickedCallback) 
	{
		m_ResumeButtonClickedCallback = onResumeButtonClickedCallback;
		m_RestartButtonClickedCallback = onRestartButtonClickedCallback;
		m_ForfeitButtonClickedCallback = onForfeitButtonClickedCallback;
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnResumeButtonClicked();
    }

	void OnResumeButtonClicked()
	{
		if(m_ResumeButtonClickedCallback != null)
		{
			m_ResumeButtonClickedCallback();
		}
	}

	void OnRestartButtonClicked()
	{
		if(m_RestartButtonClickedCallback != null)
		{
			m_RestartButtonClickedCallback();
		}
	}

	void OnForfeitButtonClicked()
	{
		if(m_ForfeitButtonClickedCallback != null)
		{
			m_ForfeitButtonClickedCallback();
		}
	}

}
