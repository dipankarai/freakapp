﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UiSinglePlayerMenu : MonoBehaviour 
{
	public Text singlePlayerText;
    public Button playButton;
	public Button backButton;

	private System.Action m_PlayButtonClickedCallback = null;
    private System.Action m_BackButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start () 
	{
		playButton.onClick.AddListener(OnPlayButtonClicked);
        backButton.onClick.AddListener(OnClickBackButton);
	}
    public void Init(System.Action onPlayButtonClickedCallback, System.Action onClickBackButton) 
	{
		m_PlayButtonClickedCallback = onPlayButtonClickedCallback;
        m_BackButtonClickedCallback = onClickBackButton;

	}

	void OnPlayButtonClicked()
	{
		if(m_PlayButtonClickedCallback != null)
		{
			m_PlayButtonClickedCallback();
		}
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnClickBackButton();
    }

	void OnClickBackButton ()
    {
        if (m_BackButtonClickedCallback != null)
        {
            m_BackButtonClickedCallback();
        }
	}

	public void SetSinglePlayerLobbyText(string text)
	{
		singlePlayerText.text = text;
	}
}
