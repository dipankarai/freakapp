﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiMultiPlayerResultScreen : MonoBehaviour 
{
	public uiMultiPlayerWinScreen winScreenController;
	public uiMultiPlayerTieScreen tieScreenController;

	private System.Action m_BackButtonClickedCallback = null;
    private System.Action m_InviteButtonClickedCallback = null;
    private System.Action m_RematchButtonClickedCallback = null;

    public void Init(System.Action onBackButtonClickedCallback,System.Action onInviteButtonClickedCallback, System.Action onRematchButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
        m_InviteButtonClickedCallback = onInviteButtonClickedCallback;
		m_RematchButtonClickedCallback = onRematchButtonClickedCallback;
	}

	public void SetResultText(Result resultStatus,bool isWin, string Player)
	{
		if(!isWin )
		{
			tieScreenController.gameObject.SetActive(true);
			winScreenController.gameObject.SetActive(false);

            tieScreenController.Init(m_BackButtonClickedCallback,m_InviteButtonClickedCallback,m_RematchButtonClickedCallback);

			if(resultStatus == Result.Draw )
			{
				ServerManager.Instance.SetMultiPlayerWinner(0);
				tieScreenController.SetStatusText("Draw!",Player);
            }
            else
            {
                ServerManager.Instance.SetMultiPlayerWinner(TTTGameManager.Instance.opponent_user_id);
                tieScreenController.SetStatusText("You Lose!",Player);
            }
        }
        else
        {
            winScreenController.gameObject.SetActive(true);
            tieScreenController.gameObject.SetActive(false);

            winScreenController.Init(m_BackButtonClickedCallback,m_InviteButtonClickedCallback,m_RematchButtonClickedCallback);
            winScreenController.SetWinPlayerText(Player);

            ServerManager.Instance.PostNewsFeedsAPICall(TTTGameManager.Instance.user_name + " defeated " + TTTGameManager.Instance.opponent_user_name + " in Tic Tac Toe.", "1");
//            FreakGame.GameManager.Instance.ShareScore(TTTGameManager.Instance.user_name + " Won in Tic Tac Toe");
		}
	}
}
