﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiResultScreen : MonoBehaviour 
{
	public uiWinScreen winScreenController;
	public uiLoseScreen loseScreenController;

	private System.Action m_BackButtonClickedCallback = null;
	private System.Action m_RetryButtonClickedCallback = null;
	private System.Action m_ChallengeButtonClickedCallback = null;


	public void Init(System.Action onBackButtonClickedCallback,System.Action onRetryButtonClickedCallback,System.Action onChallengeButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
		m_RetryButtonClickedCallback = onRetryButtonClickedCallback;
		m_ChallengeButtonClickedCallback = onChallengeButtonClickedCallback;
	}

	public void SetResultText(Result resultStatus, int winnerPlayer, bool isSinglePlayer,double score)
	{
		if(isSinglePlayer)
		{
			if(resultStatus == Result.Draw || winnerPlayer == GameInfo._PLAYER2)
			{
				loseScreenController.gameObject.SetActive(true);
				winScreenController.gameObject.SetActive(false);

				loseScreenController.Init(m_BackButtonClickedCallback,m_RetryButtonClickedCallback);
				if(TTTGameManager.Instance.opponent_user_id == -1 || TTTGameManager.Instance.game_id == -1)
				{
					if(resultStatus == Result.Draw)
					{
						loseScreenController.SetResultText("Draw!");
					}
					else
					{
						loseScreenController.SetResultText("You LOSE!");
					}
				}
				else
				{
					loseScreenController.SetResultText("YOU LOSE!");
					SendChallengeResult(1000);
				}

				loseScreenController.DisplaySinglePlayerScreen();
			}
			else
			{
				winScreenController.gameObject.SetActive(true);
				loseScreenController.gameObject.SetActive(false);

				winScreenController.Init(m_BackButtonClickedCallback,m_ChallengeButtonClickedCallback);
				winScreenController.SetResultText(score);

				if(TTTGameManager.Instance.opponent_user_id == -1 || TTTGameManager.Instance.game_id == -1)
				{
					winScreenController.DisplaySinglePlayerScreen();
				}
				else
				{
					if(score < TTTGameManager.Instance.opponent_user_score)
					{
						
						winScreenController.DisplayChallengeScreen(score);
					}
					else
					{
						loseScreenController.gameObject.SetActive(true);
						winScreenController.gameObject.SetActive(false);

						loseScreenController.Init(m_BackButtonClickedCallback,m_RetryButtonClickedCallback);
						if(score == TTTGameManager.Instance.opponent_user_score)
						{
							loseScreenController.SetResultText("DRAW!");
						}
						else
						{
							loseScreenController.SetResultText("YOU LOSE!");
						}

						loseScreenController.DisplayChallengeScreen(score);
					}

					SendChallengeResult(score);
				}
			}
		}
	}

	void SendChallengeResult(double score)
	{
		ServerManager.Instance.AcceptChallengeGame(score);
	}
}
