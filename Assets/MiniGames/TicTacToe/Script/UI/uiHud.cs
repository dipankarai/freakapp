﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class uiHud : MonoBehaviour 
{
	public double timeOutTime = 180;

	public Button pauseButton;
	public Text timerText;
	public double score;

	private DateTime startTime;

	private System.Action m_PauseButtonClickedCallback = null;
	private System.Action m_GameForfeitCallback = null;

	public List<UITweener> uiTweens;

	void Start()
	{
		pauseButton.onClick.AddListener(OnPauseButtonClicked);
	}

	void OnPauseButtonClicked()
	{
		if(m_PauseButtonClickedCallback != null)
		{
			m_PauseButtonClickedCallback();
		}
	}

	void OnEnable () 
	{
		EnableTween();

		startTime = DateTime.UtcNow;
		TTTGameManager.Instance.playerTurnStartTime = DateTime.UtcNow;
		Debug.Log(startTime);
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	public void Init(System.Action onPauseButtonClickedCallback,System.Action onGameForfeitCallback) 
	{
		m_PauseButtonClickedCallback = onPauseButtonClickedCallback;
		m_GameForfeitCallback = onGameForfeitCallback;
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
    }
	
	// Update is called once per frame
	void Update () 
	{
		score = (DateTime.UtcNow - startTime).TotalSeconds;
		timerText.text = TTTGameManager.Instance.GetFullTimeString(score);

//		Debug.Log((DateTime.UtcNow - TTTGameManager.Instance.playerTurnStartTime).TotalSeconds);
		if(((DateTime.UtcNow - TTTGameManager.Instance.playerTurnStartTime).TotalSeconds > timeOutTime) && (TTTGameManager.Instance.game_Mode == gameMode.multiPlayer))
		{
			ServerManager.Instance.SetMultiPlayerWinner(-1);
			if(m_GameForfeitCallback != null)
			{
				m_GameForfeitCallback();
			}
		}
	}
}
