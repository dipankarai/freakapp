﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class uiContactsScreen : MonoBehaviour 
{
	public Button sendRequestButton;
	public Button backButton;
	public GameObject contactParent;
	public GameObject contactObj;
	private List<GameObject> m_contactList = new List<GameObject>();

	private System.Action m_BackButtonClickedCallback = null;
	private TTTDelegate.DelegateTTTSendRequest m_SendRequestButtonClickedCallback = null;

	private gameMode m_gameMode;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start () 
	{
		backButton.onClick.AddListener(OnBackButtonClicked);
		sendRequestButton.onClick.AddListener(OnSendRequestButtonClicked);
	}

	public void PopulateItems(gameMode _gameMode)
	{
		m_gameMode = _gameMode;
		ServerManager.Instance.GetContactList(OnChallengeContactListReceived);
//        FreakAppManager.Instance.LoadServerContactList(ServerContactListCallback);
	}

	public void Init(System.Action onBackButtonClickedCallback,TTTDelegate.DelegateTTTSendRequest onSendRequestButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
		m_SendRequestButtonClickedCallback = onSendRequestButtonClickedCallback;
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnBackButtonClicked();
    }

	void OnBackButtonClicked()
	{
		if(m_BackButtonClickedCallback != null)
		{
			m_BackButtonClickedCallback();
		}
	}

	void OnSendRequestButtonClicked()
	{
//		IEnumerable<Toggle> currentActiveToggle = contactParent.GetComponent<ToggleGroup>().ActiveToggles

		List<int> user_id = new List<int>();

		if(m_gameMode ==  gameMode.multiPlayer)
		{
			IEnumerator<Toggle> toggleEnum = contactParent.GetComponent<ToggleGroup>().ActiveToggles().GetEnumerator();
			toggleEnum.MoveNext();
			Toggle toggle = toggleEnum.Current;

			if(toggle != null)
			{
				Debug.Log(toggle.GetComponent<ContactCell> ().userId);

				user_id.Add(toggle.GetComponent<ContactCell> ().userId);
                TTTGameManager.Instance.opponent_user_image_tex = toggle.GetComponent<ContactCell>().profileImage.texture as Texture2D;
			}
		}
		else
		{
			foreach(Transform child in contactParent.transform)
			{
				if(child.GetComponent<Toggle>().isOn)
				{
					user_id.Add(child.GetComponent<ContactCell> ().userId);
				}
			}

		}

		if(m_SendRequestButtonClickedCallback != null)
		{
			m_SendRequestButtonClickedCallback(user_id);
		}
	}

	void OnChallengeContactListReceived(bool success)
	{
		if(success)
		{
			PopulateContacts();
		}
	}

    /*void ServerContactListCallback (List<ServerUserContact> contactList)
    {
        for(int i = 0; i < m_contactList.Count;i++)
        {
            Destroy(m_contactList[i]);
        }

        m_contactList.RemoveRange(0,m_contactList.Count);

        for(int i = 0; i< contactList.Count; i++)
        {
            GameObject _tempContactCell = (GameObject) Instantiate (contactObj);

            _tempContactCell.SetActive(true);
            _tempContactCell.transform.SetParent(contactParent.transform, false);

            if(m_gameMode ==  gameMode.multiPlayer)
            {
                _tempContactCell.GetComponent<Toggle>().group = contactParent.GetComponent<ToggleGroup>();
            }

            _tempContactCell.GetComponent<ContactCell>().InitContactCellDetails(contactList[i]);
            m_contactList.Add(_tempContactCell);
        }
    }*/

	void PopulateContacts()
	{
		for(int i = 0; i < m_contactList.Count;i++)
		{
			Destroy(m_contactList[i]);
		}

		m_contactList.RemoveRange(0,m_contactList.Count);

		for(int i = 0; i< TTTGameManager.Instance.contactsData.Count; i++)
		{
			GameObject _tempContactCell = (GameObject) Instantiate (contactObj);

			_tempContactCell.SetActive(true);
			_tempContactCell.transform.SetParent(contactParent.transform, false);

			if(m_gameMode ==  gameMode.multiPlayer)
			{
				_tempContactCell.GetComponent<Toggle>().group = contactParent.GetComponent<ToggleGroup>();
			}

			ContactCell contactObject = _tempContactCell.GetComponent<ContactCell> ();

            contactObject.InitContactCellDetails(TTTGameManager.Instance.contactsData[i]);

            /*contactObject.nameText.text =  TTTGameManager.Instance.contactsData[i].name;

			contactObject.name =  TTTGameManager.Instance.contactsData[i].name;
			contactObject.userId = TTTGameManager.Instance.contactsData[i].user_id;*/

			m_contactList.Add(_tempContactCell);
		}
	}
}
