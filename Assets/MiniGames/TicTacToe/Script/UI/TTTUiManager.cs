﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum gameMode
{
	singlePlayer,
	multiPlayer 
}


public class TTTUiManager : MonoBehaviour 
{
	public bool IsInGamePlay 
	{
		get { return m_isInGamePlay; } 
		set { m_isInGamePlay =value; } 
	}
	private bool m_isInGamePlay = false;

	public bool IsPaused 
	{
		get { return m_isPaused; } 
		set { m_isPaused =value; } 
	}
	private bool m_isPaused = false;

	private double m_noInternetPopupDelay = 20;

	public Texture2D userTex;
	public Texture2D opponent_userTex;
	public GUIHandler gameController;

	public UiMainMenu m_mainMenuController;
	public UiSinglePlayerMenu m_singlePlayerMenuController;
	public UiMultiPlayerResultScreen m_multiPlayerresultScreenController;
	public UiResultScreen m_resultScreenController;
	public uiHud m_hud;
	public uiPauseScreen m_pauseScreen;
	public uiContactsScreen m_contactScreen;
	public uiMultiPlayerLobby m_multiPlayerLobbyScreen;
	public uiLoadingPanel m_LoadingPanelScreen;
	public uiMultiplayerGameForfeit m_multiPlayerGameForfeit;
    public uiNoInternetPopup m_NoInternetPopup;
    public UiNewChallengePopup m_NewChallengePopup;

	public void OnTestButtonClick()
	{
		ServerManager.Instance.CancelGame(Test,21);
	}

	void Test(bool success)
	{
	}

    void Awake ()
    {
        #if UNITY_ANDROID
        Screen.fullScreen = false;
        #else
        FreakAppManager.HideIOSStatusBar(true);
        Screen.fullScreen = true;
        #endif
    }

	void OnEnable()
	{
        GetUserImage();
    }

    void GetUserImage ()
    {
        TTTGameManager.Instance.user_image_tex = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(FreakAppManager.Instance.currentGameData.user_profile_url, Freak.FolderLocation.Profile, null);

        if (TTTGameManager.Instance.user_image_tex == null)
        {
            Invoke("GetUserImage", 0.5f);
        }
        /*if (string.IsNullOrEmpty (FreakAppManager.Instance.currentGameData.opponent_profile_url) == false)
        {
            opponent_userTex = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(FreakAppManager.Instance.currentGameData.opponent_profile_url, Freak.FolderLocation.Profile, null);
        }*/

    }

    void Start () 
    {
		ServerManager.Instance.InitUIManager(this);

        if (InternetConnection.Check ())
        {
            TTTGameManager.Instance.Init(OnUserLoggedIn,userTex,opponent_userTex);
        }
        else
        {
            TTTGameManager.Instance.user_image_tex = userTex;
            TTTGameManager.Instance.opponent_user_image_tex = opponent_userTex;
            InternetConnection.Instance.OnInternetConnectionBack = OnInternetConnectionBack;
        }

		InitUI();
		LoadPanel("pfMainMenu");

	}

    void OnInternetConnectionBack ()
    {
        TTTGameManager.Instance.Init(OnUserLoggedIn,userTex,opponent_userTex);
    }

	void OnUserLoggedIn(bool success)
	{
		if(success)
		{
			ServerManager.Instance.GetUserDetail(OnUserDetailReceived,TTTGameManager.Instance.user_id);
			
            CheckForChallenge();

            if(TTTGameManager.Instance.game_id !=  -1)
			{
				ServerManager.Instance.GetGameDetail(OnGameDetailReceived);
			}
		}
        else
        {
            Debug.LogError("USER LOGIN UNSUCCESS....");
        }
	}

	void OnUserDetailReceived(bool success)
	{
		if(success)
		{
            //Check for direct game play
            DirectPlay();
		}
	}

    #region DIPANKER IMPLEMENTATION
    void CheckForChallenge()
    {
        if (FreakAppManager.Instance.currentGameData != null)
        {
            if (FreakAppManager.Instance.currentGameData.isGameSelected)
            {
                if (FreakAppManager.Instance.currentGameData.is_single_mode == false)
                {
                    if (FreakAppManager.Instance.currentGameData.isChallengeAccepted)
                    {
                        TTTGameManager.Instance.game_id = FreakAppManager.Instance.currentGameData.game_id;
                        //                    ServerManager.Instance.AcceptMultiPlayerGame(ChallengeAcceptCallbackHandler);
                        
                        FreakAppManager.Instance.currentGameData.isGameSelected = false;
                    }
                    else
                    {
                        //SEND CHALLENGE...
                        List<int> user_id = new List<int>();
                        user_id.Add(int.Parse(FreakAppManager.Instance.currentGameData.opponent_id));
                        TTTGameManager.Instance.game_Mode = gameMode.multiPlayer;
                        GetOpponentProfileFromFreak();
                        SendRequest(user_id);
                    }
                }
            }
        }
    }

    void ChallengeAcceptCallbackHandler (bool obj)
    {
        if(TTTGameManager.Instance.game_id !=  -1)
        {
            ServerManager.Instance.GetGameDetail(OnGameDetailReceived);
        }
    }

    void DirectPlay ()
    {
        if (FreakAppManager.Instance.currentGameData != null)
        {
            if (FreakAppManager.Instance.currentGameData.isGameSelected)
            {
                Debug.Log("FreakGame.GameManager.Instance.GameIntegrateData.is_single_mode.. " + FreakAppManager.Instance.currentGameData.is_single_mode);
                if (FreakAppManager.Instance.currentGameData.is_single_mode == false && string.IsNullOrEmpty (FreakAppManager.Instance.currentGameData.opponent_id) == false)
                {

                    TTTGameManager.Instance.game_Mode = gameMode.multiPlayer;
                    TTTGameManager.Instance.opponent_user_id = int.Parse(FreakAppManager.Instance.currentGameData.opponent_id);
                    GetOpponentProfileFromFreak();

                    ServerManager.Instance.InviteGame(OnMultiPlayerInviteCallBack);
                    LoadMultiPlayerLobby();
                    
                    /*if(TTTGameManager.Instance.game_Mode == gameMode.multiPlayer)
                    {
                        TTTGameManager.Instance.opponent_user_id = int.Parse(FreakAppManager.Instance.currentGameData.opponent_id);
                        ServerManager.Instance.InviteGame(OnMultiPlayerInviteCallBack);
                        LoadMultiPlayerLobby();
                    }
                    else if(TTTGameManager.Instance.game_Mode == gameMode.singlePlayer)
                    {
                        for(int i = 0; i < opponent_user_id.Count; i++)
                        {
                            ServerManager.Instance.SendChallenge(OnSinglePlayerInviteCallBack,m_hud.score,opponent_user_id[i]);
                        }
                        LoadMainMenu();
                    }*/

                    FreakAppManager.Instance.currentGameData.isGameSelected = false;
                }
            }
        }
    }

    void GetOpponentProfileFromFreak ()
    {
        TTTGameManager.Instance.opponent_user_imageURL = FreakAppManager.Instance.currentGameData.opponent_profile_url;
        TTTGameManager.Instance.opponent_user_name = FreakAppManager.Instance.currentGameData.opponent_name;

        if (string.IsNullOrEmpty (TTTGameManager.Instance.opponent_user_imageURL) == false)
        {
            TTTGameManager.Instance.opponent_user_image_tex = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(TTTGameManager.Instance.opponent_user_imageURL, Freak.FolderLocation.Profile, null);
            
            if (TTTGameManager.Instance.opponent_user_image_tex == null)
            {
                Invoke("GetOpponentProfileFromFreak", 1f);
            }
        }
    }
    #endregion

	void OnGameDetailReceived(bool success)
	{
		if(success)
		{
			if(TTTGameManager.Instance.opponent_user_id !=  -1)
			{
				ServerManager.Instance.GetUserDetail(OnOpponentUserDetailReceived,TTTGameManager.Instance.opponent_user_id);
			}
		}
	}

	void OnOpponentUserDetailReceived(bool success)
	{
		if(success)
		{
			if(TTTGameManager.Instance.opponent_user_score == -1)
			{
				ServerManager.Instance.AcceptMultiPlayerGame(StartMultiPlayerGameWithPlayer2);
			}	
			else
			{
				LoadSinglePlayerMode();
				m_singlePlayerMenuController.SetSinglePlayerLobbyText("Beat "+TTTGameManager.Instance.opponent_user_name+"'s time "+TTTGameManager.Instance.GetFullTimeString(TTTGameManager.Instance.opponent_user_score));
			}
		}
	}


	public List<GameObject> panelList;
    private string currentPanel = string.Empty;

	void LoadPanel(string panelname)
	{
		IsPaused = false; 

        currentPanel = panelname;
        foreach(GameObject obj in panelList )
        {
            if(panelname == obj.name)
            {
                obj.SetActive(true);
			}
			else
			{
                obj.SetActive(false);
			}

		}
	}

    void OnEscapedButtonPressed (string panelList)
    {
        switch(panelList)
        {
            case "pfMainMenu":
                m_mainMenuController.OnEscapeButtonPressed();
                break;

            case "pfHUD":
                m_hud.OnEscapeButtonPressed();
                break;

            case "pfPausePanel":
                m_pauseScreen.OnEscapeButtonPressed();
                break;

            case "pfChallengeFriendPanel":
                m_contactScreen.OnEscapeButtonPressed();
                break;

            case "pfMultiplayerLobby":
                m_multiPlayerLobbyScreen.OnEscapeButtonPressed();
                break;

            case "pfMultiPlayerForfeitPanel":
                m_multiPlayerGameForfeit.OnEscapeButtonPressed();
                break;

            case "pfNoInternetPopup":
                m_NoInternetPopup.OnEscapeButtonPressed();
                break;

            case "pfSinglePlayerResultPanel":
            case "pfMultiPlayerResultPanel":
                LoadMainMenu();
                break;

            case "pfSinglePlayerModePanel":
                m_singlePlayerMenuController.OnEscapeButtonPressed();
                break;

            default:
                break;
        }
    }

    void Update () 
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnEscapedButtonPressed(currentPanel);
        }
	}

	void InitUI()
	{
		m_mainMenuController.Init(LoadSinglePlayerPanel,LoadChallengeMode);
		m_resultScreenController.Init(LoadMainMenu,StartSinglePlayerGame,ShowChallengeFriendScreen);
        m_multiPlayerresultScreenController.Init(LoadMainMenu, ShowChallengeFriendScreen, RematchGame);
        m_singlePlayerMenuController.Init(StartSinglePlayerGame, LoadPreviousPanel);
		m_hud.Init(PauseGame,OnMultiPlayerGameForfeit);
		m_pauseScreen.Init(ResumeGame,StartSinglePlayerGame,ForfeitGame);
		m_contactScreen.Init(LoadMainMenu,SendRequest);
		m_multiPlayerLobbyScreen.Init(LoadSinglePlayerMode,ShowChallengeFriendScreen,LoadMainMenu,StartMultiPlayerGameWithPlayer1);
		m_multiPlayerGameForfeit.Init(LoadMainMenu);
		m_NoInternetPopup.Init(OnNoInternetRetry);
        m_NewChallengePopup.Init(OnAcceptButtonClicked, OnRejectButtonClicked);
	}


	void OnMultiPlayerGameForfeit()
	{
		LoadPanel("pfMultiPlayerForfeitPanel");
	}

	void RematchGame()
	{
		TTTGameManager.Instance.last_status_id = 0;
		TTTGameManager.Instance.opponent_move = "";
		ServerManager.Instance.DisplayGameManagerDetails();
		ServerManager.Instance.Rematch(OnRematchSuccess);
	}

	void OnRematchSuccess(int _game_id)
	{
		if(_game_id != -1)
		{
			TTTGameManager.Instance.game_id = _game_id;
			LoadPanel("pfMultiplayerLobby");
			OnMultiPlayerInviteCallBack(_game_id);
		}
	}

	void StartMultiPlayerGameWithPlayer1()
	{
		TTTGameManager.Instance.game_Mode = gameMode.multiPlayer;
		LoadPanel("");
		gameController.SetMultiPlayer1();
	}

	void StartMultiPlayerGameWithPlayer2(bool success)
	{
		if(success)
		{
			TTTGameManager.Instance.game_Mode = gameMode.multiPlayer;
			LoadPanel("");
			gameController.SetMultiplayer2();
		}
	}

	void SendRequest(List<int> opponent_user_id)
	{
		if(opponent_user_id != null)
		{
			if(opponent_user_id.Count > 0)
			{
				if(TTTGameManager.Instance.game_Mode == gameMode.multiPlayer)
				{
					TTTGameManager.Instance.opponent_user_id = opponent_user_id[0];
					ServerManager.Instance.InviteGame(OnMultiPlayerInviteCallBack);
					LoadMultiPlayerLobby();
				}
				else if(TTTGameManager.Instance.game_Mode == gameMode.singlePlayer)
				{
					for(int i = 0; i < opponent_user_id.Count; i++)
					{
						ServerManager.Instance.SendChallenge(OnSinglePlayerInviteCallBack,m_hud.score,opponent_user_id[i]);
					}
					LoadMainMenu();
				}
			}
		}
		
	}


	void OnMultiPlayerInviteCallBack(int _game_id)
	{
        m_multiPlayerLobbyScreen.StartWaitingTimer();
        
		if(_game_id != -1)
		{
			TTTGameManager.Instance.game_id = _game_id;
			m_multiPlayerLobbyScreen.OnRequestSent(_game_id);
		}
	}

	void OnSinglePlayerInviteCallBack(int _game_id)
	{
		if(_game_id != -1)
		{
//			TTTGameManager.Instance.game_id = _game_id;
		}
	}

	void LoadMultiPlayerLobby()
	{
		LoadPanel("pfMultiplayerLobby");
	}

	void ShowChallengeFriendScreen()
	{
		if(TTTGameManager.Instance.opponent_user_id != -1)
		{
			List<int> opponent_user_id = new List<int>();
			opponent_user_id.Add(TTTGameManager.Instance.opponent_user_id);
			SendRequest(opponent_user_id);
		}
		else
		{
			LoadPanel("pfChallengeFriendPanel");
			m_contactScreen.PopulateItems(TTTGameManager.Instance.game_Mode);
		}
	}

	void ResumeGame()
	{
		IsPaused = false;
		m_pauseScreen.gameObject.SetActive(false);
	}

	void ForfeitGame()
	{
		gameController.QuitGame();
		LoadMainMenu();

	}

	void PauseGame()
	{
		IsPaused = true;
		m_pauseScreen.gameObject.SetActive(true);
	}

	void LoadMainMenu()
	{
		TTTGameManager.Instance.ResetOpponent();
		TTTGameManager.Instance.game_id = -1;
		TTTGameManager.Instance.last_status_id = 0;
		ServerManager.Instance.DisplayGameManagerDetails();
		LoadPanel("pfMainMenu");
	}

	void LoadSinglePlayerPanel()
	{
		m_singlePlayerMenuController.SetSinglePlayerLobbyText("Get Ready!!!");
		LoadSinglePlayerMode();
	}

	void LoadSinglePlayerMode()
	{
		TTTGameManager.Instance.game_Mode = gameMode.singlePlayer;
		LoadPanel("pfSinglePlayerModePanel");
	}

	void StartSinglePlayerGame()
	{
		TTTGameManager.Instance.game_Mode = gameMode.singlePlayer;
		LoadPanel("");
		gameController.SetSinglePlayer();
	}

    void LoadPreviousPanel ()
    {
        LoadPanel("pfMainMenu");
    }

	void LoadChallengeMode()
	{
        if (InternetConnection.Check() == false)
        {
            ShowNoInternetPopup(true);
            return;
        }
        
        TTTGameManager.Instance.game_Mode = gameMode.multiPlayer;
		m_mainMenuController.gameObject.SetActive(false);
		ShowChallengeFriendScreen();
	}

    public void SetPreGamePlayScreen(bool value)
	{
		m_singlePlayerMenuController.gameObject.SetActive(value);
		m_hud.gameObject.SetActive(!value);
	}

	public void SetPostGamePlayScreen(bool value,Result resultStatus, int winnerPlayer, int currentPlayer,bool isSinglePlayer)
	{
		m_hud.gameObject.SetActive(!value);
		if(isSinglePlayer)
		{
			
			m_resultScreenController.gameObject.SetActive(value);
			m_resultScreenController.SetResultText(resultStatus,winnerPlayer,isSinglePlayer,m_hud.score);
		}
		else
		{
			m_multiPlayerresultScreenController.gameObject.SetActive(value);
			if(winnerPlayer == currentPlayer)
			{
				m_multiPlayerresultScreenController.SetResultText(resultStatus,true,TTTGameManager.Instance.user_name);
			}
			else
			{
				m_multiPlayerresultScreenController.SetResultText(resultStatus,false,TTTGameManager.Instance.user_name);
			}
		}
	}

	public void ShowNoInternetPopup(bool value)
	{
		if(value)
		{
			if((m_NoInternetPopup.lastActiveTime == null) || ((DateTime.UtcNow - m_NoInternetPopup.lastActiveTime).TotalSeconds > m_noInternetPopupDelay))
			{
				m_NoInternetPopup.lastActiveTime = DateTime.UtcNow;
				m_NoInternetPopup.gameObject.SetActive(value);
			}
		}
		else
		{
			m_NoInternetPopup.gameObject.SetActive(value);
		}
	}

	void OnNoInternetRetry()
	{
		ShowNoInternetPopup(false);
	}

    void OnAcceptButtonClicked()
    {
        
    }

    void OnRejectButtonClicked()
    {

    }
}
