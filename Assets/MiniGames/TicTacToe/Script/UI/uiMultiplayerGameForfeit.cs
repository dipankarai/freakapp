﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class uiMultiplayerGameForfeit : MonoBehaviour 
{
	public Button okButton;


	private System.Action m_OkButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}

	void Start () 
	{
		okButton.onClick.AddListener(OnOkButtonClicked);
	}
		
	public void Init(System.Action onOkButtonClickedCallback) 
	{
		m_OkButtonClickedCallback = onOkButtonClickedCallback;
	}

    public void OnEscapeButtonPressed ()
    {
        Debug.Log("OnEscapeButtonPressed..........");
        OnOkButtonClicked();
    }

	void OnOkButtonClicked()
	{
		if(m_OkButtonClickedCallback != null)
		{
			m_OkButtonClickedCallback();
		}
	}

}
