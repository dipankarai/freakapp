﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class uiWinScreen : MonoBehaviour 
{
	public Button sendChallengeButton;
	public Button quitButton;
	public Button mainMenuButton;
	public Text  timerText;
	public Text winnerNameText;
	public Image winnerImage;

	private System.Action m_BackButtonClickedCallback = null;
	private System.Action m_ChallengeButtonClickedCallback = null;

	public List<UITweener> uiTweens;

	void OnEnable()
	{
		EnableTween();
	}

	void EnableTween()
	{
		if(uiTweens != null)
		{
			for(int i = 0; i < uiTweens.Count; i++)
			{
				uiTweens[i].enabled = true;
				uiTweens[i].ResetToBeginning();
				uiTweens[i].PlayForward();
			}
		}
	}


	void Start()
	{
		if(TTTGameManager.Instance.user_image_tex != null)
		{
			Texture2D tex = TTTGameManager.Instance.user_image_tex;
			winnerImage.sprite = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
		}

		mainMenuButton.onClick.AddListener(OnQuitButtonClicked);
		quitButton.onClick.AddListener(OnQuitButtonClicked);
		sendChallengeButton.onClick.AddListener(OnSendChallengeButtonClicked);
	}

	void OnQuitButtonClicked()
	{
		if(m_BackButtonClickedCallback != null)
		{
			m_BackButtonClickedCallback();
		}
	}

	public void Init(System.Action onBackButtonClickedCallback,System.Action onChallengeButtonClickedCallback) 
	{
		m_BackButtonClickedCallback = onBackButtonClickedCallback;
		m_ChallengeButtonClickedCallback = onChallengeButtonClickedCallback;
	}

	void OnSendChallengeButtonClicked()
	{
		if(m_ChallengeButtonClickedCallback != null)
		{
			m_ChallengeButtonClickedCallback();
		}
	}

	public void SetResultText(double score)
	{
		timerText.text = TTTGameManager.Instance.GetFullTimeString(score);
		winnerNameText.text = TTTGameManager.Instance.user_name;
	}

	public void DisplaySinglePlayerScreen()
	{
		sendChallengeButton.gameObject.SetActive(true);
		mainMenuButton.gameObject.SetActive(true);
		quitButton.gameObject.SetActive(false);
	}

	public void DisplayChallengeScreen(double score)
	{
		if(TTTGameManager.Instance.opponent_user_score == -1)
		{
			sendChallengeButton.gameObject.SetActive(true);
			mainMenuButton.gameObject.SetActive(true);
			quitButton.gameObject.SetActive(false);
		}
		else
		{
			sendChallengeButton.gameObject.SetActive(false);
			mainMenuButton.gameObject.SetActive(false);
			quitButton.gameObject.SetActive(true);
		}
	}
}
