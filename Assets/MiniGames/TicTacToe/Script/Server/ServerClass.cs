﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ServerClass
{

	[System.Serializable]
	public class TTTResponse
	{
		public int responseCode;
		public List<TTTResponseMsg> responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class TTTResponseMsg
	{

	}

	[System.Serializable]
	public class GlobalLoginDatabase
	{
		public int responseCode;
		public GlobalLoginData responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class GlobalLoginData
	{
		public int user_id;
		public string access_token;
	}

	[System.Serializable]
	public class ContactsDatabase
	{
		public int responseCode;
		public List<ContactsData> responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class ContactsData
	{
		public int user_contact_id;
		public int user_id;
		public string name;
		public float contact_number;
		public string avatar;
	}

	[System.Serializable]
	public class GameDetailsDatabase
	{
		public int responseCode;
		public GameDetailsData responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class GameDetailsData
	{
		public int game_id;
		public UserData user;
		public UserData opponent;
	}
	
	[System.Serializable]
	public class UserData
	{
		public int user_id;
		public string name;
		public int gender;
		public int country;
	}

	[System.Serializable]
	public class GameDetailsFromGameIdDatabase
	{
		public int responseCode;
		public GameDetailsFromGame responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class GameDetailsFromGame
	{
		public int game_id;
		public int user_id;
		public int type;
		public int challenge_user_id;
		public int user_score;
		public int challenge_user_score;
		public int winner_user_id;
		public int notification_id;
		public int previous_game_id;
		public string created_at;
		public int status;
	}

	[System.Serializable]
	public class GameStatusStartDatabase
	{
		public int responseCode;
		public GameStatusStartData responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class GameStatusStartData
	{
		public int last_status_id;
		public int status_count;
		public List<GameStatusStart> status;
	}

	[System.Serializable]
	public class GameStatusStart
	{
		public int status_id;
		public int game_id;
		public int user_id;
		public int type;
		public MessageData data;
		public string created_at;
		public int status;
	}

	[System.Serializable]
	public class MessageData
	{
		public int game_id;
		public int user_id;
		public string message;
	}


	[System.Serializable]
	public class GameStatusMovementDatabase
	{
		public int responseCode;
		public GameStatusMovementData responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class GameStatusMovementData
	{
		public int last_status_id;
		public int status_count;
		public List<GameStatusMovement> status;
	}

	[System.Serializable]
	public class GameStatusMovement
	{
		public int status_id;
		public int game_id;
		public int user_id;
		public int type;
		public MovementData data;
		public string created_at;
		public int status;
	}

	[System.Serializable]
	public class MovementData
	{
		public int block;
		public int piece;
	}


	[System.Serializable]
	public class GameMovementData
	{
		public int block;
		public pieces piece;
	}

	[System.Serializable]
	public class StatusUpdate
	{
		public int responseCode;
		public StatusResponseMsg responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class StatusResponseMsg
	{
		public string statusId;
	}

	[System.Serializable]
	public class UserDetailDatabase
	{
		public int responseCode;
		public UserDetailData responseMsg;
		public string responseInfo;
	}

	[System.Serializable]
	public class UserDetailData
	{
		public UserDetailInfo user_info;
	}

	[System.Serializable]
	public class UserDetailInfo
	{
		public int user_id;
		public string name;
		public int country;
		public int gender;
		public int avatar_type;
		public int age;
		public string avatar_url;
		public string avatar_name;

	}
}
