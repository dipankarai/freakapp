﻿using UnityEngine;
using System.Collections;

public class ServerConstant : MonoBehaviour 
{
	public const string EDITOR_DEVICEID = "abc123def456";

//	public const string BASEURL = "http://54.235.87.120/FREAK-TICTACTOE/TEST/rest.php?methodName=";
	public const string BASEURL = "http://www.juegostudio.in//FREAK-TICTACTOE/TEST/rest.php?methodName=";

	#if UNITY_ANDROID
	public const string APPLICATION_KEY_NO = "12345";
	#elif UNITY_IOS || UNITY_IPHONE
	public const string APPLICATION_KEY_NO = "12345";
	#endif

	//PARAMETERS
	public const string APPLICATION_KEY = "applicationKey";
	public const string USER_ID = "user_id";
	public const string USERID = "userId";
	public const string GLOBAL_ACCESS_TOKEN = "chat_access_token";
	public const string USER_ACCESS_TOKEN = "accessToken";
	public const string USER_DETAIL_ID = "user_detail_id";
	public const string OTHER_USER_ID = "other_user_id";
	public const string OPPONENT_USER_ID = "opponent_user_id";
	public const string WINNER_USER_ID = "winner_user_id";
	public const string GAME_ID = "game_id";
	public const string LAST_STATUS_ID = "last_status_id";
	public const string TYPE = "type";
	public const string DATA = "data";
	public const string SCORE = "score";
	public const string PREV_GAME_ID = "previous_game_id";

	//SERVER METHODS
	public const string GET_CONTACTLIST = "contact.list";
	public const string GET_GAMESTATUS = "status.get";
	public const string GET_GAMESTATUSUPDATE = "status.update";

	public const string GET_USER_GLOBALLOGIN = "user.globalLogin";

	public const string SEND_GAME_CHALLENGE = "game.challenge";
	public const string SEND_GAME_REMATCH = "game.rematch";
	public const string SEND_GAME_INVITE = "game.invite";
	public const string SEND_GAME_CANCEL = "game.cancel";
	public const string SEND_GAME_ACCEPT = "game.accept";
	public const string SEND_GAME_UPDATE = "game.update";
	public const string GET_GAME_DETAIL = "game.getDetail";
	public const string GET_USER_DETAIL = "user.get";

	//INPUT PARAMETER AND RESPONSE KEY
	public const string RESPONSE_CODE = "responseCode";
	public const string RESPONSE_MSG = "responseMsg";
	public const string RESPONSE_INFO = "responseInfo";

	//SERVER ERROR MESSAGE
	public const int WORKING_FINE = 1;
    public const int RESPONSE_CODE_FAILURE = 205;
	public const int EMAIL_DOESNT_EXIT = 230;
	public const int IN_CORRECT_PASSWORD = 231;
	public const int ALL_USER_OFFLINE = 234;
	public const int ALREADY_ANSWERED = 235;
	public const int USER_DONOT_EXISTS = 236;
	//230
	//emailId  does not exists in database

}