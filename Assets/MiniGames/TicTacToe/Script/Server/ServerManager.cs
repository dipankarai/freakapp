﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine.UI;
using FreakGame;

public class ServerManager : GenericSingletonClass <ServerManager>
{
    private string errorMsg;
    //	private System.Action<bool> m_callback;

    private TTTUiManager uiManager;

    public void InitUIManager(TTTUiManager _uiManager)
    {
        uiManager = _uiManager;
    }

    void ShowLoadingScreen(bool value)
    {
        uiManager.m_LoadingPanelScreen.gameObject.SetActive(value);
    }

    #region SEND SENDBIRD CHALLENGE MESSAGE

    private int mGameId, mUserId;
    private string mMessage;

    TTTDelegate.DelegateTTTGameId GameInviteCallback;
    bool isBeatMyScoreType;

    void SendGameManagerMessage(int gameId, int to_userID, string message)
    {
//        GameManager.Instance.SendChallenge(gameId, to_userID, message);

        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log ("gameId--->>"+gameId+"<<---to_userID-->>"+to_userID+"<<<---Message--->>"+message);
        #endif

        mGameId = gameId;
        mMessage = message;
        mUserId = to_userID;

        if (Freak.Chat.ChatManager.Instance.IsSeandbirdLogined == false)
        {
            Freak.Chat.ChatManager.Instance.LoginToSendbird();
        }

        List<string> user_List = new List<string>();
        user_List.Add(TTTGameManager.Instance.user_id.ToString());
        user_List.Add(to_userID.ToString());

        Freak.Chat.ChatManager.Instance.CreateSingleChannelAPIDirect(user_List, FreakAppManager.Instance.currentGameData.user_profile_url, CreateSingleChannelAPIDirectCallback);

    }

    void CreateSingleChannelAPIDirectCallback (IDictionary obj)
    {
        if (obj == null)
        {
            InviteGame(GameInviteCallback);
        }
        else
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("CreateSingleChannelAPIDirectCallback " + MiniJSON.Json.Serialize(obj));
            #endif
            
            string channel_url = (string)obj["channel_url"];
            
            Freak.Chat.FreakChatSerializeClass.NormalMessagerMetaData _metaData = new Freak.Chat.FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = Freak.Chat.ChatMessage.MetaMessageType.GameInvite;
            _metaData.gameType = Freak.Chat.FreakChatSerializeClass.NormalMessagerMetaData.GameType.Challenge;
            _metaData.gameData = "Challenge/AcceptOrRejectChallenge";
            _metaData.gameId = mGameId;
            _metaData.message = mMessage;
            _metaData.userid = mUserId.ToString();
            _metaData.gameStoreInfo = FreakAppManager.Instance.currentGameData.selectedStoreGame;
            
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);
            
            Freak.Chat.ChatManager.Instance.SendMessageAPIDirect(channel_url, TTTGameManager.Instance.user_id.ToString(), mMessage, data, SendMessageAPIDirectCallback);
        }
    }

    void SendMessageAPIDirectCallback (IDictionary obj)
    {
        #if UNITY_EDITOR && UNITY_DEBUG
        Debug.Log("SendMessageAPIDirectCallback " + MiniJSON.Json.Serialize(obj));
        #endif

        ShowLoadingScreen(false);
        if (isBeatMyScoreType)
        {
            isBeatMyScoreType = false;
            GameInviteCallback(-1);
        }
        else
        {
            GameInviteCallback(mGameId);
            uiManager.ShowNoInternetPopup(false);
        }
    }

    #endregion

    #region POST MESSAGE IN NEWS FEED

    public void PostNewsFeedsAPICall(string content, string feedtype)
    {
        Debug.Log("PostNewsFeedsAPICall ==================== " + content);

        FreakApi api = new FreakApi ("newsFeed.post", ServerResponseForPostNewsFeedsAPICall);
        api.param ("userId", FreakAppManager.Instance.userID);
        api.param ("feed_type", feedtype);
        api.param ("content", content);
        api.get (this);
    }

    void ServerResponseForPostNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
    {
        Debug.Log("ServerResponseForPostNewsFeedsAPICall ================= " + MiniJSON.Json.Serialize(output));

        // check for errors
        if (output!=null)
        {
            string responseInfo =(string)output["responseInfo"];
            switch(responseCode)
            {
                case 001:
                    /**
                    */
                    break;
                case 207:
                    /**
                    */
                    break;
                default:
                    /**
                     */
                    break;
            }   
        } 
        else {
            Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
        }  
    }

    #endregion

    bool CheckInternetCOnnection()
    {
        if (InternetConnection.Check())
        {
            return true;
        }
        else
        {
//			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
            return false;
        }
    }

    public void DisplayGameManagerDetails()
    {
        Debug.Log("User_NAME :" + TTTGameManager.Instance.user_name);
        Debug.Log("USER_ID :" + TTTGameManager.Instance.user_id);
        Debug.Log("GLOBAL_TOKEN :" + TTTGameManager.Instance.global_accessToken);

        Debug.Log("TOKEN :" + TTTGameManager.Instance.accessToken);

        Debug.Log("OPPONENT_ID :" + TTTGameManager.Instance.opponent_user_id);
        Debug.Log("OPPONENT_SCORE :" + TTTGameManager.Instance.opponent_user_score);
        Debug.Log("OPPONENT_NAME :" + TTTGameManager.Instance.opponent_user_name);

        Debug.Log("GAME_ID :" + TTTGameManager.Instance.game_id);

        Debug.Log("LAST_STATUS_ID :" + TTTGameManager.Instance.last_status_id);
        Debug.Log("OPPONENT_MOVE :" + TTTGameManager.Instance.opponent_move);
    }

    void OnInternetConnectionBack ()
    {
        
    }

    public void GlobalLoginUser(System.Action<bool> callback)
    {
        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.GET_USER_GLOBALLOGIN;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USER_ID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.GLOBAL_ACCESS_TOKEN, TTTGameManager.Instance.global_accessToken);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(OnGlobalLoginUserFromServerCallback(www, callback));
    }

    IEnumerator OnGlobalLoginUserFromServerCallback(WWW www, System.Action<bool> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GlobalLoginDatabase globalLoginDatabase = JsonConvert.DeserializeObject<ServerClass.GlobalLoginDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(globalLoginDatabase));

                if (globalLoginDatabase != null)
                {
                    TTTGameManager.Instance.accessToken = globalLoginDatabase.responseMsg.access_token;
                    ShowLoadingScreen(false);
                    callback(true);
                    uiManager.ShowNoInternetPopup(false);
                }
                else
                {
                    GlobalLoginUser(callback);
                }
            }
            else
            {
                callback(false);
            }
        }
        else
        {
            GlobalLoginUser(callback);
            uiManager.ShowNoInternetPopup(true);
//			ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void GetContactList(System.Action<bool> callback)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

//		ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.GET_CONTACTLIST;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GLOBAL_ACCESS_TOKEN, TTTGameManager.Instance.global_accessToken);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetContactListFromServerCallback(www, callback));
    }

    IEnumerator GetContactListFromServerCallback(WWW www, System.Action<bool> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.ContactsDatabase contactsDatabase = JsonConvert.DeserializeObject<ServerClass.ContactsDatabase>(www.text);
				
//                Debug.Log(JsonConvert.SerializeObject(contactsDatabase));
				
                if (contactsDatabase != null)
                {
                    TTTGameManager.Instance.contactsData.RemoveRange(0, TTTGameManager.Instance.contactsData.Count);
                    TTTGameManager.Instance.contactsData.AddRange(contactsDatabase.responseMsg);
                    ShowLoadingScreen(false);
                    callback(true);
                    uiManager.ShowNoInternetPopup(false);
                }
                else
                {
                    GetContactList(callback);
                }
            }
        }
        else
        {
            GetContactList(callback);
//			ShowLoadingScreen(false);
            uiManager.ShowNoInternetPopup(true);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void GetGameDetail(System.Action<bool> callback)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.GET_GAME_DETAIL;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, TTTGameManager.Instance.game_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetGameDetailFromServerCallback(www, callback));
    }

    IEnumerator GetGameDetailFromServerCallback(WWW www, System.Action<bool> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsFromGameIdDatabase gameDetailDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsFromGameIdDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(gameDetailDatabase));

                if (gameDetailDatabase != null)
                {
                    if (gameDetailDatabase.responseMsg.status == 1)
                    {
                        TTTGameManager.Instance.opponent_user_score = gameDetailDatabase.responseMsg.user_score;
                        TTTGameManager.Instance.opponent_user_id = gameDetailDatabase.responseMsg.user_id;
                    }
                    ShowLoadingScreen(false);
                    callback(true);
                    uiManager.ShowNoInternetPopup(false);
                }
                else
                {
                    GetGameDetail(callback);
                }
            }
        }
        else
        {
            GetGameDetail(callback);
//			ShowLoadingScreen(false);
            uiManager.ShowNoInternetPopup(true);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void GetUserDetail(System.Action<bool> callback, int user_id)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.GET_USER_DETAIL;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.USER_DETAIL_ID, user_id);
        wwwForm.AddField(ServerConstant.GLOBAL_ACCESS_TOKEN, TTTGameManager.Instance.global_accessToken);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetUserDetailFromServerCallback(www, callback, user_id));
    }

    IEnumerator GetUserDetailFromServerCallback(WWW www, System.Action<bool> callback, int user_id)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.UserDetailDatabase userDetailDatabase = JsonConvert.DeserializeObject<ServerClass.UserDetailDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(userDetailDatabase));

                if (userDetailDatabase != null)
                {
                    if (user_id == TTTGameManager.Instance.opponent_user_id)
                    {
                        TTTGameManager.Instance.opponent_user_name = userDetailDatabase.responseMsg.user_info.name;
                        if (TTTGameManager.Instance.opponent_user_imageURL.Equals(userDetailDatabase.responseMsg.user_info.avatar_url) == false)
                        {
//                            TTTGameManager.Instance.opponent_user_imageURL = userDetailDatabase.responseMsg.user_info.avatar_url;
//                            StartCoroutine(LoadImage(false, TTTGameManager.Instance.opponent_user_imageURL, callback));
                        }
                    }
                    else
                    {
                        TTTGameManager.Instance.user_name = userDetailDatabase.responseMsg.user_info.name;
                        if (TTTGameManager.Instance.user_imageURL.Equals(userDetailDatabase.responseMsg.user_info.avatar_url) == false)
                        {
//                            TTTGameManager.Instance.user_imageURL = userDetailDatabase.responseMsg.user_info.avatar_url;
//                            StartCoroutine(LoadImage(true, TTTGameManager.Instance.user_imageURL, callback));
                        }
                    }

                    ShowLoadingScreen(false);
                    uiManager.ShowNoInternetPopup(false);
                    callback(true);
                }
                else
                {
                    GetUserDetail(callback, user_id);
                }
            }
        }
        else
        {
            GetUserDetail(callback, user_id);
            uiManager.ShowNoInternetPopup(true);
//			ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }


    public void SendChallenge(TTTDelegate.DelegateTTTGameId callback, double _score, int _opponent_user_id)
    {
        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_CHALLENGE;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.SCORE, (int)_score);
        wwwForm.AddField(ServerConstant.OPPONENT_USER_ID, _opponent_user_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(SendChallengeFromServerCallback(www, callback, _score, _opponent_user_id));
    }

    IEnumerator SendChallengeFromServerCallback(WWW www, TTTDelegate.DelegateTTTGameId callback, double _score, int _opponent_user_id)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsDatabase sendChallengeDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(sendChallengeDatabase));

                if (sendChallengeDatabase != null)
                {
                    isBeatMyScoreType = true;
                    SendGameManagerMessage(sendChallengeDatabase.responseMsg.game_id, _opponent_user_id, "Beat My Score : " + _score);
                    /*ShowLoadingScreen(false);
                    callback(-1);*/
                }
                else
                {
                    SendChallenge(callback, _score, _opponent_user_id);
                }
            }
        }
        else
        {
            SendChallenge(callback, _score, _opponent_user_id);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    //	TTTDelegate.DelegateTTTGameId m_gameIdCallback;

    public void Rematch(TTTDelegate.DelegateTTTGameId callback)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_REMATCH;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.PREV_GAME_ID, TTTGameManager.Instance.game_id);
        wwwForm.AddField(ServerConstant.OPPONENT_USER_ID, TTTGameManager.Instance.opponent_user_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(RematchStatusFromServerCallback(www, callback));
    }

    IEnumerator RematchStatusFromServerCallback(WWW www, TTTDelegate.DelegateTTTGameId callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsDatabase gameInviteDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(gameInviteDatabase));

                if (gameInviteDatabase != null)
                {
                    SendGameManagerMessage(gameInviteDatabase.responseMsg.game_id, gameInviteDatabase.responseMsg.opponent.user_id, "REMATCH!! ");
                    /*ShowLoadingScreen(false);
                    callback(gameInviteDatabase.responseMsg.game_id);
                    uiManager.ShowNoInternetPopup(false);*/
                }
                else
                {
                    Rematch(callback);
                }
            }
        }
        else
        {
            Rematch(callback);
//			ShowLoadingScreen(false);
            uiManager.ShowNoInternetPopup(true);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void SetMultiPlayerWinner(int winner_user_id)
    {
//		if (CheckInternetCOnnection () == false)
//			return;
//		
//		ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_UPDATE;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, TTTGameManager.Instance.game_id);
        wwwForm.AddField(ServerConstant.WINNER_USER_ID, winner_user_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(SetMultiPlayerWinnerFromServerCallback(www, winner_user_id));
    }

    IEnumerator SetMultiPlayerWinnerFromServerCallback(WWW www, int winner_user_id)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsFromGameIdDatabase gameDetailsDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsFromGameIdDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(gameDetailsDatabase));

                if (gameDetailsDatabase != null)
                {
                    ShowLoadingScreen(false);
                }
                else
                {
                    SetMultiPlayerWinner(winner_user_id);
                }
            }
        }
        else
        {
            SetMultiPlayerWinner(winner_user_id);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void InviteGame(TTTDelegate.DelegateTTTGameId callback)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        GameInviteCallback = callback;
        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_INVITE;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.OTHER_USER_ID, TTTGameManager.Instance.opponent_user_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(InviteGameStatusFromServerCallback(www, callback));
    }

    IEnumerator InviteGameStatusFromServerCallback(WWW www, TTTDelegate.DelegateTTTGameId callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsDatabase gameInviteDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(gameInviteDatabase));

                if (gameInviteDatabase != null)
                {
                    SendGameManagerMessage(gameInviteDatabase.responseMsg.game_id, gameInviteDatabase.responseMsg.opponent.user_id, "Challenge you in Tic Tac Toe! ");
                    /*ShowLoadingScreen(false);
                    callback(gameInviteDatabase.responseMsg.game_id);
                    uiManager.ShowNoInternetPopup(false);*/
                }
                else
                {
                    InviteGame(callback);
                }
            }
        }
        else
        {
            InviteGame(callback);
            uiManager.ShowNoInternetPopup(true);
//			ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void AcceptChallengeGame(double score)
    {
//		if (CheckInternetCOnnection () == false)
//			return;
//
//		ShowLoadingScreen(true);
        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_ACCEPT;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, TTTGameManager.Instance.game_id);
        wwwForm.AddField(ServerConstant.SCORE, (int)score);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(AcceptChallengeGameStatusFromServerCallback(www, score));
    }

    IEnumerator AcceptChallengeGameStatusFromServerCallback(WWW www, double score)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsDatabase acceptMultiPlayerDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(acceptMultiPlayerDatabase));

                if (acceptMultiPlayerDatabase != null)
                {
                    ShowLoadingScreen(false);
                    TTTGameManager.Instance.game_id = -1;
                    TTTGameManager.Instance.ResetOpponent();
                }
                else
                {
                    AcceptChallengeGame(score);
                }
            }
        }
        else
        {
            AcceptChallengeGame(score);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void AcceptMultiPlayerGame(System.Action<bool> callback)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_ACCEPT;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, TTTGameManager.Instance.game_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(AcceptMultiPlayerGameStatusFromServerCallback(www, callback));
    }

    IEnumerator AcceptMultiPlayerGameStatusFromServerCallback(WWW www, System.Action<bool> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.GameDetailsDatabase acceptMultiPlayerDatabase = JsonConvert.DeserializeObject<ServerClass.GameDetailsDatabase>(www.text);

                Debug.Log(JsonConvert.SerializeObject(acceptMultiPlayerDatabase));

                if (acceptMultiPlayerDatabase != null)
                {
                    ShowLoadingScreen(false);
                    callback(true);
                    uiManager.ShowNoInternetPopup(false);
                }
                else
                {
                    AcceptMultiPlayerGame(callback);
                }
            }
        }
        else
        {
            AcceptMultiPlayerGame(callback);
            uiManager.ShowNoInternetPopup(true);
//			ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }


    public void CancelGame(System.Action<bool> callback, int game_id)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        if (game_id == -1)
        {
            return;
        }

//		ShowLoadingScreen(true);

        string url = ServerConstant.BASEURL + ServerConstant.SEND_GAME_CANCEL;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, game_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(CancelGameStatusFromServerCallback(www, callback, game_id));
    }

    IEnumerator CancelGameStatusFromServerCallback(WWW www, System.Action<bool> callback, int game_id)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.TTTResponse response = JsonConvert.DeserializeObject<ServerClass.TTTResponse>(www.text);

                Debug.Log(JsonConvert.SerializeObject(response));

                if (response != null)
                {
                    TTTGameManager.Instance.game_id = -1;
                    ShowLoadingScreen(false);
                    callback(true);
                }
                else
                {
                    CancelGame(callback, game_id);
                }
            }
        }
        else
        {
            CancelGame(callback, game_id);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void GetGameStatus(TTTDelegate.DelegateTTTGameStatus gameStatuscallback, int gameId)
    {
        if (gameId == -1)
        {
            return;
        }

        string url = ServerConstant.BASEURL + ServerConstant.GET_GAMESTATUS;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, gameId);
        wwwForm.AddField(ServerConstant.LAST_STATUS_ID, TTTGameManager.Instance.last_status_id);

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetGameStatusFromServerCallback(www, gameStatuscallback, gameId));
    }

    IEnumerator GetGameStatusFromServerCallback(WWW www, TTTDelegate.DelegateTTTGameStatus gameStatuscallback, int gameId)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {
                if (TTTGameManager.Instance.last_status_id == 0)
                {
                    ServerClass.GameStatusStartDatabase gameStatusDatabase = JsonConvert.DeserializeObject<ServerClass.GameStatusStartDatabase>(www.text);

                    Debug.Log(JsonConvert.SerializeObject(gameStatusDatabase));

                    if (gameStatusDatabase != null)
                    {
                        if (gameStatusDatabase.responseMsg.status.Count > 0)
                        {
                            if (gameStatusDatabase.responseMsg.status[0].game_id != TTTGameManager.Instance.game_id)
                            {
                                gameStatuscallback("");
                            }
                            else
                            {
                                TTTGameManager.Instance.last_status_id = gameStatusDatabase.responseMsg.last_status_id;
                                TTTGameManager.Instance.opponent_move = gameStatusDatabase.responseMsg.status[0].data.message;
                                gameStatuscallback(gameStatusDatabase.responseMsg.status[0].data.message);
                            }
                        }
                        else
                        {
                            if (gameId != TTTGameManager.Instance.game_id)
                            {
                                gameStatuscallback("");
                            }
                            else
                            {
                                GetGameStatus(gameStatuscallback, gameId);
                            }
                        }
                    }
                    else
                    {
                        if (gameId != TTTGameManager.Instance.game_id)
                        {
                            gameStatuscallback("");
                        }
                        else
                        {
                            GetGameStatus(gameStatuscallback, gameId);
                        }
                    }
                }
                else
                {
                    ServerClass.GameStatusMovementDatabase gameStatusDatabase = JsonConvert.DeserializeObject<ServerClass.GameStatusMovementDatabase>(www.text);

                    Debug.Log(JsonConvert.SerializeObject(gameStatusDatabase));

                    if (gameStatusDatabase != null)
                    {
                        if (gameStatusDatabase.responseMsg.status.Count > 0)
                        {
                            if (gameStatusDatabase.responseMsg.status[0].game_id != TTTGameManager.Instance.game_id)
                            {
                                gameStatuscallback("");
                            }
                            else
                            {
                                TTTGameManager.Instance.last_status_id = gameStatusDatabase.responseMsg.last_status_id;
                                TTTGameManager.Instance.opponent_move = JsonUtility.ToJson(gameStatusDatabase.responseMsg.status[0].data);
                                ShowLoadingScreen(false);
                                gameStatuscallback(JsonUtility.ToJson(gameStatusDatabase.responseMsg.status[0].data));
                            }
                        }
                        else
                        {
                            if (gameId != TTTGameManager.Instance.game_id)
                            {
                                gameStatuscallback("");
                            }
                            else
                            {
                                GetGameStatus(gameStatuscallback, gameId);
                            }
                        }
                    }
                    else
                    {
                        if (gameId != TTTGameManager.Instance.game_id)
                        {
                            gameStatuscallback("");
                        }
                        else
                        {
                            GetGameStatus(gameStatuscallback, gameId);
                        }
                    }
                }
            }
        }
        else
        {
            GetGameStatus(gameStatuscallback, gameId);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    public void UpdateStatus(System.Action<bool> callback, int block, pieces piece, int type = 1)
    {
//		if (CheckInternetCOnnection () == false)
//			return;

        ServerClass.GameMovementData gameMovementData = new ServerClass.GameMovementData();
        gameMovementData.block = block;
        gameMovementData.piece = piece;

        string url = ServerConstant.BASEURL + ServerConstant.GET_GAMESTATUSUPDATE;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
        wwwForm.AddField(ServerConstant.USERID, TTTGameManager.Instance.user_id);
        wwwForm.AddField(ServerConstant.USER_ACCESS_TOKEN, TTTGameManager.Instance.accessToken);
        wwwForm.AddField(ServerConstant.GAME_ID, TTTGameManager.Instance.game_id);
        wwwForm.AddField(ServerConstant.TYPE, type);
        Debug.Log("Data --> " + JsonUtility.ToJson(gameMovementData));
        wwwForm.AddField(ServerConstant.DATA, JsonUtility.ToJson(gameMovementData));

        Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(UpdateStatusFromServerCallback(www, callback, block, piece, type));
    }

    IEnumerator UpdateStatusFromServerCallback(WWW www, System.Action<bool> callback, int block, pieces piece, int type = 1)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {

                ServerClass.StatusUpdate statusUpdateData = JsonConvert.DeserializeObject<ServerClass.StatusUpdate>(www.text);
				
                Debug.Log(JsonConvert.SerializeObject(statusUpdateData));
				
                if (statusUpdateData != null)
                {
                    if (statusUpdateData.responseMsg != null)
                    {
                        callback(true);
                    }
                }
                else
                {
                    UpdateStatus(callback, block, piece, type);
                }
            }
        }
        else
        {
            UpdateStatus(callback, block, piece, type);
            ShowLoadingScreen(false);
            PopUpErrorMessage(www.error);
        }

        DisplayGameManagerDetails();
    }

    bool IsResponseCodeCorrect(string serverResponse)
    {
        IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(serverResponse);
        int responseCodeInt = int.Parse(string.Format("{0}", response[ServerConstant.RESPONSE_CODE]));
        if (responseCodeInt == ServerConstant.WORKING_FINE)
            return true;
        else
        {
            if (responseCodeInt == ServerConstant.EMAIL_DOESNT_EXIT)
            {

            }
            else if (responseCodeInt == ServerConstant.ALREADY_ANSWERED)
            {

            }
            else
            {
                PopUpErrorMessage(response[ServerConstant.RESPONSE_INFO].ToString());
            }

            Debug.Log(response[ServerConstant.RESPONSE_INFO].ToString());
            errorMsg = response[ServerConstant.RESPONSE_INFO].ToString();
        }
        return false;
    }

    void PopUpErrorMessage(string message)
    {
        Debug.Log("ERROR!!! " + message);
    }


    public IEnumerator LoadImage(bool isUser, string imageURL, System.Action<bool> callback)
    {
//        Texture2D tex = new Texture2D(2, 2, TextureFormat.DXT5, false);
        WWW www = new WWW(imageURL);
        yield return www;

        if (www.error == null)
        {
//            www.LoadImageIntoTexture(tex);
            if (isUser)
            {
                TTTGameManager.Instance.user_image_tex = www.texture;

//                if (TTTGameManager.Instance.user_image_tex != null)
//                {
//                    TTTGameManager.Instance.user_image_tex = tex;
//
////					image = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
//                }
            }
            else
            {
                TTTGameManager.Instance.opponent_user_image_tex = www.texture;

//                if (TTTGameManager.Instance.opponent_user_image_tex != null)
//                {
//                    TTTGameManager.Instance.opponent_user_image_tex = tex;
//
////                    image = Sprite.Create(tex, new Rect(0, 0,  tex.width,  tex.height), new Vector2(.5f, .5f));
//                }
            }

            callback(true);
        }
        else
        {

            print("Error");
            if (isUser)
            {
                if (TTTGameManager.Instance.user_image_tex != null)
                {
//					StartCoroutine(LoadImage(image,imageURL));
                }
            }
            else
            {
                if (TTTGameManager.Instance.opponent_user_image_tex != null)
                {
                    //					StartCoroutine(LoadImage(image,imageURL));
                }
            }

            callback(true);
        }
    }
}

