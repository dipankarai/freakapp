﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLoadController : MonoBehaviour {

	public string levelName = "TicTacToe";
	public GameObject controllerObj;
	public Slider progressBar;

	IEnumerator Start() 
	{
//		AsyncOperation async = Application.LoadLevelAdditiveAsync(levelName);

        AsyncOperation async  = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);
        while(async.isDone == false)
        {
            progressBar.value = async.progress;
            yield return null;
        }

        yield return async;

        Debug.Log("Loading complete");
	}

	public void EnableObject()
	{
		controllerObj.SetActive (true);
	}

    /*IEnumerator AsynchronousLoad (string scene)
    {
        yield return null;

        AsyncOperation ao = SceneManager.LoadSceneAsync(scene);
        ao.allowSceneActivation = false;

        while (! ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            Debug.Log("Loading progress: " + (progress * 100) + "%");

            // Loading completed
            if (ao.progress == 0.9f)
            {
                ao.allowSceneActivation = true;
            }

            yield return null;
        }
    }*/
}
