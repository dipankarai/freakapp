﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;


[System.Serializable]
public class GameStatus
{
	public string GameName;
	public string OppenentUserId;
	public string Message;
	public string Status;
	public string ProfilePicLink;
	public string OppenentName;

	private Dictionary<string,object> data;

    public GameStatus()
    {
        
    }

	public GameStatus(Dictionary<string,object> dataDictionary)
	{
		Message =			dataDictionary["message"].ToString();
	
		data = (Dictionary<string,object>) dataDictionary["data"];
		OppenentName = 		data["opponent_user_name"].ToString();
		GameName = 			data["game_name"].ToString();
		OppenentUserId = 	data["opponent_user_id"].ToString();
		Status = 			data["status"].ToString();
		ProfilePicLink = 	data["opponent_profile_pic"].ToString();
	}
}
	


public class History : MonoBehaviour
{
	public List<GameStatus> gameStatus = new List<GameStatus>();

	static System.Action<List<GameStatus>> onDone;

	private int lastStatusId = 0;
	private int maxLimit = 10;

	public void GetGameHistoryAPICall(System.Action<List<GameStatus>> _onDone = null)
	{
		onDone = _onDone;

		FreakApi api = new FreakApi ("gameHistory.get", ServerResponseForGetGameHistroyAPICall);
		api.param ("last_game_history_id", lastStatusId);
		api.param ("limit", maxLimit);
		api.get (this);

	}


	void ServerResponseForGetGameHistroyAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
	{
		// check for errors
		if (output!=null)
		{
			string responseInfo =(string)output["responseInfo"];

			switch(responseCode)
			{
			case 001:
				/**
						
				*/
				gameStatus.Clear ();
				IDictionary gameHistoryDetails = (IDictionary)output ["responseMsg"];

				List<object> gameHistoryList = (List<object>)gameHistoryDetails ["gameHistoryList"];

				Debug.Log ("message");
				if (gameHistoryList != null)
				{
					for (int i = 0; i < gameHistoryList.Count; i++) 
					{
						Dictionary<string,object> feeddata = (Dictionary<string,object>)gameHistoryList [i];
						GameStatus feeditem = new GameStatus (feeddata);

						gameStatus.Add (feeditem);

					}
				}
				OnLoadedFeeds ();
				break;
			case 207:
				/**
						
			 	*/
				break;
			default:
				/**
						
						 */
				break;
			}	
		} 
		else {
			Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			////inapi.get(this);
		}  
	}
	void OnLoadedFeeds()
	{
		if (onDone != null)
		{
			onDone (gameStatus);
		}
	}

}
