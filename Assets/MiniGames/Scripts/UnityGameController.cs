﻿using UnityEngine;
using System.Collections;


namespace FreakGame 
{
	/// <summary>
	/// This class is used to intract with buttons .
	/// </summary>
	public class UnityGameController : MonoBehaviour 
	{
		public GameObject loadGame;

		public void OnGamePlayButtonClicked(string gameName)
		{
			if (gameName == "TicTacToe") 
			{
				loadGame.SetActive (true);
			}
		}
	}
}
