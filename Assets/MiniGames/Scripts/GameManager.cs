﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Freak.Chat;
using SendBird;

namespace FreakGame{
	
	public class GameManager : MonoBehaviour 
	{
		public static GameManager Instance;
		public int user_Id ;
		public string chat_Access_Token = " ";
		public int game_id = -1;

        public MyUserProfile UserInfo
        {
            get { return FreakAppManager.Instance.myUserProfile; }
        }

        public FreakAppManager.GameIntegrateData GameIntegrateData
        {
            get { return FreakAppManager.Instance.currentGameData; }
        }

		void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
			else
			{
				Destroy(this.gameObject);
			}
			DontDestroyOnLoad (gameObject);
			user_Id = FreakAppManager.Instance.userID;
			chat_Access_Token = FreakAppManager.Instance.APIkey;
		}

		/// <summary>
		/// Recives the challenge This method is used for show in chat message 
		/// </summary>
		public void ReciveChallenge()
		{
		
		}

		/// <summary>
		/// Accepts the challenge open game play here on accepting challenge
		/// </summary>
		public void AcceptChallenge(string gameId, string platform, string userId)
		{
			game_id = int.Parse(gameId);
		}

		/// <summary>
		/// Rejects the challenge send message to the freak user that the opponent has rejected the challenge
		/// </summary>
		/// <param name="gameId">Game identifier.</param>
		/// <param name="userId">User identifier.</param>
		public void RejectChallenge(string gameId, string userId)
		{
			
		}

		/// <summary>
		/// Loads the game of unity platform
		/// </summary>
		/// <param name="gameId">Game identifier.</param>
		public void LoadGameUnity(int gameId)
		{
			//Message game id 
			game_id = gameId;
		}

        private int mGameId;
        private string mMessage;
        private int mUserId;
		public void SendChallenge(int gameId, int to_userID, string message)
		{
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log ("gameId--->>"+gameId+"<<---to_userID-->>"+to_userID+"<<<---Message--->>"+message);
            #endif

            mGameId = gameId;
            mMessage = message;
            mUserId = to_userID;

            if (ChatManager.Instance.IsSeandbirdLogined == false)
            {
                ChatManager.Instance.LoginToSendbird();
            }

            List<string> user_List = new List<string>();
            user_List.Add(TTTGameManager.Instance.user_id.ToString());
            user_List.Add(to_userID.ToString());

            Freak.Chat.ChatManager.Instance.CreateSingleChannelAPIDirect(user_List, FreakAppManager.Instance.currentGameData.user_profile_url, CreateSingleChannelAPIDirectCallback);

//            ChatManager.Instance.CreateChannelForGame(to_userID.ToString(), HandleActionCallback);
		}

        void CreateSingleChannelAPIDirectCallback (IDictionary obj)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("CreateSingleChannelAPIDirectCallback " + MiniJSON.Json.Serialize(obj));
            #endif

            string channel_url = (string)obj["channel_url"];

            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.GameInvite;
            _metaData.gameType = FreakChatSerializeClass.NormalMessagerMetaData.GameType.Challenge;
            _metaData.gameData = "Challenge/AcceptOrRejectChallenge";
            _metaData.gameId = mGameId;
            _metaData.message = mMessage;
            _metaData.userid = mUserId.ToString();
            _metaData.gameStoreInfo = FreakAppManager.Instance.currentGameData.selectedStoreGame;

            string data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);

            Freak.Chat.ChatManager.Instance.SendMessageAPIDirect(channel_url, TTTGameManager.Instance.user_id.ToString(), mMessage, data, SendMessageAPIDirectCallback);
        }

        void SendMessageAPIDirectCallback (IDictionary obj)
        {
            #if UNITY_EDITOR && UNITY_DEBUG
            Debug.Log("SendMessageAPIDirectCallback " + MiniJSON.Json.Serialize(obj));
            #endif
        }

        /*void HandleActionCallback ()
        {
            FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
            _metaData.messageType = ChatMessage.MetaMessageType.GameInvite;
            _metaData.gameType = FreakChatSerializeClass.NormalMessagerMetaData.GameType.Challenge;
            _metaData.gameData = "Challenge/AcceptOrRejectChallenge";
            _metaData.gameId = mGameId;
            _metaData.message = mMessage;
            _metaData.userid = mUserId.ToString();
            _metaData.gameStoreInfo = FreakAppManager.Instance.currentGameData.selectedStoreGame;

            string data = Newtonsoft.Json.JsonConvert.SerializeObject(_metaData);

            ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage(data);
            _chatMsg.messageType = ChatMessage.MessageType.Message;
            _chatMsg.message = mMessage;
            _chatMsg.data = data;

            MessageQueueHandler.Instance.SendUserMessage(ChatManager.Instance.currentChannel, ChatManager.Instance.currentChannel.Url, _chatMsg.message, _chatMsg.data, _chatMsg.customField, null);
        }*/


		internal long mMaxMessageTimestamp = long.MinValue;
		public long GetMaxMessageTimestamp()
		{
			return mMaxMessageTimestamp == long.MinValue ? long.MaxValue : mMaxMessageTimestamp;
		}
        /*public string GetSenderID (Freak.Chat.FreakChatSerializeClass.FreakMessagingChannel frkMsgChn)
		{
            string userid = ChatManager.Instance.UserId;

			for (int i = 0; i < frkMsgChn.freakMemberList.Count; i++)
			{
				if (frkMsgChn.freakMemberList[i].getid != userid)
				{
					return frkMsgChn.freakMemberList[i].getid;
				}
			} 

			return "";
		}*/

//		void FileUploadToSBCallback (SendBird.FileInfoEventArgs args)
//		{
//			if (args.Exception != null)
//			{
//				Debug.Log("FileUpload Exception " + args.Exception.Message);
//			}
//			else
//			{
//				Debug.Log("FileUploadEvent "+args.FileInfo.customField);
//				Debug.Log("FileUploadEvent "+args.FileInfo.name);
//				Debug.Log("FileUploadEvent "+args.FileInfo.path);
//				Debug.Log("FileUploadEvent "+args.FileInfo.size);
//				Debug.Log("FileUploadEvent "+args.FileInfo.type);
//				Debug.Log("FileUploadEvent "+args.FileInfo.url);
//
//				ChatFileType.FileType fileType = (ChatFileType.FileType)int.Parse(args.FileInfo.type);
//				ChatManager.Instance.DownLoadFile(args.FileInfo.url, ChatFileType.GetFolderLocation(fileType), null, true);
//				ChatManager.Instance.SendAnyFile(args.FileInfo.url, args.FileInfo.name, args.FileInfo.type, args.FileInfo.size, fileType.ToString());
//			}
//
////			chatWindowUI.SwitchPanel(ChatWindowUI.PanelState.CloseAll);
//		}

		/// <summary>
		/// Loads the game native platform
		/// </summary>
		/// <param name="gameId">Game identifier.</param>
		public void LoadGameOtherPlatform(string gameId)
		{
			
		}

		/// <summary>
		/// Call this to share the score in feedview
		/// </summary>
		/// <param name="message">Message.</param>
		public void ShareScore(string message){

			PostNewsFeedsAPICall (message, "1");
		}
		/// <summary>
		/// Post News Feed Info
		/// </summary>
		/// <param name="chatString">Chat string.</param>
		public void PostNewsFeedsAPICall(string content, string feedtype)
		{
            Debug.Log("PostNewsFeedsAPICall ==================== " + content);

			FreakApi api = new FreakApi ("newsFeed.post", ServerResponseForPostNewsFeedsAPICall);
			api.param ("userId", FreakAppManager.Instance.userID);
			api.param ("feed_type", feedtype);
			api.param ("content", content);
			api.get (this);

		}

		void ServerResponseForPostNewsFeedsAPICall(IDictionary output,string text, string error,int responseCode,IDictionary inCustomData)
		{
            Debug.Log("ServerResponseForPostNewsFeedsAPICall ================= " + MiniJSON.Json.Serialize(output));

			// check for errors
			if (output!=null)
			{
				string responseInfo =(string)output["responseInfo"];
				switch(responseCode)
				{
				case 001:
					/**
					*/
					break;
				case 207:
					/**
			 		*/
					break;
				default:
					/**
					 */
					break;
				}	
			} 
			else {
				Debug.Log("WWW Error from Server: "+ MiniJSON.Json.Serialize(output) + " " + text);
			}  
		}


	}
}
