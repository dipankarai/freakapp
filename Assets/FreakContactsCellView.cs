﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;
using Freak.Chat;
using SendBird;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//Inherit from TableViewCell instead of MonoBehavior to use the GameObject
//containing this component as a cell in a TableView
using System.Text;
using System.IO;


/// <summary>
/// This is the view of our cell which handles how the cell looks.
/// </summary>
public class FreakContactsCellView : EnhancedScrollerCellView
{
	/// <summary>
	/// This is a reference to the cell's underlying data.
	/// We will store it in the SetData method, and use it
	/// in the RefreshCellView method.
	/// </summary>
	//private Data _data;

	/// <summary>
	/// A reference to the UI Text element to display the cell data
	/// </summary>

	public ChatChannel chatChannel;
	private string m_channelUrl;
	private bool m_isGroup;
	public Text numberText;

	public string iD{ get; set; }
	public string userName{ get; set; }

	public Button contactsButton;
	public Image img;
	public Text nameText;
	public Text otherDetailText;
	public Button addButton;
	public string imageUrl;

	public Text m_rowNumberText;
	public Slider m_cellHeightSlider;

	public string phoneNumb;

	public Toggle selectionToggle;

	private ServerContactsSelectedForGroups userForGroup;

	public Sprite profilePicHollder;
	public Sprite groupProfilePicHolder;

	private Sprite defaultImage;

	private string profileAvatarLink;
	public Button inviteButton;

	public ServerUserContact ServerUserContactGrid;

	public RectTransform RectTransform
	{
		get
		{
			var rt = gameObject.GetComponent<RectTransform>();
			return rt;
		}
	}

	/// <summary>
	/// This function just takes the Demo data and displays it
	/// </summary>
	/// <param name="data"></param>
	public void SetData(ChatChannel channelDetails)
	{
		// store the data so that it can be used when refreshing
		chatChannel = channelDetails;
		// update the cell's UI
		RefreshCellView();
	}

	public override void RefreshCellView()
	{
		// update the UI text with the cell data
		//someTextText.text = _data.someText;
	}


	public void IntUser(string userid, string name, Freak.Chat.ChatChannel channel = null)
	{
		iD = userid;
		userName = name;

		if (channel != null)
		{
			chatChannel = null;
			chatChannel = channel;

			m_channelUrl = channel.channelUrl;
			m_isGroup = channel.isGroupChannel;

			//            ShowProfileImageURL();
		}
		else
		{
			chatChannel = new ChatChannel();
			chatChannel.otherUserId = userid;
			chatChannel.otherUserName = name;
		}
	}


	public void IntUserForGroupChat(string userid, string username, string phonenumber)
	{
		iD = userid;
		userName = username;
		userForGroup = new ServerContactsSelectedForGroups (userid, username, phonenumber);
	}


	public void AssignDetails(ServerUserContact cont, int rownumb)
	{
		if (!string.IsNullOrEmpty (cont.name)) {
			nameText.text = cont.name;
		} else {
			nameText.text = "Unknown Contact "+ rownumb;
		}
		numberText.text = cont.contact_number;
		iD = cont.user_id.ToString();
		IntUser (cont.user_id.ToString(), cont.name);
		selectionToggle.gameObject.SetActive (false);
		ShowProfileImage (cont);

		/*if (imageController == null) {
			imageController = FindObjectOfType<ImageController> ();
		}*/
		//        StartCoroutine (loadProfileImage (cont.avatar, cell.img, cell));
		//imageController.GetImageFromLocalOrDownloadImage (cont.avatar, cell.img, Freak.FolderLocation.Profile);
		//StartCoroutine(loadProfileImage(cont.avatar, cell.img, Freak.FolderLocation.Profile));
		//if (!cell.imageUrl.Equals (cont.avatar))
		//{
		//cell.imageUrl = FreakAppManager.Instance.myUserProfile.avatar_url;
		//}
	}


	public void ShowProfileImageURL ()
	{
		Freak.FolderLocation _location = Freak.FolderLocation.Profile;

		if (chatChannel.isGroupChannel)
		{
			_location = Freak.FolderLocation.GroupProfile;
		}

        Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(chatChannel.coverUrl, _location, null);

		if (texture != null)
		{
            img.FreakSetSprite(texture);
		}
		else
		{
            if (chatChannel.isGroupChannel)
            {
                img.FreakSetSprite(FreakAppManager.Instance.defaultGroupProfileTexture);// sprite = groupProfilePicHolder;
            }
            else
            {
                img.FreakSetSprite(FreakAppManager.Instance.defaultProfileTexture);// sprite = profilePicHollder;
            }

			Invoke("ShowProfileImageURL", 0.5f);
		}
	}
		

	private ServerUserContact mServerUserContact;
	public void ShowProfileImage (ServerUserContact serverUserContact)
	{
        mServerUserContact = serverUserContact;
        ShowProfileImage();
	}

	void ShowProfileImage ()
	{
		profileAvatarLink = mServerUserContact.avatar;
		Texture2D texture = Freak.DownloadManager.Instance.GetProfileTexture2DFromCache(mServerUserContact.avatar, Freak.FolderLocation.Profile, mServerUserContact);

		if (texture != null)
		{
            img.FreakSetSprite(texture);
		}
		else
		{
            img.FreakSetSprite(FreakAppManager.Instance.defaultProfileTexture);// .sprite = profilePicHollder;

			Invoke("ShowProfileImage", 0.5f);
		}
	}

	public void OnClickOpenProfile()
	{
        FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = img.sprite.texture;

        if (chatChannel.isGroupChannel)
        {
            chatChannel = CacheManager.GetChatChannel(m_channelUrl);
            
            ChatManager.Instance.groupUsersList.Clear();
            FreakAppManager.Instance.selectedGroupProfile = chatChannel;
//            GameObject go = 
            FreakAppManager.Instance.GetHomeScreenPanel().PushPanel (HomeScreenUIPanelController.PanelType.GroupProfilePanel);
            //            go.GetComponent<GroupProfileUIController> ().InitGroupProfileInfo (chatChannel);
        }
        else
        {
            FreakAppManager.Instance.currentGameData = new FreakAppManager.GameIntegrateData();
            FreakAppManager.Instance.currentGameData.opponent_id = chatChannel.otherUserId;
            FreakAppManager.Instance.currentGameData.opponent_name = chatChannel.otherUserName;
            FreakAppManager.Instance.currentGameData.opponent_profile_url = profileAvatarLink;
            
            Debug.Log("Open User Profile NAME: " + chatChannel.otherUserName + " ID: " + chatChannel.otherUserId);
            FreakAppManager.Instance.myUserProfile.memberId = chatChannel.otherUserId;
            FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
        }
	}


	public void OnClickOpenChat ()
	{

		#if UNITY_EDITOR && UNITY_DEBUG
		//        Debug.Log ("TIME.TIME:: " + Time.time);
		//        Debug.Log ("Chat User NAME ---- >"+ chatChannel.otherUserName);
		#endif

        FreakAppManager.Instance.senderCompressedPic = null;
        FreakAppManager.Instance.senderCompressedPic = img.sprite.texture;

        ChatChannel _chatChannel = CacheManager.GetChatChannel(m_channelUrl);
        GameObject go = FreakAppManager.Instance.GetHomeScreenPanel().GetPushedPanel (HomeScreenUIPanelController.PanelType.ChatWindow);
        go.GetComponent<ChatWindowUI> ().InitChat (_chatChannel, true);

        if (_chatChannel.groupChannel == null)
        {
            ChatManager.Instance.GetChannelByChannelUrl (_chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
        }
        else
        {
            go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(_chatChannel.groupChannel);
        }

		/*if (string.IsNullOrEmpty (chatChannel.channelUrl))
        {
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel);
        }
        else
        {
            go.GetComponent<ChatWindowUI> ().InitChat (chatChannel, true);
            if (chatChannel.groupChannel == null)
            {
                ChatManager.Instance.GetChannelByChannelUrl (chatChannel.channelUrl, go.GetComponent<ChatWindowUI> ().GetGroupChannelCallback);
            }
            else
            {
                go.GetComponent<ChatWindowUI>().GetGroupChannelCallback(chatChannel.groupChannel);
            }
        }*/
	}


	public void OnClickServerContact(ServerUserContact contactGrid)
	{
		ServerUserContactGrid = contactGrid;
	}


	public void OnClickServerContact()
	{
		if (selectionToggle.isOn)
		{
			if (!ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
			{
				ChatManager.Instance.groupUsersList.Add (userForGroup);
				ServerUserContactGrid.isSelectedForGroup = true;
			}
		}
		else {
			if (ChatManager.Instance.groupUsersList.Contains (userForGroup)) 
			{
				ServerUserContactGrid.isSelectedForGroup = false;
				ChatManager.Instance.groupUsersList.Remove (userForGroup);
			}
		}

		#if UNITY_EDITOR && UNITY_DEBUG
		Debug.Log ("Server Contacts selected for group ---- >"+  ChatManager.Instance.groupUsersList.Count);
		#endif
	}

	public void OnClickAdd()
	{
//		if (mFreakmember == null && !isTagFriend)
//		{
//			FindObjectOfType<AddUserApiManager> ().AddUserAPICall (phoneNumb);
//		}
//		else
//		{
//			if (!isTagFriend)
//			{
//				FindObjectOfType<ChatWindowUI>().TagFriendSelected(mFreakmember);
//			} 
//			else 
//			{
//				FindObjectOfType<NewsFeedCommentsUIPanel> ().TagFriendSelected (mTagMember);
//			}
//		}

		/*if (mFreakMember == null && !isTagFriend)
        {
            FindObjectOfType<AddUserApiManager> ().AddUserAPICall (phoneNumb);
        }
        else
        {
			if (!isTagFriend)
			{
				FindObjectOfType<ChatWindowUI> ().TagFriendSelected (mFreakMember);
			} 
			else 
			{
				FindObjectOfType<NewsFeedCommentsUIPanel> ().TagFriendSelected (mTagMember);
			}
        }*/
	}


	public void OnClickFreakMember()
	{
		FreakAppManager.Instance.myUserProfile.memberId = iD;
		FindObjectOfType<HomeScreenUIPanelController>().PushPanel(HomeScreenUIPanelController.PanelType.OtherUsersProfilePanel);
	}


	public void OnClickInviteUser()
	{
		Debug.Log ("Invite User");
		#if UNITY_ANDROID
		//string shareText = "Hey,\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n";
		string shareText = "Hey,\n\n Check out the new Freak App!\n";
		//AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		//AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		//intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		//AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		//AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + " ");
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		//intentObject.Call<AndroidJavaObject>("setType", "image/png");

		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		//AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		//AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		//AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Freak Invite");
		//currentActivity.Call("startActivity", jChooser);


		AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
		intentObject.Call<AndroidJavaObject> ("setAction", intentClass.GetStatic<string> ("ACTION_SEND"));
		intentObject.Call<AndroidJavaObject> ("setType", "text/plain");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);
		AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
		currentActivity.Call ("startActivity", intentObject);


		#elif UNITY_IOS 
		CallSocialShareAdvanced ("Hey,\n\n\n\n It is a smartphone messenger which replaces SMS. This app even lets me send pictures, video and other multimedia!\n","Invite","","");
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif

		//		string mobile = phoneNumb;
		//		//string url = "sms:"+mobile +"?body=" +"asasdasdasdasda";
		//		//Application.OpenURL("sms:" + mobile);
		//
		//		//string url = "sms:"+ mobile + "&body=/*Join Freaks*/";
		//		//Application.OpenURL(url);
		//
		//		string separator=(Application.platform == RuntimePlatform.IPhonePlayer)?";":"?";
		//		Application.OpenURL(string.Format("sms:{0}{1}body={2}", mobile, separator, WWW.EscapeURL("Join Freaks")));

		//string url = string.Format("sms:{0}?body={1}", mobile, "Freaks");
		//Application.OpenURL (url);

		}

		#if UNITY_IOS
		public struct ConfigStruct
		{
		public string title;
		public string message;
		}

		[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

		public struct SocialSharingStruct
		{
		public string text;
		public string url;
		public string image;
		public string subject;
		}

		[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

		public static void CallSocialShare(string title, string message)
		{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
		}

		public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
		{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
		}
		#endif


		public void OnClickSend()
		{

		var vcf = new StringBuilder();
		// vcf.Append("TITLE:" + nameText.text + System.Environment.NewLine); 
		//vcf.Append("NUMBER:" + phoneNumb + System.Environment.NewLine); 

		vcf.AppendLine("BEGIN:VCARD");
		vcf.AppendLine("VERSION:2.1");
		// Name
		vcf.AppendLine ("N:" + " " + ";" + " ");
		// Full name
		vcf.AppendLine("FN:" + nameText.text + " " + nameText.text);
		// Address
		vcf.Append("ADR;HOME;PREF:;;");
		vcf.Append ("StreetAddress" + ";");
		vcf.Append (" " + ";;");
		vcf.Append(" " + ";");
		vcf.AppendLine (" ");
		// Other data
		vcf.AppendLine("ORG:" + " ");
		vcf.AppendLine("TITLE:" + " ");
		vcf.AppendLine("TEL;HOME;VOICE:" + " ");
		vcf.AppendLine("TEL;CELL;VOICE:" + phoneNumb);
		vcf.AppendLine("URL;" + " ");
		vcf.AppendLine("EMAIL;PREF;INTERNET:" + string.Empty);
		vcf.AppendLine("END:VCARD");

		var filename = nameText.text+".vcf";
		var path = Freak.StreamManager.LoadFilePath(filename, ChatFileType.GetFolderLocation(ChatFileType.FileType.Contact), true);

		//  Debug.Log ("file path--->>"+path);
		File.WriteAllText(path, vcf.ToString());

		ChatFileType chatFileToSend = new ChatFileType(path, ChatFileType.FileType.Contact);
		//        FindObjectOfType<ChatWindowUI> ().DuplicateMessageBox(chatFileToSend);

		FreakChatSerializeClass.NormalMessagerMetaData _metaData = new FreakChatSerializeClass.NormalMessagerMetaData();
		_metaData.messageType = ChatMessage.MetaMessageType.Contact;
		_metaData.contactName = nameText.text;
		_metaData.contactNumber = phoneNumb;
		_metaData.message = nameText.text + " Contact";

		FindObjectOfType<ChatWindowUI> ().DuplicateMessageBox (chatFileToSend, Newtonsoft.Json.JsonConvert.SerializeObject (_metaData));

		//        ChatManager.Instance.fileSendCallback = UIController.Instance.PlaySendMessageSound;

		/*ChatMessage _chatMsg = ChatChannelHandler.DuplicateMessage ();
		_chatMsg.messageType = ChatMessage.MessageType.FileLink;
		_chatMsg.fileName = System.IO.Path.GetFileName (chatFileToSend.filePath);
		_chatMsg.fileSize = chatFileToSend.fileSize;
		_chatMsg.filePath = chatFileToSend.filePath;
		_chatMsg.fileUrl = chatFileToSend.filePath;*/

		//        ChatManager.Instance.currentSelectedChatChannel.messagesList.Add (_chatMsg);
		//        ChatManager.Instance.SendFileMessage(chatFileToSend, _chatMsg.customField, Newtonsoft.Json.JsonConvert.SerializeObject(_metaData));
		//        ChatManager.Instance.UploadFileToSendBird(chatFileToSend, FileUploadToSBCallback);


		if (FreakAppManager.Instance.GetHomeScreenPanel() != null) {
		FreakAppManager.Instance.GetHomeScreenPanel().PopPanel ();
		}
		//		else {
		//			FindObjectOfType<FreaksPanelUIController> ().PopPanel ();
		//		}
		}
}
