﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Freak.Chat;

public class LongTapSelect : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler {

    public bool mouseDown;
    public float timeMouseDown;
    public GameObject selectedChatHighlightImage;
    public bool isLongPressSelected;
    public MessageBoxBase chatMessageBox;
    public bool isDragging;

    private IEnumerator updateRoutine;

    void OnEnable ()
    {
        if (this.gameObject.activeInHierarchy)
        {
            selectedChatHighlightImage.gameObject.SetActive (false);
            
            for (int i = 0; i < ChatManager.Instance.selectedMessages.Count; i++)
            {
                if (ChatManager.Instance.selectedMessages[i].messageId == chatMessageBox.chatMessage.messageId)
                {
                    selectedChatHighlightImage.gameObject.SetActive (true);
                }
            }
        }

        if (updateRoutine != null)
            StopCoroutine(updateRoutine);

        updateRoutine = TapCheckUpdate();
        StartCoroutine(updateRoutine);
    }

    void OnDisable ()
    {
        if (updateRoutine != null)
            StopCoroutine(updateRoutine);
    }

    IEnumerator TapCheckUpdate(){
		
        while (true)
        {
            if (mouseDown)
            {
                if (isDragging == false)
                {
                    timeMouseDown += Time.deltaTime;
                    
                    if (timeMouseDown > 1f) 
                    {
                        if (!isLongPressSelected) 
                        {
                            isLongPressSelected = true;
                            LongPress ();
                        }
                    }
                }
            }
            
            yield return 0;
        }
	}

    void LongPress()
    {
//        Debug.Log ("LongPress");
        selectedChatHighlightImage.gameObject.SetActive (true);
        chatMessageBox.OnClickMessageButtonHold (true);
    }

    void ShortPress()
    {
//        Debug.Log ("ShortPress");
        if (selectedChatHighlightImage.gameObject.activeInHierarchy)
        {
            selectedChatHighlightImage.gameObject.SetActive (false);
            chatMessageBox.OnClickMessageButtonHold (false);
        }
        else
        {
            if (ChatManager.Instance.selectedMessages.Count > 0)
            {
                selectedChatHighlightImage.gameObject.SetActive (true);
                chatMessageBox.OnClickMessageButtonHold (true);
            }
            else
            {
                chatMessageBox.OnClickMessageButtonSingle ();
            }
        }
    }

	//void Upadte
	public void OnPointerDown( PointerEventData data )
	{
//		Debug.Log( "OnPointerDown called."+ timeMouseDown);
		mouseDown = true;
	}

	public void OnPointerUp( PointerEventData data )
	{
//		Debug.Log( "OnPointerUp called."+ timeMouseDown);
        mouseDown = false;
        timeMouseDown = 0;

        if (isDragging == false && isLongPressSelected == false)
        {
            ShortPress ();
        }
        isLongPressSelected = false;
	}

    public void OnDrag (PointerEventData data)
    {
//        Debug.Log ("On Drag");
        isDragging = true;
        UIController.Instance.chatWindowUI.scrollRect.OnDrag (data);
    }

    public void OnBeginDrag (PointerEventData data)
    {
//        Debug.Log ("OnBeginDrag");
        isDragging = true;
        UIController.Instance.chatWindowUI.scrollRect.OnBeginDrag (data);
    }

    public void OnEndDrag (PointerEventData data)
    {
//        Debug.Log ("OnEndDrag");
        isDragging = false;
        UIController.Instance.chatWindowUI.scrollRect.OnEndDrag (data);
    }

	public void OnPointerClick( PointerEventData data )
	{
//        Debug.Log ("OnPointerClick " + data.clickCount);
	}
}

