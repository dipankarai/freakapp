﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class demo : MonoBehaviour {

	public NativeEditBox testNativeEdit;

	public InputField inputfield;
	public Canvas		mainCanvas;
	private RectTransform rectTrans;
	private RectTransform InputTransform;
	// Use this for initialization
	void Start () {
		rectTrans = testNativeEdit.transform.FindChild("Text").GetComponent<RectTransform>();

		InputTransform = testNativeEdit.GetComponent<RectTransform> ();
		Debug.Log (InputTransform.anchoredPosition);
//		GameObject tempTest = GameObject.Instantiate(testNativeEdit.gameObject);
//		tempTest.transform.SetParent(mainCanvas.transform, false);
//		tempTest.transform.position += new Vector3(0.0f, -250.0f, 0.0f);
//
//		NativeEditBox tempNB = tempTest.GetComponent<NativeEditBox>();;
//		tempNB.SetTextNative("fdfdsfsd");
		testNativeEdit.Initialization("demo");
		Debug.Log (inputfield.contentType.ToString());
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	private string GetCurObjName()
	{
		string strObjName = "";
		GameObject objSel = EventSystem.current.currentSelectedGameObject;
		if (objSel != null && objSel.transform.parent != null)
		{
			strObjName = objSel.transform.parent.name;
		}

		return strObjName;
	}

	public void OnEditValugChanged(string str)
	{
		Text txt = this.GetComponent<Text>();
		txt.text = string.Format("[{0}] val changed {1}", this.GetCurObjName(), str);
	}


	private bool bTempFocus = false;
	private bool bTempVisible = false;
	public void OnButton1()
	{
		bTempFocus = !bTempFocus;
		Debug.Log("OnButton1 clicked");
		testNativeEdit.SetFocusNative(bTempFocus);
	}

	public void OnButton2()
	{
		Debug.Log("OnButton2 clicked");
		bTempVisible = !bTempVisible;
		testNativeEdit.SetVisible(bTempVisible);
	}

	public void OnButton3()
	{
		rectTrans.sizeDelta = new Vector2(-20.0f, -5.0f);
		testNativeEdit.SetRectNative(rectTrans);
		Debug.Log("OnButton3 clicked");
	}


	public void OnButton4()
	{
		//rectTrans.sizeDelta = new Vector2(20.0f, 5.0f);
		//testNativeEdit.SetRectNative(rectTrans);

		Text txt = this.GetComponent<Text>();
		string sCurText = testNativeEdit.GetTextNative();
		txt.text = string.Format("[{0}] GetText {1}", this.GetCurObjName(), sCurText);
		Debug.Log("OnButton4 clicked");
	}

	public void OnButton5()
	{
		Debug.Log("OnButton5 clicked");
		testNativeEdit.SetTextNative("TestText Set!!@#@5");
	}






	public void OnTop()
	{
		Debug.Log("OnTop clicked");
		InputTransform.anchoredPosition = new Vector2 (0,100);
		testNativeEdit.SetRectNative(rectTrans);
	}

	public void OnBottom()
	{
		Debug.Log("OnBottom clicked");
		InputTransform.anchoredPosition = new Vector2 (0,-100);
		testNativeEdit.SetRectNative(rectTrans);
	}

	public void DeActive()
	{
		bTempVisible = !bTempVisible;
		testNativeEdit.SetVisible(bTempVisible);
	}


	public void OnClicked()
	{		
		if (!testNativeEdit.enabled)
			testNativeEdit.enabled = true;
		Debug.Log ("OnClicked ");
		testNativeEdit.SetVisible(true);
		testNativeEdit.SetFocusNative(true);
//		testNativeEdit.SetSelection(true);
	}

	public void OnEditEnded(string str)
	{
		Text txt = this.GetComponent<Text>();
		txt.text = string.Format("[{0}] edit ended {1}", this.GetCurObjName(), str);
		Debug.Log ("OnEditEnded  clicked  ");
//		testNativeEdit.SetSelection (false);
	}



	public void OnExit()
	{
		if (testNativeEdit.enabled) 
		{
			bTempVisible = !bTempVisible;
			testNativeEdit.SetVisible(bTempVisible);
			testNativeEdit.enabled = false;
		}
	}
	public void testButton ()
	{
		Debug.Log ("testClicked");
//		testNativeEdit.CreatText ();
	}


	public void OnVideoPlay()
	{
		StopCoroutine (PLay ());
		StartCoroutine (PLay ());
	}
	IEnumerator PLay()
	{
		Debug.LogError ("Video testing play starts");
		Handheld.PlayFullScreenMovie ("videoplayback.mp4", Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
		yield return new WaitForEndOfFrame ();
		yield return new WaitForSeconds (0.5f);
		PluginMsgHandler.getInst().InitializeHandler();
		testNativeEdit.Initialization("demo");
		Debug.LogError ("Video testing play ends");
	}

	public void Create()
	{
		Debug.LogError ("Video testing play starts");
		testNativeEdit.Initialization("demo");		
	}

	public void Remove()
	{
		Debug.LogError ("Video testing play starts");
	}
}
