﻿using UnityEngine;
using System.Collections;

public abstract class NativeInput :MonoBehaviour {

	public abstract void OnBackButtonPress();
	public abstract void InputfieldClicked ();
}
