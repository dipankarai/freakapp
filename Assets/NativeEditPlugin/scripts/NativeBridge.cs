﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NativeBridge : NativeInput 
{
	NativeEditBox NativeInputField;
	public string Placeholder;


	public bool isMovementEnabled;
	public Transform ParentObjt;
	Vector3 bottomPanelPosition,_endPos;
	private LTDescr moveTween;
	[HideInInspector]public bool MoveUp;
	public LeanTweenType leanTweenType = LeanTweenType.linear;

	private RectTransform inputTextTransform;
    private string _tempMsg = string.Empty;
	// Use this for initialization
	void Start () 
	{		
		if (isMovementEnabled) 
		{
			bottomPanelPosition = ParentObjt.rectTransform ().localPosition;
			_endPos = ParentObjt.transform.position;
			inputTextTransform = transform.FindChild("Text").GetComponent<RectTransform>();
		}
	}


	void Update()
	{
		if (isMovementEnabled) 
		{
			if (NativeInputField.IsFocused&&!MoveUp) 
			{
				StartCoroutine(SwitchPanelState (true));
			}
		}
	}

	public override void OnBackButtonPress()
	{
		if (isMovementEnabled) 
		{			
			if (MoveUp) {
				NativeInputField.IsFocused = false;
				NativeInputField.SetFocusNative (false);
				StartCoroutine (SwitchPanelState (false));
			}
		} 
		else 
		{
			NativeInputField.IsFocused = false;
			NativeInputField.SetFocusNative (false);
		}
		Debug.Log ("OnBackButtonPress");
	}

	public override void InputfieldClicked()
	{
		Debug.Log ("inputfield clicked");
	}

	public IEnumerator SwitchPanelState(bool moveUp)
	{
		MoveUp = moveUp;
		yield return null;
		_endPos = bottomPanelPosition;
		if (moveUp) {
			_endPos.y += GetPanelHeight ();
			Debug.LogError ("HandleOnCompleteMove   "+_endPos.y);
		}
		moveTween = LeanTween.moveLocal (ParentObjt.gameObject, _endPos, 0.25f);
		moveTween.setEase(leanTweenType);
		moveTween.setOnComplete(HandleOnCompleteMove, MoveUp);
	}
	void OnDisable()
	{
		MoveUp = false;
		NativeInputField.IsFocused=false;
        NativeInputField.SetTextNative (string.Empty);
		NativeInputField.CustomCreate (false);
	}
	void OnEnable()
	{
		if(NativeInputField==null)
			NativeInputField = GetComponent<NativeEditBox> ();	
		
		NativeInputField.InputReciver = this;

		#if UNITY_ANDROID && !UNITY_EDITOR
		if (!string.IsNullOrEmpty (Placeholder))
		NativeInputField.Initialization (Placeholder);
		else
        NativeInputField.Initialization (string.Empty);
		#endif

		if (isMovementEnabled)
		{
			if(!inputTextTransform)
				inputTextTransform = transform.FindChild("Text").GetComponent<RectTransform>();

			NativeInputField.SetFocusNative (true);
			StartCoroutine (SwitchPanelState (true));
//			NativeInputField.SetRectNative (inputTextTransform); 
		}
		
	}	

	void HandleOnCompleteMove (object moveUp)
	{
		NativeInputField.SetRectNative (inputTextTransform); 
	}
	float GetPanelHeight ()
	{
		float _tempPanelHeight,screenRef;

		float refHeight ;
		if (Screen.height < 1280)
			screenRef = 1184;
		else
			screenRef = 1280;
		refHeight= (100 * Screen.height) / screenRef;

        _tempPanelHeight=PlayerPrefs.GetFloat(FreakAppConstantPara.PlayerPrefsName.KeyboardHeight);
		if(_tempPanelHeight<refHeight)
			_tempPanelHeight=594;
		
		return _tempPanelHeight;
	}



	void OnApplicationPause( bool isPaused )
	{
		if (isPaused) 
		{
			Debug.LogError ("input field OnApplicationFocus  pause activ");
			_tempMsg = NativeInputField.GetTextNative ();
			OnBackButtonPress ();
			NativeInputField.CustomCreate (false);
		} 
		else 
		{
			Debug.LogError ("input field OnApplicationFocus  pause deactiv");
			NativeInputField.Initialization (Placeholder);
			NativeInputField.SetTextNative (_tempMsg);
		}				
	}

}
