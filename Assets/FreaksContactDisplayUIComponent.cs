﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using Tacticsoft;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.RefreshDemo;

public class FreaksContactDisplayUIComponent : EnhancedScrollerCellView {

	public Text m_rowNumberText;
	public Slider m_cellHeightSlider;

	public string phoneNumb;
	public int rowNumber { get; set; }

	public string rowContent { get; set; }
	public string iD{ get; set; }

	public List<FreakContactsCellView> contactDetailsList;


}
